var class_gracenote_s_d_k_1_1_gn_response_video_suggestions =
[
    [ "GnResponseVideoSuggestions", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a3bcc2653c641ad280f6df5aebe04e0d7", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a3b39abc7eecef55a4cf90fbef6c206b6", null ],
    [ "SuggestionText", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#ac91fcea4b3ed6b542c18777bf8e1c1eb", null ],
    [ "SuggestionTitle", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#ac95b01a6f177abe92100cb3777ccc432", null ],
    [ "SuggestionType", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a58834c282cf7385cd0a8f1fb5f46d85f", null ],
    [ "NeedsDecision", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#afe819928c33fdbe132fe2ab55c28a9a3", null ],
    [ "RangeEnd", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a80b79400d0f491ce342a05b5e1b24fd8", null ],
    [ "RangeStart", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a4cb923009d7fd118de224f23e92a40c8", null ],
    [ "RangeTotal", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a3399897a39c9c3c646a56afdd68ec7c2", null ],
    [ "ResultCount", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html#a2ebc0a13ddc5fd151205f252c9b4964c", null ]
];