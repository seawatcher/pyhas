#pragma once
#include "../src_wrapper/gn_audiosource.hpp"
#include "bass.h" /* I'm in .../xtralibs/bass */


namespace gracenote
{
	class GnAudioFile : public gracenote::GnAudioSource
	{
	public:
		GnAudioFile(gnsdk_cstr_t filename);
		virtual ~GnAudioFile(void);

		/*
		** GnAudioSource Implementation
		*/
		virtual gnsdk_uint32_t	SourceInit();
		virtual gnsdk_void_t	SourceClose();

		virtual gnsdk_uint32_t	SamplesPerSecond();
		virtual gnsdk_uint32_t	SampleSizeInBits();
		virtual gnsdk_uint32_t	NumberOfChannels();

		virtual gnsdk_size_t	GetData(gnsdk_byte_t* audio_buffer, gnsdk_size_t buffer_size);

	private:
		gnsdk_char_t		m_filename[300];
		HSTREAM				m_chan;
		BASS_CHANNELINFO	m_info;
	};

}
