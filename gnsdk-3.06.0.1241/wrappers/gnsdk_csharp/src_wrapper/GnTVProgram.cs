/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnTVProgram : GnDataObject {
  private HandleRef swigCPtr;

  internal GnTVProgram(IntPtr cPtr, bool cMemoryOwn) : base(gnsdk_csharp_marshalPINVOKE.GnTVProgram_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnTVProgram obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnTVProgram() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnTVProgram(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GnTVProgram() : this(gnsdk_csharp_marshalPINVOKE.new_GnTVProgram__SWIG_0(), true) {
  }

  public GnTVProgram(string id, string id_tag) : this(gnsdk_csharp_marshalPINVOKE.new_GnTVProgram__SWIG_1(id, id_tag), true) {
  }

  public GnCredit Credit() {
    GnCredit ret = new GnCredit(gnsdk_csharp_marshalPINVOKE.GnTVProgram_Credit(swigCPtr), true);
    return ret;
  }

  public GnVideoWork Work() {
    GnVideoWork ret = new GnVideoWork(gnsdk_csharp_marshalPINVOKE.GnTVProgram_Work(swigCPtr), true);
    return ret;
  }

  public string GnID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTVProgram_GnID_get(swigCPtr) );
	} 

  }

  public string GnUID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTVProgram_GnUID_get(swigCPtr) );
	} 

  }

  public string ProductID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTVProgram_ProductID_get(swigCPtr) );
	} 

  }

  public string TUI {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTVProgram_TUI_get(swigCPtr) );
	} 

  }

  public string TUITag {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTVProgram_TUITag_get(swigCPtr) );
	} 

  }

  public bool FullResult {
    get {
      bool ret = gnsdk_csharp_marshalPINVOKE.GnTVProgram_FullResult_get(swigCPtr);
      return ret;
    } 
  }

}

}
