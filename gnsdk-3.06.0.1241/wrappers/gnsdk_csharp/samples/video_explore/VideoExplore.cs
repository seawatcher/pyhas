/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoExplore.cs
 *  Description:
 *  This sample shows basic video explore functionality.
 *
 *  Command-line Syntax:
 * sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoExplore
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk
     
        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

    
      

        #endregion

        private static void FindWorkForContributor(GnContributor contributor, GnUser user)
        {
            //Console.WriteLine("Fetching works done by this Actor... ");								
            GnVideo video = new GnVideo(user);
            /* fetch all work done by contributor */
            GnResponseVideoWork responseVideoWork = video.FindWorks(contributor);
            Console.WriteLine("\nNumber works: " + responseVideoWork.Works.count());
            GnVideoWorkEnumerable videoWorkEnumerable = responseVideoWork.Works;
            GnVideoWorkEnumerator videoWorkEnumerator = videoWorkEnumerable.GetEnumerator();
            int count = 0;
            while (videoWorkEnumerator.hasNext())
            {
                GnVideoWork videoWork = videoWorkEnumerator.next();
                /* Work title */
                GnTitle gnTitle = videoWork.OfficialTitle;
                String title = gnTitle.Display;
                count++;
                Console.WriteLine("\t" + count + " : " + title);


            }
        }

        private static void DisplayVideoWork(GnVideoWorkEnumerable workEnumerator)
        {
            GnVideoWorkEnumerator videoWorkEnumerator = workEnumerator.GetEnumerator();
            /* Explore contributors : Who are the cast and crew? */
            Console.WriteLine("\nVideo Work - Crouching Tiger, Hidden Dragon: \n\nActor Credits:");

            while (videoWorkEnumerator.hasNext())
            {
                GnVideoWork videoWork = videoWorkEnumerator.next();
                GnVideoCreditEnumerable videoCreditEnumerable = videoWork.VideoCredits;
                GnVideoCreditEnumerator videoCreditEnumerator = videoCreditEnumerable.GetEnumerator();
                int i = 0;
                GnContributor tempContribObj = null;

                while (videoCreditEnumerator.hasNext())
                {
                    GnVideoCredit videoCredit = videoCreditEnumerator.next();
                    string rollid = videoCredit.RoleID.ToString();

                    if (("15942").Equals(rollid))
                    {
                        ++i;

                        GnContributor contribName = videoCredit.Contributor;
                        if (1 == i)
                        {
                            tempContribObj = contribName;

                        }

                        GnNameEnumerable creditNameEnumerable = contribName.NamesOfficial;
                        GnNameEnumerator creditNameEnumerator = creditNameEnumerable.GetEnumerator();
                        while (creditNameEnumerator.hasNext())
                        {
                            GnName name = creditNameEnumerator.next();
                            Console.WriteLine("\t" + i + " : " + name.Display);
                        }
                    }
                }	//end of work loop	

                /* Now find the work done by contributor */
                GnNameEnumerable nameEnumerable = tempContribObj.NamesOfficial;
                GnNameEnumerator nameEnumerator = nameEnumerable.GetEnumerator();
                while (nameEnumerator.hasNext())
                {
                    GnName name = nameEnumerator.next();
                    Console.WriteLine("\nActor Credit: " + name.Display + " Filmography");
                }

             
                FindWorkForContributor(tempContribObj, user);
            }
        }

        private static void DoVideoExplore(GnUser user)
        {
			//string searchText = "Crouching Tiger Hidden Dragon";
            string serializedGDO = "WEcxA6R75JwbiGUIxLFZHBr4tv+bxvwlIMr0XK62z68zC+/kDDdELzwiHmBPkmOvbB4rYEY/UOOvFwnk6qHiLdb1iFLtVy44LfXNsTH3uNgYfSymsp9uL+hyHfrzUSwoREk1oX/rN44qn/3NFkEYa2FoB73sRxyRkfdnTGZT7MceHHA/28aWZlr3q48NbtCGWPQmTSrK";

            //Console.WriteLine("*****Sample Video Explore:*****");	

            GnVideo video = new GnVideo(user);
            GnDataObject inpupdataobject = new GnDataObject(serializedGDO);

            //GnResponseVideoWork VideoWorksResponse = video.FindWorks(searchText, GnVideoSearchField.kSearchFieldWorkTitle, GnVideoSearchType.kSearchTypeDefault);	
            GnResponseVideoWork videoWorksResponse = video.FindWorks(inpupdataobject);

            if (videoWorksResponse.Works.count() > 0)
            {
                /**********************************************
                    *  Needs decision (match resolution) check
                    **********************************************/
                if (videoWorksResponse.NeedsDecision)
                {
                    /**********************************************
                     * Resolve match here
                     **********************************************/
                }
                DisplayVideoWork(videoWorksResponse.Works);
            }
        }
        
        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);
						
						// Initialize the Storage SQLite Library
						GnStorageSqlite gnStorage = new GnStorageSqlite();
                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                       GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

						gnsdk.SetDefaultLocale(locale);						
                        DoVideoExplore(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }
    }
}
