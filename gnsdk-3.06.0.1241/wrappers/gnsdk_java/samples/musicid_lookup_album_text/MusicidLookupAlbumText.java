package musicid_lookup_album_text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class MusicidLookupAlbumText {
	/*
	 * Set to false to have the sample perform online queries. 
	 * Set to true to have the sample perform local queries. 
	 * For local queries, Gracenote databases are required.
	 */
	private static boolean USELOCAL = false;
	
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}
	
	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}
	
	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}

	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				localeEvents
				);

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		GnUser gnsdkUser = null;
		GnString user_string = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			user_string = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				bw.write(user_string.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		        String savedUserString = br.readLine();
		        
		        gnsdkUser = new GnUser(
		        		savedUserString,
						clientId, 
						clientIdTag, 
						applicationVersion
						);
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return gnsdkUser;
	}
	
	private static int doMatchSelection(GnResponseAlbums gnResponseAlbums) throws InvalidObjectException, GnException
	{
		/*
		This is where any matches that need resolution/disambiguation are iterated
		and a single selection of the best match is made.

		For this simplified sample, we'll just echo the matches and select the first match.
		*/		
		System.out.printf("%16s %d\n", "Match count:", gnResponseAlbums.albums().count());
		
		GnAlbumIterator gnAlbumIterator = gnResponseAlbums.albums().getIterator();
		
		while (gnAlbumIterator.hasNext()) {
			GnAlbum album = (GnAlbum) gnAlbumIterator.next();
			System.out.printf( "%16s %s\n", "Title:",  album.title().display());
		}
	
		return 0;
	}

	private static void doSampleAlbumSearch(GnUser user) throws GnException, InvalidObjectException {		
		System.out.println("\n*****Sample Album Search*****");
		
		GnMusicID musicID = new GnMusicID(user, null);
		
		if (USELOCAL) {
			musicID.optionLookupMode(GnLookupMode.kLookupModeLocal);
		}

		GnResponseAlbums response = musicID.findAlbums("Supernatural", "Africa Bamba", "Santana");

		/* See how many albums were found. */
		long count = response.albums().count();

		/* See if we need any follow-up queries or disambiguation */
		if (count == 0) {
			System.out.println("\nNo albums found for the input.\n");
		} else {
			int choiceOrdinal = 0;
			if(response.needsDecision()) {
				doMatchSelection(response);
			}else {
				choiceOrdinal = 0;
			}
			
			GnAlbum album = response.albums().at(choiceOrdinal).next();
			
			if(!album.fullResult()) {
				GnResponseAlbums gnFollowupResponse = musicID.findAlbums(album);
					
				album = gnFollowupResponse.albums().at(0).next();
			}
		
			System.out.printf( "%16s\n", "Final album:");
			System.out.printf( "%16s %s\n", "Title:",  album.title().display());
		}
	}
	
	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				if (USELOCAL) {
					GnStorageSqlite sqliteStorage = new GnStorageSqlite();
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				loadLocale(gnsdk, user);
			
				doSampleAlbumSearch(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
