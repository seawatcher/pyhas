var class_gracenote_s_d_k_1_1_gn_audio_work =
[
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a959616937928d189c96bd9e9d057e37d", null ],
    [ "CompositionForm", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a0b729d395241baac79bbcd7015494c47", null ],
    [ "Credit", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a7268d15ba86034337fd48d3bc9c9a225", null ],
    [ "Era", "class_gracenote_s_d_k_1_1_gn_audio_work.html#adf9f3721e095de103054c479963e67d6", null ],
    [ "Genre", "class_gracenote_s_d_k_1_1_gn_audio_work.html#ad9e6d87cac9069215a4834f1cfd0afef", null ],
    [ "GnID", "class_gracenote_s_d_k_1_1_gn_audio_work.html#abfbeec6a2462814319e48361267ca5a6", null ],
    [ "GnUID", "class_gracenote_s_d_k_1_1_gn_audio_work.html#af374c0e3ef1486ea616f4ac414774568", null ],
    [ "Origin", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a13afd88b619103fb9b5144cd13c9ff7b", null ],
    [ "TagID", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a9c9cfae12712974b32e9f0381da5ecdf", null ],
    [ "Title", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a61009c099aa5de1d1a5b6554823fcb0b", null ],
    [ "TUI", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a8d8488d7de9b077da4e04e9ac8aaa454", null ],
    [ "TUITag", "class_gracenote_s_d_k_1_1_gn_audio_work.html#a064f7df3ed33fa0510df332c38328b02", null ]
];