/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 *  Name: musicid_album_lookup_toc
 *  Description:
 *  This example looks up an Album based on its TOC.
 *
 *  Command-line Syntax:
 *  sample client_id client_id_tag license librarypath
 */

#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::musicid;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}

};


/*-----------------------------------------------------------------------------
 *  do_match_selection
 */
static int
do_match_selection(GnResponseAlbums& response)
{
	/*
	   This is where any matches that need resolution/disambiguation are iterated
	   and a single selection of the best match is made.

	   For this simplified sample, we'll just echo the matches and select the first match.
	 */
	printf( "%16s %d\n", "Match count:", response.Albums().count());

	metadata::album_iterator it_album = response.Albums().begin();
	for (; it_album != response.Albums().end(); ++it_album )
	{
		printf( "%16s %s\n", "Title:", it_album->Title().Display() );

	}

	return 0;
}


/*-----------------------------------------------------------------------------
 *  perform_album_toc_lookup
 */
void
perform_album_toc_lookup(GnUser& user)
{
	bool           needs_decision = false;
	gnsdk_uint32_t choice_ordinal = 0;
	gnsdk_cstr_t   toc            = "150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342";

	printf("\n*****MusicID TOC Query*****\n");
	/*LookupStatusEvents statusEvents;*/
	GnMusicID music_id(user, GNSDK_NULL);   /*LookupStatusEvents can be used as &statusEvents to view progress*/

	GnResponseAlbums response = music_id.FindAlbums(toc);

	int count =  response.Albums().count();
	if (count == 0)
	{
		printf("\nNo albums found for the input.\n");
	}
	else
	{
		needs_decision = response.NeedsDecision();

		/* See if selection of one of the albums needs to happen */
		if (needs_decision)
		{
			choice_ordinal = do_match_selection(response);

		}
		else
		{
			/* no need for disambiguation, we'll take the first album */
			choice_ordinal = 0; /* Since iterator starts reading from 0th position in the list*/
		}

		GnAlbum album = response.Albums().at(choice_ordinal).next();

		bool fullResult = album.FullResult();

		/* if we only have a partial result, we do a follow-up query to retrieve the full album */
		if (!fullResult)
		{
			/* do followup query to get full object. Setting the partial album as the query input. */
			GnResponseAlbums followup_response = music_id.FindAlbums(album);

			/* now our first album is the desired result with full data */
			album = followup_response.Albums().at(0).next();

		}
		printf( "%16s\n", "Final album:");
		printf( "%16s %s\n", "Title:", album.Title().Display() );
	}

}


#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void
display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal     = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  gdb_version = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);
	printf("Gracenote DB Version : %s\n", gdb_version);
}


#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	/*LookupStatusEvents localeEvents;*/

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, GNSDK_NULL ); /*use &statusEvents instead of GNSDK_NULL to view progress*/

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";  /* Increment with each version of your app */
	gnsdk_error_t error;


	/*    Client ID, Client ID Tag, License file and lib path must be passed in */
	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		printf("\nUsage:\n%s clientId clientIdTag license gnsdkLibraryPath\n", argv[0]);
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",
		    GNSDK_LOG_PKG_ALL,
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,
		    GNSDK_LOG_OPTION_ALL,
		    0,                                                          /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                                 /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		user.OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		/*Perform album lookup with new user*/
		perform_album_toc_lookup(user);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

	return 0;
}

