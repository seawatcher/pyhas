 /***************************************************************************/
 /*                                                                         */
 /*       Copyright (C) 1996  Daniel Sleator and David Temperley            */
 /*  See file "README" for information about commercial use of this system  */
 /*                                                                         */
 /***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "analyze.h"

void bad_param(char * line) {
  char * x;
  x = strchr(line, '\n');
  if (x != NULL) *x = '\0';
  printf("Warning: cannot interpret \"%s\" in parameters file -- skipping it.\n", line);
}

void read_parameter_file(char * filename, int file_specified) {
  FILE * param_stream;
  char line[100];
  char part[3][100];
  int i;
  float value;

  param_stream = fopen(filename, "r");
  if (param_stream == NULL) {
    if (file_specified) {
      printf("Warning: cannot open \"%s\".  Using defaults.\n", filename);
    } else {
/*      printf("Cannot open \"%s\".  Using defaults.\n", filename);       */
    }
    return;
  }
  printf("Reading parameters from file \"%s\".\n", filename);
  while(fgets(line, sizeof(line), param_stream) != NULL) {
    for (i=0; isspace(line[i]); i++);
    if (line[i] == '%' || line[i] == '\0') continue;  /* ignore comment and blank lines */
    for (i=0; line[i] != '\0'; i++) if (line[i] == '=') line[i] = ' '; /* kill all '=' signs */
    if (sscanf(line, "%99s %99s %99s", part[0], part[1], part[2]) != 2) {
      bad_param(line);
      continue;
    }
    if (sscanf(part[1], "%f", &value) != 1) {
      bad_param(line);
      continue;
    }
    if (strcmp(part[0], "tpc_variance_scale_factor") == 0) {
      tpc_variance_scale_factor = value;
    } else if (strcmp(part[0], "buckets_per_unit_of_cog") == 0) {
      buckets_per_unit_of_cog = value;
    } else if (strcmp(part[0], "half_life") == 0) {
      half_life = value;
    } else if (strcmp(part[0], "pruning_cutoff") == 0) {
      pruning_cutoff = value;
    } else if (strcmp(part[0], "verbosity") == 0) {
      verbosity = value;
      value = verbosity;  /* show the truncated value */
    } else {
      bad_param(line);
      continue;
    }
    printf("%s = %f\n", part[0], value);
  }
  close(param_stream);
}

void usage(char * s) {
  char * x;
  x = strrchr(s, '/');
  if (x != NULL) x++; else x = s;
  printf("usage: %s [-p parameter-file] [notes-file]\n", x);
  exit(1);
}

void main (int argc, char * argv[]) {
  Note * nl;
  int i;
  Chord * clist, * m_clist, * cm_clist;
  char * parameter_file;

  if (argc >= 2 && strcmp(argv[1], "-p") == 0) {
    if (argc < 3) usage(argv[0]);
    parameter_file = argv[2];
    i = 3;
  } else {
    parameter_file = "parameters";
    i = 1;
  }

  if (argc == i+1) {
    /* open the specified notes file */
    instream = fopen(argv[i], "r");
    if (instream == NULL) {
      printf("Cannot open %s\n", argv[i]);
      exit(1);
    }
  } else if (argc == i) {
    instream = stdin;
  } else {
    usage(argv[0]);
  }

  read_parameter_file(parameter_file, parameter_file == argv[2]);

  alpha = (log(2.0)/(half_life * 1000));
  initialize_hashing();

  nl = build_note_list_from_input();
  label_notes_with_ornamental_dissonance_penalties(nl);

  clist = build_chord_representation(nl);
  if (verbosity >= 4) print_chord_list(clist);
  m_clist = build_metered_chord_representation(clist);
  if (verbosity >= 4) print_chord_list(m_clist);
  cm_clist = compact_metered_chord_representation(m_clist);  
  if (verbosity >= 3) print_chord_list(cm_clist);
  
  initialize_harmonic(cm_clist);
  compute_harmonic_table();
  if (verbosity >= 1) print_harmonic();
  if (verbosity >= 0) ASCII_display(m_clist);
  cleanup_harmonic();
  free_chords(clist);
  free_chords(m_clist);
  free_chords(cm_clist);
}
