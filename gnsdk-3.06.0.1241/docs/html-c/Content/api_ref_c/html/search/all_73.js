var searchData=
[
  ['sortable',['Sortable',['../group___g_d_o___value_keys___sortable.html',1,'']]],
  ['search_20fields',['Search Fields',['../group___music___music_i_d___search_fields.html',1,'']]],
  ['storage_20functions',['Storage Functions',['../group___playlist___storage_functions.html',1,'']]],
  ['setup',['Setup',['../group___setup.html',1,'']]],
  ['storage_20ids',['Storage IDs',['../group___setup___storage_i_ds.html',1,'']]],
  ['source_5ferror_5fcode',['source_error_code',['../group___manager___error_management___types_enums.html#ga9eb55e69e6723723a24cd2fc40ef2c24',1,'gnsdk_error_info_t']]],
  ['source_5ferror_5fmodule',['source_error_module',['../group___manager___error_management___types_enums.html#gae1fefcdd2d5eb05f607d1bd7126e0235',1,'gnsdk_error_info_t']]],
  ['status_20callbacks',['Status Callbacks',['../group___status_callbacks.html',1,'']]],
  ['storage_20and_20caching',['Storage and Caching',['../group___storageand_caching.html',1,'']]],
  ['storage_20ids',['Storage IDs',['../group___storageand_caching___general___storage_i_ds.html',1,'']]],
  ['sqlite',['SQLITE',['../group___storageand_caching___s_q_l_i_t_e.html',1,'']]],
  ['search_20fields',['Search Fields',['../group___video___search_fields.html',1,'']]]
];
