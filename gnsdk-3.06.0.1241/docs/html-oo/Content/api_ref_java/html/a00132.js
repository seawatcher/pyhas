var a00132 =
[
    [ "GnLicenseInputMode", "a00047.html", "a00047" ],
    [ "GnUserRegisterMode", "a00154.html", "a00154" ],
    [ "GnSDK", "a00132.html#ac8e22e345eb5b49dd301a05fe6b73900", null ],
    [ "GnSDK", "a00132.html#afaeeec99342295bde6fef1571391e9ed", null ],
    [ "availableLocales", "a00132.html#abdcda483d2d48f0bbcbf76e4dd982b88", null ],
    [ "delete", "a00132.html#a326228b9b3344dab9d4db27d71686c47", null ],
    [ "finalize", "a00132.html#a77f659ec46267355e7c06fa13b4ee164", null ],
    [ "loggingDisable", "a00132.html#a7171da159f48883898f50c367285231f", null ],
    [ "loggingEnable", "a00132.html#a349c29548065b2c5018fb431f78876d7", null ],
    [ "loggingWrite", "a00132.html#ab50288774b9ff6e1d988f7d6aed80ca3", null ],
    [ "registerUser", "a00132.html#a7c9080f90609c84f6ef9d514dcd058b0", null ],
    [ "setDefaultLocale", "a00132.html#a32988d15f0bdac4f5617959d7f02053b", null ],
    [ "storageCleanup", "a00132.html#a666f5cb1a1932450d35a4f604cddb648", null ],
    [ "storageCompact", "a00132.html#a59f62bdb14c19085d34884df496353db", null ],
    [ "storageFlush", "a00132.html#a30da903ddcce579b6c3893718fde986d", null ],
    [ "storageLocation", "a00132.html#ae81eb9202f4b2c187dd77a7e44841424", null ],
    [ "swigCMemOwn", "a00132.html#ae48e45ebbb97b4a5b02735ec81b31f70", null ]
];