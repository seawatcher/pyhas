/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* gnsdk.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */

#include "gnsdk.hpp"

using namespace gracenote;

/******************************************************************************
** GnSDK
*/

/* static private member definition*/
gnsdk_manager_handle_t GnSDK::mManagerHandle = GNSDK_NULL;
gnsdk_uint32_t         GnSDK::mModulesInit   = 0;

/* static public member definition*/

/*
 * Logging package IDs
 */
gnsdk_uint16_t GnSDK::GN_LOG_PKG_ALL   = 0xFF;
gnsdk_uint16_t GnSDK::GN_LOG_PKG_GCSL  = 0x7E;
gnsdk_uint16_t GnSDK::GN_LOG_PKG_GNSDK = 0xFE;

/*
 * Logging Filter masks
 */
gnsdk_uint32_t GnSDK::GN_LOG_FILTER_ERROR   = 0x00000001;
gnsdk_uint32_t GnSDK::GN_LOG_FILTER_WARNING = 0x00000002;
gnsdk_uint32_t GnSDK::GN_LOG_FILTER_INFO    = 0x00000004;
gnsdk_uint32_t GnSDK::GN_LOG_FILTER_DEBUG   = 0x00000008;

gnsdk_uint32_t GnSDK::GN_LOG_LEVEL_ERROR   = ( GnSDK::GN_LOG_FILTER_ERROR );
gnsdk_uint32_t GnSDK::GN_LOG_LEVEL_WARNING = ( GnSDK::GN_LOG_FILTER_WARNING | GnSDK::GN_LOG_FILTER_ERROR );
gnsdk_uint32_t GnSDK::GN_LOG_LEVEL_INFO    = ( GnSDK::GN_LOG_FILTER_INFO | GnSDK::GN_LOG_FILTER_WARNING | GnSDK::GN_LOG_FILTER_ERROR );
gnsdk_uint32_t GnSDK::GN_LOG_LEVEL_DEBUG   = ( GnSDK::GN_LOG_FILTER_DEBUG | GnSDK::GN_LOG_FILTER_INFO | GnSDK::GN_LOG_FILTER_WARNING | GnSDK::GN_LOG_FILTER_ERROR );
gnsdk_uint32_t GnSDK::GN_LOG_LEVEL_ALL     = GnSDK::GN_LOG_LEVEL_DEBUG;

/*
 * Logging option masks
 */
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_TIMESTAMP    = 0x01000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_CATEGORY     = 0x02000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_PACKAGE      = 0x04000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_THREAD       = 0x08000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_SOURCEINFO   = 0x10000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_NEWLINE      = 0x20000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_NONE         = 0x00000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_ALL          = 0xFF000000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_SYNCHRONOUS  = 0x00100000;
gnsdk_uint32_t GnSDK::GN_LOG_OPTION_ARCHIVEDAILY = 0x00200000;

/*
 * Languages
 */
gnsdk_cstr_t GnSDK::kLanguageEnglish            = GNSDK_LANG_ENGLISH;
gnsdk_cstr_t GnSDK::kLanguageChineseSimplified  = GNSDK_LANG_CHINESE_SIMP;
gnsdk_cstr_t GnSDK::kLanguageChineseTraditional = GNSDK_LANG_CHINESE_TRAD;
gnsdk_cstr_t GnSDK::kLanguageDutch              = GNSDK_LANG_DUTCH;
gnsdk_cstr_t GnSDK::kLanguageFrench             = GNSDK_LANG_FRENCH;
gnsdk_cstr_t GnSDK::kLanguageGerman             = GNSDK_LANG_GERMAN;
gnsdk_cstr_t GnSDK::kLanguageItalian            = GNSDK_LANG_ITALIAN;
gnsdk_cstr_t GnSDK::kLanguageJapanese           = GNSDK_LANG_JAPANESE;
gnsdk_cstr_t GnSDK::kLanguageKorean             = GNSDK_LANG_KOREAN;
gnsdk_cstr_t GnSDK::kLanguagePortugueseBrazil   = GNSDK_LANG_PORTUGUESE_BRAZIL;
gnsdk_cstr_t GnSDK::kLanguageRussian            = GNSDK_LANG_RUSSIAN;
gnsdk_cstr_t GnSDK::kLanguageSpanish            = GNSDK_LANG_SPANISH;
gnsdk_cstr_t GnSDK::kLanguageSwedish            = GNSDK_LANG_SWEDISH;
gnsdk_cstr_t GnSDK::kLanguageThai               = GNSDK_LANG_THAI;
gnsdk_cstr_t GnSDK::kLanguageTurkish            = GNSDK_LANG_TURKISH;
gnsdk_cstr_t GnSDK::kLanguagePolish             = GNSDK_LANG_POLISH;
gnsdk_cstr_t GnSDK::kLanguageFarsi              = GNSDK_LANG_FARSI;
gnsdk_cstr_t GnSDK::kLanguageVietnamese         = GNSDK_LANG_VIETNAMESE;
gnsdk_cstr_t GnSDK::kLanguageHungarian          = GNSDK_LANG_HUNGARIAN;
gnsdk_cstr_t GnSDK::kLanguageCzech              = GNSDK_LANG_CZECH;
gnsdk_cstr_t GnSDK::kLanguageSlovak             = GNSDK_LANG_SLOVAK;
gnsdk_cstr_t GnSDK::kLanguageRomanian           = GNSDK_LANG_ROMANIAN;
gnsdk_cstr_t GnSDK::kLanguageGreek              = GNSDK_LANG_GREEK;
gnsdk_cstr_t GnSDK::kLanguageArabic             = GNSDK_LANG_ARABIC;
gnsdk_cstr_t GnSDK::kLanguageBahasaIndonesia    = GNSDK_LANG_BAHASA_INDONESIA;
gnsdk_cstr_t GnSDK::kLanguageFinnish            = GNSDK_LANG_FINNISH;
gnsdk_cstr_t GnSDK::kLanguageNorwegian          = GNSDK_LANG_NORWEGIAN;
gnsdk_cstr_t GnSDK::kLanguageCroatian           = GNSDK_LANG_CROATIAN;
gnsdk_cstr_t GnSDK::kLanguageBulgarian          = GNSDK_LANG_BULGARIAN;
gnsdk_cstr_t GnSDK::kLanguageSerbian            = GNSDK_LANG_SERBIAN;
gnsdk_cstr_t GnSDK::kLanguageDanish             = GNSDK_LANG_DANISH;

/*
 * Regions
 */
gnsdk_cstr_t GnSDK::kRegionDefault      = GNSDK_REGION_DEFAULT;
gnsdk_cstr_t GnSDK::kRegionGlobal       = GNSDK_REGION_GLOBAL;
gnsdk_cstr_t GnSDK::kRegionUS           = GNSDK_REGION_US;
gnsdk_cstr_t GnSDK::kRegionJapan        = GNSDK_REGION_JAPAN;
gnsdk_cstr_t GnSDK::kRegionChina        = GNSDK_REGION_CHINA;
gnsdk_cstr_t GnSDK::kRegionTaiwan       = GNSDK_REGION_TAIWAN;
gnsdk_cstr_t GnSDK::kRegionKorea        = GNSDK_REGION_KOREA;
gnsdk_cstr_t GnSDK::kRegionEurope       = GNSDK_REGION_EUROPE;
gnsdk_cstr_t GnSDK::kRegionLatinAmerica = GNSDK_REGION_LATIN_AMERICA;

/*
 * Descriptor
 */
gnsdk_cstr_t GnSDK::kDescriptorDefault    = GNSDK_DESCRIPTOR_DEFAULT;
gnsdk_cstr_t GnSDK::kDescriptorSimplified = GNSDK_DESCRIPTOR_SIMPLIFIED;
gnsdk_cstr_t GnSDK::kDescriptorDetailed   = GNSDK_DESCRIPTOR_DETAILED;

/*
 * Locale groups
 */
gnsdk_cstr_t GnSDK::kLocaleGroupMusic    = GNSDK_LOCALE_GROUP_MUSIC;
gnsdk_cstr_t GnSDK::kLocaleGroupVideo    = GNSDK_LOCALE_GROUP_VIDEO;
gnsdk_cstr_t GnSDK::kLocaleGroupPlaylist = GNSDK_LOCALE_GROUP_PLAYLIST;
gnsdk_cstr_t GnSDK::kLocaleGroupEPG      = GNSDK_LOCALE_GROUP_EPG;

/*
 * List Types
 */
gnsdk_cstr_t GnSDK::kListTypeLanguages          = GNSDK_LIST_TYPE_LANGUAGES;
gnsdk_cstr_t GnSDK::kListTypeGenres             = GNSDK_LIST_TYPE_GENRES;
gnsdk_cstr_t GnSDK::kListTypeOrigins            = GNSDK_LIST_TYPE_ORIGINS;
gnsdk_cstr_t GnSDK::kListTypeEras               = GNSDK_LIST_TYPE_ERAS;
gnsdk_cstr_t GnSDK::kListTypeArtistTypes        = GNSDK_LIST_TYPE_ARTISTTYPES;
gnsdk_cstr_t GnSDK::kListTypeRoles              = GNSDK_LIST_TYPE_ROLES;
gnsdk_cstr_t GnSDK::kListTypeGenreVideos        = GNSDK_LIST_TYPE_GENRES_VIDEO;
gnsdk_cstr_t GnSDK::kListTypeRatings            = GNSDK_LIST_TYPE_RATINGS;
gnsdk_cstr_t GnSDK::kListTypeRatingTypes        = GNSDK_LIST_TYPE_RATINGTYPES;
gnsdk_cstr_t GnSDK::kListTypeContributors       = GNSDK_LIST_TYPE_CONTRIBUTORS;
gnsdk_cstr_t GnSDK::kListTypeFeatureTypes       = GNSDK_LIST_TYPE_FEATURETYPES;
gnsdk_cstr_t GnSDK::kListTypeVideoRegions       = GNSDK_LIST_TYPE_VIDEOREGIONS;
gnsdk_cstr_t GnSDK::kListTypeVideoTypes         = GNSDK_LIST_TYPE_VIDEOTYPES;
gnsdk_cstr_t GnSDK::kListTypeMediaTypes         = GNSDK_LIST_TYPE_MEDIATYPES;
gnsdk_cstr_t GnSDK::kListTypeVideoSerialTypes   = GNSDK_LIST_TYPE_VIDEOSERIALTYPES;
gnsdk_cstr_t GnSDK::kListTypeWorkTypes          = GNSDK_LIST_TYPE_WORKTYPES;
gnsdk_cstr_t GnSDK::kListTypeMediaSpaces        = GNSDK_LIST_TYPE_MEDIASPACES;
gnsdk_cstr_t GnSDK::kListTypeMoods              = GNSDK_LIST_TYPE_MOODS;
gnsdk_cstr_t GnSDK::kListTypeTempos             = GNSDK_LIST_TYPE_TEMPOS;
gnsdk_cstr_t GnSDK::kListTypeCompostionForm     = GNSDK_LIST_TYPE_COMPOSITION_FORM;
gnsdk_cstr_t GnSDK::kListTypeInstrumentation    = GNSDK_LIST_TYPE_INSTRUMENTATION;
gnsdk_cstr_t GnSDK::kListTypeVideoStoryType     = GNSDK_LIST_TYPE_VIDEOSTORYTYPE;
gnsdk_cstr_t GnSDK::kListTypeVideoAudience      = GNSDK_LIST_TYPE_VIDEOAUDIENCE;
gnsdk_cstr_t GnSDK::kListTypeVideoMood          = GNSDK_LIST_TYPE_VIDEOMOOD;
gnsdk_cstr_t GnSDK::kListTypeVideoReputation    = GNSDK_LIST_TYPE_VIDEOREPUTATION;
gnsdk_cstr_t GnSDK::kListTypeVideoScenario      = GNSDK_LIST_TYPE_VIDEOSCENARIO;
gnsdk_cstr_t GnSDK::kListTypeVideoSettingEnv    = GNSDK_LIST_TYPE_VIDEOSETTINGENV;
gnsdk_cstr_t GnSDK::kListTypeVideoSettingPeriod = GNSDK_LIST_TYPE_VIDEOSETTINGPERIOD;
gnsdk_cstr_t GnSDK::kListTypeVideoSource        = GNSDK_LIST_TYPE_VIDEOSOURCE;
gnsdk_cstr_t GnSDK::kListTypeVideoStyle         = GNSDK_LIST_TYPE_VIDEOSTYLE;
gnsdk_cstr_t GnSDK::kListTypeVideoTopic         = GNSDK_LIST_TYPE_VIDEOTOPIC;
gnsdk_cstr_t GnSDK::kListTypeEpgViewingTypes    = GNSDK_LIST_TYPE_EPGVIEWINGTYPES;
gnsdk_cstr_t GnSDK::kListTypeEpgAudioTypes      = GNSDK_LIST_TYPE_EPGAUDIOTYPES;
gnsdk_cstr_t GnSDK::kListTypeEpgVideoTypes      = GNSDK_LIST_TYPE_EPGVIDEOTYPES;
gnsdk_cstr_t GnSDK::kListTypeEpgCaptionTypes    = GNSDK_LIST_TYPE_EPGCAPTIONTYPES;
gnsdk_cstr_t GnSDK::kListTypeIpgCategoriesL1    = GNSDK_LIST_TYPE_IPGCATEGORIES_L1;
gnsdk_cstr_t GnSDK::kListTypeIpgCategoriesL2    = GNSDK_LIST_TYPE_IPGCATEGORIES_L2;
gnsdk_cstr_t GnSDK::kListTypeEpgProductionTypes = GNSDK_LIST_TYPE_EPGPRODUCTIONTYPES;
gnsdk_cstr_t GnSDK::kListTypeEpgDeviceTypes     = GNSDK_LIST_TYPE_EPGDEVICETYPES;

/*
 * Storage Names
 */
gnsdk_cstr_t GnSDK::kStorageQueryCache   = GNSDK_MANAGER_STORAGE_QUERYCACHE;
gnsdk_cstr_t GnSDK::kStorageListsCache   = GNSDK_MANAGER_STORAGE_LISTSCACHE;
gnsdk_cstr_t GnSDK::kStorageContentCache = GNSDK_MANAGER_STORAGE_CONTENTCACHE;

GnSDK::GnSDK(gnsdk_cstr_t licenseFile, GnLicenseInputMode licenseInputMode) throw ( GnError )
{
	gnsdk_size_t  licenseMode = 0;
	gnsdk_error_t error;

	switch(licenseInputMode)
	{
	case kString:
		licenseMode = GNSDK_MANAGER_LICENSEDATA_NULLTERMSTRING;
		break;

	case kFilename:
		licenseMode = GNSDK_MANAGER_LICENSEDATA_FILENAME;
		break;

	case kStandardIn:
		licenseMode = GNSDK_MANAGER_LICENSEDATA_STDIN;
		break;

	default:
		licenseMode = 0;
		break;
	}

	error = gnsdk_manager_initialize(&mManagerHandle, licenseFile, licenseMode);
	if(error) { throw  GnError(); }
}

GnSDK::~GnSDK() throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_manager_shutdown();
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	mModulesInit = 0;
}

/*-----------------------------------------------------------------------------
 *  init_gnsdk_module
 */
void GnSDK::init_gnsdk_module(gnsdk_uint32_t moduleId)
{
	gnsdk_error_t error;

	switch(moduleId)
	{
#if GNSDK_MUSICID
	case GNSDK_COMPONENT_MUSICID:
		if(!( mModulesInit & GNSDK_COMPONENT_MUSICID ) )
		{
			error = gnsdk_musicid_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_MUSICID;
		}
		break;
#endif

#if GNSDK_DSP
	case GNSDK_COMPONENT_DSP:
		if(!( mModulesInit & GNSDK_COMPONENT_DSP ) )
		{
			error = gnsdk_dsp_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_DSP;
		}
		break;
#endif

#if GNSDK_MUSICID_FILE
	case GNSDK_COMPONENT_MUSICIDFILE:
		if(!( mModulesInit & GNSDK_COMPONENT_MUSICIDFILE ) )
		{
			error = gnsdk_musicidfile_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_MUSICIDFILE;
		}
		break;
#endif

#if GNSDK_MUSICID_STREAM
	case GNSDK_COMPONENT_MUSICIDSTREAM:
		if(!( mModulesInit & GNSDK_COMPONENT_MUSICIDSTREAM ) )
		{
			error = gnsdk_musicidstream_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_MUSICIDSTREAM;
		}
		break;
#endif

#if GNSDK_LINK
	case GNSDK_COMPONENT_LINK:
		if(!( mModulesInit & GNSDK_COMPONENT_LINK ) )
		{
			error = gnsdk_link_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_LINK;
		}
		break;
#endif

#if GNSDK_MUSICID_MATCH
	case GNSDK_COMPONENT_MUSICIDMATCH:
		if(!( mModulesInit & GNSDK_COMPONENT_MUSICIDMATCH ) )
		{
			error = gnsdk_musicidmatch_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_MUSICIDMATCH;
		}
		break;
#endif

#if GNSDK_STORAGE_SQLITE
	case GNSDK_COMPONENT_STORAGE_SQLITE:
		if(!( mModulesInit & GNSDK_COMPONENT_STORAGE_SQLITE ) )
		{
			error = gnsdk_storage_sqlite_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_STORAGE_SQLITE;
		}
		break;
#endif

#if GNSDK_LOOKUP_LOCAL
	case GNSDK_COMPONENT_LOOKUP_LOCAL:
		if(!( mModulesInit & GNSDK_COMPONENT_LOOKUP_LOCAL ) )
		{
			error = gnsdk_lookup_local_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_LOOKUP_LOCAL;
		}
		break;
#endif

#if GNSDK_LOOKUP_FPLOCAL
	case GNSDK_COMPONENT_LOOKUP_FPLOCAL:
		if(!( mModulesInit & GNSDK_COMPONENT_LOOKUP_FPLOCAL ) )
		{
			error = gnsdk_lookup_fplocal_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_LOOKUP_FPLOCAL;
		}
		break;
#endif

#if GNSDK_SUBMIT
	case GNSDK_COMPONENT_SUBMIT:
		if(!( mModulesInit & GNSDK_COMPONENT_SUBMIT ) )
		{
			error = gnsdk_submit_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_SUBMIT;
		}
		break;
#endif

#if GNSDK_VIDEO
	case GNSDK_COMPONENT_VIDEO:
		if(!( mModulesInit & GNSDK_COMPONENT_VIDEO ) )
		{
			error = gnsdk_video_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_VIDEO;
		}
		break;
#endif

#if GNSDK_PLAYLIST
	case GNSDK_COMPONENT_PLAYLIST:
		if(!( mModulesInit & GNSDK_COMPONENT_PLAYLIST ) )
		{
			error = gnsdk_playlist_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_PLAYLIST;
		}
		break;
#endif

#if GNSDK_MOODGRID
	case GNSDK_COMPONENT_MOODGRID:
		if(!( mModulesInit & GNSDK_COMPONENT_MOODGRID ) )
		{
			error = gnsdk_moodgrid_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_MOODGRID;
		}
		break;
#endif

#if GNSDK_ACR
	case GNSDK_COMPONENT_ACR:
		if(!( mModulesInit & GNSDK_COMPONENT_ACR ) )
		{
			error = gnsdk_acr_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_ACR;
		}
		break;
#endif

#if GNSDK_CORRELATES
	case GNSDK_COMPONENT_CORRELATES:
		if(!( mModulesInit & GNSDK_COMPONENT_CORRELATES ) )
		{
			error = gnsdk_correlates_initialize(mManagerHandle);
			if(error) { throw GnError(); }

			mModulesInit |= GNSDK_COMPONENT_CORRELATES;
		}
		break;
#endif
#if GNSDK_EPG
	case GNSDK_COMPONENT_EPG:
		if(!( mModulesInit & GNSDK_COMPONENT_EPG ) )
		{
			error = gnsdk_epg_initialize(mManagerHandle);
			if(error) { throw GnError(); }
			mModulesInit |= GNSDK_COMPONENT_EPG;
		}
		break;
#endif
	default:
		break;
	}
}

/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t GnSDK::Version()
{
	return gnsdk_manager_get_version();
}

/*-----------------------------------------------------------------------------
 *  ProductVersion
 */
gnsdk_cstr_t GnSDK::ProductVersion()
{
	return gnsdk_manager_get_product_version();
}

/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t GnSDK::BuildDate()
{
	return gnsdk_manager_get_build_date();
}

/*-----------------------------------------------------------------------------
 *  RegisterUser
 */
GnString GnSDK::RegisterUser(GnUserRegisterMode registerMode, gnsdk_cstr_t clientId, gnsdk_cstr_t clientTag, gnsdk_cstr_t applicationVersion)
throw ( GnError )
{
	gnsdk_str_t   serialized_user = GNSDK_NULL;
	gnsdk_error_t error           = SDKMGRERR_InvalidArg;

	switch(registerMode)
	{
	case kUserRegModeOnline:
		error = gnsdk_manager_user_register(GNSDK_USER_REGISTER_MODE_ONLINE, clientId, clientTag, applicationVersion, &serialized_user);
		break;

	case kUserRegModeLocalOnly:
		error = gnsdk_manager_user_register(GNSDK_USER_REGISTER_MODE_LOCALONLY, clientId, clientTag, applicationVersion, &serialized_user);
		break;

	default:
		break;
	}

	if(error) { throw GnError(); }

	GnString result(serialized_user);

	error = gnsdk_manager_string_free(serialized_user);

	if(error) { throw GnError(); }

	return result;
}

/*-----------------------------------------------------------------------------
 *  SetDefaultLocale
 */
void GnSDK::SetDefaultLocale(GnLocale& locale)
{
	gnsdk_error_t error;

	error = gnsdk_manager_locale_set_group_default(locale.native() );
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  LoggingEnable
 */
void GnSDK::LoggingEnable(gnsdk_cstr_t logFilePath, gnsdk_uint16_t packageId, gnsdk_uint32_t filterMask, gnsdk_uint32_t optionsMask, gnsdk_uint64_t maxSize, bool b_archive)
throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_logging_enable(logFilePath, packageId, filterMask, optionsMask, maxSize, b_archive);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  LoggingDisable
 */
void GnSDK::LoggingDisable(gnsdk_cstr_t logFilePath, gnsdk_uint16_t packageId)
throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_logging_disable(logFilePath,
		packageId);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  LoggingWrite
 */
void GnSDK::LoggingWrite(gnsdk_int32_t line, gnsdk_cstr_t fileName, gnsdk_uint16_t packageId, gnsdk_uint32_t filterMask, gnsdk_cstr_t format, ...)
throw ( GnError )
{
	gnsdk_error_t error;
	va_list       argptr;

	va_start(argptr, format);
	error = gnsdk_manager_logging_vwrite(line, fileName, packageId, filterMask, format, argptr);
	va_end(argptr);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  StorageLocation
 */
void GnSDK::StorageLocation(gnsdk_cstr_t name, gnsdk_cstr_t location) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_manager_storage_location_set(name, location);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  StorageCleanup
 */
void GnSDK::StorageCleanup(gnsdk_cstr_t name, bool bAsync) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_bool_t  _bAsync = GNSDK_FALSE;

	if(bAsync) { _bAsync = GNSDK_TRUE; }

	error = gnsdk_manager_storage_cleanup(name, _bAsync);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  StorageCompact
 */
void GnSDK::StorageCompact(gnsdk_cstr_t name, bool bAsync) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_bool_t  _bAsync = GNSDK_FALSE;

	if(bAsync) { _bAsync = GNSDK_TRUE; }

	error = gnsdk_manager_storage_compact(name, _bAsync);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  StorageFlush
 */
void GnSDK::StorageFlush(gnsdk_cstr_t name, bool bAsync) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_bool_t  _bAsync = GNSDK_FALSE;

	if(bAsync) { _bAsync = GNSDK_TRUE; }

	error = gnsdk_manager_storage_flush(name, _bAsync);
	if(error) { throw GnError(); }
}

/******************************************************************************
** GnUser
*/
GnUser::GnUser(gnsdk_cstr_t serializedUser, gnsdk_cstr_t clientId, gnsdk_cstr_t clientTag, gnsdk_cstr_t applicationVersion) throw ( GnError ) :
	m_user_handle(0)
{
	gnsdk_error_t error;

	error = gnsdk_manager_user_create(serializedUser, clientId, clientTag, applicationVersion, &m_user_handle);
	if(error) { throw GnError(); }
}

GnUser::~GnUser()
{
	gnsdk_manager_user_release(m_user_handle);
}

/*-----------------------------------------------------------------------------
 *  OptionLookupMode
 */
void GnUser::OptionLookupMode(GnLookupMode lookupMode) throw ( GnError )
{
	gnsdk_error_t error;

	switch(lookupMode)
	{
	case kLookupModeLocal:
		error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_LOCAL);
		break;

	case kLookupModeOnline:
		error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE);
		break;

	case kLookupModeOnlineNoCache:
		error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHE);
		break;

	case kLookupModeOnlineNoCacheRead:
		error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHEREAD);
		break;

	case kLookupModeOnlineCacheOnly:
		error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_CACHEONLY);
		break;

	default:
		return;
	}

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionNetworkProxy
 */
void GnUser::OptionNetworkProxy(gnsdk_cstr_t hostName, gnsdk_cstr_t userName, gnsdk_cstr_t password) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_PROXY_HOST, hostName);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_PROXY_PASS, password);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_PROXY_USER, userName);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionNetworkTimeout
 */
void GnUser::OptionNetworkTimeout(gnsdk_cstr_t timeoutMs) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_NETWORK_TIMEOUT, timeoutMs);

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionNetworkLoadBalance
 */
void GnUser::OptionNetworkLoadBalance(bool bEnable) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_NETWORK_LOADBALANCE, bEnable ? "Y" : "N");

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionUserInfo
 */
void GnUser::OptionUserInfo(gnsdk_cstr_t locationId, gnsdk_cstr_t mfg, gnsdk_cstr_t os, gnsdk_cstr_t uId) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_LOCATION_ID, locationId);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_INFO_MFG, mfg);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_INFO_OS, os);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_INFO_UID, uId);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionCacheExpiration
 */
void GnUser::OptionCacheExpiration(gnsdk_cstr_t duration_sec) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_user_option_set(m_user_handle, GNSDK_USER_OPTION_CACHE_EXPIRATION, duration_sec);

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionServiceUrl
 */
void GnUser::OptionServiceUrl(gnsdk_cstr_t baseURL) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_manager_user_option_set(m_user_handle, "gnsdk_service_url", baseURL);

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionSet
 */
void GnUser::OptionSet(gnsdk_cstr_t key, gnsdk_cstr_t value)
{
	gnsdk_error_t error = gnsdk_manager_user_option_set(m_user_handle, key, value);

	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionGet
 */
gnsdk_cstr_t GnUser::OptionGet(gnsdk_cstr_t opetion_key) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  opetionValue = GNSDK_NULL;

	error = gnsdk_manager_user_option_get(m_user_handle, opetion_key, &opetionValue);
	if(error) { throw GnError(); }

	return opetionValue;
}

/******************************************************************************
** GnError
*/
GnError::GnError()
{
	const gnsdk_error_info_t* pErrorInfo = gnsdk_manager_error_info();

	_copy_errorinfo(pErrorInfo);
}

GnError::GnError(const gnsdk_error_info_t* pErrorInfo)
{
	_copy_errorinfo(pErrorInfo);
}

/*-----------------------------------------------------------------------------
 *  _copy_errorinfo
 */
void GnError::_copy_errorinfo(const gnsdk_error_info_t* pErrorInfo)
{
	char*        ptr;
	gnsdk_size_t count;

	/* pack strings into _copy_buf:
	** [ gnsdk_error_info_t, error_api, 0, error_description, 0, error_module, 0, source_error_module, 0 ]
	*/
	m_pErrorInfo = (gnsdk_error_info_t*)_copy_buf;

	/* numeric values */
	m_pErrorInfo->error_code        = pErrorInfo->error_code;
	m_pErrorInfo->source_error_code = pErrorInfo->source_error_code;

	/* copy error_api */
	ptr                     = (char*)m_pErrorInfo + sizeof( gnsdk_error_info_t );
	count                   = sizeof( _copy_buf ) - sizeof( gnsdk_error_info_t );
	count                   = gnstd::gn_strcpy(ptr, count, pErrorInfo->error_api);
	ptr[count]              = 0;
	m_pErrorInfo->error_api = ptr;

	/* copy error_description */
	ptr                            += count + 1;
	count                           = sizeof( _copy_buf ) - sizeof( gnsdk_error_info_t ) - count - 2;
	count                           = gnstd::gn_strcpy(ptr, count, pErrorInfo->error_description);
	ptr[count]                      = 0;
	m_pErrorInfo->error_description = ptr;

	/* copy error_module */
	ptr                       += count + 1;
	count                      = sizeof( _copy_buf ) - sizeof( gnsdk_error_info_t ) - count - 2;
	count                      = gnstd::gn_strcpy(ptr, count, pErrorInfo->error_module);
	ptr[count]                 = 0;
	m_pErrorInfo->error_module = ptr;

	/* copy source_error_module */
	ptr                              += count + 1;
	count                             = sizeof( _copy_buf ) - sizeof( gnsdk_error_info_t ) - count - 2;
	count                             = gnstd::gn_strcpy(ptr, count, pErrorInfo->source_error_module);
	ptr[count]                        = 0;
	m_pErrorInfo->source_error_module = ptr;
}

/*-----------------------------------------------------------------------------
 *  ErrorAPI
 */
gnsdk_cstr_t GnError::ErrorAPI()
{
	return m_pErrorInfo->error_api;
}

/*-----------------------------------------------------------------------------
 *  ErrorCode
 */
gnsdk_error_t GnError::ErrorCode()
{
	return m_pErrorInfo->error_code;
}

/*-----------------------------------------------------------------------------
 *  ErrorDescription
 */
gnsdk_cstr_t GnError::ErrorDescription()
{
	return m_pErrorInfo->error_description;
}

/*-----------------------------------------------------------------------------
 *  ErrorModule
 */
gnsdk_cstr_t GnError::ErrorModule()
{
	return m_pErrorInfo->error_module;
}

/*-----------------------------------------------------------------------------
 *  SourceErrorCode
 */
gnsdk_error_t GnError::SourceErrorCode()
{
	return m_pErrorInfo->source_error_code;
}

/*-----------------------------------------------------------------------------
 *  SourceErrorModule
 */
gnsdk_cstr_t GnError::SourceErrorModule()
{
	return m_pErrorInfo->source_error_module;
}

/******************************************************************************
** GnLocale
*/
GnLocale::GnLocale(gnsdk_cstr_t group, gnsdk_cstr_t language, gnsdk_cstr_t region, gnsdk_cstr_t descriptor, const GnUser& user, GnStatusEvents* locale_events) throw ( GnError )
	: pEventHandler_(GNSDK_NULL), localeHandle_(GNSDK_NULL)
{
	gnsdk_error_t error;

	if(locale_events)
	{
		pEventHandler_ = locale_events;
	}

	error = gnsdk_manager_locale_load(
		group,
		language,
		region,
		descriptor,
		user.native(),
		_locale_status_callback,
		this,
		&localeHandle_);
	if(error) { throw GnError(); }
}

GnLocale::GnLocale(gnsdk_cstr_t serializedLocale, GnStatusEvents* status_events) throw ( GnError )
	: pEventHandler_(status_events), localeHandle_(GNSDK_NULL)
{
	gnsdk_error_t error;

	error = gnsdk_manager_locale_deserialize(serializedLocale, &localeHandle_);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  Group
 */
gnsdk_cstr_t GnLocale::Group() const throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  group = GNSDK_NULL;

	error = gnsdk_manager_locale_info(this->localeHandle_, &group, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL);
	if(error) { throw GnError(); }
	return group;
}

/*-----------------------------------------------------------------------------
 *  Language
 */
gnsdk_cstr_t GnLocale::Language() const throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_locale_info(this->localeHandle_, GNSDK_NULL, &value, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL);
	if(error) { throw GnError(); }
	return value;
}

/*-----------------------------------------------------------------------------
 *  Region
 */
gnsdk_cstr_t GnLocale::Region() const throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_locale_info(this->localeHandle_, GNSDK_NULL, GNSDK_NULL, &value, GNSDK_NULL, GNSDK_NULL);
	if(error) { throw GnError(); }
	return value;
}

/*-----------------------------------------------------------------------------
 *  Descriptor
 */
gnsdk_cstr_t GnLocale::Descriptor() const throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_locale_info(this->localeHandle_, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL, &value, GNSDK_NULL);
	if(error) { throw GnError(); }
	return value;
}

/*-----------------------------------------------------------------------------
 *  Revision
 */
gnsdk_cstr_t GnLocale::Revision() const throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_locale_info(this->localeHandle_, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL, &value);
	if(error) { throw GnError(); }
	return value;
}

/*-----------------------------------------------------------------------------
 *  Update
 */
bool GnLocale::Update(const GnUser& user) throw ( GnError )
{
	gnsdk_bool_t  b_updated = GNSDK_FALSE;
	gnsdk_error_t error;

	error = gnsdk_manager_locale_update(localeHandle_, user.native(), _locale_status_callback, this, &b_updated);
	if(error) { throw GnError(); }

	if(b_updated)
	{
		return true;
	}
	return false;
}

/*-----------------------------------------------------------------------------
 *  UpdateCheck
 */
bool GnLocale::UpdateCheck(const GnUser& user) throw ( GnError )
{
	gnsdk_bool_t  b_new_revision_available = GNSDK_FALSE;
	gnsdk_error_t error;

	error = gnsdk_manager_locale_update_check(localeHandle_, user.native(), _locale_status_callback, this, &b_new_revision_available);
	if(error) { throw GnError(); }

	if(b_new_revision_available)
	{
		return true;
	}
	return false;
}

/*-----------------------------------------------------------------------------
 *  Serialize
 */
GnString GnLocale::Serialize() const throw ( GnError )
{
	gnsdk_str_t   sz_value = GNSDK_NULL;
	gnsdk_error_t error;

	error = gnsdk_manager_locale_serialize(localeHandle_, &sz_value);
	if(error) { throw GnError(); }

	GnString retval(sz_value);

	error = gnsdk_manager_string_free(sz_value);

	if(error) { throw GnError(); }

	return retval;
}

/*-----------------------------------------------------------------------------
 *  _locale_status_callback
 */
void GNSDK_CALLBACK_API GnLocale::_locale_status_callback(void* callback_data, gnsdk_status_t mid_status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort)
{
	GnLocale* p_gnlocale = (GnLocale*)callback_data;

	if(p_gnlocale->pEventHandler_)
	{
		p_gnlocale->pEventHandler_->status_event(mid_status, percent_complete, bytes_total_sent, bytes_total_received);
		if(p_gnlocale->pEventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

GnLocale::~GnLocale()
{
	if(localeHandle_)
	{
		gnsdk_manager_locale_release(localeHandle_);
	}
}
