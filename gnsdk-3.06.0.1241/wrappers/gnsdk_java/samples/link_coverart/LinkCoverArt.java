/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *	Name: LinkCoverArt
 *	Description:
 *  Retrieves a coverart image using Link starting with a serialized GnDataObject as source.
 *
 *  Command-line Syntax:
 *   clientid  , clientid_tag , license path and lib path
 *
 */
package link_coverart;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class LinkCoverArt {
	/* Set USELOCAL to false to have the sample perform online queries.
	 * Set USELOCAL to true to have the sample perform local queries.
	 * Note: For local queries, Gracenote database is required.
	 */
	private static final boolean USELOCAL = false;

	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		
		if (file.isFile()) {
			return true;
		}
		return false;
	}
	
	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("Gracenote DB Version : " + versionResult);
	}
	
	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}
	
	private static void fetchImage(byte[] coverArt, String imageTypeStr) {
		if (coverArt != null) {
			System.out.println("\nRETRIEVED: "+ imageTypeStr +" image: "+ coverArt.length +" byte JPEG");
		} else {
			System.out.println("\nNOT FOUND: "+ imageTypeStr +" image ");
		}
	}
	
	private static void queryForAlbumImages(GnUser gnUser) throws InvalidObjectException, GnException {

		String serializedGdo = "WEcxAbwX1+DYDXSI3nZZ/L9ntBr8EhRjYAYzNEwlFNYCWkbGGLvyitwgmBccgJtgIM/dkcbDgrOqBMIQJZMmvysjCkx10ppXc68ZcgU0SgLelyjfo1Tt7Ix/cn32BvcbeuPkAk0WwwReVdcSLuO8cYxAGcGQrEE+4s2H75HwxFG28r/yb2QX71pR";		

		System.out.println("\n*****Sample Link Album Query*****");
		
		/* Typically, the GDO passed in to a Link query will come from the output of a GNSDK query.
		 * For an example of how to perform a query and get a GDO please refer to the documentation
		 * or other sample applications.
		 * The below serialized GDO was an 1-track album result from another GNSDK query.
		 */
		GnDataObject gnDataObject = new GnDataObject(serializedGdo);
		
		
		GnMusicID gnMusicID = new GnMusicID(gnUser);
		
		GnResponseAlbums responseAlbums = gnMusicID.findAlbums(gnDataObject);
		GnAlbum gnAlbum = responseAlbums.albums().getIterator().next();
		
		GnLink link = new GnLink(gnUser, gnAlbum, null);
		
		if (link != null) {
			byte userData[] = null;
			byte[] imageArtist = null;
			
			/* 
			 * Get cover art
			 */
			GnLinkContent coverArt = link.coverArt(GnImageSize.size_170, GnImagePreference.smallest);
			
			userData = new byte[(int) coverArt.dataSize()];
			coverArt.dataBuffer(userData);
			
			fetchImage(userData, "cover art");
						
			/* 
			 * Get artist image
			 */
			GnLinkContent artistImage = link.artistImage(GnImageSize.size_170, GnImagePreference.smallest);
			
			imageArtist = new byte[(int) artistImage.dataSize()];
			artistImage.dataBuffer(imageArtist);
			
			fetchImage(imageArtist, "artist");
		}
	}
	
	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				if (USELOCAL) {
					GnStorageSqlite sqliteStorage = new GnStorageSqlite();
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				loadLocale(gnsdk, user);

				queryForAlbumImages(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
