package com.gracenote.gnsdk;

import com.gracenote.gnsdk.GnSDK;

public class GnLocaleDetect{

	/**
	 * @return Gracenote region as mapped from the device locale information
	 */	
	public static String getLocaleRegion(GnAppContext context){
		return GnSDK.getKRegionGlobal();
	}
	
	//todo: remove?
	public static String getLocaleCountry(GnAppContext context){
		return "usa";
	}
	
	/**
	 * 
	 * @return Gracenote language as mapped from the device locale information
	 */
	public static String getLocaleLanguage(GnAppContext context){
		return "eng";
	}
}
