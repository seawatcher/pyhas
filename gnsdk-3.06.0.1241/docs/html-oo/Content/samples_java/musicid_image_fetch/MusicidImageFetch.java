package musicid_image_fetch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.gracenote.gnsdk.*;

public class MusicidImageFetch {

	private class Mp3Sound {
		private String albumTitleTag;
		private String artistNameTag;
		private String genretag;

		public Mp3Sound() {
		}

		public String getAlbumTitleTag() {
			return albumTitleTag;
		}

		public void setAlbumTitleTag(String albumTitleTag) {
			this.albumTitleTag = albumTitleTag;
		}

		public String getArtistNameTag() {
			return artistNameTag;
		}

		public void setArtistNameTag(String artistNameTag) {
			this.artistNameTag = artistNameTag;
		}

		public String getGenretag() {
			return genretag;
		}

		public void setGenretag(String genretag) {
			this.genretag = genretag;
		}

	}

	/*
	 * Online vs Local queries Set USELOCAL to false to have the sample perform
	 * online queries. Set USELOCAL to true to have the sample perform local
	 * queries. For local queries, a Gracenote local database must be present.
	 */
	private static final boolean USELOCAL = false;
	private static GnLookupLocal gnLookupLocal;
	private static GnUser user = null;
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}
	private static List<Mp3Sound> mp3SoundsList = new ArrayList<Mp3Sound>();

	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out
					.println("usage : clientid  , clientid_tag , license path and lib path  ");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		boolean isFile = checkFilePath(licensePath);

		if (!isFile) {
			System.out.println("Licence file not found");
			System.exit(0);
		}
		try {
			initializeGNSDK(licensePath, libPath, clientIdTag, clientId);
			MusicidImageFetch musicidImageFetch = new MusicidImageFetch();
			musicidImageFetch.createList();
			fetchImage();

		} catch (GnException gnException) {
			gnException.printStackTrace();
			System.out.println("GnException \t" + gnException.getMessage());
		} finally {
			/***************************************************************************
			 * 
			 * finally Block
			 * 
			 * Call shutdown on all initialized GNSDK modules. Release all
			 * existing handles before shutting down any of the modules.
			 * Shutting down the Manager module should occur last, but the
			 * shutdown ordering of all other modules does not matter.
			 * 
			 ***************************************************************************/
			System.runFinalization();
			System.gc();
		}
	}

	private void createList() {
		Mp3Sound mp3Sound1 = new Mp3Sound();

		mp3Sound1.setAlbumTitleTag("Ask Me No Questions");
		mp3Sound1.setArtistNameTag("Low Commotion Blues Band");
		mp3Sound1.setGenretag("Rock");

		mp3SoundsList.add(mp3Sound1);

		Mp3Sound mp3Sound2 = new Mp3Sound();

		mp3Sound2.setAlbumTitleTag("Supernatural");
		mp3Sound2.setArtistNameTag("Santana");

		mp3SoundsList.add(mp3Sound2);

		Mp3Sound mp3Sound3 = new Mp3Sound();

		mp3Sound3.setArtistNameTag("Phillip Glass");

		mp3SoundsList.add(mp3Sound3);

	}

	/***************************************************************************
	 * 
	 * _PROCESS_SAMPLE_MP3
	 * 
	 ***************************************************************************/
	private static void fetchImage() throws GnException, InvalidObjectException {
		GnList list = new GnList(GnSDK.getKListTypeGenres(),
				GnSDK.getKLanguageEnglish(), GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), user);
		for (int fileIndex = 0; fileIndex < mp3SoundsList.size(); fileIndex++) {
			System.out
					.println("\n\n***** Processing File " + fileIndex + " *****" );

			Mp3Sound mp3Sound = mp3SoundsList.get(fileIndex);
			/* Do a music text query and fetch image from result. */
			processSampleMp3(mp3Sound.getAlbumTitleTag(),
					mp3Sound.getArtistNameTag(), mp3Sound.getGenretag(), list);
		}

	}

	private static void processSampleMp3(String albumTitleTag,
			String artistNameTag, String genreTag, GnList list)
			throws GnException, InvalidObjectException {
		int gotMatch = 0;
		/* Do a music text query and fetch image from result. */
		gotMatch = dosampleTextQuery(albumTitleTag, artistNameTag, genreTag);
		/*
		 * If there were no results from the musicid query for this file, try
		 * looking up the genre tag to get the genre image.
		 */
		if (0 == gotMatch) {
			if (null != genreTag) {
				gotMatch = findGenreImage(genreTag, list);
			}
		}

	}

	/***************************************************************************
	 * 
	 * FindGenreImage
	 * 
	 * This function performs a text lookup of a genre string. If there is a
	 * match, the genre image is fetched.
	 * 
	 * @throws GnException
	 * 
	 ***************************************************************************/
	private static int findGenreImage(String genre, GnList list)throws GnException {
		int gotMatch = 0;
		String availableImageSize;
		GnImageSize imageSize;
		GnLink link = null;

		/*System.out.println("Genre String Search");*/

		if (genre == null) {
			System.out.println("Must pass a genre");
		}

		/* Find the list element for our input string */
		GnListElement listElement = doListSearch(genre, list);

		if (listElement != null) {
			/* we were able to idenfity the input genre string */
			gotMatch = 1;

			/*
			 * Create the query handle without a callback or callback data
			 * (GNSDK_NULL)
			 */
			link = new GnLink(user, listElement);
		}

		/* Set preferred image size */
		GnImageSize preferredImageSize = GnImageSize.size_170;

		/* Obtain image size available */
		if (USELOCAL) {
			long count = gnLookupLocal.storageInfoCount(
					GnLocalStorageName.kContent,
					GnLocalStorageInfoKey.kImageSize);

			for (long ordinal = 0; ordinal < count; ordinal++) {
				availableImageSize = gnLookupLocal.storageInfo(
						GnLocalStorageName.kContent,
						GnLocalStorageInfoKey.kImageSize, ordinal);
			}
		} else
			imageSize = preferredImageSize;

		boolean notFound = fetchImage(link, imageSize, "genre image");

		return gotMatch;
	}

	/***************************************************************************
	 * 
	 * DoListSearch
	 * 
	 * @throws GnException
	 * 
	 ***************************************************************************/
	static GnListElement doListSearch(String input, GnList list)
			throws GnException {
		GnListElement listElement = list.elementByString(input);

		long level = listElement.level();
		String value = listElement.displayString();
		
		return listElement;
	}

	/***************************************************************************
	 * 
	 * _DO_SAMPLE_TEXT_QUERY
	 * 
	 * This function performs a text lookup using musicid. If there is a match,
	 * the associated image is fetched.
	 * 
	 ***************************************************************************/
	private static int dosampleTextQuery(String albumTitleTag,
			String artistNameTag, String genreTag) throws GnException,
			InvalidObjectException {
		int gotMatch = 0;
		String gdoType = null;

		System.out.println("MusicID Text Match Query");

		if ((albumTitleTag == null) && (artistNameTag == null)) {
			System.out.println("Must pass album title or artist name");

		}

		if (albumTitleTag == null)
			albumTitleTag = "";
		else
			System.out.println("album title    : " + albumTitleTag); /*
																 * show the
																 * fields that
																 * we have set
																 */

		if (artistNameTag == null)
			artistNameTag = "";
		else
			System.out.println("artist name    : " + artistNameTag);

		GnMusicID musicID = new GnMusicID(user);
		GnResponseAlbums responseAlbums = musicID.findAlbums(albumTitleTag, "",
				artistNameTag);

		long count = responseAlbums.albums().count();

		System.out.println("Number matches = " + count);

		/* Get the first match type */
		if (count > 0) {
			/* OK, we got at least one match. */
			gotMatch = 1;

			/* Just use the first match for demonstration. */
			GnAlbum album = responseAlbums.albums().at(0).next();

			gdoType = album.getType();

			System.out.println("\nFirst Match GDO type: " + gdoType );

			/* Get the best image for the match */
			performImageFetch(album, gdoType);
		}

		return gotMatch;

	}

	/***************************************************************************
	 * 
	 * PerformImageFetch
	 * 
	 * This function performs a text lookup and image fetch
	 * 
	 * @throws GnException
	 * 
	 ***************************************************************************/
	private static void performImageFetch(GnAlbum album, String gdoType)
			throws GnException {
		GnImageSize imageSize;
		boolean notFound = false;
		String availableImageSize = null;
		if ((album == null) || (gdoType == null)) {
			System.out.println("Must pass a GDO and type");
			return;
		}

		/*
		 * Create the query handle without a callback or callback data
		 * (GNSDK_NULL)
		 */
		GnLink link = new GnLink(user, album);

		/* Set preferred image size */
		GnImageSize preferredImageSize = GnImageSize.size_170;

		/* Obtain image size available */
		if (USELOCAL) {
			long count = gnLookupLocal.storageInfoCount(
					GnLocalStorageName.kContent,
					GnLocalStorageInfoKey.kImageSize);

			for (long ordinal = 0; ordinal < count; ordinal++) {
				availableImageSize = gnLookupLocal.storageInfo(
						GnLocalStorageName.kContent,
						GnLocalStorageInfoKey.kImageSize, ordinal);
			}
		} else {
			imageSize = preferredImageSize;
		}
		/* If album type get cover art */
		notFound = fetchImage(link, imageSize, "cover art");

		/* if no cover art, try to get the album's artist image */
		if (notFound == true) {
			notFound = fetchImage(link, imageSize, "artist image");

			/*
			 * if no artist image, try to get the album's genre image so we have
			 * something to display
			 */
			if (notFound == true)
				notFound = fetchImage(link, imageSize, "genre image");
		}

	}

	/***************************************************************************
	 * 
	 * FetchImage
	 * 
	 * @throws GnException
	 * 
	 ***************************************************************************/
	private static boolean fetchImage(GnLink link, GnImageSize imageSize,
			String imageType) {
		GnLinkContent linkContent = null;
		boolean notFound = false;

		/* Perform the image fetch */
		try {
			if (imageType.equals("cover art")) {
				linkContent = link.coverArt(imageSize, GnImagePreference.exact);
			}
			else if (imageType.equals("artist image")) {
				linkContent = link.artistImage(imageSize,
						GnImagePreference.exact);
			}
			else if (imageType.equals("genre image")) {
				linkContent = link.genreArt(imageSize, GnImagePreference.exact);
			}

			System.out.println("\nRETRIEVED: " + imageType+ ": " + linkContent.dataSize() + " byte JPEG");

			notFound = false;

		} catch (GnException gn) {
			// Do not return error code for not found.
			// For image to be fetched, it must exist in the size specified and
			// you must be entitled to fetch images.
			System.out.println("NOT FOUND: " + imageType );
			notFound = true;
		}
		return notFound;
	}

	/****************************************************************************************
	 * 
	 * _INIT_GNSDK
	 * 
	 * Initializing the GNSDK is required before any other APIs can be called.
	 * First step is to always initialize the Manager module, then use the
	 * returned handle to initialize any modules to be used by the application.
	 * 
	 * For this sample, we also load a locale which is used by GNSDK to provide
	 * appropriate locale-sensitive metadata for certain metadata values.
	 * Loading of the locale is done here for sample convenience but can be done
	 * at anytime in your application.
	 * 
	 ****************************************************************************************/
	private static void initializeGNSDK(final String licensePath,
			final String libPath, final String clientIdTag,
			final String clientId) throws GnException, IOException {
		/* GNSDK initialization */
		GnSDK gnsdk = new GnSDK(libPath, licensePath,
				GnSDK.GnLicenseInputMode.kFilename);

		final String applicationVersion = "1.0.0.0";
		System.out.println("\nGNSDK Product Version    : "
				+ gnsdk.productVersion() + " \t(built " + gnsdk.buildDate() + ")\n");
		if (null == readFileAsString("user.txt")) {
			System.out.println("Info: No stored user - this must be the app's first run.");

			GnString user_string = gnsdk.registerUser(
									GnSDK.GnUserRegisterMode.kUserRegModeOnline, 
									clientId, 
									clientIdTag, 
									applicationVersion);
			// save user serialized string for reusing the user later
			writeFileAsString("user.txt", user_string.toString());
		} else {
			// recreate user from serialized user string
			user = new GnUser( 
						readFileAsString("user.txt"),
						clientId, 
						clientIdTag, 
						applicationVersion);
		}

		logginEnable(gnsdk);
		
		GnStorageSqlite sqliteStorage = new GnStorageSqlite();
		if (USELOCAL) {
			sqliteStorage.storageFolderSet("../../../sample_db");
			gnLookupLocal = new GnLookupLocal();
			user.optionLookupMode(GnLookupMode.kLookupModeLocal);
			displayEmbeddedDbInfo(gnLookupLocal);
		} else {
			user.optionLookupMode(GnLookupMode.kLookupModeOnline);
		}

		loadLocale(user);

	}

	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
			throws GnException {

		long ordinal = gnLookupLocal
				.storageInfoCount(GnLocalStorageName.kMetadata,
						GnLocalStorageInfoKey.kGDBVersion);
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, ordinal);
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}

	private static void logginEnable(GnSDK gnsdk) throws GnException {

		gnsdk.loggingEnable("sample.log", /* Log file path */
				GnSDK.getGN_LOG_PKG_ALL(), /*
											 * Include entries for all packages
											 * and subsystems
											 */
				GnSDK.getGN_LOG_LEVEL_ERROR(), /*
												 * Include only error and
												 * warning entries
												 */
				GnSDK.getGN_LOG_OPTION_ALL(), /*
											 * All logging options: timestamps,
											 * thread IDs, etc
											 */
				new BigInteger("0"), /*
									 * Max size of log: 0 means a new log file
									 * will be created each run
									 */
				false); /* TRUE = old logs will be renamed and saved */
	}

	private static void loadLocale(GnUser user) throws GnException, IOException {

		/*LocaleEvent localeEvents = new LocaleEvent();*/
		File file = new File("serialized_locale.txt");

		if (USELOCAL) {
			GnLocale locale = new GnLocale(
								GnSDK.getKLocaleGroupMusic(),
								GnSDK.getKLanguageEnglish(), 
								GnSDK.getKRegionGlobal(),
								GnSDK.getKDescriptorDefault(), 
								user, 
								null);/*localeEvents can be used at last parameter to get progress status*/
		} else {
			if (!file.exists()) {
				/*
				 * Set locale with desired Group, Language, Region and
				 * Descriptor Set the 'locale' to return locale-specifc results
				 * values. This examples loads an English locale.
				 */
				GnLocale locale = new GnLocale(
									GnSDK.getKLocaleGroupMusic(),
									GnSDK.getKLanguageEnglish(), 
									GnSDK.getKRegionGlobal(),
									GnSDK.getKDescriptorDefault(), 
									user, 
									null);
				// Serialize locale, so we can use reuse it
				if (!file.exists()) {
					String serializedUserString = (locale.serialize()
							.toString());
					FileWriter printWriter = new FileWriter(
							"serialized_locale.txt");

					printWriter.write(serializedUserString);
					printWriter.close();
				}

			} else {
				String serializedLocaleString = null;
				FileReader fileReader = new FileReader("serialized_locale.txt");
				BufferedReader br = new BufferedReader(fileReader);
				String s;
				while ((s = br.readLine()) != null) {
					serializedLocaleString = s;
				}
				fileReader.close();

				GnLocale locale = new GnLocale( serializedLocaleString );

			}
		}
	}

	private static void writeFileAsString(String file, String content)
			throws java.io.IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(content);
		} finally {
			writer.close();
		}
	}

	private static String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = null;
		try {
			char[] buf = new char[1024];
			int numRead = 0;
			reader = new BufferedReader(new FileReader(filePath));
			while ((numRead = reader.read(buf)) != -1) {
				fileData.append(buf, 0, numRead);
			}
		} finally {
			if ((reader != null) && (fileData.length() != 0)) {
				reader.close();
			} else {
				return null;
			}
		}
		return fileData.toString();
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;

	}

}
