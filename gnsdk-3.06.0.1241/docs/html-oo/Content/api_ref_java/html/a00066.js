var a00066 =
[
    [ "swigValue", "a00066.html#afd8219a8c77963b9ada5b61233bf1702", null ],
    [ "kLookupDataAdditionalCredits", "a00066.html#a056bd42712ae24bdcdabada6b48f18db", null ],
    [ "kLookupDataClassical", "a00066.html#a2c5e34facc0943536bcf9948e968456e", null ],
    [ "kLookupDataContent", "a00066.html#aefafc5f5c672233d6a5d481b1246fe6a", null ],
    [ "kLookupDataExternalIDs", "a00066.html#a22de505f020b3c8f5d1bf5856f5d104e", null ],
    [ "kLookupDataGlobalIDs", "a00066.html#aab619b812f60710bedfc32f1be5eb0e1", null ],
    [ "kLookupDataInvalid", "a00066.html#a26625726a56ec9f90141ea42fd0e2493", null ],
    [ "kLookupDataPlaylist", "a00066.html#ab35566f6d197a06b298a7bec0d6f847b", null ],
    [ "kLookupDataSonicData", "a00066.html#a819db8f2aa039023e3c316df9687f32d", null ]
];