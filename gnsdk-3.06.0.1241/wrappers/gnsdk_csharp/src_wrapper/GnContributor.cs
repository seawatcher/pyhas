/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnContributor : GnDataObject {
  private HandleRef swigCPtr;

  internal GnContributor(IntPtr cPtr, bool cMemoryOwn) : base(gnsdk_csharp_marshalPINVOKE.GnContributor_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnContributor obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnContributor() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnContributor(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GnContributor() : this(gnsdk_csharp_marshalPINVOKE.new_GnContributor__SWIG_0(), true) {
  }

  public GnContributor(string id, string id_tag) : this(gnsdk_csharp_marshalPINVOKE.new_GnContributor__SWIG_1(id, id_tag), true) {
  }

  public bool FullResult() {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnContributor_FullResult(swigCPtr);
    return ret;
  }

  public GnContributor Collaborator() {
    GnContributor ret = new GnContributor(gnsdk_csharp_marshalPINVOKE.GnContributor_Collaborator(swigCPtr), true);
    return ret;
  }

  public bool CollaboratorResult() {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnContributor_CollaboratorResult(swigCPtr);
    return ret;
  }

  public string GnID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_GnID_get(swigCPtr) );
	} 

  }

  public string GnUID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_GnUID_get(swigCPtr) );
	} 

  }

  public string ProductID {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_ProductID_get(swigCPtr) );
	} 

  }

  public string TUI {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_TUI_get(swigCPtr) );
	} 

  }

  public string TUITag {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_TUITag_get(swigCPtr) );
	} 

  }

  public string Biography {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_Biography_get(swigCPtr) );
	} 

  }

  public string BirthDate {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_BirthDate_get(swigCPtr) );
	} 

  }

  public string BirthPlace {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_BirthPlace_get(swigCPtr) );
	} 

  }

  public string DeathDate {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_DeathDate_get(swigCPtr) );
	} 

  }

  public string DeathPlace {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_DeathPlace_get(swigCPtr) );
	} 

  }

  public string MediaSpace {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnContributor_MediaSpace_get(swigCPtr) );
	} 

  }

  public GnNameEnumerable NamesOfficial {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_NamesOfficial_get(swigCPtr);
      GnNameEnumerable ret = (cPtr == IntPtr.Zero) ? null : new GnNameEnumerable(cPtr, true);
      return ret;
    } 
  }

  public GnNameEnumerable NamesRegional {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_NamesRegional_get(swigCPtr);
      GnNameEnumerable ret = (cPtr == IntPtr.Zero) ? null : new GnNameEnumerable(cPtr, true);
      return ret;
    } 
  }

  public GnExternalIDEnumerable ExternalIDs {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_ExternalIDs_get(swigCPtr);
      GnExternalIDEnumerable ret = (cPtr == IntPtr.Zero) ? null : new GnExternalIDEnumerable(cPtr, true);
      return ret;
    } 
  }

  public GnArtistType ArtistType {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_ArtistType_get(swigCPtr);
      GnArtistType ret = (cPtr == IntPtr.Zero) ? null : new GnArtistType(cPtr, true);
      return ret;
    } 
  }

  public GnGenre Genre {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_Genre_get(swigCPtr);
      GnGenre ret = (cPtr == IntPtr.Zero) ? null : new GnGenre(cPtr, true);
      return ret;
    } 
  }

  public GnEra Era {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_Era_get(swigCPtr);
      GnEra ret = (cPtr == IntPtr.Zero) ? null : new GnEra(cPtr, true);
      return ret;
    } 
  }

  public GnOrigin Origin {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnContributor_Origin_get(swigCPtr);
      GnOrigin ret = (cPtr == IntPtr.Zero) ? null : new GnOrigin(cPtr, true);
      return ret;
    } 
  }

}

}
