MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Toc',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<CatapultToc Version=\"1\" DescendantCount=\"37\">' +
	'    <TocEntry Title=\"Documentation Roadmap - Start Here\" Link=\"/Content/shared-topics/doc_roadmaps/DocumentationRoadmapOnline.html\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"true\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"OO Languages Developer\'s Guide\" ComputedFirstTopic=\"false\" DescendantCount=\"31\">' +
	'        <TocEntry Title=\"Concepts\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" StartSection=\"false\" PageNumberReset=\"continue\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"31\">' +
	'            <TocEntry Title=\"Introduction\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                <TocEntry Title=\"About Gracenote\" Link=\"/Content/shared-topics/concepts/About Gracenote.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"What is Gracenote SDK\" Link=\"/Content/shared-topics/concepts/What is Gracenote SDK.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"System Requirements and Performance\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                <TocEntry Title=\"System Requirements\" conditions=\"Default.gnsdko\" Link=\"/Content/shared-topics/concepts/System Requirements for gnsdko.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Gracenote Data\" ComputedFirstTopic=\"false\" DescendantCount=\"9\">' +
	'                <TocEntry Title=\"Gracenote Media Elements\" Link=\"/Content/shared-topics/concepts/Gracenote Media Elements.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Terminology\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                    <TocEntry Title=\"Music Terminology\" Link=\"/Content/shared-topics/concepts/Music Terminology.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Video Terminology\" Link=\"/Content/shared-topics/concepts/Video Terminology.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Gracenote Metadata\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"Core and Enriched Metadata\" Link=\"/Content/shared-topics/concepts/Core and Enriched Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Genre and Other List-Dependent Values\" Link=\"/Content/shared-topics/concepts/Genre and Other List-Dependent Values.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Mood and Tempo (Sonic Attributes)\" Link=\"/Content/shared-topics/concepts/Mood and Tempo Sonic Attributes.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Classical Music Metadata\" Link=\"/Content/shared-topics/concepts/Classical Music Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Third-Party Identifiers and Preferred Partners\" Link=\"/Content/shared-topics/concepts/Third-Party Identifiers and Preferred Partners.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Gracenote Data Objects (GDOs)\" Link=\"/Content/shared-topics/concepts/Gracenote Data Objects GDOs.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"GNSDK Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"19\">' +
	'                <TocEntry Title=\"Modules Overview\" Link=\"/Content/shared-topics/concepts/Modules Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Music Module Overview\" Link=\"/Content/shared-topics/concepts/Music Module Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Video Module Overview\" Link=\"/Content/shared-topics/concepts/Video Module Overview.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Recognition Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"MusicID\" ReplaceMergeNode=\"false\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                        <TocEntry Title=\"MusicID Overview\" Link=\"/Content/shared-topics/concepts/MusicID Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"CD TOC Recognition\" Link=\"/Content/shared-topics/concepts/CD TOC Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Text-Based Recognition\" Link=\"/Content/shared-topics/concepts/Text-Based Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Fingerprint-Based Recognition\" Link=\"/Content/shared-topics/concepts/Fingerprint-Based Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                    <TocEntry Title=\"VideoID Overview\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" Link=\"/Content/shared-topics/concepts/VideoID Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Advanced Recognition and Organization Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                    <TocEntry Title=\"MusicID-File Overview\" Link=\"/Content/shared-topics/concepts/MusicID-File Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"MusicID vs. MusicID-File\" Link=\"/Content/shared-topics/concepts/MusicID vs. MusicID-File.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Enriched Content Module (Link)\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                    <TocEntry Title=\"Link Module Overview\" Link=\"/Content/shared-topics/concepts/Link Module Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Music Enrichment\" Link=\"/Content/shared-topics/concepts/Music Enrichment.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Image Formats and Dimensions\" Link=\"/Content/shared-topics/concepts/Image Formats and Dimensions.html\" StartSection=\"false\" SectionNumberReset=\"continue\" VolumeNumberReset=\"same\" ChapterNumberReset=\"continue\" ComputeToc=\"false\" ReplaceMergeNode=\"false\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Video Enrichment\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                        <TocEntry Title=\"Video Enrichment Overview\" Link=\"/Content/shared-topics/concepts/Video Enrichment Overview.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Discovery Modules\" StartSection=\"false\" PageNumberReset=\"continue\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"Playlists\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                        <TocEntry Title=\"Playlists Overview\" Link=\"/Content/shared-topics/concepts/Playlists Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Playlist Requirements and Recommendations\" Link=\"/Content/shared-topics/concepts/Playlist Requirements.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Key Components\" Link=\"/Content/shared-topics/concepts/Key Playlist Components.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"MoodGrid Overview\" Link=\"/Content/shared-topics/concepts/MoodGrid Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                    <TocEntry Title=\"VideoExplore\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                        <TocEntry Title=\"VideoExplore Overview\" Link=\"/Content/shared-topics/concepts/VideoExplore Overview.html\" conditions=\"Default.DesktopOnly,Default.gnsdko,Default.EntourageOnly\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'        </TocEntry>' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"GNSDK Glossary\" Link=\"/Content/shared-topics/front_back_matter/GlossaryList.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"API Reference Documentation\" Link=\"/Content/shared-topics/api_refs_and_data_model/OO-APIRefsDocsOnline.html\" ComputedFirstTopic=\"false\" DescendantCount=\"3\">' +
	'        <TocEntry Title=\"Java API Reference\" Link=\"/Content/api_ref_java/html/index.html\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"C# API Reference\" Link=\"/Content/api_ref_cs/html/index.html\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"C++ API Reference\" Link=\"/Content/api_ref_cpp/html/index.html\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'</CatapultToc>'
);
