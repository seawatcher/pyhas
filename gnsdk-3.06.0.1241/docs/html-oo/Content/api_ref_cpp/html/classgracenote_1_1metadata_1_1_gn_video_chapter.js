var classgracenote_1_1metadata_1_1_gn_video_chapter =
[
    [ "GnVideoChapter", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#ab8b51b2f298300a00cf199172c29278c", null ],
    [ "GnVideoChapter", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#afbaa402bf52a1cc2afda7058cafb90cf", null ],
    [ "~GnVideoChapter", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#a54a6816894950b0bc4279ac96eb92525", null ],
    [ "Duration", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#adb680711b9c2999c55a1a0d43facb3bb", null ],
    [ "DurationUnits", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#adde813c9034080d171e58fa46fc78be3", null ],
    [ "OfficialTitle", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#a9e00e57a9f825ec4a0b9df5456b1a9bd", null ],
    [ "Ordinal", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#a1845b36f7913869ed584489239349fbc", null ],
    [ "VideoCredits", "classgracenote_1_1metadata_1_1_gn_video_chapter.html#aa61ba84c3f9dd42164dd540e57aea5d0", null ]
];