/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: musicid_list/main.c
 *  Description: Demonstrates accessesing and displaying values from a list
 */

/*
 * Warning
 *
 * The GNSDK package provides three kinds of code examples: sample applications,
 * reference applications, and code snippets.We strongly recommend you use the sample
 * and reference applications as a basis for any applications you develop.
 *
 * The code snippets are located in the /samples/code_snippets folder. They are provided
 * "as is" for documentation purposes only. Their intention is to illustrate a specific
 * function or feature. Although they have been compiled in Windows x64, they lack certain
 * basic functionality and have not been validated for all platforms.
 */

 #define GNSDK_MUSICID               1
 #include "gnsdk.h"

/* Standard C headers - used by the sample app, but not required for GNSDK */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**********************************************
 *    Local Function Declarations
 **********************************************/
static
int _init_gnsdk(
    gnsdk_cstr_t            client_id,
    gnsdk_cstr_t            client_id_tag,
    gnsdk_cstr_t            client_id_app_version,
    gnsdk_cstr_t            license_path,
    gnsdk_user_handle_t*    p_user_handle
    );

static
void _shutdown_gnsdk(
    gnsdk_user_handle_t     user_handle,
    const char*             client_id
    );

static
void _display_list(gnsdk_user_handle_t user_handle);


/******************************************************************
 *
 *    MAIN
 *
 ******************************************************************/
int main(int argc, char* argv[])
{
    gnsdk_user_handle_t     user_handle          = GNSDK_NULL;
    const char*             client_id            = NULL;
    const char*             client_id_tag        = NULL;
    const char*             client_app_version   = "1"; /* Increment with each version of your app */
    const char*             license_path         = NULL;
    int                     rc                   = 0;

    /*    Client ID, Client ID Tag and License file must be passed in */
    if (argc == 4)
    {
        client_id        = argv[1];
        client_id_tag    = argv[2];
        license_path     = argv[3];

        /* Initialize GNSDK     */
        rc = _init_gnsdk(
                client_id,
                client_id_tag,
                client_app_version,
                license_path,
                &user_handle
                );

        if (0 == rc)
        {
            /* Display list values */
            _display_list(user_handle);

            /* Clean up and shutdown */
            _shutdown_gnsdk(user_handle, client_id);
        }
    }
    else
    {
        printf("\nUsage:\n%s clientid clientidtag license\n", argv[0]);
        rc = -1;
    }

    return rc;

}    /* main() */


/******************************************************************
 *
 *    _DISPLAY_LAST_ERROR
 *
 *    Echo the error and information.
 *
 *****************************************************************/
static void
_display_last_error(
	int				line_num
	)
{
	/* Get the last error information from the SDK */
	const gnsdk_error_info_t*       error_info = gnsdk_manager_error_info();

	/* Error_info will never be GNSDK_NULL.
	 * The SDK will always return a pointer to a populated error info structure.
	 */
	printf(
		"\nerror from: %s()  [on line %d]\n\t0x%08x %s",
		error_info->error_api,
		line_num,
		error_info->error_code,
		error_info->error_description
		);

} /* display_last_error() */


/******************************************************************
 *
 *   _ D I S P L A Y _ L I S T
 *
 ******************************************************************/
static void _display_list(gnsdk_user_handle_t user_handle)
{
    gnsdk_error_t               error            = GNSDK_SUCCESS;
    gnsdk_list_element_handle_t element          = GNSDK_NULL;
    gnsdk_cstr_t                display          = GNSDK_NULL;
    gnsdk_uint32_t              id               = 0;
    gnsdk_uint32_t              level_count      = 0;
    gnsdk_uint32_t              element_count    = 0;
    gnsdk_uint32_t              i                = 0;
    gnsdk_uint32_t              j                = 0;
    gnsdk_cstr_t                list_type        = GNSDK_NULL;
    gnsdk_cstr_t                list_language    = GNSDK_NULL;
    gnsdk_cstr_t                list_region      = GNSDK_NULL;
    gnsdk_cstr_t                list_descriptor  = GNSDK_NULL;
    gnsdk_list_handle_t         list_handle      = GNSDK_NULL;


    /************************************************
     * Retrieve music genre list
     ************************************************/
    printf("\nRetrieving music genre list ...\n");
    error =  gnsdk_manager_list_retrieve(
                GNSDK_LIST_TYPE_GENRES,     // list_type
                GNSDK_LANG_ENGLISH,         // list_language
                GNSDK_REGION_DEFAULT,       // list_region
                GNSDK_DESCRIPTOR_DETAILED,  // list_descriptor
                user_handle,                // user handle
                GNSDK_NULL,                 // callback [otional], Refer "callbacks" sample application*
                GNSDK_NULL,                 // [optional] callback user data
                 &list_handle
                );

    if(GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        return;
    }

     error = gnsdk_manager_list_get_type(list_handle, &list_type);
    if (error != GNSDK_SUCCESS)
    {
        _display_last_error(__LINE__);
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_manager_list_get_language(list_handle, &list_language);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_manager_list_get_region(list_handle, &list_region);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_manager_list_get_descriptor(list_handle, &list_descriptor);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_manager_list_get_level_count(list_handle, &level_count);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        printf("\n\n________________________________________________________________\n");
        printf("List:\n\t%s\n\t%s\n\t%s\n\t%s\n\t%d levels\n",list_type, list_region ,list_descriptor,  list_language, level_count);
        printf("________________________________________________________________\n\n" );
        printf("\t\t [list id] - [display string]\n");

        for(i = 1; i <= level_count; i++)
        {
            printf("Level -  %d\n", i);

            error = gnsdk_manager_list_get_element_count(list_handle, 1, &element_count);
            if (error != GNSDK_SUCCESS)
            {
                _display_last_error(__LINE__);
            }

            if (error == GNSDK_SUCCESS)
            {
                for (j = 0; j < element_count; j++)
                {
                    error = gnsdk_manager_list_get_element(list_handle, i, j, &element);
                    if (error != GNSDK_SUCCESS)
                    {
                        _display_last_error(__LINE__);
                        break;
                    }

                    printf("\t\t ");

                    error = gnsdk_manager_list_element_get_id(element, &id);
                    if (error != GNSDK_SUCCESS)
                    {
                        _display_last_error(__LINE__);
                        break;
                    }

                    error = gnsdk_manager_list_element_get_display_string(element, &display);
                    if (error != GNSDK_SUCCESS)
                    {
                        _display_last_error(__LINE__);
                        break;
                    }

                    if (display) printf("[ %d ] - [ %s ]\n",id, display);

                    gnsdk_manager_list_element_release(element);

                } /* for each element in this level */

            } /* error == GNSDK_SUCCESS */

        } /* for each level */

    }  /* if (error == GNSDK_SUCCESS) */

    gnsdk_manager_list_release(list_handle);

    return;

}  /* _display_list() */


/******************************************************************
 *
 *    _DISPLAY_GNSDK_PRODUCT_INFO
 *
 *    Display product version information
 *
 ******************************************************************/
static void _display_gnsdk_product_info(void)
{
    /* Display GNSDK Version infomation */
    printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk_manager_get_product_version(), gnsdk_manager_get_build_date());

}    /* _display_gnsdk_product_info() */


/*****************************************************************************************
 *
 *    _GET_USER_HANDLE
 *
 *    Load existing user handle, or register new one.
 *
 *    GNSDK requires a user handle instance to perform queries.
 *    User handles encapsulate your Gracenote provided Client ID which is unique for your
 *    application. User handles are registered once with Gracenote then must be saved by
 *    your application and reused on future invocations.
 *
 *******************************************************************************************/
static int
_get_user_handle(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_error_t        error               = GNSDK_SUCCESS;
    gnsdk_user_handle_t  user_handle         = GNSDK_NULL;
    char*                user_filename       = NULL;
    size_t               user_filename_len   = 0;
    int                  rc                  = 0;
    FILE*                file                = NULL;


    user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
    user_filename = malloc(user_filename_len);

    if (NULL != user_filename)
    {
        strcpy(user_filename,client_id);
        strcat(user_filename,"_user.txt");

        /* Do we have a user saved locally? */
        file = fopen(user_filename, "r");
        if (NULL != file)
        {
            gnsdk_char_t serialized_user_string[1024] = {0};

            if (NULL != (fgets(serialized_user_string, 1024, file)))
            {
                /* Create the user handle from the saved user */
                error = gnsdk_manager_user_create(serialized_user_string, &user_handle);
                if (GNSDK_SUCCESS != error)
                {
                    _display_last_error(__LINE__);
                    rc = -1;
                }
            }
            else
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
            fclose(file);
        }
        else
        {
            printf("\nInfo: No stored user - this must be the app's first run.\n");
        }

        /* If not, create new one */
        if (GNSDK_NULL == user_handle)
        {
            error = gnsdk_manager_user_create_new(
                        client_id,
                        client_id_tag,
                        client_app_version,
                        &user_handle
                        );
            if (GNSDK_SUCCESS != error)
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
        }

        free(user_filename);
    }
    else
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    if (rc == 0)
    {
        *p_user_handle = user_handle;
    }

    return rc;

} /* _get_user_handle() */


/****************************************************************************************
 *
 *    _INIT_GNSDK
 *
 *     Initializing the GNSDK is required before any other APIs can be called.
 *     First step is to always initialize the Manager module, then use the returned
 *     handle to initialize any modules to be used by the application.
 *
 *     For this sample, we also load a locale which is used by GNSDK to provide
 *     appropriate locale-sensitive metadata for certain metadata values. Loading of the
 *     locale is done here for sample convenience but can be done at anytime in your
 *     application.
 *
 ****************************************************************************************/
static int _init_gnsdk(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    const char*             license_path,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_manager_handle_t   sdkmgr_handle    = GNSDK_NULL;
    gnsdk_error_t            error            = GNSDK_SUCCESS;
    gnsdk_user_handle_t      user_handle      = GNSDK_NULL;
    int                      rc               = 0;

    /* Display GNSDK Product Version Info */
    _display_gnsdk_product_info();

    /* Initialize the GNSDK Manager */
    error = gnsdk_manager_initialize(
                &sdkmgr_handle,
                license_path,
                GNSDK_MANAGER_LICENSEDATA_FILENAME
                );

    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }


    /* Initialize the MusicID SDK */
    error = gnsdk_musicid_initialize(sdkmgr_handle);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    /* Get a user handle for our client ID.  This will be passed in for all queries */
    if (0 == rc)
    {
        rc = _get_user_handle(
                client_id,
                client_id_tag,
                client_app_version,
                &user_handle
                );
    }

    if (0 != rc)
    {
        /* Clean up on failure. */
        _shutdown_gnsdk(user_handle, client_id);
    }
    else
    {
        /* Return the User handle for use at query time */
        *p_user_handle = user_handle;
    }

    return rc;

}    /* _init_gnsdk() */


/***************************************************************************
 *
 *    _SHUTDOWN_GNSDK
 *
 *        Call shutdown on all initialized GNSDK modules.
 *        Release all existing handles before shutting down any of the modules.
 *        Shutting down the Manager module should occur last, but the shutdown
 *        ordering of all other modules does not matter.
 *
 ***************************************************************************/
static void _shutdown_gnsdk(
    gnsdk_user_handle_t        user_handle,
    const char*                client_id
    )
{
    gnsdk_error_t    error                          = GNSDK_SUCCESS;
    gnsdk_str_t      updated_serialized_user_string = GNSDK_NULL;
    char*            user_filename                  = GNSDK_NULL;
    size_t           user_filename_len              = 0;
    int              rc                             = 0;
    int              return_value                   = 0;
    FILE*            file                           = NULL;

    /* Release our user handle and see if we need to update our stored version */
    error = gnsdk_manager_user_release(user_handle, &updated_serialized_user_string);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
    }
    else if (GNSDK_NULL != updated_serialized_user_string)
    {
        user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
        user_filename = malloc(user_filename_len);

        if (NULL != user_filename)
        {
            strcpy(user_filename,client_id);
            strcat(user_filename,"_user.txt");

            file = fopen(user_filename, "w");
            if (NULL != file)
            {
                return_value = fputs(updated_serialized_user_string, file);
                if (0 > return_value)
                {
                    printf("\nError writing user registration file from buffer.\n");
                    rc = -1;
                }
                fclose(file);
            }
            else
            {
                printf("\nError: Failed to open the user filename for use in saving the updated serialized user. (%s)\n", user_filename);
            }
            free(user_filename);
        }
        else
        {
            printf("\nError: Failed to allocated user filename for us in saving the updated serialized user.\n");
        }
        gnsdk_manager_string_free(updated_serialized_user_string);

    } /* else if (GNSDK_NULL != updated_serialized_user_string) */

    /* Shutdown the libraries */
    gnsdk_manager_shutdown();

}    /* _shutdown_gnsdk() */
