# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Common utilities for GNSDK python applications.
    - wrappers to make iteration more python-like
    - GNSDK_Configuration class
    - set path to GNSDK libraries
    - add extension module folders to system path
When finding paths trys to accommodate working in perforce or in the release package.

"""

import sys
import os
import platform

class GnSDK_Iterator:
    
    def __init__(self, iterator):
        self.__iterator = iterator
    
    def next(self):
        if self.__iterator.has_next():
            return self.__iterator.next()
        else:
            raise StopIteration

class GnSDK_Iterable:

    def __init__(self, iterable):
        self._iterable = iterable
    
    def __iter__(self):
        return GnSDK_Iterator(self._iterable.begin())
    
    def __getitem__(self, index):
        return self._iterable.at(index).next()
    
    def __len__(self):
        return self._iterable.count()

class GDO_Iterable(GnSDK_Iterable):
    
    def __iter__(self):
        return GnSDK_Iterator(self._iterable.get_iterator())

class GNSDK_Configuration:
    
    def __init__(self):
        # Set configuration defaults then load any overrides from config module.
        self.use_local = False
        self.enable_logging = True
        self.storage_path = os.path.join(gdbs_root, "sample_db")
        
        # Now try to open the config script and read any overrides.
        try:
            import gnsdk_config
        except:
            pass
        else:
            try:
                self.use_local = gnsdk_config.use_local
            except:
                pass
            try:
                self.enable_logging = gnsdk_config.enable_logging
            except:
                pass
            try:
                self.storage_path = gnsdk_config.storage_path
            except:
                pass

    def __str__(self):
        return "\tuse_local = %s\n\tenable_logging = %s\n\tstorage_path = %s" % (
            self.use_local, self.enable_logging, self.storage_path
        )

# Construct the GNSDK platform folder name.
def getPlatformDir():
    machine = platform.machine()
    try:
        hyphIdx = machine.index('_')
    except:
        if machine == 'AMD64':
            machine = 'x86-32'
        elif machine == 'i686':
            machine = 'x86-32'
        elif machine == 'i386':
            machine = 'x86-64'
    else:
        machine = machine[:hyphIdx] + '-' + machine[hyphIdx+1:]

    system = platform.system()
    if system == 'Darwin':
        system = 'mac'
    elif system == 'Windows':
        system = 'win'
    elif system == 'Linux':
        system = 'linux'
    elif system[:6] == 'CYGWIN':
        system = 'win'

    return system + '_' + machine

# Find install folder: gnsdk libs, python .cpp extension and python .py interface
#  If launched from under 'gn-sdks', it is gn-sdks/devel/main/install/
#  In build package
#     gnsdk libs are in <root>/lib/<platform>
#     python interface is in <root>/wrappers/gnsdk_python/src_wrapper/
#     compiled cpp extension is in <root>/wrappers/gnsdk_python/lib/<platform>
#  Otherwise check for ./install/.
def getInstallRoot():
    dirTmp = os.getcwd()
    while os.path.basename(dirTmp) != 'gn-sdks':
        if dirTmp != os.path.dirname(dirTmp):
            dirTmp = os.path.dirname(dirTmp)
        else:
            if os.path.exists("install"):
                return "install"
            else:
                return None
    return os.path.join(dirTmp, 'devel', 'main', 'install')

# Set up the paths to the GNSDK libraries and the python extensions.
platformDir = getPlatformDir()
installRoot = getInstallRoot()
if installRoot:
    if installRoot == "install":
        test_data = os.path.join("sample_data")
        gdbs_root = os.path.join(os.getcwd())
    else:
        test_data = os.path.join(installRoot, "..", "source", "wrappers", "sample_data")
        gdbs_root = os.path.join(installRoot, "..", "source", "common")
    lib_path = os.path.join(installRoot, 'common', 'release', 'lib', platformDir)
    moduleDirs = [
        os.path.join(installRoot, 'wrappers', 'gnsdk_python', 'src_wrapper'),
        os.path.join(installRoot, 'wrappers', 'gnsdk_python', 'lib', 'release', platformDir)
    ]
else:
    root = os.path.join("..", "..", "..", "..")
    test_data = os.path.join(root, "wrappers", "sample_data")
    gdbs_root = os.path.join(root, "samples")
    lib_path = os.path.join(root, "lib", platformDir)
    moduleDirs = [
        os.path.join(root, "wrappers", "gnsdk_python", "src_wrapper"),
        os.path.join(root, "wrappers", "gnsdk_python", "lib", platformDir)
    ]

# Check that the extension module directory and the gnsdk library directory exist
for dir in moduleDirs + [lib_path, test_data]:
    if not os.path.exists(dir):
        raise RuntimeError("Path %s does not exist" % dir)

if __name__ != "__main__":
    # Add directories with the python wrapper and extension module to sys.path.
    for moduleDir in moduleDirs:
        sys.path.append(moduleDir)

if __name__ == "__main__":
    print
    print "clientID = %s" % clientId
    print "clientIDTag = %s" % clientIdTag
    print
    print "platformDir = %s" % platformDir
    print "installRoot =   %s" % installRoot
    print "lib_path =      %s" % lib_path
    for item in moduleDirs:
        print "append to path: %s" % item
    print
    print "license = %s" % license
    print
