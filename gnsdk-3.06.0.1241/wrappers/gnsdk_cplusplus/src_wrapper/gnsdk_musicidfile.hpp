/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_MUSICIDFILE_HPP_
#define _GNSDK_MUSICIDFILE_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "metadata_music.hpp"

namespace gracenote
{
	using namespace metadata;

	namespace musicid_file
	{
		class GnMusicIDFile;
		class GnMusicIDFileInfo;

		/**
		 *  Container class for providing media file information to MusicID-File.
		 */
		class GnMusicIDFileInfo : public GnObject
		{
		public:
			GnMusicIDFileInfo() : fileInfohandle_(GNSDK_NULL) { }

			/**
			 *  Retrieves the identifier string from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the identifier
			 */
			gnsdk_cstr_t
			GetIdentifier( ) throw ( GnError );

			/**
			 *  Retrieves the file name from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the file name
			 */
			gnsdk_cstr_t
			GetFileName( ) throw ( GnError );

			/**
			 *  Sets the file name for a FileInfo object.
			 *  @param value set String value for the file name
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetFileName(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the Gracenote CDDB ID value from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the CDDB ID
			 */
			gnsdk_cstr_t
			GetCddbID( ) throw ( GnError );

			/**
			 *  Sets the Gracenote CDDB ID value for a FileInfo object.
			 *  @param value set String value for the CDDB ID
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo object are set with metadata, as MusicID-File
			 *  will not process FileInfo objects that do not contain metadata.
			 */
			void
			SetCddbID(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the album artist from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the album artist
			 */
			gnsdk_cstr_t
			GetAlbumArtist( ) throw ( GnError );

			/**
			 *  Sets the album artist for a FileInfo object.
			 *  @param value set String value for the album artist
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetAlbumArtist(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the album title from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the album title
			 */
			gnsdk_cstr_t
			GetAlbumTitle( ) throw ( GnError );

			/**
			 *  Sets the album title for a FileInfo object.
			 *  @param value set String value for the album title
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetAlbumTitle(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the track artist from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the track artist
			 */
			gnsdk_cstr_t
			GetTrackArtist( ) throw ( GnError );

			/**
			 *  Sets the track artist for a FileInfo object.
			 *  @param value set String value for the track artist
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTrackArtist(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the track title from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the track title
			 */
			gnsdk_cstr_t
			GetTrackTitle( ) throw ( GnError );

			/**
			 *  Sets the track title for a FileInfo object.
			 *  @param value set String value for the track title
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTrackTitle(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the track number from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the track number
			 */
			gnsdk_cstr_t
			GetTrackNumber( ) throw ( GnError );

			/**
			 *  Sets the track number for a FileInfo object.
			 *  @param value set String value for the track number
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTrackNumber(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the disc number from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the disc number
			 */
			gnsdk_cstr_t
			GetDiscNumber( ) throw ( GnError );

			/**
			 *  Sets the disc number for a FileInfo object.
			 *  @param value set String value for the disc number
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetDiscNumber(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the tag ID from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the tag ID
			 */
			gnsdk_cstr_t
			GetTagID( ) throw ( GnError );

			/**
			 *  Sets the tag ID for a FileInfo object.
			 *  @param value set String value for the the tag ID
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTagID(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the fingerprint from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the fingerprint
			 */
			gnsdk_cstr_t
			GetFingerprint( ) throw ( GnError );

			/**
			 *  Sets the fingerprint for a FileInfo object.
			 *  @param value set String value for the fingerprint
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetFingerprint(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the media ID from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the media ID
			 */
			gnsdk_cstr_t
			GetMediaID( ) throw ( GnError );

			/**
			 *  Sets the Media ID for a FileInfo object.
			 *  @param value set String value for the media ID
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetMediaID(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the Media Unique ID (MUI) from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the MUI
			 */
			gnsdk_cstr_t
			GetMUI( ) throw ( GnError );

			/**
			 *  Sets the Media Unique ID (MUI) for a FileInfo object.
			 *  @param value set String value for the MUI
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetMUI(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the TOC offsets value from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the TOC offsets
			 */
			gnsdk_cstr_t
			GetTocOffsets( ) throw ( GnError );

			/**
			 *  Sets the TOC offsets value for a FileInfo object.
			 *  @param value set String value for the TOC offsets
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTocOffsets(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the Title Unique Identifier (TUI) from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the TUI
			 */
			gnsdk_cstr_t
			GetTUI( ) throw ( GnError );

			/**
			 *  Sets the Title Unique Identifier (TUI) for a FileInfo object.
			 *  @param value set String value for the TUI
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTUI(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Retrieves the TUI tag from a FileInfo object.
			 *  @return gnsdk_cstr_t Pointer to receive the data value defined for the TUI tag
			 */
			gnsdk_cstr_t
			GetTuiTag( ) throw ( GnError );

			/**
			 *  Sets the TUI tag for a FileInfo object.
			 *  @param value set String value for the TUI tag
			 *  <p><b>Important:</b></p>
			 *  Ensure that all created FileInfo objects are set with metadata, as MusicID-File will not process
			   FileInfo objects that do not contain metadata.
			 */
			void
			SetTuiTag(gnsdk_cstr_t value) throw ( GnError );

			/**
			 *  Initializes fingerprint generation for a FileInfo object.
			 *  @param audioSampleRate set Sample frequency of audio to be provided: 11 kHz, 22 kHz, or 44 kHz
			 *  @param audioSampleSize set Sample rate of audio to be provided (in 8-bit, 16-bit, or
			 *  32-bit bytes per sample)
			 *  @param audioChannels set Number of channels for audio to be provided (1 or 2)
			 *  <p><b>Remarks:</b></p>
			 *  The MusicID-File fingerprinting APIs allow applications to provide audio data as a method of
			   identification. This enables MusicID-File to perform identification based on the audio itself, as
			   opposed to performing identification using only the associated metadata.
			 *  Use the MusicID-File fingerprinting APIs either before processing has begun, or during a
			   get_fingerprint() function callback.
			 * <p><b>Note:</b></p>
			 *   The application must successfully initialize the GNSDK DSP library prior to using MusicID-File.
			 */
			void
			FingerprintBegin(gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels) throw ( GnError );

			/**
			 *  Provides uncompressed audio data to a FileInfo object for fingerprint generation.
			 *  @param audioData set Pointer to audio data buffer that matches the audio format described in
			   fingerprint_begin().
			 *  @param audioDataSize set Size of audio data buffer (in bytes)
			 *  @return bool Checks whether the fingerprint generation has received enough audio data
			 *  <p><b>Remarks:</b></p>
			 *  The provided audio data must be uncompressed PCM data and must match the format given to
			   fingerprint_begin().
			 *  The p_complete parameter receives a GNSDK_TRUE value when the fingerprinting process has received
			   enough audio data to perform its processing. Any further provided audio data is ignored.
			 *  The application must provide audio data until the p_complete parameter receives a GNSDK_TRUE
			   value to successfully generate an audio fingerprint.
			 *  The application must complete the fingerprinting process by calling
			   fingerprint_end() when either the audio data terminates, or after receiving
			   a GNSDK_TRUE value.
			 *  Use the MusicID-File fingerprinting APIs either before processing has begun, or during a
			   gnsdk_musicidfile_callback_get_fingerprint_fn callback.
			 * <p><b>Note:</b></p>
			 *   The application must successfully initialize the GNSDK DSP library prior to using MusicID-File.
			 */
			bool
			FingerprintWrite(const gnsdk_byte_t* audioData, gnsdk_size_t audioDataSize) throw ( GnError );

			/**
			 *  Finalizes fingerprint generation for a FileInfo object.
			 *  <p><b>Remarks:</b></p>
			 *  The application must complete the fingerprinting process by calling
			   fingerprint_end when either the audio data terminates, or after receiving
			   a GNSDK_TRUE value.
			 *  Use the MusicID-File fingerprinting APIs either before processing has begun, or during a
			   gnsdk_musicidfile_callback_get_fingerprint_fn callback.
			 * <p><b>Note:</b></p>
			 *   The application must successfully initialize the GNSDK DSP library prior to using MusicID-File.
			 */
			void
			FingerprintEnd( ) throw ( GnError );

			void
			FingerprintFromSource(GnAudioSource& audiosource) throw ( GnError );

			/**
			 *  Retrieves the current status for a specific FileInfo object.
			 *  <p><b>Remarks:</b></p>
			 *  The File Info object's state value indicates what kind of response is available for a FileInfo object after
			 *  MusicID-File. In the case of an error, error info can be retrieved from the FileInfo object.
			 */
			gnsdk_musicidfile_fileinfo_status_t
			Status() throw ( GnError );

			/**
			 *  Retrieves the error information for a FileInfo object. This is related to the status returned.
			 *  If the status is error, this call returns the extended error information.
			 *  <p><b>Remarks:</b></p>
			 *  An error object is returned representing the FileInfo error condition. An error object exception
			 *  may be thrown if an error occurred retrieving the FileInfo object's error object.
			 */
			GnError
			ErrorInformation() throw( GnError );

			/**
			 *  Retrieves a GDO Object containing the response for the given FileInfo object.
			 *  @return GnResponseAlbums to receive the GDO Object
			 *  <p><b>Remarks:</b></p>
			 *  This function retrieves the response GDO of the match (if available) for the given query handle.
			 *  The response GDO (if available) is of an response album type.
			 *  Use status to determine if a response is available for a FileInfo
			   object.
			 */
			GnResponseAlbums
			AlbumResponse( ) throw ( GnError );

			/**
			 *  Retrieves a GDO Object containing the response for the given FileInfo object.
			 *  @return GnResponseDataMatches to receive the GDO Object
			 *  <p><b>Remarks:</b></p>
			 *  This function retrieves the response GDO of the match (if available) for the given query handle.
			 *  The response GDO (if available) is of an response GnDataMatch type.
			 *  ( which implies that it could be an album or contributor or a mix thereof depending on the query options.)
			 *  Use status to determine if a response is available for a FileInfo
			   object.
			 */
			GnResponseDataMatches
			DataMatchResponse () throw (GnError);


		protected:

			/* Protected Constructor */
			GnMusicIDFileInfo( gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t handle) : GnObject(query_handle), fileInfohandle_(handle) { };


		private:

			gnsdk_musicidfile_fileinfo_handle_t fileInfohandle_;
			friend class GnMusicIDFile;
			friend class gn_musicid_file_info_provider;

		};


		class gn_musicid_file_info_provider
		{
		public:

			gn_musicid_file_info_provider(gnsdk_musicidfile_query_handle_t query_handle) : mQueryHandle(query_handle){ }
			~gn_musicid_file_info_provider(){ }

			/*-----------------------------------------------------------------------------
			 *  get_data
			 */
			GnMusicIDFileInfo
			get_data(gnsdk_uint32_t pos) const throw ( GnError )
			{
				gnsdk_musicidfile_fileinfo_handle_t fileinfo_handle = GNSDK_NULL;
				gnsdk_error_t                       error;


				error = gnsdk_musicidfile_query_fileinfo_get_by_index(mQueryHandle, pos-1, &fileinfo_handle);
				if ( error ) { throw GnError(); }

				return GnMusicIDFileInfo(mQueryHandle, fileinfo_handle);
			}

			/*-----------------------------------------------------------------------------
			 *  count
			 */
			gnsdk_uint32_t
			count ()
			{
				gnsdk_uint32_t count = 0;


				gnsdk_musicidfile_query_fileinfo_count(mQueryHandle, &count);
				return count;
			}

			static const gnsdk_uint32_t kOrdinalStart = 1;
			static const gnsdk_uint32_t kCountOffset  = 1;


		private:

			gnsdk_musicidfile_query_handle_t mQueryHandle;
		};


		typedef gn_facade_range_iterator<GnMusicIDFileInfo, gn_musicid_file_info_provider>      musicid_file_info_iterator;

		/**
		 *  Defines the structure of callback functions for data retrieval and
		 *  status updates as various MusicID-File operations are performed.
		 */
		class GnMusicIDFileEvents
		{
		public:

			virtual
			~GnMusicIDFileEvents() { };

			/**
			 *  Retrieves the current status.
			 *  @param fileinfo FileInfo object that the callback operates on
			 *  @param midf_status FileInfo status
			 *  @param current_file Current number of the file being processed
			 *  @param total_files Total number of files to be processed by the callback
			 */
			virtual void
			status(GnMusicIDFileInfo& fileinfo, gnsdk_musicidfile_callback_status_t midf_status, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files)
			{
				/* default impl: ignore status */
				(void)fileinfo;
				(void)midf_status;
				(void)current_file;
				(void)total_files;
			}

			/**
			 *  Callback function declaration to allow cancellation of query notification.
			 *  @return True to cancel query the callback
			 */
			virtual bool
			cancel_check(void)
			{
				/* default impl: don't cancel */
				return false;
			}

			/**
			 *  Callback function declaration for a fingerprint generation request.
			 *  @param fileinfo FileInfo object that the callback operates on
			 *  @param current_file Current number of the file being processed
			 *  @param total_files Total number of files to be processed
			 *  <p><b>Remarks:</b></p>
			 *  The application can implement this callback to use the fingerprint_begin(), fingerprint_write() and fingerprint_end() APIs with the given FileInfo object to
			   generate a fingerprint from raw audio. If your application already has created the fingerprint for the respective file,
			   the application should implement this callback to use the GnMusicIDFileInfo.set*() API to set any metadata values in the FileInfo object.
			   This callback is called only if no fingerprint has already been set for the passed FileInfo object.
			 */
			virtual void
			get_fingerprint(GnMusicIDFileInfo& fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files)
			{
				/* default impl: no fingerprint to generate */
				(void)fileinfo;
				(void)current_file;
				(void)total_files;
			}

			/**
			 *  Callback function declaration for a metadata gathering request.
			 *  @param fileinfo FileInfo object that the callback operates on
			 *  @param current_file Current number of the file being processed
			 *  @param total_files Total number of files to be processed
			 *  <p><b>Remarks:</b></p>
			 *   The application should implement this callback to use the GnMusicIDFileInfo.set*() API to set any metadata values in the FileInfo object.
			   This callback is called only if no metadata has already been set for the passed FileInfo.
			 */
			virtual void
			get_metadata(GnMusicIDFileInfo& fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files)
			{
				/* default impl: no metadata to gather */
				(void)fileinfo;
				(void)current_file;
				(void)total_files;
			}

			/**
			 *  Callback function declaration for a result available notification.
			 *  @param album_result GnResponseAlbums
			 *  @param current_album Current number of the album in this response
			 *  @param total_albums Total number of albums in this response
			 *  <p><b>Remarks:</b></p>
			 *  The provided album_result will include results for one or more files.
			 */
			virtual void
			result_available(GnResponseAlbums& album_result, gnsdk_uint32_t current_album, gnsdk_uint32_t total_albums)
			{
				/* default impl: ignore result */
				(void)album_result;
				(void)current_album;
				(void)total_albums;
			}

			/*-----------------------------------------------------------------------------
			 *  result_available
			 */
			virtual void
			result_available(GnResponseDataMatches& matches_result, gnsdk_uint32_t current_album, gnsdk_uint32_t total_albums)
			{
				/* default impl: ignore result */
				(void)matches_result;
				(void)current_album;
				(void)total_albums;
			}

			/**
			 *  Callback function declaration for a no results notification.
			 *  @param fileinfo set FileInfo object that the callback operates on
			 *  @param current_file Current number of the file that is not found
			 *  @param total_files Total number of files to be processed
			 */
			virtual void
			result_not_found(GnMusicIDFileInfo& fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files)
			{
				/* default impl: ignore result */
				(void)fileinfo;
				(void)current_file;
				(void)total_files;
			}

			/**
			 *  Callback function declaration for MusicID-File processing completion.
			 *  @param musicidfile_complete_error Final error value of MusicID-File operation
			 */
			virtual void
			complete(gnsdk_error_t musicidfile_complete_error)
			{
				/* default impl: ignore complete */
				(void)musicidfile_complete_error;
			}

		};

		/**
		*  Response and processing controls for DoTrackID, DoAlbumID, DoLibraryID
		*/
		typedef int GnMidfQueryFlags;
		static const GnMidfQueryFlags GnMidfQueryReturnSingle      = GNSDK_MUSICIDFILE_QUERY_FLAG_RETURN_SINGLE;
		static const GnMidfQueryFlags GnMidfQueryReturnAll         = GNSDK_MUSICIDFILE_QUERY_FLAG_RETURN_ALL;

		static const GnMidfQueryFlags GnMidfQueryResponseAlbums    = GNSDK_MUSICIDFILE_QUERY_FLAG_RESPONSE_ALBUMS;
		static const GnMidfQueryFlags GnMidfQueryResponseMatches   = GNSDK_MUSICIDFILE_QUERY_FLAG_RESPONSE_MATCHES;

		static const GnMidfQueryFlags GnMidfQueryRunAsynchronously = GNSDK_MUSICIDFILE_QUERY_FLAG_ASYNC;
		static const GnMidfQueryFlags GnMidfQueryAggressiveSearch  = GNSDK_MUSICIDFILE_QUERY_FLAG_AGGRESSIVE;
		static const GnMidfQueryFlags GnMidfQueryNoThreads         = GNSDK_MUSICIDFILE_QUERY_FLAG_NO_THREADS;
		static const GnMidfQueryFlags GnMidfQueryDefault           = GNSDK_MUSICIDFILE_QUERY_FLAG_RESPONSE_ALBUMS|GNSDK_MUSICIDFILE_QUERY_FLAG_RETURN_SINGLE;

		/**
		 *  MusicID-File is intended to be used with collections of file-based media.
		 *  When an application provides decoded audio and text data for each file to the
		 *  library, the MusicID-File functionality both identifies each file and groups
		 *  the files into albums.
		 *
		 *  Use GnMusicIDFile for bulk music recognition with advanced features like
		 *  <ul><li>Fingerprinting</li>
		 *	<li>Background processing</li>
		 *  <li>Multiple ways to recognize music: TrackID, AlbumID, and LibraryID</li></ul>
		 *
		 *  Refer to your documentation and samples for more information about these options.
		 *
		 *  Note: Feature generation is controlled by a Gracenote License.
		 */
		class GnMusicIDFile : public GnObject
		{
		public:

			/**
			 *  Initializes the Gracenote MusicID-File library.
			 *  @param user GnUser object representing the user making the GnMusicIDFile request
			 */
			GnMusicIDFile(const GnUser& user, GnMusicIDFileEvents* pEventHandler = GNSDK_NULL) throw ( GnError );

			virtual
			~GnMusicIDFile();

			/**
			 *  Retrieves the MusicID-File library's version string.
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully.
			 *  The returned string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example version string: 1.2.3.123 (Major.Minor.Improvement.Build)
			 *
			 *  Major: New functionality
			 *
			 *  Minor: New or changed features
			 *
			 *  Improvement: Improvements and fixes
			 *
			 *  Build: Internal build number
			 */
			static gnsdk_cstr_t
			Version();

			/**
			 *  Retrieves the MusicID-File library's build date string.
			 *  @return gnsdk_cstr_t Build date string of the format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully.
			 *  The returned string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example build date string: 2008-02-12 00:41 UTC
			 */
			static gnsdk_cstr_t
			BuildDate();

			/**
			 *  Indicates whether the MusicID-File query should be performed against local embedded databases or online.
			 *  @param val  One of the #GnLookupMode values
			 */
			void
			OptionLookupMode(GnLookupMode lookupMode) throw ( GnError );

			/**
			 *  Sets the lookup data value for the MusicID-File query.
			 *  @param val set One of the #GnLookupData values
			 *  @param b_enable True or false to enable or disable
			 */
			void
			OptionLookupData(GnLookupData val, bool enable = GNSDK_TRUE) throw ( GnError );

			/**
			 *  Sets the lookup data value for the MusicID-File query.
			 *  @param lookupOption set One of the MusicID-File Option Keys
			 *  @param b_enable set True or false to enable or disable
			 */
			void
			OptionSet(gnsdk_cstr_t lookupOption, bool enable = GNSDK_TRUE) throw ( GnError );

			/**
			 *  Sets the preferred language for the MusicID-File query.
			 *  @param preferred_language One of the GnSDK language values
			 */
			void
			OptionPreferredLanguage(gnsdk_cstr_t preferedLanguage) throw ( GnError );

			/**
			 *  Sets the batch size for the MusicID-File query.
			 *  @param batch_size set String value or one of MusicID-File Option Values that corresponds to BATCH_SIZE
			 *  <p><b>Remarks:</b></p>
			 *  The option value provided for batch size must be greater than zero (0).
			 */
			void
			OptionBatchSize(gnsdk_cstr_t batchSize) throw ( GnError );

			/**
			 *  Sets the thread priority for a given MusicID-File query.
			 *  @param value set String value or one of #GnThreadPriority values that corresponds to thread priority
			 *  <p><b>Remarks:</b></p>
			 *  The option value provided for thread priority must be one of the defined
			 *  #GnThreadPriority values.
			 */
			void
			OptionThreadPriority(GnThreadPriority threadPriority) throw ( GnError );

			/**
			 *  Indicates whether MusicID-File should Process the responses Online, this may reduce the amount of resources used by the client.
			 *  @param b_enable True or false to enable or disable
			 */
			void
			OptionOnlineProcessing(bool enable) throw ( GnError );

			/**
			 * Use this option to specify an XID which MusicID-File should try to include in any responses that are returned.
			 *  <p><b>Remarks:</b></p>
			 * This option is currently only supported when GNSDK_MUSICIDFILE_OPTION_ONLINE_PROCESSING is set to "true".
			 *  @param preferred_xid the name of an XID that should be preferred when selecting matches
			 */
			void
			OptionSetPreferredXID(gnsdk_cstr_t preferredXid) throw ( GnError );

			/**
			 *  Creates and adds a MusicIDFileInfo to the current query . Its returns a GnMusicIDFileInfo object
			 *  that is an internal reference to the added MusicIDFIleInfo.
			 */
			GnMusicIDFileInfo
			CreateFileInfo(gnsdk_cstr_t uniqueIdentifier) throw (GnError );

			/**
			 *  Remove an GnMusicIDFileInfo from the  query
			 */
			void
			RemoveFileInfo(GnMusicIDFileInfo& toRemove ) throw ( GnError );

			/**
			 *  Returns an iterator for accessing the MusicID-File FileInfo instances.
			 */
			gn_iterable_container<musicid_file_info_iterator> FileInfos( )
			{
				gn_musicid_file_info_provider provider(queryHandle_);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<musicid_file_info_iterator>(musicid_file_info_iterator(provider, 1, count), musicid_file_info_iterator(provider, count, count));
			}

			/**
			 *  Serializes all current GnMusicIdFileInfo objects into XML.
			 */
			GnString
			SerializeFileInfosToXml() throw ( GnError );

			/**
			 *  Creates MusicID FileInfo objects from the XML specified.
			 *
			 *  Note: this method overwrites any existing MusicID FileInfo objects.
			 *	Use this method when when you are working with serialized FileInfo objects that you don't want to fingerprint again.
			 *  @param xmlbuffer		Const char* to the appropriate XML buffer
			 *  <p><b>Remarks:</b></p>
			 *  Returns the MusicID FileInfo object count that is created from the XML.
			 */
			gnsdk_uint32_t
			SetFileInfosFromXml( const char* xmlbuffer) throw ( GnError );

			/**
			 *  Initiates TrackID processing.
			 *
			 *  @param pEventHandler
			 *  @param resultFlag   Set result type e.g return single/ return all etc.
			 *  @param processFlag  Process type e.g run asynchronous, aggressive search and no thread etc.
			 *  @param responseFlag  set match response or album response.
			 *
			 *  <p><b>Remarks:</b></p>
			 *  TrackID processing performs MusicID-File recognition on each given FileInfo independently.
			 */
			void
			DoTrackID(GnMidfQueryFlags queryFlags) throw ( GnError );

			/**
			 *  Initiates AlbumID processing.
			 *
			 *  @param pEventHandler
			 *  @param resultFlag   Set result type e.g return single/ return all etc.
			 *  @param processFlag  Process type e.g run asynchronous, aggressive search and no thread etc.
			 *  @param responseFlag  set match response or album response.
			 *
			 *  <p><b>Remarks:</b></p>
			 *  AlbumID processing performs MusicID-File recognition on all of the given FileInfos as a group.
			   This type of processing can be more accurate than TrackID processing, as AlbumID processing matches
			   the files to similar groups of albums.
			 */
			void
			DoAlbumID(GnMidfQueryFlags queryFlags) throw ( GnError );

			/**
			 *  Initiates LibraryID processing.
			 *
			 *  @param pEventHandler
			 *  @param resultFlag   Set result type e.g return single/ return all etc.
			 *  @param processFlag  Process type e.g run asynchronous, aggressive search and no thread etc.
			 *  @param responseFlag  set match response or album response.
			 *
			 *  <p><b>Remarks:</b></p>
			 *  LibraryID processing performs MusicID-File recognition on a large number of given FileInfos. This
			   processing divides the given group of FileInfos into workable batches, and then processes each batch
			   using AlbumID functionality.
			 */
			void
			DoLibraryID(GnMidfQueryFlags queryFlags) throw ( GnError );

			/**
			 *  Value for infinite wait in a call to GnMusicIDFile.WaitForComplete().
			 *  Otherwise enter the timeout duration in milliseconds.
			 */
			static gnsdk_uint32_t kTimeValueInfinite;

			/**
			 *  Sets a wait time for a MusicID-File operation to complete.
			 *  @param timeoutValue Length of time to wait in milliseconds, or the GnMusicIDFile::kTimeValueInfinite flag
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to set a wait for TrackID, AlbumID, or LibraryID processing to complete for
			 *  a given GnMusicIDFile instance.
			 */
			void
			WaitForComplete(gnsdk_uint32_t timeoutValue) throw ( GnError );

			/**
			 * Sets the internal cancel flag for a GnMusicIDFile object that is currently being processed.
			 * <p><b>Remarks:</b></p>
			 * This function instructs the processing MusicID-File handle to cancel any current processing. This
			 * call does not wait for the actual processing to terminate and returns immediately.
			 */
			void
			Cancel( ) throw ( GnError );

		private:

			GnMusicIDFileEvents*             eventHandler_;
			gnsdk_musicidfile_query_handle_t queryHandle_;

			/**
			 *  MusicID-File status callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_status(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo, gnsdk_musicidfile_callback_status_t status, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort);

			/**
			 * Fingerprint callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_get_fingerprint(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort);

			/**
			 * Metadata callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_get_metadata(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort);

			/**
			 *  Result available callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_result_available(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_gdo_handle_t response_gdo, gnsdk_uint32_t current_album, gnsdk_uint32_t total_albums, gnsdk_bool_t* p_abort);

			/**
			 *  Result not found callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_result_not_found(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* _abort);

			/**
			 *  MusicID complete callback
			 */
			static void GNSDK_CALLBACK_API
			_callback_musicid_complete(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_error_t musicidfile_complete_error);

			friend class GnMusicIDFileInfo;

			/**
			 *  Dissallow assignment operator
			 */
			DISALLOW_COPY_AND_ASSIGN(GnMusicIDFile);
		};


		/**
		 *  Define sampling operations
		 */
		class GnIFingerprinterMusicIDFile
		{
		public:

			virtual
			~GnIFingerprinterMusicIDFile() { };

			virtual void
			Begin(gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels) = 0;
			virtual void
			Write(gnsdk_byte_t audioData[], gnsdk_uint32_t dataSize, bool b_complete) = 0;
			virtual void
			End() = 0;
		};


	} /* namespace MusicIDFile */
}     /* namespace GracenoteSDK */
#endif /* _GNSDK_MUSICIDFILE_HPP_ */

