# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Name: musicid_stream
Description:
  Uses MusicID-Stream to fingerprint and identify a music track.

Command line syntax:
> python main.py <client_id> <client_id_tag> <license_filename>

"""

import os
import sys
sys.path.append("..")

# gnsdk_utilities finds gnsdk and sets up sys.path. Import first.
import gnsdk_utilities
import gnsdk

application_version = "1.0.0.0"

def get_build_info(name, gnsdk_lib):
    return name+ \
        " Version    : " + gnsdk_lib.version() + \
        "  (built " + gnsdk_lib.build_date() + ")"

class GNSDK_Manager:

    def __init__(self, client_id, client_id_tag, license_path):
        try:
            self.__manager = gnsdk.GnSDK(
                gnsdk_utilities.lib_path,
                license_path,
                gnsdk.GnSDK.kFilename
            )
        except gnsdk.GnError as e:
            print "Error creating GnSDK instance: %s" % e.error_description()
            exit(1)

        if configuration.enable_logging:
            self.__manager.logging_enable(
                "sample.log",
                self.__manager.gn_log_pkg_all,
                self.__manager.gn_log_level_error|self.__manager.gn_log_level_warning,
                self.__manager.gn_log_option_all,
                0,                                                         # Max size
                False                                                      # save logs
            )

        try:
            self.__storage = gnsdk.GnStorageSqlite()
        except gnsdk.GnError as e:
            print "Error creating GnStorageSqlite instance: %s" % e.error_description()
            exit(1)

        # Display the build information
        self.display_build_info()
        # Get the user, registering if necessary.
        self.user = self.get_user(client_id, client_id_tag, application_version)

        # Set the locale.
        try:
            locale = gnsdk.GnLocale(
                self.__manager.k_locale_group_music,
                self.__manager.k_language_english,
                self.__manager.k_region_default,
                self.__manager.k_descriptor_simplified,
                self.user
            )
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

        try:
            self.__manager.set_default_locale(locale)
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

    def get_user(self, client_id, client_id_tag, application_version):
        user_filename = "user.txt"
        try:
            user_file = open(user_filename, "r")
        except:
            print "\nInfo: No stored user - this must be the app's first run."
            reg_mode = self.__manager.kUserRegModeOnline
            try:
                serialized_user = self.__manager.register_user(
                    reg_mode,
                    client_id,
                    client_id_tag,
                    application_version
                ).c_str()
            except gnsdk.GnError as e:
                print "Error creating user: %s" % e.error_description()
                exit(1)
            open(user_filename, "w").write(serialized_user)
        else:
            serialized_user = user_file.read()
        try:
            user = gnsdk.GnUser(
                serialized_user,
                client_id,
                client_id_tag,
                application_version
            )
        except gnsdk.GnError as e:
            print "Error creating user: %s" % e.error_description()
            exit(1)

        return user

    def display_build_info(self):
        # Print all of the version information.
        print get_build_info("\nGNSDK Product", self.__manager)

if __name__ == "__main__":
    def usage(program):
        print "USAGE!!:\n\t%s <client_id> <client_id_tag> <license_path>\n" % program
        exit(1)
    
    if len(sys.argv) != 4:
        usage(sys.argv[0])
    
    # configuration instance
    configuration = gnsdk_utilities.GNSDK_Configuration()

    # Create the manager
    manager = GNSDK_Manager(sys.argv[1], sys.argv[2], sys.argv[3])

    print "\n*****Sample MID-Stream Query*****"
    test_inputs =[os.path.join(gnsdk_utilities.test_data,"05-Hummingbird-sample.wav")]

    def display_album_titles(albums):
        for album_index, album in enumerate(albums):
            print "%2d: %s" % (album_index+1, album.title().display())

    def do_match_selection(albums):
        print "Match count: %d" % len(albums)
        display_album_titles(albums)
        # Here you could prompt the user to selection. We just hard-code 1st result.
        return 1

    def best_album(*args):

        album = None
        response = music_id.find_albums(*args)

        if type(response) is gnsdk.GnResponseAlbums:
            # albums does not have an __iter__() method. Wrap it to support iteration.
            albums = gnsdk_utilities.GDO_Iterable(response.albums())
            if len(albums) == 0:
                print "No albums found\n"
            else:
                if response.needs_decision() or len(albums) > 1:
                    choice_ordinal = do_match_selection(albums)
                else:
                    choice_ordinal = 1

                album = albums[choice_ordinal-1]

        else:
            print "Error: invalid response\n"

        return album

    def display_album_title(album):
        print "          Title: %s" % album.title().display()

    def get_fingerprint(wave_filename):
        wave_file = open(wave_filename, "rb")
        # Skip past header. Note: the header may be different size on your files.
        wave_file.seek(44)
        music_id.fingerprint_begin(gnsdk.kFingerprintTypeGNFPX, 44100, 16, 2)
        pcm_audio = bytearray(wave_file.read(2048))
        complete = False
        while len(pcm_audio) and not complete:
            pcm_data_buff = gnsdk.byte_buffer(len(pcm_audio))
            for i, pcm_audio_byte in enumerate(pcm_audio):
                pcm_data_buff[i] = pcm_audio_byte
            complete = music_id.fingerprint_write(pcm_data_buff, len(pcm_audio))
            pcm_audio = bytearray(wave_file.read(2048))
        music_id.fingerprint_end()
        return music_id.fingerprint_data_get()

    # Initialize musicid.
    music_id = gnsdk.GnMusicID(manager.user)
    # Set options here, e.g. to request the single best result uncomment the next line
    #music_id.option_result_single(True)

    for test_input in test_inputs:
        album = best_album(get_fingerprint(test_input),gnsdk.kFingerprintTypeGNFPX)
        if album:
            print "    Final album:"
            if not album.full_result():
                print "Title: %s" % album.title().display()
                print "Album is partial...requerying for full result."
                print "requery"
                album = best_album(album)
            display_album_title(album)
 