var a00129 =
[
    [ "GnResponseVideoSuggestions", "a00129.html#a52efb13aacd817cafbd3585f3ef22e54", null ],
    [ "GnResponseVideoSuggestions", "a00129.html#a53adf09c27b6c7e6c2403b13bf5638ea", null ],
    [ "delete", "a00129.html#a08410ec8d9e3adcdea633fc0dc2954c8", null ],
    [ "finalize", "a00129.html#a39454978f5131cf5669170c88748601d", null ],
    [ "needsDecision", "a00129.html#ae334cc3e1745ae034be2cc5c57bc4f06", null ],
    [ "rangeEnd", "a00129.html#a36553c24e2451d322a15ee541f7c1b7a", null ],
    [ "rangeStart", "a00129.html#a4eb567b03e786b5ab4d81095c4dcefd8", null ],
    [ "rangeTotal", "a00129.html#a6f4d094f504a5f9a92b38c071fb3b435", null ],
    [ "resultCount", "a00129.html#a79c83b47ec8fda00e59ad59aaee82e8e", null ],
    [ "suggestionText", "a00129.html#acd17f646c6e83258a16420f1cda9b3e2", null ],
    [ "suggestionTitle", "a00129.html#aaaf6dc004eff718fdfa58c327c3febd1", null ],
    [ "suggestionType", "a00129.html#af958311e3cdf6bae00a65a726bbbca63", null ]
];