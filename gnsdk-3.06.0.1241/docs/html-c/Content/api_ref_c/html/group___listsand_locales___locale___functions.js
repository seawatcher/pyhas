var group___listsand_locales___locale___functions =
[
    [ "gnsdk_manager_locale_available_count", "group___listsand_locales___locale___functions.html#ga5f6f97b1c2bc1e0bb82dd93d1d5f4c5d", null ],
    [ "gnsdk_manager_locale_available_get", "group___listsand_locales___locale___functions.html#ga62378868493ed4a7ed472126bf4f65fd", null ],
    [ "gnsdk_manager_locale_deserialize", "group___listsand_locales___locale___functions.html#gaa5c71b9db3cde22c45062f8b65c77195", null ],
    [ "gnsdk_manager_locale_info", "group___listsand_locales___locale___functions.html#ga2f55df885a1fb0a987bc4aac78f8eac3", null ],
    [ "gnsdk_manager_locale_load", "group___listsand_locales___locale___functions.html#ga348565c2e106bcf3050115746b28b875", null ],
    [ "gnsdk_manager_locale_release", "group___listsand_locales___locale___functions.html#gacbbb3a99abedb6de408d78230beebff9", null ],
    [ "gnsdk_manager_locale_serialize", "group___listsand_locales___locale___functions.html#ga4946128003a8177cd494d2b2c3d4908b", null ],
    [ "gnsdk_manager_locale_set_group_default", "group___listsand_locales___locale___functions.html#gaedcafbb4dcb1c598e2828cd28fb012f0", null ],
    [ "gnsdk_manager_locale_unset_group_default", "group___listsand_locales___locale___functions.html#gae9c7c111821f6a295cdb183edc0ce286", null ],
    [ "gnsdk_manager_locale_update", "group___listsand_locales___locale___functions.html#gaada58c83a8ce45ad30a1c39bd61a74a1", null ],
    [ "gnsdk_manager_locale_update_check", "group___listsand_locales___locale___functions.html#gaf36908d23b353d672099a076b4443ab1", null ]
];