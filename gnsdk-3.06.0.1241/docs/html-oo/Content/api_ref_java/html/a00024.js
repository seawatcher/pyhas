var a00024 =
[
    [ "GnDataMatch", "a00024.html#ab292db9f635d13d6adc40d2a4170a7d7", null ],
    [ "GnDataMatch", "a00024.html#a6fc13012ab9fbd5c2cad61e905529be8", null ],
    [ "GnDataMatch", "a00024.html#a77df844fcb2b1ca94ba01f51e958a33e", null ],
    [ "delete", "a00024.html#a6766f3b8180c01b2c1d608e696275ccf", null ],
    [ "finalize", "a00024.html#a0b73cf5e2b074617d55e900b8372ed60", null ],
    [ "getAsAlbum", "a00024.html#ae3c57f0151c8683c380f6c09b3fbcf14", null ],
    [ "getAsContributor", "a00024.html#af3f7abc040c4c9259f97be20f08b4962", null ],
    [ "isAlbum", "a00024.html#a8d7f4317227a45fa7d32b937c490ea46", null ],
    [ "isContributor", "a00024.html#af860cd6eb81c563d28aefe0b714ba314", null ]
];