var class_gracenote_s_d_k_1_1_gn_data_match_enumerator =
[
    [ "GnDataMatchEnumerator", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#aa7f98ae87a7e2e25871f6945d39ae0bd", null ],
    [ "GnDataMatchEnumerator", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#ae7319afe438fdcdc2f357a54ae577ddf", null ],
    [ "__ref__", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#ade5421da4d120dcf4691522ca462168a", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#a1ddc009eb33dd88785440c32744487de", null ],
    [ "distance", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#ac30c54d32cfa53ddca8138ad7119d9af", null ],
    [ "hasNext", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#a68abffe0780560b2b5eb3b8e01d99019", null ],
    [ "next", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#a294f7d0aa0c731a1e0aed0605070da51", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html#abfe91b6b45d1e8fbd39e3cc04fae89fe", null ]
];