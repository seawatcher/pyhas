﻿/*  
 *  Copyright (c) 2000-2013 Gracenote.
 *  
 *  This software may not be used in any way or distributed without
 *  permission. All rights reserved.
 *  
 *  Some code herein may be covered by US and international patents.
 */  
   
/*  
 *  Name: MusicIdLookupMatchesText
 *  Description:
 *  This example finds matches based on input text.
 *  
 *  Command-line Syntax:
 *  sample clientId clientTag license libraryPath
 */  

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class MusicIdLookupMatchesText
    {
        private static string localDB = "../../../sample_db";
        /*  Online vs Local queries
          *	Set to false to have the sample perform online queries.
          *  Set to true to have the sample perform local queries.
          *  For local queries, a Gracenote local database must be present.
          */
        private const bool useLocal = false;
		private static GnUser user = null;

        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";

        #region Initializegnsdk

        /*Callback delegate called when loading locale*/
        public class LookupStatusEvents : GnStatusEventsDelegate
        {
            public static List<string> statusString = new List<string> { "Unknown", "Begin", "Progress", "Complete", "ErrorInfo", "Connecting", "Sending", "Recieving", "Disconnected", "Reading", "Writing" };

            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.WriteLine("\nLocale Load \tstatus :('" + statusString[Convert.ToInt32(status)] + "')" + "\tcomplete: " + percentComplete + "\tsent:" + bytesTotalSent +
                "\treceived:" + bytesTotalReceived);
            }

            public override bool cancel_check()
            {
                return false;
            }
        };

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class MusicIdEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.Write("\nPerforming MusicID Query ...\t");
                Console.Write("status (");
                switch (status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write(" Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write(" Begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write(" Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write(" Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write(" Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write(" Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write(" Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percentComplete + "),\tTotal Bytes Sent (" + bytesTotalSent + "),\tTotal Bytes Received (" + bytesTotalReceived + ")");
            }
        }
        
        /*
         *  Display local Gracenote DB information.
         */
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        static void LoadLocale(GnUser user)
        {
            using (LookupStatusEvents localeEvents = new LookupStatusEvents())
            {
                if (useLocal)
                {
                    GnLocale locale = new GnLocale(
                        GnSDK.kLocaleGroupMusic,            /* Locale group */
                        GnSDK.kLanguageEnglish,             /* Languae */
                        GnSDK.kRegionGlobal,                /* Region */
                        GnSDK.kDescriptorDefault,           /* Descriptor */
                        user,                               /* User */
                        null /*localeEvents*/               /* locale Events object */
                        );
                }
                else
                {
                    if (!File.Exists("serialized_locale.txt"))
                    {
                        GnLocale locale = new GnLocale(
                        GnSDK.kLocaleGroupMusic,            /* Locale group */
                        GnSDK.kLanguageEnglish,             /* Languae */
                        GnSDK.kRegionGlobal,                /* Region */
                        GnSDK.kDescriptorDefault,           /* Descriptor */
                        user,                               /* User */
                        null /*localeEvents*/               /* locale Events object */
                        );

                        /*Serialize locale, so we can use reuse it*/
                        if (!File.Exists("serialized_locale.txt"))
                        {
                            String serializedLocaleString = (locale.Serialize().ToString());
                            using (StreamWriter outfile = new StreamWriter("serialized_locale.txt"))
                            {
                                outfile.Write(serializedLocaleString);
                                outfile.Close();
                            }
                        }
                        return;
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader("serialized_locale.txt"))
                        {
                            String serializedLocaleString = sr.ReadToEnd();

                            GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                            return;
                        }
                    }
                }
            }
        }

        #endregion

        private static void displayAlbumMatches(GnResponseDataMatches gnResponseMatch)
        {
            GnDataMatchEnumerable dataMatchs = gnResponseMatch.DataMatches();

            //GnDataMatch dataMatch = null;
            {
                uint choiceOrdinal = 0;
                if (gnResponseMatch.NeedsDecision())
                {
                    choiceOrdinal = DoMatchSelection(gnResponseMatch);
                }
                else
                {
                    /* no need for disambiguation, we'll take the first album */
                    choiceOrdinal = 0;
                }
                GnDataMatch dataMatch = gnResponseMatch.DataMatches().at(choiceOrdinal).next();
                Console.WriteLine("    Final match:");
                if (dataMatch.IsAlbum())
                {
                    GnAlbum album = dataMatch.GetAsAlbum();
                    /* Is this a partial album? */
                    bool fullResult = album.FullResult();
                    if (!fullResult)
                    {
                        GnMusicID musicid = new GnMusicID(user);
                        /* do followup query to get full object. Setting the partial album as the query input. */
                        GnResponseAlbums gnResponse = musicid.FindAlbums(album);

                        /* now our first album is the desired result with full data */
                        album = gnResponse.Albums.at(0).next();
                    }
                    if (album != null)
                    {
                        GnTitle iGnTitle = album.Title;
                        Console.WriteLine("          Title: " + iGnTitle.Display);
                    }
                }
                else if (dataMatch.IsContributor())
                {
                    GnContributor contributor = dataMatch.GetAsContributor();
                    Console.WriteLine("          Title: " + contributor.NamesOfficial.at(choiceOrdinal).next().Display);
                }
            }
        }

        private static uint DoMatchSelection(GnResponseDataMatches gnResponseMatch)
        {
            /*
             * This is where any matches that need resolution/disambiguation are iterated
             * and a single selection of the best match is made.
             *
             * For this simplified sample, we'll just echo the matches and select the first match.
             */
            GnDataMatchEnumerable matchEnumerable = gnResponseMatch.DataMatches();
            uint matchCount = gnResponseMatch.DataMatches().count();
            Console.WriteLine("    Match count: " + matchCount);
            for (uint Ordinal = 0; Ordinal < matchCount; Ordinal++)
            {
                /* Just use the first match for demonstration. */
                GnDataMatch dataMatch = gnResponseMatch.DataMatches().at(Ordinal).next();
                if (dataMatch.IsAlbum())
                {
                    Console.WriteLine("          Title: " + dataMatch.GetAsAlbum().Title.Display);
                }
                else if (dataMatch.IsContributor())
                {
                    GnName name = dataMatch.GetAsContributor().NamesOfficial.at(0).next();
                    Console.WriteLine("          Title: " + name.Display);
                }
            }
            return 0;
        }

        /* This function performs a text lookup*/
        private static void DoSampleTextQuery(string albumTitle, string trackTitle, string artist, GnUser user)
        {
            try
            {
                Console.WriteLine("\n*****MusicID Text Match Query*****");
                using (GnStatusEventsDelegate midEvents = new MusicIdEvents())
                {
                    GnMusicID musicid = new GnMusicID(user, null);

                    if (albumTitle == null)
                        albumTitle = String.Empty;
                    else
                        Console.WriteLine("album title    : " + albumTitle);

                    if (trackTitle == null)
                        trackTitle = String.Empty;
                    else
                        Console.WriteLine("track title    : " + trackTitle);

                    if (artist == null)
                        artist = String.Empty;
                    else
                        Console.WriteLine("artist name    : " + artist);

                    /* Perform the query*/
                    GnResponseDataMatches gnResponseMatch = musicid.FindMatches(albumTitle, trackTitle, artist);
                    if (gnResponseMatch.DataMatches().count() == 0)
                    {
                        Console.WriteLine("\nNo matches found for the input.\n");
                    }
                    else
                    {
                        /*Get Album match explicitly*/
                        displayAlbumMatches(gnResponseMatch);
                    }
                }
            }
            catch (GnException e)
            {
                Console.WriteLine("Error Code           :: " + e.ErrorCode);
                Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                Console.WriteLine("Error API            :: " + e.ErrorAPI);
                Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                Console.WriteLine("SourceE rror Module  :: " + e.SourceErrorModule);
            }
        }

        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }
            Console.OutputEncoding = Encoding.UTF8;
            /* GNSDK initialization */
            try
            {
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */

                /*         
                 *    Load existing user handle, or register new one.                    
                 */
                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

                if (useLocal)
                {
                    GnStorageSqlite gnStorage = new GnStorageSqlite();
                    gnStorage.StorageFolder = localDB;

                    GnLookupLocal gnLookupLocal = new GnLookupLocal();

                    user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                    DisplayEmbeddedDbInfo(gnLookupLocal);
                }
                else
                {
                    user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                }

                /* Set locale with desired Group, Language, Region and Descriptor
                * Set the 'locale' to return locale-specifc results values. This examples loads an English locale.
                */
                LoadLocale(user);

                /* Perform a sample text query with Album, Track and Artist inputs */
                DoSampleTextQuery("Supernatural", "Africa Bamba", "Santana", user);

                /*	Perform a query with just the album name.	*/
                DoSampleTextQuery("看我72变", null, null, user);

                /*	Perform a sample text query with only the Artist name as an input.	*/
                DoSampleTextQuery(null, null, "Philip Glass", user);
                DoSampleTextQuery(null, null, "Bob Marley", user);

                /*	Perform a sample text query with Track Title and Artist name.	*/
                DoSampleTextQuery(null, "Purple Stain", "Red Hot Chili Peppers", user);
                DoSampleTextQuery(null, "Eyeless", "Slipknot", user);
            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }

    }
}
