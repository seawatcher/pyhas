/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnVideoSeries extends GnDataObject {
  private long swigCPtr;

  protected GnVideoSeries(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnVideoSeries_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnVideoSeries obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnVideoSeries(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnVideoSeries() {
    this(gnsdk_javaJNI.new_GnVideoSeries__SWIG_0(), true);
  }

  public GnVideoSeries(String id, String id_tag) {
    this(gnsdk_javaJNI.new_GnVideoSeries__SWIG_1(id, id_tag), true);
  }

  public boolean fullResult() {
    return gnsdk_javaJNI.GnVideoSeries_fullResult(swigCPtr, this);
  }

  public String gnID() {
    return gnsdk_javaJNI.GnVideoSeries_gnID(swigCPtr, this);
  }

  public String gnUID() {
    return gnsdk_javaJNI.GnVideoSeries_gnUID(swigCPtr, this);
  }

  public String productID() {
    return gnsdk_javaJNI.GnVideoSeries_productID(swigCPtr, this);
  }

  public String TUI() {
    return gnsdk_javaJNI.GnVideoSeries_TUI(swigCPtr, this);
  }

  public String TUITag() {
    return gnsdk_javaJNI.GnVideoSeries_TUITag(swigCPtr, this);
  }

  public String videoProductionType() {
    return gnsdk_javaJNI.GnVideoSeries_videoProductionType(swigCPtr, this);
  }

  public int videoProductionTypeID() {
    return gnsdk_javaJNI.GnVideoSeries_videoProductionTypeID(swigCPtr, this);
  }

  public String dateOriginalRelease() {
    return gnsdk_javaJNI.GnVideoSeries_dateOriginalRelease(swigCPtr, this);
  }

  public int duration() {
    return gnsdk_javaJNI.GnVideoSeries_duration(swigCPtr, this);
  }

  public String durationUnits() {
    return gnsdk_javaJNI.GnVideoSeries_durationUnits(swigCPtr, this);
  }

  public int franchiseNum() {
    return gnsdk_javaJNI.GnVideoSeries_franchiseNum(swigCPtr, this);
  }

  public int franchiseCount() {
    return gnsdk_javaJNI.GnVideoSeries_franchiseCount(swigCPtr, this);
  }

  public String plotSynopsis() {
    return gnsdk_javaJNI.GnVideoSeries_plotSynopsis(swigCPtr, this);
  }

  public String plotTagline() {
    return gnsdk_javaJNI.GnVideoSeries_plotTagline(swigCPtr, this);
  }

  public String plotSynopsisLanguage() {
    return gnsdk_javaJNI.GnVideoSeries_plotSynopsisLanguage(swigCPtr, this);
  }

  public String serialType() {
    return gnsdk_javaJNI.GnVideoSeries_serialType(swigCPtr, this);
  }

  public String workType() {
    return gnsdk_javaJNI.GnVideoSeries_workType(swigCPtr, this);
  }

  public String audience() {
    return gnsdk_javaJNI.GnVideoSeries_audience(swigCPtr, this);
  }

  public String videoMood() {
    return gnsdk_javaJNI.GnVideoSeries_videoMood(swigCPtr, this);
  }

  public String storyType() {
    return gnsdk_javaJNI.GnVideoSeries_storyType(swigCPtr, this);
  }

  public String reputation() {
    return gnsdk_javaJNI.GnVideoSeries_reputation(swigCPtr, this);
  }

  public String scenario() {
    return gnsdk_javaJNI.GnVideoSeries_scenario(swigCPtr, this);
  }

  public String settingEnvironment() {
    return gnsdk_javaJNI.GnVideoSeries_settingEnvironment(swigCPtr, this);
  }

  public String settingTimePeriod() {
    return gnsdk_javaJNI.GnVideoSeries_settingTimePeriod(swigCPtr, this);
  }

  public String source() {
    return gnsdk_javaJNI.GnVideoSeries_source(swigCPtr, this);
  }

  public String style() {
    return gnsdk_javaJNI.GnVideoSeries_style(swigCPtr, this);
  }

  public String topic() {
    return gnsdk_javaJNI.GnVideoSeries_topic(swigCPtr, this);
  }

  public GnGenre genre() {
    return new GnGenre(gnsdk_javaJNI.GnVideoSeries_genre(swigCPtr, this), true);
  }

  public GnOrigin origin() {
    return new GnOrigin(gnsdk_javaJNI.GnVideoSeries_origin(swigCPtr, this), true);
  }

  public GnRating rating() {
    return new GnRating(gnsdk_javaJNI.GnVideoSeries_rating(swigCPtr, this), true);
  }

  public GnTitle officialTitle() {
    return new GnTitle(gnsdk_javaJNI.GnVideoSeries_officialTitle(swigCPtr, this), true);
  }

  public GnTitle franchiseTitle() {
    return new GnTitle(gnsdk_javaJNI.GnVideoSeries_franchiseTitle(swigCPtr, this), true);
  }

  public GnExternalIDIterable externalIDs() {
    return new GnExternalIDIterable(gnsdk_javaJNI.GnVideoSeries_externalIDs(swigCPtr, this), true);
  }

  public GnVideoWorkIterable works() {
    return new GnVideoWorkIterable(gnsdk_javaJNI.GnVideoSeries_works(swigCPtr, this), true);
  }

  public GnVideoProductIterable products() {
    return new GnVideoProductIterable(gnsdk_javaJNI.GnVideoSeries_products(swigCPtr, this), true);
  }

  public GnVideoSeasonIterable seasons() {
    return new GnVideoSeasonIterable(gnsdk_javaJNI.GnVideoSeries_seasons(swigCPtr, this), true);
  }

  public GnVideoCreditIterable videoCredits() {
    return new GnVideoCreditIterable(gnsdk_javaJNI.GnVideoSeries_videoCredits(swigCPtr, this), true);
  }

}
