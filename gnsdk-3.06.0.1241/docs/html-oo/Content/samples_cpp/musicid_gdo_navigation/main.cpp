/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: Music response data object sample appilcation
 *  Description:
 *  This application uses MusicID to look up Album response data object content,	including
 *  Album	artist,	credits, title,	year, and genre.
 *  It demonstrates how to navigate the album response that returns basic track information,
 *  including artist, credits, title, track	number,	and	genre.
 *  Notes:
 *  For clarity and simplicity error handling in not shown here.
 *  Refer "logging"	sample to learn	about GNSDK	error handling.
 *
 *  Command-line Syntax:
 *  sample client_id client_id_tag license library_path
 */

#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::musicid;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}
};


/*
 * Local function declarations
 */

static void
create_tab_string(
	gnsdk_uint32_t tab_index,
	gnsdk_char_t*  tab_string
	)
{
	gnsdk_uint32_t index = 0;

	for (index = 0; index < tab_index; index++)
	{
		strcat(tab_string, "\t");
	}

} 

static void display_value(gnsdk_cstr_t str_value_tag,gnsdk_cstr_t str_value,gnsdk_uint32_t tab_index)
{
	gnsdk_char_t   tab_string[256]    = "";
	create_tab_string(tab_index, tab_string);

	if(str_value != GNSDK_NULL)
	{
		printf("%s%s: %s\n", tab_string,str_value_tag,str_value);		

	}

}
static void
navigate_name_official(GnName& nameOfficial, gnsdk_uint32_t tab_index)
{
	gnsdk_char_t       tab_string[1024]  = "";
	create_tab_string(tab_index, tab_string);
	tab_index += 1;

	printf("%sName Official:\n", tab_string);
	display_value("Display",nameOfficial.Display(),tab_index);

}

static void
navigate_title_official(GnTitle& titleOfficial, gnsdk_uint32_t tab_index)
{
	gnsdk_char_t       tab_string[1024]  = "";
	create_tab_string(tab_index, tab_string);
	tab_index += 1;

	printf("%sTitle Official:\n", tab_string);
	display_value("Display",titleOfficial.Display(),tab_index);	

}

/*-----------------------------------------------------------------------------
 *  navigate_contributor
 */
static void
navigate_contributor( GnContributor contributor, gnsdk_uint32_t tab_index )
{
	gnsdk_char_t       tab_string[1024] = "";;

	create_tab_string( tab_index, tab_string );
	tab_index += 1;

	/* Navigate	Contributor	Object	*/
	printf("%sContributor:\n", tab_string);

	metadata::name_iterator it_name = contributor.NamesOfficial().begin();
	for (; it_name != contributor.NamesOfficial().end(); ++it_name)
	{		
		navigate_name_official(*it_name,tab_index);		
	} 

	/* Display origin levels from the contributor */
	GnOrigin origin = contributor.Origin();
	display_value("Origin Level 1",origin.Level1(),tab_index );
	display_value("Origin Level 2",origin.Level2(),tab_index );
	display_value("Origin Level 3",origin.Level3(),tab_index );
	display_value("Origin Level 4",origin.Level4(),tab_index );

	/* Display Era levels from the contributor */
	GnEra era = contributor.Era();
	display_value("Era Level 1",era.Level1(),tab_index );
	display_value("Era Level 2",era.Level2(),tab_index );
	display_value("Era Level 3",era.Level3(),tab_index );

	/* Display Artist type levels from the contributor */
	GnArtistType type = contributor.ArtistType();
	display_value("Artist Type	Level 1",type.Level1(),tab_index );
	display_value("Artist Type	Level 2",type.Level2(),tab_index );

	
}


/*-----------------------------------------------------------------------------
 *  navigate_credit
 */
static void
navigate_credit( GnArtist gnArtist, gnsdk_uint32_t tab_index )
{
	gnsdk_char_t       tab_string[1024]  = "";

	create_tab_string(tab_index, tab_string);
	tab_index += 1;

	printf("%sCredit:\n", tab_string);

	navigate_contributor(gnArtist.Contributor(), tab_index);
}


/*-----------------------------------------------------------------------------
 *  navigate_track
 */
static void
navigate_track( GnTrack track, gnsdk_uint32_t tab_index )
{
	gnsdk_char_t       tab_string[1024]  = "";

	create_tab_string(tab_index, tab_string);
	tab_index += 1;

	printf("%sTrack:\n", tab_string);

	display_value("Track TUI",track.TUI(),tab_index );

	display_value("Track Number",track.TrackNumber(),tab_index );
	
	/* Navigate	credit from track artist  */

	GnArtist artist = track.Artist();
	if(artist.native())
	{
		navigate_credit(track.Artist(), tab_index);
	}

	/* Navigate	Title Official from track  */	
	navigate_title_official(track.Title(),tab_index);


	display_value("Year",track.Year(),tab_index );

	/*Gnere levels from track*/
	GnGenre genre = track.Genre();
	display_value("Genre Level 1",genre.Level1(),tab_index );
	display_value("Genre Level 2",genre.Level2(),tab_index );
	display_value("Genre Level 3",genre.Level3(),tab_index );

}


/*-----------------------------------------------------------------------------
 *  navigate_album_response
 */
static void
navigate_album_response( GnResponseAlbums& response )
{
	gnsdk_uint32_t     tab_index     = 1;	
	char strBuffer [33];	

	printf("\n***Navigating	Result GDO***\n");

	printf("Album:\n");

	/* Get the album from the match	response */
	int albCount =  response.Albums().count();
	if(albCount == 0) return;

	GnAlbum album = response.Albums().at(0).next();

	display_value("Package Language",album.Language().Display(),tab_index );

	/* Navigate	the	credit artist from the album */ 		
	navigate_credit( album.Artist(), tab_index );

	/* Navigate	Title Official from album  */		
	navigate_title_official(album.Title(),tab_index);	

	/* Display album attributes  */	
	display_value("Year",album.Year(),tab_index );

	/*Display genre levels form album*/
	GnGenre genre = album.Genre();
	display_value("Genre Level 1",genre.Level1(),tab_index );
	display_value("Genre Level 2",genre.Level2(),tab_index );
	display_value("Genre Level 3",genre.Level3(),tab_index );

	display_value("Album Label",album.Label(),tab_index );

	gnstd::gn_itoa(strBuffer,33,album.TotalInSet());
	display_value("Total in Set",strBuffer,tab_index );

	gnstd::gn_itoa(strBuffer,33,album.DiscInSet());
	display_value("Disc in Set",strBuffer,tab_index );		

	printf("\tTrack Count: %d\n",album.Tracks().count());

	metadata::track_iterator it_track = album.Tracks().begin();
	for (; it_track != album.Tracks().end(); ++it_track)
	{
		navigate_track( *it_track, tab_index );
	}

}


/*-----------------------------------------------------------------------------
 *  do_album_tui_lookup
 */
static void
do_album_tui_lookup( gnsdk_cstr_t inputTuiId, gnsdk_cstr_t inputTuiTag, GnUser& user )
{
	printf("\n*****Sample MusicID Query*****\n");

	/*LookupStatusEvents statusEvents;*/
	GnMusicID musicid(user,GNSDK_NULL);/*use &statusEvents instead of GNSDK_NULL to view progress*/

	GnAlbum albObj(inputTuiId,inputTuiTag);

	GnResponseAlbums albResponse= musicid.FindAlbums(albObj);
	int albCount =  albResponse.Albums().count();
	if(albCount > 0)
	{
		/* Match */
		printf("Match.\n");
		/* Get first album GDO and check if	it's a partial album */
		GnAlbum album = albResponse.Albums().at(0).next();
		/* Is this a partial album? */
		bool fullResult = album.FullResult();
		if(!fullResult)
		{
			printf("retrieving FULL	RESULT\n");
			/* Query for this match	in full	*/
			GnResponseAlbums full_alb_response = musicid.FindAlbums((GnDataObject)album);

			/* Full	album match	retrieved. Now we navigate the full	album GDO */ 		
			navigate_album_response(full_alb_response);						

		}else
		{
			navigate_album_response(albResponse);	
		}

	}else
	{
		/* No Matches */
		printf("No matches.\n");
	}

}


/*-----------------------------------------------------------------------------
 *  do_tui_lookups
 */
static void
do_sample_tui_lookups( GnUser& user )
{
	/* Lookup album: Nelly -	Nellyville to demonstrate collaborative artist navigation in track level (track#12)*/
	gnsdk_cstr_t inputTuiId  = "30716057";
	gnsdk_cstr_t inputTuiTag = "BB402408B507485074CC8B3C6D313616";


	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Dido -	Life for Rent */
	inputTuiId  = "3020551";
	inputTuiTag = "CAA37D27FD12337073B54F8E597A11D3";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Jean-Pierre Rampal	- Portrait Of Rampal */
	inputTuiId  = "2971440";
	inputTuiTag = "7F6C280498E077330B1732086C3AAD8F";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Various Artists - Grieg: Piano	Concerto, Peer Gynth Suites	#1 */
	inputTuiId  = "2971440";
	inputTuiTag = "7F6C280498E077330B1732086C3AAD8F";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Stephen Kovacevich	- Brahms: Rhapsodies, Waltzes &	Piano Pieces*/
	inputTuiId  = "2972852";
	inputTuiTag = "EC246BB5B359D88BEBDC1EF55873311E";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Nirvana - Nevermind */
	inputTuiId  = "2897699";
	inputTuiTag = "2FAE8F59CCECBA288810EC27DCD56A0A";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup album: Eminem	- Encore */
	inputTuiId  = "68056434";
	inputTuiTag = "C6E3634DF05EF343E3D22CE3A28A901A";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup Japanese album:
	 * NOTE: In order to correctly see the Japanese metadata results for
	 * this lookup, this program will need to write out to UTF-8
	 */
	inputTuiId  = "16391605";
	inputTuiTag = "F272BD764FDEB344A54F53D0756DC3FD";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);

	/* Lookup Chinese album:
	 * NOTE: In order to correctly see the Chinese metadata results for this
	 * lookup, this program will need to write out to UTF-8
	 */
	inputTuiId  = "3798282";
	inputTuiTag = "6BF6849840A77C987E8D3AF675129F33";
	do_album_tui_lookup(inputTuiId, inputTuiTag, user);
}


#if USE_LOCAL
static void display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  gdb_version = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);	
	printf("Gracenote DB Version : %s\n", gdb_version);
}
#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	/*LookupStatusEvents localeEvents;*/

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, GNSDK_NULL);/*use &statusEvents instead of GNSDK_NULL to view progress*/

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main( int argc, char* argv[] )
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";  /* Increment with each version of your app */
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		printf("\nUsage:\n%s clientId clientIdTag license gnsdkLibraryPath\n", argv[0]);
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",
		    GNSDK_LOG_PKG_ALL,
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,
		    GNSDK_LOG_OPTION_ALL,
		    0,                                                          /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                                 /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		user.OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		do_sample_tui_lookups( user );
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}
}

