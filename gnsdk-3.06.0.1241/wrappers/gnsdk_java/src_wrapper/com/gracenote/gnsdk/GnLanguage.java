/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnLanguage extends GnDataObject {
  private long swigCPtr;

  protected GnLanguage(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnLanguage_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnLanguage obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnLanguage(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnLanguage() {
    this(gnsdk_javaJNI.new_GnLanguage(), true);
  }

  public String display() {
    return gnsdk_javaJNI.GnLanguage_display(swigCPtr, this);
  }

  public String code() {
    return gnsdk_javaJNI.GnLanguage_code(swigCPtr, this);
  }

}
