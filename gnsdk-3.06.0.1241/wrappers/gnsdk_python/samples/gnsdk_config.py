# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Configuration data for GNSDK python samples.
Set these to override defaults in gnskd_sample_utilities.py

"""
import os

use_local = False
enable_logging = True
#storage_path = os.path.join("..", "..", "..", "..", "gdb-NA-0010-1.1.1.210")
