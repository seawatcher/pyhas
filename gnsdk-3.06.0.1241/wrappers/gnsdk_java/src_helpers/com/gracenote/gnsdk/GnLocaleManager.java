package com.gracenote.gnsdk;


import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.gracenote.gnsdk.GnException;
import com.gracenote.gnsdk.GnLocale;
import com.gracenote.gnsdk.GnUser;
import com.gracenote.gnsdk.GnKeyValueStore;


public class GnLocaleManager {

	GnSDK gnsdk;
	GnUser user;
	volatile boolean localeDirty;
	private boolean GNSDKERR_ListUpdateNeeded;
	private long updateCheckPeriod; //time of last update check, millisecs since epoch
	private long updateCheckPeriod_rollback;
	private HashMap<String,GnMsdkLocale> managedLocales; // <localeIDs, GnLocale_Wrappers>
	
	private static final String UpdateCheckPeriod_key = "time_locales_last_updated";
	private static final long updateCheckExpiry = 14 * 24 * 60 * 60 * 1000; //14 days,  millisecs
	private ExecutorService threadPool;
		
	public GnLocaleManager(GnSDK gnsdk, GnUser user){
		this.gnsdk = gnsdk;
		this.user = user;
		managedLocales = new HashMap<String,GnMsdkLocale>();
		localeDirty = false;
		GNSDKERR_ListUpdateNeeded = false;
		
		String lastUpdateTimeMillis = GnKeyValueStore.read(UpdateCheckPeriod_key);
		try{
		updateCheckPeriod = lastUpdateTimeMillis==null? -1:Long.parseLong(lastUpdateTimeMillis);
		} catch (NumberFormatException e){
			updateCheckPeriod = -1;
		}
		threadPool = Executors.newCachedThreadPool();
		
		/* PATDF do this outside of GnLocaleManager
		GnStorageSqlite sqliteStorage;
		try {
			String gn_list_gdb_path = Environment.getExternalStorageDirectory() + "/gracenote/lists";
			File gdbDir = new File(gn_list_gdb_path);
			if (!gdbDir.exists()) {
				gdbDir.mkdirs();
			}
			if (gdbDir.isDirectory()) {
				sqliteStorage = new GnStorageSqlite();
				sqliteStorage.storageFolderSet(Environment
						.getExternalStorageDirectory() + "/gracenote/lists");
			}

		} catch (GnException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		
	}
	
	/**
	 * used by GnSdk to indicate list update needed
	 */
	public void localeUpdateNeeded(){
		GNSDKERR_ListUpdateNeeded = true;
	}
	
	public void addLocale(String igroup, String ilanguage, String iregion,
			String idescriptor, boolean groupDefault) throws GnException {
		try {
			String group = formatString(igroup),
				   language = formatString(ilanguage),
				   region = formatString(iregion),
				   descriptor = formatString(idescriptor);

			String localeID = group + language + region + descriptor;

			if (!managedLocales.containsKey(localeID)) {
				GnMsdkLocale locale = new GnMsdkLocale(group, language, region,
						descriptor, groupDefault);
				managedLocales.put(localeID, locale);
				localeDirty = true;
			}

		} catch (NullPointerException e) {
			throw new GnException(); // todo: get params for GnException
		}
	}
	
	public void removeLocale(String igroup, String ilanguage, String iregion, String idescriptor){	
		
		String group = formatString(igroup),
				   language = formatString(ilanguage),
				   region = formatString(iregion),
				   descriptor = formatString(idescriptor);
		
		String localeID = group + language + region + descriptor;
		GnMsdkLocale mlocale = managedLocales.get(localeID);
		mlocale.unload();
		managedLocales.remove(localeID);
		//localeDirty = true;
		//remove from sqlite, compact db
		
	}
		
	public void localeCheck(){
				
		boolean updateCheck = false;
		if (GNSDKERR_ListUpdateNeeded || updateCheckPeriod < 0
				|| (System.currentTimeMillis() - updateCheckPeriod) >= updateCheckExpiry){
			updateCheck = true;
			GNSDKERR_ListUpdateNeeded = false;
			updateCheckPeriod_rollback = updateCheckPeriod;
			updateCheckPeriod = System.currentTimeMillis(); 
			try {
				GnKeyValueStore.write(UpdateCheckPeriod_key, String.valueOf(updateCheckPeriod)); //will rollback if update unsuccessful
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("updateCheck: " + updateCheck);
		// load locales and update if needed
		if (localeDirty || updateCheck) {
			localeDirty = false; //will be reverted to true when a load is unsuccessful
			for (Map.Entry<String, GnMsdkLocale> entry : managedLocales.entrySet()) {
				GnMsdkLocale mLocale = entry.getValue();
				threadPool.execute(mLocale.getLoadAndUpdateRunnable(updateCheck));
			}
		}		
						
		
	}
	
	
	//td: debug.log exceptions
	private class GnMsdkLocale{
		
		GnLocale gn_locale = null;
		String group;
		String language;
		String region;
		String descriptor; 
		boolean groupDefault;

		
		GnMsdkLocale(String group, String language, String region, String descriptor, boolean groupDefault){
			this.group = group;
			this.language = language;
			this.region = region;
			this.descriptor = descriptor;
			this.groupDefault = groupDefault;
		}
		
		
		void unload() {
			synchronized (gn_locale) {
				if (gn_locale != null) {
					//gn_locale.delete(); //releases native memory, shoudln't need to call this, todo: load test, verify mem usage
					gn_locale = null;
				}
			
			}
		}
					
		
		loadAndUpdateLocaleRunnable getLoadAndUpdateRunnable(boolean updateCheck){
			return new loadAndUpdateLocaleRunnable(updateCheck);
		}

		private class loadAndUpdateLocaleRunnable implements Runnable {

			boolean updateCheck;

			loadAndUpdateLocaleRunnable(boolean updateCheck) {
				this.updateCheck = updateCheck;
			}
			
			
			@Override
			public void run() {

				// load locale

				try {

//					System.out.println("********load locale begins: "
//							+ language);
					gn_locale = new GnLocale(group, language, region,
							descriptor, user, null);
//					System.out.println("********load locale complete "
//							+ language);
//					System.out.println(gn_locale.language() + ", "
//							+ gn_locale.revision() + ", " + gn_locale.region());
					
					if ( groupDefault ) {
						gnsdk.setDefaultLocale( gn_locale );
					}

				} catch (GnException e) {
					e.printStackTrace();
					// System.out.println("failed to load locale: " +
					// e.getMessage() + ", " + e.getErrorAPI() + ", " +
					// e.getErrorCode());
					gn_locale = null;
					localeDirty = true;
				}

				// update locale, td: possibly unnecessary update check here if
				// locales have just been loaded from online
				if (updateCheck) {
					if (gn_locale != null) {
						try {
							if ((gn_locale.updateCheck(user))) {
//								System.out.println("****updating locale");
								gn_locale.update(user);
							} 
						} catch (Exception e) {
							e.printStackTrace();
							// System.out.println("failed to update locale");
							localeUpdateNeeded(); // re-try update in the next
													// pass
							updateCheckPeriod = updateCheckPeriod_rollback;
							try {
								GnKeyValueStore.write(UpdateCheckPeriod_key, String.valueOf(updateCheckPeriod));
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					}
				}

			}

		}

	}


	
	private String formatString(String inString){
		String resultString = null;
		if(inString != null){
			resultString = inString.toLowerCase(Locale.US).trim();
		}		
		return resultString;
	}
	
}
