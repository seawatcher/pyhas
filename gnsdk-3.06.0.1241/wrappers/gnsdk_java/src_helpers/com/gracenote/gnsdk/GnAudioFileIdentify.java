package com.gracenote.gnsdk;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GnAudioFileIdentify {

	private GnAudioFileIdentifyEventsListener 	listener;
	private List<GnAudioFile> 					files;
	private GnMusicIDFile 						musicIDFile;
	
	
	/**
	 * Identify the file, if a file path is provided, or all
	 * supported audio files recursively if a directory
	 * is provided 
	 * @param fileOrDirectory	File or directory for recognition
	 * @throws GnException 
	 */
	public GnAudioFileIdentify ( GnUser user, File fileOrDirectory, GnAudioFileIdentifyEventsListener listener ) throws GnException {
		this.listener = listener;
		
		if ( fileOrDirectory.isFile() ){
			files = new LinkedList<GnAudioFile>();
			files.add( new GnAudioFile( fileOrDirectory ) );
		} else {
			files = getFilesFromDirectory( fileOrDirectory );
		}
	}
	
	/**
	 * Identify the provided audio file
	 * @param file
	 * @throws GnException 
	 */
	public GnAudioFileIdentify ( GnUser user, GnAudioFile file, GnAudioFileIdentifyEventsListener listener ) throws GnException {
		this.listener = listener;
		files = new LinkedList<GnAudioFile>();
		files.add( file );
	}
	
	/**
	 * Identify the provided audio files
	 * @param file
	 * @throws GnException 
	 */
	public GnAudioFileIdentify ( GnUser user, List<GnAudioFile> files, GnAudioFileIdentifyEventsListener listener ) throws GnException {
		this.listener = listener;
		this.files = files;
	}
	
	/**
	 * Identify audio files using Gracenote Track ID
	 * @param user
	 * @throws GnException
	 */
	public void IdentifyWithTrackID ( GnUser user ) throws GnException {
		
		musicIDFile = new GnMusicIDFile( user, new MusicIDFileEvents( listener ) );
		
		addFilesToMusicIDFile( musicIDFile, files );
		
		musicIDFile.doTrackID( 0 ); // PATDF TODO: zero isn't what we want here, need to resolve query flags
	}

	/**
	 * Identify audio files using Gracenote Album ID
	 * @param user
	 * @throws GnException
	 */
	public void IdentifyWithAlbumID ( GnUser user ) throws GnException {
		
		musicIDFile = new GnMusicIDFile( user, new MusicIDFileEvents( listener ) );
		
		addFilesToMusicIDFile( musicIDFile, files );
		
		musicIDFile.doAlbumID( 0 ); // PATDF TODO: zero isn't what we want here, need to resolve query flags
	}
	
	/**
	 * Identify audio files using Gracenote Library ID
	 * @param user
	 * @param files
	 * @throws GnException
	 */
	public void IdentifyWithLibraryID ( GnUser user ) throws GnException {
		
		musicIDFile = new GnMusicIDFile( user, new MusicIDFileEvents( listener ) );
		
		addFilesToMusicIDFile( musicIDFile, files );
		
		musicIDFile.doLibraryID( 0 ); // PATDF TODO: zero isn't what we want here, need to resolve query flags
	}
	
	
	private class MusicIDFileEvents extends GnMusicIDFileEventsListener {
		
		private GnAudioFileIdentifyEventsListener listener;
		
		MusicIDFileEvents( GnAudioFileIdentifyEventsListener listener ){
			this.listener = listener;
		}
		
		@Override
		public void status(GnMusicIDFileInfo fileinfo, GnMusicIdFileCallbackStatus midf_status, long current_file, long total_files) {
		
		}

		@Override
		public boolean cancelCheck() {
			return false;
		}

		@Override
		public void getFingerprint(GnMusicIDFileInfo fileinfo, long current_file, long total_files) {
			try {
				fileinfo.fingerprintFromSource( files.get((int)current_file) );
			} catch (GnException e) {
				// ignore
			}
		}

		@Override
		public void getMetadata(GnMusicIDFileInfo fileinfo, long current_file, long total_files) {
			// TODO: Integrate with interface to get metadata
		}

		@Override
		public void resultAvailable(GnResponseAlbums album_result, long current_album, long total_albums) {
			listener.resultAvailable(album_result, current_album, total_albums);
		}

		@Override
		public void resultAvailable(GnResponseDataMatches matches_result, long current_album, long total_albums) {
			listener.resultAvailable(matches_result, current_album, total_albums);
		}

		@Override
		public void resultNotFound(GnMusicIDFileInfo fileinfo, long current_file, long total_files) {
			listener.resultNotFound(fileinfo, current_file, total_files);
		}

		@Override
		public void complete(long musicidfile_complete_error) {
			listener.complete(musicidfile_complete_error);
		}
	}
	
	private void addFilesToMusicIDFile ( GnMusicIDFile locMusicIDFile, List<GnAudioFile> locFiles ) throws GnException{
		if ( null != locFiles ) {
			Iterator<GnAudioFile> iter = locFiles.iterator();
			
			while ( iter.hasNext() ) {
				locMusicIDFile.createFileInfo( iter.next().filename() );
			}
		}
	}
	
	private List<GnAudioFile> getFilesFromDirectory( File directory ) {
		ArrayList<GnAudioFile> filepathList = new ArrayList<GnAudioFile>();
		getdirectory(directory.getAbsolutePath(), filepathList);
		return filepathList;
	}

	private void getdirectory(String path, List<GnAudioFile> filepathList) {
		File sdcard = new File(path);
		if (sdcard.exists()) {
			File[] filenames = sdcard.listFiles();
			for (int i = 0; i < filenames.length; i++) {
				if (filenames[i].isDirectory())
				{
					getdirectory(filenames[i].getPath(), filepathList);
				}
				else
				{
					String audioFilename = filenames[i].getPath();
					if ( GnAudioFile.isFileFormatSupported(audioFilename)) {
						filepathList.add(new GnAudioFile(new File(filenames[i].getPath())));
					}
				}
			}
		}
	}

}
