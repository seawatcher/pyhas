/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnDsp : GnObject {
  private HandleRef swigCPtr;

  internal GnDsp(IntPtr cPtr, bool cMemoryOwn) : base(gnsdk_csharp_marshalPINVOKE.GnDsp_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnDsp obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnDsp() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnDsp(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GnDsp(GnUser user, GnDspFeatureType featureType, uint audioSampleRate, uint audioSampleSize, uint audioChannels) : this(gnsdk_csharp_marshalPINVOKE.new_GnDsp(GnUser.getCPtr(user), (int)featureType, audioSampleRate, audioSampleSize, audioChannels), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public bool FeatureAudioWrite(byte[] audioData, uint audioDataBytes) {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnDsp_FeatureAudioWrite(swigCPtr, audioData, audioDataBytes);
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void FeatureEndOfAudioWrite() {
    gnsdk_csharp_marshalPINVOKE.GnDsp_FeatureEndOfAudioWrite(swigCPtr);
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnDspFeature FeatureRetrieve() {
    IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnDsp_FeatureRetrieve(swigCPtr);
    GnDspFeature ret = (cPtr == IntPtr.Zero) ? null : new GnDspFeature(cPtr, true);
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public string Version {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnDsp_Version_get(swigCPtr) );
	} 

  }

  public string BuildDate {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnDsp_BuildDate_get(swigCPtr) );
	} 

  }

}

}
