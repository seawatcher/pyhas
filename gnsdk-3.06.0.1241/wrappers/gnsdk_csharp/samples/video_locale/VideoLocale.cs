/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoLocale
 *  Description:
 *  This sample shows basic access of locale-dependent fields
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoLocale
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
        private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                return;  // Remove to see status messages
                Console.Write("\nPerforming Video locale ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoLookupEvents localeEvents = new VideoLookupEvents())
            {
                if (!File.Exists("serialized_video_locale.txt"))
                {                  
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLoclaeString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLoclaeString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DoWorkSearch(GnUser user)
        {
            string value = null;
            string searchText = "Harrison Ford";
            string serializedGDO = "WEcxA6R75JwbiGUIxLFZHBr4tv+bxvwlIMr0XK62z68zC+/kDDdELzwiHmBPkmOvbB4rYEY/UOOvFwnk6qHiLdb1iFLtVy44LfXNsTH3uNgYfSymsp9uL+hyHfrzUSwoREk1oX/rN44qn/3NFkEYa2FoB73sRxyRkfdnTGZT7MceHHA/28aWZlr3q48NbtCGWPQmTSrK";

            Console.WriteLine("\n*****Sample Video Work Search*****");

            using (GnStatusEventsDelegate videoEvents = new VideoLookupEvents())
            {

                GnVideo video = new GnVideo(user, null /*videoEvents*/);
                GnDataObject inpupdataobject = new GnDataObject(serializedGDO);

                //GnResponseVideoWork videoWorksResponse = video.FindWorks(searchText, GnVideoSearchField.kSearchFieldContributorName, GnVideoSearchType.kSearchTypeDefault);
                GnResponseVideoWork videoWorksResponse = video.FindWorks(inpupdataobject);

                Console.WriteLine("\n\nNumber matches: " + videoWorksResponse.Works.count());

                /**********************************************
			     *  Needs decision (match resolution) check
			     **********************************************/
                if (videoWorksResponse.NeedsDecision)
                {
                    /**********************************************
				     * Resolve match here
				     **********************************************/
                }

                if (videoWorksResponse.Works.count() > 0)
                {
                    GnVideoWorkEnumerable videoWorkEnumerable = videoWorksResponse.Works;

                    foreach (GnVideoWork work in videoWorkEnumerable)
                    {


                        /*Work title*/
                        GnTitle workTitle = work.OfficialTitle;
                        value = workTitle.Display;
                        if (value != null && value != "")
                            Console.WriteLine("\nTitle: " + value);

                        /*Origin info*/
                        GnOrigin origin = work.Origin;
                        value = origin.Level1;
                        if (value != null && value != "")
                            Console.WriteLine("\nOrigin1: " + value);
                        value = origin.Level2;
                        if (value != null && value != "")
                            Console.WriteLine("\nOrigin2: " + value);
                        value = origin.Level3;
                        if (value != null && value != "")
                            Console.WriteLine("\nOrigin3: " + value);
                        value = origin.Level4;
                        if (value != null && value != "")
                            Console.WriteLine("\nOrigin4: " + value);

                        /*Genre info*/
                        GnGenre genre = work.Genre;
                        value = genre.Level1;
                        if (value != null && value != "")
                            Console.WriteLine("\nGenre1: " + value);
                        value = genre.Level2;
                        if (value != null && value != "")
                            Console.WriteLine("\nGenre2: " + value);
                        value = genre.Level3;
                        if (value != null && value != "")
                            Console.WriteLine("\nGenre3: " + value);

                        /*Video mood
                        value = work.VideoMood;
                        if (value != null && value != "")
                            Console.WriteLine("\nMood: " + value);*/

                        /* Primary rating */
                        GnRating rating = work.Rating();
                        value = rating.Rating;
                        if (value != null && value != "")
                        {
                            Console.Write("\nRating: " + value);

                            value = rating.RatingType;
                            if (value != null && value != "")
                                Console.Write(" [" + value + "]");

                            value = rating.RatingDesc();
                            if (value != null && value != "")
                                Console.WriteLine(" - " + value);

                            value = rating.RatingReason;
                            if (value != null && value != "")
                                Console.WriteLine(" Reason: " + value);

                            Console.WriteLine();
                        }

                        value = work.PlotSynopsis;
                        if (value != null && value != "")
                            Console.WriteLine("Plot synopsis: " + work.PlotSynopsis);

                        GnVideoCreditEnumerable videoCreditEnumerable = work.VideoCredits;

                        foreach (GnVideoCredit credit in videoCreditEnumerable)
                        {
                            /* Display name  
                            GnName name = credit.OfficialName;
                            value = name.Display;
                            if (value != null && value != "")
                                Console.WriteLine("\nName: " + value);*/

                            /* Genres */
                            GnGenre creditGenre = credit.Genre;
                            value = creditGenre.Level1;
                            if (value != null && value != "")
                                Console.WriteLine("\n\tGenre1: " + value);
                            value = creditGenre.Level2;
                            if (value != null && value != "")
                                Console.WriteLine("\n\tGenre2: " + value);
                            value = creditGenre.Level3;
                            if (value != null && value != "")
                                Console.WriteLine("\n\tGenre3: " + value);

                        }
                    }
                }
            }
        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Lookup AV Works and display */
                        DoWorkSearch(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }
    }
}
