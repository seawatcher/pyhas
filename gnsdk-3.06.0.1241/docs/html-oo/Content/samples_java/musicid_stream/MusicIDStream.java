/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: MusicIDStream
 *  Description:
 *  This example uses MusicID-Stream to fingerprint and identify a music track.
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license path lib path
 */

package musicid_stream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class MusicIDStream {

	/*
	 * Set USELOCAL to false to have the sample perform online queries.
	 * Set USELOCAL to true to have the sample perform local queries.
	 * For local queries, a Gracenote local database must be present.
	 */
	private final static String key = "../../../sample_data/05-Hummingbird-sample.wav";
	
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		// Checked licence file exit .
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}
	
	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				localeEvents
				);

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}
		
	private static void setFingerprintBeginWriteEnd(
			GnMusicID gnMusicID
			) throws GnException, IOException {
		/* get the file */
		File file = new File(key);
		/*
		 * initialize the fingerprinter Note: Our sample files are non-standard
		 * 11025 Hz 16-bit mono to save on file size
		 */
		gnMusicID.fingerprintBegin(
				GnFingerprintType.kFingerprintTypeGNFPX, 
				44100, 
				16, 
				2
				);
		
		FileInputStream iStream = new FileInputStream(file);

		byte audioData[] = new byte[2048 * 2];
		int offset = 0;
		int numRead = 0;
		boolean fingerPrintWrite = false;

		while (offset < iStream.available()
				&& (numRead = iStream.read(audioData, 0, audioData.length)) >= 0
				&& fingerPrintWrite == false) {
			offset += numRead;
			
			/* write audio to the finger printer */
			fingerPrintWrite = gnMusicID.fingerprintWrite(audioData, audioData.length);
			
			audioData = new byte[2048 * 2];
		}
		/* does the finger printer have enough audio? */
		gnMusicID.fingerprintEnd();
	}
	
	private static void musicidFingerprintAlbum(
			GnUser user
			) throws IOException, GnException {
		System.out.println("\n*****Sample MID-Stream Query*****");

		GnMusicID gnMusicID = new GnMusicID(user);

		setFingerprintBeginWriteEnd(gnMusicID);
		
		GnResponseAlbums gnResponseAlbums = gnMusicID.findAlbums(
				gnMusicID.fingerprintDataGet(), 
				GnFingerprintType.kFingerprintTypeGNFPX
				);

		/* Navigate album to examine the response */
		GnAlbumIterable gnAlbumIterable = gnResponseAlbums.albums();
		GnAlbumIterator gnAlbumIterator = gnAlbumIterable.getIterator();

		System.out.println("Final album:");
		
		while (gnAlbumIterator.hasNext()) {
			GnAlbum gnAlbum = (GnAlbum) gnAlbumIterator.next();
			System.out.println("\tTitle:\t" + gnAlbum.title().display());
		}
	}

	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
				
				user.optionLookupMode(GnLookupMode.kLookupModeOnline);

				loadLocale(gnsdk, user);
			
				musicidFingerprintAlbum(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
