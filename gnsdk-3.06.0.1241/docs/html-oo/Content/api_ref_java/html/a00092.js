var a00092 =
[
    [ "swigValue", "a00092.html#a5e5470511682853c1fdd91092cae691e", null ],
    [ "gnsdk_musicidfile_status_albumid_cancelled", "a00092.html#ad9821728b058a9cb79ddb916896ba4b3", null ],
    [ "gnsdk_musicidfile_status_albumid_cancelling", "a00092.html#a952b655cfa46ef69b646a7db138cadc1", null ],
    [ "gnsdk_musicidfile_status_albumid_complete", "a00092.html#aa95f13408323e8d1984253669ec07851", null ],
    [ "gnsdk_musicidfile_status_albumid_processing", "a00092.html#a8dff542cfbda880cadf6662f0eea3a23", null ],
    [ "gnsdk_musicidfile_status_libraryid_cancelled", "a00092.html#a00ad04974127d7a582fc58cfe56231b2", null ],
    [ "gnsdk_musicidfile_status_libraryid_cancelling", "a00092.html#ac36179b58917796a942e326705593d7e", null ],
    [ "gnsdk_musicidfile_status_libraryid_complete", "a00092.html#a7f40015b26d84dd8ac78ac17d997df95", null ],
    [ "gnsdk_musicidfile_status_libraryid_processing", "a00092.html#ac4d6ee2c90859f9b2edcbd8d062af601", null ],
    [ "gnsdk_musicidfile_status_trackid_cancelled", "a00092.html#a011fef2fbb9d312b74276e405194fded", null ],
    [ "gnsdk_musicidfile_status_trackid_cancelling", "a00092.html#a1750450ca519d6498a02c21398290c67", null ],
    [ "gnsdk_musicidfile_status_trackid_complete", "a00092.html#a8f516c73b77cf1e2331daa22422f3081", null ],
    [ "gnsdk_musicidfile_status_trackid_processing", "a00092.html#a0fc823ae673c75ef82047642ee9b19ce", null ],
    [ "gnsdk_musicidfile_status_unknown", "a00092.html#a537618a6e9de98b057287121918b216c", null ],
    [ "gnsdk_musicidfile_status_unprocessed", "a00092.html#a9f04cac6caa7fb8852e454b950b7e472", null ]
];