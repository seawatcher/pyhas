/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnVideoProductEnumerator : System.Collections.Generic.IEnumerator<GnVideoProduct>, IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnVideoProductEnumerator(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnVideoProductEnumerator obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnVideoProductEnumerator() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnVideoProductEnumerator(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

			public bool
			MoveNext( )
			{
				return hasNext( );
			}

			public GnVideoProduct Current {
				get {
					return next( );
				}
			}
			object System.Collections.IEnumerator.Current {
				get {
					return Current;
				}
			}
			public void
			Reset( )
			{
			}

		
  public GnVideoProduct next() {
    GnVideoProduct ret = new GnVideoProduct(gnsdk_csharp_marshalPINVOKE.GnVideoProductEnumerator_next(swigCPtr), true);
    return ret;
  }

  public bool hasNext() {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnVideoProductEnumerator_hasNext(swigCPtr);
    return ret;
  }

  public uint distance(GnVideoProductEnumerator itr) {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnVideoProductEnumerator_distance(swigCPtr, GnVideoProductEnumerator.getCPtr(itr));
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public GnVideoProductEnumerator(GnVideoProductProvider provider, uint pos, uint count) : this(gnsdk_csharp_marshalPINVOKE.new_GnVideoProductEnumerator__SWIG_0(GnVideoProductProvider.getCPtr(provider), pos, count), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnVideoProductEnumerator(GnVideoProductProvider provider, uint pos, uint count, GnVideoProduct start) : this(gnsdk_csharp_marshalPINVOKE.new_GnVideoProductEnumerator__SWIG_1(GnVideoProductProvider.getCPtr(provider), pos, count, GnVideoProduct.getCPtr(start)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
