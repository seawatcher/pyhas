var class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate =
[
    [ "EventsIdentiferStatus", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a9f22c578e2bcd228cd3f450cc62b43f3", [
      [ "kIdentifierStatusUnknown", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a9f22c578e2bcd228cd3f450cc62b43f3a37bc253c7ddab92f825c8395c8ca2a36", null ],
      [ "kIdentifierStatusNew", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a9f22c578e2bcd228cd3f450cc62b43f3a30146c50167269b2ea00ad2b1317b052", null ],
      [ "kIdentifierStatusOld", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a9f22c578e2bcd228cd3f450cc62b43f3a498f84d569bcdbecdbcbf27914cf1808", null ]
    ] ],
    [ "GnPlaylistCollectionSyncEventsDelegate", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a6d6ccdfee39d4de9b0bc0c874599b1ec", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#ab1d5270d8145f87cfb938494b49c5f9e", null ],
    [ "OnCancelCheck", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#acd6a9ce222b76475e5bd07e78ff667fe", null ],
    [ "OnUpdate", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a53905fcd1186a79cae814d44f684bf1e", null ],
    [ "SwigDelegateGnPlaylistCollectionSyncEventsDelegate_0", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a61116e7600f4a08f915507a0ad2ddf0c", null ],
    [ "SwigDelegateGnPlaylistCollectionSyncEventsDelegate_1", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a79868f0c844bf2e03ce0718fc2b04d86", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html#a4da56be6132687fbd7cf06ebee5bd8ff", null ]
];