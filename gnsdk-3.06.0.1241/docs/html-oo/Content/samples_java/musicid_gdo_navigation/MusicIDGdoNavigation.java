/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: MusicIDGdoNavigation sample application
 *  Description:
 *  This application uses MusicID to look up Album response data object content,	including
 *  Album	artist,	credits, title,	year, and genre.
 *  It demonstrates how to navigate the album response that returns basic track information,
 *  including artist, credits, title, track	number,	and	genre.
 *  Notes:
 *  For clarity and simplicity error handling in not shown here.
 *  Refer "logging"	sample to learn	about GNSDK	error handling.
 *
 *  Command-line Syntax:
 *  sample client_id client_id_tag license library_path
 */
package musicid_gdo_navigation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class MusicIDGdoNavigation {
	/*
	 * Set USELOCAL to false to have the sample perform online queries.
	 * Set USELOCAL to true to have the sample perform local queries.
	 * For local queries, Gracenote databases are required.
	 */
	private static final boolean USELOCAL = false;

	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		
		if (file.isFile()) {
			return true;
		}
		return false;
	}
	
	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}

	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				localeEvents
				);

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}
	
	private static void displayTrack(GnTrack gnTrack) {
		System.out.println("Track : ");
		System.out.println("\tTrack TUI : " + gnTrack.TUI());
		System.out.println("\tTrack Number : " + gnTrack.trackNumber());
		
		GnArtist artist = gnTrack.artist();
		
		GnContributor contributor = artist.contributor();
		if (contributor != null) {
			displayContributors(contributor);
		}
		
		System.out.println("\tTitle Official : ");
		System.out.println("\tDisplay : " + gnTrack.title().display());
		
		if (gnTrack.year() != null && gnTrack.year() != "") {
			System.out.println("\tYear : " + gnTrack.year());
		}
		if (gnTrack.title().sortable() != null && gnTrack.title().sortable().display() != "") {
			System.out.println("\tSortable : "+ gnTrack.title().sortable().display());
		}
	}

	private static void displayContributors(GnContributor gnContributor) {
		
		GnNameIterable nameIterable = gnContributor.namesOfficial();
		GnNameIterator nameIterator = nameIterable.getIterator();
		
		if (nameIterator.hasNext()) {
			System.out.println("Contributor :");
			System.out.println("\t\tName Official: ");
			
			while (nameIterator.hasNext()) {
				GnName gnName = nameIterator.next();
				System.out.println("\t\tDisplay : " + gnName.display());
			}

			if (gnContributor.origin().level1() != null && gnContributor.origin().level1() != "") {
				System.out.println("\t\t\tOrigin Level 1: "+ gnContributor.origin().level1());
			}
			
			if (gnContributor.origin().level2() != null && gnContributor.origin().level2() != "") {
				System.out.println("\t\t\tOrigin Level 2: "+ gnContributor.origin().level2());
			}
			
			if (gnContributor.origin().level3() != null && gnContributor.origin().level3() != "") {
				System.out.println("\t\t\tOrigin Level 3: "+ gnContributor.origin().level3());
			}
			
			if (gnContributor.origin().level4() != null && gnContributor.origin().level4() != "") {
				System.out.println("\t\t\tOrigin Level 4: "+ gnContributor.origin().level4());
			}
			
			if (gnContributor.era().level1() != null && gnContributor.era().level1() != "") {
				System.out.println("\t\t\tEra Level 1: "+ gnContributor.era().level1());
			}
			
			if (gnContributor.era().level2() != null && gnContributor.era().level2() != "") {
				System.out.println("\t\t\tEra Level 2: "+ gnContributor.era().level2());
			}
			
			if (gnContributor.era().level3() != null && gnContributor.era().level3() != "") {
				System.out.println("\t\t\tEra Level 3: "+ gnContributor.era().level3());
			}
			
			if (gnContributor.artistType().level1() != null && gnContributor.artistType().level1() != "") {
				System.out.println("\t\t\tArtist Type Level 1: "+ gnContributor.artistType().level1());
			}
			
			if (gnContributor.artistType().level2() != null && gnContributor.artistType().level2() != "") {
				System.out.println("\t\t\tArtist Type Level 2: "+ gnContributor.artistType().level2());
			}
		}
	}

	private static void displayResult(GnResponseAlbums gnResult) {
		GnAlbumIterable gnAlbumIterable = gnResult.albums();
		GnAlbumIterator gnAlbumIterator = gnAlbumIterable.getIterator();
		
		while (gnAlbumIterator.hasNext()) {
			GnAlbum gnAlbum = (GnAlbum) gnAlbumIterator.next();
		
			System.out.println("Album");
			System.out.println("\tPackage Language :"+ gnAlbum.language().display()); 
			
			GnArtist artist = gnAlbum.artist();
			GnContributor contributor = artist.contributor();
			
			if (contributor != null) {
				System.out.println("Credit : ");
			
				displayContributors(contributor);
			}
			
			GnTitle gnTitle = gnAlbum.title();
			
			System.out.println("Title Official : ");
			System.out.println("\t\tDisplay : " + gnTitle.display());
			
			if (gnTitle.sortable() != null && gnTitle.sortable().display() != "") {
				System.out.println("\t\tSortable : " + gnTitle.sortable().display());
			}
			
			if (gnAlbum.year() != null && gnAlbum.year() != "") {
				System.out.println("\tYear : " + gnAlbum.year());
			}
			
			if (gnAlbum.genre().level1() != null && gnAlbum.genre().level1() != "") {
				System.out.println("\tGenre Level 1 : "+ gnAlbum.genre().level1());
			}
			
			if (gnAlbum.genre().level2() != null && gnAlbum.genre().level2() != "") {
				System.out.println("\tGenre Level 2 : "+ gnAlbum.genre().level2());
			}
			
			if (gnAlbum.genre().level3() != null && gnAlbum.genre().level3() != "") {
				System.out.println("\tGenre Level 3 : "+ gnAlbum.genre().level3());
			}
			
			if (gnAlbum.label() != null && gnAlbum.label() != "") {
				System.out.println("\tAlbum Label : " + gnAlbum.label());
			}
			
			if (gnAlbum.totalInSet() != 0) {
				System.out.println("\tTotal in Set : " + gnAlbum.totalInSet());
			}
			
			if (gnAlbum.discInSet() != 0) {
				System.out.println("\tDisc in Set : " + gnAlbum.discInSet());
			}
			
			System.out.println("Track	Count: " + gnAlbum.trackCount());
			
			GnTrackIterable gnTrackIterable = gnAlbum.tracks();
			GnTrackIterator gnTrackIterator = gnTrackIterable.getIterator();
			
			while (gnTrackIterator.hasNext()) {
				GnTrack gnTrack = gnTrackIterator.next();
				
				displayTrack(gnTrack);
			}
		}
	}
	
	private static class MusicIDEvent extends GnStatusEventsListener {
		@Override
		public void statusEvent(GnStatus midmStatus,
								long percentComplete,
								long bytesTotalSent,
								long bytesTotalReceived){
			System.out.print("\n MusicID Match Query :" + midmStatus + "\t");
			
			System.out.println("\t% Complete (" + percentComplete + "),\n\tTotal Bytes Sent ("
					+ bytesTotalSent + "),\n\tTotal Bytes Received ("
					+ bytesTotalReceived+ ")");
		}
		
		@Override
		public boolean cancelCheck(){
			return false;
		}
	}
	
	private static void musicidLookupAlbum(
			String inputTuiId, 
			String inputTuiTag,
			GnUser user
			) throws IOException, GnException {
		System.out.println("*****Sample MusicID Result Navigation*****");
		
		MusicIDEvent musicIDEvent = new MusicIDEvent();
		
		GnMusicID musicid = new GnMusicID(user, musicIDEvent);
		
		GnDataObject gnid = new GnAlbum(inputTuiId, inputTuiTag);
		GnResponseAlbums gnResult = musicid.findAlbums(gnid);
		
		displayResult(gnResult);
	}	
	
	private static void doSampleTuiLookups(GnUser user) throws IOException, GnException {
		/*
		 * Lookup album: Nelly - Nellyville to demonstrate collaborative artist
		 * navigation in track level (track#12)
		 */
		String inputTuiId = "30716057";
		String inputTuiTag = "BB402408B507485074CC8B3C6D313616";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/* Lookup album: Dido - Life for Rent */
		inputTuiId = "46508189";
		inputTuiTag = "951407B37F9D8EAE68F74B0B5C5E1224";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/* Lookup album: Jean-Pierre Rampal - Portrait Of Rampal */
		inputTuiId = "3020551";
		inputTuiTag = "CAA37D27FD12337073B54F8E597A11D3";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/*
		 * Lookup album: Various Artists - Grieg: Piano Concerto, Peer Gynth
		 * Suites #1
		 */
		inputTuiId = "2971440";
		inputTuiTag = "7F6C280498E077330B1732086C3AAD8F";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/*
		 * Lookup album: Stephen Kovacevich - Brahms: Rhapsodies, Waltzes &
		 * Piano Pieces
		 */
		inputTuiId = "2972852";
		inputTuiTag = "EC246BB5B359D88BEBDC1EF55873311E";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/* Lookup album: Nirvana - Nevermind */
		inputTuiId = "2897699";
		inputTuiTag = "2FAE8F59CCECBA288810EC27DCD56A0A";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/* Lookup album: Eminem - Encore */
		inputTuiId = "68056434";
		inputTuiTag = "C6E3634DF05EF343E3D22CE3A28A901A";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/*
		 * Lookup Japanese album: NOTE: In order to correctly see the Japanese
		 * metadata results for this lookup, this program will need to write out
		 * to UTF-8
		 */
		inputTuiId = "16391605";
		inputTuiTag = "F272BD764FDEB344A54F53D0756DC3FD";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);

		/*
		 * Lookup Chinese album: NOTE: In order to correctly see the Chinese
		 * metadata results for this lookup, this program will need to write out
		 * to UTF-8
		 */
		inputTuiId = "3798282";
		inputTuiTag = "6BF6849840A77C987E8D3AF675129F33";
		musicidLookupAlbum(inputTuiId, inputTuiTag, user);
	}
	
	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				if (USELOCAL) {
					GnStorageSqlite sqliteStorage = new GnStorageSqlite();
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				loadLocale(gnsdk, user);
			
				doSampleTuiLookups(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
