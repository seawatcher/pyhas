package playlist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnPlaylistCollection.*;
import com.gracenote.gnsdk.GnSDK.*;

public class Playlist {
	private static boolean USELOCAL = false;
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}
	
	private static void enumeratePlaylistResults(
			GnUser user,
			GnPlaylistCollection playlistCollection,
			GnPlaylistResult playlistResult
			) throws GnException {
		System.out.println("Generated Playlist : " + playlistResult.identifiers().count());

		GnPlaylistResultIdentIterator gnPlaylistResultIdentIterator = playlistResult.identifiers().begin();

		while (gnPlaylistResultIdentIterator.hasNext()) {
			GnPlaylistIdentifier playlistIdentifier = gnPlaylistResultIdentIterator.next();
			
			GnPlaylistMetadata data = playlistCollection.metadata(user, playlistIdentifier);
			
			System.out.println("Identifier: " + playlistIdentifier.mediaIdentifier());
			System.out.println("Collection Name: " + playlistIdentifier.collectionName());
			System.out.println("GN_AlbumName: " + data.albumName());
			System.out.println("GN_ArtistName: " + data.artistName());
			System.out.println("GN_Era: " + data.era());
			System.out.println("GN_Genre: " + data.genre());
			System.out.println("GN_Origin: " + data.origin());
			System.out.println("GN_Mood: " + data.mood());
			System.out.println("GN_Tempo: " + data.tempo());
		}
	}

	private static void printPlaylistMorelikethisOptions(GnPlaylistCollection playlistCollection) throws GnException {
		long value ;

		value = playlistCollection.moreLikeThisOptionGet(GenerateMoreLikeThisOption.kMoreLikeThisMaxTracks);
		System.out.println(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_TRACKS :"+ value);

		value = playlistCollection.moreLikeThisOptionGet(GenerateMoreLikeThisOption.kMoreLikeThisMaxPerArtist);
		System.out.println(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ARTIST :"+ value);

		value = playlistCollection.moreLikeThisOptionGet(GenerateMoreLikeThisOption.kMoreLikeThisMaxPerAlbum);
		System.out.println(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ALBUM :"+ value);

		value = playlistCollection.moreLikeThisOptionGet(GenerateMoreLikeThisOption.kMoreLikeThisRandom);
		System.out.println(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_RANDOM :"+ value);
	}
	
	private static void doPlaylistMorelikethis(
			GnUser user, 
			GnPlaylistCollection collection
			) throws GnException {
		System.out.println("\n MoreLikeThis tests\n");
		System.out.println("\n MoreLikeThis with Default Options \n");
				
		/* Print the default More Like This options */
		printPlaylistMorelikethisOptions(collection);
		
		GnPlaylistResult resultMoreLikeThis = collection.generateMoreLikeThis(user, getSeedData(user, collection));
		
		enumeratePlaylistResults(user, collection, resultMoreLikeThis);
		
		/* Generate a more Like this with the custom settings */
		System.out.println("\n MoreLikeThis with Custom Options \n");

		/* Change the possible result set to be a maximum of 30 tracks. */
		collection.moreLikeThisOptionSet(GenerateMoreLikeThisOption.kMoreLikeThisMaxTracks, 30);
		/* Change the max per artist to be 20 */
		collection.moreLikeThisOptionSet(GenerateMoreLikeThisOption.kMoreLikeThisMaxPerArtist, 10);
		/* Change the max per album to be 5 */
		collection.moreLikeThisOptionSet(GenerateMoreLikeThisOption.kMoreLikeThisMaxPerAlbum, 5);
		/* Change the random result to be 1 so that there is no randomization */
		collection.moreLikeThisOptionSet(GenerateMoreLikeThisOption.kMoreLikeThisRandom, 1);

		/* Print the customized More Like This options */
		printPlaylistMorelikethisOptions(collection);
		
		GnPlaylistResult resultCustomMoreLikeThis = collection.generateMoreLikeThis(user, getSeedData(user, collection));
		
		enumeratePlaylistResults(user, collection, resultCustomMoreLikeThis);
	}

	private static GnPlaylistMetadata getSeedData(
			GnUser user,
			GnPlaylistCollection gnPlaylistCollection) throws GnException{		
		/* In this case , randomly selecting the 5th element */
		GnPlaylistIdentifier seed_ident = gnPlaylistCollection.mediaIdentifiers().at(4).next();

		GnPlaylistMetadata gdo_seed = gnPlaylistCollection.metadata(user, seed_ident);
		
		return gdo_seed;
	}
	
	private static void doPdlGeneration(
			GnUser user, 
			GnPlaylistCollection gnPlaylistCollection
			) throws GnException {
		int stmtIdx = 0;
		
		String[] pdlStatements = {
				"GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 0", 	/* like pop with a low score threshold (0) */
				"GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 300", 	/* like pop with a reasonable score threshold (300) */
				"GENERATE PLAYLIST WHERE GN_Genre = 2929", 				/* exactly pop */
				"GENERATE PLAYLIST WHERE GN_Genre = 2821", 				/* exactly rock */
				"GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 0", 	/* like rock with a low score threshold (0) */
				"GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 300",	 /* like rock with a reasonable score threshold (300) */
				"GENERATE PLAYLIST WHERE (GN_Genre LIKE SEED) > 300 LIMIT 20 RESULTS",
				"GENERATE PLAYLIST WHERE (GN_ArtistName LIKE 'Green Day') > 300 LIMIT 20 RESULTS, 2 PER GN_ArtistName;", };
		
		for (stmtIdx = 0; stmtIdx < pdlStatements.length; stmtIdx++) {
			System.out.println("\n PDL " + stmtIdx + " : "+ pdlStatements[stmtIdx]);
			
			GnPlaylistResult playlistResult = gnPlaylistCollection.generatePlaylist(
					user, 
					pdlStatements[stmtIdx], 
					getSeedData(user, gnPlaylistCollection)
					);

			enumeratePlaylistResults(
					user, 
					gnPlaylistCollection, 
					playlistResult
					);
		}
	}

	private static void doMusicRecognition(
			GnUser user, 
			GnPlaylistCollection gnPlaylistCollection
			) throws GnException {
		String inputQueryTocs[] = {
				"150 13224 54343 71791 91348 103567 116709 132142 141174 157219 175674 197098 238987 257905",
				"182 23637 47507 63692 79615 98742 117937 133712 151660 170112 189281",
				"182 14035 25710 40955 55975 71650 85445 99680 115902 129747 144332 156122 170507",
				"150 10705 19417 30005 40877 50745 62252 72627 84955 99245 109657 119062 131692 141827 152207 164085 173597 187090 204152 219687 229957 261790 276195 289657 303247 322635 339947 356272",
				"150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342",
				"150 1307 15551 31744 45022 57486 72947 85253 100214 115073 128384 141948 152951 167014",
				"183 69633 96258 149208 174783 213408 317508",
				"150 19831 36808 56383 70533 87138 105157 121415 135112 151619 169903 189073",
				"182 10970 29265 38470 59517 74487 83422 100987 113777 137640 150052 162445 173390 196295 221582",
				"150 52977 87922 128260 167245 187902 215777 248265",
				"183 40758 66708 69893 75408 78598 82983 87633 91608 98690 103233 108950 111640 117633 124343 126883 132298 138783 144708 152358 175233 189408 201408 214758 239808",
				"150 92100 135622 183410 251160 293700 334140",
				"150 17710 33797 65680 86977 116362 150932 166355 183640 193035",
				"150 26235 51960 73111 93906 115911 142086 161361 185586 205986 227820 249300 277275 333000",
				"150 1032 27551 53742 75281 96399 118691 145295 165029 189661 210477 232501 254342 282525",
				"150 26650 52737 74200 95325 117675 144287 163975 188650 209350 231300 253137 281525 337875",
				"150 19335 35855 59943 78183 96553 111115 125647 145635 163062 188810 214233 223010 241800 271197",
				"150 17942 32115 47037 63500 79055 96837 117772 131940 148382 163417 181167 201745",
				"150 17820 29895 41775 52915 69407 93767 105292 137857 161617 171547 182482 204637 239630 250692 282942 299695 311092 319080",
				"182 21995 45882 53607 71945 80495 94445 119270 141845 166445 174432 187295 210395 230270 240057 255770 277745 305382 318020 335795 356120",
				"187 34360 64007 81050 122800 157925 195707 230030 255537 279212 291562 301852 310601",
				"150 72403 124298 165585 226668 260273 291185" };

		int count = inputQueryTocs.length;

		System.out.println("\nPopulating Collection Summary from sample TOCs");
		
		/* Create the query handle */
		GnMusicID musicid = new GnMusicID(user);

		musicid.optionLookupData(GnLookupData.kLookupDataSonicData, true);
		musicid.optionLookupData(GnLookupData.kLookupDataPlaylist, true);

		String uniqueIdent = "";

		for (int index = 0; index < count; ++index) {
			GnResponseAlbums response = musicid.findAlbums(inputQueryTocs[index]);
			GnAlbum gnAlbum = response.albums().at(0).next();
			
			GnTrackIterable gnTrackIterable = gnAlbum.tracks();
			GnTrackIterator gnTrackIterator = gnTrackIterable.getIterator();
			
			long ntrack = 1;
			
			/* create a unique ident for every track that is added to the playlist.
			   Ideally the ident allows for the identification of which track it is.
			   e.g. path/filename.ext , or an id that can be externally looked up.
			 */
			while (gnTrackIterator.hasNext()) {
				GnTrack gnTrack = gnTrackIterator.next();

				uniqueIdent = String.valueOf(index).concat("_").concat(String.valueOf(ntrack));

				/*
				 * Add the the Album and Track GDO for the same ident so that we can
				 * query the Playlist Collection with both track and album level attributes.
				 */
				gnPlaylistCollection.add(uniqueIdent, gnAlbum);
				gnPlaylistCollection.add(uniqueIdent, gnTrack);
				
				ntrack++;
			}
		}
		
		System.out.println("\n Finished Recognition");
	}
	
	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				GnStorageSqlite sqliteStorage = new GnStorageSqlite();
				if (USELOCAL) {
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				/* Set locale with desired Group, Language, Region and Descriptor */	
				GnLocale locale = new GnLocale(
						GnSDK.getKLocaleGroupPlaylist(),
						GnSDK.getKLanguageEnglish(), 
						GnSDK.getKRegionDefault(),
						GnSDK.getKDescriptorSimplified(), 
						user
						);

				/* set this locale as default for the duration of gnsdk */
				gnsdk.setDefaultLocale(locale);
				
				GnPlaylist myPlaylist = new GnPlaylist();
				
				System.out.println("\nGnSDK Playlist Version : "+ myPlaylist.version());
				System.out.println(" \t(built " + myPlaylist.buildDate()+ ")" );
				
				System.out.println("\nStored Collections Count: " + myPlaylist.storedCollectionsCount());
				
				GnPlaylistCollection myCollection = new GnPlaylistCollection();
				
				if (0 == myPlaylist.storedCollectionsCount()) {
					myCollection = myPlaylist.createCollection("MyCollection");
					doMusicRecognition(user, myCollection);
					myPlaylist.storeCollection(myCollection);
				}else {
					myCollection = myPlaylist.loadCollection("MyCollection");
				}
				
				/* demonstrate PDL usage */
				doPdlGeneration(user, myCollection);

				/* demonstrate MoreLike usage */
				doPlaylistMorelikethis(user, myCollection);
				
				
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
