/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: link_coverart
 *  Description:
 *  Retrieves a coverart image using Link starting with a serialized GDO as source.
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag licenseFile gnsdkLibraryPath
 */

#include <iostream>
#include <list>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_link.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::musicid;
using namespace gracenote::link;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}
};


/*-----------------------------------------------------------------------------
 *  fetchImage
 *  Display file size
 */
static void
fetchImage(GnLink& link,gnsdk_link_content_type_t contentType, const char*  imageTypeStr)
{
	gnsdk_uint32_t  fileSize			  = 0;
	GnLinkContent *linkContent			  = GNSDK_NULL;
	GnImagePreference imgPreference		  = smallest;
	gnsdk_cstr_t	fileName			  = GNSDK_NULL;
	GnImageSize imageSize				  = size_170;

	/* Perform the image fetch */
	try
	{
		switch (contentType)
		{
		case gnsdk_link_content_cover_art:
			linkContent = link.CoverArt(imageSize, imgPreference);
			fileName = "cover.jpg";
			break;

		case gnsdk_link_content_image_artist:
			linkContent = link.ArtistImage(imageSize, imgPreference);
			fileName = "artist.jpg";
			break;		
		}

		fileSize = linkContent->DataSize();	 

		/* Do something with the image, e.g. display, save, etc. Here we just print the size. */
		printf("\nRETRIEVED: %s: %d byte JPEG\n", imageTypeStr, fileSize);	

		/* Here saving file. */

		gnsdk_byte_t *contentData = new gnsdk_byte_t[fileSize];		
		{
			std::fstream saveFile(fileName, std::fstream::out);
			saveFile.write((char*)contentData, fileSize);
		}				

		/*clean up*/
		if(GNSDK_NULL != linkContent){ delete linkContent; }
		if(GNSDK_NULL != contentData){ delete contentData; }

	}
	catch(GnError e)
	{
		/* For image to be fetched, it must exist in the size specified and you must be entitled to fetch images. */
		printf("NOT FOUND: %s\n", imageTypeStr);		
	}	
}

/*-----------------------------------------------------------------------------
 *  queryForAlbumImages
 */
static void
queryForAlbumImages(GnUser& user)
{
	gnsdk_cstr_t   serialized_gdo = "WEcxAbwX1+DYDXSI3nZZ/L9ntBr8EhRjYAYzNEwlFNYCWkbGGLvyitwgmBccgJtgIM/dkcbDgrOqBMIQJZMmvysjCkx10ppXc68ZcgU0SgLelyjfo1Tt7Ix/cn32BvcbeuPkAk0WwwReVdcSLuO8cYxAGcGQrEE+4s2H75HwxFG28r/yb2QX71pR";
	gnsdk_uint32_t file_size      = 0;


	printf("\n*****Sample Link Album Query*****\n");

	/* Typically, the GDO passed in to a Link query will come from the output of a GNSDK query.
	 * For an example of how to perform a query and get a GDO please refer to the documentation
	 * or other sample applications.
	 * The below serialized GDO was an 1-track album result from another GNSDK query.
	 */

	GnDataObject gnDataObject(serialized_gdo);

	/*LinkEvents linkevents;*/

	GnLink link(user, gnDataObject,GNSDK_NULL);/* use linkevents object to view progress status*/

	/* Perform the image fetches */	
	fetchImage(link,gnsdk_link_content_cover_art, "cover art");
	fetchImage(link,gnsdk_link_content_image_artist, "artist");	
	
}

#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  gdb_version = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);	
	printf("Gracenote DB Version : %s\n", gdb_version);
}
#endif

/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	/*LookupStatusEvents localeEvents;*/

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, GNSDK_NULL );/*use &statusEvents instead of GNSDK_NULL to view progress*/

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		printf("\nUsage:\n%s clientId clientIdTag license gnsdkLibraryPath\n", argv[0]);
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",                                   /* Log file path */
		    GNSDK_LOG_PKG_ALL,                              /* Include entries for all packages and subsystems */
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,  /* Include only error and warning entries */
		    GNSDK_LOG_OPTION_ALL,                           /* All logging options: timestamps, thread IDs, etc */
		    0,                                              /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                     /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		user.OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		/* Perform a sample cover art query */
		queryForAlbumImages(user);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorDescription() << std::endl;
		std::cout << std::hex << e.ErrorCode() << std::endl;
	}

	return 0;
}





