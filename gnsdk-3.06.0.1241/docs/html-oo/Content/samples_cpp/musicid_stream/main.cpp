/*
 *  Copyright (c) 2000-2013 Gracenote.
 *
 *  This software may not be used in any way or distributed without
 *  permission. All rights reserved.
 *
 *  Some code herein may be covered by US and international patents.
 */

/*
 *   Name: musicid_stream
 *   Description:
 *   This example uses MusicID-Stream to fingerprint and identify a music track.
 *
 *   Command-line Syntax:
 *   sample client_id client_id_tag license library_path
 */

#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicidstream.hpp"

#include "GnMic.h"
#include "GnWavCapture.h"
#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::musicid_stream;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}

};


/* GnStatusEvents : overrider methods of this class to get delegate callbacks */
class MusicIDStreamEvents : public GnMusicIDStreamEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(GnMusicIDStream& mids, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		GNSDK_UNUSED(mids);

		std::cout << "status (%" << percent_complete << ") ";
		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout << "Unknown      ";
			break;

		case gnsdk_status_begin:
			std::cout << "Begin        ";
			break;

		case gnsdk_status_connecting:
			std::cout << "Connecting   ";
			break;

		case gnsdk_status_sending:
			std::cout << "Sending      ";
			break;

		case gnsdk_status_receiving:
			std::cout << "Receiving    ";
			break;

		case gnsdk_status_disconnected:
			std::cout << "Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout << "Complete     ";
			break;

		default:
			break;
		}

		std::cout << "sent/rcvd " << bytes_total_sent << "/" << bytes_total_received << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check(GnMusicIDStream& mids)
	{
		GNSDK_UNUSED(mids);
		return false;
	}

	/*-----------------------------------------------------------------------------
	 *  midstream_status_event
	 */
	virtual void
	midstream_status_event(GnMusicIDStream& mids, gnsdk_musicidstream_status_t status)
	{
		GNSDK_UNUSED(mids);

		switch (status)
		{
		case gnsdk_mids_status_invalid:
			std::cout <<"Invalid status!" << std::endl;
			break;

		case gnsdk_mids_identifying_started:
			std::cout <<"Identification: started" << std::endl;
			break;

		case gnsdk_mids_identifying_fp_generated:
			std::cout <<"Identification: fingerprint generated" << std::endl;
			break;

		case gnsdk_mids_identifying_local_query_started:
			std::cout <<"Identification: local query started" << std::endl;
			break;

		case gnsdk_mids_identifying_local_query_ended:
			std::cout <<"Identification: local query ended" << std::endl;
			break;

		case gnsdk_mids_identifying_online_query_started:
			std::cout <<"Identification: online query started" << std::endl;
			break;

		case gnsdk_mids_identifying_online_query_ended:
			std::cout <<"Identification: online query ended" << std::endl;
			break;

		case gnsdk_mids_identifying_ended:
			std::cout <<"Identification: ended" << std::endl;
			break;

		case gnsdk_mids_status_audio_none:
			std::cout <<"Audio classification: none" << std::endl;
			break;

		case gnsdk_mids_status_audio_silence:
			std::cout <<"Audio classification: silence" << std::endl;
			break;

		case gnsdk_mids_status_audio_noise:
			std::cout <<"Audio classification: noise" << std::endl;
			break;

		case gnsdk_mids_status_audio_speech:
			std::cout <<"Audio classification: speech" << std::endl;
			break;

		case gnsdk_mids_status_audio_music:
			std::cout <<"Audio classification: music" << std::endl;
			break;

		case gnsdk_mids_status_transition_none:
			std::cout <<"Audio transition: none" << std::endl;
			break;

		case gnsdk_mids_status_transition_channel_change:
			std::cout <<"Audio transition: channel change" << std::endl;
			break;

		case gnsdk_mids_status_transition_content_to_content:
			std::cout <<"Audio transition: content to content" << std::endl;
			break;
		}
	}

	/*-----------------------------------------------------------------------------
	 *  result_available
	 */
	virtual void
	result_available(GnMusicIDStream& mids, GnResponseAlbums result)
	{
		if (result.ResultCount() > 0)
		{
			metadata::album_iterator it_album = result.Albums().begin();

			for (; it_album != result.Albums().end(); ++it_album)
			{
				GnAlbum album = *it_album;

				std::cout << std::endl;
				std::cout << "      Track: " << album.TracksMatched()[0]->Title().Display() << std::endl;
				std::cout << "      Album: " << album.Title().Display() << std::endl;
			}
		}
		else
		{
			std::cout << "      No match found" << std::endl;
		}
		std::cout << std::endl;

		mids.AudioProcessStop();
	}

};


/*-----------------------------------------------------------------------------
 *  do_musicid_stream
 */
static void
do_musicid_stream(GnUser& user)
{
	printf("\n*****Sample MID-Stream Query*****\n");

	GnMusicIDStreamEvents* mids_events = new MusicIDStreamEvents;

	try
	{
		GnMusicIDStream mids(user, mids_events);

		/* create microphone to use as audio source */
		GnMic        mic(44100, 16, 1);
		GnWavCapture wavrec(mic, "mic_record.wav");

		/* start identification in background - will wait for audio if none yet */
		mids.Identify();

		/* provide audio */
		mids.AudioProcessStart(wavrec);

		/* returns when recognition complete */
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

	delete mids_events;
}


#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void
display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  versionResult = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);
	std::cout << "GracenoteDB Source DB ID :\t: " << versionResult<< std::endl;

}


#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	LookupStatusEvents localeEvents;

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, &localeEvents );

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		std::cout << "\nUsage:" << argv[0] << " clientId clientIdTag license gnsdkLibraryPath\n"<< std::endl;
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",                                   /* Log file path */
		    GNSDK_LOG_PKG_ALL,                              /* Include entries for all packages and subsystems */
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,  /* Include only error and warning entries */
		    GNSDK_LOG_OPTION_ALL,                           /* All logging options: timestamps, thread IDs, etc */
		    0,                                              /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                     /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		GnLookupLocal gnLookupLocal;

		user->OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		do_musicid_stream(user);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

	return 0;
}

