/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: musicid_lookup_matches_text
 *  Description:
 *  This example finds matches based on input text.
 *
 *  Command-line Syntax:
 *  sample client_id client_id_tag license librarypath
 */

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *  Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

using namespace gracenote;
using namespace gracenote::musicid;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}
};


/*-----------------------------------------------------------------------------
 *  display_match
 */
static void
display_match( GnDataMatch& dataMatch )
{
	if (dataMatch.IsAlbum())
	{
		/* Display Album match*/
		GnAlbum album = dataMatch.GetAsAlbum();
		printf( "%16s %s\n", "Title:", album.Title().Display() );		

	}
	else if (dataMatch.IsContributor())
	{
		/* Display Contributor match */
		GnContributor contrib = dataMatch.GetAsContributor();
		GnName name = contrib.NamesOfficial().at(0).next();
		printf( "%16s %s\n", "Name:", name );
	}
	else
	{
		printf("\nUnsupported match data type: %s\n", dataMatch.GetType());
	}

}

/*-----------------------------------------------------------------------------
 *  do_match_selection
 */
static gnsdk_uint32_t
do_match_selection(GnResponseDataMatches& match_response)
{
	/*
	   This is where any matches that need resolution/disambiguation are iterated
	   and a single selection of the best match is made.

	   For this simplified sample, we'll just echo the matches and select the first match.
	 */

	printf( "%16s %d\n", "Match count:", match_response.DataMatches().count());

	metadata::datamatches_iterator it_match = match_response.DataMatches().begin();
	for (; it_match != match_response.DataMatches().end(); ++it_match )
	{
		GnDataMatch match = *it_match;
		/* Display match object */
		display_match(match);

	}

	return 1;
}

/*-----------------------------------------------------------------------------
 *  do_sample_text_query
 */
void
do_sample_text_query(gnsdk_cstr_t album_title, gnsdk_cstr_t track_title, gnsdk_cstr_t artist_name, GnUser& user)
{
	bool           needs_decision = false;
	gnsdk_uint32_t choice_ordinal = 0;

	printf("\n*****MusicID Text Match Query*****\n");

	if (album_title != GNSDK_NULL) {	printf( "%-15s: %s\n", "album title", album_title );	}
	if (track_title != GNSDK_NULL) {	printf( "%-15s: %s\n", "track title", track_title );	}
	if (artist_name != GNSDK_NULL) {	printf( "%-15s: %s\n", "artist name", artist_name );	}

	GnMusicID musicid(user);

	/* Perform the query */
	GnResponseDataMatches match_response = musicid.FindMatches(album_title, track_title, artist_name);

	int count =match_response.DataMatches().count();
	if (count == 0)
	{
		printf("\nNo matches found for the input.\n");
	}
	else
	{
		/* we have at least one match, see if disambiguation (match resolution) is necessary. */
		needs_decision = match_response.NeedsDecision();

		/* See if selection of one of the albums needs to happen */
		if (needs_decision)
		{
			choice_ordinal = do_match_selection(match_response);

		}
		else
		{
			/* no need for disambiguation, we'll take the first album */
			choice_ordinal = 1;
		}

		GnDataMatch match = match_response.DataMatches().at(choice_ordinal-1).next();

		GnAlbum album;

		if (match.IsAlbum())
		{
			album = match.GetAsAlbum();
			bool fullResult = album.FullResult();
			/* See if the match has full data or only partial data. */
			if (!fullResult)
			{
				/* do followup query to get full object. Setting the partial album as the query input. */
				GnResponseAlbums followup_response = musicid.FindAlbums(album);

				/* now our first album is the desired result with full data */
				album = followup_response.Albums().at(0).next();

			}		
		}

		/* We should now have our final, full match result. */
		printf( "%16s\n", "Final match:");	
		if (album.native())
		{
			printf( "%16s %s\n", "Title:", album.Title().Display() );
		}
		else
		{
			display_match(match);

		}
	}

} /* do_sample_text_query() */


#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void
display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal     = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  gdb_version = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);
	printf("Gracenote DB Version : %s\n", gdb_version);
}


#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	/*LookupStatusEvents localeEvents;*/

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, GNSDK_NULL ); /*use &statusEvents instead of GNSDK_NULL to view progress*/

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		printf("\nUsage:\n%s clientId clientIdTag license gnsdkLibraryPath\n", argv[0]);
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",                                   /* Log file path */
		    GNSDK_LOG_PKG_ALL,                              /* Include entries for all packages and subsystems */
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,  /* Include only error and warning entries */
		    GNSDK_LOG_OPTION_ALL,                           /* All logging options: timestamps, thread IDs, etc */
		    0,                                              /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                     /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		user.OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		/* Perform a sample text query with Album, Track and Artist inputs */
		do_sample_text_query( "Supernatural", "Africa Bamba", "Santana", user);

		/*	Perform a query with just the album name.	*/
		do_sample_text_query( "看我72变", GNSDK_NULL, GNSDK_NULL, user);

		/*	Perform a sample text query with only the Artist name as an input.	*/
		do_sample_text_query(GNSDK_NULL, GNSDK_NULL, "Philip Glass", user);
		do_sample_text_query(GNSDK_NULL, GNSDK_NULL, "Bob Marley", user);

		/*	Perform a sample text query with Track Title and Artist name.	*/
		do_sample_text_query(GNSDK_NULL, "Purple Stain", "Red Hot Chili Peppers", user);
		do_sample_text_query(GNSDK_NULL, "Eyeless", "Slipknot", user);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

} /*main()*/

