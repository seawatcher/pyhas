var group___music___fingerprinting___feature_generation_functions =
[
    [ "gnsdk_dsp_feature_audio_begin", "group___music___fingerprinting___feature_generation_functions.html#ga3dd4b73cfcf846c2f64933c5cf74cfe8", null ],
    [ "gnsdk_dsp_feature_audio_write", "group___music___fingerprinting___feature_generation_functions.html#gab2d389945e3c3538cfe8ec64957b2b7b", null ],
    [ "gnsdk_dsp_feature_end_of_write", "group___music___fingerprinting___feature_generation_functions.html#ga476bc00356cd0b268ebed16097839087", null ],
    [ "gnsdk_dsp_feature_release", "group___music___fingerprinting___feature_generation_functions.html#ga00f94d2e705ec3337c51574ceed54e6e", null ],
    [ "gnsdk_dsp_feature_retrieve_data", "group___music___fingerprinting___feature_generation_functions.html#ga644c1744509f514d0c641d747f68f5a1", null ]
];