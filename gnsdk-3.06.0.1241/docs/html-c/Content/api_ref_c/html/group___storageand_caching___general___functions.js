var group___storageand_caching___general___functions =
[
    [ "gnsdk_manager_storage_cleanup", "group___storageand_caching___general___functions.html#gaedc6ef12ba9e6ee33eb202462a235c9a", null ],
    [ "gnsdk_manager_storage_compact", "group___storageand_caching___general___functions.html#ga8a6f324952c2a20af22c83ae44352e36", null ],
    [ "gnsdk_manager_storage_flush", "group___storageand_caching___general___functions.html#gaaf1702b74e05b26ab5d21b5354893212", null ],
    [ "gnsdk_manager_storage_location_set", "group___storageand_caching___general___functions.html#ga70fc6e23b6bb467d7e31a8685c24fa53", null ],
    [ "gnsdk_manager_storage_validate", "group___storageand_caching___general___functions.html#gac3583625f90608759e3faed7dbc177e9", null ]
];