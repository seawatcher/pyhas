var searchData=
[
  ['peformsyncprocess',['PeformSyncProcess',['../class_gracenote_s_d_k_1_1_gn_playlist_collection.html#a8342cc6dd24bdddb1ee4c008adc68a2f',1,'GracenoteSDK::GnPlaylistCollection']]],
  ['presentation_5fdata_5fprovider',['presentation_data_provider',['../class_gracenote_s_d_k_1_1presentation__data__provider.html#add9d12dffb4f5aab94e785779ebf435f',1,'GracenoteSDK.presentation_data_provider.presentation_data_provider()'],['../class_gracenote_s_d_k_1_1presentation__data__provider.html#af5cb1847bb5fbf8cc4ff74411f3e676c',1,'GracenoteSDK.presentation_data_provider.presentation_data_provider(GnMoodgridPresentationType type)']]],
  ['presentation_5fdata_5fprovider_5fcount',['presentation_data_provider_count',['../class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e.html#ad2e0c14c91231b3d8f152902a6111cbd',1,'GracenoteSDK::gnsdk_csharp_marshalPINVOKE']]],
  ['presentation_5fdata_5fprovider_5fget_5fdata',['presentation_data_provider_get_data',['../class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e.html#a620d0c5777c9c415d9bba63f4f7d6a40',1,'GracenoteSDK::gnsdk_csharp_marshalPINVOKE']]],
  ['products',['Products',['../class_gracenote_s_d_k_1_1_gn_response_video_objects.html#a452895c5c1962c7ede4cb529b26f913b',1,'GracenoteSDK.GnResponseVideoObjects.Products()'],['../class_gracenote_s_d_k_1_1_gn_video_work.html#a846498ebe0d9160ba9552d8bf98808b0',1,'GracenoteSDK.GnVideoWork.Products()']]]
];
