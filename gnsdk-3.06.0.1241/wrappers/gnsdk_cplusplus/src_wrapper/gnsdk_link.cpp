/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* link.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */
#include "gnsdk.hpp"
#include "metadata.hpp"
#include "metadata_music.hpp"

#if GNSDK_LINK

using namespace gracenote;
using namespace gracenote::link;

/******************************************************************************
** GnLink
*/
GnLink::GnLink(const GnUser& user, const GnDataObject& gnDataObject, GnStatusEvents* pEventHandler)  throw ( GnError ) :
	mEventHandler(GNSDK_NULL)
{
	gnsdk_error_t error;

	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_LINK);

	mEventHandler = pEventHandler;

	error = gnsdk_link_query_create(user.native(), _CallbackStatus, this, &mQueryHandle);
	if(error) { throw GnError(); }

	error = gnsdk_link_query_set_gdo(mQueryHandle, gnDataObject.native() );
	if(error) { throw GnError(); }
}

GnLink::GnLink(const GnUser& user, const GnListElement& listElement, GnStatusEvents* pEventHandler)  throw ( GnError ) :
	mEventHandler(GNSDK_NULL)
{
	gnsdk_error_t error;

	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_LINK);

	mEventHandler = pEventHandler;

	error = gnsdk_link_query_create(user.native(), _CallbackStatus, this, &mQueryHandle);
	if(error) { throw GnError(); }

	error = gnsdk_link_query_set_list_element(mQueryHandle, listElement.native() );
	if(error) { throw GnError(); }
}

GnLink::~GnLink()
{
	gnsdk_link_query_release(mQueryHandle);
}

/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t GnLink::Version()
{
	return gnsdk_link_get_version();
}

/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t GnLink::BuildDate()
{
	return gnsdk_link_get_build_date();
}

/*-----------------------------------------------------------------------------
 *  OptionLookupMode
 */
void GnLink::OptionLookupMode(gracenote::GnLookupMode lookupMode) throw ( GnError )
{
	gnsdk_cstr_t  lookup_mode_value = GNSDK_NULL;
	gnsdk_error_t error;

	switch(lookupMode)
	{
	case kLookupModeLocal:
		lookup_mode_value = GNSDK_LOOKUP_MODE_LOCAL;
		break;

	case kLookupModeOnline:
		lookup_mode_value = GNSDK_LOOKUP_MODE_ONLINE;
		break;

	case kLookupModeOnlineCacheOnly:
		lookup_mode_value = GNSDK_LOOKUP_MODE_ONLINE_CACHEONLY;
		break;

	case kLookupModeOnlineNoCache:
		lookup_mode_value = GNSDK_LOOKUP_MODE_ONLINE_NOCACHE;
		break;

	case kLookupModeOnlineNoCacheRead:
		lookup_mode_value = GNSDK_LOOKUP_MODE_ONLINE_NOCACHEREAD;
		break;

	default:
		break;
	}

	error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_LOOKUP_MODE, lookup_mode_value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionTrackOrd
 */
void GnLink::OptionTrackOrd(gnsdk_cstr_t optionValue) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_TRACK_ORD, optionValue);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionDataSource
 */
void GnLink::OptionDataSource(gnsdk_cstr_t optionValue) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_DATASOURCE, optionValue);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionDataType
 */
void GnLink::OptionDataType(gnsdk_cstr_t optionValue) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_DATATYPE, optionValue);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  ClearOptions
 */
void GnLink::ClearOptions() throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_link_query_options_clear(mQueryHandle);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  _MapImgSizeToCstr
 */
gnsdk_cstr_t GnLink::_MapImgSizeToCstr
	(GnImageSize imgSize)
{
	gnsdk_cstr_t coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_450;

	switch(imgSize)
	{
	case size_75:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_75;
		break;

	case size_170:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_170;
		break;

	case size_300:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_300;
		break;

	case size_450:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_450;
		break;

	case size_720:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_720;
		break;

	case size_1080:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_1080;
		break;

	case size_110:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_110;
		break;

	case size_220:
		coverSize = GNSDK_LINK_OPTION_VALUE_IMAGE_SIZE_220;
		break;
	}

	return coverSize;
}

/*-----------------------------------------------------------------------------
 *  _CallbackStatus
 */
void GNSDK_CALLBACK_API GnLink::_CallbackStatus(void* callback_data, gnsdk_status_t link_status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort)
{
	GnLink* p_link = (GnLink*)callback_data;

	if(p_link->mEventHandler)
	{
		p_link->mEventHandler->status_event(link_status, percent_complete, bytes_total_sent, bytes_total_received);
		if(p_link->mEventHandler->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _GnImageArt
 */
GnLinkContent* GnLink::_GnImageArt(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_link_content_type_t linkContentType, gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_int32_t img_size = imageSize;

	error = gnsdk_link_query_options_clear(mQueryHandle);
	if(error) { throw GnError(); }

	if(imagePreference == exact)
	{
		error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_IMAGE_SIZE, _MapImgSizeToCstr(imageSize) );
		if(error) { throw GnError(); }
	}
	else if(imagePreference == largest)
	{
		while(img_size >= 0)
		{
			error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_IMAGE_SIZE, _MapImgSizeToCstr( (GnImageSize)img_size) );
			if(error) { throw GnError(); }

			img_size--;
		}
	}
	else if(imagePreference == smallest)
	{
		while(img_size <= size_1080)
		{
			error = gnsdk_link_query_option_set(mQueryHandle, GNSDK_LINK_OPTION_KEY_IMAGE_SIZE, _MapImgSizeToCstr( (GnImageSize)img_size) );
			if(error) { throw GnError(); }

			img_size++;
		}
	}

	return new GnLinkContent(mQueryHandle, linkContentType, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  CoverArt
 */
GnLinkContent* GnLink::CoverArt(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return _GnImageArt(imageSize, imagePreference, gnsdk_link_content_cover_art, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  GenreArt
 */
GnLinkContent* GnLink::GenreArt(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return _GnImageArt(imageSize, imagePreference, gnsdk_link_content_genre_art, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  Image
 */
GnLinkContent* GnLink::Image(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return _GnImageArt(imageSize, imagePreference, gnsdk_link_content_image, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  ArtistImage
 */
GnLinkContent* GnLink::ArtistImage(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return _GnImageArt(imageSize, imagePreference, gnsdk_link_content_image_artist, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  Review
 */
GnLinkContent* GnLink::Review(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_review, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  Biography
 */
GnLinkContent* GnLink::Biography(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_biography, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  ArtistNews
 */
GnLinkContent* GnLink::ArtistNews(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_artist_news, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  LyricXML
 */
GnLinkContent* GnLink::LyricXML(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_lyric_xml, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  LyricText
 */
GnLinkContent* GnLink::LyricText(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_lyric_text, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  DspData
 */
GnLinkContent* GnLink::DspData(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_dsp_data, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  CommentsListener
 */
GnLinkContent* GnLink::CommentsListener(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_comments_listener, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  CommentsRelease
 */
GnLinkContent* GnLink::CommentsRelease(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_comments_release, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  News
 */
GnLinkContent* GnLink::News(gnsdk_uint32_t itemOrdinal) throw ( GnError )
{
	return new GnLinkContent(mQueryHandle, gnsdk_link_content_news, itemOrdinal);
}

/*-----------------------------------------------------------------------------
 *  Count
 */
gnsdk_uint32_t GnLink::Count(gnsdk_link_content_type_t contentType) throw ( GnError )
{
	gnsdk_uint32_t count = 0;

	gnsdk_error_t  error = gnsdk_link_query_content_count(mQueryHandle, contentType, &count);

	if(error) { throw GnError(); }

	return count;
}

/*-----------------------------------------------------------------------------
 *  GnLinkContent
 */
GnLinkContent::GnLinkContent(const gnsdk_link_query_handle_t query_handle, gnsdk_link_content_type_t contentType, gnsdk_uint32_t itemOrdinal) throw ( GnError )
	: mQueryHandle(query_handle),
	  mContentType(contentType),
	  mItemOrdinal(itemOrdinal),
	  mData(GNSDK_NULL),
	  mDataType(gnsdk_link_data_unknown),
	  mDataSize(0)
{
	gnsdk_error_t error = gnsdk_link_query_content_retrieve(mQueryHandle, mContentType, mItemOrdinal, &mDataType, &mData, &mDataSize);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  DataBuffer
 */
void GnLinkContent::DataBuffer(gnsdk_byte_t* contentData)
{
	gnsdk_uint32_t i = 0;

	for(i = 0; i < mDataSize; i++)
	{
		contentData[i] = mData[i];
	}
}

/*-----------------------------------------------------------------------------
 *  DataSize
 */
gnsdk_size_t GnLinkContent::DataSize()
{
	return mDataSize;
}

/*-----------------------------------------------------------------------------
 *  DataType
 */
gnsdk_link_data_type_t GnLinkContent::DataType()
{
	return mDataType;
}

GnLinkContent::~GnLinkContent()
{
	gnsdk_link_query_content_free(mData);
}
#endif /* GNSDK_LINK */
