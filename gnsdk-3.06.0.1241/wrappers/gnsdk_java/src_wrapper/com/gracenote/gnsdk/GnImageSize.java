/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;


public enum GnImageSize {
  size_75(1),
  size_110,
  size_170,
  size_220,
  size_300,
  size_450,
  size_720,
  size_1080,
;

  public final int swigValue() {
    return swigValue;
  }

  public static GnImageSize swigToEnum(int swigValue) {
    GnImageSize[] swigValues = GnImageSize.class.getEnumConstants();
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (GnImageSize swigEnum : swigValues)
      if (swigEnum.swigValue == swigValue)
        return swigEnum;
    throw new IllegalArgumentException("No enum " + GnImageSize.class + " with value " + swigValue);
  }

  @SuppressWarnings("unused")
  private GnImageSize() {
    this.swigValue = SwigNext.next++;
  }

  @SuppressWarnings("unused")
  private GnImageSize(int swigValue) {
    this.swigValue = swigValue;
    SwigNext.next = swigValue+1;
  }

  @SuppressWarnings("unused")
  private GnImageSize(GnImageSize swigEnum) {
    this.swigValue = swigEnum.swigValue;
    SwigNext.next = this.swigValue+1;
  }

  private final int swigValue;

  private static class SwigNext {
    private static int next = 0;
  }
}

