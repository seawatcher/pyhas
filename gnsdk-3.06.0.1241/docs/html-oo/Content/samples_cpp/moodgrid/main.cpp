/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *
 *	Description:
 *	Moodgrid enables querying offline and online providers for mood based playlists.
 *  Moodgrid is a curated flow of adjacent moods that is available in ( multiple) 2 Dimension data grids.
 *  The sample shows using a playlist collection as an offline provider of music for a 5x5 Moodgrid and 10x10 Moodgrid.
 *	Command-line Syntax:
 *	sample client_id client_id_tag license gnsdk_lib_path
 */

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#define USE_LOCAL 0

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_playlist.hpp"
#include "gnsdk_moodgrid.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

using namespace gracenote;
using namespace gracenote::musicid;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;



/*-----------------------------------------------------------------------------
 *  print_library_information
 */
void
print_library_information(gnsdk_cstr_t libname,  gnsdk_cstr_t version, gnsdk_cstr_t builddate)
{
	printf("Library    : %s \nVersion    :%s\tBuild Date :%s  \n", libname, version, builddate);
}


/*-----------------------------------------------------------------------------
 *  do_music_recognition
 */
void
do_music_recognition (GnUser& user,   playlist::GnPlaylistCollection& collection )
{
	gnsdk_cstr_t input_query_tocs[] =  {
		"150 13224 54343 71791 91348 103567 116709 132142 141174 157219 175674 197098 238987 257905",
		"182 23637 47507 63692 79615 98742 117937 133712 151660 170112 189281",
		"182 14035 25710 40955 55975 71650 85445 99680 115902 129747 144332 156122 170507",
		"150 10705 19417 30005 40877 50745 62252 72627 84955 99245 109657 119062 131692 141827 152207 164085 173597 187090 204152 219687 229957 261790 276195 289657 303247 322635 339947 356272",
		"150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342",
		"150 1307 15551 31744 45022 57486 72947 85253 100214 115073 128384 141948 152951 167014",
		"183 69633 96258 149208 174783 213408 317508",
		"150 19831 36808 56383 70533 87138 105157 121415 135112 151619 169903 189073",
		"182 10970 29265 38470 59517 74487 83422 100987 113777 137640 150052 162445 173390 196295 221582",
		"150 52977 87922 128260 167245 187902 215777 248265",
		"183 40758 66708 69893 75408 78598 82983 87633 91608 98690 103233 108950 111640 117633 124343 126883 132298 138783 144708 152358 175233 189408 201408 214758 239808",
		"150 92100 135622 183410 251160 293700 334140",
		"150 17710 33797 65680 86977 116362 150932 166355 183640 193035",
		"150 26235 51960 73111 93906 115911 142086 161361 185586 205986 227820 249300 277275 333000",
		"150 1032 27551 53742 75281 96399 118691 145295 165029 189661 210477 232501 254342 282525",
		"150 26650 52737 74200 95325 117675 144287 163975 188650 209350 231300 253137 281525 337875",
		"150 19335 35855 59943 78183 96553 111115 125647 145635 163062 188810 214233 223010 241800 271197",
		"150 17942 32115 47037 63500 79055 96837 117772 131940 148382 163417 181167 201745",
		"150 17820 29895 41775 52915 69407 93767 105292 137857 161617 171547 182482 204637 239630 250692 282942 299695 311092 319080",
		"182 21995 45882 53607 71945 80495 94445 119270 141845 166445 174432 187295 210395 230270 240057 255770 277745 305382 318020 335795 356120",
		"187 34360 64007 81050 122800 157925 195707 230030 255537 279212 291562 301852 310601",
		"150 72403 124298 165585 226668 260273 291185"
	};


	printf("\nPopulating Collection Summary from sample TOCs \n");
	gnsdk_uint32_t count             = sizeof(input_query_tocs) / sizeof(input_query_tocs[0]);
	
	GnMusicID musicId(user);

	musicId.OptionLookupData(kLookupDataPlaylist, true);

    std::ostringstream outStream;
	for (gnsdk_uint32_t index = 0; index < count; ++index) {
		GnResponseAlbums response = musicId.FindAlbums(input_query_tocs[index]);

		GnAlbum        album  = *(response.Albums().begin());
		gnsdk_uint32_t ntrack = 1;
		for (metadata::track_iterator itr = album.Tracks().begin(); itr != album.Tracks().end(); ++itr, ++ntrack)
		{
			/* create a unique ident for every track that is added to the playlist.
			   Ideally the ident allows for the identification of which track it is.
			   e.g. path/filename.ext , or an id that can be externally looked up.
			 */
			outStream.str("");
			outStream << index << '_' << ntrack ;
				/*
				   Add the the Album and Track GDO for the same ident so that we can
				   query the Playlist Collection with both track and album level attributes.
				 */
            std::string unique_ident = outStream.str();
            collection.Add(unique_ident.c_str(), album);     /* Add the album*/
            collection.Add(unique_ident.c_str(), *itr);      /* Add the track*/
            outStream.clear();
			
		}
	}

	printf("\n Finished Recognition \n");
}


/*-----------------------------------------------------------------------------
 *  do_moodgrid_recommendations
 */
void
do_moodgrid_recommendations(GnUser& user, playlist::GnPlaylistCollection& collection, moodgrid::GnMoodgridPresentationType type)
{
	moodgrid::GnMoodgrid myMoodgrid;


	print_library_information("gnsdk_moodgrid", myMoodgrid.Version(), myMoodgrid.BuildDate());

	moodgrid::GnMoodgridProvider myProvider = *(myMoodgrid.Providers().at(0));

	/* get the details for the provider */
	gnsdk_cstr_t mProviderName = myProvider.Name();
	printf("\n GNSDK_MOODGRID_PROVIDER_NAME :%s \n", mProviderName);

	gnsdk_cstr_t mProviderType = myProvider.Type();
	printf("\n GNSDK_MOODGRID_PROVIDER_TYPE :%s \n", mProviderType);

	/* create a moodgrid presentation for the specified type */
	moodgrid::GnMoodgridPresentation myPresentation =  myMoodgrid.CreatePresentation(user, type);

	moodgrid::GnMoodgridPresentation::data_iterator itr = myPresentation.Moods().begin();

	for (; itr != myPresentation.Moods().end(); ++itr) {
		/* find the recommendation for the mood */
		moodgrid::GnMoodgridResult result = myPresentation.FindRecommendations(myProvider, *itr);

		printf("\n\n\tX:%d  Y:%d\tMood Name: %s\tMood ID: %s\tCount: %d\n", itr->X, itr->Y, myPresentation.MoodName(*itr), myPresentation.MoodId(*itr), result.Count());

		moodgrid::GnMoodgridResult::iterator result_itr = result.Identifiers().begin();
		/* iterate the results for the idents */
		for (; result_itr != result.Identifiers().end(); ++result_itr) {
			printf("\n\n\tX:%d  Y:%d", itr->X, itr->Y);

			printf("\nident:\t%s\n", result_itr->MediaIdentifier());
			printf("group:\t%s\n", result_itr->Group());

			playlist::GnPlaylistMetadata   data = collection.Metadata(user, result_itr->MediaIdentifier(), result_itr->Group());

			printf("Album:\t%s\n", data.AlbumName());
			printf("Mood :\t%s\n", data.Mood());

		}
	}

}


#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void
display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  versionResult = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);
	std::cout << "GracenoteDB Source DB ID :\t: " << versionResult<< std::endl;

}
#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}




/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;

	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		std::cout << "\nUsage:" << argv[0] << " clientId clientIdTag license gnsdkLibraryPath\n"<< std::endl;
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",                                   /* Log file path */
		    GNSDK_LOG_PKG_ALL,                              /* Include entries for all packages and subsystems */
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,  /* Include only error and warning entries */
		    GNSDK_LOG_OPTION_ALL,                           /* All logging options: timestamps, thread IDs, etc */
		    0,                                              /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                     /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

        GnStorageSqlite storageSqlite;
#if USE_LOCAL
		
		storageSqlite.StorageFolderSet("../../../sample_db");

		GnLookupLocal gnLookupLocal;

		user->OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

        /* Set locale with desired Group, Language, Region and Descriptor */
        GnLocale locale( GnSDK::kLocaleGroupPlaylist, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user );
        
        /* set this locale as default for the duration of gnsdk */
        gnsdk.SetDefaultLocale(locale);


		/*Get Playlist library information*/
		playlist::GnPlaylist myPlaylist;
		print_library_information("gnsdk_playlist", myPlaylist.Version(), myPlaylist.BuildDate());


		/*How may collections are stored? */
		int storedCollCount = myPlaylist.StoredCollections().count();
		playlist::GnPlaylistCollection myCollection;
		printf("\nStored Collections Count: %d\n", storedCollCount);

		if (storedCollCount == 0)
		{
			/* Create new collection onlne if not stored any*/
			myCollection = myPlaylist.CreateCollection("sample_collection");
			do_music_recognition(user, myCollection);
			myPlaylist.StoreCollection(myCollection);
		}
		else
		{
			/* Load existing collection from local store*/
			playlist::GnPlaylist::storage_iterator storageItr = myPlaylist.StoredCollections().begin();
			myCollection = myPlaylist.LoadCollection(storageItr);
					
        }

		/* create, query and print a  5 x  5  moodgrid  */
		do_moodgrid_recommendations(user, myCollection, moodgrid::kMoodgridPresentationType5x5);

		/* create, query and print a 10 x 10 moodgrid*/
		do_moodgrid_recommendations(user, myCollection, moodgrid::kMoodgridPresentationType10x10);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;

	}


}

