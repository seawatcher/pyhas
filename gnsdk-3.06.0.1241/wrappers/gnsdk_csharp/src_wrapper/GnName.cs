/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnName : GnDataObject {
  private HandleRef swigCPtr;

  internal GnName(IntPtr cPtr, bool cMemoryOwn) : base(gnsdk_csharp_marshalPINVOKE.GnName_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnName obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnName() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnName(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GnName() : this(gnsdk_csharp_marshalPINVOKE.new_GnName(), true) {
  }

  public string SortableScheme() {
	IntPtr temp = gnsdk_csharp_marshalPINVOKE.GnName_SortableScheme(swigCPtr); 
	return GnMarshalUTF8.StringFromNativeUtf8(temp);
}

  public GnLanguage Language {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnName_Language_get(swigCPtr);
      GnLanguage ret = (cPtr == IntPtr.Zero) ? null : new GnLanguage(cPtr, true);
      return ret;
    } 
  }

  public GnSortable Sortable {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnName_Sortable_get(swigCPtr);
      GnSortable ret = (cPtr == IntPtr.Zero) ? null : new GnSortable(cPtr, true);
      return ret;
    } 
  }

  public string Display {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnName_Display_get(swigCPtr) );
	} 

  }

  public string Prefix {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnName_Prefix_get(swigCPtr) );
	} 

  }

  public string Family {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnName_Family_get(swigCPtr) );
	} 

  }

  public string Given {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnName_Given_get(swigCPtr) );
	} 

  }

  public string GlobalId {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnName_GlobalId_get(swigCPtr) );
	} 

  }

}

}
