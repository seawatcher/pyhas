/*
 * Copyright (c) 2012 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *    Name: code_snippets/musicid_locale2/main.c
 *    Description: Gets a Locale-Dependent Values for the Default Locale and Another Locale
 */

/*
 * Warning
 *
 * The GNSDK package provides three kinds of code examples: sample applications,
 * reference applications, and code snippets.We strongly recommend you use the sample
 * and reference applications as a basis for any applications you develop.
 *
 * The code snippets are located in the /samples/code_snippets folder. They are provided
 * "as is" for documentation purposes only. Their intention is to illustrate a specific
 * function or feature. Although they have been compiled in Windows x64, they lack certain
 * basic functionality and have not been validated for all platforms.
 */


#define GNSDK_MUSICID    1
#include "gnsdk.h"

/* Standard C headers - used by the sample app, but not required for GNSDK */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**********************************************
 *    Local Function Declarations
 **********************************************/
static int
_init_gnsdk(
    gnsdk_cstr_t            client_id,
    gnsdk_cstr_t            client_id_tag,
    gnsdk_cstr_t            client_id_app_version,
    gnsdk_cstr_t            license_path,
    gnsdk_user_handle_t*    p_user_handle
    );

static
void _shutdown_gnsdk(
    gnsdk_user_handle_t     user_handle,
    const char*             client_id
    );

static int _change_locale(gnsdk_user_handle_t user_handle);

/******************************************************************
 *
 *    MAIN
 *
 ******************************************************************/
int main(int argc, char* argv[])
{
    gnsdk_user_handle_t        user_handle          = GNSDK_NULL;
    const char*                client_id            = NULL;
    const char*                client_id_tag        = NULL;
    const char*                client_app_version   = "1"; /* Increment with each version of your app */
    const char*                license_path         = NULL;
    int                        rc                   = 0;

    /*    Client ID, Client ID Tag and License file must be passed in */
    if (argc == 4)
    {
        client_id        = argv[1];
        client_id_tag    = argv[2];
        license_path     = argv[3];

        /* Initialize GNSDK     */
        rc = _init_gnsdk(
                client_id,
                client_id_tag,
                client_app_version,
                license_path,
                &user_handle
                );

        if (0 == rc)
        {

            /* Set locale */
             _change_locale(user_handle);

            /* Clean up and shutdown */
            _shutdown_gnsdk(user_handle, client_id);
        }
    }
    else
    {
        printf("\nUsage:\n%s clientid clientidtag license\n", argv[0]);
        rc = -1;
    }

    return rc;

}    /* main() */


/******************************************************************
 *
 *    _DISPLAY_GNSDK_PRODUCT_INFO
 *
 *    Display product version information
 *
 ******************************************************************/
static void _display_gnsdk_product_info(void)
{
    /* Display GNSDK Version infomation */
    printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk_manager_get_product_version(), gnsdk_manager_get_build_date());

}    /* _display_gnsdk_product_info() */


/******************************************************************
 *
 *    _DISPLAY_LAST_ERROR
 *
 *    Echo the error and information.
 *
 *****************************************************************/
static void
_display_last_error(
	int				line_num
	)
{
	/* Get the last error information from the SDK */
	const gnsdk_error_info_t*       error_info = gnsdk_manager_error_info();

	/* Error_info will never be GNSDK_NULL.
	 * The SDK will always return a pointer to a populated error info structure.
	 */
	printf(
		"\nerror from: %s()  [on line %d]\n\t0x%08x %s",
		error_info->error_api,
		line_num,
		error_info->error_code,
		error_info->error_description
		);

} /* display_last_error() */


/*****************************************************************
 *
 *   _DO_MUSICID_LOOKUP
 *
 *****************************************************************/
static void
_do_musicid_lookup(
    gnsdk_user_handle_t user_handle,
    gnsdk_gdo_handle_t* p_album_gdo
    )
{
    gnsdk_error_t                    error                = GNSDK_SUCCESS;
    gnsdk_musicid_query_handle_t    query_handle        = GNSDK_NULL;
    gnsdk_gdo_handle_t                input_gdo            = GNSDK_NULL;
    gnsdk_gdo_handle_t                response_gdo        = GNSDK_NULL;

    error = gnsdk_musicid_query_create(user_handle,GNSDK_NULL,    GNSDK_NULL, &query_handle);
    if (error != GNSDK_SUCCESS)
    {
        _display_last_error(__LINE__);
        return;
    }

    error =                             gnsdk_manager_gdo_deserialize("WEcxAwpU57x6ijap4RVINrN7Uui/UEzqC7RJqnLkGzfPBYPZAIQR3ZNCbN78yxFd+uKAIOeqqVYNQSg90E9jWpzS3dCNtKaLSmSQQGJFVEtwdIe6tCPYme+bCSsN9qzj1xSnrNTwwf5HUof9PPlX4d71JWGUczplR50Bh4I2b4tvmW2xpdZ1O91gMfxVZYU=", &input_gdo);
    if (error != GNSDK_SUCCESS)
    {
        _display_last_error(__LINE__);
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_musicid_query_set_gdo(query_handle, input_gdo);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_musicid_query_find_albums(query_handle,&response_gdo);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        error = gnsdk_manager_gdo_child_get(response_gdo, GNSDK_GDO_CHILD_ALBUM, 1, p_album_gdo);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    gnsdk_manager_gdo_release(input_gdo);
    gnsdk_manager_gdo_release(response_gdo);
    gnsdk_musicid_query_release(query_handle);

    return;

} /* _do_musicid_lookup() */


/******************************************************************
 *
 *   _CHANGE_LOCALE
 *
 ******************************************************************/
static int _change_locale(gnsdk_user_handle_t user_handle)
{
    gnsdk_error_t             error                   = GNSDK_SUCCESS;
    gnsdk_locale_handle_t     eng_locale_handle       = GNSDK_NULL;
    gnsdk_locale_handle_t     chinese_locale_handle   = GNSDK_NULL;
    gnsdk_gdo_handle_t        work_gdo                = GNSDK_NULL;
    gnsdk_cstr_t              value                   = GNSDK_NULL;



    /* Load an English locale as the default locale to be used when an explicit locale is not set */
    printf("\nLoading locale : English\n");
    error = gnsdk_manager_locale_load(
                GNSDK_LOCALE_GROUP_MUSIC,
                GNSDK_LANG_ENGLISH,              // English
                GNSDK_REGION_GLOBAL,             // Default video region is US
                GNSDK_DESCRIPTOR_DETAILED,       // Default video descriptor is 'detailed'
                user_handle,                     // User handle from the init
                GNSDK_NULL,                      // No status callback
                GNSDK_NULL,                      // No status userdata
                &eng_locale_handle
                );

    if(GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
    }

    if (error == GNSDK_SUCCESS)
    {
        /* Load a Chinese locale so it is available if needed */
        printf("\nLoading locale : Chinese Simplified\n");
        error = gnsdk_manager_locale_load(
                    GNSDK_LOCALE_GROUP_MUSIC,
                    GNSDK_LANG_CHINESE_SIMP,
                    GNSDK_REGION_DEFAULT,
                    GNSDK_DESCRIPTOR_DETAILED,
                    user_handle,
                    GNSDK_NULL,
                    GNSDK_NULL,
                    &chinese_locale_handle
                    );
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {
        /* Music ID lookup */
        _do_musicid_lookup(user_handle, &work_gdo);
    }

    if (error == GNSDK_SUCCESS)
    {
        /* Fetch the  value in English */
        error = gnsdk_manager_gdo_set_locale(work_gdo, eng_locale_handle);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {

        /* Grab a locale-specific value in our default locale (English) */
        error = gnsdk_manager_gdo_value_get(work_gdo, GNSDK_GDO_VALUE_GENRE_LEVEL1, 1, &value);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
        else
        {
            printf("\nPrimary genre (English): %s\r\n", value);
        }
    }

    if (error == GNSDK_SUCCESS)
    {

        /* Load Chinese locale*/
        error = gnsdk_manager_gdo_set_locale(work_gdo, chinese_locale_handle);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
    }

    if (error == GNSDK_SUCCESS)
    {

        /* Now fetch the same value in Chinese */
        error = gnsdk_manager_gdo_value_get(work_gdo,GNSDK_GDO_VALUE_GENRE_LEVEL1, 1,&value);
        if (error != GNSDK_SUCCESS)
        {
            _display_last_error(__LINE__);
        }
        else
        {
            printf("\nPrimary genre (Chinese): %s\r\n", value);
        }
    }

    /* Cleanup and Shutdown*/
    gnsdk_manager_gdo_release(work_gdo);
    gnsdk_manager_locale_release(eng_locale_handle);
    gnsdk_manager_locale_release(chinese_locale_handle);

    return 0;

} /* _change_locale() */


/*****************************************************************************************
 *
 *    _GET_USER_HANDLE
 *
 *    Load existing user handle, or register new one.
 *
 *    GNSDK requires a user handle instance to perform queries.
 *    User handles encapsulate your Gracenote provided Client ID which is unique for your
 *    application. User handles are registered once with Gracenote then must be saved by
 *    your application and reused on future invocations.
 *
 *******************************************************************************************/
static int
_get_user_handle(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_error_t        error               = GNSDK_SUCCESS;
    gnsdk_user_handle_t  user_handle         = GNSDK_NULL;
    char*                user_filename       = NULL;
    size_t               user_filename_len   = 0;
    int                  rc                  = 0;
    FILE*                file                = NULL;


    user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
    user_filename = malloc(user_filename_len);

    if (NULL != user_filename)
    {
        strcpy(user_filename,client_id);
        strcat(user_filename,"_user.txt");

        /* Do we have a user saved locally? */
        file = fopen(user_filename, "r");
        if (NULL != file)
        {
            gnsdk_char_t serialized_user_string[1024] = {0};

            if (NULL != (fgets(serialized_user_string, 1024, file)))
            {
                /* Create the user handle from the saved user */
                error = gnsdk_manager_user_create(serialized_user_string, &user_handle);
                if (GNSDK_SUCCESS != error)
                {
                    _display_last_error(__LINE__);
                    rc = -1;
                }
            }
            else
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
            fclose(file);
        }
        else
        {
            printf("\nInfo: No stored user - this must be the app's first run.\n");
        }

        /* If not, create new one */
        if (GNSDK_NULL == user_handle)
        {
            error = gnsdk_manager_user_create_new(
                        client_id,
                        client_id_tag,
                        client_app_version,
                        &user_handle
                        );
            if (GNSDK_SUCCESS != error)
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
        }

        free(user_filename);
    }
    else
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    if (rc == 0)
    {
        *p_user_handle = user_handle;
    }

    return rc;

} /* _get_user_handle() */


/****************************************************************************************
 *
 *    _INIT_GNSDK
 *
 *     Initializing the GNSDK is required before any other APIs can be called.
 *     First step is to always initialize the Manager module, then use the returned
 *     handle to initialize any modules to be used by the application.
 *
 *     For this sample, we also load a locale which is used by GNSDK to provide
 *     appropriate locale-sensitive metadata for certain metadata values. Loading of the
 *     locale is done here for sample convenience but can be done at anytime in your
 *     application.
 *
 ****************************************************************************************/
static int _init_gnsdk(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    const char*             license_path,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_manager_handle_t   sdkmgr_handle    = GNSDK_NULL;
    gnsdk_error_t            error            = GNSDK_SUCCESS;
    gnsdk_user_handle_t      user_handle      = GNSDK_NULL;
    int                      rc               = 0;

    /* Display GNSDK Product Version Info */
    _display_gnsdk_product_info();

    /* Initialize the GNSDK Manager */
    error = gnsdk_manager_initialize(
                &sdkmgr_handle,
                license_path,
                GNSDK_MANAGER_LICENSEDATA_FILENAME
                );

    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }


    /* Initialize the MusicID SDK */
    error = gnsdk_musicid_initialize(sdkmgr_handle);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    /* Get a user handle for our client ID.  This will be passed in for all queries */
    if (0 == rc)
    {
        rc = _get_user_handle(client_id, client_id_tag, client_app_version, &user_handle );
    }

    if (0 != rc)
    {
        /* Clean up on failure. */
        _shutdown_gnsdk(user_handle, client_id);
    }
    else
    {
        /* Return the User handle for use at query time */
        *p_user_handle = user_handle;
    }

    return rc;

}    /* _init_gnsdk() */


/***************************************************************************
 *
 *    _SHUTDOWN_GNSDK
 *
 *        Call shutdown on all initialized GNSDK modules.
 *        Release all existing handles before shutting down any of the modules.
 *        Shutting down the Manager module should occur last, but the shutdown
 *        ordering of all other modules does not matter.
 *
 ***************************************************************************/
static void _shutdown_gnsdk(
    gnsdk_user_handle_t        user_handle,
    const char*                client_id
    )
{
    gnsdk_error_t    error                          = GNSDK_SUCCESS;
    gnsdk_str_t      updated_serialized_user_string = GNSDK_NULL;
    char*            user_filename                  = GNSDK_NULL;
    size_t           user_filename_len              = 0;
    int              rc                             = 0;
    int              return_value                   = 0;
    FILE*            file                           = NULL;

    /* Release our user handle and see if we need to update our stored version */
    error = gnsdk_manager_user_release(user_handle, &updated_serialized_user_string);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
    }
    else if (GNSDK_NULL != updated_serialized_user_string)
    {
        user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
        user_filename = malloc(user_filename_len);

        if (NULL != user_filename)
        {
            strcpy(user_filename,client_id);
            strcat(user_filename,"_user.txt");

            file = fopen(user_filename, "w");
            if (NULL != file)
            {
                return_value = fputs(updated_serialized_user_string, file);
                if (0 > return_value)
                {
                    printf("\nError writing user registration file from buffer.\n");
                    rc = -1;
                }
                fclose(file);
            }
            else
            {
                printf("\nError: Failed to open the user filename for use in saving the updated serialized user. (%s)\n", user_filename);
            }
            free(user_filename);
        }
        else
        {
            printf("\nError: Failed to allocated user filename for us in saving the updated serialized user.\n");
        }
        gnsdk_manager_string_free(updated_serialized_user_string);

    } /* else if (GNSDK_NULL != updated_serialized_user_string) */

    /* Shutdown the libraries */
    gnsdk_manager_shutdown();

}    /* _shutdown_gnsdk() */
