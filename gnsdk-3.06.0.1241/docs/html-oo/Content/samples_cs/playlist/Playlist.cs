﻿/*
* Copyright (c) 2000-2013 Gracenote.
*
* This software may not be used in any way or distributed without
* permission. All rights reserved.
*
* Some code herein may be covered by US and international patents.
*/

/*
*	Name: playlist
*	Description:
*	Playlist is the GNSDK library that generates playlists when integrated with the GNSDK MusicID or MusicID-File
*	library (or both). Playlist APIs enable an application to:
*	01. Create, administer, populate, and synchronize a collection summary.
*	02. Store a collection summary within a local storage solution.
*	03. Validate PDL statements.
*	04. Generate Playlists using either the More Like This function or the general playlist generation function.
*	05. Manage results.
*	Streamline your Playlist implementation by using the provided More Like This function
*	(gnsdk_playlist_generate_morelikethis), which contains the More Like This algorithm to generate
*	optimal playlist results and eliminates the need to create and validate Playlist Definition
*	Language statements.
*	Steps:
*	See inline comments below.
*
*	Command-line Syntax:
 *	sample clientId clientTag license libraryPath
*/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using GracenoteSDK;
namespace Sample
{
    class Playlist
    {
        private static bool useLocal = false;
		private static GnUser user = null;

        private static string localDB = "../../../sample_db";

        static void
                     Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                String clientId = args[0];
                String clientIdTag = args[1];
                String licenseFile = args[2];
                String lib_path = args[3];
                String applicationVersion = "1.0.0.0";

                try
                {
                    /* GNSDK initialization */
                    GnSDK gnsdk = new GnSDK(lib_path, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                    Console.WriteLine("GNSDK(Product Version)       : " + gnsdk.ProductVersion);
                    Console.WriteLine("GNSDK(Build Date) : " + gnsdk.BuildDate);

                    gnsdk.LoggingEnable(
                        "sample.log",
                        GnSDK.GN_LOG_PKG_ALL,
                        GnSDK.GN_LOG_LEVEL_ERROR,
                        GnSDK.GN_LOG_OPTION_ALL,
                        0,                                /* Max size of log: 0 means a new log file will be created each run */
                        false);                           /* true = old logs will be renamed and saved */



                    user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

                    // Initialize the Storage SQLite Library
                    GnStorageSqlite gnStorage = new GnStorageSqlite();

                    if (useLocal)
                    {
                        

                        gnStorage.StorageFolder = localDB;

                        GnLookupLocal gnLookupLocal = new GnLookupLocal();

                        //Set the user option to use our local Gracenote DB unless overridden			            
                        user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                        // Display information about our local EDB 
                        DisplayEmbeddedDbInfo(gnLookupLocal);

                    }
                    else
                    {
                        user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                    }


                    //  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
                    
					GnLocale locale = new GnLocale(GnSDK.kLocaleGroupPlaylist,
												   GnSDK.kLanguageEnglish,
												   GnSDK.kRegionDefault,
												   GnSDK.kDescriptorSimplified,
												   user,
													null);
					
					gnsdk.SetDefaultLocale(locale);
                    

                    // Perform a sample album TOC query
                    PerformSamplePlaylist(user);
                }

                // All gracenote sdk objects throws GnException
                catch (GnException e)
                {
                    Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                    Console.WriteLine("Error API            :: " + e.ErrorAPI);
                    Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                    Console.WriteLine("SourceE rror Module  :: " + e.SourceErrorModule);
                }
			
            }
        }
		
	
        /***************************************************************************
         *
         *    PerformSamplePlaylist
         *
         *    Our top level function that calls the required Playlist routines
         *
         ***************************************************************************/
        private static void
        PerformSamplePlaylist(GnUser user)
        {
            GnPlaylistCollection playlistCollection = PlaylistCollectionCreate(user);

            PlaylistAttributeEnum(playlistCollection);

            /* demonstrate PDL usage */
            DoPlaylistPdl(user, playlistCollection);

            /* demonstrate MoreLike usage*/
            DoPlaylistMorelikethis(user, playlistCollection);
        }

        /***************************************************************************
         *
         *    DoPlaylistMorelikethis
         *
         *    The following illustrates how to get the various morelikethis options.
         *
         ***************************************************************************/
        private static void
        DoPlaylistMorelikethis(GnUser user, GnPlaylistCollection playlistCollection)
        {
            /*
             A seed gdo can be any recognized media gdo.
             In this example we are using the a gdo from a random track in the playlist collection summary
             */
            GnPlaylistIdentifier identifier = playlistCollection.MediaIdentifiers.at(3).Current;

            GnPlaylistMetadata data = playlistCollection.Metadata(user, identifier);

            Console.WriteLine("\n MoreLikeThis tests\n MoreLikeThis Seed details:");

            PlaylistGetAttributeValue(playlistCollection, data);

            /* Generate a more Like this with the default settings */
            Console.WriteLine("\n MoreLikeThis with Default Options \n");

            /* Print the default More Like This options */
            PrintPlaylistMorelikethisOptions(playlistCollection);

            GnPlaylistResult playlistResult = playlistCollection.GenerateMoreLikeThis(user, data);

            EnumeratePlaylistResults(user, playlistCollection, playlistResult);

            /* Generate a more Like this with the custom settings */
            Console.WriteLine("\n MoreLikeThis with Custom Options \n");

            /* Change the possible result set to be a maximum of 30 tracks.*/
            playlistCollection.MoreLikeThisOptionSet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxTracks, 30);         

            /* Change the max per artist to be 20 */
            playlistCollection.MoreLikeThisOptionSet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxPerArtist, 10);

            /* Change the max per album to be 5 */
            playlistCollection.MoreLikeThisOptionSet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxPerAlbum, 5);       

            /* Change the random result to be 1 so that there is no randomization*/
            playlistCollection.MoreLikeThisOptionSet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisRandom, 1);       

            /* Print the customized More Like This options */
            PrintPlaylistMorelikethisOptions(playlistCollection);

            playlistResult = playlistCollection.GenerateMoreLikeThis(user, data);

            EnumeratePlaylistResults(user, playlistCollection, playlistResult);
        }


        /***************************************************************************
         *
         *    PrintPlaylistMorelikethisOptions
         *
         *    The following illustrates how to get the various morelikethis options.
         *
         ***************************************************************************/
        private static void
        PrintPlaylistMorelikethisOptions(GnPlaylistCollection playlistCollection)
        {
            uint value;

            value = playlistCollection.MoreLikeThisOptionGet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxTracks);              
            Console.WriteLine(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_TRACKS :" + value);

            value = playlistCollection.MoreLikeThisOptionGet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxPerArtist);              
            Console.WriteLine(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ARTIST :" + value);

            value = playlistCollection.MoreLikeThisOptionGet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisMaxPerAlbum);              
            Console.WriteLine(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ALBUM :" + value);

            value = playlistCollection.MoreLikeThisOptionGet(GnPlaylistCollection.GenerateMoreLikeThisOption.kMoreLikeThisRandom);              
            Console.WriteLine(" GNSDK_PLAYLIST_MORELIKETHIS_OPTION_RANDOM :" + value);

        }

        /***************************************************************************
         *
         *    DoPlaylistPdl
         *
         *    Here we perform playlist generation from our created Collection Summary via
         *    custom PDL (Playlist Definition Language) statements.
         *
         ***************************************************************************/
        private static void
        DoPlaylistPdl(GnUser user, GnPlaylistCollection playlistCollection)
        {
            uint stmtIdx = 0;
            uint pdlstmtCount = 0;
          
            string[] pdlStatements =
	            {
		            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 0",   	/* like pop with a low score threshold (0)*/
		            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 300", 	/* like pop with a reasonable score threshold (300)*/
		            "GENERATE PLAYLIST WHERE GN_Genre = 2929", 				/* exactly pop */
		            "GENERATE PLAYLIST WHERE GN_Genre = 2821",    			/* exactly rock */
		            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 0",     /* like rock with a low score threshold (0)*/
		            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 300",  	/* like rock with a reasonable score threshold (300)*/
		            "GENERATE PLAYLIST WHERE (GN_Genre LIKE SEED) > 300 LIMIT 20 RESULTS",
		            "GENERATE PLAYLIST WHERE (GN_ArtistName LIKE 'Green Day') > 300 LIMIT 20 RESULTS, 2 PER GN_ArtistName;",
	            };
            GnPlaylistMetadata gdoSeed = null;

            pdlstmtCount = (uint)pdlStatements.Count();
			gdoSeed = playlistCollection.Metadata(user, playlistCollection.MediaIdentifiers.at(4).Current);
            for (stmtIdx = 0; stmtIdx < pdlstmtCount; stmtIdx++)
            {
                Console.WriteLine("\n PDL " + stmtIdx + " : " + pdlStatements[stmtIdx]);

                if (playlistCollection.TryValidateStatement(pdlStatements[stmtIdx]))
				{

					/*
						A seed gdo can be any recognized media gdo.
						In this example we are using the a gdo from a track in the playlist collection summary
					*/
					GnPlaylistResult playlistResult = playlistCollection.GeneratePlaylist(user, pdlStatements[stmtIdx], gdoSeed);

					EnumeratePlaylistResults(user, playlistCollection, playlistResult);
					}

            }
        }

        /***************************************************************************
         *
         *    EnumeratePlaylistResults
         *
         *    The following illustrates how to get each  ident and its associated
         *    GDO from a results handle.
         *
         ***************************************************************************/
        private static void
        EnumeratePlaylistResults(GnUser user, GnPlaylistCollection playlistCollection, GnPlaylistResult playlistResult)
        {
            GnPlaylistMetadata gdoAttr = null;
            string ident = null;
            string collectionName = null;
            uint countOrdinal = 0;
            uint resultsCount = 0;
            GnPlaylistCollection tempCollection = null;

            resultsCount = playlistResult.Identifiers.count();

            Console.WriteLine("Generated Playlist : " + resultsCount);

            GnPlaylistResultIdentEnumerable playlistResultIdentEnumerable = playlistResult.Identifiers;
            foreach (GnPlaylistIdentifier playlistIdentifier in playlistResultIdentEnumerable)
            {
                collectionName = playlistIdentifier.CollectionName;
                ident = playlistIdentifier.MediaIdentifier;

                Console.Write("\n\t" + ++countOrdinal + "  : " + ident + " Collection Name:" + collectionName);

                /*	The following illustrates how to get a collection handle
                    from the collection name string in the results enum function call.
                    It ensures that Joined collections as well as non joined collections will work with minimal overhead.
                    */

                tempCollection = playlistCollection.JoinSearchByName(collectionName);

                gdoAttr = tempCollection.Metadata(user, playlistIdentifier);

                PlaylistGetAttributeValue(tempCollection, gdoAttr);

            }

        }

        /***************************************************************************
         *
         *    PlaylistGetAttributeValue
         *
         ***************************************************************************/

        private static void
        PlaylistGetAttributeValue(GnPlaylistCollection temp_collection, GnPlaylistMetadata gdoAttr)
        {
            Console.WriteLine("\n\t\tGN_AlbumName:" + gdoAttr.AlbumName
                            + "\n\t\tGN_ArtistName:" + gdoAttr.ArtistName
                            + "\n\t\tGN_ArtistType:" + gdoAttr.ArtistType
                            + "\n\t\tGN_Era:" + gdoAttr.Era
                            + "\n\t\tGN_Genre:" + gdoAttr.Genre
                            + "\n\t\tGN_Origin:" + gdoAttr.Origin
                            + "\n\t\tGN_Mood:" + gdoAttr.Mood
                            + "\n\t\tGN_Tempo:" + gdoAttr.Tempo);

        }

        /***************************************************************************
         *
         *    PlaylistCollectionCreate
         *
         *    Here we attempt to load an existing Playlist Collection Summary from the GNSDK
         *    storage if one was previously stored. If not, we create a new Collection Summary
         *    (which initially is empty) and populate it with media. We then store this
         *    Collection Summary in the GNSDK storage.
         *
         ***************************************************************************/
        private static GnPlaylistCollection
        PlaylistCollectionCreate(GnUser user)
        {
            /* Initialize the Playlist Library */
            GnPlaylist playlist = new GnPlaylist();

            /* Specify the location for our collection store. */
            playlist.SetStorageLocation(".");

            GnPlaylistCollection playlistCollection = new GnPlaylistCollection();
            uint count = playlist.StoredCollectionsCount();

            Console.WriteLine("\nCurrently stored collections : " + count);

            if (count == 0)
            {
                Console.WriteLine("\nCreating a new collection");
                playlistCollection = playlist.CreateCollection("sample_collection");

                PlaylistCollectionPopulate(user, playlistCollection);

                Console.WriteLine("\nStoring collection... ");
                playlist.StoreCollection(playlistCollection);

            }
			
            /* get the count again */
            count = playlist.StoredCollectionsCount();
            Console.WriteLine("\nCurrently stored collections : " + count);

            // get the first collection . 
            string collectionName = playlist.StoredCollectionNameAt(0);
			
            playlistCollection = playlist.LoadCollection(collectionName);

            Console.WriteLine("\n Loading Collection " + collectionName + " from store");

            return playlistCollection;
        }

        /***************************************************************************
         *
         *    PlaylistCollectionPopulate
         *
         *    Here we are doing basic MusicID queries on CDTOCs to populate a
         *    Playlist Collection Summary.
         *    Any GNSDK result GDO can be used for Collection Summary
         *    population.
         *
         ***************************************************************************/
        private static void
        PlaylistCollectionPopulate(GnUser user, GnPlaylistCollection playlistCollection)
        {
            uint countOrdinal = 0;
            string[] inputQueryTocs =  {
		        "150 13224 54343 71791 91348 103567 116709 132142 141174 157219 175674 197098 238987 257905",
		        "182 23637 47507 63692 79615 98742 117937 133712 151660 170112 189281",
		        "182 14035 25710 40955 55975 71650 85445 99680 115902 129747 144332 156122 170507",
		        "150 10705 19417 30005 40877 50745 62252 72627 84955 99245 109657 119062 131692 141827 152207 164085 173597 187090 204152 219687 229957 261790 276195 289657 303247 322635 339947 356272",
		        "150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342",
		        "150 1307 15551 31744 45022 57486 72947 85253 100214 115073 128384 141948 152951 167014",
		        "183 69633 96258 149208 174783 213408 317508",
		        "150 19831 36808 56383 70533 87138 105157 121415 135112 151619 169903 189073",
		        "182 10970 29265 38470 59517 74487 83422 100987 113777 137640 150052 162445 173390 196295 221582",
		        "150 52977 87922 128260 167245 187902 215777 248265",
		        "183 40758 66708 69893 75408 78598 82983 87633 91608 98690 103233 108950 111640 117633 124343 126883 132298 138783 144708 152358 175233 189408 201408 214758 239808",
		        "150 92100 135622 183410 251160 293700 334140",
		        "150 17710 33797 65680 86977 116362 150932 166355 183640 193035",
		        "150 26235 51960 73111 93906 115911 142086 161361 185586 205986 227820 249300 277275 333000",
		        "150 1032 27551 53742 75281 96399 118691 145295 165029 189661 210477 232501 254342 282525",
		        "150 26650 52737 74200 95325 117675 144287 163975 188650 209350 231300 253137 281525 337875",
		        "150 19335 35855 59943 78183 96553 111115 125647 145635 163062 188810 214233 223010 241800 271197",
		        "150 17942 32115 47037 63500 79055 96837 117772 131940 148382 163417 181167 201745",
		        "150 17820 29895 41775 52915 69407 93767 105292 137857 161617 171547 182482 204637 239630 250692 282942 299695 311092 319080",
		        "182 21995 45882 53607 71945 80495 94445 119270 141845 166445 174432 187295 210395 230270 240057 255770 277745 305382 318020 335795 356120",
		        "187 34360 64007 81050 122800 157925 195707 230030 255537 279212 291562 301852 310601",
		        "150 72403 124298 165585 226668 260273 291185"
	        };



            int count = inputQueryTocs.Count();

            Console.WriteLine("\nPopulating Collection Summary from sample TOCs");
            /* Create the query handle */
            GnMusicID musicid = new GnMusicID(user);

            /* Set the option for retrieving DSP attributes.
            Note: Please note that these attributes are entitlements to your user id. You must have them enabled in your licence.
            */
            musicid.OptionLookupData(GnLookupData.kLookupDataSonicData, true);
            /*
            Set the option for retrieving Playlist attributes.
            Note: Please note that these attributes are entitlements to your user id. You must have them enabled in your licence.
            */
            musicid.OptionLookupData(GnLookupData.kLookupDataPlaylist, true);

            if (useLocal)
            {
                Console.WriteLine(" LOCAL lookup...");
                musicid.OptionLookupMode(GnLookupMode.kLookupModeLocal);
            }
            else
                Console.WriteLine(" ONLINE lookup...");

            foreach (string toc in inputQueryTocs)
            {
                // Find album
                GnResponseAlbums responseAlbums = musicid.FindAlbums(toc);

                GnAlbum album = responseAlbums.Albums.at(0).Current;

                /*  Add MusicID result to Playlist   */
                uint trackCount = album.Tracks.count();

                /*  To make playlist of tracks we add each and every track in the album to the collection summary */
                for (uint trackOrdinal = 1; trackOrdinal <= trackCount; ++trackOrdinal)
                {
                    GnTrack track = album.Tracks.at(trackOrdinal - 1).Current;


                    /* create a unique ident for every track that is added to the playlist.
                       Ideally the ident allows for the identification of which track it is.
                       e.g. path/filename.ext , or an id that can be externally looked up.
                    */
                    string uniqueIdent = countOrdinal + "_" + trackOrdinal;
                    /*
                        Add the the Album and Track GDO for the same ident so that we can
                        query the Playlist Collection with both track and album level attributes.
                    */
                    playlistCollection.Add(uniqueIdent, album);
                    playlistCollection.Add(uniqueIdent, track);

                    Console.Write("..");
                }
                countOrdinal++;
            }


            /* count the albums recognized */
            uint identCount = playlistCollection.MediaIdentifiers.count();
            Console.WriteLine("\n Recognized " + identCount + " tracks out of " + count + " Album TOCS");

            Console.WriteLine("\n Finished Recognition");
        }


        /***************************************************************************
         *
         *     PlaylistAttributeEnum
         *
         *     Here we display the Playlist Attributes supported by this version of GNSDK Playlist
         *     as well as the Playlist Attributes currently employed in the Collection Summary.
         *     The sets of Attributes in these cases may differ if the Collection Summary was
         *     generated by a different version of GNSDK (however such differences will occur
         *     infrequently if ever).
         *
         ***************************************************************************/
        private static void
        PlaylistAttributeEnum(GnPlaylistCollection playlistCollection)
        {
            string collectionName = null;
            uint identCount = 0;

            // TODO
            // Console.WriteLine("\nAll Playlist Attributes:\n");
            // gnsdk_playlist_attributes_enum


            identCount = playlistCollection.MediaIdentifiers.count();

            collectionName = playlistCollection.Name;

            Console.WriteLine("Collection Attributes: \'" + collectionName + "\' (" + identCount + " idents)\n");


            // TODO
            // gnsdk_playlist_collection_attributes_enum


        }  /* PlaylistAttributeEnum() */


        // Display local Gracenote DB information.
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }




        // Callback delegate called when loading locale
        public class LocaleEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t locale_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("Status ");
                switch (locale_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Begin  ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write("Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }

            public override bool cancel_check()
            {
                return false;
            }
        };

        // Load existing user, or register new one.

        // GNSDK requires a user instance to perform queries.
        // User encapsulates your Gracenote provided Client ID which is unique for your
        // application. Users  are registered once with Gracenote then must be saved by
        // your application and reused on future invocations.

        static GnUser
        GetUser( GnSDK sdk, string clientId, string clientIdTag, string applicationVersion)
        {
            GnUser user;

            // check file for existence
            if (!File.Exists("user.txt"))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.\n");

				GnString serializedUserString = sdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion);
				using (StreamWriter outfile = new StreamWriter("user.txt"))
				{
					outfile.Write(serializedUserString.ToString());
					outfile.Close();
				}
				user = new GnUser(serializedUserString.ToString(), clientId, clientIdTag, applicationVersion);
            }
            else
            {
                using (StreamReader sr = new StreamReader("user.txt"))
                {
                    String serializedUserString = sr.ReadToEnd();
                    user = new GnUser(serializedUserString, clientId, clientIdTag,applicationVersion );
                }
            }

            return user;
        }

       
    }
}
