var group___video___search_fields =
[
    [ "GNSDK_VIDEO_SEARCH_FIELD_ALL", "group___video___search_fields.html#ga925f3a0ace972656d6426dea1bc5410c", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_CHARACTER_NAME", "group___video___search_fields.html#ga025f9c793ef619332943ad8630268ab0", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_CONTRIBUTOR_NAME", "group___video___search_fields.html#gaefb134bb319916a66ed40a45d824f90c", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_PRODUCT_TITLE", "group___video___search_fields.html#ga08290c02887e53a649c3ef958c4186dc", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE", "group___video___search_fields.html#ga534a4fa6f8f9f50f7ab9353892d9de96", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_WORK_FRANCHISE", "group___video___search_fields.html#gad99f3e7432c6be8d9e78effdc2fad40c", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_WORK_SERIES", "group___video___search_fields.html#gab8161175904162218ddc2bebc51355d9", null ],
    [ "GNSDK_VIDEO_SEARCH_FIELD_WORK_TITLE", "group___video___search_fields.html#ga7a21fb19f891dbbcbc076b99e735cddd", null ]
];