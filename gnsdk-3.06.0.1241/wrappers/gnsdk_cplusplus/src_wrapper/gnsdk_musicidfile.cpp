/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* musicidfile.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */
#include "gnsdk.hpp"
#include "metadata.hpp"
#include "metadata_music.hpp"

#if GNSDK_MUSICID_FILE

using namespace gracenote;
using namespace gracenote::musicid_file;

/**************************************************************************
** GnMusicIDFile
*/

/* static varibale definitions *//*
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_RESPONSE_ALBUMS	=  GNSDK_MUSICIDFILE_QUERY_FLAG_RESPONSE_ALBUMS;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_RESPONSE_MATCHES	=  GNSDK_MUSICIDFILE_QUERY_FLAG_RESPONSE_MATCHES;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_RETURN_SINGLE		=  GNSDK_MUSICIDFILE_QUERY_FLAG_RETURN_SINGLE;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_RETURN_ALL		=  GNSDK_MUSICIDFILE_QUERY_FLAG_RETURN_ALL;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_AGGRESSIVE		=  GNSDK_MUSICIDFILE_QUERY_FLAG_AGGRESSIVE;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_ASYNC				=  GNSDK_MUSICIDFILE_QUERY_FLAG_ASYNC;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_NO_THREADS		=  GNSDK_MUSICIDFILE_QUERY_FLAG_NO_THREADS;
 * gnsdk_uint32_t  GnMusicIDFile::GN_MUSICIDFILE_QUERY_FLAG_DEFAUL			=  GNSDK_MUSICIDFILE_QUERY_FLAG_DEFAULT;
 *
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_THREADPRIORITY			= GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_BATCH_SIZE				= GNSDK_MUSICIDFILE_OPTION_BATCH_SIZE;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_LOOKUP_MODE				= GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_CLASSICAL_DATA		= GNSDK_MUSICIDFILE_OPTION_ENABLE_CLASSICAL_DATA;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_GLOBALIDS			= GNSDK_MUSICIDFILE_OPTION_ENABLE_GLOBALIDS;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_SONIC_DATA			= GNSDK_MUSICIDFILE_OPTION_ENABLE_SONIC_DATA;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_DSP_DATA			= GNSDK_MUSICIDFILE_OPTION_ENABLE_DSP_DATA;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_PLAYLIST			= GNSDK_MUSICIDFILE_OPTION_ENABLE_PLAYLIST;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_EXTERNAL_IDS		= GNSDK_MUSICIDFILE_OPTION_ENABLE_EXTERNAL_IDS;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_CONTENT_DATA		= GNSDK_MUSICIDFILE_OPTION_ENABLE_CONTENT_DATA;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_ENABLE_LINK_DATA			= GNSDK_MUSICIDFILE_OPTION_ENABLE_LINK_DATA;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_PREFERRED_LANG			= GNSDK_MUSICIDFILE_OPTION_PREFERRED_LANG;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_PRIORITY_IDLE		= GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_IDLE;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_PRIORITY_LOW		= GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_LOW;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_PRIORITY_NORM		= GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_NORM;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_PRIORITY_HIGH		= GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_HIGH;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_PRIORITY_DEFAULT	= GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_DEFAULT;
 * gnsdk_cstr_t GnMusicIDFile::GN_MUSICIDFILE_OPTION_VALUE_BATCH_SIZE_DEFAULT	= GNSDK_MUSICIDFILE_OPTION_VALUE_BATCH_SIZE_DEFAULT;*/

gnsdk_uint32_t GnMusicIDFile::kTimeValueInfinite = GNSDK_MUSICIDFILE_TIMEOUT_INFINITE;

GnMusicIDFile::GnMusicIDFile(const GnUser& user, GnMusicIDFileEvents* pEventHandler) throw ( GnError ) :
	queryHandle_(GNSDK_NULL)
{
	gnsdk_error_t                 error;
	gnsdk_musicidfile_callbacks_t callbacks =
	{
		_callback_status,
		_callback_get_fingerprint,
		_callback_get_metadata,
		_callback_result_available,
		_callback_result_not_found,
		_callback_musicid_complete
	};

	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_DSP);
	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_MUSICIDFILE);

	eventHandler_ = pEventHandler;

	error = gnsdk_musicidfile_query_create(user.native(), &callbacks, this, &queryHandle_);
	if(error) { throw GnError(); }
}

GnMusicIDFile::~GnMusicIDFile()
{
	gnsdk_musicidfile_query_release(queryHandle_);
}

/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t GnMusicIDFile::Version()
{
	return gnsdk_musicidfile_get_version();
}

/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t GnMusicIDFile::BuildDate()
{
	return gnsdk_musicidfile_get_build_date();
}

/*-----------------------------------------------------------------------------
 *  OptionLookupMode
 */
void GnMusicIDFile::OptionLookupMode(GnLookupMode lookupMode) throw ( GnError )
{
	gnsdk_error_t error;

	switch(lookupMode)
	{
	case kLookupModeLocal:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_LOCAL);
		break;

	case kLookupModeOnline:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE);
		break;

	case kLookupModeOnlineNoCache:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHE);
		break;

	case kLookupModeOnlineNoCacheRead:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHEREAD);
		break;

	case kLookupModeOnlineCacheOnly:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_CACHEONLY);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionLookupData
 */
void GnMusicIDFile::OptionLookupData(GnLookupData lookupData, bool enable) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_enable = ( enable ) ? GNSDK_VALUE_TRUE : GNSDK_VALUE_FALSE;

	switch(lookupData)
	{
	case kLookupDataContent:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_CONTENT_DATA, sz_enable);
		break;

	case kLookupDataClassical:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_CLASSICAL_DATA, sz_enable);
		break;

	case kLookupDataSonicData:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_SONIC_DATA, sz_enable);
		break;

	case kLookupDataPlaylist:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_PLAYLIST, sz_enable);
		break;

	case kLookupDataExternalIDs:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_EXTERNAL_IDS, sz_enable);
		break;

	case kLookupDataGlobalIDs:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ENABLE_GLOBALIDS, sz_enable);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionSet
 */
void GnMusicIDFile::OptionSet(gnsdk_cstr_t lookupOption, bool enable) throw ( GnError )
{
	gnsdk_error_t error;

	gnsdk_cstr_t  sz_enable = ( enable ) ? GNSDK_VALUE_TRUE : GNSDK_VALUE_FALSE;

	error = gnsdk_musicidfile_query_option_set(queryHandle_, lookupOption, sz_enable);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionPreferredLanguage
 */
void GnMusicIDFile::OptionPreferredLanguage(gnsdk_cstr_t preferredLanguage) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_musicidfile_query_option_set(queryHandle_,
		GNSDK_MUSICIDFILE_OPTION_PREFERRED_LANG,
		preferredLanguage);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionBatchSize
 */
void GnMusicIDFile::OptionBatchSize(gnsdk_cstr_t batchSize) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_musicidfile_query_option_set(queryHandle_,
		GNSDK_MUSICIDFILE_OPTION_BATCH_SIZE,
		batchSize);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionThreadPriority
 */
void GnMusicIDFile::OptionThreadPriority(GnThreadPriority value) throw ( GnError )
{
	gnsdk_error_t error;

	switch(value)
	{
	case kThreadPriorityDefault:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY, GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_DEFAULT);
		break;

	case kThreadPriorityIdle:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY, GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_IDLE);
		break;

	case kThreadPriorityLow:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY, GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_LOW);
		break;

	case kThreadPriorityNormal:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY, GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_NORM);
		break;

	case kThreadPriorityHigh:
		error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_THREADPRIORITY, GNSDK_MUSICIDFILE_OPTION_VALUE_PRIORITY_HIGH);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionOnlineProcessing
 */
void GnMusicIDFile::OptionOnlineProcessing(bool enable) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_enable = ( enable ) ? GNSDK_VALUE_TRUE : GNSDK_VALUE_FALSE;

	error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_ONLINE_PROCESSING, sz_enable);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionSetPreferredXID
 */
void GnMusicIDFile::OptionSetPreferredXID(gnsdk_cstr_t preferredXid) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_query_option_set(queryHandle_, GNSDK_MUSICIDFILE_OPTION_PREFERRED_XID, preferredXid);

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  DoTrackID
 */
void GnMusicIDFile::DoTrackID(GnMidfQueryFlags queryFlags) throw ( GnError )
{
	gnsdk_error_t  error;

	error = gnsdk_musicidfile_query_do_trackid(queryHandle_, (gnsdk_uint32_t)queryFlags);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  DoAlbumID
 */
void GnMusicIDFile::DoAlbumID(GnMidfQueryFlags queryFlags) throw ( GnError )
{
	gnsdk_error_t  error;

	error = gnsdk_musicidfile_query_do_albumid(queryHandle_, (gnsdk_uint32_t)queryFlags);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  DoLibraryID
 */
void GnMusicIDFile::DoLibraryID(GnMidfQueryFlags queryFlags) throw ( GnError )
{
	gnsdk_error_t  error;

	error = gnsdk_musicidfile_query_do_libraryid(queryHandle_, (gnsdk_uint32_t)queryFlags);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  CreateFileInfo
 */
GnMusicIDFileInfo GnMusicIDFile::CreateFileInfo(gnsdk_cstr_t uniqueIdentifier) throw ( GnError )
{
	gnsdk_musicidfile_fileinfo_handle_t fileInfoHandle = GNSDK_NULL;
	gnsdk_error_t                       error;

	error = gnsdk_musicidfile_query_fileinfo_create(queryHandle_, uniqueIdentifier, GNSDK_NULL, GNSDK_NULL, &fileInfoHandle);
	if(error) { throw GnError(); }

	GnMusicIDFileInfo result(this->queryHandle_, fileInfoHandle);
	return result;
}

/*-----------------------------------------------------------------------------
 *  RemoveFileInfo
 */
void GnMusicIDFile::RemoveFileInfo(GnMusicIDFileInfo& fileInfo) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_query_fileinfo_remove(queryHandle_, fileInfo.fileInfohandle_);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  SerializeFileInfosToXml
 */
GnString GnMusicIDFile::SerializeFileInfosToXml() throw ( GnError )
{
	gnsdk_str_t   str;
	gnsdk_error_t error;

	error = gnsdk_musicidfile_query_fileinfo_render_to_xml(queryHandle_, &str);
	if(error) { throw GnError(); }

	GnString retval(str);
	error = gnsdk_manager_string_free(str);
	if(error) { throw GnError(); }
	return retval;
}

/*-----------------------------------------------------------------------------
 *  SetFileInfosFromXml
 */
gnsdk_uint32_t GnMusicIDFile::SetFileInfosFromXml(const char* xmlbuffer) throw ( GnError )
{
	gnsdk_uint32_t count = 0;
	gnsdk_error_t  error;

	error = gnsdk_musicidfile_query_fileinfo_create_from_xml(queryHandle_, xmlbuffer, &count);
	if(error) { throw GnError(); }
	return count;
}

/*-----------------------------------------------------------------------------
 *  _callback_status
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_status(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo_handle, gnsdk_musicidfile_callback_status_t status, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		GnMusicIDFileInfo fileinfo = GnMusicIDFileInfo(query_handle, fileinfo_handle);
		p_midf->eventHandler_->status(fileinfo, status, current_file, total_files);
		if(p_midf->eventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _callback_get_fingerprint
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_get_fingerprint(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo_handle, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		GnMusicIDFileInfo fileinfo = GnMusicIDFileInfo(query_handle, fileinfo_handle);
		p_midf->eventHandler_->get_fingerprint(fileinfo, current_file, total_files);
		if(p_midf->eventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _callback_get_metadata
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_get_metadata(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo_handle, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		GnMusicIDFileInfo fileinfo = GnMusicIDFileInfo(query_handle, fileinfo_handle);
		p_midf->eventHandler_->get_metadata(fileinfo, current_file, total_files);
		if(p_midf->eventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _callback_result_available
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_result_available(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_gdo_handle_t response_gdo, gnsdk_uint32_t current_album, gnsdk_uint32_t total_albums, gnsdk_bool_t* p_abort)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		GnResponseAlbums albumResponse(response_gdo);
		if(albumResponse.IsType(GNSDK_GDO_TYPE_RESPONSE_MATCH) )
		{
			GnResponseDataMatches dataMatchResponse(response_gdo);
			p_midf->eventHandler_->result_available(dataMatchResponse, current_album, total_albums);
		}
		else
		{
			p_midf->eventHandler_->result_available(albumResponse, current_album, total_albums);
		}

		if(p_midf->eventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _callback_result_not_found
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_result_not_found(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_musicidfile_fileinfo_handle_t fileinfo_handle, gnsdk_uint32_t current_file, gnsdk_uint32_t total_files, gnsdk_bool_t* p_abort)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		GnMusicIDFileInfo fileinfo = GnMusicIDFileInfo(query_handle, fileinfo_handle);
		p_midf->eventHandler_->result_not_found(fileinfo, current_file, total_files);
		if(p_midf->eventHandler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}

/*-----------------------------------------------------------------------------
 *  _callback_musicid_complete
 */
void GNSDK_CALLBACK_API GnMusicIDFile::_callback_musicid_complete(void* callback_data, gnsdk_musicidfile_query_handle_t query_handle, gnsdk_error_t musicidfile_complete_error)
{
	GnMusicIDFile* p_midf = (GnMusicIDFile*)callback_data;

	(void)query_handle;

	if(p_midf->eventHandler_)
	{
		p_midf->eventHandler_->complete(musicidfile_complete_error);
	}
}

/*-----------------------------------------------------------------------------
 *  WaitForComplete
 */
void GnMusicIDFile::WaitForComplete(gnsdk_uint32_t timeoutValue) throw ( GnError )
{
	gnsdk_error_t pMidFComplteError;
	gnsdk_error_t error;

	error = gnsdk_musicidfile_query_wait_for_complete(queryHandle_, timeoutValue, &pMidFComplteError);
	if(error || pMidFComplteError) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  Cancel
 */
void GnMusicIDFile::Cancel() throw ( GnError )
{
	gnsdk_error_t error;

	this->WaitForComplete(kTimeValueInfinite);
	error = gnsdk_musicidfile_query_cancel(queryHandle_);
	if(error) { throw GnError(); }
}

/**************************************************************************
** GnMusicIDFileInfo
**
**   NOtes: Currently life cycle management of a GnMusicIDFileInfo is determined by
**     :  the object . i.e. it holds onto its parent to stay relevant . the
**        bad behaviour is that a FileInfo holds onto the life cycles the Query
**        and all its remaining fileInfos.
*/
gnsdk_cstr_t GnMusicIDFileInfo::GetIdentifier() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_IDENT, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  GetFileName
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetFileName() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_FILENAME, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetFileName
 */
void GnMusicIDFileInfo::SetFileName(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_FILENAME, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetCddbID
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetCddbID() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_CDDB_IDS, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetCddbID
 */
void GnMusicIDFileInfo::SetCddbID(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_CDDB_IDS, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetAlbumArtist
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetAlbumArtist() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_ALBUMARTIST, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetAlbumArtist
 */
void GnMusicIDFileInfo::SetAlbumArtist(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_ALBUMARTIST, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetAlbumTitle
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetAlbumTitle() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_ALBUMTITLE, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetAlbumTitle
 */
void GnMusicIDFileInfo::SetAlbumTitle(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_ALBUMTITLE, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTrackArtist
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTrackArtist() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKARTIST, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTrackArtist
 */
void GnMusicIDFileInfo::SetTrackArtist(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKARTIST, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTrackTitle
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTrackTitle() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKTITLE, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTrackTitle
 */
void GnMusicIDFileInfo::SetTrackTitle(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKTITLE, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTrackNumber
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTrackNumber() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKNUMBER, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTrackNumber
 */
void GnMusicIDFileInfo::SetTrackNumber(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TRACKNUMBER, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetDiscNumber
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetDiscNumber() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_DISCNUMBER, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetDiscNumber
 */
void GnMusicIDFileInfo::SetDiscNumber(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_DISCNUMBER, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTagID
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTagID() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TAGID, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTagID
 */
void GnMusicIDFileInfo::SetTagID(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TAGID, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetFingerprint
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetFingerprint() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_FINGERPRINT, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetFingerprint
 */
void GnMusicIDFileInfo::SetFingerprint(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_FINGERPRINT, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetMediaID
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetMediaID() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_MEDIA_ID, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetMediaID
 */
void GnMusicIDFileInfo::SetMediaID(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_MEDIA_ID, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetMUI
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetMUI() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_MUI, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetMUI
 */
void GnMusicIDFileInfo::SetMUI(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_MUI, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTocOffsets
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTocOffsets() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TOC_OFFSETS, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTocOffsets
 */
void GnMusicIDFileInfo::SetTocOffsets(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TOC_OFFSETS, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTUI
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTUI() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TUI, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTUI
 */
void GnMusicIDFileInfo::SetTUI(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TUI, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  GetTuiTag
 */
gnsdk_cstr_t GnMusicIDFileInfo::GetTuiTag() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_value = GNSDK_NULL;

	error = gnsdk_musicidfile_fileinfo_metadata_get(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TUI_TAG, &sz_value, GNSDK_NULL);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return sz_value;
}

/*-----------------------------------------------------------------------------
 *  SetTuiTag
 */
void GnMusicIDFileInfo::SetTuiTag(gnsdk_cstr_t value) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_metadata_set(fileInfohandle_, GNSDK_MUSICIDFILE_FILEINFO_VALUE_TUI_TAG, value);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FingerprintBegin
 */
void GnMusicIDFileInfo::FingerprintBegin(gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels)
throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_fingerprint_begin(fileInfohandle_, audioSampleRate, audioSampleSize, audioChannels);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FingerprintWrite
 */
bool GnMusicIDFileInfo::FingerprintWrite(const gnsdk_byte_t* audioData, gnsdk_size_t audioDataSize) throw ( GnError )
{
	gnsdk_char_t  b_complete = GNSDK_FALSE;
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_fingerprint_write(fileInfohandle_, audioData, audioDataSize, &b_complete);
	if(error) { throw GnError(); }

	if(b_complete)
	{
		return true;
	}
	return false;
}

/*-----------------------------------------------------------------------------
 *  FingerprintEnd
 */
void GnMusicIDFileInfo::FingerprintEnd() throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicidfile_fileinfo_fingerprint_end(fileInfohandle_);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FingerprintFromSource
 */
void GnMusicIDFileInfo::FingerprintFromSource(GnAudioSource& audioSource)  throw ( GnError )
{
	gnsdk_byte_t  audio_buffer[1024];
	gnsdk_size_t  audio_data_size;
	gnsdk_bool_t  b_complete;
	gnsdk_error_t error;

	error = audioSource.SourceInit();
	if(!error)
	{
		error = gnsdk_musicidfile_fileinfo_fingerprint_begin(fileInfohandle_, audioSource.SamplesPerSecond(), audioSource.SampleSizeInBits(), audioSource.NumberOfChannels() );
		if(!error)
		{
			b_complete = GNSDK_FALSE;

			while(0 < ( audio_data_size = audioSource.GetData(audio_buffer, sizeof( audio_buffer ) ) ) )
			{
				error = gnsdk_musicidfile_fileinfo_fingerprint_write(fileInfohandle_, audio_buffer, audio_data_size, &b_complete);
				if(error)
				{
					break;
				}

				if(b_complete)
				{
					break;
				}
			}

			if(!b_complete)
			{
				error = gnsdk_musicidfile_fileinfo_fingerprint_end(fileInfohandle_);
			}
		}

		audioSource.SourceClose();
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  Status
 */
gnsdk_musicidfile_fileinfo_status_t GnMusicIDFileInfo::Status() throw ( GnError )
{
	gnsdk_musicidfile_fileinfo_status_t status;
	const gnsdk_error_info_t*           pErrorInfo;
	gnsdk_error_t                       error;

	error = gnsdk_musicidfile_fileinfo_status(fileInfohandle_, &status, &pErrorInfo);
	if(error) { throw GnError(); }

	return status;
}

/*-----------------------------------------------------------------------------
 *  ErrorInformation
 */
GnError GnMusicIDFileInfo::ErrorInformation() throw ( GnError )
{
	gnsdk_musicidfile_fileinfo_status_t status;
	const gnsdk_error_info_t*           pErrorInfo;
	gnsdk_error_t                       error;

	error = gnsdk_musicidfile_fileinfo_status(fileInfohandle_, &status, &pErrorInfo);
	if(error) { throw GnError(); }

	GnError errorObj(pErrorInfo);

	return errorObj;
}

/*-----------------------------------------------------------------------------
 *  AlbumResponse
 */
GnResponseAlbums GnMusicIDFileInfo::AlbumResponse() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo;
	gnsdk_error_t      error;

	error = gnsdk_musicidfile_fileinfo_get_response_gdo(fileInfohandle_, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseAlbums result(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return result;
}

/*-----------------------------------------------------------------------------
 *  DataMatchResponse
 */
GnResponseDataMatches GnMusicIDFileInfo::DataMatchResponse() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo;
	gnsdk_error_t      error;

	error = gnsdk_musicidfile_fileinfo_get_response_gdo(fileInfohandle_, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseDataMatches result(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return result;
}
#endif /* GNSDK_MUSICID_FILE */
