/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: MusicIDLookupAlbumID
 *  Description:
 *  This example looks up an Album based on its TOC.
 *
 *  Command-line Syntax:
 * clientid  , clientid_tag , license path and lib path
 */

package musicid_lookup_album_id;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class MusicIDLookupAlbumID {

	/*
	 * Set USELOCAL to false to have the sample perform online queries.
	 * Set USELOCAL to true to have the sample perform local queries.
	 * For local queries, Gracenote databases are required.
	 */
	private static final boolean USELOCAL = false;

	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}
	
	private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}

	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				localeEvents
				);

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}

	private static int doMatchSelection(GnResponseAlbums gnResponseAlbums) throws InvalidObjectException, GnException {
		/*
		This is where any matches that need resolution/disambiguation are iterated
		and a single selection of the best match is made.

		For this simplified sample, we'll just echo the matches and select the first match.
		*/		
		System.out.printf("%16s %d\n", "Match count:", gnResponseAlbums.albums().count());
		
		GnAlbumIterator gnAlbumIterator = gnResponseAlbums.albums().getIterator();
		
		while (gnAlbumIterator.hasNext()) {
			GnAlbum album = (GnAlbum) gnAlbumIterator.next();
			System.out.printf( "%16s %s\n", "Title:",  album.title().display());
		}
	
		return 0;
	}

	private static void musicIDAlbumID(GnUser user) throws IOException, GnException {
		System.out.println("\n*****MusicID ID Query*****");
		
		GnMusicID gnMusicID = new GnMusicID(user);
		
		GnDataObject gnid = new GnAlbum("5154004", "0168B3134B141081DE907A15E792D4E0");		
		GnResponseAlbums gnResponseAlbums = gnMusicID.findAlbums(gnid);
		
		if(0 == gnResponseAlbums.albums().count()) {
			System.out.println("\nNo albums found for the input.");
		}else {
			int choiceOrdinal = 0;
			
			if(gnResponseAlbums.needsDecision()){
				choiceOrdinal = doMatchSelection(gnResponseAlbums);
			}else {
				choiceOrdinal = 0;
			}
			
			GnAlbum finalAlbum = gnResponseAlbums.albums().at(choiceOrdinal).next();
			
			if(!finalAlbum.fullResult()) {
				GnResponseAlbums gnFollowupResult = gnMusicID.findAlbums(gnid);
					
				finalAlbum = gnFollowupResult.albums().at(0).next();
			}
		
			System.out.printf( "%16s\n", "Final album:");
			System.out.printf( "%16s %s\n", "Title:",  finalAlbum.title().display());
		}
	}
	
	/*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				if (USELOCAL) {
					GnStorageSqlite sqliteStorage = new GnStorageSqlite();
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				loadLocale(gnsdk, user);
			
				musicIDAlbumID(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
