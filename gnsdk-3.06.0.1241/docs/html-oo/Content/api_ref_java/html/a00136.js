var a00136 =
[
    [ "gnsdk_musicidstream_callbacks_t", "a00136.html#aa3150dd0aa10a0bdf1e69d3906d0385f", null ],
    [ "gnsdk_musicidstream_callbacks_t", "a00136.html#a0614731aff38d2e905ddc3e5c2cf9fc7", null ],
    [ "delete", "a00136.html#ac3fe5edcf3f528f00cd59deaefe461dc", null ],
    [ "finalize", "a00136.html#a1209c528bf293e91ae36f732a1ccdd37", null ],
    [ "getCallback_mids_status", "a00136.html#a47340431f4e5e0fc2875bfedf7efa00b", null ],
    [ "getCallback_result_available", "a00136.html#af0ceaceb624571c5b186bd534abb8684", null ],
    [ "getCallback_status", "a00136.html#a8a5283fba62f0f292ef4850fdf7f8f50", null ],
    [ "setCallback_mids_status", "a00136.html#a57a34775c3721926e960f11a584e8a5c", null ],
    [ "setCallback_result_available", "a00136.html#ac59751d2ebd05898eac64209124a0f99", null ],
    [ "setCallback_status", "a00136.html#ac0ec7756835615687bcafe1b270d9a3e", null ],
    [ "swigCMemOwn", "a00136.html#ad14e7bf5adef3e6d3d45ffc894a7976d", null ]
];