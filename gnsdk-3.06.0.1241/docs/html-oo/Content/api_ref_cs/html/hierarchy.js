var hierarchy =
[
    [ "ApplicationException", null, [
      [ "GracenoteSDK.GnException", "class_gracenote_s_d_k_1_1_gn_exception.html", null ]
    ] ],
    [ "GracenoteSDK.gnsdk_csharp_marshal", "class_gracenote_s_d_k_1_1gnsdk__csharp__marshal.html", null ],
    [ "GracenoteSDK.gnsdk_csharp_marshalPINVOKE", "class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e.html", null ],
    [ "IDisposable", null, [
      [ "GracenoteSDK.collection_ident_provider", "class_gracenote_s_d_k_1_1collection__ident__provider.html", null ],
      [ "GracenoteSDK.collection_join_provider", "class_gracenote_s_d_k_1_1collection__join__provider.html", null ],
      [ "GracenoteSDK.collection_storage_provider", "class_gracenote_s_d_k_1_1collection__storage__provider.html", null ],
      [ "GracenoteSDK.gn_locale_info_provider", "class_gracenote_s_d_k_1_1gn__locale__info__provider.html", null ],
      [ "GracenoteSDK.gn_musicid_file_info_provider", "class_gracenote_s_d_k_1_1gn__musicid__file__info__provider.html", null ],
      [ "GracenoteSDK.GnAlbumEnumerable", "class_gracenote_s_d_k_1_1_gn_album_enumerable.html", null ],
      [ "GracenoteSDK.GnAlbumEnumerator", "class_gracenote_s_d_k_1_1_gn_album_enumerator.html", null ],
      [ "GracenoteSDK.GnAlbumProvider", "class_gracenote_s_d_k_1_1_gn_album_provider.html", null ],
      [ "GracenoteSDK.GnAudioSource", "class_gracenote_s_d_k_1_1_gn_audio_source.html", null ],
      [ "GracenoteSDK.GnContributorEnumerable", "class_gracenote_s_d_k_1_1_gn_contributor_enumerable.html", null ],
      [ "GracenoteSDK.GnContributorEnumerator", "class_gracenote_s_d_k_1_1_gn_contributor_enumerator.html", null ],
      [ "GracenoteSDK.GnCreditEnumerable", "class_gracenote_s_d_k_1_1_gn_credit_enumerable.html", null ],
      [ "GracenoteSDK.GnCreditEnumerator", "class_gracenote_s_d_k_1_1_gn_credit_enumerator.html", null ],
      [ "GracenoteSDK.GnCreditProvider", "class_gracenote_s_d_k_1_1_gn_credit_provider.html", null ],
      [ "GracenoteSDK.GnDataMatchEnumerable", "class_gracenote_s_d_k_1_1_gn_data_match_enumerable.html", null ],
      [ "GracenoteSDK.GnDataMatchEnumerator", "class_gracenote_s_d_k_1_1_gn_data_match_enumerator.html", null ],
      [ "GracenoteSDK.GnDataMatchProvider", "class_gracenote_s_d_k_1_1_gn_data_match_provider.html", null ],
      [ "GracenoteSDK.GnError", "class_gracenote_s_d_k_1_1_gn_error.html", null ],
      [ "GracenoteSDK.GnExternalIDEnumerable", "class_gracenote_s_d_k_1_1_gn_external_i_d_enumerable.html", null ],
      [ "GracenoteSDK.GnExternalIDEnumerator", "class_gracenote_s_d_k_1_1_gn_external_i_d_enumerator.html", null ],
      [ "GracenoteSDK.GnExternalIDProvider", "class_gracenote_s_d_k_1_1_gn_external_i_d_provider.html", null ],
      [ "GracenoteSDK.GnIFingerprinterMusicIDFile", "class_gracenote_s_d_k_1_1_gn_i_fingerprinter_music_i_d_file.html", null ],
      [ "GracenoteSDK.GnListElementChildEnumerable", "class_gracenote_s_d_k_1_1_gn_list_element_child_enumerable.html", null ],
      [ "GracenoteSDK.GnListElementChildEnumerator", "class_gracenote_s_d_k_1_1_gn_list_element_child_enumerator.html", null ],
      [ "GracenoteSDK.GnListLevelEnumerable", "class_gracenote_s_d_k_1_1_gn_list_level_enumerable.html", null ],
      [ "GracenoteSDK.GnListLevelEnumerator", "class_gracenote_s_d_k_1_1_gn_list_level_enumerator.html", null ],
      [ "GracenoteSDK.GnLocaleInfoEnumerable", "class_gracenote_s_d_k_1_1_gn_locale_info_enumerable.html", null ],
      [ "GracenoteSDK.GnLocaleInfoEnumerator", "class_gracenote_s_d_k_1_1_gn_locale_info_enumerator.html", null ],
      [ "GracenoteSDK.GnMarshalUTF8", "class_gracenote_s_d_k_1_1_gn_marshal_u_t_f8.html", null ],
      [ "GracenoteSDK.GnMatchEnumerable", "class_gracenote_s_d_k_1_1_gn_match_enumerable.html", null ],
      [ "GracenoteSDK.GnMatchEnumerator", "class_gracenote_s_d_k_1_1_gn_match_enumerator.html", null ],
      [ "GracenoteSDK.GnMatchProvider", "class_gracenote_s_d_k_1_1_gn_match_provider.html", null ],
      [ "GracenoteSDK.GnMoodgridDataPoint", "class_gracenote_s_d_k_1_1_gn_moodgrid_data_point.html", null ],
      [ "GracenoteSDK.GnMoodgridIdentifier", "class_gracenote_s_d_k_1_1_gn_moodgrid_identifier.html", null ],
      [ "GracenoteSDK.GnMoodgridProviderEnumerable", "class_gracenote_s_d_k_1_1_gn_moodgrid_provider_enumerable.html", null ],
      [ "GracenoteSDK.GnMoodgridProviderEnumerator", "class_gracenote_s_d_k_1_1_gn_moodgrid_provider_enumerator.html", null ],
      [ "GracenoteSDK.GnMusicIDFileEventsDelegate", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_events_delegate.html", null ],
      [ "GracenoteSDK.GnMusicIDFileInfoEnumerable", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_info_enumerable.html", null ],
      [ "GracenoteSDK.GnMusicIDFileInfoEnumerator", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_info_enumerator.html", null ],
      [ "GracenoteSDK.GnMusicIDStreamEventsDelegate", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html", null ],
      [ "GracenoteSDK.GnNameEnumerable", "class_gracenote_s_d_k_1_1_gn_name_enumerable.html", null ],
      [ "GracenoteSDK.GnNameEnumerator", "class_gracenote_s_d_k_1_1_gn_name_enumerator.html", null ],
      [ "GracenoteSDK.GnNameProvider", "class_gracenote_s_d_k_1_1_gn_name_provider.html", null ],
      [ "GracenoteSDK.GnObject", "class_gracenote_s_d_k_1_1_gn_object.html", [
        [ "GracenoteSDK.GnDataObject", "class_gracenote_s_d_k_1_1_gn_data_object.html", [
          [ "GracenoteSDK.GnAlbum", "class_gracenote_s_d_k_1_1_gn_album.html", null ],
          [ "GracenoteSDK.GnArtist", "class_gracenote_s_d_k_1_1_gn_artist.html", null ],
          [ "GracenoteSDK.GnArtistType", "class_gracenote_s_d_k_1_1_gn_artist_type.html", null ],
          [ "GracenoteSDK.GnAudioWork", "class_gracenote_s_d_k_1_1_gn_audio_work.html", null ],
          [ "GracenoteSDK.GnContents", "class_gracenote_s_d_k_1_1_gn_contents.html", null ],
          [ "GracenoteSDK.GnContributor", "class_gracenote_s_d_k_1_1_gn_contributor.html", null ],
          [ "GracenoteSDK.GnCredit", "class_gracenote_s_d_k_1_1_gn_credit.html", null ],
          [ "GracenoteSDK.GnDataMatch", "class_gracenote_s_d_k_1_1_gn_data_match.html", null ],
          [ "GracenoteSDK.GnEra", "class_gracenote_s_d_k_1_1_gn_era.html", null ],
          [ "GracenoteSDK.GnExternalID", "class_gracenote_s_d_k_1_1_gn_external_i_d.html", null ],
          [ "GracenoteSDK.GnGenre", "class_gracenote_s_d_k_1_1_gn_genre.html", null ],
          [ "GracenoteSDK.GnLanguage", "class_gracenote_s_d_k_1_1_gn_language.html", null ],
          [ "GracenoteSDK.GnMatch", "class_gracenote_s_d_k_1_1_gn_match.html", null ],
          [ "GracenoteSDK.GnMood", "class_gracenote_s_d_k_1_1_gn_mood.html", null ],
          [ "GracenoteSDK.GnName", "class_gracenote_s_d_k_1_1_gn_name.html", null ],
          [ "GracenoteSDK.GnOrigin", "class_gracenote_s_d_k_1_1_gn_origin.html", null ],
          [ "GracenoteSDK.GnPlaylistMetadata", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html", null ],
          [ "GracenoteSDK.GnRating", "class_gracenote_s_d_k_1_1_gn_rating.html", null ],
          [ "GracenoteSDK.GnResponseAlbums", "class_gracenote_s_d_k_1_1_gn_response_albums.html", null ],
          [ "GracenoteSDK.GnResponseContributor", "class_gracenote_s_d_k_1_1_gn_response_contributor.html", null ],
          [ "GracenoteSDK.GnResponseDataMatches", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html", null ],
          [ "GracenoteSDK.GnResponseMatches", "class_gracenote_s_d_k_1_1_gn_response_matches.html", null ],
          [ "GracenoteSDK.GnResponseTracks", "class_gracenote_s_d_k_1_1_gn_response_tracks.html", null ],
          [ "GracenoteSDK.GnResponseVideoObjects", "class_gracenote_s_d_k_1_1_gn_response_video_objects.html", null ],
          [ "GracenoteSDK.GnResponseVideoProduct", "class_gracenote_s_d_k_1_1_gn_response_video_product.html", null ],
          [ "GracenoteSDK.GnResponseVideoProgram", "class_gracenote_s_d_k_1_1_gn_response_video_program.html", null ],
          [ "GracenoteSDK.GnResponseVideoSeasons", "class_gracenote_s_d_k_1_1_gn_response_video_seasons.html", null ],
          [ "GracenoteSDK.GnResponseVideoSeries", "class_gracenote_s_d_k_1_1_gn_response_video_series.html", null ],
          [ "GracenoteSDK.GnResponseVideoSuggestions", "class_gracenote_s_d_k_1_1_gn_response_video_suggestions.html", null ],
          [ "GracenoteSDK.GnResponseVideoWork", "class_gracenote_s_d_k_1_1_gn_response_video_work.html", null ],
          [ "GracenoteSDK.GnRole", "class_gracenote_s_d_k_1_1_gn_role.html", null ],
          [ "GracenoteSDK.GnSortable", "class_gracenote_s_d_k_1_1_gn_sortable.html", null ],
          [ "GracenoteSDK.GnTempo", "class_gracenote_s_d_k_1_1_gn_tempo.html", null ],
          [ "GracenoteSDK.GnTitle", "class_gracenote_s_d_k_1_1_gn_title.html", null ],
          [ "GracenoteSDK.GnTrack", "class_gracenote_s_d_k_1_1_gn_track.html", null ],
          [ "GracenoteSDK.GnTVProgram", "class_gracenote_s_d_k_1_1_gn_t_v_program.html", null ],
          [ "GracenoteSDK.GnVideoChapter", "class_gracenote_s_d_k_1_1_gn_video_chapter.html", null ],
          [ "GracenoteSDK.GnVideoCredit", "class_gracenote_s_d_k_1_1_gn_video_credit.html", null ],
          [ "GracenoteSDK.GnVideoDisc", "class_gracenote_s_d_k_1_1_gn_video_disc.html", null ],
          [ "GracenoteSDK.GnVideoFeature", "class_gracenote_s_d_k_1_1_gn_video_feature.html", null ],
          [ "GracenoteSDK.GnVideoLayer", "class_gracenote_s_d_k_1_1_gn_video_layer.html", null ],
          [ "GracenoteSDK.GnVideoProduct", "class_gracenote_s_d_k_1_1_gn_video_product.html", null ],
          [ "GracenoteSDK.GnVideoSeason", "class_gracenote_s_d_k_1_1_gn_video_season.html", null ],
          [ "GracenoteSDK.GnVideoSeries", "class_gracenote_s_d_k_1_1_gn_video_series.html", null ],
          [ "GracenoteSDK.GnVideoSide", "class_gracenote_s_d_k_1_1_gn_video_side.html", null ],
          [ "GracenoteSDK.GnVideoWork", "class_gracenote_s_d_k_1_1_gn_video_work.html", null ]
        ] ],
        [ "GracenoteSDK.GnDsp", "class_gracenote_s_d_k_1_1_gn_dsp.html", null ],
        [ "GracenoteSDK.GnDspFeature", "class_gracenote_s_d_k_1_1_gn_dsp_feature.html", null ],
        [ "GracenoteSDK.GnLink", "class_gracenote_s_d_k_1_1_gn_link.html", null ],
        [ "GracenoteSDK.GnLinkContent", "class_gracenote_s_d_k_1_1_gn_link_content.html", null ],
        [ "GracenoteSDK.GnList", "class_gracenote_s_d_k_1_1_gn_list.html", null ],
        [ "GracenoteSDK.GnListElement", "class_gracenote_s_d_k_1_1_gn_list_element.html", null ],
        [ "GracenoteSDK.GnLocale", "class_gracenote_s_d_k_1_1_gn_locale.html", null ],
        [ "GracenoteSDK.GnLocaleInfo", "class_gracenote_s_d_k_1_1_gn_locale_info.html", null ],
        [ "GracenoteSDK.GnLookupLocal", "class_gracenote_s_d_k_1_1_gn_lookup_local.html", null ],
        [ "GracenoteSDK.GnMoodgrid", "class_gracenote_s_d_k_1_1_gn_moodgrid.html", null ],
        [ "GracenoteSDK.GnMoodgridPresentation", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html", null ],
        [ "GracenoteSDK.GnMoodgridProvider", "class_gracenote_s_d_k_1_1_gn_moodgrid_provider.html", null ],
        [ "GracenoteSDK.GnMoodgridResult", "class_gracenote_s_d_k_1_1_gn_moodgrid_result.html", null ],
        [ "GracenoteSDK.GnMusicID", "class_gracenote_s_d_k_1_1_gn_music_i_d.html", null ],
        [ "GracenoteSDK.GnMusicIDFile", "class_gracenote_s_d_k_1_1_gn_music_i_d_file.html", null ],
        [ "GracenoteSDK.GnMusicIDFileInfo", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_info.html", null ],
        [ "GracenoteSDK.GnMusicIDStream", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream.html", null ],
        [ "GracenoteSDK.GnPlaylist", "class_gracenote_s_d_k_1_1_gn_playlist.html", null ],
        [ "GracenoteSDK.GnPlaylistCollection", "class_gracenote_s_d_k_1_1_gn_playlist_collection.html", null ],
        [ "GracenoteSDK.GnPlaylistResult", "class_gracenote_s_d_k_1_1_gn_playlist_result.html", null ],
        [ "GracenoteSDK.GnStorageSqlite", "class_gracenote_s_d_k_1_1_gn_storage_sqlite.html", null ],
        [ "GracenoteSDK.GnString", "class_gracenote_s_d_k_1_1_gn_string.html", null ],
        [ "GracenoteSDK.GnUser", "class_gracenote_s_d_k_1_1_gn_user.html", null ],
        [ "GracenoteSDK.GnVideo", "class_gracenote_s_d_k_1_1_gn_video.html", null ]
      ] ],
      [ "GracenoteSDK.GnPlaylistCollectionIdentEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_collection_ident_enumerable.html", null ],
      [ "GracenoteSDK.GnPlaylistCollectionIdentEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_collection_ident_enumerator.html", null ],
      [ "GracenoteSDK.GnPlaylistCollectionSyncEventsDelegate", "class_gracenote_s_d_k_1_1_gn_playlist_collection_sync_events_delegate.html", null ],
      [ "GracenoteSDK.GnPlaylistIdentifier", "class_gracenote_s_d_k_1_1_gn_playlist_identifier.html", null ],
      [ "GracenoteSDK.GnPlaylistJoinEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_join_enumerable.html", null ],
      [ "GracenoteSDK.GnPlaylistJoinEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_join_enumerator.html", null ],
      [ "GracenoteSDK.GnPlaylistResultIdentEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_result_ident_enumerable.html", null ],
      [ "GracenoteSDK.GnPlaylistResultIdentEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_result_ident_enumerator.html", null ],
      [ "GracenoteSDK.GnSDK", "class_gracenote_s_d_k_1_1_gn_s_d_k.html", null ],
      [ "GracenoteSDK.gnsdk_error_info_t", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html", null ],
      [ "GracenoteSDK.gnsdk_musicidstream_callbacks_t", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html", null ],
      [ "GracenoteSDK.GnStatusEventsDelegate", "class_gracenote_s_d_k_1_1_gn_status_events_delegate.html", null ],
      [ "GracenoteSDK.GnTrackEnumerable", "class_gracenote_s_d_k_1_1_gn_track_enumerable.html", null ],
      [ "GracenoteSDK.GnTrackEnumerator", "class_gracenote_s_d_k_1_1_gn_track_enumerator.html", null ],
      [ "GracenoteSDK.GnTrackProvider", "class_gracenote_s_d_k_1_1_gn_track_provider.html", null ],
      [ "GracenoteSDK.GnTVProgramEnumerable", "class_gracenote_s_d_k_1_1_gn_t_v_program_enumerable.html", null ],
      [ "GracenoteSDK.GnTVProgramEnumerator", "class_gracenote_s_d_k_1_1_gn_t_v_program_enumerator.html", null ],
      [ "GracenoteSDK.GnTVProgramProvider", "class_gracenote_s_d_k_1_1_gn_t_v_program_provider.html", null ],
      [ "GracenoteSDK.GnVideoChapterEnumerable", "class_gracenote_s_d_k_1_1_gn_video_chapter_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoChapterEnumerator", "class_gracenote_s_d_k_1_1_gn_video_chapter_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoChapterProvider", "class_gracenote_s_d_k_1_1_gn_video_chapter_provider.html", null ],
      [ "GracenoteSDK.GnVideoCreditEnumerable", "class_gracenote_s_d_k_1_1_gn_video_credit_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoCreditEnumerator", "class_gracenote_s_d_k_1_1_gn_video_credit_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoCreditProvider", "class_gracenote_s_d_k_1_1_gn_video_credit_provider.html", null ],
      [ "GracenoteSDK.GnVideoDiscEnumerable", "class_gracenote_s_d_k_1_1_gn_video_disc_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoDiscEnumerator", "class_gracenote_s_d_k_1_1_gn_video_disc_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoDiscProvider", "class_gracenote_s_d_k_1_1_gn_video_disc_provider.html", null ],
      [ "GracenoteSDK.GnVideoFeatureEnumerable", "class_gracenote_s_d_k_1_1_gn_video_feature_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoFeatureEnumerator", "class_gracenote_s_d_k_1_1_gn_video_feature_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoFeatureProvider", "class_gracenote_s_d_k_1_1_gn_video_feature_provider.html", null ],
      [ "GracenoteSDK.GnVideoLayerEnumerable", "class_gracenote_s_d_k_1_1_gn_video_layer_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoLayerEnumerator", "class_gracenote_s_d_k_1_1_gn_video_layer_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoLayerProvider", "class_gracenote_s_d_k_1_1_gn_video_layer_provider.html", null ],
      [ "GracenoteSDK.GnVideoProductEnumerable", "class_gracenote_s_d_k_1_1_gn_video_product_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoProductEnumerator", "class_gracenote_s_d_k_1_1_gn_video_product_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoProductProvider", "class_gracenote_s_d_k_1_1_gn_video_product_provider.html", null ],
      [ "GracenoteSDK.GnVideoSeasonEnumerable", "class_gracenote_s_d_k_1_1_gn_video_season_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoSeasonEnumerator", "class_gracenote_s_d_k_1_1_gn_video_season_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoSeasonProvider", "class_gracenote_s_d_k_1_1_gn_video_season_provider.html", null ],
      [ "GracenoteSDK.GnVideoSeriesEnumerable", "class_gracenote_s_d_k_1_1_gn_video_series_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoSeriesEnumerator", "class_gracenote_s_d_k_1_1_gn_video_series_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoSeriesProvider", "class_gracenote_s_d_k_1_1_gn_video_series_provider.html", null ],
      [ "GracenoteSDK.GnVideoSideEnumerable", "class_gracenote_s_d_k_1_1_gn_video_side_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoSideEnumerator", "class_gracenote_s_d_k_1_1_gn_video_side_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoSideProvider", "class_gracenote_s_d_k_1_1_gn_video_side_provider.html", null ],
      [ "GracenoteSDK.GnVideoWorkEnumerable", "class_gracenote_s_d_k_1_1_gn_video_work_enumerable.html", null ],
      [ "GracenoteSDK.GnVideoWorkEnumerator", "class_gracenote_s_d_k_1_1_gn_video_work_enumerator.html", null ],
      [ "GracenoteSDK.GnVideoWorkProvider", "class_gracenote_s_d_k_1_1_gn_video_work_provider.html", null ],
      [ "GracenoteSDK.list_element_child_provider", "class_gracenote_s_d_k_1_1list__element__child__provider.html", null ],
      [ "GracenoteSDK.list_element_provider", "class_gracenote_s_d_k_1_1list__element__provider.html", null ],
      [ "GracenoteSDK.moodgrid_provider", "class_gracenote_s_d_k_1_1moodgrid__provider.html", null ],
      [ "GracenoteSDK.moodgrid_result_provider", "class_gracenote_s_d_k_1_1moodgrid__result__provider.html", null ],
      [ "GracenoteSDK.presentation_data_provider", "class_gracenote_s_d_k_1_1presentation__data__provider.html", null ],
      [ "GracenoteSDK.result_provider", "class_gracenote_s_d_k_1_1result__provider.html", null ]
    ] ],
    [ "IEnumerable< GnAlbum >", null, [
      [ "GracenoteSDK.GnAlbumEnumerable", "class_gracenote_s_d_k_1_1_gn_album_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnContributor >", null, [
      [ "GracenoteSDK.GnContributorEnumerable", "class_gracenote_s_d_k_1_1_gn_contributor_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnCredit >", null, [
      [ "GracenoteSDK.GnCreditEnumerable", "class_gracenote_s_d_k_1_1_gn_credit_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnExternalID >", null, [
      [ "GracenoteSDK.GnExternalIDEnumerable", "class_gracenote_s_d_k_1_1_gn_external_i_d_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnListElement >", null, [
      [ "GracenoteSDK.GnListElementChildEnumerable", "class_gracenote_s_d_k_1_1_gn_list_element_child_enumerable.html", null ],
      [ "GracenoteSDK.GnListLevelEnumerable", "class_gracenote_s_d_k_1_1_gn_list_level_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnLocaleInfo >", null, [
      [ "GracenoteSDK.GnLocaleInfoEnumerable", "class_gracenote_s_d_k_1_1_gn_locale_info_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnMatch >", null, [
      [ "GracenoteSDK.GnMatchEnumerable", "class_gracenote_s_d_k_1_1_gn_match_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnMoodgridDataPoint >", null, [
      [ "GracenoteSDK.GnMoodgridPresentationDataEnumerable", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation_data_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnMoodgridIdentifier >", null, [
      [ "GracenoteSDK.GnMoodgridResultEnumerable", "class_gracenote_s_d_k_1_1_gn_moodgrid_result_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnMoodgridProvider >", null, [
      [ "GracenoteSDK.GnMoodgridProviderEnumerable", "class_gracenote_s_d_k_1_1_gn_moodgrid_provider_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnMusicIDFileInfo >", null, [
      [ "GracenoteSDK.GnMusicIDFileInfoEnumerable", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_info_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnName >", null, [
      [ "GracenoteSDK.GnNameEnumerable", "class_gracenote_s_d_k_1_1_gn_name_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnPlaylistCollection >", null, [
      [ "GracenoteSDK.GnPlaylistJoinEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_join_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnPlaylistIdentifier >", null, [
      [ "GracenoteSDK.GnPlaylistResultIdentEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_result_ident_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnPlaylistIdentifier >", null, [
      [ "GracenoteSDK.GnPlaylistCollectionIdentEnumerable", "class_gracenote_s_d_k_1_1_gn_playlist_collection_ident_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnTrack >", null, [
      [ "GracenoteSDK.GnTrackEnumerable", "class_gracenote_s_d_k_1_1_gn_track_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnTVProgram >", null, [
      [ "GracenoteSDK.GnTVProgramEnumerable", "class_gracenote_s_d_k_1_1_gn_t_v_program_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoChapter >", null, [
      [ "GracenoteSDK.GnVideoChapterEnumerable", "class_gracenote_s_d_k_1_1_gn_video_chapter_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoCredit >", null, [
      [ "GracenoteSDK.GnVideoCreditEnumerable", "class_gracenote_s_d_k_1_1_gn_video_credit_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoDisc >", null, [
      [ "GracenoteSDK.GnVideoDiscEnumerable", "class_gracenote_s_d_k_1_1_gn_video_disc_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoFeature >", null, [
      [ "GracenoteSDK.GnVideoFeatureEnumerable", "class_gracenote_s_d_k_1_1_gn_video_feature_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoLayer >", null, [
      [ "GracenoteSDK.GnVideoLayerEnumerable", "class_gracenote_s_d_k_1_1_gn_video_layer_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoProduct >", null, [
      [ "GracenoteSDK.GnVideoProductEnumerable", "class_gracenote_s_d_k_1_1_gn_video_product_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoSeason >", null, [
      [ "GracenoteSDK.GnVideoSeasonEnumerable", "class_gracenote_s_d_k_1_1_gn_video_season_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoSeries >", null, [
      [ "GracenoteSDK.GnVideoSeriesEnumerable", "class_gracenote_s_d_k_1_1_gn_video_series_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoSide >", null, [
      [ "GracenoteSDK.GnVideoSideEnumerable", "class_gracenote_s_d_k_1_1_gn_video_side_enumerable.html", null ]
    ] ],
    [ "IEnumerable< GnVideoWork >", null, [
      [ "GracenoteSDK.GnVideoWorkEnumerable", "class_gracenote_s_d_k_1_1_gn_video_work_enumerable.html", null ]
    ] ],
    [ "IEnumerator< GnAlbum >", null, [
      [ "GracenoteSDK.GnAlbumEnumerator", "class_gracenote_s_d_k_1_1_gn_album_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnContributor >", null, [
      [ "GracenoteSDK.GnContributorEnumerator", "class_gracenote_s_d_k_1_1_gn_contributor_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnCredit >", null, [
      [ "GracenoteSDK.GnCreditEnumerator", "class_gracenote_s_d_k_1_1_gn_credit_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnExternalID >", null, [
      [ "GracenoteSDK.GnExternalIDEnumerator", "class_gracenote_s_d_k_1_1_gn_external_i_d_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnListElement >", null, [
      [ "GracenoteSDK.GnListElementChildEnumerator", "class_gracenote_s_d_k_1_1_gn_list_element_child_enumerator.html", null ],
      [ "GracenoteSDK.GnListLevelEnumerator", "class_gracenote_s_d_k_1_1_gn_list_level_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnLocaleInfo >", null, [
      [ "GracenoteSDK.GnLocaleInfoEnumerator", "class_gracenote_s_d_k_1_1_gn_locale_info_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnMatch >", null, [
      [ "GracenoteSDK.GnMatchEnumerator", "class_gracenote_s_d_k_1_1_gn_match_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnMoodgridDataPoint >", null, [
      [ "GracenoteSDK.GnMoodgridPresentationDataEnumerator", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation_data_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnMoodgridIdentifier >", null, [
      [ "GracenoteSDK.GnMoodgridResultEnumerator", "class_gracenote_s_d_k_1_1_gn_moodgrid_result_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnMoodgridProvider >", null, [
      [ "GracenoteSDK.GnMoodgridProviderEnumerator", "class_gracenote_s_d_k_1_1_gn_moodgrid_provider_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnMusicIDFileInfo >", null, [
      [ "GracenoteSDK.GnMusicIDFileInfoEnumerator", "class_gracenote_s_d_k_1_1_gn_music_i_d_file_info_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnName >", null, [
      [ "GracenoteSDK.GnNameEnumerator", "class_gracenote_s_d_k_1_1_gn_name_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnPlaylistCollection >", null, [
      [ "GracenoteSDK.GnPlaylistJoinEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_join_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnPlaylistIdentifier >", null, [
      [ "GracenoteSDK.GnPlaylistCollectionIdentEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_collection_ident_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnPlaylistIdentifier >", null, [
      [ "GracenoteSDK.GnPlaylistResultIdentEnumerator", "class_gracenote_s_d_k_1_1_gn_playlist_result_ident_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnTrack >", null, [
      [ "GracenoteSDK.GnTrackEnumerator", "class_gracenote_s_d_k_1_1_gn_track_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnTVProgram >", null, [
      [ "GracenoteSDK.GnTVProgramEnumerator", "class_gracenote_s_d_k_1_1_gn_t_v_program_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoChapter >", null, [
      [ "GracenoteSDK.GnVideoChapterEnumerator", "class_gracenote_s_d_k_1_1_gn_video_chapter_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoCredit >", null, [
      [ "GracenoteSDK.GnVideoCreditEnumerator", "class_gracenote_s_d_k_1_1_gn_video_credit_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoDisc >", null, [
      [ "GracenoteSDK.GnVideoDiscEnumerator", "class_gracenote_s_d_k_1_1_gn_video_disc_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoFeature >", null, [
      [ "GracenoteSDK.GnVideoFeatureEnumerator", "class_gracenote_s_d_k_1_1_gn_video_feature_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoLayer >", null, [
      [ "GracenoteSDK.GnVideoLayerEnumerator", "class_gracenote_s_d_k_1_1_gn_video_layer_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoProduct >", null, [
      [ "GracenoteSDK.GnVideoProductEnumerator", "class_gracenote_s_d_k_1_1_gn_video_product_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoSeason >", null, [
      [ "GracenoteSDK.GnVideoSeasonEnumerator", "class_gracenote_s_d_k_1_1_gn_video_season_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoSeries >", null, [
      [ "GracenoteSDK.GnVideoSeriesEnumerator", "class_gracenote_s_d_k_1_1_gn_video_series_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoSide >", null, [
      [ "GracenoteSDK.GnVideoSideEnumerator", "class_gracenote_s_d_k_1_1_gn_video_side_enumerator.html", null ]
    ] ],
    [ "IEnumerator< GnVideoWork >", null, [
      [ "GracenoteSDK.GnVideoWorkEnumerator", "class_gracenote_s_d_k_1_1_gn_video_work_enumerator.html", null ]
    ] ],
    [ "GracenoteSDK.gnsdk_csharp_marshalPINVOKE.SWIGExceptionHelper", "class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e_1_1_s_w_i_g_exception_helper.html", null ],
    [ "GracenoteSDK.gnsdk_csharp_marshalPINVOKE.SWIGPendingException", "class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e_1_1_s_w_i_g_pending_exception.html", null ],
    [ "GracenoteSDK.gnsdk_csharp_marshalPINVOKE.SWIGStringHelper", "class_gracenote_s_d_k_1_1gnsdk__csharp__marshal_p_i_n_v_o_k_e_1_1_s_w_i_g_string_helper.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_f_p_void_enum_gnsdk_musicidstream_status_t__void", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__f__p__void__enum__gnsdk__musicidstream__status__t____void.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_f_p_void_enum_gnsdk_status_t_unsigned_int_size_t_size_t_p_char__void", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__f__p__void__enum__gnsdk__status__t__unsigned__int_9e51dfb51264045a886e75235188af89.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_f_p_void_p_void_gnsdk_gdo_handle_t__void", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__f__p__void__p__void__gnsdk__gdo__handle__t____void.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_gdo_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__gdo__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_list_element_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__list__element__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_list_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__list__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_manager_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__manager__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_playlist_results_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__playlist__results__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gnsdk_user_handle_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gnsdk__user__handle__t.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gracenote__gn_facade_range_iteratorT_char_const_p_gracenote__playlist__collection_storage_provider_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gracenote____gn__facade__range__iterator_t__char__0409dc2a295cffc928a0cf704b583741.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__gracenote____metadata____gn__gdo__provider_t__gracc70db5c71fd13c6b61d57793602807a1.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_p_char", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__p__char.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_p_void", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__p__void.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_unsigned_char", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__unsigned__char.html", null ],
    [ "GracenoteSDK.SWIGTYPE_p_void", "class_gracenote_s_d_k_1_1_s_w_i_g_t_y_p_e__p__void.html", null ]
];