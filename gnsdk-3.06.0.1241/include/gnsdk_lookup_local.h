/*
  *
  *  GRACENOTE, INC. PROPRIETARY INFORMATION
  *  This software is supplied under the terms of a license agreement or
  *  nondisclosure agreement with Gracenote, Inc. and may not be copied
  *  or disclosed except in accordance with the terms of that agreement.
  *  Copyright(c) 2000-2013. Gracenote, Inc. All Rights Reserved.
  *
 */

#ifndef _GNSDK_LOOKUP_LOCAL_H_
/* gnsdk_lookup_local.h: Primary interface for the Lookup Local module.
 */
#define _GNSDK_LOOKUP_LOCAL_H_

#include "gnsdk_manager.h"

#ifdef __cplusplus
extern "C"{
#endif


/******************************************************************************
 * Initialization APIs
 ******************************************************************************/

/** @internal gnsdk_lookup_local_initialize @endinternal
  * Initializes the Lookup Local module.
  * This function must be successfully called before any calls to other Lookup Local APIs will succeed.
  * @param sdkmgr_handle [in] Handle from a successful gnsdk_manager_initialize() call.
  *
  * @return GNSDK_SUCCESS Initialization succeeded
  * @return error Initialization failed
  *
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_initialize(
	gnsdk_manager_handle_t sdkmgr_handle
	);

/** @internal gnsdk_lookup_local_shutdown @endinternal
  * Shuts down and release resources for the Lookup Local module.
  * gnsdk_lookup_local_shutdown must be called to shutdown this module;
  * all Lookup Local APIs cease to function until the module is again initialized.
  * @return GNSDK_SUCCESS Shutdown succeeded
  * @return error Shutdown failed
  *
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_shutdown(void);

/** @internal gnsdk_lookup_local_get_version @endinternal
  * Retrieves the Lookup Local module version string.
  * This API can be called at any time, even before initialization and after shutdown.
  * @return  Returns the version string for this module
  *
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_cstr_t GNSDK_API
gnsdk_lookup_local_get_version(void);

/** @internal gnsdk_lookup_local_get_build_date @endinternal
  * Retrieves the Lookup Local module's build date string.
  * This API can be called at any time, even before initialization and after shutdown.
  * @return Returns the build date string for this module
  *
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_cstr_t GNSDK_API
gnsdk_lookup_local_get_build_date(void);

/******************************************************************************
 * Lookup local storage management APIs
 ******************************************************************************/

/** @internal gnsdk_lookup_local_storage_compact @endinternal
  * Performs compaction on named local storage.
  * See @link StorageandCaching_General_StorageIDs Storage IDs @endlink for possible storage names.
  * @param storage_name [in] Local storage name
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
  * Long Running Potential: File system I/O, database size affects running time
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_compact(
	gnsdk_cstr_t	storage_name
	);

/** @internal gnsdk_lookup_local_storage_location_set @endinternal
  * Sets the location for named local storage.
  * See @link Setup_StorageIDs Storage IDs @endlink for possible storage names.
  * <p><b>Remarks:</b></p>
  *  The storage location path can be absolute or relative.
  *  However, Windows CE developers will need to set the full path, since relative path is not supported in Windows CE.
  * 
  * You can use gnsdk_storage_sqlite_option_set() to set all the storage locations at once.
  * @param storage_name [in] Local storage name
  * @param storage_location [in] String value of path to indicate location of storage
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_location_set(
	gnsdk_cstr_t	storage_name,
	gnsdk_cstr_t	storage_location
	);

/** @internal gnsdk_lookup_local_storage_validate @endinternal
  * Performs validation on named local storage.
  * @param storage_name [in] Local storage name
  * @param p_valid [out] Pointer to error info struct to hold validation result.
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
  * Long Running Potential: File system I/O, database size affects running time
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_validate(
	gnsdk_cstr_t			storage_name,
	gnsdk_error_info_t*		p_valid
	);

/** @internal gnsdk_lookup_local_storage_version_get @endinternal
  * <b>Deprecated.</b> Use gnsdk_lookup_local_storage_info_get() instead. 
  * @param storage_name [in] Local storage name
  * @param p_gdb_version [out] String value for format version.
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_version_get(
	gnsdk_cstr_t	storage_name,
	gnsdk_cstr_t*	p_gdb_version
	);
	
/** @internal gnsdk_lookup_local_storage_info_count @endinternal
  * Get count of information available for named local storage and local storage key.
  * @param storage_name [in] Local storage name
  * @param storage_info_key [in] Local storage information key
  * @param p_info_count [out] Pointer to integer value for information count.
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_info_count(
	gnsdk_cstr_t	storage_name,
	gnsdk_cstr_t	storage_info_key,
	gnsdk_uint32_t*	p_info_count
	);

/** @internal gnsdk_lookup_local_storage_info_get @endinternal
  * Get ordinal based information for named local storage and local storage key.
  * @param storage_name [in] Local storage name
  * @param storage_info_key [in] Local storage information key
  * @param ordinal [in] Integer ordinal input
  * @param p_storage_info [out] String value for information.
  * @return GNSDK_SUCCESS
  * @return error
  * @ingroup Setup_LookupLocal_Functions
*/
gnsdk_error_t GNSDK_API
gnsdk_lookup_local_storage_info_get(
	gnsdk_cstr_t	storage_name,
	gnsdk_cstr_t	storage_info_key,
	gnsdk_uint32_t	ordinal,
	gnsdk_cstr_t*	p_storage_info
	);

/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT @endinternal
  * Name of the local storage the SDK uses to query Gracenote Content (gn_cds.gdb).
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT			"gnsdk_lookup_local_gdbcds"

/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_METADATA @endinternal
  * Name of the local storage the SDK uses to query Gracenote Metadata (gn_mdata.gdb).
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_METADATA			"gnsdk_lookup_local_gdbmdata"

/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX @endinternal
  * Name of the local storage the SDK uses for CD TOC searching (gn_itoc.gdb).
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX			"gnsdk_lookup_local_gdbitoc"

/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX @endinternal
  * Name of the local storage the SDK uses for text searching (gn_itxt.gdb).
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX		"gnsdk_lookup_local_gdbitxt"
	
/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_GDB_VERSION @endinternal
  * Key the local storage the SDK uses for version searching.
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_GDB_VERSION		"gnsdk_lookup_local_gdb_version"

/** @internal GNSDK_LOOKUP_LOCAL_STORAGE_IMAGE_SIZE @endinternal
  * Key the local storage the SDK uses for content image size searching.
  * @ingroup Setup_StorageIDs
*/
#define GNSDK_LOOKUP_LOCAL_STORAGE_IMAGE_SIZE		"gnsdk_lookup_local_image_size"


#ifdef __cplusplus
}
#endif

#endif /* _GNSDK_LOOKUP_LOCAL_H_ */

