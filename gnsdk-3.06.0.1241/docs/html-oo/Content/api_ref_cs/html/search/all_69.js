var searchData=
[
  ['id',['ID',['../class_gracenote_s_d_k_1_1_gn_list_element.html#a10be2dd896449b0840b1543674469738',1,'GracenoteSDK::GnListElement']]],
  ['identifier',['Identifier',['../class_gracenote_s_d_k_1_1_gn_music_i_d_file_info.html#aee3e8122a6971ce61ab0ec5eb8bbbe6f',1,'GracenoteSDK::GnMusicIDFileInfo']]],
  ['identifiers',['Identifiers',['../class_gracenote_s_d_k_1_1_gn_moodgrid_result.html#ae6b03d8c94b8dc52834e804fc1cd5adb',1,'GracenoteSDK.GnMoodgridResult.Identifiers()'],['../class_gracenote_s_d_k_1_1_gn_playlist_result.html#ab1003292d75fd7f1ad5b572f174c0f2e',1,'GracenoteSDK.GnPlaylistResult.Identifiers()']]],
  ['identify',['Identify',['../class_gracenote_s_d_k_1_1_gn_music_i_d_stream.html#a85b9a11a7789c5944b9ad6c0d7354f3d',1,'GracenoteSDK::GnMusicIDStream']]],
  ['idforsubmit',['IDForSubmit',['../class_gracenote_s_d_k_1_1_gn_list_element.html#a9d5c4326eb54b5a1f32f0a5c83492791',1,'GracenoteSDK::GnListElement']]],
  ['image',['Image',['../class_gracenote_s_d_k_1_1_gn_link.html#a6b85ed70e2dc14be37aae05203e2c2cb',1,'GracenoteSDK.GnLink.Image(GnImageSize imageSize, GnImagePreference imagePreference, uint item_ord)'],['../class_gracenote_s_d_k_1_1_gn_link.html#ab85a3aed8fb30f5fe32b8691039ba520',1,'GracenoteSDK.GnLink.Image(GnImageSize imageSize, GnImagePreference imagePreference)']]],
  ['init_5fgnsdk_5fmodule',['init_gnsdk_module',['../class_gracenote_s_d_k_1_1_gn_s_d_k.html#a2e896fa9b2de55bbc6a5e7d49190c4eb',1,'GracenoteSDK::GnSDK']]],
  ['isalbum',['IsAlbum',['../class_gracenote_s_d_k_1_1_gn_data_match.html#a99ef247b9e046613894015e4540ae9b6',1,'GracenoteSDK::GnDataMatch']]],
  ['isclassical',['IsClassical',['../class_gracenote_s_d_k_1_1_gn_album.html#adfaec66196db62889a3ec921b591f846',1,'GracenoteSDK::GnAlbum']]],
  ['iscontributor',['IsContributor',['../class_gracenote_s_d_k_1_1_gn_data_match.html#ac0f4de2f60c852b892cc24699f4e228a',1,'GracenoteSDK::GnDataMatch']]],
  ['isseedrequiredinstatement',['IsSeedRequiredInStatement',['../class_gracenote_s_d_k_1_1_gn_playlist_collection.html#a2a7e5fb084ba009a28f2805cb1253c07',1,'GracenoteSDK::GnPlaylistCollection']]],
  ['istype',['IsType',['../class_gracenote_s_d_k_1_1_gn_data_object.html#a84790bd49bf8a960cfc3e966b78f5ecc',1,'GracenoteSDK::GnDataObject']]],
  ['isupdateavailable',['IsUpdateAvailable',['../class_gracenote_s_d_k_1_1_gn_list.html#a1269f39b26c246465650e635b1021447',1,'GracenoteSDK::GnList']]]
];
