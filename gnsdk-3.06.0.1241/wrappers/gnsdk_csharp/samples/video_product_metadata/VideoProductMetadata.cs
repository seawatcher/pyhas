/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoProductsMetadata.cs
 *  Description:
 *  This sample shows accessing product metadata: Disc > Side >  Layer >  Feature > Chapters.
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoProductMetadata
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
        private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoProductLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Product Metadata ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoProductLookupEvents localeEvents = new VideoProductLookupEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DisplayMultipleProduct(GnResponseVideoProduct videoResponse, GnUser user)
        {
            int count = 0;

            /*
             * LOOP THROUGH MATCHES
             *
             * Note that the GDO accessors below are "ordinal" based, not index based.  so the 'first' of
             * anything has a one-based ordinal of '1' - "not" an index of '0'
             */

            GnVideoProductEnumerable videoProductEnumerable = videoResponse.Products;

            foreach (GnVideoProduct videoProduct in videoProductEnumerable)
            {
                Console.WriteLine("\nMatch : " + ++count);
                DispalyBasicData(videoProduct);
            }
        }

        private static void DisplayChapters(GnVideoFeature gnVideoFeature)
        {
            string value = null;
            /* Get chapter count */
            uint chapterCount = gnVideoFeature.Chapters.count();

            Console.WriteLine("\t\t\tchapters: " + chapterCount);

            GnVideoChapterEnumerable chapterEnumerable = gnVideoFeature.Chapters;

            foreach (GnVideoChapter gnVideoChapter in chapterEnumerable)
            {
                /* Get chapter number */
                int chapternumStr = gnVideoChapter.Ordinal;

                /* Get the chapter title name GDO */
                GnTitle gnChpaterTitle = gnVideoChapter.OfficialTitle;

                value = gnChpaterTitle.Display;
                if (value != null && value != "")
                    Console.Write("\t\t\t\t" + chapternumStr + ": " + value);

                int duration = gnVideoChapter.Duration;

                if (duration > 0)
                {
                    int seconds = duration;
                    int minutes = seconds / 60;
                    int hours = minutes / 60;
                    seconds = seconds - (60 * minutes);
                    minutes = minutes - (60 * hours);
                    Console.WriteLine(" [" + hours.ToString("0") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00") + "]");
                }

            } /* For each chapter */
        }

        private static void DisplayLayers(GnVideoSide side)
        {
            string value = null;
            uint layerCount = side.Layers.count();
            if (layerCount > 0)
                Console.WriteLine("\tNumber layers: " + layerCount);
            else
            {
                Console.WriteLine("\tNo layer data");
                return;
            }

            GnVideoLayerEnumerable layerEnumerable = side.Layers;

            foreach (GnVideoLayer gnVideoLayer in layerEnumerable)
            {
                /* Get the layer number */
                uint layerNumber = Convert.ToUInt32(gnVideoLayer.Ordinal);

                /* matched - was this the layer that matched the initial query input */
                string matched = null;

                if (!gnVideoLayer.Matched)
                    matched = "";
                else
                    matched = "MATCHED";

                Console.WriteLine("\t\tLayer " + layerNumber + " -------- " + matched);

                /* Get media type */
                value = gnVideoLayer.MediaType;
                if (value != null && value != "")
                    Console.WriteLine("\t\tMedia type: " + value);

                /* Get TV system */
                value = gnVideoLayer.TvSystem;
                if (value != null && value != "")
                    Console.WriteLine("\t\tTV system: " + value);

                /* Get region */
                value = gnVideoLayer.RegionCode;
                if (value != null && value != "")
                    Console.WriteLine("\t\tRegion Code: " + value);
                value = gnVideoLayer.VideoRegion;
                if (value != null && value != "")
                    Console.WriteLine("\t\tVideo Region: " + value);

                /* Get aspect ratio */
                value = gnVideoLayer.AspectRatio;
                if (value != null && value != "")
                {
                    Console.Write("\t\tAspect ratio: " + value);

                    /* Get aspect ration type */
                    value = gnVideoLayer.AspectRatioType;
                    if (value != null && value != "")
                        Console.Write(" [" + value + "]");
                    Console.WriteLine();
                }

                /* Get number of layer features */
                uint featureCount = gnVideoLayer.Features.count();
                if (featureCount > 0)
                    Console.WriteLine("\t\tFeatures: " + featureCount);

                /* Loop thru features */
                GnVideoFeatureEnumerable videoFeatureEnumerable = gnVideoLayer.Features;
                foreach (GnVideoFeature gnVideoFeature in videoFeatureEnumerable)
                {
                    /* Get feature number */
                    int featureNumber = gnVideoFeature.Ordinal;

                    /* Get matched */
                    if (!gnVideoFeature.Matched)
                        matched = "";
                    else
                        matched = "MATCHED";

                    Console.WriteLine("\n\t\t\tFeature " + featureNumber + " -------- " + matched);

                    /* Get the feature title name GDO */
                    GnTitle gnTitle = gnVideoFeature.OfficialTitle;

                    /* Get title */
                    value = gnTitle.Display;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tFeature title: " + value);

                    int duration = gnVideoFeature.Duration;

                    if (duration > 0)
                    {
                        int seconds = duration;
                        int minutes = seconds / 60;
                        int hours = minutes / 60;
                        seconds = seconds - (60 * minutes);
                        minutes = minutes - (60 * hours);
                        Console.WriteLine("\t\t\tLength: " + hours.ToString("0") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00"));
                    }

                    /* Aspect ratio */
                    value = gnVideoFeature.AspectRatio;
                    if (value != null && value != "")
                    {
                        Console.Write("\t\t\tAspect ratio: " + value);

                        /* Aspect ratio type */
                        value = gnVideoFeature.AspectRatioType;
                        if (value != null && value != "")
                            Console.Write(" [" + value + "]");
                        Console.WriteLine();
                    }
                    /*
                     * Note: Genre display strings come from a genre hierarchy, so you can choose what level
                     * of granularity to display.  GNSDK_GDO_VALUE_GENRE_LEVEL1 are the top level genres and
                     * the most general, GNSDK_GDO_VALUE_GENRE_LEVEL2 is the mid-level genre and the most
                     * granular genre is GNSDK_GDO_VALUE_GENRE_LEVEL#.
                     */

                    /* Feature genre(s)  */
                    value = gnVideoFeature.Genre.Level1;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tPrimary genre: " + value);

                    /* Feature rating */
                    GnRating gnFeatureRating = gnVideoFeature.Rating;
                    value = gnFeatureRating.Rating;
                    if (value != null && value != "")
                    {
                        Console.Write("\t\t\tRating: " + value);
                        value = gnFeatureRating.RatingType;
                        if (value != null && value != "")
                            Console.Write(" [" + value + "]");
                        value = gnFeatureRating.RatingDesc();
                        if (value != null && value != "")
                            Console.Write(" - " + value);
                        Console.WriteLine();
                    }

                    /* Feature type */
                    value = gnVideoFeature.VideoFeatureType;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tFeature type: " + value);

                    /* Video type */
                    value = gnVideoFeature.VideoProductionType;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tProduction type: " + value);

                    /* feature plot */
                    value = gnVideoFeature.PlotSummary;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tPlot summary: " + value);
                    value = gnVideoFeature.PlotSynopsis;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tPlot synopsis: " + value);
                    value = gnVideoFeature.PlotTagline;
                    if (value != null && value != "")
                        Console.WriteLine("\t\t\tTagline: " + value);

                    DisplayChapters(gnVideoFeature);

                }

            }
        }

        private static void DisplayDiscInformation(GnVideoProduct product)
        {
            /* Get disc count */
            uint discCount = product.Discs.count();
            Console.WriteLine("\nDiscs:" + discCount);

            /* Discs in set */
            GnVideoDiscEnumerable discEnumerable = product.Discs;
            //metadata::disc_iterator discItr = product.Discs().begin();

            foreach (GnVideoDisc disc in discEnumerable)/*Discs in set loop*/
            {
                /* Disc number */
                int discNumber = disc.Ordinal;
                /* Matched --  */
                string discMatch = null;

                if (!disc.Matched)
                    discMatch = "";
                else
                    discMatch = "MATCHED";

                Console.WriteLine("disc " + discNumber + " -------- " + discMatch);

                GnTitle discTitle = disc.OfficialTitle;
                Console.WriteLine("\tTitle: " + discTitle.Display);

                /* Count the number of sides */
                uint sideCount = disc.Sides.count();

                Console.WriteLine("\tNumber sides: " + sideCount);

                /* Sides in set - sides details */
                GnVideoSideEnumerable sideEnumerable = disc.Sides;

                foreach (GnVideoSide side in sideEnumerable)/*side loop*/
                {
                    /* Side number */
                    int sideNumber = side.Ordinal;
                    /* Matched -- */
                    string sideMatch = null;
                    if (!side.Matched)
                        sideMatch = "";
                    else
                        sideMatch = "MATCHED";

                    Console.WriteLine("\tSide " + sideNumber + " -------- " + sideMatch);

                    DisplayLayers(side);

                }/*side loop*/

            }/*Discs in set loop*/
        }

        private static void DispalyBasicData(GnVideoProduct videoProduct)
        {
            string value = null;
            /* Get the video title GDO */
            GnTitle productTitle = videoProduct.OfficialTitle;

            /* Display video title */
            Console.WriteLine("Title: " + productTitle.Display);

            /* Video edition */
            value = productTitle.StringValue("gnsdk_val_edition" /*gnsdk_marshal.GNSDK_GDO_VALUE_EDITION*/);
            if (value != null && value != "")
                Console.WriteLine("Edition: " + value);

            /* Video type */
            value = videoProduct.VideoProductionType;
            if (value != null && value != "")
                Console.WriteLine("Production type: " + value);

            /* Video original release */
            value = videoProduct.DateOriginalRelease;
            if (value != null && value != "")
                Console.WriteLine("Orig release: " + value);

            /* Rating */
            GnRating rating = videoProduct.Rating;
            value = rating.Rating;
            if (value != null && value != "")
            {
                Console.Write("Rating: " + value);

                value = rating.RatingType;
                if (value != null && value != "")
                    Console.Write(" [" + value + "]");

                value = rating.RatingDesc();
                if (value != null && value != "")
                    Console.WriteLine(" - " + value);

                Console.WriteLine();
            }
            /* Video release year */
            value = videoProduct.DateRelease;
            if (value != null && value != "")
                Console.WriteLine("Release: " + value);

            /* Discs in set */
            Console.WriteLine("Discs: " + videoProduct.Discs.count());
        }

        private static void DisplaySingleProduct(GnResponseVideoProduct videoResponse, GnUser user)
        {
            /* Get the current product from the response */
            GnVideoProductEnumerable productEnumerable = videoResponse.Products;
            GnVideoProductEnumerator productEnumerator = productEnumerable.GetEnumerator();
            GnVideoProduct product = productEnumerator.Current;
            //foreach (GnVideoProduct product in productEnumerable)
            {

                DispalyBasicData(product);

                GnVideoProduct fullVideoProduct = product;
                if (!product.FullResult)
                {
                    /* Get a GDO with full metadata */
                    GnVideo gnVideo = new GnVideo(user);
                    GnResponseVideoProduct response = gnVideo.FindProducts(product);
                    fullVideoProduct = response.Products.at(0).next();
                }

                DisplayDiscInformation(fullVideoProduct);
            }
            
        }

        private static void DoProductSearch(GnUser user)
        {
            string searchTitle = "Star";

            Console.WriteLine("\n*****Sample Title Search: '" + searchTitle + "'*****");


            using (GnStatusEventsDelegate videoEvents = new VideoProductLookupEvents())
            {
                GnVideo video = new GnVideo(user, null /*videoEvents*/);

                /* Setting range values */
                video.OptionRangeStart("1");
                video.OptionRangeSize("20");

                GnResponseVideoProduct videoResponse = video.FindProducts(searchTitle, GnVideoSearchField.kSearchFieldProductTitle, GnVideoSearchType.kSearchTypeDefault);

                if (1 == videoResponse.Products.count())
                {
                    DisplaySingleProduct(videoResponse, user);
                }
                else
                {
                    /* We now have 1-n matches needing resolution  
                    DisplayMultipleProduct(videoResponse, user);*/

                    /* Typically the user would choose one (or none) of the presented choices.
                     * For this simplified sample, just pick the first choice  */

                    DisplaySingleProduct(videoResponse, user);

                }
            }
        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Lookup products and display */
                        DoProductSearch(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }

    }
}
