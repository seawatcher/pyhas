/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* gnsdk_dsp.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */
#include "gnsdk.hpp"

#if GNSDK_DSP

using namespace gracenote;
using namespace gracenote::dsp;

/******************************************************************************
** GnDsp
*/
GnDsp::GnDsp(const GnUser& user, GnDspFeatureType featureType, gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels)  throw ( GnError )
{
	gnsdk_error_t error;

	gracenote::GnSDK::init_gnsdk_module(GNSDK_COMPONENT_DSP);

	error = gnsdk_dsp_feature_audio_begin(user.native(), _GetFeatureType(featureType), audioSampleRate, audioSampleSize, audioChannels, &mFeatureHandle);

	if (error) { throw GnError(); }
}


GnDsp::~GnDsp()
{
	gnsdk_dsp_feature_release(mFeatureHandle);
}


/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t
GnDsp::Version()
{
	return gnsdk_dsp_get_version();
}


/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t
GnDsp::BuildDate()
{
	return gnsdk_dsp_get_build_date();
}


/*-----------------------------------------------------------------------------
 *  FeatureAudioWrite
 */
bool
GnDsp::FeatureAudioWrite(const gnsdk_byte_t* audioData, gnsdk_size_t audioDataBytes) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_bool_t  b_complete;

	error = gnsdk_dsp_feature_audio_write(mFeatureHandle, audioData, audioDataBytes, &b_complete);
	if (error) { throw GnError(); }

	if (b_complete) { return true; }
	else { return false; }
}


/*-----------------------------------------------------------------------------
 *  FeatureEndOfAudioWrite
 */
void
GnDsp::FeatureEndOfAudioWrite()  throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_dsp_feature_end_of_write(mFeatureHandle);
	if (error) { throw GnError(); }
}


/*-----------------------------------------------------------------------------
 *  FeatureRetrieve
 */
GnDspFeature*
GnDsp::FeatureRetrieve() throw ( GnError )
{
	gnsdk_dsp_feature_qualities_t featureQality;
	gnsdk_error_t                 error;

	GnDspFeature* dspFeature = new GnDspFeature;

	/* To retrieve dsp feature data, here passing pointer of GnDspFeature's member i.e dsp_feature->data. */
	error = gnsdk_dsp_feature_retrieve_data(mFeatureHandle, &featureQality, &( dspFeature->mData ) );

	if (error) { throw GnError(); }

	switch (featureQality)
	{
	case GNSDK_DSP_FEATURE_QUALITY_DEFAULT:
		dspFeature->mFeatureQuality = kDspFeatureQualityStandard;
		break;

	case GNSDK_DSP_FEATURE_QUALITY_SHORT:
		dspFeature->mFeatureQuality = kDspFeatureQualityShort;
		break;

	case GNSDK_DSP_FEATURE_QUALITY_SILENT:
		dspFeature->mFeatureQuality = kDspFeatureQualitySilent;
		break;
	}

	return dspFeature;
}


#endif /* GNSDK_DSP */

