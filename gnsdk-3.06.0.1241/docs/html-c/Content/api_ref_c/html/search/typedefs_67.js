var searchData=
[
  ['gnsdk_5fdsp_5ffeature_5fhandle_5ft',['gnsdk_dsp_feature_handle_t',['../group___music___fingerprinting___types_enums.html#ga938a01d7a207bccf98f627d6a786f953',1,'gnsdk_dsp.h']]],
  ['gnsdk_5fdsp_5ffeature_5fqualities_5ft',['gnsdk_dsp_feature_qualities_t',['../group___music___fingerprinting___types_enums.html#gac043b709d1c47a916bcf42215cce67bf',1,'gnsdk_dsp.h']]],
  ['gnsdk_5fmanager_5flogging_5fcallback_5ffn',['gnsdk_manager_logging_callback_fn',['../group___setup___logging___callback.html#ga32c455836c6bb5156b9978e24d78fb11',1,'gnsdk_manager_logging.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fget_5ffingerprint_5ffn',['gnsdk_musicidfile_callback_get_fingerprint_fn',['../group___music___music_i_d_file___callbacks.html#ga66efe9f4254fa994a2ed8c5858faf0ac',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fget_5fmetadata_5ffn',['gnsdk_musicidfile_callback_get_metadata_fn',['../group___music___music_i_d_file___callbacks.html#gaf99fa8ad5a32a3ff39598fb0f9a5006a',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fmusicid_5fcomplete_5ffn',['gnsdk_musicidfile_callback_musicid_complete_fn',['../group___music___music_i_d_file___callbacks.html#gaf726c372b546d425d3010d496d5011f9',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fresult_5favailable_5ffn',['gnsdk_musicidfile_callback_result_available_fn',['../group___music___music_i_d_file___callbacks.html#ga16cb29c2da186c3505c305eb89a18f85',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fresult_5fnot_5ffound_5ffn',['gnsdk_musicidfile_callback_result_not_found_fn',['../group___music___music_i_d_file___callbacks.html#ga0e031abb31ce0d9180813a5e48cc5ae9',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallback_5fstatus_5ffn',['gnsdk_musicidfile_callback_status_fn',['../group___music___music_i_d_file___callbacks.html#gab6ad324446767245b08b1ad2bbe910e4',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5fcallbacks_5ft',['gnsdk_musicidfile_callbacks_t',['../group___music___music_i_d_file___callbacks.html#ga5d53d5d0033b661f5126da134f6973cf',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5ffileinfo_5fcallbacks_5ft',['gnsdk_musicidfile_fileinfo_callbacks_t',['../group___music___music_i_d_file___callbacks.html#ga0d359ff6010c28d870ab4ac8239cf446',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fmusicidfile_5ffileinfo_5fuserdata_5fdelete_5ffn',['gnsdk_musicidfile_fileinfo_userdata_delete_fn',['../group___music___music_i_d_file___callbacks.html#gaa64057c6adb4d1a97a523a99592beb21',1,'gnsdk_musicid_file.h']]],
  ['gnsdk_5fplaylist_5fupdate_5fcallback_5ffn',['gnsdk_playlist_update_callback_fn',['../group___playlist___callbacks.html#ga54cf72f8d6bff3524a3c8a4aa6017cde',1,'gnsdk_playlist.h']]],
  ['gnsdk_5fstatus_5fcallback_5ffn',['gnsdk_status_callback_fn',['../group___status_callbacks___callbacks.html#ga6ec737297ed4f8473c9ac731ef8f156b',1,'gnsdk_status.h']]]
];
