var a00140 =
[
    [ "GnStorageSqlite", "a00140.html#accd106f833cfe31f96c40f5d3ffce477", null ],
    [ "GnStorageSqlite", "a00140.html#a580eee2e93e3cdd67f0265f09ef0b053", null ],
    [ "delete", "a00140.html#a615b07f9c90d623b3c65cda0641f67f8", null ],
    [ "finalize", "a00140.html#a5a6171c7585c4cea21b71ef2a443cee3", null ],
    [ "journalModeGet", "a00140.html#ab6062547228b640790156de6304339ae", null ],
    [ "journalModeSet", "a00140.html#a4b6234dcfae89b3881bb91c357d7b513", null ],
    [ "maximumMemorySizeForCacheGet", "a00140.html#ac769e69a318bbc4e2a5880b8cb9a9885", null ],
    [ "maximumMemorySizeForCacheSet", "a00140.html#aa19d05dd565599cb258db2cc7f1d828d", null ],
    [ "maximumSizeForCacheFileGet", "a00140.html#a5f37542da3740ad65cfd7c1f8e021c67", null ],
    [ "maximumSizeForCacheFileSet", "a00140.html#a38b35b3cb19c2b44466335b634766a19", null ],
    [ "storageFolderGet", "a00140.html#af040b851cf2b6ff353ae38bdb5317cc5", null ],
    [ "storageFolderSet", "a00140.html#ab8212f40155f43838cac68c5174a7a96", null ],
    [ "synchronousOptionForCacheGet", "a00140.html#a92688952867ecfef36cce3a81289bca6", null ],
    [ "synchronousOptionForCacheSet", "a00140.html#a3c2e650c4003ee0f40f6ed415b1001df", null ]
];