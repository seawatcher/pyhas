/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_HPP_
#define _GNSDK_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#ifdef _MSC_VER
#pragma warning( disable : 4290 ) // disable "warning C4290: C++ exception specification ignored except to indicate a function is not __declspec(nothrow)"
#endif

#define GNSDK_STRICT_HANDLES    1

#include "gnsdk.h"
#include "gnsdk_iterator_base.hpp"
#include "gn_audiosource.hpp"

#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
	TypeName(const TypeName &);            \
	void                                   \
	operator                 = (const TypeName&)

#define MOVE_BUT_DISALLOW_COPY_AND_ASSIGN(TypeName) \
	TypeName(TypeName &);                           \
	void                                            \
	operator = (TypeName&)

#define GNSDK_COMPONENT_MANAGER           0x01
#define GNSDK_COMPONENT_MUSICID           0x02
#define GNSDK_COMPONENT_MUSICIDFILE       0x04
#define GNSDK_COMPONENT_DSP               0x08
#define GNSDK_COMPONENT_LINK              0x10
#define GNSDK_COMPONENT_MUSICIDMATCH      0x20
#define GNSDK_COMPONENT_STORAGE_SQLITE    0x40
#define GNSDK_COMPONENT_LOOKUP_LOCAL      0x80
#define GNSDK_COMPONENT_LOOKUP_FPLOCAL    0x100
#define GNSDK_COMPONENT_SUBMIT            0x200
#define GNSDK_COMPONENT_VIDEO             0x400
#define GNSDK_COMPONENT_PLAYLIST          0x800
#define GNSDK_COMPONENT_MOODGRID          0x1000
#define GNSDK_COMPONENT_ACR               0x2000
#define GNSDK_COMPONENT_EPG               0x4000
#define GNSDK_COMPONENT_CORRELATES        0x8000
#define GNSDK_COMPONENT_MUSICIDSTREAM     0x10000

#define GNSDK_NULL_GDO                    (gnsdk_gdo_handle_t)GNSDK_NULL

/*
 * #ifdef SWIGPYTHON
 * #define GNSDK_FUNCTION_DECLARE(function_name) %feature("autodoc", "2"); function_name
 * #else
 * #define GNSDK_FUNCTION_DECLARE(function_name) function_name
 * #endif */

namespace gracenote
{
	/**************************************************************************
	** Enums
	*/
	enum GnLookupMode
	{
		kLookupModeInvalid = 0,

		/**
		 * This mode forces the lookup to be done against the local database only. Local caches created from (online) query
		 * results are not queried in this mode.
		 * If no local database exists, the query will fail.
		 */
		kLookupModeLocal,

		/**
		 * This is the default lookup mode. If a cache exists, the query checks it first for a match.
		 * If a no match is found in the cache, then an online query is performed against Gracenote Service.
		 * If a result is found there, it is stored in the local cache.  If no online provider exists, the query will fail.
		 * The length of time before cache lookup query expires can be set via the user object.
		 */
		kLookupModeOnline,

		/**
		 * This mode forces the query to be done online only and will not perform a local cache lookup first.
		 * If no online provider exists, the query will fail. In this mode online queries and lists are not
		 * written to local storage, even if a storage provider has been initialize.
		 */
		kLookupModeOnlineNoCache,

		/**
		 * This mode forces the query to be done online only and will not perform a local cache lookup first.
		 * If no online provider exists, the query will fail. If a storage provider has been initialized,
		 * queries and lists are immediately written to local storage, but are never read unless the lookup mode is changed.
		 */
		kLookupModeOnlineNoCacheRead,

		/**
		 * This mode forces the query to be done against the online cache only and will not perform a network lookup.
		 * If no online provider exists, the query will fail.
		 */
		kLookupModeOnlineCacheOnly
	};

	enum GnLookupData
	{
		kLookupDataInvalid = 0,

		/**
		 * Indicates whether a response should include data for use in fetching content (like images).
		 * <p><b>Remarks:</b></p>
		 * An application's client ID must be entitled to retrieve this specialized data. Contact your
		 *	Gracenote Global Services and Support representative with any questions about this enhanced
		 *	functionality.
		 */
		kLookupDataContent,

		/**
		 * Indicates whether a response should include any associated classical music data.
		 * <p><b>Remarks:</b></p>
		 * An application's license must be entitled to retrieve this specialized data. Contact your
		 * Gracenote Global Services and Support representative with any questions about this enhanced functionality.
		 */
		kLookupDataClassical,

		/**
		 * Indicates whether a response should include any associated sonic attribute data.
		 * <p><b>Remarks:</b></p>
		 * An application's license must be entitled to retrieve this specialized data. Contact your
		 * Gracenote Global Services and Support representative with any questions about this enhanced functionality.
		 */
		kLookupDataSonicData,

		/**
		 * Indicates whether a response should include associated attribute data for GNSDK Playlist.
		 * <p><b>Remarks:</b></p>
		 * An application's license must be entitled to retrieve this specialized data. Contact your
		 * Gracenote Global Services and Support representative with any questions about this enhanced functionality.
		 */
		kLookupDataPlaylist,

		/**
		 * Indicates whether a response should include external IDs (third-party IDs).
		 * <p><b>Remarks:</b></p>
		 * External IDs are third-party IDs associated with the results (such as an Amazon ID),
		 *	configured specifically for your application.
		 * An application's client ID must be entitled to retrieve this specialized data. Contact your
		 * Gracenote Global Services and Support representative with any questions about this enhanced functionality.
		 * External IDs can be retrieved from applicable query response objects.
		 */
		kLookupDataExternalIDs,

		/**
		 * Indicates whether a response should include global IDs.
		 */
		kLookupDataGlobalIDs,

		/**
		 * Indicates whether a response should include additional credits.
		 */
		kLookupDataAdditionalCredits
	};

	/**
	 * Thread priority values for GNSDK multi-threaded functionality such as  MusicID-File.
	 */
	enum GnThreadPriority
	{
		kThreadPriorityInvalid = 0,

		/**
		 * Use of default thread priority.
		 */
		kThreadPriorityDefault,

		/**
		 * Use idle thread priority.
		 */
		kThreadPriorityIdle,

		/**
		 * Use low thread priority (default).
		 */
		kThreadPriorityLow,

		/**
		 * Use normal thread priority.
		 */
		kThreadPriorityNormal,

		/**
		 * Use high thread priority.
		 */
		kThreadPriorityHigh
	};

	/**
	 * Fingerprint algorithm type.
	 */
	enum GnFingerprintType
	{
		kFingerprintTypeInvalid = 0,

		kFingerprintTypeFile,

		/**
		 * <p><b>Remarks:</b></p>
		 * This is the option to use the 3-second streaming-audio algorithm, and this is the Gracenote recommended option
		 */
		kFingerprintTypeStream3,

		/**
		 * <p><b>Remarks:</b></p>
		 * This is the option to use the 6-second streaming-audio algorithm
		 */
		kFingerprintTypeStream6,

		/**
		 * Specifies a Cantametrix (CMX) fingerprint data type for generating fingerprints
		 * <p><b>Remarks:</b></p>
		 * A CMX fingerprint is a fingerprint of the beginning 16 seconds of audio in an audio file such
		 * as a purchased MP3.
		 * <p><b>Note:</b></p>
		 * Do not design your application to submit only 16 seconds of an audio file; the application must submit
		 * data until GNSDK indicates it has received enough input to overcome any initial silence in the track.
		 */
		kFingerprintTypeCMX,

		/**
		 * Specifies a Gracenote Fingerprint Extraction (GNFPX) data type for generating fingerprints used
		 * with MusicID-Stream.
		 * <p><b>Remarks:</b></p>
		 * A GNFPX fingerprint is a fingerprint of a specific 6-second point in time of the audio stream;
		 * this is also known as a time slice. Use this fingerprint type when identifying a continuous stream
		 * of audio data with MusicID-Stream, and when retrieving Track Match Position values.
		 * <p><b>Note:</b></p>
		 * Do not design your application to submit only one or more precise time slices of an audio stream; the
		 * application must submit data until GNSDK indicates it has received enough input.
		 * The usage of this fingerprint type must be configured to your specific User ID, otherwise queries
		 * of this type will not succeed.
		 */
		kFingerprintTypeGNFPX
	};

	/**
	 * GNSDK error condition.
	 */
	class GnError
	{
	public:
		GnError();

		GnError(const gnsdk_error_info_t* pErrorInfo);

		/**
		 * Retrieves this error's code.
		 */
		gnsdk_error_t ErrorCode(void);

		/**
		 * Retrieves this error's description.
		 */
		gnsdk_cstr_t ErrorDescription(void);

		/**
		 * Retrieves the API this error occurred in.
		 */
		gnsdk_cstr_t ErrorAPI(void);

		/**
		 * Retrieves the module this error occurred in.
		 */
		gnsdk_cstr_t        ErrorModule(void);

		gnsdk_error_t       SourceErrorCode(void);

		gnsdk_cstr_t        SourceErrorModule(void);

	private:
		void                _copy_errorinfo(const gnsdk_error_info_t* pErrorInfo);

		gnsdk_char_t        _copy_buf[512];
		gnsdk_error_info_t* m_pErrorInfo;
	};

	class GnObject
	{
	protected:
		GnObject() throw ( GnError ) : handle_(GNSDK_NULL)
		{
			gnsdk_error_t error = gnsdk_manager_initialize(GNSDK_NULL, 0, 0);

			if(error) { throw GnError(); }
		}

		GnObject(gnsdk_handle_t handle) throw ( GnError ) : handle_(handle)
		{
			gnsdk_error_t error = gnsdk_manager_initialize(GNSDK_NULL, 0, 0);

			if(error) { throw GnError(); }

			if(GNSDK_NULL != handle)
			{
				error = gnsdk_handle_addref(handle_);
				if(error) { throw GnError(); }
			}
		}

		GnObject(const GnObject& copy) : handle_(GNSDK_NULL)
		{
			gnsdk_error_t error = gnsdk_manager_initialize(GNSDK_NULL, 0, 0);

			if(error) { throw GnError(); }

			if(GNSDK_NULL != copy.handle_)
			{
				handle_ = copy.handle_;
				error   = gnsdk_handle_addref(handle_);
				if(error) { throw GnError(); }
			}
		}

		GnObject&
		operator = (const GnObject& rhs)
		{
			gnsdk_error_t error;

			// Check for self assignment
			if(this == &rhs)
			{
				return *this;
			}

			if(GNSDK_NULL != handle_)
			{
				error = gnsdk_handle_release(handle_);
				if(error) { throw GnError(); }
			}

			handle_ = rhs.handle_;

			if(GNSDK_NULL != handle_)
			{
				error = gnsdk_handle_addref(handle_);
				if(error) { throw GnError(); }
			}

			return *this;
		}

		virtual
		~GnObject()
		{
			if(GNSDK_NULL != handle_) { gnsdk_handle_release(handle_); }
			gnsdk_manager_shutdown();
		}

		void
		Reset(gnsdk_handle_t handle)
		{
			if(handle == handle_)
			{
				return;
			}

			gnsdk_error_t error;
			if(GNSDK_NULL != handle_)
			{
				error = gnsdk_handle_release(handle_);
				if(error) { throw GnError(); }
			}

			handle_ = handle;
			if(GNSDK_NULL != handle_)
			{
				error = gnsdk_handle_addref(handle_);
				if(error) { throw GnError(); }
			}
		}

		void
		AcceptOwnership(gnsdk_handle_t handle)
		{
			if(GNSDK_NULL != handle_)
			{
				gnsdk_error_t error = gnsdk_handle_release(handle_);
				if(error) { throw GnError(); }
			}
			handle_ = handle;
		}

		gnsdk_handle_t
		               Handle() const { return handle_; }

		template<typename HandleType>
		HandleType
		               get() const    { return static_cast<HandleType>( handle_ ); }

	private:
		gnsdk_handle_t handle_;
	};

	/**
	 * Encapsulation of a managed string as returned by GNSDK.
	 */
	class GnString : public GnObject
	{
	public:
		explicit
		GnString(gnsdk_str_t str) : GnObject(str)  { }

		/**
		 * Retrieve the raw string.
		 */
		const char*
		c_str() const { return this->get<gnsdk_str_t>(); }

		/* disallow assignment operator    */
	};

	/**
	 * Delegate object for receiving status updates as GNSDK operations are performed.
	 */
	class GnStatusEvents
	{
	public:
		virtual
		~GnStatusEvents() { }

		/**
		 * Status change notification method. Override to receive notification.
		 * @param status				Status type
		 * @param percent_complete		Operation progress
		 * @param bytes_total_sent		Total number of bytes sent
		 * @param bytes_total_received	Total number of bytes received
		 */
		virtual void
		status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
		{
			/* default impl: ignore status */
			(void)status;
			(void)percent_complete;
			(void)bytes_total_sent;
			(void)bytes_total_received;
		}

		/**
		 * Cancel method. Override to indicate the operation should cancel.
		 * @return True if the operation should be cancelled, false otherwise.
		 */
		virtual bool
		cancel_check(void)
		{
			/* default impl: don't cancel */
			return false;
		}
	};

	/**
	 * Cancel method. Override to indicate the operation should cancel.
	 * @return True if the operation should be cancelled, false otherwise.
	 */
	class GnUser : public GnObject
	{
	public:
		/**
		 * Reconstitutes user handle from serialized user handle data.
		 * Use this constructor to reconstitute a previously serialized GnUser. Reconstitution does not
		 * count towards the user count for your client in Gracenote Service.
		 * @param serialized_user		String of serialized user handle data
		 */
		GnUser(gnsdk_cstr_t serialized_user, gnsdk_cstr_t clientId, gnsdk_cstr_t clientTag, gnsdk_cstr_t applicationVersion) throw ( GnError );

		virtual
		~GnUser();

		/**
		 * Retrieve this user's native handle.
		 */
		gnsdk_user_handle_t
		native() const { return m_user_handle; }

		/**
		 * Set this user's lookup mode.
		 * @param mode		Lookup mode
		 */
		void OptionLookupMode(GnLookupMode lookupMode) throw ( GnError );

		/**
		 * Sets host name, username and password for proxy to route GNSDK queries through.
		 * @param hostname		Fully qualified host name with optional port number. If no port number
		 *                  is given the default port number is assumed to be 80.
		 *                  Example Option Values http://proxy.mycompany.com:8080/ proxy.mycompany.com:8080
		 *                  proxy.mycompany.com
		 * @param username  Valid user name for the proxy server. Do not set this option if a username is not required.
		 * @param password      Valid password for the proxy server. Do not set this option if a password is not required.
		 */
		void OptionNetworkProxy(gnsdk_cstr_t hostname, gnsdk_cstr_t username = GNSDK_NULL, gnsdk_cstr_t password = GNSDK_NULL) throw ( GnError );

		/**
		 * Sets the network time-out for all GNSDK queries. Option value is in milliseconds.
		 * Value for this option is a string with a numeric value that indicates the number of milliseconds
		 * to set for network time-outs.
		 * Example Option Values
		 * For 30-second time-out, set option value as "30000"
		 * @param timeout_ms	Time-out in milliseconds
		 */
		void OptionNetworkTimeout(gnsdk_cstr_t timeout_ms) throw ( GnError );

		/**
		 * Enables distributing queries across multiple Gracenote co-location facilities. When not enabled,
		 * queries will generally resolve to a single co-location.
		 * To implement load balancing, enable this option.
		 * Value to enable this option must be a boolean value that indicates true.
		 * @param b_enable  True to enable load balancing, false otherwise.
		 * <p><b>Note:</b></p>
		 * Ensure that any security settings (such as a firewall) in your network infrastructure do not
		 * affect outbound access and prevent GNSDK from transmitting queries to various hosts with unique IP
		 * addresses.
		 */
		void OptionNetworkLoadBalance(bool b_enable) throw ( GnError );

		/**
		 * Set information about this user.
		 * @param location_id		Set an IP address or country code to represent the location of user performing requests.
		 *                      Value for this parameter is a string with the IP address, or a 3-character country code
		 *                      for the client making the request. This is generally required when setting a proxy for
		 *                      GNSDK queries.
		 *                      Example Option Values
		 *                          - "192.168.1.1"
		 *                          - "usa"
		 *                          - "jpn"
		 * @param mfg				The manufacturer of the device running the SDK. Used mostly by Gracenote Service to collect
		 *                      runtime statistics.
		 * @param os				The OS version of the device running the SDK. Used mostly by Gracenote Service to collect
		 *                      runtime statistics.
		 * @param uid				Unique ID of the device running the SDK, such as ESN. Used mostly by Gracenote Service to
		 *                      collect runtime statistics.
		 */
		void OptionUserInfo(gnsdk_cstr_t location_id, gnsdk_cstr_t mfg, gnsdk_cstr_t os, gnsdk_cstr_t uid) throw ( GnError );

		/**
		 * Sets the maximum duration for which an item in the GNSDK query cache is valid. This duration is in
		 * seconds, and must exceed one day.
		 * The value set for this option is a string with a numeric value that indicates the number of
		 * seconds to set for the expiration of cached queries. The maximum duration is set by Gracenote and
		 * varies by requirements.
		 * @param duration_sec		Expiry duration in seconds
		 * <p><b>Note:</b></p>
		 * Setting this option to a zero value (0) causes the cache to start deleting records upon cache
		 * hit, and not write new or updated records to the cache; in short, the cache effectively flushes
		 * itself. The cache will start caching records again once this option is set to a value greater than
		 * 0. Setting this option to a value less than 0 (for example: -1) causes the cache to use default
		 * expiration values.
		 * This value only has an effect if the application initializes a GNSDK caching library. See SQLite
		 * for information on a caching implementation.
		 * Example Option Values
		 * For a one day expiration, set an option value of "86400" (60 seconds * 60 minutes * 24 hours).
		 * For a seven day expiration set an option value of "604800" (60 seconds * 60 minutes * 24 hours *
		 * 7 days).
		 */
		void OptionCacheExpiration(gnsdk_cstr_t duration_sec) throw ( GnError );

		/**
		 * Sets the URL used for contacting Gracenote Service.
		 */
		void OptionServiceUrl(gnsdk_cstr_t base_url) throw ( GnError );

		/**
		 * Sets User option
		 */
		void OptionSet(gnsdk_cstr_t key, gnsdk_cstr_t value);

		gnsdk_cstr_t OptionGet(gnsdk_cstr_t option_key) throw ( GnError );

	private:
		gnsdk_user_handle_t m_user_handle;
	};

	/**************************************************************************
	** GnLocale
	*/
	class GnLocale : public GnObject
	{
	public:
		/**
		 * Loads the specified locale. A locale contains Gracenote lists; therefore loading a locale effectively
		 * loads multiple Gracenote lists. Lists are only loaded if they are not currently held in memory and they are
		 * loaded from the Gracenote Service or local storage according to the configuration of the user lookup mode.
		 * @param locale_group		Locale grouping which determines which lists are included when the locale is loaded
		 * @param language			Language of loaded lists where applicable
		 * @param region			Region of the loaded lists where applicable
		 * @param descriptor		Descriptor, or verbosity, of the loaded lists where applicable
		 * @param user				User object
		 * @param status_events    Status events
		 * <p><b>Note</b><p> This function blocks the current thread until the download is complete;
		 * set a GnStatusEvents function to receive progress messages.
		 *
		 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
		 */
		GnLocale(gnsdk_cstr_t locale_group, gnsdk_cstr_t language, gnsdk_cstr_t region, gnsdk_cstr_t descriptor, const GnUser& user, GnStatusEvents* status_events = GNSDK_NULL)  throw ( GnError );

		/**
		 * Reconstitutes locale from serialized locale data.
		 * @param serialized_locale	String of serialized locale handle data
		 */
		GnLocale(gnsdk_cstr_t serialized_locale, GnStatusEvents* status_events = GNSDK_NULL) throw ( GnError );

		virtual
		~GnLocale();

		/**
		 * Retrieves this locale's group.
		 */
		gnsdk_cstr_t Group() const throw ( GnError );

		/**
		 * Retrieves this locale's descriptor.
		 */
		gnsdk_cstr_t Descriptor() const throw ( GnError );

		/**
		 * Retrieves this locale's language.
		 */
		gnsdk_cstr_t Language() const throw ( GnError );

		/**
		 * Retrieves this locale's region.
		 */
		gnsdk_cstr_t Region() const throw ( GnError );

		/**
		 * Retrieves this locale's revision string.
		 */
		gnsdk_cstr_t Revision() const throw ( GnError );

		/**
		 * Updates a locale with new versions of the locale lists, if available.
		 * The application must ensure the Locale module can contact the Gracenote Service
		 * to test for a new list version by appropriately configuring the user's lookup mode to allow online access.
		 * @param user				User requesting the locale update
		 * @param status_events	Delegate object instance used to receive status and progress notifications
		 * @return True indicates updates were applied, false indicates no updates are available
		 * <p><b>Note</b></p>
		 * This function blocks the current thread until the download is complete;
		 * set a status callback function to receive progress messages.
		 *
		 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
		 */
		bool Update(const GnUser& user) throw ( GnError );

		/**
		 * Tests a locale to determine if a newer revision of any locale lists is available. If available the new list revisions
		 * are not downloaded. To download the new revisions the application must use the update method.
		 * The application must ensure the List module can contact the Gracenote Service to test for new list versions, by
		 * appropriately configuring the user's lookup mode.
		 * @param user				User requesting the locale update check
		 * @param status_events	Delegate object instance used to receive status and progress notifications
		 * @return True indicates updates are available, false otherwise.
		 * <p><b>Remarks:</b></p>
		 * Use this function to periodically check Gracenote Service for updates to an existing list handle.
		 * <p><b>Note:</b></p>
		 * You should configure application(s) to automatically check for list updates to ensure use of the
		 * most current data.
		 * You should immediately check for updates after constructing a list object from a saved serialized list string as it may
		 * be out of date.
		 * This function blocks the current thread until the download is complete;
		 * set a status callback function to receive progress messages.
		 *
		 * Long Running Potential: Network I/O
		 */
		bool UpdateCheck(const GnUser& user) throw ( GnError );

		/**
		 * Serializes a locale into encrypted text that the application can store locally for later use.
		 * <p><b>Note:</b></p>
		 * If the application is using a GNSDK local storage solution, locale lists are automatically stored
		 * and retrieved from a local store according to the configuration of user lookup option.
		 * Applications implementing their own local storage functionality can use Serialize() to
		 * render a list into a format that can be stored persistently and restored at a later time using
		 * the appropriate constructor.
		 */
		GnString Serialize() const throw ( GnError );

		/**
		 * Retrieve this locale's native handle.
		 */
		gnsdk_locale_handle_t
		                               native() const { return localeHandle_; }

	private:
		GnStatusEvents*                pEventHandler_;
		gnsdk_locale_handle_t          localeHandle_;

		static void GNSDK_CALLBACK_API _locale_status_callback(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort);

		friend class GnLocaleInfo;

		DISALLOW_COPY_AND_ASSIGN(GnLocale);
	};

	/**
	 * Providing information about a locale, used where GNSDK delivers a locale description such as
	 * iterating the available locales.
	 */
	class GnLocaleInfo : public GnObject
	{
	public:
		GnLocaleInfo() : m_locale_group(NULL), m_locale_lang(NULL), m_locale_descriptor(NULL), m_locale_region(NULL) { }

		/**
		 * Retrieve locale group
		 */
		gnsdk_cstr_t Group(void)
		{
			return m_locale_group;
		}

		/**
		 * Retrieve locale descriptor
		 */
		gnsdk_cstr_t Descriptor(void)
		{
			return m_locale_descriptor;
		}

		/**
		 * Retrieve locale language
		 */
		gnsdk_cstr_t Language(void)
		{
			return m_locale_lang;
		}

		/**
		 * Retrieve locale region
		 */
		gnsdk_cstr_t Region(void)
		{
			return m_locale_region;
		}

	private:
		gnsdk_cstr_t m_locale_group;
		gnsdk_cstr_t m_locale_lang;
		gnsdk_cstr_t m_locale_descriptor;
		gnsdk_cstr_t m_locale_region;

		GnLocaleInfo(gnsdk_cstr_t locale_group, gnsdk_cstr_t language, gnsdk_cstr_t region, gnsdk_cstr_t descriptor) :
			m_locale_group(locale_group), m_locale_lang(language), m_locale_descriptor(descriptor), m_locale_region(region) { }

		friend class GnLocale;
		friend class gn_locale_info_provider;
	};

	class gn_locale_info_provider
	{
	public:
		gn_locale_info_provider() { }
		~gn_locale_info_provider() { }

		GnLocaleInfo
		get_data(gnsdk_uint32_t pos) const throw ( GnError )
		{
			gnsdk_error_t error = GNSDK_SUCCESS;
			gnsdk_cstr_t  type, language, descriptor, region;

			error = gnsdk_manager_locale_available_get(pos, &type, &language, &region, &descriptor);
			if(error) { throw GnError(); }

			return GnLocaleInfo(type, language, region, descriptor);
		}

		gnsdk_uint32_t
		count()
		{
			gnsdk_uint32_t count = 0;

			gnsdk_manager_locale_available_count(&count);
			return count;
		}

		static const gnsdk_uint32_t kOrdinalStart = 1;
		static const gnsdk_uint32_t kCountOffset  = 1;
	};

	typedef gn_facade_range_iterator<GnLocaleInfo, gn_locale_info_provider>   available_locale_iterator;

	/**
	 * Provides core functionality necessary to all Gracenote objects.
	 */
	class GnSDK
	{
	public:
		enum GnLicenseInputMode
		{
			kInvalid = 0,

			kString,
			kFilename,
			kStandardIn
		};

		enum GnUserRegisterMode
		{
			kUserRegModeOnline = 1,
			kUserRegModeLocalOnly
		};

		static void init_gnsdk_module(gnsdk_uint32_t module_id);

		/**
		 * Initializes GNSDK. You must initialize the GNSDK prior to instantiating any other GNSDK objects.
		 * Valid license data must be provided. Gracenote Global Services and Support team provides a license with the SDK.
		 * @param gnSDKLibPath        Path to GNSDK native libraries
		 * @param licenseFile			License file data
		 * @param licenseInputMode	Mode/format of the license file data
		 */
		GnSDK(gnsdk_cstr_t licenseFile, GnLicenseInputMode licenseInputMode) throw ( GnError );

		virtual
		~GnSDK()  throw ( GnError );

		/**
		 * Retrieves the GNSDK version string.
		 * This API can be called at any time after GnSDK instance is constructed successfully. The returned
		 * string is a constant. Do not attempt to modify or delete.
		 * Example: 1.2.3.123 (Major.Minor.Improvement.Build)
		 * Major: New functionality
		 * Minor: New or changed features
		 * Improvement: Improvements and fixes
		 */
		static gnsdk_cstr_t Version();

		/**
		 * Retrieves the product version string.
		 * This API can be called at any time after GnSDK instance is constructed successfully. The returned
		 * string is a constant. Do not attempt to modify or delete.
		 * Example: 1.2.3.123 (Major.Minor.Improvement.Build)
		 * Major: New functionality
		 * Minor: New or changed features
		 * Improvement: Improvements and fixes
		 * Build: Internal build number
		 */
		static gnsdk_cstr_t ProductVersion();

		/**
		 * Retrieves the GNSDK's build date string.
		 * This API can be called at any time after GnSDK instance is constructed successfully. The returned
		 * string is a constant. Do not attempt to modify or delete.
		 * Example: 2008-02-12 00:41 UTC
		 */
		static gnsdk_cstr_t BuildDate();

		/**
		 * Creates a new Serialized User and also increments the user's Client ID user count with Gracenote Service.
		 * Use this constructor to create a new user; when successful, this call registers a new user for
		 * a specified client in Gracenote Service. Once the new user is registered and the user count
		 * incremented in Gracenote Service, the count cannot be reduced, nor can the same user be
		 * re-retrieved.
		 * Newly registered user handles must be serialized and stored locally for that user to be used
		 * again for future queries; failing to do this quickly depletes the client's allotted user quota
		 * within Gracenote Service.
		 * @param client_id     Client ID that initiates requests with this handle; value provided by
		 *                      Gracenote
		 * @param client_tag    Client ID tag value that matches client ID; value provided by Gracenote
		 * @param app_version   Client application version; numeric value provided by application, and
		 *                      this value is required
		 */
		GnString RegisterUser(GnUserRegisterMode registerMode, gnsdk_cstr_t clientId, gnsdk_cstr_t clientTag, gnsdk_cstr_t applicationVersion) throw ( GnError );

		void     SetDefaultLocale(GnLocale& locale);

		/*
		 * Logging package IDs
		 */
		static gnsdk_uint16_t GN_LOG_PKG_ALL;
		static gnsdk_uint16_t GN_LOG_PKG_GCSL;
		static gnsdk_uint16_t GN_LOG_PKG_GNSDK;

		/*
		 * Logging Filter masks
		 */
		static gnsdk_uint32_t GN_LOG_FILTER_ERROR;
		static gnsdk_uint32_t GN_LOG_FILTER_WARNING;
		static gnsdk_uint32_t GN_LOG_FILTER_INFO;
		static gnsdk_uint32_t GN_LOG_FILTER_DEBUG;

		static gnsdk_uint32_t GN_LOG_LEVEL_ERROR;
		static gnsdk_uint32_t GN_LOG_LEVEL_WARNING;
		static gnsdk_uint32_t GN_LOG_LEVEL_INFO;
		static gnsdk_uint32_t GN_LOG_LEVEL_DEBUG;
		static gnsdk_uint32_t GN_LOG_LEVEL_ALL;

		/*
		 * Logging option masks
		 */
		static gnsdk_uint32_t GN_LOG_OPTION_TIMESTAMP;
		static gnsdk_uint32_t GN_LOG_OPTION_CATEGORY;
		static gnsdk_uint32_t GN_LOG_OPTION_PACKAGE;
		static gnsdk_uint32_t GN_LOG_OPTION_THREAD;
		static gnsdk_uint32_t GN_LOG_OPTION_SOURCEINFO;
		static gnsdk_uint32_t GN_LOG_OPTION_NEWLINE;
		static gnsdk_uint32_t GN_LOG_OPTION_NONE;
		static gnsdk_uint32_t GN_LOG_OPTION_ALL;
		static gnsdk_uint32_t GN_LOG_OPTION_SYNCHRONOUS;
		static gnsdk_uint32_t GN_LOG_OPTION_ARCHIVEDAILY;

		/**
		 * Enables or changes logging settings of GNSDK messages for a specific log file.
		 * @param logFilePath		Path and file name to log messages to
		 * @param packageId		ID of package to filter log messages for this log
		 * @param filterMmask		Filter mask for type of message to log
		 * @param optionsMask		Formatting options for this log
		 * @param maxSize			Maximum size of log before new log is created. Enter a value of zero (0) to
		 *                          always create new log on open
		 * @param bArchive         Specify true for the archive to retain and rename old logs, or
		 *                          false to delete old logs
		 * <p><b>Remarks:</b></p>
		 * The GNSDK logging system allows multiple logs to be enabled simultaneously. Each call to
		 * this method with a new log_file_path parameter will enable a new log. Filter
		 * and option settings are set per Package ID. Multiple Package IDs can be enabled per log file
		 * by calling this API multiple times with different Package IDs. The packages, filter and option
		 * parameter will apply only to the log indicated by log_file_path.
		 * If the log_file_path parameter matches an existing enabled log, then the package, filter and
		 * option settings will be applied to the existing log file. These changes can be applied at run time.
		 * Windows CE developers: log_file_path parameter must be absolute path as relative paths are not supported
		 * in Windows CE.
		 */
		void LoggingEnable(gnsdk_cstr_t logFilePath, gnsdk_uint16_t packageId, gnsdk_uint32_t filterMmask, gnsdk_uint32_t optionsMask, gnsdk_uint64_t maxSize, bool bArchive)  throw ( GnError );

		/**
		 * Disables logging of GNSDK messages.
		 * @param logFilePath     Absolute path and file name of log to apply this disable
		 * @param packageId        ID of package to to disable messages for
		 * <p><b>Remarks:</b></p>
		 * This disables logging to the given log file for the given package ID. The file handle for the log
		 * file may not be closed until the GNSDK Manager is shut down.
		 */
		void LoggingDisable(gnsdk_cstr_t logFilePath, gnsdk_uint16_t packageId)  throw ( GnError );

		/**
		 * Enables an application to write to a log for its own messages.
		 * @param line          Source line number of this call
		 * @param fileName      Source file name of this call
		 * @param packageId        Package ID of application making call
		 * @param filterMask       Log level mask for this logging message
		 * @param format            Log message format
		 * <p><b>Remarks:</b></p>
		 * This function enables an application to write their own logging messages to the GNSDK logging
		 * system. If a log or callback is enabled that handles the given package ID and filter mask, then
		 * these message are written to those log(s). The package ID must be one previously registered by the
		 * application.
		 */
		void LoggingWrite(gnsdk_int32_t line, gnsdk_cstr_t fileName, gnsdk_uint16_t packageId, gnsdk_uint32_t filterMask, gnsdk_cstr_t format, ...)  throw ( GnError );

		/*
		 * Storage Names
		 */
		static gnsdk_cstr_t kStorageQueryCache;
		static gnsdk_cstr_t kStorageListsCache;
		static gnsdk_cstr_t kStorageContentCache;

		/**
		 *  This function can be used to specify the location of the named content cache local storage.
		 *
		 *  If this option is not specified the option set for the initialized storage SDK will be used.
		 *  For example, if the SQLite Storage SDK has been initialized any path set using
		 *   <code>GnLookupLocal.StorageLocation(..)</code> will be used.
		 *
		 *  Storage Names
		 *
		 *  Currently there are three storages managed by the SDK:
		 *  <ul>
		 *   <li>GnSDK::kStorageContentCache The content cache, which stores cover art and related information
		 *   <li>GnSDK::kStorageQueryCache he query cache, which stores media identification requests
		 *   <li>GnSDK::kStorageListsCache The list storage, which stores Gracenote display lists
		 *  </ul>
		 *  Use the appropriate name to perform this operation on the target storage. Each of these require a
		 *  separate call to this API, with the name of the storage on which to to specify the location for.
		 *
		 *  The storage location must be a valid relative or absolute path to an existing folder that
		 *  has the necessary write permissions.
		 *
		 *  <p><b>Important:</b></p>
		 *  For Windows CE an absolute path must be used.
		 *  @param name cache storage name
		 *  @param location response cache location
		 */
		void StorageLocation(gnsdk_cstr_t name, gnsdk_cstr_t location) throw ( GnError );

		/**
		 *  This function clears all records from the named local storage.
		 *
		 *  When the b_async parameter is set to value that equals true, then the function immediately
		 *  returns and the flush is performed in the background and on a separate thread.
		 *
		 *  Storage Names
		 *
		 *  Currently there are three storages managed by the SDK:
		 *  <ul>
		 *   <li>GnSDK::kStorageContentCache The content cache, which stores cover art and related information
		 *   <li>GnSDK::kStorageQueryCache he query cache, which stores media identification requests
		 *   <li>GnSDK::kStorageListsCache The list storage, which stores Gracenote display lists
		 *  </ul>
		 *  Use the appropriate name to perform this operation on the target storage. Each of these require a
		 *  separate call to this API, with the name of the storage on which to perform the flush.
		 *
		 *  Performance
		 *
		 *  If <code>b_async</code> is true this API spawns a thread to perform the operation. This thread runs at the
		 *  lowest priority to minimize the impact on other running queries or operations of the SDK.
		 *
		 *  This function can cause performance issues due to creating a large amount of disk I/O.
		 *
		 *  This operation can be performed on different storages at the same time, but note that performing
		 *  multiple simultaneous calls will potentially further degrade performance.
		 *
		 *  @param name  Local storage name
		 *  @param bAsync Boolean value to indicate whether to perform an asynchronous cache flush in the background, on a separate thread
		 */
		void StorageFlush(gnsdk_cstr_t name, bool bAsync) throw ( GnError );

		/**
		 *  Attempts to compact the named local storage to minimize the amount of space required.
		 *
		 *  When the b_async parameter is set to value that equals true, then the function immediately
		 *  returns and the flush is performed in the background and on a separate thread.
		 *
		 *  Storage Names
		 *
		 *  Currently there are three storages managed by the SDK:
		 *  <ul>
		 *   <li>GnSDK::kStorageContentCache The content cache, which stores cover art and related information
		 *   <li>GnSDK::kStorageQueryCache he query cache, which stores media identification requests
		 *   <li>GnSDK::kStorageListsCache The list storage, which stores Gracenote display lists
		 *  </ul>
		 *  Use the appropriate name to perform this operation on the target storage. Each of these require a
		 *  separate call to this API, with the name of the storage on which to perform the clean-up.
		 *
		 *  Performance
		 *
		 *  If <code>b_async</code> is true this API spawns a thread to perform the operation. This thread runs at the
		 *  lowest priority to minimize the impact on other running queries or operations of the SDK.
		 *
		 *  This function can cause performance issues due to creating a large amount of disk I/O.
		 *
		 *  This operation can be performed on different storages at the same time, but note that performing
		 *  multiple simultaneous calls will potentially further degrade performance.
		 *
		 *  @param name Local storage name
		 *  @param bAsync Boolean value to indicate whether to perform an asynchronous cache compact in the background, on a separate thread
		 *
		 *
		 */
		void StorageCompact(gnsdk_cstr_t name, bool bAsync) throw ( GnError );

		/**
		 *  This function searches the existing local SDK cache and removes any records that are expired.
		 *
		 *  Operation
		 *
		 *  This API first decides if maintenance is required; this is determined by tracking when the next
		 *  record expiration is needed.
		 *
		 *  Maintenance is always run the first time this API is called; any subsequent calls only perform
		 *  maintenance if this is required. Consequently, calling this API multiple times does not result in
		 *  excessive maintenance.
		 *
		 *  Storage Names
		 *
		 *  Currently there are three storages managed by the SDK:
		 *  <ul>
		 *   <li>GnSDK::kStorageContentCache The content cache, which stores cover art and related information
		 *   <li>GnSDK::kStorageQueryCache he query cache, which stores media identification requests
		 *   <li>GnSDK::kStorageListsCache The list storage, which stores Gracenote display lists
		 *  </ul>
		 *  Use the appropriate name to perform this operation on the target storage. Each of these require a
		 *  separate call to this API, with the name of the storage to on which to perform the maintenance.
		 *
		 *  Performance
		 *
		 *  If <code>b_async</code> is true and if maintenance is necessary, this API spawns a thread to perform the clean-up.
		 *  This thread runs at the lowest priority to minimize the impact on other running queries or operations of the SDK.
		 *
		 *  This function can cause performance issues due to creating a large amount of disk I/O.
		 *
		 *  This operation can be performed on different storages at the same time, but note that performing
		 *  multiple simultaneous calls will potentially further degrade performance.
		 *
		 *  Expired Records
		 *
		 *  Expired records are those older than the maximum allowable, even if the record has been recently
		 *  read. Old but actively read records are removed because Gracenote Service may have an updated
		 *  matching record.
		 *
		 *  The maximum allowable age of a record varies by query type and is managed internally by
		 *  Gracenote. Applications can use GnUser.OptionCacheExpiration() to adjust the age at which
		 *  records are expired.
		 *
		 *  @param name Local storage name
		 *  @param bAsync Boolean value to indicate whether to perform an asynchronous clean-up maintenance in the background
		 *
		 */
		void StorageCleanup(gnsdk_cstr_t name, bool bAsync) throw ( GnError );

		/*
		 * Package()  -- pav-todo
		 */

		/**
		 * Retrieve iterator for locales available from locale lists cache
		 */
		gn_iterable_container<available_locale_iterator> AvailableLocales()
		{
			gn_locale_info_provider provider;
			gnsdk_uint32_t          count = provider.count() + 1;

			return gn_iterable_container<available_locale_iterator>(available_locale_iterator(provider, 1, count), available_locale_iterator(provider, count, count) );
		}

		/*
		 * Languages used when specifying which locale or list to load and preferring a language for metadata returned in
		 * responses.
		 */
		static gnsdk_cstr_t kLanguageEnglish;
		static gnsdk_cstr_t kLanguageChineseSimplified;
		static gnsdk_cstr_t kLanguageChineseTraditional;
		static gnsdk_cstr_t kLanguageDutch;
		static gnsdk_cstr_t kLanguageFrench;
		static gnsdk_cstr_t kLanguageGerman;
		static gnsdk_cstr_t kLanguageItalian;
		static gnsdk_cstr_t kLanguageJapanese;
		static gnsdk_cstr_t kLanguageKorean;
		static gnsdk_cstr_t kLanguagePortugueseBrazil;
		static gnsdk_cstr_t kLanguageRussian;
		static gnsdk_cstr_t kLanguageSpanish;
		static gnsdk_cstr_t kLanguageSwedish;
		static gnsdk_cstr_t kLanguageThai;
		static gnsdk_cstr_t kLanguageTurkish;
		static gnsdk_cstr_t kLanguagePolish;
		static gnsdk_cstr_t kLanguageFarsi;
		static gnsdk_cstr_t kLanguageVietnamese;
		static gnsdk_cstr_t kLanguageHungarian;
		static gnsdk_cstr_t kLanguageCzech;
		static gnsdk_cstr_t kLanguageSlovak;
		static gnsdk_cstr_t kLanguageRomanian;
		static gnsdk_cstr_t kLanguageGreek;
		static gnsdk_cstr_t kLanguageArabic;
		static gnsdk_cstr_t kLanguageBahasaIndonesia;
		static gnsdk_cstr_t kLanguageFinnish;
		static gnsdk_cstr_t kLanguageNorwegian;
		static gnsdk_cstr_t kLanguageCroatian;
		static gnsdk_cstr_t kLanguageBulgarian;
		static gnsdk_cstr_t kLanguageSerbian;
		static gnsdk_cstr_t kLanguageDanish;

		/*
		 * Regions used when specifying which locale or list is loaded.
		 */
		static gnsdk_cstr_t kRegionDefault;
		static gnsdk_cstr_t kRegionGlobal;
		static gnsdk_cstr_t kRegionUS;
		static gnsdk_cstr_t kRegionJapan;
		static gnsdk_cstr_t kRegionChina;
		static gnsdk_cstr_t kRegionTaiwan;
		static gnsdk_cstr_t kRegionKorea;
		static gnsdk_cstr_t kRegionEurope;
		static gnsdk_cstr_t kRegionLatinAmerica;

		/*
		 * List and locale descriptor (how verbose).
		 */
		static gnsdk_cstr_t kDescriptorDefault;
		static gnsdk_cstr_t kDescriptorSimplified;
		static gnsdk_cstr_t kDescriptorDetailed;

		/*
		 * Locale groups used when specifying which locale to load.
		 */
		static gnsdk_cstr_t kLocaleGroupMusic;
		static gnsdk_cstr_t kLocaleGroupVideo;
		static gnsdk_cstr_t kLocaleGroupPlaylist;
		static gnsdk_cstr_t kLocaleGroupEPG;

		/*
		 * List types. GNSDK uses lists to provide media metadata via response objects. Response metadata depending
		 * on lists is known as list-based metadata. Lists are localized meaning, where applicable, the data they
		 * contain is specific to a region and/or translated into languages supported by Gracenote.
		 * Where a list is not available in the requested language the default language, English, is returned.
		 */

		/**
		 * This list contains languages that are supported by Gracenote, and are typically used to indicate
		 * the original language of an item.
		 */
		static gnsdk_cstr_t kListTypeLanguages;

		/**
		 * The list of supported music genres.
		 * <p><b>Remarks:</b></p>
		 * The genre list contains a hierarchy of genres available from Gracenote strictly for music data.
		 */
		static gnsdk_cstr_t kListTypeGenres;

		/**
		 * The list of supported geographic origins for artists.
		 */
		static gnsdk_cstr_t kListTypeOrigins;

		/**
		 * The list of supported music era categories.
		 */
		static gnsdk_cstr_t kListTypeEras;

		/**
		 * The list of supported artist type categories.
		 */
		static gnsdk_cstr_t kListTypeArtistTypes;

		/**
		 * This list contains role list elements supported Gracenote for album data, such as Vocalist and Bass Guitar.
		 */
		static gnsdk_cstr_t kListTypeRoles;

		/**
		 *  This list contains a hierarchy of genre list elements available from Gracenote, strictly for
		 * video data.
		 */
		static gnsdk_cstr_t kListTypeGenreVideos;

		/**
		 * This list contains movie rating list elements supported by Gracenote.
		 */
		static gnsdk_cstr_t kListTypeRatings;

		/**
		 * This list contains film content rating list elements supported by Gracenote.
		 */
		static gnsdk_cstr_t kListTypeRatingTypes;

		/**
		 * This list contains contributor role list elements available from Gracenote, such as Actor or
		 * Costume Design. These apply to video data.
		 */
		static gnsdk_cstr_t kListTypeContributors;

		/**
		 * The list of supported feature types for video data.
		 */
		static gnsdk_cstr_t kListTypeFeatureTypes;

		/**
		 *  The list of supported video regions.
		 */
		static gnsdk_cstr_t kListTypeVideoRegions;

		/**
		 * The list of supported video types, such as Documentary, Sporting Event, or Motion Picture.
		 */
		static gnsdk_cstr_t kListTypeVideoTypes;

		/**
		 * The list of supported media types for music and video, such as Audio CD, Blu-ray, DVD, or HD DVD.
		 */
		static gnsdk_cstr_t kListTypeMediaTypes;

		/**
		 * The list of supported video serial types, such as Series or Episode.
		 */
		static gnsdk_cstr_t kListTypeVideoSerialTypes;

		/**
		 * The list of supported work types for video data, such as Musical or Image.
		 */
		static gnsdk_cstr_t kListTypeWorkTypes;

		/**
		 * The list of supported media spaces for video data, such as Music, Film, or Stage.
		 */
		static gnsdk_cstr_t kListTypeMediaSpaces;

		/**
		 * The list of supported moods for music data. Moods are categorized into levels, from more general
		 * (Level 1, such as Blue) to more specific (Level 2, such as Gritty/Earthy/Soulful).
		 */
		static gnsdk_cstr_t kListTypeMoods;

		/**
		 * The list of supported tempos for music data; has three levels of granularity.
		 * The tempos are categorized in levels in increasing order of granularity.
		 * Level 1: The meta level, such as Fast Tempo.
		 * Level 2: The sub tempo level, such as Very Fast.
		 * Level 3: The micro level, which may be displayed as a numeric tempo range, such as 240-249, or a
		 * descriptive phrase.
		 */
		static gnsdk_cstr_t kListTypeTempos;

		/**
		 * The list of supported composition forms for classical music.
		 */
		static gnsdk_cstr_t kListTypeCompostionForm;

		/**
		 * The list of supported instrumentation for classical music.
		 */
		static gnsdk_cstr_t kListTypeInstrumentation;

		/**
		 * The list of supported overall story types for video data, such as Love Story.
		 * It includes general theme classifications such as such as Love Story, Family Saga, Road Trip,
		 * and Rags to Riches.
		 */
		static gnsdk_cstr_t kListTypeVideoStoryType;

		/**
		 * The list of supported audience types for video data.
		 * It includes general audience classifications by age, ethnicity, gender, and spiritual beliefs,
		 * such as Kids & Family, African-American, Female, Gay & Lesbian, and Buddhist.
		 */
		static gnsdk_cstr_t kListTypeVideoAudience;

		/**
		 * The list of supported moods for video data, such as Offbeat.
		 * It includes general classifications such as such as Offbeat, Uplifting, Mystical, and Sarcastic.
		 */
		static gnsdk_cstr_t kListTypeVideoMood;

		/**
		 * The list of supported film reputation types for video data, such as Classic.
		 * It includes general classifications such as such as Classic, Chick Flick, and Cult.
		 */
		static gnsdk_cstr_t kListTypeVideoReputation;

		/**
		 * The list of supported scenarios for video data. It
		 * includes general classifications such as such as Action, Comedy, and Drama.
		 */
		static gnsdk_cstr_t kListTypeVideoScenario;

		/**
		 * The language of the list is determined by the language value given to
		 */
		static gnsdk_cstr_t kListTypeVideoSettingEnv;

		/**
		 * The list of supported historical time settings for video data, such as Elizabethan Era,
		 * 1558-1603, or Jazz Age, 1919-1929.
		 */
		static gnsdk_cstr_t kListTypeVideoSettingPeriod;

		/**
		 * The list of supported story concept sources for video data, such as Fairy Tales & Nursery Rhymes.
		 * It includes story source classifications such as Novel, Video Game, and True Story.
		 */
		static gnsdk_cstr_t kListTypeVideoSource;

		/**
		 * The list of supported film style types for video data, such as Film Noir.It
		 * includes general style classifications such as Art House, Film Noir, and Silent.
		 */
		static gnsdk_cstr_t kListTypeVideoStyle;

		/**
		 * The list of supported film topics for video data, such as Racing or Teen Angst. It includes a diverse
		 * range of film topics, such as Politics, Floods, Mercenaries, Surfing, and Adventure. It also includes
		 * some list elements that can be considered sub-topics of a broader topic. For example, the list element Aliens (the broad topic),
		 * and Nice Aliens and Bad Aliens (the more defined topics).
		 */
		static gnsdk_cstr_t kListTypeVideoTopic;

		/**
		 * The list of supported viewing types for EPG data, such as live and rerun.
		 */
		static gnsdk_cstr_t kListTypeEpgViewingTypes;

		/**
		 * The list of supported audio types for EPG data, such as stereo and dolby.
		 */
		static gnsdk_cstr_t kListTypeEpgAudioTypes;

		/**
		 * The list of supported video types for EPG data, such as HDTV and PAL30.
		 */
		static gnsdk_cstr_t kListTypeEpgVideoTypes;

		/**
		 * The list of supported video types for EPG data, such as closed caption.
		 */
		static gnsdk_cstr_t kListTypeEpgCaptionTypes;

		/**
		 * The list of supported categories for IPG data, such as movie and TV series.
		 */
		static gnsdk_cstr_t kListTypeIpgCategoriesL1;

		/**
		 * The list of supported categories for IPG data, such as action and adventure.
		 */
		static gnsdk_cstr_t kListTypeIpgCategoriesL2;

		/**
		 * The list of supported production types for EPG data, such as news and documentary.
		 */
		static gnsdk_cstr_t kListTypeEpgProductionTypes;

		/**
		 * The list of supported device types for EPG data.
		 */
		static gnsdk_cstr_t           kListTypeEpgDeviceTypes;

	private:
		static gnsdk_manager_handle_t mManagerHandle;
		static gnsdk_uint32_t         mModulesInit;

		/* disallow assignment operator */
		DISALLOW_COPY_AND_ASSIGN(GnSDK);
	};
} // namespace gracenote

#if GNSDK_MUSICID
#include "gnsdk_musicid.hpp"
#endif
#if GNSDK_MUSICID_FILE
#include "gnsdk_musicidfile.hpp"
#endif
#if GNSDK_MUSICID_STREAM
#include "gnsdk_musicidstream.hpp"
#endif
#if GNSDK_MUSICID_MATCH
#include "gnsdk_musicidmatch.hpp"
#endif
#if GNSDK_VIDEO
#include "gnsdk_video.hpp"
#endif
#if GNSDK_LINK
#include "gnsdk_link.hpp"
#endif
#if GNSDK_SUBMIT
#include "gnsdk_submit.hpp"
#endif
#if GNSDK_DSP
#include "gnsdk_dsp.hpp"
#endif
#if GNSDK_PLAYLIST
#include "gnsdk_playlist.hpp"
#endif
#if GNSDK_MOODGRID
#include "gnsdk_moodgrid.hpp"
#endif
#if GNSDK_STORAGE_SQLITE
#include "gnsdk_storage_sqlite.hpp"
#endif
#if GNSDK_LOOKUP_LOCAL
#include "gnsdk_lookup_local.hpp"
#endif
#if GNSDK_LOOKUP_FPLOCAL
#include "gnsdk_lookup_fplocal.hpp"
#endif
#if  GNSDK_SUBMIT
#include "gnsdk_submit.hpp"
#endif
#if  GNSDK_ACR
#include "gnsdk_acr.hpp"
#endif
#if  GNSDK_CORRELATES
#include "gnsdk_correlates.hpp"
#endif
#endif /* _GNSDK_HPP_ */
