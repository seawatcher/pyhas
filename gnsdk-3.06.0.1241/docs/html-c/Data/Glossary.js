MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Glossary',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<html xmlns:MadCap=\"http://www.madcapsoftware.com/Schemas/MadCap.xsd\" MadCap:tocPath=\"\" MadCap:InPreviewMode=\"false\" MadCap:PreloadImages=\"false\" MadCap:RuntimeFileType=\"Glossary\" MadCap:TargetType=\"WebHelp2\" lang=\"en-us\" xml:lang=\"en-us\" MadCap:PathToHelpSystem=\"../\" MadCap:HelpSystemFileName=\"index.xml\">' +
	'    <head>' +
	'        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />' +
	'        <link href=\"../Skins/Default/Stylesheets/TextEffects.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <link href=\"../Skins/Default/Stylesheets/Topic.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <title>Glossary</title>' +
	'        <link href=\"../Content/Resources/Stylesheets/BookStyles.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <script src=\"../Resources/Scripts/jquery.min.js\" type=\"text/javascript\">' +
	'        </script>' +
	'        <script src=\"../Resources/Scripts/plugins.min.js\" type=\"text/javascript\">' +
	'        </script>' +
	'        <script src=\"../Resources/Scripts/MadCapAll.js\" type=\"text/javascript\">' +
	'        </script>' +
	'    </head>' +
	'    <body style=\"background-color: #fafafa;\">' +
	'        <ul>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor1\">ASR</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Automated Speech Recognition: Technology that converts spoken sounds to equivalent phonetic transcriptions.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor2\">Child Key</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Used to access other GDOs contained in the current GDO.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor3\">Client ID</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Each  customer receives a unique Client ID string from Gracenote. This string uniquely identifies each application and lets Gracenote deliver the specific features for which the application is licensed. ' +
	'The Client ID string has the following format: 123456-789123456789012312. The first part is a six-digit Client ID, and the second part is a 17-digit Client ID Tag. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor4\">Contributor' +
	'</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A Contributor refers to any person who plays a role in an AV Work. Actors, Directors, Producers, Narrators, and Crew are all consider a Contributor. Popular recurring Characters such as Batman, Harry Potter, or Spider-man are also considered Contributors in Video Explore. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor5\">Credit ' +
	'</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A credit lists the contribution of a person (or occasionally a company, such as a record label) to a recording. Generally, a credit consists of: ' +
	'The name of the person or company.' +
	'The role the person played on the recording (an instrument played, or another role such as composer or producer).' +
	'The tracks affected by the contribution.' +
	'A set of optional notes (such as “plays courtesy of Capitol records”). ' +
	'See Role</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor6\">Editable GDO</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A GDO with editable metadata. Only specific fields are editable to preserve the GDO\'s metadata integrity with Gracenote.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor7\">Features ' +
	'</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Term used to encompass a particular media stream\'s characteristics and attributes; this is metadata and information accessed from processing a media stream. For example, when submitting an Album\'s Track, this includes information such as fingerprint, mood, and tempo metadata (Attributes).</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor8\">Fingerprint</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Fingerprint Types:' +
	'Cantametrix' +
	'Philips' +
	'microFAPI ' +
	'nanoFAPI</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor9\">Full GDO</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A GDO that contains all the information associated with the query match. See Partial GDO.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor10\">GDO</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Gracenote Data Object: Containers most-commonly used to store information returned by a query. GDOs can contain metadata values (names, titles, external IDs, and so on) that can be accessed by an application using Value keys.  GDOs can also contain references to other GDOs, which can be accessed using Child GDO keys.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor11\">Generation Criterion' +
	'</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A selection rule for determining which tracks to add to a playlist.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor12\">Generator ' +
	'</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Short for playlist generator. This is a term used to indicate that an artist has at least one of the extended metadata descriptors populated. The metadata may or may not be complete, and the artist text may or may not be locked or certified. See also: extended metadata, playlist optimized artist.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor13\">Genre</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A categorization of a musical composition characterized by a particular style.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor14\">Link</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A module that allows applications to access and present enriched content related to media that has been identified using  identification features.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor15\">Manual Playlist</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A manual playlist is a playlist that the user has created by manually selecting tracks and a play order. An auto-playlist is a playlist generated automatically by software. This distinction only describes the initial creation of the playlist; once created and saved, all that matters to the end-user is whether the playlist is static (and usually editable) or dynamic (and non-editable). All manual playlists are static; the track contents do not change unless the end-user edits the playlist. An auto-playlist may be static or dynamic. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor16\">Metadata</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Data about data. For example, metadata such as the artist, title, and other information about a piece of digital audio such as a song recording. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor17\">Mood</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Track-level perceptual descriptor of a piece of music, using emotional terminology that a typical listener might use to describe the audio track; includes hierarchical categories of increasing granularity. See Sonic Attributes.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor18\">More Like This </a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A mechanism for quickly generating playlists from a user’s music collection based on their similarity to a designated seed track, album, or artist.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor19\">Multiple Match </a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">During CD recognition, the case where a single TOC from a CD may have more than one exact match in the MDB. This is quite rare, but can happen. For example with CDs that have only one track, it is possible that two of these one-track CDs may have exactly the same length (the exact same number of frames). There is no way to resolve these cases automatically as there is no other information on the CD to distinguish between them. So the user must be presented with a dialog box to allow a choice between the alternatives. See also: frame, GN media database, TOC.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor20\">MusicID</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Enables MusicID recognition for identifying CDs, digital music files and streaming audio and delivers relevant metadata such as track titles, artist names, album names, and genres. Also provides library organization and direct lookup features.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor21\">MusicID-File</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A feature of the MusicID product that identifies digital music files using a combination of waveform analysis technology, text hints and/or text lookups. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor22\">Partial GDO</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A GDO that contains a subset of information for the query match, but enough to perform additional processing.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor23\">Playlist</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A set of tracks from a user’s music collection, generated according to the criteria and limits defined by a playlist generator.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor24\">Popularity</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Popularity is a relative value indicating how often metadata for a track, album, artist and so on is accessed when compared to others of the same type. Gracenote’s statistical information about the popularity of an album or track, based on aggregate (non-user-specific) lookup history maintained by Gracenote servers. Note that there’s a slight difference between track popularity and album popularity statistics. Track popularity information identifies the most popular tracks on an album, based on text lookups. Album popularity identifies the most frequently looked up albums, either locally by the end-user, or globally across all Gracenote users. See Rating and Ranking</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor25\">Ranking </a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">For Playlist, ranking refers to any criteria used to order songs within a playlist. So a playlist definition (playlist generator) may rank songs in terms of rating values, or may rank in terms of some other field, such as last-played date, bit rate, etc. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor26\">Rating</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Rating is a value assigned by a user for the songs in his or her collection. See Popularity and Ranking.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor27\">Role</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">A role is the musical instrument a contributor plays on a recording. Roles can also be more general, such as composer, producer, or engineer. Gracenote has a specific list of supported roles, and these are broken into role categories, such as string instruments, brass instruments. See Credit.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor28\">Seed Track, Disc, Artist, Genre </a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Used by playlist definitions to generate a new playlist of songs that are related in some way, or similar, to a certain artist, album, or track.This is a term used to indicate that an artist has at least one of the extended metadata descriptors populated. The metadata may or may not be complete, and the artist text may or may not be locked or certified. See also: extended metadata, playlist optimized artist, playlist-related terms. </div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor29\">Sonic Attributes</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">The Gracenote Service provides two metadata fields that describe the sonic attributes of an audio track. These fields, mood and tempo, are track-level descriptors that capture the unique characteristics of a specific recording. Mood is a perceptual descriptor of a piece of music, using emotional terminology that a typical listener might use to describe the audio track. Tempo is a description of the overall perceived speed or pace of the music. The Gracenote mood and tempo descriptor systems include hierarchical categories of increasing granularity, from very broad parent categories to more specific child categories.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor30\">Target Field</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">The track attribute used by a generation criterion for selecting tracks to include in a generated playlist.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor31\">Tempo</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Track-level descriptor of the overall perceived speed or pace of the music; includes hierarchical categories of increasing granularity. See Sonic Attributes.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor32\">TOC</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Table of Contents. An area on CDs, DVDs, and Blu-ray discs that describes the unique track layout of the disc.</div>' +
	'            </li>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"1522998350_anchor33\">Value Key</a>' +
	'                </div>' +
	'                <div class=\"GlossaryPageDefinition\">Used to access metadata Values returned in a GDO.</div>' +
	'            </li>' +
	'        </ul>' +
	'        <p>&#160;</p>' +
	'    </body>' +
	'</html>'
);
