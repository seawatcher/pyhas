#include <stdio.h>
#include <stdlib.h>

/* this is a hack to rewrite the output of the mftext program in a way
   that's easier to digest: each note is on a single line.  It's hacked
   to gether from pieces of the main analysis program, so it is not
   pretty.

   Written by Daniel Sleator <sleator@cs.cmu.edu>, Sept, 1996

It converts this:

Note-on 250 60
Note-off 500 60
Note-on 500 62
Note-off 750 62
Note-on 750 64
Note-off 1000 64
Note-on 1000 65
Note-off 1250 65
Note-on 1250 62
Note-off 1500 62

To this:

Note    250    500 60
Note    500    750 62
Note    750   1000 64
Note   1000   1250 65
Note   1250   1500 62

*/

#define MAX_PITCH 200 /* the maximum value of a pitch is MAX_PITCH-1. */
                      /* pitches are also assumed to be non-negative */
typedef int Pitch;

typedef struct note_struct {
  int start;    /* starting time in milliseconds */
  int duration; /* also in milliseconds */
  Pitch pitch;  /* the pitch of this note */

  struct note_struct * next;   /* sometimes we want a list of notes */
} Note;

/* global variables */
extern int N_notes;

/* read-input.c */
Note * build_note_list_from_input(void);

static int line_no;  /* the line number */

typedef struct event_struct {
  /* this structure is just used for reading in the notes */
  int note_on;  /* 1 if this is note on, 0 otherwise */
  Pitch pitch;
  int time;      /* the time of this event */
  int end_time;  /* filled in for events where "note_on" is 1 */
  struct event_struct * next;
} Event;

void free_event_list(Event *e) {
    Event *xe;
    for (; e != NULL; e = xe) {
	xe = e->next;

	free ((char *) e);
    }
}

void free_note_list(Note *e) {
    Note *xe;
    for (; e != NULL; e = xe) {
	xe = e->next;

	free ((char *) e);
    }
}

void test_pitch(Pitch pitch) {
  if (pitch < 0 || pitch > MAX_PITCH-1) {
    fprintf (stderr, "Error (line %d) Found a pitch that is out of range: %d\n", line_no, pitch);
    exit(1);
  }
}

Event * build_event_list_from_input() {
  char line[1000];
  char first_part[100];
  int second_part;
  int third_part;
  int fourth_part;
  int event_time;
  int N_parts;
  int on_event, both_event, off_event;
  int on_time, off_time;
  Pitch pitch;
  Event * event_list, * new_event;
  
  event_time = 0;
  event_list = NULL;

  for (line_no = 1; fgets(line, sizeof(line), stdin) != NULL; line_no++) {
    N_parts = sscanf(line, "%99s %d %d %d", first_part, &second_part, &third_part, &fourth_part);
    if (N_parts < 2) continue;

    if (strcmp(first_part, "BaseUnit") == 0) {
      printf("%s", line);
      continue;
    }

    if (strcmp(first_part, "BeatLevel") == 0) {
      printf("%s", line);
      continue;
    }

    on_event = (strcmp(first_part, "Note-on") == 0);
    both_event = (strcmp(first_part, "Note") == 0);
    off_event = (strcmp(first_part, "Note-off") == 0);

    if (!(on_event || off_event || both_event)) continue;
    if (on_event || off_event) {
      if (N_parts != 3) {
	fprintf(stderr, "Error (line %d) a note needs a time and a pitch\n", line_no);
	exit(1);		
      }
      
      event_time = second_part;
      pitch = third_part;

      test_pitch(pitch);
      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = on_event;
      new_event->time = event_time;
      new_event->next = event_list;
      event_list = new_event;
    } else {
      /* it's a both event */
      if (N_parts != 4) {
	fprintf(stderr, "Error (line %d) a note needs an on-time an off-time and a pitch\n", line_no);
	exit(1);		
      }
      on_time = second_part;
      off_time = third_part;
      pitch = fourth_part;
      
      test_pitch(pitch);      
      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = 1;
      new_event->time = on_time;
      new_event->next = event_list;
      event_list = new_event;

      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = 0;
      new_event->time = off_time;
      new_event->next = event_list;
      event_list = new_event;
    }
  }
  
  return event_list;
}

Note * reverse(Note * a) {
  Note *slx;
  Note *b = NULL;
    
    for (; a != NULL; a = slx) {
	slx = a->next;
	
	a->next = b;
	b = a;
    }
    return b;
}

int comp_event(Event ** a, Event ** b) {
  if ((*a)->time != (*b)->time) return  ((*a)->time - (*b)->time);
  return ((*a)->note_on - (*b)->note_on);
}

/* This function builds a note list from an event list.  This note list
   is sorted by increasing starting time.  And notes are broken up so
   that each pair of notes either cover the identical time interval or
   they cover disjoint intervals. 

   The following fields of each note are filled in:
       start, duration, pitch, next
*/
Note * build_note_list_from_event_list(Event * e) {
  int event_time;
  int i, event_n;
  Note * note_list, * new_note;
  Event *el;
  Event ** etable;
  int N_events;

  for (N_events=0, el = e; el != NULL; el = el->next) N_events++;
  etable = (Event **) malloc (N_events * sizeof (Event *));
  for (i=0, el = e; el != NULL; el = el->next, i++) etable[i] = el;

  qsort(etable, N_events, sizeof (Event *), (int (*)(const void *, const void *))comp_event);

  event_time = 0;
  note_list = NULL;

  for (i=0; i<N_events; i++) {
    el = etable[i];
    /*    printf("event: pitch = %d  note_on = %d  time = %d \n", el->pitch, el->note_on, el->time); */
  }

  for (event_n=0; event_n<N_events; event_n++) {
    el = etable[event_n];
    if (!(el->note_on)) continue;
    for (i=event_n+1; i<N_events; i++) {
      if (etable[i]->pitch == el->pitch) {
	el->end_time = etable[i]->time;
	break;
      }
    }
    if (i == N_events) {
      fprintf(stderr, "Error: No note off found for pitch %d\n", el->pitch);
      exit(1);
    }
  }

  /* now the end times for all the on-events are stored */
  /* now run through them and convert them into notes */

  note_list = NULL;
  for (event_n=0; event_n<N_events; event_n++) {
    el = etable[event_n];
    if (!(el->note_on)) continue;
    new_note = (Note*) malloc(sizeof(Note));
    new_note->pitch = el->pitch;
    new_note->start = el->time;
    new_note->duration = el->end_time - el->time;
    new_note->next = note_list;
    note_list = new_note;
  }

  free(etable);

  return reverse(note_list);
}

void print_note_list(Note *nl) {
  for (; nl != NULL; nl = nl->next) {
    printf("Note %6d %6d %2d\n", nl->start, nl->start + nl->duration, nl->pitch);
  }
}

void main(void) {
  Event * e;
  Note * nl;
  e = build_event_list_from_input();
  nl = build_note_list_from_event_list(e);
  free_event_list(e);
  print_note_list(nl);
  free_note_list(nl);
}
