/*
 * Copyright (c) 2012 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoSeriesLookup.cs
 *  Description:
 *  This sample shows basic use of the FindSeries() call
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */  


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoSeriesLookup
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
        private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class VideoSeriesLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Series Lookup ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoSeriesLookupEvents localeEvents = new VideoSeriesLookupEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    /* Set locale with disired Group, Language, Region and Descriptor
		             * Set the 'locale' to return locale-specifc results values. This examples loads an English locale.
		             */
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DisplaySeriesMetadata(GnVideoSeries series)
        {
            string value = null;
            GnGenre genere = series.Genre;
            Console.WriteLine("Genre: " + genere.Level1);

            value = series.VideoProductionType;
            if (value != null && value != "")
                Console.WriteLine("Production Type: " + value);

            value = series.PlotSynopsis;
            if (value != null && value != "")
                Console.WriteLine("Plot: " + value);

            value = series.Reputation;
            if (value != null && value != "")
                Console.WriteLine("Reputation: " + value);
        }

        private static void DoSeriesLookup(GnUser user)
        {
            string tuiID = "238073738";
            string tuiTag = "AE1B6F5143346B16AAD196EDC3093C6F";

            /* Get Simpson's First Series GDO */
            Console.WriteLine("\n*****Sample Video Series Search: 'Simpsons Series'*****");

            /* Get Product based on TOC */
            using (GnStatusEventsDelegate videoEvents = new VideoSeriesLookupEvents())
            {
                GnVideo video = new GnVideo(user, null /*videoEvents*/);
                GnVideoSeries dataObject = new GnVideoSeries(tuiID, tuiTag);           

                Console.WriteLine("\nPerform the search...");
                GnResponseVideoSeries videoSeriesResponse = video.FindSeries(dataObject);
                if (videoSeriesResponse.Series.count() > 0)
                {
                    /****************************************************************
					*  Needs decision (match resolution) check
					*  We have at least one match - see if resolution is necessary.
					****************************************************************/
                    if (videoSeriesResponse.NeedsDecision)
                    {
                        /**********************************************
						* Resolve match here
						**********************************************/
                    }

                    GnVideoSeriesEnumerable gnSeriesEnumerable = videoSeriesResponse.Series;

                    foreach (GnVideoSeries series in gnSeriesEnumerable)
                    {
                        GnVideoSeries viseoSeries = series;
                        if(!series.FullResult)
                        {
                            GnResponseVideoSeries seriesResponse = video.FindSeries(series);
                            viseoSeries = seriesResponse.Series.at(0).next();
                        }
                        /* Series title */
                        GnTitle seriesTitle = viseoSeries.OfficialTitle;
                        Console.WriteLine("\nTitle: " + seriesTitle.Display);

                        /* Display series metadata */
                        DisplaySeriesMetadata(viseoSeries);
                    }
                }
            }
        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Lookup Series and display */
                        DoSeriesLookup(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }   

    }
}
