/*
 *  GRACENOTE, INC. PROPRIETARY INFORMATION
 *  This software is supplied under the terms of a license agreement or
 *  nondisclosure agreement with Gracenote, Inc. and may not be copied
 *  or disclosed except in accordance with the terms of that agreement.
 *  Copyright(c) 2000-2013. Gracenote, Inc. All Rights Reserved.
 *
 */
/*
 *  Name: VideoProductsLookup
 *  Description:
 *  This sample shows use of the findProducts() API
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license path lib path 
 */
package video_products_lookup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class VideoProductsLookup {
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	/*
	 * GnStatusEventsListener : overrider methods of this class to get delegate
	 * callbacks
	 */
	private static class LookupStatusEvents extends GnStatusEventsListener {
		@Override
		public void statusEvent(GnStatus video_status, long percent_complete,
				long bytes_total_sent, long bytes_total_received) {
			System.out.print("status (");
			switch (video_status) {
				case gnsdk_status_unknown :
					System.out.print("Unknown ");
					break;
				case gnsdk_status_begin :
					System.out.print("Begin ");
					break;
				case gnsdk_status_connecting :
					System.out.print("Connecting ");
					break;
				case gnsdk_status_sending :
					System.out.print("Sending ");
					break;
				case gnsdk_status_receiving :
					System.out.print("Receiving ");
					break;
				case gnsdk_status_disconnected :
					System.out.print("Disconnected ");
					break;
				case gnsdk_status_complete :
					System.out.print("Complete ");
					break;
				default :
					break;
			}
			System.out.println("), % complete (" + percent_complete
					+ "), sent (" + bytes_total_sent + "), received ("
					+ bytes_total_received + ")");
		}

		@Override
		public boolean cancelCheck() {
			return false;
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {

		LookupStatusEvents localeEvents = new LookupStatusEvents();
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupVideo(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(final GnSDK gnsdk, final String clientId,
			final String clientIdTag, final String applicationVersion)
			throws GnException, IOException {

		String serialized = null;

		if (!new File("user.txt").canRead()) {
			System.out
					.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			
			serialized = gnsdk.registerUser(userRegistrationMode, clientId,
					clientIdTag, applicationVersion).toString();

			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));

			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
			try {
				/* read stored user data from file */
				serialized = br.readLine();

			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				br.close();
			}
		}

		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}

	/* Perform a video product lookup */
	private static void doProductsLookup(GnUser user) throws GnException {
		GnVideoProduct gnVideoProduct = null;
		/* Get Product based on TOC */
		String toc = "1:15;2:198 15;3:830 7241 6099 3596 9790 3605 2905 2060 10890 3026 6600 2214 5825 6741 3126 6914 1090 2490 3492 6515 6740 4006 6435 3690 1891 2244 5881 1435 7975 4020 4522 2179 3370 2111 7630 2564 8910 15;4:830 7241 6099 3596 9790 3605 2905 2060 10890 3026 6600 2214 5825 6741 3126 6914 1090 2490 3492 6515 6740 4006 6435 3690 1891 2244 5881 1435 7975 4020 4522 2179 3370 2111 7630 2564 8910 15;5:8962 15;6:11474 15;7:11538 15;";
		LookupStatusEvents midEvents = new LookupStatusEvents();
		/* Create a new video query handle */
		GnVideo video = new GnVideo(user, null);/*
												 * midEvents can be used to view
												 * the progress
												 */
		/* / Get Product based on TOC */
		GnResponseVideoProduct response = video.findProducts(toc,
				GnVideoTOCFlag.kTOCFlagDefault);
		GnVideoProductIterable gnVideoProductIterable = response.products();
		GnVideoProductIterator gnVideoProductIterator = gnVideoProductIterable
				.getIterator();
		while (gnVideoProductIterator.hasNext()) {
			gnVideoProduct = (GnVideoProduct) gnVideoProductIterator.next();
			/* Product title */
			GnTitle productTitle = gnVideoProduct.officialTitle();
			System.out.println("\nTitle: " + productTitle.display());
			/* Product aspect ratio */
			System.out.println("Aspect ratio: " + gnVideoProduct.aspectRatio());
			/* Video production type */
			System.out.println("Production Type: "
					+ gnVideoProduct.videoProductionType());
			/* Package Language */
			System.out.println("Package language: "
					+ gnVideoProduct.packageLanguageDisplay());
			/* Rating */
			GnRating rating = gnVideoProduct.rating();
			System.out.println("Rating: " + rating.rating());
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				loadLocale(gnsdk, user);
			doProductsLookup(user);
			} catch (GnException gnException) {
				System.out.println("\nError Code: " + gnException.getErrorCode()
						+ "\nError Message: " + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
