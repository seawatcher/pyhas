var classgracenote_1_1metadata_1_1_gn_name =
[
    [ "GnName", "classgracenote_1_1metadata_1_1_gn_name.html#a39c6686762ff4f272b9f566f41a7522e", null ],
    [ "GnName", "classgracenote_1_1metadata_1_1_gn_name.html#a65775e0944cb93c7d23077aae65f7e13", null ],
    [ "~GnName", "classgracenote_1_1metadata_1_1_gn_name.html#a1393c7f0653e7e91e68a2aba9b9d5227", null ],
    [ "Display", "classgracenote_1_1metadata_1_1_gn_name.html#a1c8458226012b2ae44f8d3db344d1a30", null ],
    [ "Family", "classgracenote_1_1metadata_1_1_gn_name.html#a9ee0eaf4d8c247c5ccf34106c798a3d9", null ],
    [ "Given", "classgracenote_1_1metadata_1_1_gn_name.html#a886bce42f5d538d4401c0ba5476f70d4", null ],
    [ "GlobalId", "classgracenote_1_1metadata_1_1_gn_name.html#a83b51ee47066dcf2a3ffda44d1e979aa", null ],
    [ "Language", "classgracenote_1_1metadata_1_1_gn_name.html#ad2f53fbd2c37085ab1dd4fa9575e106e", null ],
    [ "Prefix", "classgracenote_1_1metadata_1_1_gn_name.html#ad4558c19dc268ea1dfe5b04923de67f0", null ],
    [ "Sortable", "classgracenote_1_1metadata_1_1_gn_name.html#a3506d7fbe3bd54f2a7e7a5c382d3de44", null ],
    [ "SortableScheme", "classgracenote_1_1metadata_1_1_gn_name.html#aa36a49bd99ac58acc49a6b5bcba232ad", null ]
];