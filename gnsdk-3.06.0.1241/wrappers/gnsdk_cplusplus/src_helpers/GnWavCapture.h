#pragma once
#include "../src_wrapper/gn_audiosource.hpp"

#include <stdio.h>

namespace gracenote
{
	class GnWavCapture : public gracenote::GnAudioSource
	{
	public:
		GnWavCapture(GnAudioSource& audioSource, gnsdk_cstr_t output_filename);
		virtual ~GnWavCapture();

		/*
		** GnAudioSource Implementation
		*/
		virtual gnsdk_uint32_t	SourceInit();
		virtual gnsdk_void_t	SourceClose();

		virtual gnsdk_uint32_t	SamplesPerSecond();
		virtual gnsdk_uint32_t	SampleSizeInBits();
		virtual gnsdk_uint32_t	NumberOfChannels();

		virtual gnsdk_size_t	GetData(gnsdk_byte_t* audio_buffer, gnsdk_size_t buffer_size);

	private:
		gnsdk_char_t			m_filename[300];
		FILE*					m_file;

		gnsdk_size_t			m_data_size;
		long					m_data_pos;
		long					m_total_pos;

		GnAudioSource*			m_p_audioSource;
	};
}