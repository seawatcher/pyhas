/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoProductsLookup.cs
 *  Description:
 *  This sample shows use of the FindProducts() API
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoProductsLookup
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoProductLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Find Product Query ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoProductLookupEvents localeEvents = new VideoProductLookupEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLoclaeString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLoclaeString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion
        
        private static void DoProductsLookup(GnUser user)
        {
            string toc = "1:15;2:198 15;3:830 7241 6099 3596 9790 3605 2905 2060 10890 3026 6600 2214 5825 6741 3126 6914 1090 2490 3492 6515 6740 4006 6435 3690 1891 2244 5881 1435 7975 4020 4522 2179 3370 2111 7630 2564 8910 15;4:830 7241 6099 3596 9790 3605 2905 2060 10890 3026 6600 2214 5825 6741 3126 6914 1090 2490 3492 6515 6740 4006 6435 3690 1891 2244 5881 1435 7975 4020 4522 2179 3370 2111 7630 2564 8910 15;5:8962 15;6:11474 15;7:11538 15;";

            using (GnStatusEventsDelegate videoEvents = new VideoProductLookupEvents())
            {

                /* Create a new video query handle */
                GnVideo video = new GnVideo(user, null /*videoEvents*/);

                /* Get Product based on TOC */
                GnResponseVideoProduct response = video.FindProducts(toc, GnVideoTOCFlag.kTOCFlagDefault);

                //Console.WriteLine("\nProduct Match Count: " + response.Products.count());

                GnVideoProductEnumerable videoProductIEnumerable = response.Products;

                foreach (GnVideoProduct videoProduct in videoProductIEnumerable)
                {
                    /* Product title */
                    GnTitle productTitle = videoProduct.OfficialTitle;
                    Console.WriteLine("\nTitle: " + productTitle.Display);

                    /* Product aspect ratio */
                    Console.WriteLine("Aspect ratio: " + videoProduct.AspectRatio);

                    /* Video production type */
                    Console.WriteLine("Production Type: " + videoProduct.VideoProductionType);

                    /* Package Language */
                    Console.WriteLine("Package language: " + videoProduct.PackageLanguageDisplay);

                    /* Rating */
                    GnRating rating = videoProduct.Rating;
                    Console.WriteLine("Rating: " + rating.Rating);
                }

            }


        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Lookup products and display */
                        DoProductsLookup(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }
    }
}
