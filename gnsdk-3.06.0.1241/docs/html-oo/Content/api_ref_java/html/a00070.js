var a00070 =
[
    [ "GnMatchIterable", "a00070.html#a03fb544c8b3109da07451ce0b924882b", null ],
    [ "GnMatchIterable", "a00070.html#a95b87cb5afe52e0c898b430b432fe753", null ],
    [ "at", "a00070.html#adab8ebe5ad6514a906c3848289ea547b", null ],
    [ "count", "a00070.html#a8eeb7e357b30f1b9326614f81b03c9b8", null ],
    [ "delete", "a00070.html#a8da58b45438d3a72fea937e928a0e75f", null ],
    [ "end", "a00070.html#a42a73e4542f5ba5fb00a60debde22f28", null ],
    [ "finalize", "a00070.html#abeb87d6e34bea9a6ad1485fdc2c1e67e", null ],
    [ "getByOrdinal", "a00070.html#a730d6167683b0206b692ec403addc0e9", null ],
    [ "getIterator", "a00070.html#ab7ee447469378e81178ab20ae20493d1", null ],
    [ "swigCMemOwn", "a00070.html#a22ba27ff0f4bf5221249d1ab6c50884d", null ]
];