# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Name: moodgrid
Description:
  Moodgrid is the GNSDK library that generates playlists based on Mood by simply 
  accessing parts of the Grid. moodgrid APIs enable an application to:
    01. Discover and enumerate all datasources available for Moodgrid (from both 
        offline and online sources )
    02. Create and administer data representation of Moodgrid for different types 
        of datasources and grid types.
    03. Query a grid cell for a playlist based on the Mood it represents.
    04. Filter the Moodgrid on Music Genre and Artist Origin, Era and Type.
    05. Manage results.
  Datasources : Offline datasources for Moodgrid can be created by using Gnsdk 
                Playlist. This sample demonstrates this.

Command line syntax:
> python main.py <client_id> <client_id_tag> <license_filename>

"""

import os
import sys
sys.path.append("..")

# gnsdk_utilities finds gnsdk and sets up sys.path. Import first.
import gnsdk_utilities
import gnsdk

application_version = "1.0.0.0"

def get_build_info(name, gnsdk_lib):
    return name.ljust(20, '.') + \
        " version: " + gnsdk_lib.version() + \
        " builddate: " + gnsdk_lib.build_date()

class GNSDK_Manager:

    def __init__(self, client_id, client_id_tag, license_path):
        try:
            self.__manager = gnsdk.GnSDK(
                gnsdk_utilities.lib_path,
                license_path,
                gnsdk.GnSDK.kFilename
            )
        except gnsdk.GnError as e:
            print "Error creating GnSDK instance: %s" % e.error_description()
            exit(1)

        if configuration.enable_logging:
            self.__manager.logging_enable(
                "sample.log",
                self.__manager.gn_log_pkg_all,
                self.__manager.gn_log_level_error|self.__manager.gn_log_level_warning,
                self.__manager.gn_log_option_all,
                0,                                                         # Max size
                False                                                      # save logs
            )

        try:
            self.__storage = gnsdk.GnStorageSqlite()
        except gnsdk.GnError as e:
            print "Error creating GnStorageSqlite instance: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            self.__storage.storage_folder_set(configuration.storage_path)
            try:
                self.lookup_local = gnsdk.GnLookupLocal()
            except gnsdk.GnError as e:
                print "Error creating GnLookupLocal instance: %s" % e.error_description()
                exit(1)

        # Get the user, registering if necessary.
        self.user = self.get_user(client_id, client_id_tag, application_version)

        # Set the locale.
        try:
            locale = gnsdk.GnLocale(
                self.__manager.k_locale_group_playlist,
                self.__manager.k_language_english,
                self.__manager.k_region_default,
                self.__manager.k_descriptor_simplified,
                self.user
            )
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

        try:
            self.__manager.set_default_locale(locale)
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

    def get_user(self, client_id, client_id_tag, application_version):
        user_filename = "user.txt"
        try:
            user_file = open(user_filename, "r")
        except:
            print "\n%s not found: Welcome new user!\n" % user_filename
            if configuration.use_local:
                reg_mode = self.__manager.kUserRegModeLocalOnly
            else:
                reg_mode = self.__manager.kUserRegModeOnline
            try:
                serialized_user = self.__manager.register_user(
                    reg_mode,
                    client_id,
                    client_id_tag,
                    application_version
                ).c_str()
            except gnsdk.GnError as e:
                print "Error creating user: %s" % e.error_description()
                exit(1)
            open(user_filename, "w").write(serialized_user)
        else:
            print "\nLoading user from %s: Welcome back!\n" % user_filename
            serialized_user = user_file.read()
        try:
            user = gnsdk.GnUser(
                serialized_user,
                client_id,
                client_id_tag,
                application_version
            )
        except gnsdk.GnError as e:
            print "Error creating user: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            user.option_lookup_mode(gnsdk.kLookupModeLocal)

        return user

    def display_build_info(self):
        # Print all of the version information.
        print get_build_info("manager", self.__manager)
        print get_build_info("storage SQLite", self.__storage)
        if configuration.use_local:
            print get_build_info("lookup_local", self.lookup_local)
            print "gdb version: %s" % self.get_gdb_version()

    def get_gdb_version(self):
        record_count = self.lookup_local.storage_info_count(
            gnsdk.kMetadata,
            gnsdk.kGDBVersion
        )
        for i in xrange(record_count):
            version = self.lookup_local.storage_info(
                gnsdk.kMetadata,
                gnsdk.kGDBVersion,
                i+1
            )
        return version

if __name__ == "__main__":
    def usage(program):
        print "USAGE!!:\n\t%s <client_id> <client_id_tag> <license_path>\n" % program
        exit(1)

    if len(sys.argv) != 4:
        usage(sys.argv[0])

    # configuration instance
    configuration = gnsdk_utilities.GNSDK_Configuration()
    print "\nConfiguration:\n" + str(configuration)

    # Create the manager
    manager = GNSDK_Manager(sys.argv[1], sys.argv[2], sys.argv[3])
    manager.display_build_info()

    print "\n*****Mood Grid Sample*****\n"

    def get_mood_grid_provider(mood_grid):
        provider_iter = gnsdk_utilities.GnSDK_Iterable(mood_grid.providers())
        if len(provider_iter):
            print "Returning first mood grid provider available"
            return provider_iter[0]
        else:
            print "No mood grid providers available."
            return None

    def display_mood_grid(user, provider, presentation_type):
        print "\nMood Grid results"
        presentation = mood_grid.create_presentation(user, presentation_type)
        moods = gnsdk_utilities.GnSDK_Iterable(presentation.moods())
        for mood in moods:
            results = gnsdk_utilities.GnSDK_Iterable(
                presentation.find_recommendations(provider, mood).identifiers()
            )
            print "  %2d tracks with mood %s (%2d, %2d) (ID = %s)" % (
                len(results),
                presentation.mood_name(mood).ljust(27, '.'),
                mood.x,
                mood.y,
                presentation.mood_id(mood)
            )

    collection_summary_name = "sample_collection"

    # Create a playlist instance and load a pre-prepared collection summary which
    # will be populated into the mood grid.
    playlist = gnsdk.GnPlaylist()
    print get_build_info("playlist", playlist)
    playlist.set_storage_location(os.getcwd())
    playlist_collection = playlist.load_collection(collection_summary_name)
    print "Loaded Collection '%s' from storage" % collection_summary_name

    # Create a mood grid instance
    mood_grid = gnsdk.GnMoodgrid()
    print get_build_info("mood grid", mood_grid)

    # Get the provider and populate the mood grid with the loaded collection.
    provider = get_mood_grid_provider(mood_grid)
    if provider:
        needs_network = { True:"requires", False:"does not require" }
        print "Mood Grid Provider '%s' is of type '%s' and %s network access." % (
            provider.name(),
            provider.type(),
            needs_network[provider.requires_network()]
        )
        display_mood_grid(manager.user, provider, gnsdk.kMoodgridPresentationType5x5)
        display_mood_grid(manager.user, provider, gnsdk.kMoodgridPresentationType10x10)
