/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_MOODGRID_HPP_
#define _GNSDK_MOODGRID_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "metadata_music.hpp"

#if GNSDK_MOODGRID

namespace gracenote
{
	namespace moodgrid
	{
		/**
		** GnMoodgridDataPoint
		*/
		struct GnMoodgridDataPoint
		{
			gnsdk_uint32_t X;
			gnsdk_uint32_t Y;
		};

		/**
		** GnMoodgridPresentationType
		*/
		enum GnMoodgridPresentationType
		{
			kMoodgridPresentationType5x5   = 0x0505,
			kMoodgridPresentationType10x10 = 0x0A0A
		};

		/**
		** GnMoodgridIdentifier
		*/
		class GnMoodgridIdentifier 
		{
		public:
			/**
			 * Default Constructor
			 */
			GnMoodgridIdentifier() : ident_(GNSDK_NULL), group_(GNSDK_NULL) { }

			/**
			 * Destructor
			 */
			virtual ~GnMoodgridIdentifier() { }

			/**
			 * Retrieves a read only string that is the media identifier.
			 */
			gnsdk_cstr_t MediaIdentifier() const { return ident_; }

			/**
			 * Retrieves a read only string that is the group the MediaIdentifier belongs too.
			 *  E.g. in the case of a Playlist provider , the group represents the name of the collection.
			 */
			gnsdk_cstr_t  Group() const                              { return group_; }

			bool operator == (const GnMoodgridIdentifier& rhs) const { return ( ident_ == rhs.ident_ ) && ( group_ == rhs.group_ ); }

		protected:
			friend class moodgrid_result_provider;
			GnMoodgridIdentifier(gnsdk_cstr_t media_identifier, gnsdk_cstr_t group) : ident_(media_identifier), group_(group) { }

		private:
			gnsdk_cstr_t ident_;
			gnsdk_cstr_t group_;
		};

		/// Internal moodgrid result provider class
		class moodgrid_result_provider
		{
		public:
			moodgrid_result_provider(gnsdk_moodgrid_result_handle_t results) : resultHandle_(results) { }
			~moodgrid_result_provider() { }

			GnMoodgridIdentifier           get_data(gnsdk_uint32_t pos);

			// optional
			gnsdk_uint32_t                 count();

		private:
			gnsdk_moodgrid_result_handle_t resultHandle_;
		};

		/**
		** GnMoodgridResult
		*/
		class GnMoodgridResult : public GnObject
		{
		public:
			typedef utility::gnsdk_proxy<gnsdk_moodgrid_result_handle_t>                       result_proxy_handle;
			typedef gn_facade_range_iterator<GnMoodgridIdentifier, moodgrid_result_provider>   iterator;

			GnMoodgridResult() { }

			/**
			 *  GnMoodgridResult Constructor.
			 *  @param handle : create a new GnMoodgridResult object that takes ownership of the native handle.
			 */
			explicit GnMoodgridResult(gnsdk_moodgrid_result_handle_t handle) : GnObject(handle) { }

			/**
			 *  Returns the count of the GnMoodgridIdentifiers in this result.
			 */
			gnsdk_uint32_t Count() const throw ( GnError );

			/**
			 * Retrieves an iterator to access the GnMoodgridIdentifiers in this result.
			 */
			gn_iterable_container<iterator> Identifiers();

		protected:
			friend class GnMoodgridPresentation;
		};

		/**
		** GnMoodgridProvider
		*/
		class GnMoodgridProvider : public GnObject
		{
		public:
			GnMoodgridProvider()  { }
			explicit GnMoodgridProvider(gnsdk_moodgrid_provider_handle_t handle) : GnObject(handle) { }

			gnsdk_cstr_t Name() throw ( GnError );

			gnsdk_cstr_t Type() throw ( GnError );

			bool         RequiresNetwork() throw ( GnError );

		private:
			friend class GnMoodgridPresentation;
		};

		/**
		** Internal class  moodgrid_provider
		*/
		class moodgrid_provider
		{
		public:
			// Required.
			GnMoodgridProvider get_data(gnsdk_uint32_t pos);

			// optional
			gnsdk_uint32_t     count() const;
		};

		/**
		** Internal class  presentation_data_provider
		*/
		class presentation_data_provider
		{
		public:
			presentation_data_provider() : type_(kMoodgridPresentationType5x5) { }
			presentation_data_provider(GnMoodgridPresentationType type) : type_(type) { }

			GnMoodgridDataPoint        get_data(gnsdk_uint32_t pos);

			// optional
			gnsdk_uint32_t             count() const;

		private:
			GnMoodgridPresentationType type_;
		};

		/**
		** GnMoodgridPresentation
		*/
		class GnMoodgridPresentation : public GnObject
		{
		public:
			enum FilterConditionType
			{
				kConditionTypeInclude = 1,
				kConditionTypeExclude
			};

			enum FilterListType
			{
				kListTypeGenre = 1,
				kListTypeOrigins,
				kListTypeEras
			};

			typedef gn_facade_range_iterator<GnMoodgridDataPoint, presentation_data_provider>   data_iterator;

			GnMoodgridPresentation() : GnObject(GNSDK_NULL) { }
			GnMoodgridPresentation(gnsdk_moodgrid_presentation_handle_t handle) : GnObject(handle) { }

			gn_iterable_container<data_iterator> Moods();

			GnMoodgridPresentationType           LayoutType() throw ( GnError );

			void                                 AddFilter(gnsdk_cstr_t uniqueIndentfier, FilterListType elistType, gnsdk_cstr_t strValueId, FilterConditionType eConditionType) throw ( GnError );
			void                                 RemoveFilter(gnsdk_cstr_t uniqueIndentfier) throw ( GnError );
			void                                 RemoveAllFilters () throw ( GnError );

			gnsdk_cstr_t                         MoodName(GnMoodgridDataPoint& position) throw ( GnError );

			gnsdk_cstr_t                         MoodId(GnMoodgridDataPoint& position) throw ( GnError );

			GnMoodgridResult                     FindRecommendations(GnMoodgridProvider& provider, GnMoodgridDataPoint& position) throw ( GnError );

			gnsdk_uint32_t                       FindRecommendationsEstimate(GnMoodgridProvider& provider, GnMoodgridDataPoint& position) throw ( GnError );

		protected:
			friend class GnMoodgrid;
		};

		/**
		 * GnMoodgrid
		 */
		class GnMoodgrid : public GnObject
		{
		public:
			typedef gn_facade_range_iterator<GnMoodgridProvider, moodgrid_provider>   provider_iterator;
			GnMoodgrid() { GnSDK::init_gnsdk_module(GNSDK_COMPONENT_MOODGRID); }

			// Version
			static gnsdk_cstr_t                      Version() { return gnsdk_moodgrid_get_version(); }

			// Build Date
			static gnsdk_cstr_t                      BuildDate() { return gnsdk_moodgrid_get_build_date(); }

			// Enumeration of Providers
			gn_iterable_container<provider_iterator> Providers();

			GnMoodgridPresentation                   CreatePresentation(GnUser& user, GnMoodgridPresentationType type) throw ( GnError );

			// Creation of Presentation

		private:
			DISALLOW_COPY_AND_ASSIGN(GnMoodgrid);
		};
	} // namespace Playlist
}     // namespace GracenoteSDK
#endif /* GNSDK_MOODGRID */
#endif // _GNSDK_MOODGRID_HPP_
