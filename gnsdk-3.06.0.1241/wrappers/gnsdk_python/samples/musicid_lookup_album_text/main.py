# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Name: musicid_lookup_album_text
Description:
  Looks up an Album based on its Text and displays some metadata.
  Supports local and online lookup modes.

Command line syntax:
> python main.py <client_id> <client_id_tag> <license_filename>

"""

import os
import sys
sys.path.append("..")

# gnsdk_utilities finds gnsdk and sets up sys.path. Import first.
import gnsdk_utilities
import gnsdk

application_version = "1.0.0.0"

def get_build_info(name, gnsdk_lib):
    return name + \
        " Version    : " + gnsdk_lib.version() + \
        " \t(built " + gnsdk_lib.build_date() + ")"

class GNSDK_Manager:

    def __init__(self, client_id, client_id_tag, license_path):
        try:
            self.__manager = gnsdk.GnSDK(
                gnsdk_utilities.lib_path,
                license_path,
                gnsdk.GnSDK.kFilename
            )
        except gnsdk.GnError as e:
            print "Error creating GnSDK instance: %s" % e.error_description()
            exit(1)
            
        if configuration.enable_logging:
            self.__manager.logging_enable(
                "sample.log",
                self.__manager.gn_log_pkg_all,
                self.__manager.gn_log_level_error|self.__manager.gn_log_level_warning,
                self.__manager.gn_log_option_all,
                0,                                                         # Max size
                False                                                      # save logs
            )
            
        try:
            self.__storage = gnsdk.GnStorageSqlite()
        except gnsdk.GnError as e:
            print "Error creating GnStorageSqlite instance: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            self.__storage.storage_folder_set(configuration.storage_path)
            try:
                self.lookup_local = gnsdk.GnLookupLocal()
            except gnsdk.GnError as e:
                print "Error creating GnLookupLocal instance: %s" % e.error_description()
                exit(1)

        self.display_build_info()

        # Get the user, registering if necessary.
        self.user = self.get_user(client_id, client_id_tag, application_version)

        # Set the locale.
        try:
            locale = gnsdk.GnLocale(
                self.__manager.k_locale_group_music,
                self.__manager.k_language_english,
                self.__manager.k_region_default,
                self.__manager.k_descriptor_simplified,
                self.user
            )
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

        try:
            self.__manager.set_default_locale(locale)
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

    def get_user(self, client_id, client_id_tag, application_version):
        user_filename = "user.txt"
        try:
            user_file = open(user_filename, "r")
        except:
            print "Info: No stored user - this must be the app's first run."
            if configuration.use_local:
                reg_mode = self.__manager.kUserRegModeLocalOnly
            else:
                reg_mode = self.__manager.kUserRegModeOnline
            try:
                serialized_user = self.__manager.register_user(
                    reg_mode,
                    client_id,
                    client_id_tag,
                    application_version
                ).c_str()
            except gnsdk.GnError as e:
                print "Error creating user: %s" % e.error_description()
                exit(1)
            open(user_filename, "w").write(serialized_user)
        else:
            serialized_user = user_file.read()
        try:
            user = gnsdk.GnUser(
                serialized_user,
                client_id,
                client_id_tag,
                application_version
            )
        except gnsdk.GnError as e:
            print "Error creating user: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            user.option_lookup_mode(gnsdk.kLookupModeLocal)

        return user

    def display_build_info(self):
        # Print all of the version information.
        print get_build_info("\nGNSDK Product", self.__manager)
        
        if configuration.use_local:
            print get_build_info("lookup_local", self.lookup_local)
            print "gdb version: %s" % self.get_gdb_version()

    def get_gdb_version(self):
        record_count = self.lookup_local.storage_info_count(
            gnsdk.kMetadata,
            gnsdk.kGDBVersion
        )
        for i in xrange(record_count):
            version = self.lookup_local.storage_info(
                gnsdk.kMetadata,
                gnsdk.kGDBVersion,
                i+1
            )
        return version

if __name__ == "__main__":
    def usage(program):
        print "USAGE!!:\n\t%s <client_id> <client_id_tag> <license_path>\n" % program
        exit(1)

    if len(sys.argv) != 4:
        usage(sys.argv[0])

    # configuration instance
    configuration = gnsdk_utilities.GNSDK_Configuration()

    # Create the manager
    manager = GNSDK_Manager(sys.argv[1], sys.argv[2], sys.argv[3])

    print "\n*****Sample Album Search*****"    

    def display_album_titles(albums):
        for album_index, album in enumerate(albums):
            print "          Title: %s" % (album.title().display())

    def do_match_selection(albums):
        display_album_titles(albums)
        # Here you could prompt the user to selection. We just hard-code 1st result.
        return 1

    def best_album(albumTitle, trackTitle, artistName):

        album = None
        response = music_id.find_albums(albumTitle, trackTitle, artistName)

        if type(response) is gnsdk.GnResponseAlbums:
            # albums does not have an __iter__() method. Wrap it to support iteration.
            albums = gnsdk_utilities.GDO_Iterable(response.albums())
            if len(albums) == 0:
                print "No albums found for the input\n"
            else:
                print "    Match count: %d" % len(albums)
                if response.needs_decision() or len(albums) > 1:
                    choice_ordinal = do_match_selection(albums)
                else:
                    choice_ordinal = 1

                album = albums[choice_ordinal - 1]

        else:
            print "Error: invalid response\n"

        return album

    def display_album_metadata(album):
        print "          Title: %s" % album.title().display()

    # Initialize musicid.
    music_id = gnsdk.GnMusicID(manager.user)
    # Set options here, e.g. to request the single best result uncomment the next line
    # music_id.option_result_single(True)

    album = best_album("Supernatural", "Africa Bamba", "Santana")
    if album:
        print "    Final album:"
        if not album.full_result():
            followUpResponse = music_id.find_albums(album)
            if type(followUpResponse) is gnsdk.GnResponseAlbums:
                finalAlbums = followUpResponse.albums().at(0).next()
            display_album_metadata(finalAlbums)
        else:
            display_album_metadata(album)
