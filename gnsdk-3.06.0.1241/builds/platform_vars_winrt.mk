#
# GNSDK Build System: Platform Variable Setup
#

#
# TARGET PLATFORM: Windows RT 32/64-bit
#


ifneq ($(GNSDK_BUILD_PLATFORM),windows)
$(error Building MSWindows target is currently only supported on Windows clients)
endif


SHARED_LIB_EXT = dll
STATIC_LIB_EXT = lib
SHARED_LINK_EXT = $(STATIC_LIB_EXT)
STATIC_LINK_EXT = $(STATIC_LIB_EXT)

APP_EXE_PATTERN := %.exe
SHARED_LIB_PATTERN := group_library.$(SHARED_LIB_EXT)
STATIC_LIB_PATTERN := group_library.$(STATIC_LIB_EXT)
SHARED_LINK_PATTERN := group_library.$(STATIC_LIB_EXT)
STATIC_LINK_PATTERN := group_library.$(STATIC_LIB_EXT)

SHARED_LIB_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(SHARED_LIB_PATTERN)))
STATIC_LIB_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(STATIC_LIB_PATTERN)))
SHARED_LINK_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(SHARED_LINK_PATTERN)))
STATIC_LINK_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(STATIC_LINK_PATTERN)))


#
# Tool Chain variables
#

TOOLDIR=

# Compilation 
CCPP=$(CC)
COUTFLAG=-Fo
CWARNERR=-WX
PDB_NAME=$(group_name)_$(library_name)
CDEFS = -DUNICODE -D_UNICODE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -DWINAPI_FAMILY=WINAPI_PARTITION_APP -DGCSL_STRICT_HANDLES -DGNSDK_STRICT_HANDLES


ifneq ($(filter thread time process socket random crypt,$(library_name)),)
	CFLAGS_DEBUG=-Od -MDd -J -W4 -GS -GF -RTC1 -nologo -Zi -ZW -TP -EHsc -Fd$(PDB_NAME) -D_DEBUG $(CDEFS) -AI "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcpackages" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata\EN" -FU"Platform.winmd" -FU"Windows.winmd"
	CFLAGS_RELEASE=-O2 -MD -J -W4 -GS -GF -Zi -ZW -TP -EHsc -Fd$(PDB_NAME) -DNDEBUG $(CDEFS) -AI "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcpackages" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata\EN" -FU"Platform.winmd" -FU"Windows.winmd"
else
	CFLAGS_DEBUG=-Od -MDd -J -W4 -GS -GF -RTC1 -nologo -Zi -EHsc -Fd$(PDB_NAME) -D_DEBUG $(CDEFS) -AI "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcpackages" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata\EN" -FU"Platform.winmd" -FU"Windows.winmd"
	CFLAGS_RELEASE=-O2 -MD -J -W4 -GS -GF -Zi -EHsc -Fd$(PDB_NAME) -DNDEBUG $(CDEFS) -AI "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcpackages" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata" -AI "C:\Program Files (x86)\Windows Kits\8.0\Windows Metadata\EN" -FU"Platform.winmd" -FU"Windows.winmd"
endif

# dynamic library building
ifdef NMCL
	ifdef no_nmcl
		LD=$(TOOLDIR)/link.exe
	else
		LD=nmlink.exe $(NMCL)
	endif
else
	LD=$(TOOLDIR)/link.exe
endif
LDOUTFLAG=-OUT:
LDFLAGS_DEBUG=-NOLOGO -DEBUG -DLL -DEF:$(group_name)_exports.def -INCREMENTAL:NO -OPT:REF -OPT:ICF -DYNAMICBASE -APPCONTAINER -NXCOMPAT -NODEFAULTLIB:libcmtd.lib
LDFLAGS_RELEASE=-NOLOGO -DEBUG -DLL -DEF:$(group_name)_exports.def -RELEASE -OPT:REF -OPT:ICF -DYNAMICBASE -APPCONTAINER -NXCOMPAT -NODEFAULTLIB:libcmt.lib
LDLIBS_PLATFORM=ws2_32.lib advapi32.lib

ifeq ($(IMPLARCH),x86-32)
LDFLAGS_DEBUG+= -SAFESEH 
LDFLAGS_RELEASE+= -SAFESEH
endif

ifeq ($(IMPLARCH),x86-64)
LDFLAGS_DEBUG+= -SAFESEH 
LDFLAGS_RELEASE+= -SAFESEH
endif

# static library building
LDS=$(TOOLDIR)/lib.exe
LDSOUTFLAG=-OUT:
LDSFLAGS_DEBUG=-NOLOGO /force:allowzwobj
LDSFLAGS_RELEASE=-NOLOGO /force:allowzwobj

# application linking
LINK=$(TOOLDIR)/link.exe
LINKOUTFLAG=-OUT:
LINKFLAGS_DEBUG=-NOLOGO -DEBUG -SUBSYSTEM:CONSOLE -NODEFAULTLIB:libcmtd.lib
LINKFLAGS_RELEASE=-NOLOGO -RELEASE -SUBSYSTEM:CONSOLE -NODEFAULTLIB:libcmt.lib

# resource compiling
RC=rc.exe
RCOUTFLAG=-fo
RCFLAGS_DEBUG=-d_DEBUG
RCFLAGS_RELEASE=

# Other Tools
AR=
STRIP=
CP=cp -fpu
MV=mv -f


ifeq ($(ARCH),winrt)
	# detect 32/64-bit compiler (since arch is driven by compiler, not flags)
	ccver := $(shell cl.exe 2>&1)
	ifneq ($(findstring x86,$(ccver)),)
		IMPLDIR = winrt
		IMPLARCH = x86-32
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter i386 x86_32 x86-32 x86,$(ARCH)),)
				IMPLARCH = x86-32
			endif
		endif
	endif
	ifneq ($(findstring x64,$(ccver)),)
		IMPLDIR = winrt
		IMPLARCH = x86-64
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin/amd64"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter x86_64 x86-64 x64,$(ARCH)),)
				IMPLARCH = x86-64
			endif
		endif
	endif
	ifneq ($(findstring ARM,$(ccver)),)
		IMPLDIR = winrt
		IMPLARCH = arm-32
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin/x86_arm"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter arm_32 arm-32 arm,$(ARCH)),)
				IMPLARCH = arm-32
			endif
		endif
		LDLIBS_PLATFORM=
	endif	
endif

ifeq ($(IMPLARCH),)
$(error ARCH="$(IMPLARCH)" is unsupported. Windows can't cross compile (use different vcvars.bat setting))
endif

# set compiler version (needed for Windows static lib linking)
ifneq ($(findstring Version 17,$(ccver)),)
	COMPILER_VER=vs12
else
	$(error unsupported compiler version - install windows8 and visual studio 11)
endif

ifdef NMCL
	ifdef no_nmcl
		CC=$(TOOLDIR)/cl.exe
	else
		CC=nmcl.exe $(NMCL)
	endif
else
	CC=$(TOOLDIR)/cl.exe
endif

