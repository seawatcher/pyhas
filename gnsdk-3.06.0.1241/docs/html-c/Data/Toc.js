MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Toc',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<CatapultToc Version=\"1\" DescendantCount=\"101\">' +
	'    <TocEntry Title=\"Documentation Roadmap - Start Here\" Link=\"/Content/shared-topics/doc_roadmaps/DocumentationRoadmapOnline.html\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"true\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"C Developer\'s Guide\" ComputedFirstTopic=\"false\" DescendantCount=\"96\">' +
	'        <TocEntry Title=\"Concepts\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" StartSection=\"false\" PageNumberReset=\"continue\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"31\">' +
	'            <TocEntry Title=\"Introduction\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                <TocEntry Title=\"About Gracenote\" Link=\"/Content/shared-topics/concepts/About Gracenote.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"What is Gracenote SDK\" Link=\"/Content/shared-topics/concepts/What is Gracenote SDK.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"System Requirements and Performance\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                <TocEntry Title=\"System Requirements\" conditions=\"Default.gnsdko\" Link=\"/Content/shared-topics/concepts/System Requirements for gnsdko.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Gracenote Data\" ComputedFirstTopic=\"false\" DescendantCount=\"9\">' +
	'                <TocEntry Title=\"Gracenote Media Elements\" Link=\"/Content/shared-topics/concepts/Gracenote Media Elements.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Terminology\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                    <TocEntry Title=\"Music Terminology\" Link=\"/Content/shared-topics/concepts/Music Terminology.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Video Terminology\" Link=\"/Content/shared-topics/concepts/Video Terminology.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Gracenote Metadata\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"Core and Enriched Metadata\" Link=\"/Content/shared-topics/concepts/Core and Enriched Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Genre and Other List-Dependent Values\" Link=\"/Content/shared-topics/concepts/Genre and Other List-Dependent Values.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Mood and Tempo (Sonic Attributes)\" Link=\"/Content/shared-topics/concepts/Mood and Tempo Sonic Attributes.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Classical Music Metadata\" Link=\"/Content/shared-topics/concepts/Classical Music Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Third-Party Identifiers and Preferred Partners\" Link=\"/Content/shared-topics/concepts/Third-Party Identifiers and Preferred Partners.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Gracenote Data Objects (GDOs)\" Link=\"/Content/shared-topics/concepts/Gracenote Data Objects GDOs.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"GNSDK Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"19\">' +
	'                <TocEntry Title=\"Modules Overview\" Link=\"/Content/shared-topics/concepts/Modules Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Music Module Overview\" Link=\"/Content/shared-topics/concepts/Music Module Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Video Module Overview\" Link=\"/Content/shared-topics/concepts/Video Module Overview.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Recognition Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"MusicID\" ReplaceMergeNode=\"false\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                        <TocEntry Title=\"MusicID Overview\" Link=\"/Content/shared-topics/concepts/MusicID Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"CD TOC Recognition\" Link=\"/Content/shared-topics/concepts/CD TOC Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Text-Based Recognition\" Link=\"/Content/shared-topics/concepts/Text-Based Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Fingerprint-Based Recognition\" Link=\"/Content/shared-topics/concepts/Fingerprint-Based Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                    <TocEntry Title=\"VideoID Overview\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" Link=\"/Content/shared-topics/concepts/VideoID Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Advanced Recognition and Organization Modules\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                    <TocEntry Title=\"MusicID-File Overview\" Link=\"/Content/shared-topics/concepts/MusicID-File Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"MusicID vs. MusicID-File\" Link=\"/Content/shared-topics/concepts/MusicID vs. MusicID-File.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Enriched Content Module (Link)\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                    <TocEntry Title=\"Link Module Overview\" Link=\"/Content/shared-topics/concepts/Link Module Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Music Enrichment\" Link=\"/Content/shared-topics/concepts/Music Enrichment.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Image Formats and Dimensions\" Link=\"/Content/shared-topics/concepts/Image Formats and Dimensions.html\" StartSection=\"false\" SectionNumberReset=\"continue\" VolumeNumberReset=\"same\" ChapterNumberReset=\"continue\" ComputeToc=\"false\" ReplaceMergeNode=\"false\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Video Enrichment\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                        <TocEntry Title=\"Video Enrichment Overview\" Link=\"/Content/shared-topics/concepts/Video Enrichment Overview.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Discovery Modules\" StartSection=\"false\" PageNumberReset=\"continue\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"Playlists\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                        <TocEntry Title=\"Playlists Overview\" Link=\"/Content/shared-topics/concepts/Playlists Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Playlist Requirements and Recommendations\" Link=\"/Content/shared-topics/concepts/Playlist Requirements.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Key Components\" Link=\"/Content/shared-topics/concepts/Key Playlist Components.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"MoodGrid Overview\" Link=\"/Content/shared-topics/concepts/MoodGrid Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                    <TocEntry Title=\"VideoExplore\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                        <TocEntry Title=\"VideoExplore Overview\" Link=\"/Content/shared-topics/concepts/VideoExplore Overview.html\" conditions=\"Default.DesktopOnly,Default.gnsdko,Default.EntourageOnly\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'        </TocEntry>' +
	'        <TocEntry Title=\"Implementing Applications\" ComputedFirstTopic=\"false\" DescendantCount=\"65\">' +
	'            <TocEntry Title=\"Basic Application Design Tasks\" ComputedFirstTopic=\"false\" DescendantCount=\"11\">' +
	'                <TocEntry Title=\"Application Flow\" Link=\"/Content/c-topics/basic_app_design/Application Flow.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Setup and Initialization\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                    <TocEntry Title=\"Working with Header Files and Libraries\" Link=\"/Content/c-topics/basic_app_design/Working with Header Files and Libraries.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Authorizing a GNSDK Application\" Link=\"/Content/c-topics/basic_app_design/Authorizing a GNSDK Application.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Initializing and Shutting Down GNSDK\" Link=\"/Content/c-topics/basic_app_design/Initializing and Shutting Down GNSDK.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Configuring Logging\" Link=\"/Content/c-topics/basic_app_design/Configuring Logging.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Gracenote Data Objects (GDOs)\" ComputedFirstTopic=\"false\" DescendantCount=\"3\">' +
	'                    <TocEntry Title=\"GDO Workflows\" Link=\"/Content/c-topics/basic_app_design/GDO Workflows.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Common GDO Tasks\" Link=\"/Content/c-topics/basic_app_design/Common GDO Tasks.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"GDO Navigation Examples\" Link=\"/Content/c-topics/basic_app_design/GDO Navigation Examples.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Storage and Caching\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'                    <TocEntry Title=\"Using SQLite for Storage and Caching\" Link=\"/Content/c-topics/basic_app_design/Using SQLite for Storage and Caching.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Implementing Status Callbacks\" Link=\"/Content/c-topics/basic_app_design/Implementing Status Callbacks.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Using Locales\" Link=\"/Content/c-topics/basic_app_design/Using Locales.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Using the Sample Applications\" ComputedFirstTopic=\"false\" DescendantCount=\"7\">' +
	'                <TocEntry Title=\"Sample Applications Overview\" Link=\"/Content/c-topics/using_sample_apps/Sample Applications Overview.html\" conditions=\"Default.ScreenOnly\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Building a Sample Application\" Link=\"/Content/c-topics/using_sample_apps/Building a Sample Application.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"MusicID Sample Application Walkthrough\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'                    <TocEntry Title=\"GNSDK MusicID Sample Application\" Link=\"/Content/c-topics/using_sample_apps/MusicID Sample Application Walkthrough/GNSDK MusicID Sample Application.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Prerequisites\" Link=\"/Content/c-topics/using_sample_apps/MusicID Sample Application Walkthrough/Prerequisites.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Initialization\" Link=\"/Content/c-topics/using_sample_apps/MusicID Sample Application Walkthrough/Initialization.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"MusicID Queries\" Link=\"/Content/c-topics/using_sample_apps/MusicID Sample Application Walkthrough/MusicID Queries.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Releasing Resources and Shutting Down\" Link=\"/Content/c-topics/using_sample_apps/MusicID Sample Application Walkthrough/Releasing Resources and Shutting.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Implementing Recognition Features\" ComputedFirstTopic=\"false\" DescendantCount=\"7\">' +
	'                <TocEntry Title=\"Recognizing Music\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                    <TocEntry Title=\"Music Recognition Overview\" Link=\"/Content/c-topics/recognition/music/Music Recognition Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Identifying Music Using a CD TOC\" Link=\"/Content/c-topics/recognition/music/Identifying Music Using a CD TOC.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Identifying Music Using Text\" Link=\"/Content/c-topics/recognition/music/Identifying Music Using Text.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Identifying Music Using Fingerprints\" Link=\"/Content/c-topics/recognition/music/Identifying Music Using Fingerprints.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Recognizing Video\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"3\">' +
	'                    <TocEntry Title=\"Using VideoID\" Link=\"/Content/c-topics/recognition/video/Using VideoID.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Recognizing Video Products with VideoID\" Link=\"/Content/c-topics/recognition/video/Recognizing Video Products with VideoID.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Finding Video Products by TOC\" Link=\"/Content/c-topics/recognition/video/Find Video Products by TOC.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Implementing Advanced Recognition and Organization Features\" ComputedFirstTopic=\"false\" DescendantCount=\"10\">' +
	'                <TocEntry Title=\"Using the MusicID-File APIs\" Link=\"/Content/c-topics/recognition/music/Using the MusicID-File APIs.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Performing Advanced Music Identification\" Link=\"/Content/c-topics/recognition/music/Performing Advanced Music Identification.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Performing Advanced Video Recognition (Video Explore)\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"8\">' +
	'                    <TocEntry Title=\"VideoExplore Find Queries\" Link=\"/Content/c-topics/recognition/video/VideoExplore Find Queries.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"GDO Inputs for Video Find Queries\" Link=\"/Content/c-topics/recognition/video/GDO Inputs for Video Find Queries.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Accessing Video GDOs from other Video GDOs\" Link=\"/Content/c-topics/recognition/video/Accessing Video GDOs from other.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Find Works\" Link=\"/Content/c-topics/recognition/video/Find Works.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Find Seasons\" Link=\"/Content/c-topics/recognition/video/Find Seasons.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Find Series\" Link=\"/Content/c-topics/recognition/video/Find Series.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Find Contributors\" Link=\"/Content/c-topics/recognition/video/Find Contributors.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Find Suggestions\" Link=\"/Content/c-topics/recognition/video/Find Suggestions.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Accessing Core Metadata\" ComputedFirstTopic=\"false\" DescendantCount=\"18\">' +
	'                <TocEntry Title=\"Accessing Music Metadata\" ComputedFirstTopic=\"false\" DescendantCount=\"7\">' +
	'                    <TocEntry Title=\"Navigating Music GDOs\" Link=\"/Content/c-topics/accessing_metadata/music/Navigating Music GDOs.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Accessing Mood and Tempo Metadata\" Link=\"/Content/c-topics/accessing_metadata/music/Accessing Mood and Tempo Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Accessing Classical Music Metadata\" Link=\"/Content/c-topics/accessing_metadata/music/Accessing Classical Music Metadata.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Accessing External IDs\" Link=\"/Content/c-topics/accessing_metadata/music/Accessing External IDs.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Music Metadata Reference\" ComputedFirstTopic=\"false\" DescendantCount=\"3\">' +
	'                        <TocEntry Title=\"Music Metadata Reference Overview\" Link=\"/Content/c-topics/accessing_metadata/music/Music Metadata Reference Overview.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Album Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/music/Album Metadata Example.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Classical Album Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/music/Classical Album Metadata Example.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'                <TocEntry Title=\"Accessing Video Metadata\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"11\">' +
	'                    <TocEntry Title=\"Getting Video Metadata\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'                        <TocEntry Title=\"Implementing Text Filters\" Link=\"/Content/c-topics/accessing_metadata/video/Implementing Text Filters.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Accessing Locale-Dependent Data\" Link=\"/Content/c-topics/accessing_metadata/video/Accessing Locale-Dependent Data.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Accessing AV Work Credits and Filmography\" Link=\"/Content/c-topics/accessing_metadata/video/Accessing AV Work Credits and Filmography.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Accessing Video Product Metadata\" Link=\"/Content/c-topics/accessing_metadata/video/Accessing Video Product Metadata.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                    <TocEntry Title=\"Video Metadata Reference\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"7\">' +
	'                        <TocEntry Title=\"Video Metadata Reference Overview\" Link=\"/Content/c-topics/accessing_metadata/video/Video Metadata Reference Overview.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Contributor Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Contributor Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Product Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Product Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Work Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Work Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Seasons Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Seasons Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Series Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Series Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                        <TocEntry Title=\"Suggestions Metadata Example\" Link=\"/Content/c-topics/accessing_metadata/video/Suggestions Metadata Example.html\" conditions=\"Default.DesktopOnly,Default.EntourageOnly,Default.gnsdko\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    </TocEntry>' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Accessing Enriched Content (Link)\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                <TocEntry Title=\"Using Link\" Link=\"/Content/c-topics/accessing_metadata/link/Using Link.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Accessing Enriched Music Content\" Link=\"/Content/c-topics/accessing_metadata/link/Accessing Enriched Music Content.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Implementing Discovery Features\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                <TocEntry Title=\"Implementing Playlist Features\" StartSection=\"false\" PageNumberReset=\"continue\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                    <TocEntry Title=\"Generating a Playlist\" Link=\"/Content/c-topics/discovery/Generating a Playlist.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                    <TocEntry Title=\"Implementing MoodGrid\" Link=\"/Content/c-topics/discovery/Implementing MoodGrid.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                </TocEntry>' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Advanced Topics\" StartSection=\"false\" PageNumberReset=\"continue\" BreakType=\"chapter\" PageLayout=\"/Content/Resources/PageLayouts/ChapterLetterPL.flpgl\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"false\" DescendantCount=\"6\">' +
	'                <TocEntry Title=\"Working with Non GDO Identifiers\" Link=\"/Content/c-topics/advanced_topics/Working with Non GDO Identifiers.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Improving Matches Using Both CD TOC and Fingerprints\" Link=\"/Content/c-topics/advanced_topics/Improving Matches Using Both.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Rendering a GDO as XML\" Link=\"/Content/c-topics/advanced_topics/Rendering a GDO as XML.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Using the Video TOC Generator APIs\" Link=\"/Content/c-topics/advanced_topics/Using the Video TOC Generator APIs.html\" conditions=\"Default.DesktopOnly,Default.gnsdko,Default.EntourageOnly\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Using Lists\" Link=\"/Content/c-topics/advanced_topics/Using Lists.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Playlist PDL Specification\" Link=\"/Content/c-topics/advanced_topics/Playlist PDL Specification.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'            <TocEntry Title=\"Best Practices and Design Requirements\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'                <TocEntry Title=\"Image Best Practices\" Link=\"/Content/shared-topics/gnsdk_best_practices/Best Practices for Image Dimensions.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'                <TocEntry Title=\"Audio Stream Recognition\" Link=\"/Content/shared-topics/gnsdk_best_practices/Best Practices for Audio Stream Recognition.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'            </TocEntry>' +
	'        </TocEntry>' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"GNSDK Glossary\" Link=\"/Content/shared-topics/front_back_matter/GlossaryList.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"C Data Model Documentation\" Link=\"/Content/shared-topics/api_refs_and_data_model/C-DataModelOnline.html\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"API Reference Documentation\" Link=\"/Content/shared-topics/api_refs_and_data_model/APIRefsDocsOnline.html\" ComputedFirstTopic=\"false\" DescendantCount=\"1\">' +
	'        <TocEntry Title=\"C API Reference\" Link=\"/Content/api_ref_c/html/index.html\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'</CatapultToc>'
);
