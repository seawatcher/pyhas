var group___video___types_enums =
[
    [ "gnsdk_video_search_type_t", "group___video___types_enums.html#ga41d06aa85b7dbfae052519aad723ba26", [
      [ "gnsdk_video_search_type_unknown", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26aea76b61ef665f7227a863b000e6d79b4", null ],
      [ "gnsdk_video_search_type_anchored", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26adc21b93f46d1c3bfe5d6521da8199a69", null ],
      [ "gnsdk_video_search_type_default", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26a458b8fc195d8fd215b04d874c1b51ade", null ]
    ] ],
    [ "gnsdk_video_search_type_anchored", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26adc21b93f46d1c3bfe5d6521da8199a69", null ],
    [ "gnsdk_video_search_type_default", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26a458b8fc195d8fd215b04d874c1b51ade", null ],
    [ "gnsdk_video_search_type_unknown", "group___video___types_enums.html#gga41d06aa85b7dbfae052519aad723ba26aea76b61ef665f7227a863b000e6d79b4", null ],
    [ "GNSDK_DECLARE_HANDLE", "group___video___types_enums.html#gacedf34b7c00032d6af647a818d9ec519", null ]
];