<!DOCTYPE html>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" lang="en-us" xml:lang="en-us" class="no-feedback" data-mc-search-type="Stem" data-mc-help-system-file-name="index.xml" data-mc-path-to-help-system="../../../" data-mc-target-type="WebHelp2" data-mc-runtime-file-type="Topic" data-mc-preload-images="false" data-mc-in-preview-mode="false" data-mc-toc-path="">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>Authorizing a GNSDK Application</title>
        <link href="../../../Skins/Default/Stylesheets/TextEffects.css" rel="stylesheet" />
        <link href="../../../Skins/Default/Stylesheets/Topic.css" rel="stylesheet" />
        <link href="../../Resources/Stylesheets/BookStyles.css" rel="stylesheet" />
        <script src="../../../Resources/Scripts/jquery.min.js">
        </script>
        <script src="../../../Resources/Scripts/plugins.min.js">
        </script>
        <script src="../../../Resources/Scripts/MadCapAll.js">
        </script>
    </head>
    <body>
        <h1><a name="_Toc334991736"></a>Authorizing a <span class="GNVariablesProductName">GNSDK</span> Application</h1>
        <p>Gracenote manages access to metadata using a combination of product licensing and server-side metadata entitlements. </p>
        <p>When developing a <span class="GNVariablesProductName">GNSDK</span> application, you must include a client ID and license file to authorize your application with Gracenote. In general, the License file enables your application to use the Gracenote products (and their corresponding <span class="GNVariablesProductName">GNSDK</span> modules) that you purchased.&#160;The client ID is used by Gracenote Media Services to enable access to the metadata your application is entitled to use.</p>
        <h2><a name="_Toc334991737"></a>Client IDs</h2>
        <p>Each <span class="GNVariablesProductName">GNSDK</span> customer receives a unique client ID string from Gracenote.&#160;This string uniquely identifies each application to Gracenote Media Services and lets Gracenote deliver the specific metadata the application requires.&#160;</p>
        <p>A client ID string has the following format: 123456-789123456789012312. It consists of a six-digit client ID, and  a 17-digit client ID Tag, separated by a hyphen (-).</p>
        <p>When creating a new User with gnsdk_manager_user_create_new(), you must pass in the client ID and client ID tag as separate parameters. For more information, see <a href="#_Toc334991739" class="GNBasic MCXref xref xrefGNBasic" xrefformat="{paratext}">Users</a>.</p>
        <h2><a name="_Toc334991738"></a>License Files</h2>
        <p>Gracenote provides a license file along with your client ID.&#160; The license file notifies Gracenote to enable the <span class="GNVariablesProductName">GNSDK</span> products you purchased for your application. Below is an example license file, showing enabled and disabled Gracenote products for Customer A. These products generally map to corresponding <span class="GNVariablesProductName">GNSDK</span> modules.</p><pre xml:space="preserve">-- BEGIN LICENSE v1.0 ABC123XYZ --
licensee: Gracenote, Inc.
name: CustomerA
notes: [CustomerA Account#]

client_id: 999999
musicid_text: enabled
musicid_cd: enabled
playlist: enabled
local_images: enabled
local_mood: enabled

-- SIGNATURE ABC123XYZ --
ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcedefghijklmnopqrstuvwxyz
-- END LICENSE ABC123XYZ --
</pre>
        <h2><a name="_Toc334991739"></a>Users</h2>
        <p>To perform queries, an application must first register a new User and get its handle. A User represents an individual installation of a specific client ID string. This ensures that each application instance is receiving all required metadata entitlements. </p>
        <p>Users are represented in <span class="GNVariablesProductName">GNSDK</span> by User handles.  You can create a User handle using either gnsdk_manager_user_create_new() or gnsdk_manager_user_create(). </p>
        <p>When creating a new User with gnsdk_manager_user_create_new(), you must pass in the client ID and client ID tag as separate parameters. For more information about client IDs and client ID tags, see <a href="#_Toc334991737" class="GNBasic MCXref xref xrefGNBasic" xrefformat="{paratext}">Client IDs</a>.</p>
        <p>Each application installation should only request a new User once and store the serialized representation of the User for future use. For example:</p><pre>
gnsdk_user_handle_t user_hdl = GNSDK_NULL; // User handle
gnsdk_cstr_t        ser_hdl  = GNSDK_NULL; // Serialized user handle

/* Get serialized user handle if one has been saved */
userHandleSaved = get_serialized_user_data(&amp;ser_hdl); // NOT a Gracenote API

/* Call API to register and create new user */
if (userHandleSaved)
{
    // This API takes a serialized user handle ('ser_hdl') and creates a
    // non-serialized user handle in 'user_hdl'
    error = gnsdk_manager_user_create(ser_hdl, &amp;user_hdl);
}
else
{
    // This API creates a new, non-serialized user handle in 'user_hdl'
    error = gnsdk_manager_user_create_new(clientid, clienttag, clientver, &amp;user_hdl);
}

/* Perform queries - this changes the user handle */
/* ... */

/*********************************************************************
 *
 * R E L E A S E  U S E R
 *
 * This API MAY serialize the user handle to 'ser_hdl':
 * If you called gnsdk_manager_user_create_new(), then
 * gnsdk_manager_user_release() will ALWAYS set 'ser_hdl' to a serialized
 * user handle. If you used gnsdk_manager_user_create(), then 'ser_hdl' will
 * ONLY contain data if the user handle has changed between 'create' and 'release'.
 *
 *********************************************************************/
error = gnsdk_manager_user_release(user_hdl, &amp;ser_hdl);

/*
 * Save serialized user handle before exiting
 * It is recommended that a serialized user handle be saved only upon close as
 * the user handle can change during the course of the app
 */
if (!error &amp;&amp; ser_hdl)
{
    save_serialized_user_data(ser_hdl); /* NOT a Gracenote API */
}

/* Close GNSDK */

		</pre>
        <h3><a name="_Toc334991741"></a>Managing User Handles</h3>
        <p>In general, all User handles are thread-safe and can be simultaneously used by multiple queries.</p>
        <p>Basic User management process:</p>
        <ol>
            <li value="1">First application run: create new User and get handle.</li>
            <li value="2">Provide User handle to GNSDK APIs that require one.</li>
            <li value="3">Release User handle when finished and store it as serialized data.</li>
            <li value="4">Subsequent application runs: create User from stored serialized data.</li>
        </ol>
        <p>If an application registers a new User on every use instead of storing a serialized User, then the User quota maintained for the client ID is quickly exhausted. Once the quota is reached, attempting to create new Users will fail. If an application uses a single User registration across multiple installations of the application—in short, forcing all the installations to use the same User—the application risks exhausting Gracenote per-user query quota.</p>
        <p>To maintain an accurate usage profile of your application, and to ensure that the services you are entitled to are not being used unintentionally, your application should register new Users only when needed, and then store that User for future queries.</p>
        <p class="onlineFooter">© 2000 to present. Gracenote, Inc. All rights reserved.</p>
        <p><a href="mailto:doc_feedback@gracenote.com?subject=Gracenote Documentation Feedback" target="_blank" title="Send comments about this topic to Gracenote Technical Publications." alt="Send comments about this topic to Gracenote Technical Publications.">How can we improve this documentation?</a>
        </p>
    </body>
</html>