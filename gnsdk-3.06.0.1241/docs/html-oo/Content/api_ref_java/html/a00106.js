var a00106 =
[
    [ "GnPlaylist", "a00106.html#a07bc650f3e35c101dd2a0657d0f228ec", null ],
    [ "GnPlaylist", "a00106.html#a3240fcb164f9ae1e1bf0ede1150bfdfd", null ],
    [ "buildDate", "a00106.html#adcfe359625d8ae20bf11d57bc4bbaea8", null ],
    [ "compactStorage", "a00106.html#a3dd90169b3f55c920658bd328aba0e20", null ],
    [ "createCollection", "a00106.html#a7ad63e098682fb6582ab6122ed5a6826", null ],
    [ "delete", "a00106.html#ab3ee883a6e12f3e3a88846a65ef2721a", null ],
    [ "deserializeCollection", "a00106.html#ac8ba03caf1e30716a13b3a4ade0e59ee", null ],
    [ "finalize", "a00106.html#a07109f3aea94b6197117e20b730453cb", null ],
    [ "loadCollection", "a00106.html#ae2b3cffb2d48f1dbb94629c6c95406b1", null ],
    [ "loadCollection", "a00106.html#a521b095960bfb4c3e2b766ac112f8e2f", null ],
    [ "removeFromStorage", "a00106.html#aae5068ca54749799f86eb00ab53fc385", null ],
    [ "removeFromStorage", "a00106.html#ac40e2c316d5c65254e226a3a4c57614c", null ],
    [ "serializeBufferSize", "a00106.html#a627479f066757a25e8bd318ca533d1a8", null ],
    [ "serializeCollection", "a00106.html#abe8b66f24b2f88d1a307db745d31fa2e", null ],
    [ "setStorageLocation", "a00106.html#a49b229be4167c2313dcda5c91c7789e8", null ],
    [ "storageIsValid", "a00106.html#adcda74d9a31a1777ea8cdcaec12b58f8", null ],
    [ "storeCollection", "a00106.html#a62305e4b608ab25aacd643f1a147a728", null ],
    [ "storedCollectionNameAt", "a00106.html#a4323483c1c989b39e7945a3c66439b0b", null ],
    [ "storedCollectionsCount", "a00106.html#a642a76ebaa5feb5c16aa85de248571b1", null ],
    [ "version", "a00106.html#a31b9fc99ba35814dccd0b3ce5d7df79b", null ]
];