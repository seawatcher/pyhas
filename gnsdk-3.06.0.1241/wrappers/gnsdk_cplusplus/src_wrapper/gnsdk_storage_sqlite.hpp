/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_STORAGE_SQLITE_HPP_
#define _GNSDK_STORAGE_SQLITE_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"

#if GNSDK_STORAGE_SQLITE

namespace gracenote
{
	namespace storage_sqlite
	{
		/**************************************************************************
		** GnStorageSqlite
		*/
		class GnStorageSqlite : public GnObject
		{
		public:
			GnStorageSqlite()  throw ( GnError )
			{
				GnSDK::init_gnsdk_module(GNSDK_COMPONENT_STORAGE_SQLITE);
			}

			virtual ~GnStorageSqlite() { }

			/**
			 *  Retrieves the version string of the Storage SQLite library.
			 **/
			static gnsdk_cstr_t Version()
			{
				return gnsdk_storage_sqlite_get_version();
			}

			static gnsdk_cstr_t BuildDate()
			{
				return gnsdk_storage_sqlite_get_build_date();
			}

			/**
			 *  Retrieves the version string of the internal SQLite database engine.
			 **/
			static gnsdk_cstr_t SqliteVersion()
			{
				return gnsdk_storage_sqlite_get_sqlite_version();
			}

			void StorageFolderSet(gnsdk_cstr_t folderPath) throw ( GnError )
			{
				gnsdk_error_t error;

				error = gnsdk_storage_sqlite_option_set(GNSDK_STORAGE_SQLITE_OPTION_STORAGE_FOLDER, folderPath);
				if(error) { throw GnError(); }
			}

			gnsdk_cstr_t StorageFolderGet() throw ( GnError )
			{
				gnsdk_error_t error;
				gnsdk_cstr_t  storageFolder = "";

				error = gnsdk_storage_sqlite_option_get(GNSDK_STORAGE_SQLITE_OPTION_STORAGE_FOLDER, &storageFolder);
				if(error) { throw GnError(); }

				return storageFolder;
			}

			/**
			 * Sets the maximum size the GNSDK cache can grow to; for example �100� for 100 Kb or �1024� for 1
			 *  MB. This limit applies to each cache that is created.
			 *
			 *  The value passed for this option is the maximum number of Kilobytes that the cache files can grow
			 *  to. For example, �100� sets the maximum to 100 KB, and �1024� sets the maximum to 1 MB.
			 *
			 *  If the cache files' current size already exceeds the maximum when this option is set, then the
			 *  set maximum is not applied.
			 *
			 *  When the maximum size is reached, new cache entries are not written to the database.
			 *  Additionally, a maintenance thread is run that attempts to clean up expired records from the
			 *  database and create space for new records.
			 *
			 *  If this option is not set the cache files default to having no maximum size.
			 */
			void MaximumSizeForCacheFileSet(gnsdk_cstr_t maxCacheSize) throw ( GnError )
			{
				gnsdk_error_t error;

				error = gnsdk_storage_sqlite_option_set(GNSDK_STORAGE_SQLITE_OPTION_CACHE_FILESIZE, maxCacheSize);
				if(error) { throw GnError(); }
			}

			gnsdk_cstr_t MaximumSizeForCacheFileGet() throw ( GnError )
			{
				gnsdk_error_t error;
				gnsdk_cstr_t  maxCacheFileSize = "";

				error = gnsdk_storage_sqlite_option_get(GNSDK_STORAGE_SQLITE_OPTION_CACHE_FILESIZE, &maxCacheFileSize);
				if(error) { throw GnError(); }

				return maxCacheFileSize;
			}

			/**
			 *  Sets the maximum amount of memory SQLite can use to buffer cache data.
			 *
			 *  The value passed for this option is the maximum number of Kilobytes of memory that can be used.
			 *  For example, �100� sets the maximum to 100 KB, and �1024� sets the maximum to 1 MB.
			 *
			 */
			void MaximumMemorySizeForCacheSet(gnsdk_cstr_t maxMemSize) throw ( GnError )
			{
				gnsdk_error_t error;

				error = gnsdk_storage_sqlite_option_set(GNSDK_STORAGE_SQLITE_OPTION_CACHE_MEMSIZE, maxMemSize);
				if(error) { throw GnError(); }
			}

			gnsdk_cstr_t MaximumMemorySizeForCacheGet() throw ( GnError )
			{
				gnsdk_error_t error;
				gnsdk_cstr_t  maxMemSize = "";

				error = gnsdk_storage_sqlite_option_get(GNSDK_STORAGE_SQLITE_OPTION_CACHE_MEMSIZE, &maxMemSize);
				if(error) { throw GnError(); }

				return maxMemSize;
			}

			/**
			 *  Sets the method that SQLite uses to write to the cache files.
			 *
			 *  This option is available for SQLite performance tuning. Valid values for this option are:
			 *  <ul>
			 *  <li>OFF (default setting): No synchronous writing; the quickest but least safe method.</li>
			 *  <li>NORMAL: Synchronous writing only at critical times; the generally quick and safe method.</li>
			 *  <li>FULL: Always synchronous writing; the slowest and safest method.</li>
			 *  </ul>
			 *
			 *  If the threat of losing a cache file due to hardware failure is high, then set this option in
			 *  your application to NORMAL or FULL.
			 */
			void SynchronousOptionForCacheSet(gnsdk_cstr_t option) throw ( GnError )
			{
				gnsdk_error_t error;

				error = gnsdk_storage_sqlite_option_set(GNSDK_STORAGE_SQLITE_OPTION_SYNCHRONOUS, option);
				if(error) { throw GnError(); }
			}

			gnsdk_cstr_t SynchronousOptionForCacheGet() throw ( GnError )
			{
				gnsdk_error_t error;
				gnsdk_cstr_t  syncOption = "";

				error = gnsdk_storage_sqlite_option_get(GNSDK_STORAGE_SQLITE_OPTION_SYNCHRONOUS, &syncOption);
				if(error) { throw GnError(); }

				return syncOption;
			}

			/**
			 *  Sets how the SQLite journal file is managed for database transactions.
			 *
			 *  This option is available for SQLite performance tuning. Valid values for this option are:
			 *  <ul>
			 *  <li>DELETE: Journal file is deleted after each transaction.</li>
			 *  <li>TRUNCATE: Journal file is truncated (but not deleted) after each transaction.</li>
			 *  <li>PERSIST: Journal file remains after each transaction.</li>
			 *  <li>MEMORY (default setting): Journal file is only stored in memory for each transaction.</li>
			 *  <li>OFF: No journaling is performed.</li>
			 *  </ul>
			 **/
			void JournalModeSet(gnsdk_cstr_t mode) throw ( GnError )
			{
				gnsdk_error_t error;

				error = gnsdk_storage_sqlite_option_set(GNSDK_STORAGE_SQLITE_OPTION_JOURNAL_MODE, mode);
				if(error) { throw GnError(); }
			}

			gnsdk_cstr_t JournalModeGet() throw ( GnError )
			{
				gnsdk_error_t error;
				gnsdk_cstr_t  journalMode = "";

				error = gnsdk_storage_sqlite_option_get(GNSDK_STORAGE_SQLITE_OPTION_JOURNAL_MODE, &journalMode);
				if(error) { throw GnError(); }

				return journalMode;
			}

		private:
			/* dissallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnStorageSqlite);
		};
	} // namespace storage_sqlite
}     // namespace gracenote
#endif /* GNSDK_STORAGE_SQLITE */
#endif // _GNSDK_STORAGE_SQLITE_HPP_
