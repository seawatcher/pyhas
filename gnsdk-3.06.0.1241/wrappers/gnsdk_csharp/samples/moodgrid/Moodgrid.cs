﻿/*
* Copyright (c) 2000-2013 Gracenote.
*
* This software may not be used in any way or distributed without
* permission. All rights reserved.
*
* Some code herein may be covered by US and international patents.
*/

/*
 * Name: Moodgrid
 * Description:
 * Moodgrid is the GNSDK library that generates playlists based on Mood by simply accessing parts of the Grid.
 * moodgrid APIs enable an application to:
 * 01. Discover and enumerate all datasources available for Moodgrid (from both offline and online sources )
 * 02. Create and administer data representation of Moodgrid for different types of datasources and grid types.
 * 03. Query a grid cell for a playlist based on the Mood it represents.
 * 04. Set Pre Filters on the Moodgrid for Music Genre, Artist Origin and Artist Era.
 * 05. Manage results.
 * Datasources : Offline datasources for Moodgrid can be created by using Gnsdk Playlist. This sample demonstrates this.
 * Steps:
 *  See inline comments below.
 * Notes:
 * For clarity and simplicity error handling in not shown here.
 * Refer "logging" sample to learn about GNSDK error handling.

 * Command-line Syntax:
 *   sample client_id client_id_tag license
 */

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;

namespace Sample
{
    class Moodgrid
    {
        private static string localDB = @"..\..\..\sample_db";
        private static GnUser user = null;

        //set this to false to do online queries
        private static bool useLocal = false;
        static void
                    Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                String clientId = args[0];
                String clientIdTag = args[1];
                String licenseFile = args[2];
                String gnsdkLibraryPath = args[3];
                String applicationVersion = "1.0.0.0";

                try
                {
                    // Initialize SDK
                    GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                    // Dispaly SDK version
                    Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                    // Enable logging
                    gnsdk.LoggingEnable(
                        "sample.log",
                        GnSDK.GN_LOG_PKG_ALL,
                        GnSDK.GN_LOG_LEVEL_ERROR,
                        GnSDK.GN_LOG_OPTION_ALL,
                        0,                                /* Max size of log: 0 means a new log file will be created each run */
                        false);                           /* true = old logs will be renamed and saved */

                    /*         
                     *    Load existing user handle, or register new one.                    
                     */
                    user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

                    // Initialize the Storage SQLite Library
                    GnStorageSqlite gnStorage = new GnStorageSqlite();
					
                    if (useLocal)
                    {
                        gnStorage.StorageFolder = localDB;

                        GnLookupLocal gnLookupLocal = new GnLookupLocal();

                        //Set the user option to use our local Gracenote DB unless overridden			            
                        user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                        // Display information about our local EDB 
                        DisplayEmbeddedDbInfo(gnLookupLocal);

                    }
                    else
                    {
                        user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                    }
					//  locale to retrieve Gracenote Descriptor/List based values e.g, genre, era, origin, mood, tempo
					GnLocale locale = new GnLocale(GnSDK.kLocaleGroupPlaylist,
											   GnSDK.kLanguageEnglish,
											   GnSDK.kRegionDefault,
											   GnSDK.kDescriptorSimplified,
											   user,
											   null /*localeEvents*/);
                 
                   
					gnsdk.SetDefaultLocale(locale);
                    /* Perform a sample album TOC query */
                    PerformSampleMoodgrid(user);

                }

                // All gracenote sdk objects throws GnException
                catch (GnException e)
                {
                    Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                    Console.WriteLine("Error API            :: " + e.ErrorAPI);
                    Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                    Console.WriteLine("SourceE rror Module :: " + e.SourceErrorModule);
                }
            }
        }

        /***************************************************************************
         *
         *    PerformSampleMoodgrid
         *
         ***************************************************************************/
        private static void PerformSampleMoodgrid(GnUser user)
        {
            GnPlaylist playlist = new GnPlaylist();
            playlist.SetStorageLocation(".");

            /*How may collections are stored? */
            uint storedCollCount = playlist.StoredCollectionsCount();
            GnPlaylistCollection collection = null;
            Console.WriteLine("\nStored Collections Count: " + storedCollCount);


            if (storedCollCount == 0)
            {
                /* Create new collection onlne if not stored any*/
                collection = playlist.CreateCollection("sample_collection");
                DoMusicRecognition(user, collection);
                playlist.StoreCollection(collection);
            }
            else
            {
                /* Load existing collection from local store*/
                Console.WriteLine("\nLoading sample collection");
                collection = playlist.LoadCollection("sample_collection");
            }

            /* create, query and print a  5 x  5  moodgrid  */
            DoMoodgridPresentation(user, collection, GnMoodgridPresentationType.kMoodgridPresentationType5x5);

            /* create, query and print a 10 x 10 moodgrid*/
            DoMoodgridPresentation(user, collection, GnMoodgridPresentationType.kMoodgridPresentationType10x10);

        }

        /***************************************************************************
         *
         *   DoMusicRecognition
         *
         *   Do Music Recognition
         *
         ***************************************************************************/
        private static void DoMusicRecognition(GnUser user, GnPlaylistCollection collection)
        {
            uint countOrdinal = 0;
            string[] inputQueryTocs =  {
		        "150 13224 54343 71791 91348 103567 116709 132142 141174 157219 175674 197098 238987 257905",
		        "182 23637 47507 63692 79615 98742 117937 133712 151660 170112 189281",
		        "182 14035 25710 40955 55975 71650 85445 99680 115902 129747 144332 156122 170507",
		        "150 10705 19417 30005 40877 50745 62252 72627 84955 99245 109657 119062 131692 141827 152207 164085 173597 187090 204152 219687 229957 261790 276195 289657 303247 322635 339947 356272",
		        "150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342",
		        "150 1307 15551 31744 45022 57486 72947 85253 100214 115073 128384 141948 152951 167014",
		        "183 69633 96258 149208 174783 213408 317508",
		        "150 19831 36808 56383 70533 87138 105157 121415 135112 151619 169903 189073",
		        "182 10970 29265 38470 59517 74487 83422 100987 113777 137640 150052 162445 173390 196295 221582",
		        "150 52977 87922 128260 167245 187902 215777 248265",
		        "183 40758 66708 69893 75408 78598 82983 87633 91608 98690 103233 108950 111640 117633 124343 126883 132298 138783 144708 152358 175233 189408 201408 214758 239808",
		        "150 92100 135622 183410 251160 293700 334140",
		        "150 17710 33797 65680 86977 116362 150932 166355 183640 193035",
		        "150 26235 51960 73111 93906 115911 142086 161361 185586 205986 227820 249300 277275 333000",
		        "150 1032 27551 53742 75281 96399 118691 145295 165029 189661 210477 232501 254342 282525",
		        "150 26650 52737 74200 95325 117675 144287 163975 188650 209350 231300 253137 281525 337875",
		        "150 19335 35855 59943 78183 96553 111115 125647 145635 163062 188810 214233 223010 241800 271197",
		        "150 17942 32115 47037 63500 79055 96837 117772 131940 148382 163417 181167 201745",
		        "150 17820 29895 41775 52915 69407 93767 105292 137857 161617 171547 182482 204637 239630 250692 282942 299695 311092 319080",
		        "182 21995 45882 53607 71945 80495 94445 119270 141845 166445 174432 187295 210395 230270 240057 255770 277745 305382 318020 335795 356120",
		        "187 34360 64007 81050 122800 157925 195707 230030 255537 279212 291562 301852 310601",
		        "150 72403 124298 165585 226668 260273 291185"
	        };



            int count = inputQueryTocs.Count();

            Console.WriteLine("\nPopulating Collection Summary from sample TOCs");
            /* Create the query handle */
            GnMusicID musicid = new GnMusicID(user);

            /*
            Set the option for retrieving Playlist attributes.
            Note: Please note that these attributes are entitlements to your user id. You must have them enabled in your licence.
            */
            musicid.OptionLookupData(GnLookupData.kLookupDataPlaylist, true);

            foreach (string toc in inputQueryTocs)
            {
                // Find album
                GnResponseAlbums responseAlbums = musicid.FindAlbums(toc);

                GnAlbum album = responseAlbums.Albums.at(0).Current;

                /*  Add MusicID result to Playlist   */
                uint trackCount = album.Tracks.count();

                /*  To make playlist of tracks we add each and every track in the album to the collection summary */
                for (uint trackOrdinal = 1; trackOrdinal <= trackCount; ++trackOrdinal)
                {
                    GnTrack track = album.Tracks.at(trackOrdinal - 1).Current;


                    /* create a unique ident for every track that is added to the playlist.
                       Ideally the ident allows for the identification of which track it is.
                       e.g. path/filename.ext , or an id that can be externally looked up.
                    */
                    string uniqueIdent = countOrdinal + "_" + trackOrdinal;
                    /*
                        Add the the Album and Track GDO for the same ident so that we can
                        query the Playlist Collection with both track and album level attributes.
                    */
                    collection.Add(uniqueIdent, album);
                    collection.Add(uniqueIdent, track);

                    Console.Write("..");
                }
                countOrdinal++;
            }

            Console.WriteLine("\n Finished Recognition");
        }

        /***************************************************************************
         *
         *   DoMoodgridPresentation
         *
         *   Do Moodgrid Presentation
         *
         ***************************************************************************/
        private static void DoMoodgridPresentation(GnUser user, GnPlaylistCollection collection, GnMoodgridPresentationType gnMoodgridPresentationType)
        {

            GnMoodgridPresentation presentation = null;
            uint count = 0;
            string value = null;

            GnMoodgrid moodgrid = new GnMoodgrid();

            GnMoodgridProvider provider = new GnMoodgridProvider();
            provider = moodgrid.Providers.begin().next();

            /* get the details for the provider */
            value = provider.Name;
            Console.WriteLine("\nGNSDK_MOODGRID_PROVIDER_NAME : " + value);

            value = provider.Type;
            Console.WriteLine("\nGNSDK_MOODGRID_PROVIDER_TYPE : " + value);

            value = provider.RequiresNetwork.ToString();
            Console.WriteLine("\nGNSDK_MOODGRID_PROVIDER_NETWORK_USE : " + value);

            GnMoodgrid moodGrid = new GnMoodgrid();

            /* create a moodgrid presentation for the specified type */
            presentation = moodGrid.CreatePresentation(user, gnMoodgridPresentationType);

            /* query the presentation type for its dimensions */
            // TODO get max_x and max_y
            // Console.WriteLine("\n PRINTING MOODGRID " + max_x + " x " + max_y + " GRID \n");

            /* enumerate through the moodgrid getting individual data and results */
            GnMoodgridPresentationDataEnumerable moodgridPresentationDataEnumerable = presentation.Moods;
            foreach (GnMoodgridDataPoint position in moodgridPresentationDataEnumerable)
            {
                uint x = position.X;
                uint y = position.Y;

                /* get the name for the grid coordinates in the language defined by Locale*/
                string name = presentation.MoodName(position);

                /* get the mood id */
                string id = presentation.MoodId(position);

                /* find the recommendation for the mood */
                GnMoodgridResult moodgridResult = presentation.FindRecommendations(provider, position);

                /* count the number of results */
                count = moodgridResult.Count();
                Console.WriteLine("\n\n\tX:" + x + " Y:" + y + "\tMood Name: " + name + "\tMood ID: " + id + "\tCount: " + count + "\n");

                /* iterate the results for the idents */
                GnMoodgridResultEnumerable identifiers = moodgridResult.Identifiers;
                foreach (GnMoodgridIdentifier identifier in identifiers)
                {
                    string ident = identifier.MediaIdentifier;
                    string group = identifier.Group;

                    Console.WriteLine("\n\tX:" + x + " Y:" + y + "\nident:\t" + ident + "\ngroup:\t" + group + "\n");
                    
                 
                    GnPlaylistMetadata data = collection.Metadata(user,ident, group);
					Console.WriteLine("Album:\t" + data.AlbumName);
                    Console.WriteLine("Mood :\t" + data.Mood);
   
                }

            }
        }

        // Display local Gracenote DB information.
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }


        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }
    }
}
