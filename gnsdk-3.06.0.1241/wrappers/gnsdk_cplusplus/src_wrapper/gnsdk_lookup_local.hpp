/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_LOOKUP_LOCAL_HPP_
#define _GNSDK_LOOKUP_LOCAL_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"

#if GNSDK_LOOKUP_LOCAL

namespace gracenote
{
	namespace lookup_local
	{
		enum GnLocalStorageName
		{
			/**
			 * Name of the local storage the SDK uses to query Gracenote Content.
			 * @ingroup Setup_StorageIDs
			 */
			kContent = 1,

			/**
			 * Name of the local storage the SDK uses to query Gracenote Metadata.
			 * @ingroup Setup_StorageIDs
			 */
			kMetadata,

			/**
			 * Name of the local storage the SDK uses for CD TOC searching.
			 * @ingroup Setup_StorageIDs
			 */
			kTOCIndex,

			/**
			 * Name of the local storage the SDK uses for text searching.
			 * @ingroup Setup_StorageIDs
			 */
			KTextIndex
		};

		enum GnLocalStorageInfoKey
		{
			kGDBVersion = 1,
			kImageSize
		};

		class GnLookupLocal : public GnObject
		{
		public:
			/**
			 *  GnLookupLocal
			 *  Initializes the Lookup Local library.
			 *  @param user [in] GnUser object
			 *  <p><b>Remarks:</b></p>
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			GnLookupLocal() throw ( GnError )
			{
				GnSDK::init_gnsdk_module(GNSDK_COMPONENT_LOOKUP_LOCAL);
			}

			virtual ~GnLookupLocal() { }

			/**
			 *  Version
			 *  Retrieves the Lookup Local SDK version string.
			 *  @return version string if successful
			 *  @return GNSDK_NULL if not successful
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
			 *   string is a constant. Do not attempt to modify or delete.
			 *  Example version string: 1.2.3.123 (Major.Minor.Improvement.Build)
			 *  Major: New functionality
			 *  Minor: New or changed features
			 *  Improvement: Improvements and fixes
			 *  Build: Internal build number
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			gnsdk_cstr_t Version()
			{
				return gnsdk_lookup_local_get_version();
			}

			/**
			 *  BuildDate
			 *  Retrieves the Lookup Local SDK's build date string.
			 *  @return Note Build date string of the format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
			 *   string is a constant. Do not attempt to modify or delete.
			 *  Example build date string: 2008-02-12 00:41 UTC
			 * @ingroup Music_Local_InitializationFunctions
			 */
			gnsdk_cstr_t BuildDate()
			{
				return gnsdk_lookup_local_get_build_date();
			}

			void StorageCompact(GnLocalStorageName storageName) throw ( GnError )
			{
				gnsdk_cstr_t  storagename = GNSDK_NULL;
				gnsdk_error_t error       = GNSDK_SUCCESS;

				switch(storageName)
				{
				case kContent:    storagename = GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT;
					break;

				case kMetadata:   storagename = GNSDK_LOOKUP_LOCAL_STORAGE_METADATA;
					break;

				case kTOCIndex:  storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX;
					break;

				case KTextIndex: storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX;
					break;
				}

				error = gnsdk_lookup_local_storage_compact(storagename);
				if(GNSDK_SUCCESS != error)
				{
					throw GnError();
				}
			}

			/**
			 *  Storage Location
			 *  Sets the location for a particular local storage.
			 *	@param storageName [in] local storage name
			 *	@param storageLocation [in] local storage location
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully.
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			void StorageLocation(GnLocalStorageName storageName, gnsdk_cstr_t storageLocation) throw ( GnError )
			{
				gnsdk_cstr_t  storagename = GNSDK_NULL;
				gnsdk_error_t error       = GNSDK_SUCCESS;

				switch(storageName)
				{
				case kContent:    storagename = GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT;
					break;

				case kMetadata:   storagename = GNSDK_LOOKUP_LOCAL_STORAGE_METADATA;
					break;

				case kTOCIndex:  storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX;
					break;

				case KTextIndex: storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX;
					break;
				}

				error = gnsdk_lookup_local_storage_location_set(storagename, storageLocation);
				if(GNSDK_SUCCESS != error)
				{
					throw GnError();
				}
			}

			/**
			 *  Storage Validate
			 *  Performs validation on named local storage.
			 *	@param storageName [in] local storage name
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully.
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			void StorageValidate(GnLocalStorageName storageName) throw ( GnError )
			{
				gnsdk_cstr_t       storagename = GNSDK_NULL;
				gnsdk_error_t      error       = GNSDK_SUCCESS;
				gnsdk_error_info_t error_info;

				switch(storageName)
				{
				case kContent:    storagename = GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT;
					break;

				case kMetadata:   storagename = GNSDK_LOOKUP_LOCAL_STORAGE_METADATA;
					break;

				case kTOCIndex:  storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX;
					break;

				case KTextIndex: storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX;
					break;
				}

				error = gnsdk_lookup_local_storage_validate(storagename, &error_info);
				if(GNSDK_SUCCESS != error)
				{
					throw GnError();
				}
			}

			/**
			 *  Storage Info
			 *  Retrieves the Local Storage information for a key.
			 *  @param storageName [in] local storage name
			 *  @param storageInfoKey [in] local storage info key
			 *  @param ordinal [in] ordinal
			 *  @return info string if successful
			 *  @return GNSDK_NULL if not successful
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. he returned
			 *   string is a constant. Do not attempt to modify or delete.
			 *  Example version string: 1.2.3.123 (Major.Minor.Improvement.Build)
			 *  Major: New functionality
			 *  Minor: New or changed features
			 *  Improvement: Improvements and fixes
			 *  Build: Internal build number
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			gnsdk_cstr_t StorageInfo(GnLocalStorageName storageName, GnLocalStorageInfoKey storageInfoKey, gnsdk_uint32_t ordinal) throw ( GnError )
			{
				gnsdk_cstr_t  storagename = GNSDK_NULL;
				gnsdk_cstr_t  key         = GNSDK_NULL;
				gnsdk_error_t error       = GNSDK_SUCCESS;
				gnsdk_cstr_t  info        = GNSDK_NULL;

				switch(storageName)
				{
				case kContent:    storagename = GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT;
					break;

				case kMetadata:   storagename = GNSDK_LOOKUP_LOCAL_STORAGE_METADATA;
					break;

				case kTOCIndex:  storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX;
					break;

				case KTextIndex: storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX;
					break;
				}

				switch(storageInfoKey)
				{
				case kGDBVersion:   key = GNSDK_LOOKUP_LOCAL_STORAGE_GDB_VERSION;
					break;

				case kImageSize:    key = GNSDK_LOOKUP_LOCAL_STORAGE_IMAGE_SIZE;
					break;
				}

				error = gnsdk_lookup_local_storage_info_get(storagename, key, ordinal, &info);
				if(GNSDK_SUCCESS != error)
				{
					throw GnError();
				}

				return info;
			}

			/**
			 *  Storage Info Count
			 *  Retrieves the Local Storage information count for a key.
			 *  @param storageName [in] local storage name
			 *  @param storageInfoKey [in] local storage info key
			 *  @return count if successful
			 *  @return GNSDK_NULL if not successful
			 * @ingroup Music_LookupLocal_InitializationFunctions
			 */
			gnsdk_uint32_t StorageInfoCount(GnLocalStorageName storageName, GnLocalStorageInfoKey storageInfoKey) throw ( GnError )
			{
				gnsdk_cstr_t   storagename = GNSDK_NULL;
				gnsdk_cstr_t   key         = GNSDK_NULL;
				gnsdk_error_t  error       = GNSDK_SUCCESS;
				gnsdk_uint32_t count       = 0;

				switch(storageName)
				{
				case kContent:    storagename = GNSDK_LOOKUP_LOCAL_STORAGE_CONTENT;
					break;

				case kMetadata:   storagename = GNSDK_LOOKUP_LOCAL_STORAGE_METADATA;
					break;

				case kTOCIndex:  storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TOCINDEX;
					break;

				case KTextIndex: storagename = GNSDK_LOOKUP_LOCAL_STORAGE_TEXTINDEX;
					break;
				}

				switch(storageInfoKey)
				{
				case kGDBVersion:   key = GNSDK_LOOKUP_LOCAL_STORAGE_GDB_VERSION;
					break;

				case kImageSize:    key = GNSDK_LOOKUP_LOCAL_STORAGE_IMAGE_SIZE;
					break;
				}

				error = gnsdk_lookup_local_storage_info_count(storagename, key, &count);
				if(GNSDK_SUCCESS != error)
				{
					throw GnError();
				}

				return count;
			}

		private:
			/* disallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnLookupLocal);
		};
	} // namespace lookup_local
}     // namespace gracenote
#endif /* GNSDK_LOOKUP_LOCAL */
#endif // _GNSDK_LOOKUP_LOCAL_HPP_
