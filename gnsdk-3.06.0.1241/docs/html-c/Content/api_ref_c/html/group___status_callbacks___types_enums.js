var group___status_callbacks___types_enums =
[
    [ "gnsdk_status_t", "group___status_callbacks___types_enums.html#ga02f47ba3efea3a0f3e79e69fb8a6ea29", [
      [ "gnsdk_status_unknown", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a2eeaf55721734eed5f6da26341cd8957", null ],
      [ "gnsdk_status_begin", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29aee425b378e7968ca0db4eb44be14b0ae", null ],
      [ "gnsdk_status_progress", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a3e103fb094fb1df580f27596b2aca713", null ],
      [ "gnsdk_status_complete", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a054b9ea657d5858e9f137bee043a2fca", null ],
      [ "gnsdk_status_error_info", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29aea4785fa3fe493b96a05f6547035ac58", null ],
      [ "gnsdk_status_connecting", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29ae7717c5b69ee4432787a65d5acb030fe", null ],
      [ "gnsdk_status_sending", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a8868aae740e5a4e2eda3ee902d8af987", null ],
      [ "gnsdk_status_receiving", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a895b442773e7495112122e90d0a770c1", null ],
      [ "gnsdk_status_disconnected", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a6ce9f1a3118fe8c762f807ab267e3d4a", null ],
      [ "gnsdk_status_reading", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a0c90e17fe91ffadeb3b7b32c83f59978", null ],
      [ "gnsdk_status_writing", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29afee5979d7a63fe5c691be63254cc8846", null ]
    ] ],
    [ "gnsdk_status_begin", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29aee425b378e7968ca0db4eb44be14b0ae", null ],
    [ "gnsdk_status_complete", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a054b9ea657d5858e9f137bee043a2fca", null ],
    [ "gnsdk_status_connecting", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29ae7717c5b69ee4432787a65d5acb030fe", null ],
    [ "gnsdk_status_disconnected", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a6ce9f1a3118fe8c762f807ab267e3d4a", null ],
    [ "gnsdk_status_error_info", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29aea4785fa3fe493b96a05f6547035ac58", null ],
    [ "gnsdk_status_progress", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a3e103fb094fb1df580f27596b2aca713", null ],
    [ "gnsdk_status_reading", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a0c90e17fe91ffadeb3b7b32c83f59978", null ],
    [ "gnsdk_status_receiving", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a895b442773e7495112122e90d0a770c1", null ],
    [ "gnsdk_status_sending", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a8868aae740e5a4e2eda3ee902d8af987", null ],
    [ "gnsdk_status_unknown", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29a2eeaf55721734eed5f6da26341cd8957", null ],
    [ "gnsdk_status_writing", "group___status_callbacks___types_enums.html#gga02f47ba3efea3a0f3e79e69fb8a6ea29afee5979d7a63fe5c691be63254cc8846", null ]
];