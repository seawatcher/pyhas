/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 *  Name: VideoContributorsLookup
 *  Description:
 *  This sample shows basic use of the findContributors() API
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license_path lib_path 
 */
package video_contributors_lookup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class VideoContributorsLookup {

	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupVideo(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}
	
	/* Find contributors for work */
	private static void findContributor(GnUser user, GnDataObject pWorkHandle)
			throws GnException {
		int count = 0;
		GnVideo mVideoID = new GnVideo(user);
		GnResponseContributor contribResponse = mVideoID
				.findContributors(pWorkHandle);
		GnContributorIterable contrib = contribResponse.contributors();
		GnContributorIterator gnContributorIterator = contrib.getIterator();
		System.out.println("\nContributors:");
		/* Find all contributors */
		while (gnContributorIterator.hasNext()) {
			GnNameIterable gnNameIterable = gnContributorIterator.next()
					.namesOfficial();
			GnNameIterator gnNameIterator = gnNameIterable.getIterator();
			while (gnNameIterator.hasNext()) {
				GnName contribName = gnNameIterator.next();
				System.out.println("\t" + ++count + " : "
						+ contribName.display());
			}
		}
	}

	/* Perform a video contributor lookup */
	private static void videoContributorsLookup(GnUser user) throws GnException {
		/* Convert serialized gdo into GnDataObject */
		String serialized_gdo = "WEcxAxpHl1mb/3LWaUoNklp7jGNb+JJ6JjfVejkh2RyiKi05lSEtfOeCNB5yvBRo4liQy9nzqL5HawtU90Sz7GcuetF6p+V0JpiB+lLSKydG5bhN2YYNqLzQdaJOUbIBnhTUdiXmAWuDh9FhGx9fgGKHH1Zjn+V7ff+DwxqK7lpY+EI243+xFw4kuePtsf+SkOKExQ==";

		GnVideo gnVideo = new GnVideo(user);
		
		/* Perform a sample album TUI query */
		GnDataObject dataObject = new GnDataObject(serialized_gdo);
		
		/* Find work with this serialized GDO */
		GnResponseVideoWork videoWorksResponse = gnVideo.findWorks(dataObject);
		
		GnVideoWorkIterable gnVideoWorkIterable = videoWorksResponse.works();
		GnVideoWorkIterator gnVideoWorkIterator = gnVideoWorkIterable.getIterator();
		
		while (gnVideoWorkIterator.hasNext()) {
			GnVideoWork work = gnVideoWorkIterator.next();
			GnTitle workTitle = work.officialTitle();
			
			System.out.println("\n\nTitle: " + workTitle.display());
			
			/* find contributors using workGDO */
			GnDataObject workGDO = new GnDataObject(work.serialize().toString());
			
			findContributor(user, workGDO);
		}
	}


	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				loadLocale(gnsdk, user);
			/* Sample Album creation and submission */
				videoContributorsLookup(user);
			} catch (GnException gnException) {
				System.out.println("\nError Code: " + gnException.getErrorCode()
						+ "\nError Message: " + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
