var a00036 =
[
    [ "GnException", "a00036.html#abb319ddf35d165083d039a7a69812d29", null ],
    [ "GnException", "a00036.html#ab9b896b0c51646ce9a7def9b99c7b1aa", null ],
    [ "GnException", "a00036.html#a3b8375b3ed85f98349f6c459c16c8676", null ],
    [ "delete", "a00036.html#ad3b645f749e9c6c4a71f2c6bf8262ad5", null ],
    [ "finalize", "a00036.html#a0511a9a6f48d8f04655755dc5f33416e", null ],
    [ "getErrorAPI", "a00036.html#af1065a80c6aa564bc13e143485d25e6d", null ],
    [ "getErrorCode", "a00036.html#a9ec03170c19b37782b1ece98422d67b8", null ],
    [ "getErrorDescription", "a00036.html#a0d4541ac3ba3f1380a9fbfb3af95d4b8", null ],
    [ "getErrorModule", "a00036.html#a186d35d8c981b6ee57fb065687b86071", null ],
    [ "getMessage", "a00036.html#aff599634337160f13375c5709199e5ae", null ],
    [ "getSourceErrorCode", "a00036.html#af7507143f7f5ca16bb875e4e1aa69af1", null ],
    [ "getSourceErrorModule", "a00036.html#a7f7ccdefc69337cc0973c136643c77e9", null ],
    [ "swigCMemOwn", "a00036.html#ab875d8b50b27f671316d6dccf14466a6", null ]
];