﻿/*
 *  Copyright (c) 2000-2013 Gracenote.
 *
 *  This software may not be used in any way or distributed without
 *  permission. All rights reserved.
 *
 *  Some code herein may be covered by US and international patents.
 */

/*
 *  Name: MusicID album identifier lookup sample application
 *  Description:
 *  This example looks up an Album using a TUI.
 *
 *  Command-line Syntax:
 *  sample clientId clientTag license gnsdkLibraryPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;

namespace Sample
{
    public class MusicIDLookupAlbumId
    {
        private static string localDB = "../../../sample_db";
        /*  Online vs Local queries
          *	Set to false to have the sample perform online queries.
          *  Set to true to have the sample perform local queries.
          *  For local queries, a Gracenote local database must be present.
          */
        private const bool useLocal = false;
		private static GnUser user = null;

        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";             

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class MusicIdEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.Write("\nPerforming MusicID Query ...\t");
                Console.Write("status (");
                switch (status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write(" Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write(" Begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write(" Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write(" Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write(" Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write(" Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write(" Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percentComplete + "),\tTotal Bytes Sent (" + bytesTotalSent + "),\tTotal Bytes Received (" + bytesTotalReceived + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /*Callback delegate called when loading locale*/
        public class LookupStatusEvents : GnStatusEventsDelegate
        {
            public static List<string> statusString = new List<string> { "Unknown", "Begin", "Progress", "Complete", "ErrorInfo", "Connecting", "Sending", "Recieving", "Disconnected", "Reading", "Writing" };

            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.WriteLine("\nLocale Load \tstatus :('" + statusString[Convert.ToInt32(status)] + "')" + "\tcomplete: " + percentComplete + "\tsent:" + bytesTotalSent +
                "\treceived:" + bytesTotalReceived);
            }

            public override bool cancel_check()
            {
                return false;
            }
        };

        /*
         *  Display local Gracenote DB information.
         */
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        static void LoadLocale(GnUser user)
        {
            using (LookupStatusEvents localeEvents = new LookupStatusEvents())
            {
                if (useLocal)
                {
                    GnLocale locale = new GnLocale(
                        GnSDK.kLocaleGroupMusic,            /* Locale group */
                        GnSDK.kLanguageEnglish,             /* Languae */
                        GnSDK.kRegionGlobal,                /* Region */
                        GnSDK.kDescriptorDefault,           /* Descriptor */
                        user,                               /* User */
                        null /*localeEvents*/               /* locale Events object */
                        );
                }
                else
                {
                    if (!File.Exists("serialized_locale.txt"))
                    {
                        GnLocale locale = new GnLocale(
                            GnSDK.kLocaleGroupMusic,            /* Locale group */
                            GnSDK.kLanguageEnglish,             /* Languae */
                            GnSDK.kRegionGlobal,                /* Region */
                            GnSDK.kDescriptorDefault,           /* Descriptor */
                            user,                               /* User */
                            null /*localeEvents*/               /* locale Events object */
                            );

                        /*Serialize locale, so we can use reuse it*/
                        if (!File.Exists("serialized_locale.txt"))
                        {
                            String serializedLocaleString = (locale.Serialize().ToString());
                            using (StreamWriter outfile = new StreamWriter("serialized_locale.txt"))
                            {
                                outfile.Write(serializedLocaleString);
                                outfile.Close();
                            }
                        }

                        return;
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader("serialized_locale.txt"))
                        {
                            String serializedLocaleString = sr.ReadToEnd();

                            GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                            return;
                        }

                    }
                }
            }

        }  

        #endregion

        private static uint DisplayForResolve(GnResponseAlbums response)
        {
            uint albumCount = 0;

            albumCount = response.Albums.count();

            Console.WriteLine("    Match count: " + albumCount);

            GnAlbumEnumerable albumEnumerable = response.Albums;
            GnAlbumEnumerator albumEnumerator = albumEnumerable.GetEnumerator();

            while (albumEnumerator.hasNext())
            {
                GnAlbum album = albumEnumerator.Current;
                /* Album Title */
                Console.WriteLine("          Title: " + album.Title.Display);
            }
            return 0;
        }

        private static void MusicIDLookupAlbumID(GnUser user, string id, string tag)
        {
            using (GnStatusEventsDelegate midEvents = new MusicIdEvents())
            {
                GnMusicID gnMusicID = new GnMusicID(user, null /*midEvents*/);

                try
                {
                    GnAlbum dataObject = new GnAlbum(id, tag);                    

                    /* Perform the Query */
                    Console.WriteLine("\n*****MusicID ID Query*****");
                    GnResponseAlbums gnResponse = gnMusicID.FindAlbums(dataObject);
                    if (gnResponse.Albums.count() == 0)
                    {
                        Console.WriteLine("\nNo albums found for the input.");
                    }
                    else
                    {
                        uint choice_ordinal = 0;
                        /* See if selection of one of the albums needs to happen */
                        if (gnResponse.NeedsDecision)
                        {
                            choice_ordinal = DisplayForResolve(gnResponse);
                        }
                        else
                        {
                            /* no need for disambiguation, we'll take the first album */
                            choice_ordinal = 0; /* Since iterator starts reading from 0th position in the list*/
                        }

                        GnAlbum album = gnResponse.Albums.at(choice_ordinal).next();

                        /* Is this a partial album? */
                        bool fullResult = album.FullResult();
                        if (!fullResult)
                        {
                            Console.WriteLine("retrieving FULL	RESULT");
                            /* do followup query to get full object. Setting the partial album as the query input. */
                            //GnDataObject dataobj = new GnDataObject(album);
                            gnResponse = gnMusicID.FindAlbums(album);

                            /* now our first album is the desired result with full data */
                            album = gnResponse.Albums.at(0).next();
                        }
                        if (album != null)
                        {
                            GnTitle iGnTitle = album.Title;
                            Console.WriteLine("    Final album:");
                            Console.WriteLine("          Title: " + iGnTitle.Display);
                        }
                    }

                }
                catch (GnException e)
                {
                    Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                    Console.WriteLine("Error API            :: " + e.ErrorAPI);
                    Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                    Console.WriteLine("SourceE rror Module  :: " + e.SourceErrorModule);
                }
            }
        }
        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }

            /* GNSDK initialization */
            try
            {
                // Initialize SDK
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                // Dispaly SDK version
                Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                // Enable logging
                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */

                /*         
                    *    Load existing user handle, or register new one.                    
                    */
                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

                if (useLocal)
                {
                    GnStorageSqlite gnStorage = new GnStorageSqlite();
                    gnStorage.StorageFolder = localDB;

                    GnLookupLocal gnLookupLocal = new GnLookupLocal();

                    user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                    DisplayEmbeddedDbInfo(gnLookupLocal);

                }
                else
                {
                    user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                }

                /* Set locale with desired Group, Language, Region and Descriptor
		        * Set the 'locale' to return locale-specifc results values. This examples loads an English locale.
		        */
                LoadLocale(user);

                MusicIDLookupAlbumID(user, "5154004", "0168B3134B141081DE907A15E792D4E0");

            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }
         
    }
}
