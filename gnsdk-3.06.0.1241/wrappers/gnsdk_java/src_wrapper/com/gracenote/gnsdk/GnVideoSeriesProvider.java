/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnVideoSeriesProvider {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected GnVideoSeriesProvider(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnVideoSeriesProvider obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnVideoSeriesProvider(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public GnVideoSeriesProvider(GnDataObject obj, String key) {
    this(gnsdk_javaJNI.new_GnVideoSeriesProvider(GnDataObject.getCPtr(obj), obj, key), true);
  }

  public GnVideoSeries getData(long pos) {
    return new GnVideoSeries(gnsdk_javaJNI.GnVideoSeriesProvider_getData(swigCPtr, this, pos), true);
  }

  public long count() {
    return gnsdk_javaJNI.GnVideoSeriesProvider_count(swigCPtr, this);
  }

}
