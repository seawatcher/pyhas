/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnTrack extends GnDataObject {
  private long swigCPtr;

  protected GnTrack(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnTrack_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnTrack obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnTrack(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnTrack() {
    this(gnsdk_javaJNI.new_GnTrack__SWIG_0(), true);
  }

  public GnTrack(String id, String id_tag) {
    this(gnsdk_javaJNI.new_GnTrack__SWIG_1(id, id_tag), true);
  }

  public boolean fullResult() {
    return gnsdk_javaJNI.GnTrack_fullResult(swigCPtr, this);
  }

  public GnTitle title() {
    return new GnTitle(gnsdk_javaJNI.GnTrack_title(swigCPtr, this), true);
  }

  public GnArtist artist() {
    return new GnArtist(gnsdk_javaJNI.GnTrack_artist(swigCPtr, this), true);
  }

  public GnCredit credit() {
    return new GnCredit(gnsdk_javaJNI.GnTrack_credit(swigCPtr, this), true);
  }

  public GnAudioWork work() {
    return new GnAudioWork(gnsdk_javaJNI.GnTrack_work(swigCPtr, this), true);
  }

  public GnMood mood() {
    return new GnMood(gnsdk_javaJNI.GnTrack_mood(swigCPtr, this), true);
  }

  public GnTempo tempo() {
    return new GnTempo(gnsdk_javaJNI.GnTrack_tempo(swigCPtr, this), true);
  }

  public GnGenre genre() {
    return new GnGenre(gnsdk_javaJNI.GnTrack_genre(swigCPtr, this), true);
  }

  public GnExternalIDIterable externalIDs() {
    return new GnExternalIDIterable(gnsdk_javaJNI.GnTrack_externalIDs(swigCPtr, this), true);
  }

  public String TUI() {
    return gnsdk_javaJNI.GnTrack_TUI(swigCPtr, this);
  }

  public String TUITag() {
    return gnsdk_javaJNI.GnTrack_TUITag(swigCPtr, this);
  }

  public String tagID() {
    return gnsdk_javaJNI.GnTrack_tagID(swigCPtr, this);
  }

  public String gnID() {
    return gnsdk_javaJNI.GnTrack_gnID(swigCPtr, this);
  }

  public String gnUID() {
    return gnsdk_javaJNI.GnTrack_gnUID(swigCPtr, this);
  }

  public String trackNumber() {
    return gnsdk_javaJNI.GnTrack_trackNumber(swigCPtr, this);
  }

  public String year() {
    return gnsdk_javaJNI.GnTrack_year(swigCPtr, this);
  }

  public String matchedIdent() {
    return gnsdk_javaJNI.GnTrack_matchedIdent(swigCPtr, this);
  }

  public String matchedFilename() {
    return gnsdk_javaJNI.GnTrack_matchedFilename(swigCPtr, this);
  }

  public String matchLookupType() {
    return gnsdk_javaJNI.GnTrack_matchLookupType(swigCPtr, this);
  }

  public String matchConfidence() {
    return gnsdk_javaJNI.GnTrack_matchConfidence(swigCPtr, this);
  }

  public int matchScore() {
    return gnsdk_javaJNI.GnTrack_matchScore(swigCPtr, this);
  }

  public GnTitle titleTLS() {
    return new GnTitle(gnsdk_javaJNI.GnTrack_titleTLS(swigCPtr, this), true);
  }

  public GnTitle titleRegional() {
    return new GnTitle(gnsdk_javaJNI.GnTrack_titleRegional(swigCPtr, this), true);
  }

}
