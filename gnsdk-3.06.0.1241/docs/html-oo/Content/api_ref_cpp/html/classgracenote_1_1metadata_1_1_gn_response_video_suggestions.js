var classgracenote_1_1metadata_1_1_gn_response_video_suggestions =
[
    [ "GnResponseVideoSuggestions", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a3a8080d047059120ff97ef38bb1db7e8", null ],
    [ "GnResponseVideoSuggestions", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a49ba17bd9534bbd113413a95f5e401ef", null ],
    [ "~GnResponseVideoSuggestions", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a8bccdf1cca207a3b2fb178aa765d107b", null ],
    [ "NeedsDecision", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a69da79eab2297b33390e42b9220ace35", null ],
    [ "RangeEnd", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a6384877fef3030407af3ccfc57020de1", null ],
    [ "RangeStart", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#aa5df76511a7d4045a5789e951d2fa8c1", null ],
    [ "RangeTotal", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a693a529651968ad1364703bc9d1a8802", null ],
    [ "ResultCount", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a59942706b1e8e6af2b63c9fdbe6461df", null ],
    [ "SuggestionText", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#a31f575c8ab2f4d526d5f1115d65d2c4d", null ],
    [ "SuggestionTitle", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#aa631da2374cab559868829ef288e08f1", null ],
    [ "SuggestionType", "classgracenote_1_1metadata_1_1_gn_response_video_suggestions.html#aeafd939f1f4de9027ed3a75a35b73557", null ]
];