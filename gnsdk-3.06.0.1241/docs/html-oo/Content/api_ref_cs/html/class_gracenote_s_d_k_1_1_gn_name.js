var class_gracenote_s_d_k_1_1_gn_name =
[
    [ "GnName", "class_gracenote_s_d_k_1_1_gn_name.html#adb2ab0061b1fd51ba0deaa80792a9570", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_name.html#aec1e288d15505e8f4aaa183628a23088", null ],
    [ "SortableScheme", "class_gracenote_s_d_k_1_1_gn_name.html#a5bc5f9c41cc4d9d6647efb30b00b03a6", null ],
    [ "Display", "class_gracenote_s_d_k_1_1_gn_name.html#ab998a85b29d20469a1e83316b6954e23", null ],
    [ "Family", "class_gracenote_s_d_k_1_1_gn_name.html#ab9a279e14b7114eba2b0c99d9cd886e9", null ],
    [ "Given", "class_gracenote_s_d_k_1_1_gn_name.html#a1e6d2a930a0d54d68ed2dc846f57ce6c", null ],
    [ "GlobalId", "class_gracenote_s_d_k_1_1_gn_name.html#a7fffccd1def4f09b58c1db2556d4cb4e", null ],
    [ "Language", "class_gracenote_s_d_k_1_1_gn_name.html#ace6a7ae7768790dc8eac45eba7065ed0", null ],
    [ "Prefix", "class_gracenote_s_d_k_1_1_gn_name.html#a1e458fae35019b4e240e027e8c6b0939", null ],
    [ "Sortable", "class_gracenote_s_d_k_1_1_gn_name.html#a21b37b7406f9225d305dfdf37cd63e20", null ]
];