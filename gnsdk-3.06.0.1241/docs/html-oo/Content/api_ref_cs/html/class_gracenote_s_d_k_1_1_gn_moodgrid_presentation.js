var class_gracenote_s_d_k_1_1_gn_moodgrid_presentation =
[
    [ "FilterConditionType", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a80f2c0e52a89b26e344c9ae7f89d83f2", [
      [ "kConditionTypeInclude", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a80f2c0e52a89b26e344c9ae7f89d83f2a69a583e8fd24eadd8cb34a988caf7925", null ],
      [ "kConditionTypeExclude", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a80f2c0e52a89b26e344c9ae7f89d83f2aa54de4ccef9749d161425c54974fea25", null ]
    ] ],
    [ "FilterListType", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#aa1d46ed113da10c80fe1649e1978851b", [
      [ "kListTypeGenre", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#aa1d46ed113da10c80fe1649e1978851ba115be457408ed3f56bf27e6002593b4d", null ],
      [ "kListTypeOrigins", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#aa1d46ed113da10c80fe1649e1978851bad6fccb553990596ae673d83dca083b8f", null ],
      [ "kListTypeEras", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#aa1d46ed113da10c80fe1649e1978851baa2b802ef3f90ea7fe6a5e9dc7486a512", null ]
    ] ],
    [ "GnMoodgridPresentation", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a87443c89eaa9a46fa5dbd3bf99d76851", null ],
    [ "AddFilter", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#adf2e276b31230b2df32d2af12ae74d04", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#acf4236e3cd824037546639aabeb70a0b", null ],
    [ "FindRecommendations", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a2b71572d28ebae601f6fab4faead59e9", null ],
    [ "FindRecommendationsEstimate", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a0b9adc8171e699251225cbb77f6bed93", null ],
    [ "MoodId", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a25a8bd8be7782624966c54c06f330cc4", null ],
    [ "MoodName", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a7ca2490f466b849e8ec013f5f8f9b030", null ],
    [ "RemoveAllFilters", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#ab9a546cba14ce1a347694f1224c47500", null ],
    [ "RemoveFilter", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a2390756ccc9da8c15a7d6f23e835f9fd", null ],
    [ "LayoutType", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a888608a170990f9e547982b6b2dd9006", null ],
    [ "Moods", "class_gracenote_s_d_k_1_1_gn_moodgrid_presentation.html#a84098ec563ab0d7ca7b8868ddabfc82c", null ]
];