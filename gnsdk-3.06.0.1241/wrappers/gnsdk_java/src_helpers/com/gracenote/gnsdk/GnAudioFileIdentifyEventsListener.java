package com.gracenote.gnsdk;

public interface GnAudioFileIdentifyEventsListener {

	public void status(GnMusicIDFileInfo fileinfo, GnMusicIdFileCallbackStatus midf_status, long current_file, long total_files);

	public boolean cancelCheck();

	public void resultAvailable(GnResponseAlbums album_result, long current_album, long total_albums);

	public void resultAvailable(GnResponseDataMatches matches_result, long current_album, long total_albums);

	public void resultNotFound(GnMusicIDFileInfo fileinfo, long current_file, long total_files);

	public void complete(long musicidfile_complete_error);

}
