/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _METADATA_HPP_
#define _METADATA_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "gnsdk_iterator_base.hpp"

namespace gracenote
{
	namespace metadata
	{
		/**
		 * Encapsulation of media elements and metadata delivered by GNSDK.
		 */
		class GnDataObject : public GnObject
		{
		public:
			GnDataObject(gnsdk_gdo_handle_t gdoHandle) throw ( GnError ) :GnObject(gdoHandle){}

            GnDataObject(gnsdk_cstr_t strId, gnsdk_cstr_t strIdTag, gnsdk_cstr_t strIdSrc) throw ( GnError )
			{
                gnsdk_gdo_handle_t handle = GNSDK_NULL;
				gnsdk_error_t error = gnsdk_manager_gdo_create_from_id(strId, strIdTag, strIdSrc, &handle);

				if(error) { throw GnError(); }
                this->Reset(handle);
			}

			GnDataObject(gnsdk_cstr_t strSerializedGdo) throw ( GnError )
			{
                gnsdk_gdo_handle_t handle = GNSDK_NULL;
				gnsdk_error_t error = gnsdk_manager_gdo_deserialize(strSerializedGdo, &handle);

				if(error) { throw GnError(); }
                this->Reset(handle);
			}

			template<typename gdoType>
			gdoType Reflect() const
			{
				return gdoType(get<gnsdk_gdo_handle_t>());
			}

			GnString Serialize() const throw ( GnError )
			{
				gnsdk_str_t   serialized = GNSDK_NULL;
				gnsdk_error_t error      = gnsdk_manager_gdo_serialize(get<gnsdk_gdo_handle_t>(), &serialized);

				if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

				GnString      retval(serialized);
				error = gnsdk_manager_string_free(serialized);
				if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
				return retval;
			}

			/**
			 * Returns media or metadata value as a string based on the provided key and ordinal.
			 * @param value_key     Key of the value to return
			 * @param ordinal		1-based specifier of a value where multiple values for a key exist
			 */
			gnsdk_cstr_t StringValue(gnsdk_cstr_t strValueKey, gnsdk_uint32_t ordinal = 1) const throw ( GnError )
			{
				gnsdk_cstr_t  sz_value = GNSDK_NULL;
				gnsdk_error_t error;

				if(get<gnsdk_gdo_handle_t>())
				{
					error = gnsdk_manager_gdo_value_get(get<gnsdk_gdo_handle_t>(), strValueKey, ordinal, &sz_value);
					if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
				}
				return sz_value;
			}

			/**
			 * Retrieves the number of children of a given key that are available.
			 * @param child_key [in] Child key to count
			 *
			 * <p><b>Remarks:</b></p>
			 * Use this function to count children of a specific data instance; note that only those children
			 * accessible in the current type are considered.
			 *
			 * When this function successfully counts zero (0) occurrences of the child key with no errors, the
			 * method returns successfully.
			 */
			gnsdk_uint32_t ChildCount(gnsdk_cstr_t strChildKey) const throw ( GnError )
			{
				gnsdk_uint32_t count = 0;
				gnsdk_error_t  error;

				if(get<gnsdk_gdo_handle_t>())
				{
					error = gnsdk_manager_gdo_child_count(get<gnsdk_gdo_handle_t>(), strChildKey, &count);
					if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
				}
				return count;
			}

			/**
			 * Retrieves a child data object specified by the key and ordinal
			 * @param child_key     Child key to return
			 * @param child_ordinal		1-based specifier of a child where multiple values for a key exist
			 *
			 * <p><b>Remarks:</b></p>
			 * Use this function to count children of a specific data instance; note that only those children
			 * accessible in the current type are considered.
			 */
			template<typename gdoType>
			gdoType ChildGet(gnsdk_cstr_t strChildKey, gnsdk_uint32_t childOrdinal = 1) const throw ( GnError )
			{
				gnsdk_gdo_handle_t h_gdo_child = GNSDK_NULL;
				gnsdk_error_t      error;

				if(get<gnsdk_gdo_handle_t>())
				{
					error = gnsdk_manager_gdo_child_get(get<gnsdk_gdo_handle_t>(), strChildKey, childOrdinal, &h_gdo_child);
					if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
				}

				gdoType tmp = gdoType(h_gdo_child);

				gnsdk_manager_gdo_release(h_gdo_child);

				return tmp;
			}

			/**
			 * Renders contents of the data object as an XML string.
			 */
			GnString RenderToXML() const throw ( GnError )
			{
				gnsdk_str_t   sz_xml = GNSDK_NULL;
				gnsdk_error_t error;

				if(get<gnsdk_gdo_handle_t>())
				{
					error = gnsdk_manager_gdo_render(get<gnsdk_gdo_handle_t>(), 0, &sz_xml);
					if(error) { throw GnError(); }
				}
				GnString retval(sz_xml);
				error = gnsdk_manager_string_free(sz_xml);
				if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
				return retval;
			}

			virtual bool operator == (const GnDataObject& rhs) const
			{
				if(get<gnsdk_gdo_handle_t>() == rhs.get<gnsdk_gdo_handle_t>())
				{
					return true;
				}
				return false;
			}

			virtual bool operator != (const GnDataObject& rhs) const
			{
				return !( *this == rhs );
			}

			/**
			 * Returns the native handle for the data object.
			 */
			gnsdk_gdo_handle_t native() const
			{
				return get<gnsdk_gdo_handle_t>();
			}

			/**
			 * Applies lists to use for retrieving and rendering locale-related values.
			 * @param	locale Locale to apply to this data object
			 * <p><b>Remarks:</b></p>
			 * Use this function to set the locale of retrieved and rendered locale-dependent data for this
			 * data object. This function overrides any applicable global locale defaults set elsewhere.
			 * <p><b>Locale Language Support</b></p>
			 * This function supports all locale languages and successfully assigns a locale for this data object.
			 * The locale is used for future calls to get data values when locale-dependent values are requested.
			 *
			 * When list-based locale-specific values are requested from this data object they are returned
			 * only if an applicable locale has been successfully set by this method or a default locale has been
			 * successfully set elsehwere.
			 *
			 * When a non-list-based locale-specific values are requested from this data object they are returned
			 * only if these values are available from Gracenote Service or the locale database (where online queries
			 * are not accessible or permitted). If not, the application uses the default (or "official") locale data for
			 * these values. For example, plot values are non-list-based. If a plot summary value is available only in the
			 * English language, and the specific locale is defined for the Spanish language, the application
			 * displays the plot summary in the English language.
			 */
			void LocaleSet(GnLocale& locale) const
			{
				gnsdk_error_t error;

				error = gnsdk_manager_gdo_set_locale(get<gnsdk_gdo_handle_t>(), locale.native() );
				if(error) { throw GnError(); }
			}

			/**
			 * Retrieves the type of this data object.
			 * <p><b>Remarks:</b></p>
			 * The data object's contents are not clearly defined. Use this API to retrieve a data object's type,
			 * as this enables the application to more accurately determine what data the specific data it contains.
			 * Typically an application will use data objects subclassed from the data object, which do specifically
			 * define a type.
			 */
			gnsdk_cstr_t GetType() const
			{
				gnsdk_cstr_t  response_context = GNSDK_NULL;
				gnsdk_error_t error;

				if(get<gnsdk_gdo_handle_t>())
				{
					error = gnsdk_manager_gdo_get_type(get<gnsdk_gdo_handle_t>(), &response_context);
					if(error) { throw GnError(); }
				}
				return response_context;
			}

			bool IsType(gnsdk_cstr_t strType) const
			{
				gnsdk_cstr_t gdoType = GetType();

				if( ( strType == NULL ) || ( gdoType == NULL ) )
				{
					return false;
				}
				return gnstd::gn_strcmp(strType, gdoType) == 0;
			}
	};

		template<typename _GdoType>
		class gn_gdo_provider
		{
		public:
			gn_gdo_provider(const GnDataObject& obj, gnsdk_cstr_t key) : obj_(obj), key_(key) { }
			~gn_gdo_provider() { }

			// required
			_GdoType get_data(gnsdk_uint32_t pos)
			{
				return obj_.template ChildGet<_GdoType>(key_, pos);
			}

			// optional
			gnsdk_uint32_t              count() { return obj_.ChildCount(key_); }
			static const gnsdk_uint32_t kOrdinalStart = 1;
			static const gnsdk_uint32_t kCountOffset  = 1;

		private:
			GnDataObject                obj_;
			gnsdk_cstr_t                key_;
		};

		class GnName;
		class GnExternalID;

		typedef gn_facade_range_iterator<GnName, gn_gdo_provider<GnName> >               name_iterator;
		typedef gn_facade_range_iterator<GnExternalID, gn_gdo_provider<GnExternalID> >   externalid_iterator;

		// typedef gn_facade_range_iterator<GnContents, gn_gdo_provider<GnContents> >	contents_iterator;

		/**
		 * Language
		 */
		class GnLanguage : public GnDataObject
		{
		public:
			GnLanguage() : GnDataObject(GNSDK_NULL_GDO) { }
			explicit GnLanguage(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnLanguage()  { }

			/**
			 * Retrieves this language's display string, such as "English".
			 */
			gnsdk_cstr_t Display() const
			{
				return StringValue(GNSDK_GDO_VALUE_DISPLAY_LANGUAGE_DISPLAY);
			}

			/**
			 * Retrieves this language's code, such as "eng".
			 * <b>Note:</b> Display language codes are not available for track titles.
			 */
			gnsdk_cstr_t Code() const
			{
				return StringValue(GNSDK_GDO_VALUE_DISPLAY_LANGUAGE);
			}
		};

		/**
		 * Third-party identifier used to match identified media to merchandise IDs in online
		 * stores and other services
		 */
		class GnExternalID : public GnDataObject
		{
		public:
			GnExternalID() : GnDataObject(GNSDK_NULL_GDO) { }
			GnExternalID(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnExternalID()  { }

			/**
			 *  Retrieves this external ID source
			 */
			gnsdk_cstr_t Source() const
			{
				return StringValue(GNSDK_GDO_VALUE_EXTERNALID_SOURCE);
			}

			/**
			 *  Retrieves this external ID type
			 */
			gnsdk_cstr_t Type() const
			{
				return StringValue(GNSDK_GDO_VALUE_EXTERNALID_TYPE);
			}

			/**
			 *  Retrieves this external ID value
			 */
			gnsdk_cstr_t Value() const
			{
				return StringValue(GNSDK_GDO_VALUE_EXTERNALID_VALUE);
			}
		};

		class GnSortable : public GnDataObject
		{
		public:
			GnSortable() : GnDataObject(GNSDK_NULL_GDO) { }
			GnSortable(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle)    { }

			virtual ~GnSortable() { }

			/**
			 *  Retrieves this sortable's display value.
			 */
			gnsdk_cstr_t Display() const
			{
				return StringValue(GNSDK_GDO_VALUE_DISPLAY);
			}

			/**
			 *  Retrieves this sortable's language
			 */
			GnLanguage Language() const
			{
				return Reflect<GnLanguage>();
			}
		};

		/**
		 * Name
		 */
		class GnName : public GnDataObject
		{
		public:
			GnName() : GnDataObject(GNSDK_NULL_GDO) { }
			GnName(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle)    { }

			virtual ~GnName()  { }

			GnSortable Sortable() const
			{
				return Reflect<GnSortable>();
			}

			GnLanguage Language() const
			{
				return Reflect<GnLanguage>();
			}

			gnsdk_cstr_t Display() const
			{
				return StringValue(GNSDK_GDO_VALUE_DISPLAY);
			}

			gnsdk_cstr_t SortableScheme() const
			{
				return StringValue(GNSDK_GDO_VALUE_SORTABLE_SCHEME);
			}

			gnsdk_cstr_t Prefix() const
			{
				return StringValue(GNSDK_GDO_VALUE_PREFIX);
			}

			gnsdk_cstr_t Family() const
			{
				return StringValue(GNSDK_GDO_VALUE_FAMILY);
			}

			gnsdk_cstr_t Given() const
			{
				return StringValue(GNSDK_GDO_VALUE_GIVEN);
			}

			gnsdk_cstr_t GlobalId() const
			{
				return StringValue(GNSDK_GDO_VALUE_GLOBALID);
			}
		};

		/**
		 * Title
		 */
		class GnTitle : public GnDataObject
		{
		public:
			GnTitle() : GnDataObject(GNSDK_NULL_GDO) { }
			GnTitle(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnTitle() { }

			GnSortable Sortable() const
			{
				return Reflect<GnSortable>();
			}

			GnLanguage Language() const
			{
				return Reflect<GnLanguage>();
			}

			gnsdk_cstr_t Display() const
			{
				return StringValue(GNSDK_GDO_VALUE_DISPLAY);
			}

			gnsdk_cstr_t Prefix() const
			{
				return StringValue(GNSDK_GDO_VALUE_PREFIX);
			}

			gnsdk_cstr_t SortableScheme() const
			{
				return StringValue(GNSDK_GDO_VALUE_SORTABLE_SCHEME);
			}

			gnsdk_cstr_t MainTitle() const
			{
				return StringValue(GNSDK_GDO_VALUE_MAIN_TITLE);
			}

			gnsdk_cstr_t Edition() const
			{
				return StringValue(GNSDK_GDO_VALUE_EDITION);
			}
		};
	} // namespace metadata
}     // namespace gracenote
#endif // _METADATA_HPP_
