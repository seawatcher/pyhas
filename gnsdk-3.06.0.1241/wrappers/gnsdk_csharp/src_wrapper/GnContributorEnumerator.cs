/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnContributorEnumerator : System.Collections.Generic.IEnumerator<GnContributor>, IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnContributorEnumerator(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnContributorEnumerator obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnContributorEnumerator() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnContributorEnumerator(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

			public bool
			MoveNext( )
			{
				return hasNext( );
			}

			public GnContributor Current {
				get {
					return next( );
				}
			}
			object System.Collections.IEnumerator.Current {
				get {
					return Current;
				}
			}
			public void
			Reset( )
			{
			}

		
  public GnContributor next() {
    GnContributor ret = new GnContributor(gnsdk_csharp_marshalPINVOKE.GnContributorEnumerator_next(swigCPtr), true);
    return ret;
  }

  public bool hasNext() {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnContributorEnumerator_hasNext(swigCPtr);
    return ret;
  }

  public uint distance(GnContributorEnumerator itr) {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnContributorEnumerator_distance(swigCPtr, GnContributorEnumerator.getCPtr(itr));
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public GnContributorEnumerator(SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t provider, uint pos, uint count) : this(gnsdk_csharp_marshalPINVOKE.new_GnContributorEnumerator__SWIG_0(SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t.getCPtr(provider), pos, count), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnContributorEnumerator(SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t provider, uint pos, uint count, GnContributor start) : this(gnsdk_csharp_marshalPINVOKE.new_GnContributorEnumerator__SWIG_1(SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t.getCPtr(provider), pos, count, GnContributor.getCPtr(start)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
