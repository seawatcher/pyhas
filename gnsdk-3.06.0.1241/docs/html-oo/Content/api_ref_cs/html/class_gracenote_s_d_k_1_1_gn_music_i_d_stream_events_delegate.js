var class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate =
[
    [ "GnMusicIDStreamEventsDelegate", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a7cc2e3d74f210429b9b1c4c3e11b586e", null ],
    [ "cancel_identify", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a84a9a8214b825c1be28724448a7d2dfd", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#adeff618c581fa824a7f5755a08f1545c", null ],
    [ "midstream_status_event", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#af9e02151c7e72a629683b7037d71c8c6", null ],
    [ "result_available", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#afaa2e86914c23826d0897414551d720c", null ],
    [ "status_event", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#aa534f56722a29c4a7892c864ce416909", null ],
    [ "SwigDelegateGnMusicIDStreamEventsDelegate_0", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a10e5a9c56da64025d4bec7c2980eb4f6", null ],
    [ "SwigDelegateGnMusicIDStreamEventsDelegate_1", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#aec90686e0c05a780d92080f2877e9eae", null ],
    [ "SwigDelegateGnMusicIDStreamEventsDelegate_2", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a933aa5613a604493c082da716c4ee585", null ],
    [ "SwigDelegateGnMusicIDStreamEventsDelegate_3", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a4dd4d04610338289dab5b6a03abbd282", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1_gn_music_i_d_stream_events_delegate.html#a22219e0e4f4e80be2d82e020b42edd71", null ]
];