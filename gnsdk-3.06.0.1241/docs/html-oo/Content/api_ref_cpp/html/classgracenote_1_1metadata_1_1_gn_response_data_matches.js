var classgracenote_1_1metadata_1_1_gn_response_data_matches =
[
    [ "GnResponseDataMatches", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a5dcbd8d511ba8931b3480f8f1399dc28", null ],
    [ "GnResponseDataMatches", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#ab12dc70de15f8aa42c44416be444d2cc", null ],
    [ "~GnResponseDataMatches", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#af86718ea5a503178d5b10102c5e65b66", null ],
    [ "DataMatches", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#ad424229ea44236d4fd8a7da9e91adae6", null ],
    [ "NeedsDecision", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a70602e271e37888814786c0e3e9cd554", null ],
    [ "RangeEnd", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a9b04611415b150c2911bed745334435b", null ],
    [ "RangeStart", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a22e441c2e9649e841f655e840d9ff813", null ],
    [ "RangeTotal", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a7a26563f67621dc13c87fa68e98fcbad", null ],
    [ "ResultCount", "classgracenote_1_1metadata_1_1_gn_response_data_matches.html#a1d7981efd15cc052a3e09da3e7f9eb57", null ]
];