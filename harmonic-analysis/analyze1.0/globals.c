 /***************************************************************************/
 /*                                                                         */
 /*       Copyright (C) 1996  Daniel Sleator and David Temperley            */
 /*  See file "README" for information about commercial use of this system  */
 /*                                                                         */
 /***************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "analyze.h"

FILE * instream;
int N_notes;
int N_chords;

Note ** note_array;
/* This is an array of pointers to notes.
   it allows us to find the note itself from
   the number of the note */

Column * column_table;

int table_size;  /* the size of the hash tables */

int baseunit = -1;  /* number of milliseconds in the basic beat */
                    /* -1 indicates it has not been initialized */

Beatlevel beatlevel[MAX_BEAT_LEVEL+1];
int N_beatlevel;

double alpha;  /* exp(-alpha * deltat) is .5 after the half life.
		  when deltat is written in milliseconds */

struct side_effect_struct side_effect;
   /* this structure contains the results returned by tpc_choice_score() */


/* settable parameters of the algorithm */

double tpc_variance_scale_factor = .1;
   /* the scale factor for the tpc variance for combining with other costs */

double buckets_per_unit_of_cog = 5.;
   /* discretize cog to this level */

double half_life = 2.0;
   /* In this many seconds, the relevance of a note to the current note
      goes down by half. */

double pruning_cutoff = 30.0;
   /* Partial solutions with a current score worse than the optimal
      by more than this are thrown away. */

int verbosity = 1;
   /* 0 = just the graphical analysis
      1 = above plus note by note feedback
      2 = above plus dump of all data at the end plus beatlevel info at start
      3 = above plus last chord list plus scores chord by chord
      4 = above plus orn dis penalty values plus all chord lists plus masses
    */
