#
# GNSDK Build System: Platform Variable Setup
#

#
# TARGET PLATFORM: Windows Phone 8 32/64-bit
#


ifneq ($(GNSDK_BUILD_PLATFORM),windows)
$(error Building MSWindows target is currently only supported on Windows clients)
endif


SHARED_LIB_EXT = dll
STATIC_LIB_EXT = lib
SHARED_LINK_EXT = $(STATIC_LIB_EXT)
STATIC_LINK_EXT = $(STATIC_LIB_EXT)

APP_EXE_PATTERN := %.exe
SHARED_LIB_PATTERN := group_library.$(SHARED_LIB_EXT)
STATIC_LIB_PATTERN := group_library.$(STATIC_LIB_EXT)
SHARED_LINK_PATTERN := group_library.$(STATIC_LIB_EXT)
STATIC_LINK_PATTERN := group_library.$(STATIC_LIB_EXT)

SHARED_LIB_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(SHARED_LIB_PATTERN)))
STATIC_LIB_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(STATIC_LIB_PATTERN)))
SHARED_LINK_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(SHARED_LINK_PATTERN)))
STATIC_LINK_TARGET = $(subst group,$(group_name),$(subst library,$(library_name),$(STATIC_LINK_PATTERN)))


#
# Tool Chain variables
#

TOOLDIR=

# Compilation
CC=$(TOOLDIR)/cl.exe
CCPP=$(CC)
COUTFLAG=-Fo
CWARNERR=-WX
PDB_NAME=$(group_name)_$(library_name)
CDEFS = -DUNICODE -D_UNICODE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -DWINAPI_FAMILY=WINAPI_FAMILY_PHONE_APP -DGCSL_STRICT_HANDLES -DGNSDK_STRICT_HANDLES


ifneq ($(filter thread time process socket random crypt,$(library_name)),)
	CFLAGS_DEBUG=-Od -MDd -J -W4 -GS -GF -RTC1 -nologo -Zi -ZW -TP -EHsc -Fd$(PDB_NAME) -D_DEBUG $(CDEFS) /FU"Platform.winmd"
	CFLAGS_RELEASE=-O2 -MD -J -W4 -GS -GF -Zi -ZW -TP -EHsc -Fd$(PDB_NAME) -DNDEBUG $(CDEFS) /FU"Platform.winmd"
else
	CFLAGS_DEBUG=-Od -MDd -J -W4 -GS -GF -RTC1 -nologo -Zi -EHsc -Fd$(PDB_NAME) -D_DEBUG $(CDEFS) /FU"Platform.winmd"
	CFLAGS_RELEASE=-O2 -MD -J -W4 -GS -GF -Zi -EHsc -Fd$(PDB_NAME) -DNDEBUG $(CDEFS)  /FU"Platform.winmd"
endif

LD=$(TOOLDIR)/link.exe
LDOUTFLAG=-OUT:
LDFLAGS_DEBUG=-NOLOGO -DEBUG -DLL -DEF:$(group_name)_exports.def -INCREMENTAL:NO -OPT:REF -OPT:ICF -DYNAMICBASE -NXCOMPAT -NODEFAULTLIB:libcmtd.lib
LDFLAGS_RELEASE=-NOLOGO -DLL -DEF:$(group_name)_exports.def -RELEASE -OPT:REF -OPT:ICF -DYNAMICBASE -NXCOMPAT -NODEFAULTLIB:libcmt.lib
LDLIBS_PLATFORM=ws2_32.lib PhoneAppModelHost.lib WindowsPhoneCore.lib RuntimeObject.lib

ifeq ($(IMPLARCH),x86-32)
LDFLAGS_DEBUG+= -SAFESEH 
LDFLAGS_RELEASE+= -SAFESEH
endif

ifeq ($(IMPLARCH),x86-64)
LDFLAGS_DEBUG+= -SAFESEH 
LDFLAGS_RELEASE+= -SAFESEH
endif

# static library building
LDS=$(TOOLDIR)/lib.exe
LDSOUTFLAG=-OUT:
LDSFLAGS_DEBUG=-NOLOGO /force:allowzwobj
LDSFLAGS_RELEASE=-NOLOGO /force:allowzwobj

# application linking
LINK=$(TOOLDIR)/link.exe
LINKOUTFLAG=-OUT:
LINKFLAGS_DEBUG=-NOLOGO -DEBUG -SUBSYSTEM:CONSOLE -NODEFAULTLIB:libcmtd.lib
LINKFLAGS_RELEASE=-NOLOGO -RELEASE -SUBSYSTEM:CONSOLE -NODEFAULTLIB:libcmt.lib

# resource compiling
RC=rc.exe
RCOUTFLAG=-fo
RCFLAGS_DEBUG=-d_DEBUG
RCFLAGS_RELEASE=

# Other Tools
AR=
STRIP=
CP=cp -fpu
MV=mv -f

# CSharp
CSHARP_CC=csc
CSHARP_OUTFLAG=-OUT:
CSHARP_FLAGS=/noconfig /nowarn:1701,1702,2008 /nostdlib+ /errorreport:prompt /warn:4 /define:TRACE /define:WINDOWS_PHONE /errorendlocation /preferreduilang:en-US /errorendlocation /preferreduilang:en-US /debug:pdbonly /optimize+ /utf8output 
CSHARP_REFERENCES=/reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.CSharp.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Devices.Sensors.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Phone.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Phone.Interop.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Phone.Maps.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Phone.Reactive.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Xna.Framework.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Xna.Framework.GamerServices.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Xna.Framework.GamerServicesExtensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Xna.Framework.Input.Touch.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\Microsoft.Xna.Framework.MediaLibraryExtensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\mscorlib.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\mscorlib.extensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Collections.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ComponentModel.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ComponentModel.EventBasedAsync.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Core.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Data.Linq.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Device.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Diagnostics.Contracts.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Diagnostics.Debug.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Diagnostics.Tools.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Dynamic.Runtime.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Globalization.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.IO.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Linq.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Linq.Expressions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Linq.Queryable.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Net.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Net.NetworkInformation.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Net.Primitives.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Net.Requests.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ObjectModel.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Observable.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Reflection.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Reflection.Emit.IlGeneration.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Reflection.Emit.Lightweight.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Reflection.Extensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Reflection.Primitives.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Resources.ResourceManager.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.Extensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.InteropServices.WindowsRuntime.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.Serialization.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.Serialization.Json.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.Serialization.Primitives.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.Serialization.Xml.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Runtime.WindowsRuntime.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Security.Principal.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ServiceModel.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ServiceModel.Http.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ServiceModel.Primitives.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ServiceModel.Security.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.ServiceModel.Web.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Text.Encoding.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Text.Encoding.Extensions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Text.RegularExpressions.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Threading.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Threading.Tasks.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Windows.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.Linq.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.ReaderWriter.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.Serialization.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.XDocument.dll" /reference:"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\WindowsPhone\v8.0\System.Xml.XmlSerializer.dll" /reference:"C:\Program Files (x86)\Windows Phone Kits\8.0\\Windows Metadata\Windows.winmd"




ifeq ($(ARCH),winphone8)
	# detect 32/64-bit compiler (since arch is driven by compiler, not flags)
	ccver := $(shell cl.exe 2>&1)
	ifneq ($(findstring x86,$(ccver)),)
		IMPLDIR = winphone8
		IMPLARCH = x86-32
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter i386 x86_32 x86-32 x86,$(ARCH)),)
				IMPLARCH = x86-32
			endif
		endif
	else ifneq ($(findstring x64,$(ccver)),)
		IMPLDIR = winphone8
		IMPLARCH = x86-64
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin/amd64"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter x86_64 x86-64 x64,$(ARCH)),)
				IMPLARCH = x86-64
			endif
		endif
	else ifneq ($(findstring ARM,$(ccver)),)
		IMPLDIR = winphone8
		IMPLARCH = arm-32
		TOOLDIR := "$(shell cygpath -u $$VCINSTALLDIR)/bin/x86_arm"
		TOOLDIR := $(TOOLDIR:%/=%)
		ifneq ($(ARCH),)
			ifeq ($(filter arm_32 arm-32 arm,$(ARCH)),)
				IMPLARCH = arm-32
			endif
		endif
	endif	
endif

ifeq ($(IMPLARCH),)
$(error ARCH="$(IMPLARCH)" is unsupported. Windows can't cross compile (use different vcvars.bat setting))
endif

# set compiler version (needed for Windows static lib linking)
ifneq ($(findstring Version 17,$(ccver)),)
	COMPILER_VER=vs12
else
	$(error unsupported compiler version - install windows8 and visual studio 11)
endif

