/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* musicmetadata.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */
#include "gnsdk.hpp"
#include "metadata.hpp"
#include "metadata_music.hpp"

#if GNSDK_MUSICID

using namespace gracenote;
using namespace gracenote::musicid;

/******************************************************************************
** GnMusicID
*/
GnMusicID::GnMusicID(const GnUser& user, GnStatusEvents* pEventHandler)  throw ( GnError ) :
	mEventHandler(GNSDK_NULL)
{
	gnsdk_error_t error;

	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_DSP);
	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_MUSICID);

	mEventHandler = pEventHandler;

	error = gnsdk_musicid_query_create(user.native(), _callback_status, this, &mQueryHandle);
	if(error) { throw GnError(); }
}

GnMusicID::~GnMusicID()
{
	gnsdk_musicid_query_release(mQueryHandle);
}

/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t GnMusicID::Version()
{
	return gnsdk_musicid_get_version();
}

/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t GnMusicID::BuildDate()
{
	return gnsdk_musicid_get_build_date();
}

/*-----------------------------------------------------------------------------
 *  OptionLookupMode
 */
void GnMusicID::OptionLookupMode(GnLookupMode lookupMode) throw ( GnError )
{
	gnsdk_error_t error;

	switch(lookupMode)
	{
	case kLookupModeLocal:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_LOCAL);
		break;

	case kLookupModeOnline:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE);
		break;

	case kLookupModeOnlineNoCache:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHE);
		break;

	case kLookupModeOnlineNoCacheRead:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_NOCACHEREAD);
		break;

	case kLookupModeOnlineCacheOnly:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_LOOKUP_MODE, GNSDK_LOOKUP_MODE_ONLINE_CACHEONLY);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionLookupData
 */
void GnMusicID::OptionLookupData(GnLookupData lookupData, bool bEnable) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_enable;

	if(bEnable)
	{
		sz_enable = GNSDK_VALUE_TRUE;
	}
	else
	{
		sz_enable = GNSDK_VALUE_FALSE;
	}

	switch(lookupData)
	{
	case kLookupDataContent:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_CONTENT_DATA, sz_enable);
		break;

	case kLookupDataClassical:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_CLASSICAL_DATA, sz_enable);
		break;

	case kLookupDataSonicData:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_SONIC_DATA, sz_enable);
		break;

	case kLookupDataPlaylist:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_PLAYLIST, sz_enable);
		break;

	case kLookupDataExternalIDs:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_EXTERNAL_IDS, sz_enable);
		break;

	case kLookupDataGlobalIDs:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ENABLE_GLOBALIDS, sz_enable);
		break;

	case kLookupDataAdditionalCredits:
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_ADDITIONAL_CREDITS, sz_enable);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionSet
 */
void GnMusicID::OptionSet(gnsdk_cstr_t strLookupOption, bool bEnable) throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  sz_enable;

	if(bEnable)
	{
		sz_enable = GNSDK_VALUE_TRUE;
	}
	else
	{
		sz_enable = GNSDK_VALUE_FALSE;
	}

	error = gnsdk_musicid_query_option_set(mQueryHandle, strLookupOption, sz_enable);
	if(error)
	{
		throw GnError();
	}
}

/*-----------------------------------------------------------------------------
 *  OptionPreferredLanguage
 */
void GnMusicID::OptionPreferredLanguage(gnsdk_cstr_t strLanguage) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_PREFERRED_LANG, strLanguage);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionPreferredExternalID
 */
void
GnMusicID::OptionPreferredExternalID(gnsdk_cstr_t strExternalId) throw ( GnError )
{
	gnsdk_error_t error;


	error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_PREFER_XID, strExternalId);
	if ( error ) { throw GnError( ); }
}

/*-----------------------------------------------------------------------------
 *  OptionResultPreferCoverart
 */
void GnMusicID::OptionPreferredCoverart(gnsdk_cstr_t strCoverart) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_PREFER_COVERART, strCoverart);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionResultSingle
 */
void GnMusicID::OptionResultSingle(bool bEnable) throw ( GnError )
{
	gnsdk_error_t error;

	if(bEnable)
	{
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_SINGLE, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_SINGLE, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionRevisionCheck
 */
void GnMusicID::OptionRevisionCheck(bool bEnable) throw ( GnError )
{
	gnsdk_error_t error;

	if(bEnable)
	{
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_REVISION_CHECK, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_REVISION_CHECK, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionRangeStart
 */
void GnMusicID::OptionRangeStart(gnsdk_uint32_t rangeStart) throw ( GnError )
{
	gnsdk_error_t error;
	char buffer[11];
	gnstd::gn_itoa(buffer, 11, rangeStart);

	error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_RANGE_START, buffer);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionRangeSize
 */
void GnMusicID::OptionRangeSize(gnsdk_uint32_t rangeSize) throw ( GnError )
{
	gnsdk_error_t error;
	char buffer[11];
	gnstd::gn_itoa(buffer, 11, rangeSize);
	error = gnsdk_musicid_query_option_set(mQueryHandle, GNSDK_MUSICID_OPTION_RESULT_RANGE_SIZE, buffer);
	if(error) { throw GnError(); }
}






/*-----------------------------------------------------------------------------
 *  FingerprintDataGet
 */
gnsdk_cstr_t GnMusicID::FingerprintDataGet() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  str = GNSDK_NULL;

	error = gnsdk_musicid_query_get_fp_data(mQueryHandle, &str);
	if(error) { throw GnError(); }

	return str;
}

/*-----------------------------------------------------------------------------
 *  FingerprintBegin
 */
void GnMusicID::FingerprintBegin(GnFingerprintType fpType, gnsdk_uint32_t audio_sample_rate, gnsdk_uint32_t audio_sample_size, gnsdk_uint32_t audio_channels)
throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_fingerprint_begin(mQueryHandle, _MapfPTypeCStr(fpType), audio_sample_rate, audio_sample_size, audio_channels);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FingerprintWrite
 */
bool GnMusicID::FingerprintWrite(const gnsdk_byte_t* audio_data, gnsdk_size_t audio_data_size) throw ( GnError )
{
	gnsdk_bool_t  b_complete = GNSDK_FALSE;
	gnsdk_error_t error;

	error = gnsdk_musicid_query_fingerprint_write(mQueryHandle, audio_data, audio_data_size, &b_complete);
	if(error) { throw GnError(); }

	if(b_complete)
	{
		return true;
	}
	return false;
}

/*-----------------------------------------------------------------------------
 *  FingerprintEnd
 */
void GnMusicID::FingerprintEnd() throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_fingerprint_end(mQueryHandle);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FingerprintFromSource
 */
void GnMusicID::FingerprintFromSource(GnAudioSource& audioSource, GnFingerprintType fpType) throw ( GnError )
{
	gnsdk_byte_t  audio_buffer[1024];
	gnsdk_size_t  audio_data_size;
	gnsdk_bool_t  b_complete;
	gnsdk_error_t error;

	error = audioSource.SourceInit();
	if(!error)
	{
		error = gnsdk_musicid_query_fingerprint_begin(mQueryHandle, _MapfPTypeCStr(fpType), audioSource.SamplesPerSecond(), audioSource.SampleSizeInBits(), audioSource.NumberOfChannels() );
		if(!error)
		{
			b_complete = GNSDK_FALSE;

			while(0 < ( audio_data_size = audioSource.GetData(audio_buffer, sizeof( audio_buffer ) ) ) )
			{
				error = gnsdk_musicid_query_fingerprint_write(mQueryHandle, audio_buffer, audio_data_size, &b_complete);
				if(error)
				{
					break;
				}

				if(b_complete)
				{
					break;
				}
			}

			if(!b_complete)
			{
				error = gnsdk_musicid_query_fingerprint_end(mQueryHandle);
			}
		}

		audioSource.SourceClose();
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName) throw ( GnError )
{
	this->_SetText(albumTitle, trackTitle, artistName, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL);

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName) throw ( GnError )
{
	this->_SetText(albumTitle, trackTitle, artistName, albumArtistName, trackArtistName, composerName);
    
	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  _MapfPTypeCStr
 */
gnsdk_cstr_t GnMusicID::_MapfPTypeCStr(GnFingerprintType fpType)
{
	gnsdk_cstr_t str = GNSDK_NULL;

	switch(fpType)
	{
	case kFingerprintTypeCMX:
		str = GNSDK_MUSICID_FP_DATA_TYPE_CMX;
		break;

	case kFingerprintTypeFile:
		str = GNSDK_MUSICID_FP_DATA_TYPE_FILE;
		break;

	case kFingerprintTypeGNFPX:
		str = GNSDK_MUSICID_FP_DATA_TYPE_GNFPX;
		break;

	case kFingerprintTypeStream3:
		str = GNSDK_MUSICID_FP_DATA_TYPE_STREAM3;
		break;

	case kFingerprintTypeStream6:
		str = GNSDK_MUSICID_FP_DATA_TYPE_STREAM6;
		break;

	default:
		break;
	}

	return str;
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(gnsdk_cstr_t strCDTOC) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_toc_string(mQueryHandle, strCDTOC);
	if(error) { throw GnError(); }

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(gnsdk_cstr_t strCDTOC, gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType)
throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_toc_string(mQueryHandle, strCDTOC);
	if(error) { throw GnError(); }

	error = gnsdk_musicid_query_set_fp_data(mQueryHandle, strFingerprintData, _MapfPTypeCStr(fpType) );
	if(error) { throw GnError(); }

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_fp_data(mQueryHandle, strFingerprintData, _MapfPTypeCStr(fpType) );
	if(error) { throw GnError(); }

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_gdo(mQueryHandle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  FindAlbums
 */
GnResponseAlbums GnMusicID::FindAlbums(GnAudioSource& audioSource, GnFingerprintType fpType) throw ( GnError )
{
	FingerprintFromSource(audioSource, fpType);

	return _FindAlbums();
}

/*-----------------------------------------------------------------------------
 *  _FindAlbums
 */
GnResponseAlbums GnMusicID::_FindAlbums() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo;
	gnsdk_error_t      error;

	error = gnsdk_musicid_query_find_albums(mQueryHandle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseAlbums tmp = GnResponseAlbums(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  FindMatches
 */

GnResponseDataMatches GnMusicID::FindMatches(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName) throw ( GnError )
{
    return this->FindMatches(albumTitle, trackTitle, artistName, GNSDK_NULL, GNSDK_NULL, GNSDK_NULL);

}


GnResponseDataMatches  GnMusicID::FindMatches(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName) throw ( GnError ) {
    
    gnsdk_error_t      error;
	gnsdk_gdo_handle_t response_gdo;
    
	this->_SetText(albumTitle, trackTitle, artistName, albumArtistName, trackArtistName,composerName );
	
	error = gnsdk_musicid_query_find_matches(this->mQueryHandle, &response_gdo);
	if(error) { throw GnError(); }
    
	GnResponseDataMatches result(response_gdo);
    
	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }
    
	return result;

}

/*-----------------------------------------------------------------------------
 *  FindTracks
 */
GnResponseTracks GnMusicID::FindTracks(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_gdo(mQueryHandle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindTracks();
}

/*-----------------------------------------------------------------------------
 *  FindTracks
 */
GnResponseTracks GnMusicID::FindTracks(gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_musicid_query_set_fp_data(mQueryHandle, strFingerprintData, _MapfPTypeCStr(fpType) );
	if(error) { throw GnError(); }

	return _FindTracks();
}




/*-----------------------------------------------------------------------------
 *  _FindTracks
 */
GnResponseTracks GnMusicID::_FindTracks() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo;
	gnsdk_error_t      error;
    
	error = gnsdk_musicid_query_find_tracks(mQueryHandle, &response_gdo);
	if(error) { throw GnError(); }
    
	GnResponseTracks tmp(response_gdo);
    
	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }
    
	return tmp;
}


/*-----------------------------------------------------------------------------
 *  _SetText
 */
void
GnMusicID::_SetText(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName ) throw ( GnError )
{
    gnsdk_error_t error = MIDERR_NoError;
    
    if(albumTitle)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_ALBUM, albumTitle);
		if(error) { throw GnError(); }
	}
    
	if(trackTitle)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_TITLE, trackTitle);
		if(error) { throw GnError(); }
	}
    
    if(artistName)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_ARTIST, artistName);
		if(error) { throw GnError(); }
	}
    
    if(albumArtistName)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_ALBUM_ARTIST, albumArtistName);
		if(error) { throw GnError(); }
	}
    
    if(trackArtistName)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_TRACK_ARTIST, trackArtistName);
		if(error) { throw GnError(); }
	}
    
    if(composerName)
	{
		error = gnsdk_musicid_query_set_text(mQueryHandle, GNSDK_MUSICID_FIELD_COMPOSER, composerName);
		if(error) { throw GnError(); }
	}

}

/*-----------------------------------------------------------------------------
 *  _callback_status
 */
void GNSDK_CALLBACK_API GnMusicID::_callback_status(void* callback_data, gnsdk_status_t mid_status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort)
{
	GnMusicID* p_musicid = (GnMusicID*)callback_data;

	if(p_musicid->mEventHandler)
	{
		p_musicid->mEventHandler->status_event(mid_status, percent_complete, bytes_total_sent, bytes_total_received);
		if(p_musicid->mEventHandler->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}
#endif /* GNSDK_MUSICID */
