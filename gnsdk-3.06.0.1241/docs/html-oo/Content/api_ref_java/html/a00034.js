var a00034 =
[
    [ "GnError", "a00034.html#aa846bde079d7f85600ef05b8e808da10", null ],
    [ "GnError", "a00034.html#adaab6e6ac420d780f88466aebcb42841", null ],
    [ "delete", "a00034.html#ac473b806aad27044152a5945da90e0bd", null ],
    [ "errorAPI", "a00034.html#af5710d8f5614198b2adb1884cf2566c8", null ],
    [ "errorCode", "a00034.html#aece8750bd95b7f27b305c7e8ee51135e", null ],
    [ "errorDescription", "a00034.html#a6e6968fbfb8d88cd37697fdcb82bdabf", null ],
    [ "errorModule", "a00034.html#adc4a6d0f049e1103172c43bd09b13b92", null ],
    [ "finalize", "a00034.html#a16c5ce72a0a84021225eb12b7824b26e", null ],
    [ "sourceErrorCode", "a00034.html#a8870372a63b4e652c98534ee49751143", null ],
    [ "sourceErrorModule", "a00034.html#a8f4e520b6d1d040e3147f99dcef37613", null ],
    [ "swigCMemOwn", "a00034.html#a13fdaa8c4e964a22ee58c182d629116a", null ]
];