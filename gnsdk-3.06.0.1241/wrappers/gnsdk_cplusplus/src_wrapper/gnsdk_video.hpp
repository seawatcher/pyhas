/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_VIDEO_HPP_

/* gnsdk_video.hpp: primary interface for the VideoID SDK
 */
#define _GNSDK_VIDEO_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "gnsdk_list.hpp"
#include "metadata_video.hpp"

namespace gracenote
{
	using namespace metadata;

	namespace video
	{
		/**
		 *  The type of text search that is used to find results.
		 */
		enum GnVideoSearchType
		{
			/**
			 *   Unknown search type; the default state.
			 */
			kSearchTypeUnknown = 0,

			/**
			 *   Anchored text search, used for product title and suggestion searches.
			 *   Retrieves results that begin with the same characters as exactly
			 *   specified; for example, entering <i>dar</i>, <i>dark</i>, <i>dark k</i>,
			 *   or <i>dark kni</i> retrieves the title <i>Dark Knight,</i> but entering<i>
			 *   knight</i> will not retrieve<i> Dark Knight</i>. Note that this search
			 *   type recognizes both partial and full words.
			 */
			kSearchTypeAnchored,

			/**
			 *   Normal keyword filter search for contributor, product, and work title
			 *   searches; for example, a search using a keyword of <i>dark</i>, <i>knight</i>,
			 *   or <i>dark knight </i>retrieves the title <i>Dark Knight</i>. Note that
			 *   this search type recognizes only full words, not partial words; this
			 *   means that entering only <i>dar</i> for <i>dark</i> is not recognized.
			 */
			kSearchTypeDefault
		};

		/**
		 *  The type of text search field that is used to find results.
		 */
		enum GnVideoSearchField
		{
			/**
			 *  Specifies text for a contributor name search field.
			 */
			kSearchFieldContributorName = 1,

			/**
			 *  Specifies text for a character name search field.
			 */
			kSearchFieldCharacterName,

			/**
			 *  Specifies text for a work franchise search field.
			 */
			kSearchFieldWorkFranchise,

			/**
			 *  Specifies text for a work series search field.
			 */
			kSearchFieldWorkSeries,

			/**
			 *  Specifies text for a work title search field.
			 */
			kSearchFieldWorkTitle,

			/**
			 *
			 *  Specifies text for a product title search field.
			 */
			kSearchFieldProductTitle,

			/**
			 *
			 *  Specifies text for a series title search field.
			 */
			kSearchFieldSeriesTitle,

			/**
			 *  Specifies text for a comprehensive search field.
			 *  <p><b>Remarks:</b></p>
			 *  This video search field performs a search on all fields relevant to the search, and returns any
			 *  data found in any of these fields. However, this search field cannot be used when performing
			 *  suggestion search queries.
			 */
			kSearchFieldAll
		};

		/**
		 *  The TOC Flag type.
		 */
		enum GnVideoTOCFlag
		{
			/**
			 *  Generally recommended flag to use when setting a video TOC.
			 *  <p><b>Remarks:</b></p>
			 *  Use this flag for the TOC flag parameter that is set with the
			 *  FindProducts() API, for all general cases; this includes when using the Gracenote
			 *  Video Thin Client component to produce TOC strings.
			 *  For cases when other VideoID TOC flags seem necessary, contact Gracenote for guidance on setting
			 *  the correct flag.
			 */
			kTOCFlagDefault = 0,

			/**
			 *  Flag to indicate a given simple video TOC is from a PAL disc.
			 *  <p><b>Remarks:</b></p>
			 *  Use this flag only when directed to by Gracenote. Only special video TOCs that are provided by
			 *  Gracenote and external to the
			 *  GNSDK may require this flag.
			 */
			kTOCFlagPal,

			/**
			 *  Flag to indicate a given simple video TOC contains angle data.
			 *  <p><b>Remarks:</b></p>
			 *  Use this flag only when directed to by Gracenote. Only special video TOCs that are provided by
			 *  Gracenote and external to the
			 *  GNSDK may require this flag.
			 */
			kTOCFlagAngles
		};

		/**
		 *  The list element filter type.
		 */
		enum GnVideoListElementFilterType
		{
			/**
			 *  List-based filter to include/exclude genres.
			 */
			kListElementFilterGenre = 1,

			/**
			 *  List-based filter to include/exclude production types.
			 */
			kListElementFilterProductionType,

			/**
			 *  List-based filter to include/exclude serial types.
			 */
			kListElementFilterSerialType,

			/**
			 *  List-based filter to include/exclude origin.
			 */
			kListElementFilterOrigin
		};

		/**
		 *  The video filter type.
		 */
		enum GnVideoFilterType
		{
			/**
			 *  Filter for Season numbers; not list-based.
			 */
			kFilterSeasonNumber = 1,

			/**
			 *  Filter for season episode numbers; not list-based.
			 */
			kFilterSeasonEpisodeNumber,

			/**
			 *  Filter for series episode numbers; not list-based.
			 */
			kFilterSeriesEpisodeNumber
		};

		/**
		 *  The video external ID type.
		 */
		enum GnVideoExternalIdType
		{
			/**
			 *  Sets a Universal Product Code (UPC) value as an external ID for a Products query.
			 *  <p><b>Remarks:</b></p>
			 *  Use as the external ID type parameter set with the ExternalIdTypeSet() API when
			 *  providing a video UPC value for identification.
			 */
			kExternalIdTypeUPC = 1,

			/**
			 *
			 *  Sets a International Standard Audiovisual Number (ISAN) code as an external ID for a Works
			 *  query.
			 *  <p><b>Remarks:</b></p>
			 *  Use as the external ID Type parameter set with the ExternalIdTypeSet() API when
			 *   providing a video ISAN value for identification.
			 */
			kExternalIdTypeISAN
		};

		/**************************************************************************
		** GnVideo
		*/
		class GnVideo : public GnObject
		{
		public:
			/**
			 *  Initializes the Gracenote VideoID and Video Explore libraries.
			 *  @param user Set user
			 *  @param pEventHandler Event handler
			 *  @return VIDERR_InvalidArg - Given parameter is invalid
			 *  @return VIDERR_InitFailed - Initialization failed
			 *  @return VIDERR_NoError - Initialization succeeded
			 *  <p><b>Remarks:</b></p>
			 *  This function must be successfully called before any other VideoID and Video Explore APIs will
			 *  succeed.
			 */
			GnVideo(const GnUser& user, GnStatusEvents* pEventHandler = GNSDK_NULL) throw ( GnError );

			/**
			 *  Shuts down and release resources for the VideoID and Video Explore libraries.
			 *  @return VIDERR_NotInited - Object was not successfully initiated
			 *  @return VIDERR_NoError - Success
			 */
			virtual ~GnVideo();

			/**
			 *  Retrieves the VideoID and Video Explore library's version string.
			 *
			 *  This API can be called at any time, even before initialization and after shutdown. The returned
			 *   string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example: <code>1.2.3.123</code> (Major.Minor.Improvement.Build)<br>
			 *  Major: New functionality<br>
			 *  Minor: New or changed features<br>
			 *  Improvement: Improvements and fixes<br>
			 *  Build: Internal build number<br>
			 */
			static gnsdk_cstr_t Version();

			/**
			 *  Retrieves the VideoID and Video Explore library's build date as a string.
			 *  @return String with format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, even before initialization and after shutdown. The returned
			 *   string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example:<code>"2008-02-12 00:41 UTC"</code>
			 */
			static gnsdk_cstr_t BuildDate();

			/**
			 *  Specifies the preferred language of the returned results. This option applies only to TOC
			 *  Lookups.
			 *  @param languageOption Set language option
			 */
			void OptionPreferredLanguage(gnsdk_cstr_t languageOption) throw ( GnError );

			/**
			 *  This method set both the option start and result range size.
			 *  @param resultStart Set the starting position for the result
			 *  @param resultCount Set the count that specifies the maximum number of results a response can return.
			 *  <p><b>Remarks:</b></p>
			 *  This option is useful for paging through results.
			 * <p><b>Note:</b></p>
			 *   Gracenote Service limits the range size for some queries. If you specify a range size greater
			 *   than the limit, the results are constrained. Additionally, neither Gracenote Service nor GNSDK
			 *   returns an error about this behavior.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 */
			void OptionResultRange(gnsdk_cstr_t resultStart, gnsdk_cstr_t resultCount) throw ( GnError );

			/**
			 *  Specifies the initial value for a result range that is returned by a response. The initial value
			 *   can be an ordinal.
			 *  @param rangeStart Set the starting range
			 *  <p><b>Remarks:</b></p>
			 *  This option is useful for paging through results.
			 * <p><b>Note:</b></p>
			 *   Gracenote Service enforces that the range start value must be less than or equal to the total
			 *   number of results. If you specify a range start value that is greater than the total number of
			 *   results, no results are returned.
			 */
			void OptionRangeStart(gnsdk_cstr_t rangeStart) throw ( GnError );

			/**
			 *  This method sets only result range size.
			 *  @param rangeSize Set the result range size
			 *  Specifies a maximum number of results that a response can return.
			 *  <p><b>Remarks:</b></p>
			 *  This option is useful for paging through results.
			 * <p><b>Note:</b></p>
			 *   Gracenote Service limits the range size for some queries. If you specify a range size greater
			 *   than the limit, the results are constrained. Additionally, neither Gracenote Service nor GNSDK
			 *   returns an error about this behavior.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 */
			void OptionRangeSize(gnsdk_cstr_t rangeSize) throw ( GnError );

			/**
			 *  Indicates whether a response includes any external IDs (third-party IDs).
			 *  @param bEnableExternalId Set boolean to enable external IDs
			 *  <p><b>Remarks:</b></p>
			 *  External IDs are third-party IDs associated with the results (such as an Amazon ID),
			 *   configured specifically for your application. Contact your Gracenote Professional Services
			 *   representative about enabling this option.
			 *
			 *  External IDs are supported for the following GDO types:
			 *  <ul>
			 *  <li>GNSDK_GDO_TYPE_CONTRIBUTOR</li>
			 *  <li>GNSDK_GDO_TYPE_VIDEOCLIP</li>
			 *  <li>GNSDK_GDO_TYPE_VIDEO_PRODUCT</li>
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SEASON</li>
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SERIES</li>
			 *  <li>GNSDK_GDO_TYPE_VIDEO_WORK</li>
			 *  </ul>
			 *  All the listed types support these child GDOs:
			 *  <ul>
			 *  <li>GNSDK_GDO_CHILD_EXTERNAL_ID</li>
			 *  </ul>
			 *
			 */
			void OptionEnableExternalID(gnsdk_bool_t bEnableExternalId) throw ( GnError );

			/**
			 *  Indicates whether a response includes any Link data (third-party metadata).
			 *  @param bEnableLinkData Set boolean to enable Link Data
			 *  <p><b>Remarks:</b></p>
			 *  Link data is third-party metadata associated with the results (such as an Amazon ID or cover art
			 *   ID), configured specifically for your application. Contact your Gracenote Professional Services
			 *   representative about enabling this option.
			 *
			 *  Link data is supported for the following types:
			 *  <ul>
			 *  <li>Contributor</li>
			 *  <li>Video clip</li>
			 *  <li>Video product</li>
			 *  <li>Video season</li>
			 *  <li>Video series</li>
			 *  <li>Video work</li>
			 *  </ul>
			 */
			void OptionEnableLinkData(gnsdk_bool_t bEnableLinkData) throw ( GnError );

			/**
			 *  Indicates whether a response will be used to fetch content like cover art, reviews, etc.
			 *  @param bEnableLinkData Set boolean to enable Link Data
			 *  <p><b>Remarks:</b></p>
			 *  <p><b>Image Availability</b></p>
			 *  At the time of this guide's publication, only limited content is available for Video Explore
			 *   Seasons and Series image retrieval through Seasons, Series, and Works queries. Contact your
			 *   Gracenote representative for updates on the latest supported images.
			 */
			void OptionEnableContentData(gnsdk_bool_t bEnableLinkData) throw ( GnError );

			/**
			 *  Indicates whether a response is not automatically stored in the cache.
			 *  @param bNoCache Set boolean to enable caching
			 *  <p><b>Remarks:</b></p>
			 *  Set this option to True to retrieve query data from a call to Gracenote Service; doing this
			 *   ensures retrieving the most recent available data.
			 *  Set this option to False to retrieve query data that already exists in the lookup cache. When no
			 *   data exists in the cache, VideoID
			 *  or Video Explore automatically calls Gracenote Service with the query request.
			 */
			void OptionQueryNoCache(gnsdk_bool_t bNoCache) throw ( GnError );

			/**
			 *  Specifies that a TOC lookup includes the disc's commerce type.
			 *  @param bEnableCommerceType Set boolean to enable commerce type
			 */
			void OptionEnableCommerceType(gnsdk_bool_t bEnableCommerceType) throw ( GnError );

			/**
			 *  Sets a filter for a Video Explore query handle, using a value from a list.
			 *  @param bInclude [in] Set Boolean value to True to include filter key; set to False to exclude the filter
			 *  @param listElementFilterType [in] One of the #GnVideoListElementFilterType filter types
			 *  @param listElement [in] A list element handle used to populate the filter value. The list
			 *   element must be from a list that corresponds to the filter name.
			 *
			 *  @return VIDERR_NotInited - Object was not successfully initiated
			 *  @return VIDERR_InvalidArg - One of the given parameter values is empty or invalid
			 *  @return VIDERR_NoMemory - System is out of memory
			 *  @return VIDERR_NoError - Filter was successfully set
			 *  <p><b>Remarks:</b></p>
			 *  Use this function with the GNSDK Manager Lists functionality to apply filters on and improve the
			 *   relevance of specific Video Explore search results. To use this function, the application must be
			 *   licensed for Video Explore.
			 * <p><b>Note:</b></p>
			 *  The system disregards filters when performing non-text related video queries, and filtering
			 *   by list elements is restricted to performing a Works search using the following Filter Values:
			 *  <ul>
			 *  <li>kListElementFilterGenre</li>
			 *  <li>kListElementFilterProductionType</li>
			 *  <li>kListElementFilterSerialType</li>
			 *  <li>kListElementFilterOrigin</li>
			 *  </ul>
			 */
			void FilterByListElement(gnsdk_bool_t bInclude, GnVideoListElementFilterType listElementFilterType, const GnListElement& listElement) throw ( GnError );

			/**
			 *
			 *  Sets a filter for a VideoID or Video Explore query handle.
			 *  @param filterValue [in] String value for corresponding data key
			 *  @param filterType [in] One of the #GnVideoFilterType filter types
			 *  @return VIDERR_NotInited - Object was not successfully initiated
			 *  @return VIDERR_InvalidArg - A given parameter value is empty or invalid
			 *  @return VIDERR_InvalidData - The <code>filterValue</code> is not valid with the <code>filterType</code>
			 *  @return VIDERR_NoMemory - System is out of memory
			 *  @return VIDERR_NoError - Filter was successfully set
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to apply certain filters on and improve the relevance of Work text search query
			 *   results.
			 * <p><b>Note:</b></p>
			 *  The system disregards filters when performing non-text related video queries, and that filtering by<br>
			 *  list elements is restricted to performing a Works search using the following Filter Values:
			 *  <ul>
			 *  <li>kFilterSeasonNumber text (integer as a string)</li>
			 *  <li>kFilterSeasonEpisodeNumber text (integer as a string)</li>
			 *  <li>kFilterSeriesEpisodeNumber text (integer as a string)</li>
			 *  </ul>
			 *
			 */
			void Filter(gnsdk_cstr_t filterValue, GnVideoFilterType filterType) throw ( GnError );

			/**
			 *  Sets a video's external ID string information used to query a VideoID or Video Explore query
			 *   handle.
			 *  @param externalId [in] External ID string
			 *  @param externalIdType [in_opt] One of the #GnVideoExternalIdType external ID types (such as a UPC)
			 *
			 *  @return VIDERR_NotInited - Object was not successfully initiated
			 *  @return VIDERR_InvalidArg - A given parameter value is empty or invalid
			 *  @return VIDERR_NoMemory - System is out of memory
			 *  @return VIDERR_NoError - External ID was successfully set
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to set an external ID for a product. You can set only one external ID per
			 *   product.
			 */
			void ExternalIdTypeSet(gnsdk_cstr_t externalId, GnVideoExternalIdType externalIdType) throw ( GnError );

			/**
			 *  This method is used to find Video Products using a TOC.
			 *  Performs a VideoID or Video Explore query for Products.
			 *  @param videoTOC [in] TOC string; must be an XML string constructed by the Gracenote Video Thin Client component.
			 *  @param TOCFlag [in] TOC string flag; for flags types, see #GnVideoTOCFlag
			 *
			 *  @return GnResponseVideoProduct
			 *  @return VIDERR_NotInited Object was not successfully initiated
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A product is a specific version of a video presentation, either one television show episode
			 *  or a complete season, or a movie. Products are generally available on a DVD, Blu-ray disc, or a box set.
			 *  Use this API to find a video product by its TOC.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a <code>*IDERR_NoError</code> code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  </ul>
			 *
			 *  The following licensing requirements apply:
			 *  <ul>
			 *  <li>Querying by TOC requires VideoID licensing.</li>
			 *  </ul>
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */

			GnResponseVideoProduct FindProducts(gnsdk_cstr_t videoTOC, GnVideoTOCFlag TOCFlag) throw ( GnError );

			/**
			 *  This method is used to find Video Products using a GNDataObject.
			 *  Performs a VideoID or Video Explore query for Products.
			 *  @param gnObj [in]
			 *  @return GnResponseVideoProduct
			 *  @return VIDERR_NotInited Object was not successfully initiated
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A product is a specific version of a video presentation, either one television show episode
			 *  or a complete season, or a movie. Products are generally available on a DVD, Blu-ray disc, or a box set.
			 *  Use this API to find a video product by using a GNDataObject.
			 * <p><b>Note:</b></p>
			 *   To query by TUI, you must first convert a TUI and its associated TUI Tag to a GDO using
			 *   <code>gnsdk_manager_gdo_create_from_id</code>.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a <code>*IDERR_NoError</code> code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  </ul>
			 *
			 *  The following licensing requirements apply:
			 *  <ul>
			 *  <li>Querying by external ID and GDO requires VideoID or Video Explore licensing.</li>
			 *  </ul>
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)	*/

			GnResponseVideoProduct FindProducts(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  This method is used to find Video Products using Querying by Text
			 *  Performs a VideoID or Video Explore query for Products.
			 *  @param textInput [in] text string.
			 *  @param searchField [in] Video search field e.g. character name, work franchise and work series search field etc; see #GnVideoSearchField for available search fields
			 *  @param searchType [in] video search type, see #GnVideoSearchType for available search types
			 *
			 *  @return GnResponseVideoProduct
			 *  @return VIDERR_NotInited Object was not successfully initiated
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A product is a specific version of a video presentation, either one television show episode
			 *  or a complete season, or a movie. Products are generally available on a DVD, Blu-ray disc, or a box set.
			 *  Use this API to find a video product by its identifying text.
			 *
			 *  The response GDO, if available, is of the GNSDK_GDO_TYPE_RESPONSE_VIDEO_PRODUCT context.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a <code>*IDERR_NoError</code> code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  <li>Returns a GDO for the <code>None</code> match type response.</li>
			 *  </ul>
			 *
			 *  As the GDO returns only a <code>None</code> value, ensure that the GDO is released to conserve system
			 *   resources.
			 *
			 *  The following licensing requirements apply:
			 *  <ul>
			 *  <li>Querying by Text  requires VideoID or Video Explore licensing.</li>
			 *  </ul>
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoProduct FindProducts(gnsdk_cstr_t textInput, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError );

			/**
			 *  This method is used to find a Video Work with a GNDataObject.
			 *  Performs a Video Explore query for Works.
			 *  @param gnObj [in]
			 *
			 *  @return GnResponseVideoWork
			 *  @return VIDERR_NotInited Object was not successfully initiated
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  The term 'work' has both generic and specific meanings. It can be used generically to indicate a
			 *   body of work, or, more specifically, to indicate a particular movie or television show.
			 *  Use this function to find a work using a GNDataObject.
			 * <p><b>Notes:</b></p>
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  </ul>
			 * <p><b>Note:</b></p>
			 *   Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoWork FindWorks(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  This method is used to find Video Work by text string.
			 *  Performs a Video Explore query for Works.
			 *  @param textInput [in] text string.
			 *  @param searchField [in] video search field e.g. character name,work franchise and work series search field etc; see #GnVideoSearchField for available search fields
			 *  @param searchType [in] video search type, see #GnVideoSearchType for available search types
			 *
			 *  @return GnResponseVideoWork
			 *  @return VIDERR_NotInited Object was not successfully initiated
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  The term 'work' has both generic and specific meanings. It can be used generically to indicate a
			 *   body of work, or, more specifically, to indicate a particular movie or television show.
			 *  Use this function to find a work by its identifying text.
			 * <p><b>Notes:</b></p>
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  </ul>
			 * <p><b>Note:</b></p>
			 *   Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoWork FindWorks(gnsdk_cstr_t textInput, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError );

			/**
			 *  Performs a Video Explore query for Seasons.
			 *  This method is used to find Video Seasons using a GnDataObject.
			 *  @param gnObj [in]
			 *  @return GnResponseVideoSeasons
			 *
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_Busy
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_InsufficientInputData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A Season is an ordered collection of Works.
			 *
			 * Examples: CSI: Las Vegas, Season One; CSI: Las Vegas, Season Two; and CSI: Las Vegas, Season Three.
			 *
			 * Use this API to find a Seasons object using a GnDataObject. A partial Seasons object can be contained in a full Contributor, Series, or
			 *   Work object.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.
			 *  <li>Sets the match type to <code>None</code>.
			 *  </ul>
			 *  Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoSeasons FindSeasons(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  Performs a Video Explore query for Series.
			 *  @param gnObj [in]
			 *  @return GnResponseVideoSeries
			 *
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_Busy
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_InsufficientInputData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A Series is a collection of related Works, typically in sequence, and for television programs,
			 *   often comprised of Seasons. Examples are the film series <em>The Matrix Trilogy</em> and the television
			 *   series <em>CSI: Las Vegas</em>. Use this API to find a Series object by identifying text, an external ID, or
			 *   a GDO. A partial Seasons object can be contained in a full Contributors, Series, or Works object.
			 *
			 *  Valid inputs for this query are the following:
			 *  <ul>
			 *  <li><code>gnsdk_video_query_set_text</code> with GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE. When performing this
			 *   query type, you can perform a regular text search or indexed suggestion search. You cannot search
			 *   for a Series using <code>gnsdk_video_query_set_text</code> with GNSDK_VIDEO_SEARCH_FIELD_ALL.</li>
			 *  <li><code>gnsdk_video_query_set_gdo</code> with an allowed context, as shown in Valid Full and Partial GDO
			 *   Query Input.
			 * <p><b>Note:</b> To query by TUI, you must first convert a TUI and its associated TUI Tag to a GDO
			 *   using <code>gnsdk_manager_gdo_create_from_id</code>.</p></li>
			 *  <li><code>gnsdk_video_query_set_external_id</code> for implementation-specific (pass-through) ID types. For
			 *   more information about using this function with implementation-specific IDs, contact your Gracenote
			 *   Professional Services representative.</li>
			 *  </ul>
			 *  The response GDO, if available, is of the GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES context.
			 *  Through the GNSDK_GDO_TYPE_VIDEO_SERIES context, this API supports the
			 *   GNSDK_VIDEO_OPTION_ENABLE_LINK_DATA option. For more information, see the
			 *   GNSDK_VIDEO_OPTION_ENABLE_LINK_DATA topic.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  <li>Returns a GDO for the <code>None</code> match type response.</li>
			 *  </ul>
			 *  As the GDO returns only a <code>None</code> value, ensure that the GDO is released to conserve system
			 *   resources.
			 *  Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoSeries FindSeries(GnDataObject& gnObj) throw ( GnError );

			GnResponseVideoSeries FindSeries(gnsdk_cstr_t textInput, GnVideoSearchType searchType) throw ( GnError );

			/**
			 *  Performs a Video Explore query for Contributors.
			 *  @param gnObj [in]
			 *  @return GnResponseContributor
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return  VIDERR_HandleObjectWrongType
			 *  @return  VIDERR_NoMemory
			 *  @return  VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  A contributor is a person involved in the creation of a work (such as an actor or a director) or
			 *   an entity (such as a production company).
			 *
			 * Use this function to find a contributor by its external ID, identifying text, or by a GDO.
			 *
			 * <p><b>Note:</b> To query by TUI, you must first convert a TUI and its
			 *   associated TUI Tag to a GDO using <code>gnsdk_manager_gdo_create_from_id</code>.</p>
			 *
			 *  The response GDO, if available, is of the GNSDK_GDO_TYPE_RESPONSE_CONTRIBUTOR context.
			 *
			 * This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  <li>Returns a GDO for the <code>None</code> match type response.</li>
			 *   </ul>
			 *
			 * <p><b>Note:</b></p>
			 *   As the GDO returns only a <code>None</code> value, ensure that the GDO is released to conserve system
			 *   resources.
			 * <p><b>Note:</b></p>
			 *   Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseContributor FindContributors(GnDataObject& gnObj) throw ( GnError );

			GnResponseContributor FindContributors(gnsdk_cstr_t textInput, GnVideoSearchType searchType) throw ( GnError );

			/**
			 *  Performs a Video Explore query for Programs.
			 *  @param gnObj [in]
			 *  @return GnResponseVideoProgram
			 *
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoProgram FindPrograms(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  Performs a Video Explore query for any type of video object.
			 *  @param gnObj [in]
			 *  @return GnResponseVideoObjects
			 *
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to retrieve a specific Video Explore object referenced by a GDO, or to retrieve
			 *   all the Video Explore objects (Contributors, Products, Seasons, Series, and Works) associated with a
			 *   particular external ID.
			 *
			 *  This query is useful when you are unsure of all the potential object types existing for a
			 *   particular GDO or external ID input.
			 *
			 *  Valid inputs for this query are the following:
			 *
			 *  <code>gnsdk_video_query_set_gdo</code> with any of the following GDO types:
			 *  <ul>
			 *  <li>GNSDK_GDO_TYPE_CONTRIBUTOR
			 *  <li>GNSDK_GDO_TYPE_VIDEO_PRODUCT
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SEASON
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SERIES
			 *  <li>GNSDK_GDO_TYPE_VIDEO_WORK
			 *  <li>GNSDK_GDO_TYPE_EXTENDED_DATA
			 *  </ul>
			 * <p><b>Note:</b></p>
			 *   To query by TUI, you must first convert a TUI and its associated TUI Tag to a GDO using
			 *   <code>gnsdk_manager_gdo_create_from_id</code>,
			 *  <code>gnsdk_video_query_set_external_id</code> for any external ID that Gracenote Service accepts (such as
			 *   UPC), and implementation-specific (pass-through) ID types.
			 *
			 *  For more information about using this function with implementation-specific IDs, contact your
			 *   Gracenote Professional Services representative.
			 *
			 *  The response GDO, if available, is of the GNSDK_GDO_TYPE_RESPONSE_VIDEO_OBJECT context.
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query with
			 *   no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a *IDERR_NoError code.</li>
			 *  <li>Sets the match type to <code>None</code>.</li>
			 *  <li>Returns a GDO for the <code>None</code> match type response.</li>
			 *  </ul>
			 *  As the GDO returns only a <code>None</code> value, ensure that the GDO is released to conserve system
			 *   resources.
			 *
			 *  Performing this query requires Video Explore licensing.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoObjects FindObjects(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  Performs a VideoID or Video Explore query for search suggestion text.
			 *  This method is used to find Video suggestion by obj.
			 *  @param gnObj [in]
			 *
			 *  @return GnResponseVideoSuggestions
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to suggest potential matching titles, names, and series as a user enters
			 *  text in a search query.
			 *
			 *   Your license determines what suggestion searches your app can perform:
			 *  <ul>
			 *  <li>To query for Product titles requires VideoID or Video Explore licensing</li>
			 *  <li>To query for Works titles, Series titles, or Contributors names requires Video Explore licensing.</li>
			 *  </ul>
			 *  Use this function with a query handle set with a <code>GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE</code> key to
			 *  use suggestion functionality in video series searches.
			 *
			 *  This function uses the anchored search type - <code>gnsdk_video_search_type_anchored</code>.
			 *  The response GDO, if available, is of the <code>GNSDK_GDO_TYPE_RESPONSE_SUGGESTIONS</code> context.<br>
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query
			 *  with no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a <code>*IDERR_NoError</code> code.</li>
			 *  </ul>
			 *  Suggestion searching has 2 limitations:
			 *  <ol>
			 *  <li>A suggestion search can be performed on only one search field at a time.</li>
			 *  <li>A suggestion search cannot be performed on a query handle defined with the
			 *  <code>GNSDK_VIDEO_SEARCH_FIELD_ALL</code> search field.</li>
			 *   </ol>
			 *	<p><b>Steps:</b></p>
			 *   <ol>
			 *	<li>Initialize GNSDK Manager</li>
			 *	<li>Get user handle</li>
			 *	<li>Initialize VideoID</li>
			 *	<li>Create video query</li>
			 *	<li>Set query text</li>
			 *	<li>Set query options</li>
			 *	<li>Perform "find_suggestions" query</li>
			 *	<li>Display results</li>
			 *   </ol>
			 *
			 *
			 * For Example, see the Video Search Suggestions sample application.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoSuggestions FindSuggestions(GnDataObject& gnObj) throw ( GnError );

			/**
			 *  Performs a VideoID or Video Explore query for search suggestion text.
			 *  This method is used to find Video suggestion by text string.
			 *  @param search_text [in] text string.
			 *  @param searchField [in] video search field e.g. character name,work franchise and work series search field etc; see #GnVideoSearchField for available search fields
			 *  @param searchType [in] video search type, see #GnVideoSearchType for available search types
			 *
			 *  @return GnResponseVideoSuggestions
			 *  @return VIDERR_NotInited
			 *  @return VIDERR_InvalidArg
			 *  @return VIDERR_InvalidData
			 *  @return VIDERR_Unsupported
			 *  @return VIDERR_HandleObjectInvalid
			 *  @return VIDERR_HandleObjectWrongType
			 *  @return VIDERR_NoMemory
			 *  @return VIDERR_NoError
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to suggest potential matching titles, names, and series as a user enters
			 *  text in a search query.
			 *
			 *   Your license determines what suggestion searches your app can perform:
			 *  <ul>
			 *  <li>To query for Product titles requires VideoID or Video Explore licensing</li>
			 *  <li>To query for Works titles, Series titles, or Contributors names requires Video Explore licensing.</li>
			 *  </ul>
			 *  Use this function with a query handle set with a <code>GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE</code> key to
			 *  use suggestion functionality in video series searches.
			 *
			 *  This function uses the anchored search type - <code>gnsdk_video_search_type_anchored</code>.
			 *  The response GDO, if available, is of the <code>GNSDK_GDO_TYPE_RESPONSE_SUGGESTIONS</code> context.<br>
			 *
			 *  This API performs the following behavior when it successfully finds no results for the query
			 *  with no errors:
			 *  <ul>
			 *  <li>Displays a return code indicating processing success; generally, a <code>*IDERR_NoError</code> code.</li>
			 *  </ul>
			 *  Suggestion searching has 2 limitations:
			 *  <ol>
			 *  <li>A suggestion search can be performed on only one search field at a time.</li>
			 *  <li>A suggestion search cannot be performed on a query handle defined with the
			 *  <code>GNSDK_VIDEO_SEARCH_FIELD_ALL</code> search field.</li>
			 *   </ol>
			 *	<p><b>Steps:</b></p>
			 *   <ol>
			 *	<li>Initialize GNSDK Manager</li>
			 *	<li>Get user handle</li>
			 *	<li>Initialize VideoID</li>
			 *	<li>Create video query</li>
			 *	<li>Set query text</li>
			 *	<li>Set query options</li>
			 *	<li>Perform "find_suggestions" query</li>
			 *	<li>Display results</li>
			 *   </ol>
			 *
			 * For example information, see the Video Search Suggestions sample application.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseVideoSuggestions FindSuggestions(gnsdk_cstr_t search_text, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError );

		private:
			GnStatusEvents*            v_event_handler;

			/**
			 *  The VideoID query handle.
			 */
			gnsdk_video_query_handle_t v_query_handle;

			/**
			 *   Internal method used by FindProduct()
			 */
			GnResponseVideoProduct _FindProduct() throw ( GnError );

			/**
			 *   Internal method used by FindWork()
			 */
			GnResponseVideoWork _FindWork() throw ( GnError );

			/**
			 *   Internal method used by FindSeasons()
			 */
			GnResponseVideoSeasons _FindSeasons() throw ( GnError );

			/**
			 *   Internal method used by FindSeries()
			 */
			GnResponseVideoSeries _FindSeries() throw ( GnError );

			/**
			 *   Internal method used by FindContributors()
			 */
			GnResponseContributor _FindContributors() throw ( GnError );

			/**
			 *   Internal method used by FindPrograms()
			 */
			GnResponseVideoProgram _FindPrograms() throw ( GnError );

			/**
			 *   Internal method used by FindObjects()
			 */

			GnResponseVideoObjects _FindObjects() throw ( GnError );

			/**
			 *   Internal method used by FindSuggestions()
			 */
			GnResponseVideoSuggestions     _FindSuggestions() throw ( GnError );

			gnsdk_cstr_t                   _MapSearchField(GnVideoSearchField searchField);

			static void GNSDK_CALLBACK_API _callback_status(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t*  p_abort);

			/* dissallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnVideo);
		};
	} // namespace video
}     // namespace gracenote
#endif // _GNSDK_VIDEO_HPP_
