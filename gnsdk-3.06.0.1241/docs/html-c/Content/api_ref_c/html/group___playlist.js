var group___playlist =
[
    [ "Attributes", "group___playlist___attributes.html", "group___playlist___attributes" ],
    [ "Callbacks", "group___playlist___callbacks.html", "group___playlist___callbacks" ],
    [ "Collection Summary Functions", "group___playlist___collection_summary_functions.html", "group___playlist___collection_summary_functions" ],
    [ "GDO Type", "group___playlist___g_d_o_type.html", "group___playlist___g_d_o_type" ],
    [ "Initialization Functions", "group___playlist___initialization_functions.html", "group___playlist___initialization_functions" ],
    [ "Playlist Generation", "group___playlist___playlist_generation.html", "group___playlist___playlist_generation" ],
    [ "Storage Functions", "group___playlist___storage_functions.html", "group___playlist___storage_functions" ],
    [ "Type Enums", "group___playlist___types_enums.html", "group___playlist___types_enums" ]
];