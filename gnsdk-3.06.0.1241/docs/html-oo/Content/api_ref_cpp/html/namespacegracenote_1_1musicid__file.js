var namespacegracenote_1_1musicid__file =
[
    [ "gn_musicid_file_info_provider", "classgracenote_1_1musicid__file_1_1gn__musicid__file__info__provider.html", "classgracenote_1_1musicid__file_1_1gn__musicid__file__info__provider" ],
    [ "GnIFingerprinterMusicIDFile", "classgracenote_1_1musicid__file_1_1_gn_i_fingerprinter_music_i_d_file.html", "classgracenote_1_1musicid__file_1_1_gn_i_fingerprinter_music_i_d_file" ],
    [ "GnMusicIDFile", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file.html", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file" ],
    [ "GnMusicIDFileEvents", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file_events.html", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file_events" ],
    [ "GnMusicIDFileInfo", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file_info.html", "classgracenote_1_1musicid__file_1_1_gn_music_i_d_file_info" ]
];