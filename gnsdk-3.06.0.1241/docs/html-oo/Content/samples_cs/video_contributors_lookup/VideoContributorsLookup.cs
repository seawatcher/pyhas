/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoContributorsLookup.cs
 *  Description:
 *  This sample shows basic use of the FindContributors() API
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoContributorsLookup
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoContributorLookupEvents localeEvents = new VideoContributorLookupEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void FindContributor(GnDataObject workGDO, GnUser user)
        {
            int i = 0;
            GnVideo video = new GnVideo(user);

            GnResponseContributor responseContributor = video.FindContributors(workGDO);

            GnContributorEnumerable contributorEnumerable = responseContributor.Contributors;

            GnContributorEnumerator contributorEnumerator = contributorEnumerable.GetEnumerator();

            /*Find all contributors */

            Console.WriteLine("\nContributors:");

            while (contributorEnumerator.hasNext())
            {
                GnContributor contributor = contributorEnumerator.next();

                GnNameEnumerable nameEnumerable = contributor.NamesOfficial;

                foreach (GnName name in nameEnumerable)
                {
                    Console.WriteLine("\t" + ++i + " : " + name.Display);
                }
            }

        }

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoContributorLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Contributor lookup ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        private static void DoContributorsLookup(GnUser user)
        {
            /* Convert serialized gdo into GnDataObject */
            string serializedGDO = "WEcxAxpHl1mb/3LWaUoNklp7jGNb+JJ6JjfVejkh2RyiKi05lSEtfOeCNB5yvBRo4liQy9nzqL5HawtU90Sz7GcuetF6p+V0JpiB+lLSKydG5bhN2YYNqLzQdaJOUbIBnhTUdiXmAWuDh9FhGx9fgGKHH1Zjn+V7ff+DwxqK7lpY+EI243+xFw4kuePtsf+SkOKExQ==";

            using (GnStatusEventsDelegate videoEvents = new VideoContributorLookupEvents())
            {
                //Console.WriteLine("*****Video Contributor Search:*****");

                GnVideo video = new GnVideo(user, null /*videoEvents*/);

                /* Perform a sample album TUI query */
                GnDataObject dataObject = new GnDataObject(serializedGDO);

                GnResponseVideoWork responseVideoWork = video.FindWorks(dataObject);

                if (responseVideoWork.Works.count() > 0)
                {

                    /**********************************************
                   *  Needs decision (match resolution) check
                   **********************************************/
                    if (responseVideoWork.NeedsDecision)
                    {
                        /**********************************************
                         * Resolve match here
                         **********************************************/
                    }
                }

                GnVideoWorkEnumerable videoWorkEnumerable = responseVideoWork.Works;

                foreach (GnVideoWork work in videoWorkEnumerable)
                {
                    GnVideoWork gnVideoWork = work;
                    if (!work.FullResult)
                    {
                        GnResponseVideoWork responseWorrk = video.FindWorks(work);
                        gnVideoWork = responseWorrk.Works.at(0).next();
                    }
                    /********************************
		             * Get and display video title
		             ********************************/
                    GnTitle workTitle = gnVideoWork.OfficialTitle;
                    Console.WriteLine("\n\nTitle: " + workTitle.Display);
                    /******************************
	                * Get contributors
	                ******************************/
                    FindContributor(gnVideoWork, user);
                }
            }

        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrieve Gracenote Descriptor/List based values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Do Video search Filter */
                        DoContributorsLookup(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }            
    }
}
