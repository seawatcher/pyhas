var class_gracenote_s_d_k_1_1_gn_video_credit =
[
    [ "GnVideoCredit", "class_gracenote_s_d_k_1_1_gn_video_credit.html#af41e375b2dabffa4c5b73d25c59d1c52", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_video_credit.html#af72c086a9cda268e5ce36b0a28cc4235", null ],
    [ "ArtistType", "class_gracenote_s_d_k_1_1_gn_video_credit.html#aafddffbbfefeb6424829928187839da8", null ],
    [ "CharacterName", "class_gracenote_s_d_k_1_1_gn_video_credit.html#ab5534d52b45fae114a1357fe7b5ca70e", null ],
    [ "Contributor", "class_gracenote_s_d_k_1_1_gn_video_credit.html#af103cecb71312485dd6b510dc7de6071", null ],
    [ "Era", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a41b2280012dd8e0e42c19f5d6ad42418", null ],
    [ "Genre", "class_gracenote_s_d_k_1_1_gn_video_credit.html#abb4ce98a87af91567da90d27403452e5", null ],
    [ "OfficialName", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a7d4c79724bfe29466945abd99ab38e19", null ],
    [ "Origin", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a939900cb8bd2ee6369d927925abc01f2", null ],
    [ "Rank", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a94ac02ac80e537dd96b23ea8d241ef5f", null ],
    [ "Role", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a54de32a5ee5e061d788c25ce990bfd76", null ],
    [ "RoleBilling", "class_gracenote_s_d_k_1_1_gn_video_credit.html#ae71de7b3d2354ab12850c253182fffcf", null ],
    [ "RoleID", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a8419a4274acdb81f21e3840a6dd119f6", null ],
    [ "Seasons", "class_gracenote_s_d_k_1_1_gn_video_credit.html#ad6033e1eda34df212b14753a8ead8953", null ],
    [ "Series", "class_gracenote_s_d_k_1_1_gn_video_credit.html#ad3e010c52f16f03aabfd4364f8b08c94", null ],
    [ "Works", "class_gracenote_s_d_k_1_1_gn_video_credit.html#a9956c11ddb2d3af76a175936152905eb", null ]
];