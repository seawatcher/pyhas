/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnListElementChildEnumerable : System.Collections.Generic.IEnumerable<GnListElement>, IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnListElementChildEnumerable(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnListElementChildEnumerable obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnListElementChildEnumerable() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnListElementChildEnumerable(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

System.Collections.Generic.IEnumerator<GnListElement> System.Collections.Generic.IEnumerable<GnListElement>.GetEnumerator( )
{
	return begin( );
}
System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator( )
{
	return begin( );
}

  public GnListElementChildEnumerable(GnListElementChildEnumerator start, GnListElementChildEnumerator end) : this(gnsdk_csharp_marshalPINVOKE.new_GnListElementChildEnumerable(GnListElementChildEnumerator.getCPtr(start), GnListElementChildEnumerator.getCPtr(end)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnListElementChildEnumerator begin() {
    GnListElementChildEnumerator ret = new GnListElementChildEnumerator(gnsdk_csharp_marshalPINVOKE.GnListElementChildEnumerable_begin(swigCPtr), true);
    return ret;
  }

  public GnListElementChildEnumerator end() {
    GnListElementChildEnumerator ret = new GnListElementChildEnumerator(gnsdk_csharp_marshalPINVOKE.GnListElementChildEnumerable_end(swigCPtr), true);
    return ret;
  }

  public uint count() {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnListElementChildEnumerable_count(swigCPtr);
    return ret;
  }

  public GnListElementChildEnumerator at(uint index) {
    GnListElementChildEnumerator ret = new GnListElementChildEnumerator(gnsdk_csharp_marshalPINVOKE.GnListElementChildEnumerable_at(swigCPtr, index), true);
    return ret;
  }

  public GnListElementChildEnumerator getByOrdinal(uint index) {
    GnListElementChildEnumerator ret = new GnListElementChildEnumerator(gnsdk_csharp_marshalPINVOKE.GnListElementChildEnumerable_getByOrdinal(swigCPtr, index), true);
    return ret;
  }

}

}
