/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: VideoSearchSuggestion.cs
 *  Description:
 *  This sample shows basic use of FindSuggestions() call
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license libPath
 */  


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
using System.Diagnostics;
namespace Sample
{
    public class VideoSearchSuggestion
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
        private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class VideoSuggestionSearchEvents : GnStatusEventsDelegate
        {

            public override void status_event(gnsdk_status_t locale_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                return; // Remove to see status messages
                Console.WriteLine("\nPerforming Video Contributor lookup ...\t");
                Console.Write("status ");
                switch (locale_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Begin  ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write("Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }

            public override bool cancel_check()
            {
                return false;
            }
        };

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoSuggestionSearchEvents localeEvents = new VideoSuggestionSearchEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DoSampleSuggestionSearch(GnUser user)
        {
            uint match = 0;
            uint moreResults = 1;
            uint pageStart = 1;
            uint pageCount = 20;
            string suggestionTitle = null;
            string searchText = "spider";

            Console.WriteLine("\n*****Sample VideoID Suggestion Lookup*****\n");
            try
            {
                while (1 == moreResults)
                {

                    moreResults = 0;

                    GnVideo video = new GnVideo(user);

                    /* Set the search options */
                    video.OptionRangeStart(pageStart.ToString());

                    video.OptionRangeSize(pageCount.ToString());

                    /* Set the input text and perform the search with no optional data passed to callback function */
                    GnResponseVideoSuggestions responseVideoSuggestions = video.FindSuggestions(searchText, GnVideoSearchField.kSearchFieldProductTitle, GnVideoSearchType.kSearchTypeAnchored);

                    /* Handle the results. Retrieve number of search terms in result */
                    uint count = responseVideoSuggestions.ChildCount("gnsdk_val_suggestion_title" /*gnsdk_marshal.GNSDK_GDO_VALUE_SUGGESTION_TITLE*/);

                    uint rangeStart = Convert.ToUInt32(responseVideoSuggestions.RangeStart);
                    uint rangeEnd = Convert.ToUInt32(responseVideoSuggestions.RangeEnd);
                    uint rangeCount = Convert.ToUInt32(responseVideoSuggestions.RangeTotal);

                    if (count > 0)
                        Console.WriteLine("\t" + rangeStart + " - " + rangeEnd + " of " + rangeCount + " suggestions for \'" + searchText + "\'");

                    string type = responseVideoSuggestions.GetType();

                    for (uint i = 1; i <= count; i++)
                    {
                        suggestionTitle = responseVideoSuggestions.SuggestionTitle(i);
                        Console.WriteLine("\t" + ++match + ": " + suggestionTitle);
                    }

                    if (rangeCount > rangeEnd)
                    {
                        moreResults = 1;
                        pageStart += pageCount;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception Message: " + e.Message);
            }

        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    Console.OutputEncoding = Encoding.UTF8;
                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version\t: " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Do suggestion search */
                        DoSampleSuggestionSearch(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }
    }
}
