var group___manager =
[
    [ "Display Languages", "group___manager___display_languages.html", "group___manager___display_languages" ],
    [ "Error Management", "group___manager___error_management.html", "group___manager___error_management" ],
    [ "GNSDK Metrics", "group___manager___g_n_s_d_k_metrics.html", "group___manager___g_n_s_d_k_metrics" ],
    [ "Type Enums", "group___manager___types_enums.html", "group___manager___types_enums" ],
    [ "Utility Functions", "group___manager___utility_functions.html", "group___manager___utility_functions" ]
];