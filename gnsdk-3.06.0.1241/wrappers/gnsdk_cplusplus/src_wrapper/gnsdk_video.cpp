/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* gnsdk_video.cpp
 *
 * Implementation of C++ wrapper for GNSDK Video
 *
 */

#include "gnsdk.hpp"

#if GNSDK_VIDEO

using namespace gracenote;
using namespace gracenote::video;

/******************************************************************************
** GnVideo
*/

GnVideo::GnVideo(const GnUser& user, GnStatusEvents* pEventHandler)  throw ( GnError ) :
	v_event_handler(GNSDK_NULL)
{
	gnsdk_error_t error;

	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_VIDEO);

	if(pEventHandler)
	{
		v_event_handler = pEventHandler;
	}

	error = gnsdk_video_query_create(user.native(), _callback_status, this, &v_query_handle);
	if(error) { throw GnError(); }
}

GnVideo::~GnVideo()
{
	gnsdk_video_query_release(v_query_handle);
}

/*-----------------------------------------------------------------------------
 *  Version
 */
gnsdk_cstr_t GnVideo::Version()
{
	return gnsdk_video_get_version();
}

/*-----------------------------------------------------------------------------
 *  BuildDate
 */
gnsdk_cstr_t GnVideo::BuildDate()
{
	return gnsdk_video_get_build_date();
}

/*-----------------------------------------------------------------------------
 *  OptionPreferredLanguage
 */
void GnVideo::OptionPreferredLanguage(gnsdk_cstr_t languageOption) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_PREFERRED_LANG, languageOption);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionResultRange
 */
void GnVideo::OptionResultRange(gnsdk_cstr_t resultStart, gnsdk_cstr_t resultCount) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_RESULT_RANGE_START, resultStart);
	if(error) { throw GnError(); }

	error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_RESULT_RANGE_SIZE, resultCount);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionRangeStart
 */
void GnVideo::OptionRangeStart(gnsdk_cstr_t rangeStart) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_RESULT_RANGE_START, rangeStart);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionRangeSize
 */
void GnVideo::OptionRangeSize(gnsdk_cstr_t rangeSize) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_RESULT_RANGE_SIZE, rangeSize);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionEnableLinkData
 */
void GnVideo::OptionEnableLinkData(gnsdk_bool_t bEnableLinkData) throw ( GnError )
{
	gnsdk_error_t error;

	if(GNSDK_TRUE == bEnableLinkData)
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_LINK_DATA, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_LINK_DATA, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionEnableContentData
 */
void GnVideo::OptionEnableContentData(gnsdk_bool_t bEnableLinkData) throw ( GnError )
{
	gnsdk_error_t error;

	if(GNSDK_TRUE == bEnableLinkData)
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_CONTENT_DATA, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_CONTENT_DATA, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionEnableExternalID
 */
void GnVideo::OptionEnableExternalID(gnsdk_bool_t bEnableExternalId) throw ( GnError )
{
	gnsdk_error_t error;

	if(GNSDK_TRUE == bEnableExternalId)
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_EXTERNAL_IDS, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_ENABLE_EXTERNAL_IDS, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionQueryNoCache
 */
void GnVideo::OptionQueryNoCache(gnsdk_bool_t bNoCache) throw ( GnError )
{
	gnsdk_error_t error;

	if(GNSDK_TRUE == bNoCache)
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_QUERY_NOCACHE, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_QUERY_NOCACHE, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  OptionEnableCommerceType
 */
void GnVideo::OptionEnableCommerceType(gnsdk_bool_t bEnableCommerceType) throw ( GnError )
{
	gnsdk_error_t error;

	if(GNSDK_TRUE == bEnableCommerceType)
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_QUERY_ENABLE_COMMERCE_TYPE, GNSDK_VALUE_TRUE);
	}
	else
	{
		error = gnsdk_video_query_option_set(v_query_handle, GNSDK_VIDEO_OPTION_QUERY_ENABLE_COMMERCE_TYPE, GNSDK_VALUE_FALSE);
	}

	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FilterByListElement
 */
void GnVideo::FilterByListElement(gnsdk_bool_t bInclude, GnVideoListElementFilterType listElementFilterType, const gracenote::GnListElement& listElement) throw ( GnError )
{
	gnsdk_cstr_t  filter_key = GNSDK_NULL;
	gnsdk_error_t error;

	switch(listElementFilterType)
	{
	case kListElementFilterGenre:
		filter_key = bInclude ? GNSDK_VIDEO_FILTER_KEY_GENRE_INCLUDE : GNSDK_VIDEO_FILTER_KEY_GENRE_EXCLUDE;
		break;

	case kListElementFilterProductionType:
		filter_key = bInclude ? GNSDK_VIDEO_FILTER_KEY_PRODUCTION_TYPE_INCLUDE : GNSDK_VIDEO_FILTER_KEY_PRODUCTION_TYPE_EXCLUDE;
		break;

	case kListElementFilterSerialType:
		filter_key = bInclude ? GNSDK_VIDEO_FILTER_KEY_SERIAL_TYPE_INCLUDE : GNSDK_VIDEO_FILTER_KEY_SERIAL_TYPE_EXCLUDE;
		break;

	case kListElementFilterOrigin:
		filter_key = bInclude ? GNSDK_VIDEO_FILTER_KEY_ORIGIN_INCLUDE : GNSDK_VIDEO_FILTER_KEY_ORIGIN_EXCLUDE;
		break;

	default:
		break;
	}

	error = gnsdk_video_query_set_filter_by_list_element(v_query_handle, filter_key, listElement.native() );
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  Filter
 */
void GnVideo::Filter(gnsdk_cstr_t filterValue, GnVideoFilterType filterType) throw ( GnError )
{
	gnsdk_cstr_t  filter_key = GNSDK_NULL;
	gnsdk_error_t error;

	switch(filterType)
	{
	case kFilterSeasonNumber:
		filter_key = GNSDK_VIDEO_FILTER_KEY_SEASON_NUM;
		break;

	case kFilterSeasonEpisodeNumber:
		filter_key = GNSDK_VIDEO_FILTER_KEY_SEASON_EPISODE_NUM;
		break;

	case kFilterSeriesEpisodeNumber:
		filter_key = GNSDK_VIDEO_FILTER_KEY_SERIES_EPISODE_NUM;
		break;

	default:
		break;
	}

	error = gnsdk_video_query_set_filter(v_query_handle, filter_key, filterValue);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  ExternalIdTypeSet
 */
void GnVideo::ExternalIdTypeSet(gnsdk_cstr_t externalId, GnVideoExternalIdType externalIdType) throw ( GnError )
{
	gnsdk_cstr_t  id_key = GNSDK_NULL;
	gnsdk_error_t error;

	switch(externalIdType)
	{
	case kExternalIdTypeISAN:
		id_key = GNSDK_VIDEO_EXTERNAL_ID_TYPE_ISAN;
		break;

	case kExternalIdTypeUPC:
		id_key = GNSDK_VIDEO_EXTERNAL_ID_TYPE_UPC;
		break;

	default:
		break;
	}

	error = gnsdk_video_query_set_external_id(v_query_handle, externalId, id_key, GNSDK_VIDEO_EXTERNAL_ID_SOURCE_DEFAULT);
	if(error) { throw GnError(); }
}

/*-----------------------------------------------------------------------------
 *  FindProducts
 */
GnResponseVideoProduct GnVideo::FindProducts(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindProduct();
}

/*-----------------------------------------------------------------------------
 *  FindProducts
 */
GnResponseVideoProduct GnVideo::FindProducts(gnsdk_cstr_t search_text, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError )
{
	gnsdk_cstr_t  field_key;
	gnsdk_error_t error;

	field_key = _MapSearchField(searchField);

	error = gnsdk_video_query_set_text(v_query_handle, field_key, search_text, (gnsdk_video_search_type_t)searchType);
	if(error) { throw GnError(); }

	return _FindProduct();
}

/*-----------------------------------------------------------------------------
 *  FindProducts
 */
GnResponseVideoProduct GnVideo::FindProducts(gnsdk_cstr_t videoTOC, GnVideoTOCFlag TOCFlag) throw ( GnError )
{
	gnsdk_uint32_t toc_flag = GNSDK_NULL;
	gnsdk_error_t  error;

	switch(TOCFlag)
	{
	case kTOCFlagDefault:
		toc_flag = GNSDK_VIDEO_TOC_FLAG_DEFAULT;
		break;

	case kTOCFlagPal:
		toc_flag = GNSDK_VIDEO_TOC_FLAG_PAL;
		break;

	case kTOCFlagAngles:
		toc_flag = GNSDK_VIDEO_TOC_FLAG_ANGLES;
		break;

	default:
		break;
	}

	error = gnsdk_video_query_set_toc_string(v_query_handle, videoTOC, toc_flag);
	if(error) { throw GnError(); }

	return _FindProduct();
}

/*-----------------------------------------------------------------------------
 *  FindWorks
 */
GnResponseVideoWork GnVideo::FindWorks(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindWork();
}

/*-----------------------------------------------------------------------------
 *  FindWorks
 */
GnResponseVideoWork GnVideo::FindWorks(gnsdk_cstr_t search_text, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError )
{
	gnsdk_cstr_t  field_key;
	gnsdk_error_t error;

	field_key = _MapSearchField(searchField);

	error = gnsdk_video_query_set_text(v_query_handle, field_key, search_text, (gnsdk_video_search_type_t)searchType);
	if(error) { throw GnError(); }

	return _FindWork();
}

/*-----------------------------------------------------------------------------
 *  FindSeasons
 */
GnResponseVideoSeasons GnVideo::FindSeasons(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindSeasons();
}

/*-----------------------------------------------------------------------------
 *  FindSeries
 */
GnResponseVideoSeries GnVideo::FindSeries(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindSeries();
}

/*-----------------------------------------------------------------------------
 *  FindSeries
 */
GnResponseVideoSeries GnVideo::FindSeries(gnsdk_cstr_t textInput, GnVideoSearchType searchType) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_text(v_query_handle, GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE, textInput, (gnsdk_video_search_type_t)searchType);
	if(error) { throw GnError(); }

	return _FindSeries();
}

/*-----------------------------------------------------------------------------
 *  FindContributors
 */
GnResponseContributor GnVideo::FindContributors(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindContributors();
}

/*-----------------------------------------------------------------------------
 *  FindContributors
 */
GnResponseContributor GnVideo::FindContributors(gnsdk_cstr_t textInput, GnVideoSearchType searchType) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_text(v_query_handle, GNSDK_VIDEO_SEARCH_FIELD_CONTRIBUTOR_NAME, textInput, (gnsdk_video_search_type_t)searchType);
	if(error) { throw GnError(); }

	return _FindContributors();
}

/*-----------------------------------------------------------------------------
 *  FindPrograms
 */
GnResponseVideoProgram GnVideo::FindPrograms(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindPrograms();
}

/*-----------------------------------------------------------------------------
 *  FindObjects
 */
GnResponseVideoObjects GnVideo::FindObjects(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindObjects();
}

/*-----------------------------------------------------------------------------
 *  FindSuggestions
 */
GnResponseVideoSuggestions GnVideo::FindSuggestions(GnDataObject& gnObj) throw ( GnError )
{
	gnsdk_error_t error;

	error = gnsdk_video_query_set_gdo(v_query_handle, gnObj.native() );
	if(error) { throw GnError(); }

	return _FindSuggestions();
}

/*-----------------------------------------------------------------------------
 *  FindSuggestions
 */
GnResponseVideoSuggestions GnVideo::FindSuggestions(gnsdk_cstr_t search_text, GnVideoSearchField searchField, GnVideoSearchType searchType) throw ( GnError )
{
	gnsdk_cstr_t  field_key;
	gnsdk_error_t error;

	field_key = _MapSearchField(searchField);

	error = gnsdk_video_query_set_text(v_query_handle, field_key, search_text, (gnsdk_video_search_type_t)searchType);
	if(error) { throw GnError(); }

	return _FindSuggestions();
}

/*-----------------------------------------------------------------------------
 *  _FindSuggestions
 */
GnResponseVideoSuggestions GnVideo::_FindSuggestions() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_suggestions(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoSuggestions tmp = GnResponseVideoSuggestions(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindObjects
 */
GnResponseVideoObjects GnVideo::_FindObjects() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_objects(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoObjects tmp = GnResponseVideoObjects(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindPrograms
 */
GnResponseVideoProgram GnVideo::_FindPrograms() throw ( GnError )
{
/*
 *    gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
 *    gnsdk_error_t      error;
 *
 *    error = gnsdk_video_query_find_programs(v_query_handle, &response_gdo);
 *    if ( error ) { throw GnError( ); }
 *
 *    GnResponseVideoProgram tmp = GnResponseVideoProgram(response_gdo);
 *
 *    error = gnsdk_manager_gdo_release(response_gdo);
 *    if ( error ) { throw GnError( ); }
 */
	return GnResponseVideoProgram(GNSDK_NULL);
}

/*-----------------------------------------------------------------------------
 *  _FindContributors
 */
GnResponseContributor GnVideo::_FindContributors() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_contributors(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseContributor tmp = GnResponseContributor(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindSeries
 */
GnResponseVideoSeries GnVideo::_FindSeries() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_series(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoSeries tmp = GnResponseVideoSeries(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindSeasons
 */
GnResponseVideoSeasons GnVideo::_FindSeasons() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_seasons(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoSeasons tmp = GnResponseVideoSeasons(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindWork
 */
GnResponseVideoWork GnVideo::_FindWork() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_works(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoWork tmp = GnResponseVideoWork(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _FindProduct
 */
GnResponseVideoProduct GnVideo::_FindProduct() throw ( GnError )
{
	gnsdk_gdo_handle_t response_gdo = GNSDK_NULL;
	gnsdk_error_t      error;

	error = gnsdk_video_query_find_products(v_query_handle, &response_gdo);
	if(error) { throw GnError(); }

	GnResponseVideoProduct tmp = GnResponseVideoProduct(response_gdo);

	error = gnsdk_manager_gdo_release(response_gdo);
	if(error) { throw GnError(); }

	return tmp;
}

/*-----------------------------------------------------------------------------
 *  _MapSearchField
 */
gnsdk_cstr_t GnVideo::_MapSearchField(GnVideoSearchField searchField)
{
	gnsdk_cstr_t field_key = GNSDK_NULL;

	switch(searchField)
	{
	case kSearchFieldContributorName:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_CONTRIBUTOR_NAME;
		break;

	case kSearchFieldCharacterName:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_CHARACTER_NAME;
		break;

	case kSearchFieldWorkFranchise:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_WORK_FRANCHISE;
		break;

	case kSearchFieldWorkSeries:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_WORK_SERIES;
		break;

	case kSearchFieldWorkTitle:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_WORK_TITLE;
		break;

	case kSearchFieldProductTitle:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_PRODUCT_TITLE;
		break;

	case kSearchFieldSeriesTitle:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_SERIES_TITLE;
		break;

	case kSearchFieldAll:
		field_key = GNSDK_VIDEO_SEARCH_FIELD_ALL;
		break;

	default:
		break;
	}

	return field_key;
}

/*-----------------------------------------------------------------------------
 *  _callback_status
 */
void GNSDK_CALLBACK_API GnVideo::_callback_status(void* callback_data,
	gnsdk_status_t                                      status,
	gnsdk_uint32_t                                      percent_complete,
	gnsdk_size_t                                        bytes_total_sent,
	gnsdk_size_t                                        bytes_total_received,
	gnsdk_bool_t*                                       p_abort
    )
{
	GnVideo* p_video = (GnVideo*)callback_data;

	if(p_video->v_event_handler)
	{
		p_video->v_event_handler->status_event(status, percent_complete, bytes_total_sent, bytes_total_received);
		if(p_video->v_event_handler->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}
#endif /* GNSDK_VIDEO */
