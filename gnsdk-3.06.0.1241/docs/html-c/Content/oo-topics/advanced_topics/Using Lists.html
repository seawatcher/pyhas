<!DOCTYPE html>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" lang="en-us" xml:lang="en-us" class="no-feedback" data-mc-search-type="Stem" data-mc-help-system-file-name="index.xml" data-mc-path-to-help-system="../../../" data-mc-target-type="WebHelp2" data-mc-runtime-file-type="Topic" data-mc-preload-images="false" data-mc-in-preview-mode="false" data-mc-toc-path="">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>Using Lists</title>
        <link href="../../../Skins/Default/Stylesheets/TextEffects.css" rel="stylesheet" />
        <link href="../../../Skins/Default/Stylesheets/Topic.css" rel="stylesheet" />
        <link href="../../Resources/Stylesheets/BookStyles.css" rel="stylesheet" />
        <script src="../../../Resources/Scripts/jquery.min.js">
        </script>
        <script src="../../../Resources/Scripts/plugins.min.js">
        </script>
        <script src="../../../Resources/Scripts/MadCapAll.js">
        </script>
    </head>
    <body>
        <h1><a name="_Toc334991803"></a>Using Lists</h1>
        <p><span class="GNVariablesProductName">GNSDK</span> uses list structures to store strings and other information that do not directly appear in results returned from the Gracenote Service. Lists generally contain information such as localized strings and region-specific information. Each list is contained in a corresponding <i>List Type</i>.</p>
        <p>Lists are either <i>flat</i> or <i>hierarchical</i>. Flat lists contain only one level of metadata, such as  languages. Hierarchical lists are tree-like structures with parents and children. </p>
        <p>Typically, hierarchical lists contain general display strings at the upper levels (Level 1) and more granular strings at the lower levels (Level 2 and Level 3, respectively). For example, a parent Level 1 music genre of Rock contains a grandchild Level 3 genre of Rock Opera. The application can determine what level of list granularity is needed by using the list functions (gnsdk_sdkmgr_list_*) in the GNSDK Manager. For more information, see <a href="../../shared-topics/concepts/Core and Enriched Metadata.html" class="GNBasic MCXref xref xrefGNBasic">Core and Enriched Metadata</a>.</p>
        <p>Lists can be specific to a <i>Region</i>, as well as a <i>Language</i>. For example, the music genre known as J-pop (Japanese pop) in America is called pop in Japan.</p>
        <p>In general, Lists provide:</p>
        <ul>
            <li value="1">Mappings from Gracenote IDs to Gracenote Descriptors, in various languages</li>
            <li value="2">Delivery of content that powers features such as MoreLikeThis, <span class="MCTextPopup MCTextPopup MCTextPopupHotSpot MCTextPopup_Open MCTextPopupHotSpot_ MCTextPopupHotSpot_Popup #text #textPopup">Playlist<span class="MCTextPopupBody popupBody"><span class="MCTextPopupArrow"></span>A set of tracks from a user’s music collection, generated according to the criteria and limits defined by a playlist generator.</span></span>, and MoodGrid</li>
        </ul>
        <p class="note">MoreLikeThis, Playlist, and MoodGrid also use <i>Correlates</i>. These lists specify the correlations among different genres, moods, and so on. For example, Punk correlates higher to Rock than it does to Country. Using the MoreLikeThis feature when playing a Punk track will likely return more Rock suggestions than Country. </p>
        <h2><a name="_Toc334991806"></a>List and Locale Interdependence</h2>
        <p>GNSDK for Auto provides <em>Locales</em> as a way to group lists specific to a region and language. Using locales is relatively straightforward for most applications to implement. However, it is not as flexible as directly accessing lists - most of the processing is done in the background and is not configurable. For more information, see <a href="../basic_app_design/Using Locales.html" class="GNBasic MCXref xref xrefGNBasic">Using Locales</a>.</p>
        <p>Conversely, the List functionality is robust, flexible, and useful in complex applications. However, it requires more implementation design and maintenance. It also does not support non-list-based metadata and the <span class="MCTextPopup MCTextPopup MCTextPopupHotSpot MCTextPopup_Open MCTextPopupHotSpot_ MCTextPopupHotSpot_Popup #text #textPopup">GDO<span class="MCTextPopupBody popupBody"><span class="MCTextPopupArrow"></span>Gracenote Data Object: Containers most-commonly used to store information returned by a query. GDOs can contain metadata values (names, titles, external IDs, and so on) that can be accessed by an application using Value keys.  GDOs can also contain references to other GDOs, which can be accessed using Child GDO keys.</span></span> value keys (GNSDK_GDO_VALUE_*).</p>
        <p>For most applications, using locales is more than sufficient. In cases where the application requires non-locale functionality (for example, list display), implementing both methods may be necessary. The following&#160; sections discuss each method's advantages and disadvantages.</p>
        <p>The GNSDK locale functionality includes:</p>
        <ul>
            <li value="1">Loading a locale into the application using gnsdk_manager_gdo_load_locale(). GNSDK loads a locale behind the scenes, and loads only those lists that are not already in memory. When  database (such as  SQLite) cache is enabled, the GNSDK automatically caches the list. When caching is not enabled, GNSDK downloads the locale for each request.</li>
            <li style="margin-top:10px;" value="2">You can set a loaded locale as the application’s default locale, and this can eliminate the need to call gnsdk_manager_set_locale() for a GDO . Instead, you call the default locale.</li>
            <li style="margin-top:10px;" value="3">Setting a locale for a GDO using gnsdk_manager_gdo_set_locale(). Doing this ensures that all language-specific metadata is returned with the language (and region and descriptor, if applicable) defined for the locale.</li>
        </ul>
        <p>When using locales, be aware that:</p>
        <ul>
            <li value="1">You can serialize locales and lists and save them within your application for later re-use. To store non-serialized locales and lists, you must implement a database cache. If you do not want to use a cache, you can instead download required locales and lists during your application’s initialization.</li>
            <li style="margin-top:10px;" value="2">A list’s contents cannot be displayed (for example, to present a list of available genres to a user). </li>
            <li style="margin-top:10px;" value="3">Performing advanced list functionality, such as displaying list items in a dropdown box for a user’s selection, or accessing list elements for filtering lookups (for example, restricting lookups to a particular mood or tempo), is not possible.</li>
        </ul>
        <p>The <span class="GNVariablesProductName">GNSDK</span> list functionality includes:
</p>
        <ul>
            <li value="1">Directly accessing individual lists using the gnsdk_manager_list_* APIs.</li>
            <li value="2">Accessing locale-specific list metadata.</li>
        </ul>
        <p>When using lists, be aware that:</p>
        <ul>
            <li value="1">There are numerous list handles.</li>
            <li value="2">List handles must be released after use.</li>
            <li value="3">Locale-specific non-list metadata is not supported.</li>
            <li value="4">List metadata GDO keys are not supported.</li>
            <li value="5">When using gnsdk_manager_list_retrieve() to get a list, you must provide a user handle. </li>
        </ul>
        <h2>Updating Lists</h2>
        <p>To update lists, follow the procedure described in <a href="../basic_app_design/Using Locales.html#Updating" class="GNBasic MCXref xref xrefGNBasic">Updating Locales and Lists</a>. The following list-specific functions are available:</p>
        <ul>
            <li value="1">gnsdk_manager_list_update()</li>
            <li value="2">gnsdk_manager_list_update_check()</li>
        </ul>
        <h2 class="example"><a name="_Toc334991805"></a>Example: Accessing a Music Genre List</h2>
        <p>This example demonstrates accessing a list and displaying the list's elements.</p>
        <p>Code Snippet: <a href="../../samples/code_snippets/musicid_list/main.c">code_snippets/musicid_list/main.c</a></p>
        <p class="warning">Warning: Code snippets are provided "as is" for documentation purposes only.  Their intention is to illustrate a specific function or feature. We strongly recommend you use the sample and reference applications as a basis for any applications you develop.</p>
        <p>Application Steps:</p>
        <ol>
            <li value="1">Initialize GNSDK Manager and get User handle.</li>
            <li value="2">Get music genre list with gnsdk_manager_list_retrieve().</li>
            <li value="3">Make SDK list calls (gnsdk_manager_list_*) to get list type, language, region, descriptor and # of levels and display.</li>
            <li value="4">Make SDK list calls for each level element and display list ID and name.</li>
            <li value="5">Release the list.</li>
            <li value="6">Release resources and shutdown SDK.</li>
        </ol>
        <p><b>Snippet output:</b>
        </p><pre>GNSDK Product Version    : 3.05.0.798 	(built 2013-05-08 16:09-0700)

Retrieving music genre list ...
____________________________________________________________
List:
	gnsdk_list_type_genres
	gnsdk_region_global
	gnsdk_desc_detailed
	eng
	3 levels
________________________________________________________________

		 [list id] - [display string]
Level -  1
		 [ 24041 ] - [ Alternative ]
		 [ 35286 ] - [ Punk ]
		 [ 35287 ] - [ Indie Rock ]
		 [ 35288 ] - [ Metal ]
		 [ 24045 ] - [ Rock ]
		 [ 24046 ] - [ Pop ]
		 [ 24047 ] - [ Easy Listening ]
		 [ 35289 ] - [ Dance &amp; House ]
		 [ 35290 ] - [ Electronica ]
		 [ 35291 ] - [ R&amp;B ]
		 [ 35292 ] - [ Rap ]
		 [ 24052 ] - [ Jazz ]
		 [ 35293 ] - [ Classical ]
		 [ 35294 ] - [ Blues ]
		 [ 35295 ] - [ Country &amp; Folk ]
		 [ 24059 ] - [ Traditional ]
		 [ 35296 ] - [ Soundtrack ]
		 [ 24054 ] - [ Religious ]
		 [ 24057 ] - [ Latin ]
		 [ 35297 ] - [ Reggae ]
		 [ 24061 ] - [ Children's ]
		 [ 35298 ] - [ Spoken &amp; Audio ]
		 [ 35299 ] - [ Holiday ]
		 [ 35300 ] - [ New Age ]
		 [ 24065 ] - [ Other ]
Level -  2
		 [ 24066 ] - [ Alt Metal ]
		 [ 35301 ] - [ Alternative Rock ]
		 [ 24068 ] - [ Funk Metal ]
		 [ 24069 ] - [ Alternative Dance ]
		 [ 24070 ] - [ Grunge ]
		 [ 24071 ] - [ Alternative Pop ]
		 [ 24072 ] - [ Alternative Singer-Songwriter ]
		 [ 35307 ] - [ Ska Revival ]
		 [ 24141 ] - [ New Wave ]
		 [ 70826 ] - [ Rockabilly Revival ]
		 [ 70827 ] - [ Garage Rock Revival ]
		 [ 70828 ] - [ Surf Revival ]
		 [ 70829 ] - [ Swing Revival ]
		 [ 35302 ] - [ Industrial ]
		 [ 24075 ] - [ Goth ]
		 [ 35303 ] - [ Old School Punk ]
		 [ 24077 ] - [ Classic Pop Punk ]
		 [ 35304 ] - [ Fast Punk ]
		 [ 24079 ] - [ Pop Punk ]
		 [ 35305 ] - [ Other Punk ]
		 [ 35306 ] - [ Emo &amp; Hardcore ]
		 [ 65162 ] - [ Brit Rock ]
		 [ 65163 ] - [ Brit Pop ]
		 [ 24083 ] - [ Alternative Folk ]
		 [ 35308 ] - [ Post-Punk ]
Level -  3
		 [ 24333 ] - [ Nu-Metal ]
		 [ 24334 ] - [ Rap Metal ]
		 [ 24335 ] - [ General Alternative Rock ]
		 [ 24336 ] - [ Alternative Rap-Rock ]
		 [ 24337 ] - [ Pre-Grunge Alternative Rock ]
		 [ 24338 ] - [ Post-Grunge Alternative Rock ]
		 [ 24339 ] - [ Funk Metal ]
		 [ 24340 ] - [ Alternative Dance ]
		 [ 24341 ] - [ Grunge ]
		 [ 24342 ] - [ Alternative Pop ]
		 [ 24343 ] - [ Alternative Pop Singer-Songwriter ]
		 [ 24344 ] - [ Alternative Rock Singer-Songwriter ]
		 [ 24382 ] - [ Two-Tone Ska Revival ]
		 [ 24383 ] - [ Modern Ska ]
		 [ 24384 ] - [ Ska Punk ]
		 [ 24684 ] - [ Neue Deutche Welle ]
		 [ 24685 ] - [ New Wave Pop ]
		 [ 24686 ] - [ New Wave Dance ]
		 [ 35426 ] - [ New Wave Quirk ]
		 [ 24688 ] - [ Synth Pop ]
		 [ 24689 ] - [ New Romantic ]
		 [ 24690 ] - [ New Wave Rock ]
		 [ 24691 ] - [ Pub Rock ]
		 [ 24692 ] - [ Mod Revival ]
		 [ 24693 ] - [ Original New Wave Scene ]
      </pre>
        <p class="onlineFooter">© 2000 to present. Gracenote, Inc. All rights reserved.</p>
        <p><a href="mailto:doc_feedback@gracenote.com?subject=Gracenote Documentation Feedback" target="_blank" title="Send comments about this topic to Gracenote Technical Publications." alt="Send comments about this topic to Gracenote Technical Publications.">How can we improve this documentation?</a>
        </p>
    </body>
</html>