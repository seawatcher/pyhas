/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnLocale extends GnObject {
  private long swigCPtr;

  protected GnLocale(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnLocale_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnLocale obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnLocale(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnLocale(String locale_group, String language, String region, String descriptor, GnUser user, GnStatusEventsListener status_events) throws com.gracenote.gnsdk.GnException {
    this(gnsdk_javaJNI.new_GnLocale__SWIG_0(locale_group, language, region, descriptor, GnUser.getCPtr(user), user, GnStatusEventsListener.getCPtr(status_events), status_events), true);
  }

  public GnLocale(String locale_group, String language, String region, String descriptor, GnUser user) throws com.gracenote.gnsdk.GnException {
    this(gnsdk_javaJNI.new_GnLocale__SWIG_1(locale_group, language, region, descriptor, GnUser.getCPtr(user), user), true);
  }

  public GnLocale(String serialized_locale, GnStatusEventsListener status_events) throws com.gracenote.gnsdk.GnException {
    this(gnsdk_javaJNI.new_GnLocale__SWIG_2(serialized_locale, GnStatusEventsListener.getCPtr(status_events), status_events), true);
  }

  public GnLocale(String serialized_locale) throws com.gracenote.gnsdk.GnException {
    this(gnsdk_javaJNI.new_GnLocale__SWIG_3(serialized_locale), true);
  }

  public String group() throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_group(swigCPtr, this);
  }

  public String descriptor() throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_descriptor(swigCPtr, this);
  }

  public String language() throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_language(swigCPtr, this);
  }

  public String region() throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_region(swigCPtr, this);
  }

  public String revision() throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_revision(swigCPtr, this);
  }

  public boolean update(GnUser user) throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_update(swigCPtr, this, GnUser.getCPtr(user), user);
  }

  public boolean updateCheck(GnUser user) throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLocale_updateCheck(swigCPtr, this, GnUser.getCPtr(user), user);
  }

  public GnString serialize() throws com.gracenote.gnsdk.GnException {
    return new GnString(gnsdk_javaJNI.GnLocale_serialize(swigCPtr, this), true);
  }

}
