var class_gracenote_s_d_k_1_1gnsdk__error__info__t =
[
    [ "gnsdk_error_info_t", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a5f26dd685257c51f5f80a1bdda3be11a", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#aa86dce20b95fa21507bfd6fe4396f919", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a27ecec136df6f4abb6f6301ec6815e2a", null ],
    [ "error_api", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a0bac685613de756496ff5aa44db25bab", null ],
    [ "error_code", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a4317ce0795e33dcd43dc47dd5cb3aeeb", null ],
    [ "error_description", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#abe4eae682faa74fa9e0bcba7af055619", null ],
    [ "error_module", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a4654eb473160d9197ab05479209baebe", null ],
    [ "source_error_code", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#aaf6c9287eb2a2a1cc8626ac4a7fbea1a", null ],
    [ "source_error_module", "class_gracenote_s_d_k_1_1gnsdk__error__info__t.html#a5532f52f75fd5ba8cf830f39a4aad606", null ]
];