/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnVideoLayerEnumerator : System.Collections.Generic.IEnumerator<GnVideoLayer>, IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnVideoLayerEnumerator(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnVideoLayerEnumerator obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnVideoLayerEnumerator() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnVideoLayerEnumerator(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

			public bool
			MoveNext( )
			{
				return hasNext( );
			}

			public GnVideoLayer Current {
				get {
					return next( );
				}
			}
			object System.Collections.IEnumerator.Current {
				get {
					return Current;
				}
			}
			public void
			Reset( )
			{
			}

		
  public GnVideoLayer next() {
    GnVideoLayer ret = new GnVideoLayer(gnsdk_csharp_marshalPINVOKE.GnVideoLayerEnumerator_next(swigCPtr), true);
    return ret;
  }

  public bool hasNext() {
    bool ret = gnsdk_csharp_marshalPINVOKE.GnVideoLayerEnumerator_hasNext(swigCPtr);
    return ret;
  }

  public uint distance(GnVideoLayerEnumerator itr) {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnVideoLayerEnumerator_distance(swigCPtr, GnVideoLayerEnumerator.getCPtr(itr));
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public GnVideoLayerEnumerator(GnVideoLayerProvider provider, uint pos, uint count) : this(gnsdk_csharp_marshalPINVOKE.new_GnVideoLayerEnumerator__SWIG_0(GnVideoLayerProvider.getCPtr(provider), pos, count), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnVideoLayerEnumerator(GnVideoLayerProvider provider, uint pos, uint count, GnVideoLayer start) : this(gnsdk_csharp_marshalPINVOKE.new_GnVideoLayerEnumerator__SWIG_1(GnVideoLayerProvider.getCPtr(provider), pos, count, GnVideoLayer.getCPtr(start)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
