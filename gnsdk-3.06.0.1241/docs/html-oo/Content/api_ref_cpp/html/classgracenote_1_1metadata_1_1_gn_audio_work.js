var classgracenote_1_1metadata_1_1_gn_audio_work =
[
    [ "GnAudioWork", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a68c833db807efee0a0b72bf246008101", null ],
    [ "~GnAudioWork", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a619546199f58dce1b13012e8e4172985", null ],
    [ "CompositionForm", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a568b2af3e4b8dd493b7eb9f3fb613325", null ],
    [ "Credit", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a8091db14eb127315cf97bd62feaf9abe", null ],
    [ "Era", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a4143eca5f1ba46381d7e6add2581e0d9", null ],
    [ "Genre", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a56d1c58bfdbd23424107e25cf8ec3a03", null ],
    [ "GnID", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a324eee4327c95f71d138d79a302809a5", null ],
    [ "GnUID", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a4ed6446d04261b98a0867a43322ff688", null ],
    [ "Origin", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a8195b100c2cd6cdbc883561592e069de", null ],
    [ "TagID", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a4e35fdb5f85f2f82fec2a5a5cce7a3bb", null ],
    [ "Title", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a01849ec4ee46dcc34b86ea7c70cc311f", null ],
    [ "TUI", "classgracenote_1_1metadata_1_1_gn_audio_work.html#a5f99fe25b178f5576f1d242a4e25f749", null ],
    [ "TUITag", "classgracenote_1_1metadata_1_1_gn_audio_work.html#ae8c547bb8db91efac7950f44984a7e68", null ]
];