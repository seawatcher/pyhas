var group___setup___lookup_local___functions =
[
    [ "gnsdk_lookup_local_get_build_date", "group___setup___lookup_local___functions.html#gaa162c032765f1e2b2e49c65a0b75eac6", null ],
    [ "gnsdk_lookup_local_get_version", "group___setup___lookup_local___functions.html#ga3ea18293c99317bbd3a84f0371cccab4", null ],
    [ "gnsdk_lookup_local_initialize", "group___setup___lookup_local___functions.html#ga73108da4ef34f95d51755616e22c8666", null ],
    [ "gnsdk_lookup_local_shutdown", "group___setup___lookup_local___functions.html#ga1bd5265b061f4b4890aa3845863bd3cf", null ],
    [ "gnsdk_lookup_local_storage_compact", "group___setup___lookup_local___functions.html#ga2c471bc2248eb0dfc99f604bd7258f2d", null ],
    [ "gnsdk_lookup_local_storage_info_count", "group___setup___lookup_local___functions.html#ga0c830a23db0200f400bf53cdb6d4f2e3", null ],
    [ "gnsdk_lookup_local_storage_info_get", "group___setup___lookup_local___functions.html#gab2d86a123b2214cacdd03c0239fb3cf7", null ],
    [ "gnsdk_lookup_local_storage_location_set", "group___setup___lookup_local___functions.html#ga77443fd6b0ce5c40342d8978de70b1ca", null ],
    [ "gnsdk_lookup_local_storage_validate", "group___setup___lookup_local___functions.html#ga3686c3ad2cd71e6012c59c945d3d5f96", null ],
    [ "gnsdk_lookup_local_storage_version_get", "group___setup___lookup_local___functions.html#ga4b23e2feecad1eb5cf893104a269680e", null ]
];