var a00138 =
[
    [ "swigValue", "a00138.html#ae618846126d8f295e89546ce5fbc9c6c", null ],
    [ "gnsdk_status_begin", "a00138.html#a7ec2e49b785d5f38cea089d2a078fdfc", null ],
    [ "gnsdk_status_complete", "a00138.html#ab5480d9b01a87a7325673c7915abbe7f", null ],
    [ "gnsdk_status_connecting", "a00138.html#a5ad6b72bc4fab96c00f37c509277123b", null ],
    [ "gnsdk_status_disconnected", "a00138.html#a6a934b658287c8957be6c40804b33088", null ],
    [ "gnsdk_status_error_info", "a00138.html#abc89203bb35887eaa5cc0b28e5315bfe", null ],
    [ "gnsdk_status_progress", "a00138.html#a8d507904d7eb0de6f8ef408cac5f931c", null ],
    [ "gnsdk_status_reading", "a00138.html#a08a4bf3072ea6dc0a5d656911cdf669b", null ],
    [ "gnsdk_status_receiving", "a00138.html#a326f42209551ec6334a69ead7a287dcf", null ],
    [ "gnsdk_status_sending", "a00138.html#aa7721af8e10b83067feea4dd0a717568", null ],
    [ "gnsdk_status_unknown", "a00138.html#adf141e22ec24ad94914db6ebcecc7966", null ],
    [ "gnsdk_status_writing", "a00138.html#a48b97c838ab80b12eca70341be32a471", null ]
];