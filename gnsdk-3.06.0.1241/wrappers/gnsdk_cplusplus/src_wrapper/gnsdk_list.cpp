/*
 * Copyright (c) 2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/* link.cpp
 *
 * Implementation of C++ wrapper for GNSDK
 *
 */
#include "gnsdk.hpp"
#include "gnsdk_list.hpp"
#include "metadata.hpp"

using namespace gracenote;

/**************************************************************************
 * list_element_child_provider
 **/


GnListElement 
list_element_child_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_list_element_handle_t list_element = GNSDK_NULL;

	gnsdk_error_t error = gnsdk_manager_list_element_get_child(elementHandle_, pos, &list_element);
	if(error) { throw GnError(); }

	GnListElement result(list_element);

	error = gnsdk_handle_release(list_element);
	if(error) { throw GnError(); }

	return  result;
}

/**************************************************************************
 * GnListElement
 **/


gnsdk_cstr_t GnListElement::DisplayString() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  display_string = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_display_string(get<gnsdk_list_element_handle_t>(), &display_string);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return display_string;
}

gnsdk_uint32_t GnListElement::ID() throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t id = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_id(get<gnsdk_list_element_handle_t>(), &id);
	if(error) { throw GnError(); }

	return id;
}

gnsdk_uint32_t GnListElement::IDForSubmit() throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t submit_id = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_id_for_submit(get<gnsdk_list_element_handle_t>(), &submit_id);
	if(error) { throw GnError(); }

	return submit_id;
}

gnsdk_cstr_t GnListElement::Description() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  val = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_value(get<gnsdk_list_element_handle_t>(), GNSDK_LIST_KEY_DESC, &val);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return val;
}

gnsdk_cstr_t GnListElement::RatingTypeID() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  val = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_value(get<gnsdk_list_element_handle_t>(), GNSDK_LIST_KEY_RATINGTYPE_ID, &val);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	return val;
}

GnListElement GnListElement::Parent()  throw ( GnError )
{
	gnsdk_error_t               error;
	gnsdk_list_element_handle_t parent_handle = GNSDK_NULL;

	error = gnsdk_manager_list_element_get_parent(get<gnsdk_list_element_handle_t>(), &parent_handle);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	GnListElement result;
	result.AcceptOwnership(parent_handle);

	return result;
}

gnsdk_uint32_t GnListElement::Level()  throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t level;

	error = gnsdk_manager_list_element_get_level(get<gnsdk_list_element_handle_t>(), &level);
	if(error) { throw GnError(); }

	return level;
}


gn_iterable_container<GnListElement::list_element_child_iterator> GnListElement::Children() throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t count = 0;

	error = gnsdk_manager_list_element_get_child_count(get<gnsdk_list_element_handle_t>(), &count);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	list_element_child_provider provider(get<gnsdk_list_element_handle_t>());
	return gn_iterable_container<GnListElement::list_element_child_iterator>(
						list_element_child_iterator(provider, 0, count) , 
						list_element_child_iterator(provider, count, count) 
						);
}

/**************************************************************************
 * list_element_provider
 **/


GnListElement 
list_element_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_list_element_handle_t list_element = GNSDK_NULL;
	
	gnsdk_error_t error = gnsdk_manager_list_get_element(listHandle_, level_, pos, &list_element);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }

	GnListElement result(list_element);
	
	error = gnsdk_handle_release(list_element);
	if(GNSDKERR_SEVERE(error) ) { throw GnError(); }
	
	return result;
}



/**************************************************************************
 * GnList
 **/
GnList::GnList(gnsdk_cstr_t strListType, gnsdk_cstr_t strLanguage, gnsdk_cstr_t strRegion, gnsdk_cstr_t strDescriptor, const GnUser& user, GnStatusEvents* list_events)  throw ( GnError )
	: event_handler_(list_events)
{
	gnsdk_list_handle_t list_handle = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_manager_list_retrieve(	strListType,
														strLanguage,
														strRegion,
														strDescriptor,
														user.native(),
														_list_status_callback,
														this,
														&list_handle);

	if(error) { throw GnError(); }
	this->AcceptOwnership(list_handle);
}

GnList::GnList(gnsdk_cstr_t strListType, GnLocale& locale, const GnUser& user, GnStatusEvents* list_events)  throw ( GnError )
	: event_handler_(list_events)
{
	gnsdk_list_handle_t list_handle = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_manager_list_retrieve(	strListType,
														locale.Language(),
														locale.Region(),
														locale.Descriptor(),
														user.native(),
														_list_status_callback,
														this,
														&list_handle);
	if(error) { throw GnError(); }
	this->AcceptOwnership(list_handle);

}

GnList::GnList(gnsdk_cstr_t serialized_list) throw ( GnError )
{
	
	gnsdk_list_handle_t list_handle = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_manager_list_deserialize(serialized_list, &list_handle);
	if(error) { throw GnError(); }

	this->AcceptOwnership(list_handle);
}



bool GnList::Update(const GnUser& user) throw ( GnError )
{
	gnsdk_error_t       error;
	bool                result        = false;
	gnsdk_list_handle_t update_handle = GNSDK_NULL;

	error = gnsdk_manager_list_update(get<gnsdk_list_handle_t>(), user.native(), _list_status_callback, this, &update_handle);
	if(error) { throw GnError(); }

	if(GNSDK_NULL != update_handle)
	{
		this->AcceptOwnership(update_handle);
		result = true;
	}
	return result;
}

bool GnList::IsUpdateAvailable(const GnUser& user) throw ( GnError )
{
	gnsdk_bool_t  b_new_revision_available = GNSDK_FALSE;
	gnsdk_error_t error;

	error = gnsdk_manager_list_update_check(get<gnsdk_list_handle_t>(), user.native(), _list_status_callback, this, &b_new_revision_available);
	if(error) { throw GnError(); }

	return ( b_new_revision_available ) ? true : false;
}

GnString GnList::Serialize() throw ( GnError )
{
	gnsdk_str_t   sz_value = GNSDK_NULL;
	gnsdk_error_t error;

	error = gnsdk_manager_list_serialize(get<gnsdk_list_handle_t>(), &sz_value);
	if(error) { throw GnError(); }

	GnString retval(sz_value);

	error = gnsdk_manager_string_free(sz_value);
	if(error) { throw GnError(); }

	return retval;
}

GnString GnList::RenderToXml(gnsdk_uint32_t levels, GnListRenderFlags render_flags) throw ( GnError )
{
	gnsdk_uint32_t render_flag_uint32 = GNSDK_LIST_RENDER_XML_MINIMAL;
	gnsdk_str_t    xml                = GNSDK_NULL;
	gnsdk_error_t  error;

	if(kRenderFull == render_flags)
	{
		render_flag_uint32 = GNSDK_LIST_RENDER_XML_SUBMIT;
	}

	error = gnsdk_manager_list_render_to_xml(get<gnsdk_list_handle_t>(), levels, render_flag_uint32, &xml);
	if(error) { throw GnError(); }

	GnString retval(xml);

	error = gnsdk_manager_string_free(xml);
	if(error) { throw GnError(); }

	return retval;
}

gnsdk_cstr_t GnList::Type() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_list_get_type(get<gnsdk_list_handle_t>(), &value);
	if(error) { throw GnError(); }

	return value;
}

gnsdk_cstr_t GnList::Descriptor() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_list_get_descriptor(get<gnsdk_list_handle_t>(), &value);
	if(error) { throw GnError(); }

	return value;
}

gnsdk_cstr_t GnList::Region() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_list_get_region(get<gnsdk_list_handle_t>(), &value);
	if(error) { throw GnError(); }

	return value;
}

gnsdk_cstr_t GnList::Language() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_list_get_language(get<gnsdk_list_handle_t>(), &value);
	if(error) { throw GnError(); }

	return value;
}

gnsdk_cstr_t GnList::Revision() throw ( GnError )
{
	gnsdk_error_t error;
	gnsdk_cstr_t  value = GNSDK_NULL;

	error = gnsdk_manager_list_get_revision(get<gnsdk_list_handle_t>(), &value);
	if(error) { throw GnError(); }

	return value;
}

gnsdk_uint32_t GnList::LevelCount() throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t level_count = 0;

	error = gnsdk_manager_list_get_level_count(get<gnsdk_list_handle_t>(), &level_count);
	if(error) { throw GnError(); }

	return level_count;
}

gn_iterable_container<GnList::list_element_iterator> GnList::ListElements(gnsdk_uint32_t level) throw ( GnError )
{
	gnsdk_error_t  error;
	gnsdk_uint32_t count = 0;

	error = gnsdk_manager_list_get_element_count(get<gnsdk_list_handle_t>(), level, &count);
	if(error) { throw GnError(); }

	list_element_provider provider(get<gnsdk_list_handle_t>(), level);

	return gn_iterable_container<GnList::list_element_iterator>(
								GnList::list_element_iterator(provider, 0, count), 
								GnList::list_element_iterator(provider, count, count) 
								);
}

GnListElement GnList::ElementById(gnsdk_uint32_t itemId) throw ( GnError )
{
	gnsdk_list_element_handle_t element_handle = GNSDK_NULL;

	gnsdk_error_t  error = gnsdk_manager_list_get_element_by_id(get<gnsdk_list_handle_t>(), itemId, &element_handle);
	if(error) { throw GnError(); }

	GnListElement result(element_handle);
	
	error = gnsdk_handle_release(element_handle);
	if(error) { throw GnError(); }
	
	return result;
}

GnListElement GnList::ElementByRange(gnsdk_uint32_t range) throw ( GnError )
{
	gnsdk_list_element_handle_t element_handle = GNSDK_NULL;

	gnsdk_error_t error = gnsdk_manager_list_get_element_by_range(get<gnsdk_list_handle_t>(), range, &element_handle);
	if(error) { throw GnError(); }

	GnListElement result(element_handle);
	
	error = gnsdk_handle_release(element_handle);
	if(error) { throw GnError(); }
	
	return result;
}

GnListElement GnList::ElementByString(gnsdk_cstr_t strEquality) throw ( GnError )
{
	gnsdk_list_element_handle_t element_handle = GNSDK_NULL;

	gnsdk_error_t error = gnsdk_manager_list_get_element_by_string(get<gnsdk_list_handle_t>(), strEquality, &element_handle);
	if(error) { throw GnError(); }
	
	GnListElement result(element_handle);
	
	error = gnsdk_handle_release(element_handle);
	if(error) { throw GnError(); }
	
	return result;
}


GnListElement GnList::ElementByGnDataObject(const GnDataObject& dataObject, gnsdk_uint32_t ordinal, gnsdk_uint32_t level) throw ( GnError )
{
	gnsdk_list_element_handle_t element_handle = GNSDK_NULL;

	gnsdk_error_t  error = gnsdk_manager_list_get_element_by_gdo(get<gnsdk_list_handle_t>(), dataObject.native(), ordinal, level, &element_handle);
	if(error) { throw GnError(); }

	GnListElement result(element_handle);
	
	error = gnsdk_handle_release(element_handle);
	if(error) { throw GnError(); }
	
	return result;
}

void GNSDK_CALLBACK_API GnList::_list_status_callback(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort)
{
	GnList* p_gn_list = (GnList*)callback_data;

	if(p_gn_list->event_handler_)
	{
		p_gn_list->event_handler_->status_event(status, percent_complete, bytes_total_sent, bytes_total_received);
		if(p_gn_list->event_handler_->cancel_check() )
		{
			*p_abort = GNSDK_TRUE;
		}
	}
}
