/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnError {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected GnError(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnError obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnError(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public GnError() {
    this(gnsdk_javaJNI.new_GnError(), true);
  }

  public long errorCode() {
    return gnsdk_javaJNI.GnError_errorCode(swigCPtr, this);
  }

  public String errorDescription() {
    return gnsdk_javaJNI.GnError_errorDescription(swigCPtr, this);
  }

  public String errorAPI() {
    return gnsdk_javaJNI.GnError_errorAPI(swigCPtr, this);
  }

  public String errorModule() {
    return gnsdk_javaJNI.GnError_errorModule(swigCPtr, this);
  }

  public long sourceErrorCode() {
    return gnsdk_javaJNI.GnError_sourceErrorCode(swigCPtr, this);
  }

  public String sourceErrorModule() {
    return gnsdk_javaJNI.GnError_sourceErrorModule(swigCPtr, this);
  }

}
