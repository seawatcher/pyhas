/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnDataMatchEnumerable : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnDataMatchEnumerable(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnDataMatchEnumerable obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnDataMatchEnumerable() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnDataMatchEnumerable(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public GnDataMatchEnumerable(GnDataMatchEnumerator start, GnDataMatchEnumerator end) : this(gnsdk_csharp_marshalPINVOKE.new_GnDataMatchEnumerable(GnDataMatchEnumerator.getCPtr(start), GnDataMatchEnumerator.getCPtr(end)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnDataMatchEnumerator begin() {
    GnDataMatchEnumerator ret = new GnDataMatchEnumerator(gnsdk_csharp_marshalPINVOKE.GnDataMatchEnumerable_begin(swigCPtr), true);
    return ret;
  }

  public GnDataMatchEnumerator end() {
    GnDataMatchEnumerator ret = new GnDataMatchEnumerator(gnsdk_csharp_marshalPINVOKE.GnDataMatchEnumerable_end(swigCPtr), true);
    return ret;
  }

  public uint count() {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnDataMatchEnumerable_count(swigCPtr);
    return ret;
  }

  public GnDataMatchEnumerator at(uint index) {
    GnDataMatchEnumerator ret = new GnDataMatchEnumerator(gnsdk_csharp_marshalPINVOKE.GnDataMatchEnumerable_at(swigCPtr, index), true);
    return ret;
  }

  public GnDataMatchEnumerator getByOrdinal(uint index) {
    GnDataMatchEnumerator ret = new GnDataMatchEnumerator(gnsdk_csharp_marshalPINVOKE.GnDataMatchEnumerable_getByOrdinal(swigCPtr, index), true);
    return ret;
  }

}

}
