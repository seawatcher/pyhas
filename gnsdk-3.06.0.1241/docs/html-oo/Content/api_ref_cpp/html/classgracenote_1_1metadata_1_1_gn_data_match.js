var classgracenote_1_1metadata_1_1_gn_data_match =
[
    [ "GnDataMatch", "classgracenote_1_1metadata_1_1_gn_data_match.html#ac53006998edc808cd750c9742b6b6a00", null ],
    [ "GnDataMatch", "classgracenote_1_1metadata_1_1_gn_data_match.html#a57dd82ec2be15eb676d2d47e57548cff", null ],
    [ "~GnDataMatch", "classgracenote_1_1metadata_1_1_gn_data_match.html#acc5447a54b3836a84dab924c5e410ef5", null ],
    [ "GetAsAlbum", "classgracenote_1_1metadata_1_1_gn_data_match.html#af204034785bb410cb2ff25b89fb44807", null ],
    [ "GetAsContributor", "classgracenote_1_1metadata_1_1_gn_data_match.html#a379e2499e84739a631b3e2230c4b7de7", null ],
    [ "IsAlbum", "classgracenote_1_1metadata_1_1_gn_data_match.html#a169b0285002f21efa1b61f7b7462f189", null ],
    [ "IsContributor", "classgracenote_1_1metadata_1_1_gn_data_match.html#aa2a71880ca5a86c835668eb76bbfd15d", null ]
];