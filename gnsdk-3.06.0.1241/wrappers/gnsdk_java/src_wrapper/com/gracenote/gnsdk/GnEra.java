/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnEra extends GnDataObject {
  private long swigCPtr;

  protected GnEra(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnEra_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnEra obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnEra(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnEra() {
    this(gnsdk_javaJNI.new_GnEra(), true);
  }

  public String level1() {
    return gnsdk_javaJNI.GnEra_level1(swigCPtr, this);
  }

  public String level2() {
    return gnsdk_javaJNI.GnEra_level2(swigCPtr, this);
  }

  public String level3() {
    return gnsdk_javaJNI.GnEra_level3(swigCPtr, this);
  }

}
