﻿/*
 *  Copyright (c) 2000-2013 Gracenote.
 *  
 *  This software may not be used in any way or distributed without
 *  permission. All rights reserved.
 *
 *  Some code herein may be covered by US and international patents.
 */

/* 
 *  Name: Music response data object sample appilcation
 *  Description:
 *  This application uses MusicID to look up Album GDO content,	including Album	artist,	credits, title,	year, and genre.
 *  It demonstrates	how	to navigate	the	album GDO that returns basic track information,	including artist, credits, title, track	number,	and	genre.
 *  Notes:
 *  For	clarity	and	simplicity error handling in not shown here.
 *  Refer "logging"	sample to learn	about GNSDK	error handling.
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license gnsdkLibraryPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;

namespace Sample
{
    public class MusicIDGdoNavigation
    {
        private static string localDB = "../../../sample_db";
        /*  Online vs Local queries
          *	Set to false to have the sample perform online queries.
          *  Set to true to have the sample perform local queries.
          *  For local queries, a Gracenote local database must be present.
          */
        private const bool useLocal = false;
		private static GnUser user = null;

        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";

        #region Initializegnsdk

        /*Callback delegate called when loading locale*/
        public class LookupStatusEvents : GnStatusEventsDelegate
        {
            public static List<string> statusString = new List<string> { "Unknown", "Begin", "Progress", "Complete", "ErrorInfo", "Connecting", "Sending", "Recieving", "Disconnected", "Reading", "Writing" };

            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.WriteLine("\nLocale Load \tstatus :('" + statusString[Convert.ToInt32(status)] + "')" + "\tcomplete: " + percentComplete + "\tsent:" + bytesTotalSent +
                "\treceived:" + bytesTotalReceived);
            }

            public override bool cancel_check()
            {
                return false;
            }
        };

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class MusicIdEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.Write("\nPerforming MusicID Query ...\t");
                Console.Write("status (");
                switch (status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write(" Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write(" Begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write(" Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write(" Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write(" Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write(" Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write(" Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percentComplete + "),\tTotal Bytes Sent (" + bytesTotalSent + "),\tTotal Bytes Received (" + bytesTotalReceived + ")");
            }
        }

        // Display local Gracenote DB information.
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        static void LoadLocale(GnUser user)
        {
            using (LookupStatusEvents localeEvents = new LookupStatusEvents())
            {
                if (useLocal)
                {
                    GnLocale locale = new GnLocale(
                        GnSDK.kLocaleGroupMusic,            /* Locale group */
                        GnSDK.kLanguageEnglish,             /* Languae */
                        GnSDK.kRegionDefault,                /* Region */
                        GnSDK.kDescriptorSimplified,           /* Descriptor */
                        user,                               /* User */
                        null /*localeEvents*/               /* locale Events object */
                        );
                }
                else
                {
                    if (!File.Exists("serialized_locale.txt"))
                    {
                        GnLocale locale = new GnLocale(
                            GnSDK.kLocaleGroupMusic,            /* Locale group */
                            GnSDK.kLanguageEnglish,             /* Languae */
                            GnSDK.kRegionDefault,                /* Region */
                            GnSDK.kDescriptorSimplified,           /* Descriptor */
                            user,                               /* User */
                            null /*localeEvents*/               /* locale Events object */
                            );

                        /*Serialize locale, so we can use reuse it*/
                        if (!File.Exists("serialized_locale.txt"))
                        {
                            String serializedLocaleString = (locale.Serialize().ToString());
                            using (StreamWriter outfile = new StreamWriter("serialized_locale.txt"))
                            {
                                outfile.Write(serializedLocaleString);
                                outfile.Close();
                            }
                        }
                        return;
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader("serialized_locale.txt"))
                        {
                            String serializedLocaleString = sr.ReadToEnd();
                            GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                            return;
                        }

                    }
                }
            }

        }        

        #endregion

        private static void dispalyGnTrack(GnTrack gnTrack)
        {
            Console.WriteLine("\tTrack:");
            Console.WriteLine("\t\tTrack TUI: " + gnTrack.TUI);
            Console.WriteLine("\t\tTrack Number: " + gnTrack.TrackNumber);

            GnArtist artist = gnTrack.Artist;
            GnContributor contributor = artist.Contributor;

            if (contributor != null)
            {
                displayGnContributors(contributor, "\t\t");
            }

            Console.WriteLine("\t\tTitle\tOfficial:");
            Console.WriteLine("\t\t\tDisplay: " + gnTrack.Title.Display);
            Console.WriteLine("\t\t\tSortable: " + gnTrack.Title.Sortable.Display);
            
            if (gnTrack.Year != null && gnTrack.Year != "")
            {
                Console.WriteLine("\t\tYear: " + gnTrack.Year);
            }
            if (gnTrack.Genre.Level1 != null && gnTrack.Genre.Level1 != "")
            {
                Console.WriteLine("\t\tGenre Level 1: " + gnTrack.Genre.Level1);
            }
            if (gnTrack.Genre.Level2 != null && gnTrack.Genre.Level2 != "")
            {
                Console.WriteLine("\t\tGenre Level 2: " + gnTrack.Genre.Level2);
            }
            if (gnTrack.Genre.Level3 != null && gnTrack.Genre.Level3 != "")
            {
                Console.WriteLine("\t\tGenre Level 3: " + gnTrack.Genre.Level3);
            }
        }

        private static void displayGnTitle(GnAlbum gnAlbum)
        {
            GnTitle gnTitle = gnAlbum.Title;
            Console.WriteLine("\tTitle\tOfficial:");
            Console.WriteLine("\t\tDisplay: " + gnTitle.Display);
            Console.WriteLine("\t\tSortable: " + gnTitle.Sortable.Display);
        }

        /*This function demonstrates how to navigate a contributor GDO retrieved from a parent	GDO*/
        private static void displayGnContributors(GnContributor gnContributor, string tab)
        {
            GnNameEnumerable nameEnumerable = gnContributor.NamesOfficial;
            GnNameEnumerator nameEnumerator = nameEnumerable.GetEnumerator();

            if (nameEnumerator.hasNext())
            {
                Console.WriteLine(tab + "Credit:");
                Console.WriteLine(tab + "\tContributor:");
                Console.WriteLine(tab + "\t\tName Official:");

                while (nameEnumerator.hasNext())
                {
                    GnName gnName = nameEnumerator.next();
                    Console.WriteLine(tab + "\t\t\tDisplay: " + gnName.Display);
                }

                if (gnContributor.Origin.Level1 != null && gnContributor.Origin.Level1 != "")
                {
                    Console.WriteLine(tab + "\t\tOrigin Level 1: " + gnContributor.Origin.Level1);
                }
                if (gnContributor.Origin.Level2 != null && gnContributor.Origin.Level2 != "")
                {
                    Console.WriteLine(tab + "\t\tOrigin Level 2: " + gnContributor.Origin.Level2);
                }
                if (gnContributor.Origin.Level3 != null && gnContributor.Origin.Level3 != "")
                {
                    Console.WriteLine(tab + "\t\tOrigin Level 3: " + gnContributor.Origin.Level3);
                }
                if (gnContributor.Origin.Level4 != null && gnContributor.Origin.Level4 != "")
                {
                    Console.WriteLine(tab + "\t\tOrigin Level 4: " + gnContributor.Origin.Level4);
                }
                if (gnContributor.Era.Level1 != null && gnContributor.Era.Level1 != "")
                {
                    Console.WriteLine(tab + "\t\tEra Level 1: " + gnContributor.Era.Level1);
                }
                if (gnContributor.Era.Level2 != null && gnContributor.Era.Level2 != "")
                {
                    Console.WriteLine(tab + "\t\tEra Level 2: " + gnContributor.Era.Level2);
                }
                if (gnContributor.Era.Level3 != null && gnContributor.Era.Level3 != "")
                {
                    Console.WriteLine(tab + "\t\tEra Level 3: " + gnContributor.Era.Level3);
                }
                if (gnContributor.ArtistType.Level1 != null && gnContributor.ArtistType.Level1 != "")
                {
                    Console.WriteLine(tab + "\t\tArtist Type\tLevel 1: " + gnContributor.ArtistType.Level1);
                }
                if (gnContributor.ArtistType.Level2 != null && gnContributor.ArtistType.Level2 != "")
                {
                    Console.WriteLine(tab + "\t\tArtist Type\tLevel 2: " + gnContributor.ArtistType.Level2);
                }
            }
        }

        /* This function demonstrates how to navigate the response GDO from an Album GDO. */
        private static void displayGnAlbum(GnAlbum gnAlbum)
        {
            Console.WriteLine("\n***Navigating	Result GDO***");
            Console.WriteLine("Album:");

            /*Display the package language	for	this album GDO */
            Console.WriteLine("\tPackage Language: " + gnAlbum.Language.Display);

            GnArtist artist = gnAlbum.Artist;
            GnContributor contributor = artist.Contributor;

            if (contributor != null)
            {
                displayGnContributors(contributor, "\t");

                displayGnTitle(gnAlbum);
            }
            if (gnAlbum.Year != null && gnAlbum.Year != "")
            {
                Console.WriteLine("\tYear: " + gnAlbum.Year);
            }
            if (gnAlbum.Genre.Level1 != null && gnAlbum.Genre.Level1 != "")
            {
                Console.WriteLine("\tGenre Level 1: " + gnAlbum.Genre.Level1);
            }
            if (gnAlbum.Genre.Level2 != null && gnAlbum.Genre.Level2 != "")
            {
                Console.WriteLine("\tGenre Level 2: " + gnAlbum.Genre.Level2);
            }
            if (gnAlbum.Genre.Level3 != null && gnAlbum.Genre.Level3 != "")
            {
                Console.WriteLine("\tGenre Level 3: " + gnAlbum.Genre.Level3);
            }
            if (gnAlbum.Label != null && gnAlbum.Label != "")
            {
                Console.WriteLine("\tAlbum Label: " + gnAlbum.Label);
            }
            if (gnAlbum.TotalInSet > 0)
            {
                Console.WriteLine("\tTotal In	Set: " + gnAlbum.TotalInSet);
            }
            if (gnAlbum.DiscInSet > 0)
            {
                Console.WriteLine("\tDisc In Set: " + gnAlbum.DiscInSet);
            }
            /*Navigate	a track	GDO retrieved from an album	GDO */
            Console.WriteLine("\tTrack	Count: " + gnAlbum.TrackCount);
            GnTrackEnumerable gnTrackEnumerable = gnAlbum.Tracks;
            foreach (GnTrack gnTrack in gnTrackEnumerable)
            {
                dispalyGnTrack(gnTrack);               
            }
        }

        /*  Get the album GDO from the match	response */
        private static void displayFindAlbumResults(GnResponseAlbums gnResponse)
        {
            IEnumerable<GnAlbum> albums = gnResponse.Albums;
            foreach (GnAlbum album in albums)
            {
                /* Navigate	the	album GDO  */                
                displayGnAlbum(album);
            }
        }

        /*
        *  This function looks up an album by its TUI. Upon successful single matches, it
        *  will call the displayFindAlbumResults to navigate and display the album GDO.
        */
        private static void musicidLookupAlbum(string inputTuiId, string inputTuiTag, GnUser user)
        {
            Console.WriteLine("\n*****Sample MusicID Query*****");

            GnMusicID musicid = new GnMusicID(user, null);
            GnAlbum gnDataObj = new GnAlbum(inputTuiId, inputTuiTag);
            GnResponseAlbums gnResponse = musicid.FindAlbums(gnDataObj);
            if (gnResponse.Albums.count() == 0)
            {
                /* No Matches */
                Console.WriteLine("No matches.");
            }
            else
            {
                Console.WriteLine("Match.");
                GnAlbum album = gnResponse.Albums.at(0).next();
                /* Is this a partial album? */
                bool fullResult = album.FullResult();
                if (!fullResult)
                {
                    Console.WriteLine("retrieving FULL	RESULT");
                    /* do followup query to get full object. Setting the partial album as the query input. */
                    gnResponse = musicid.FindAlbums(album);
                }
                displayFindAlbumResults(gnResponse);
            }
        }

        /* This function performs TUI lookups for several different albums. */
        private static void doSampleTuiLookups(GnUser user)
        {
            /* Lookup album: Nelly -	Nellyville to demonstrate collaborative artist navigation in track level (track#12)*/
            string inputTuiId = "30716057";
            string inputTuiTag = "BB402408B507485074CC8B3C6D313616";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /* Lookup album: Dido - Life for Rent  */
            inputTuiId = "46508189";
            inputTuiTag = "951407B37F9D8EAE68F74B0B5C5E1224";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);


            /* Lookup album: Jean-Pierre Rampal - Portrait Of Rampal   */
            inputTuiId = "3020551";
            inputTuiTag = "CAA37D27FD12337073B54F8E597A11D3";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /*
             * Lookup album: Various Artists - Grieg: Piano Concerto, Peer Gynth
             * Suites #1	
             */
            inputTuiId = "2971440";
            inputTuiTag = "7F6C280498E077330B1732086C3AAD8F";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /*
             * Lookup album: Stephen Kovacevich - Brahms: Rhapsodies, Waltzes &
             * Piano Pieces
             */
            inputTuiId = "2972852";
            inputTuiTag = "EC246BB5B359D88BEBDC1EF55873311E";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /* Lookup album: Nirvana - Nevermind    */
            inputTuiId = "2897699";
            inputTuiTag = "2FAE8F59CCECBA288810EC27DCD56A0A";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);


            /*  Lookup album: Eminem - Encore  */
            inputTuiId = "68056434";
            inputTuiTag = "C6E3634DF05EF343E3D22CE3A28A901A";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /*
            * Lookup Japanese album: 川澄綾子 - 藍より青し -音絵巻-
            * NOTE: In order to correctly see the Japanese metadata results for
            * this lookup, this program will need to write out to UTF-8 
            */
            inputTuiId = "16391605";
            inputTuiTag = "F272BD764FDEB344A54F53D0756DC3FD";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);

            /*
             * Lookup Chinese album: 蘇芮	- 蘇芮經典
             * NOTE: In order to correctly see the Chinese metadata results for this
             * lookup, this program will need to write out to UTF-8 
             */
            inputTuiId = "3798282";
            inputTuiTag = "6BF6849840A77C987E8D3AF675129F33";
            musicidLookupAlbum(inputTuiId, inputTuiTag, user);
        }

        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }

            /* GNSDK initialization */
            try
            {
                // Initialize SDK
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                // Dispaly SDK version
                Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                // Enable logging
                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */

                /*         
                 *    Load existing user handle, or register new one.                    
                 */
                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

                if (useLocal)
                {
                    GnStorageSqlite gnStorage = new GnStorageSqlite();
                    gnStorage.StorageFolder = localDB;

                    GnLookupLocal gnLookupLocal = new GnLookupLocal();

                    user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                    DisplayEmbeddedDbInfo(gnLookupLocal);
                }
                else
                {
                    user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                }

                /* Set locale with desired Group, Language, Region and Descriptor
                * Set the 'locale' to return locale-specifc results values. This examples loads an English locale.
                */
                LoadLocale(user);

                /* Perform a sample	album lookup from an input GDO */
                doSampleTuiLookups(user);
            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }

    }
}
