/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnException extends java.lang.Exception {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected GnException(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnException obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnException(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

		private long 	errorCode;
		private String 	errorDescription;
		private String 	errorAPI; 
		private String 	errorModule; 
		private long 	sourceErrorCode;
		private String 	sourceErrorModule;
		
		public GnException(long errorCode, String errorDescription, String errorAPI, String errorModule, long sourceErrorCode, String sourceErrorModule){
			super();
			this.errorCode = errorCode;
			this.errorDescription = errorDescription;
			this.errorAPI = errorAPI;
			this.errorModule = errorModule;
			this.sourceErrorCode = sourceErrorCode;
			this.sourceErrorModule = sourceErrorModule;
		}
		
		public long		getErrorCode()			{return errorCode;}
		public String 	getErrorDescription() 	{return errorDescription;}
		public String 	getErrorAPI() 			{return errorAPI;}
		public String 	getErrorModule()		{return errorModule;}
		public long		getSourceErrorCode()	{return sourceErrorCode;}
		public String 	getSourceErrorModule()	{return sourceErrorModule;}
		public String 	getMessage() 			{return errorDescription;}
	
  public GnException() {
    this(gnsdk_javaJNI.new_GnException(), true);
  }

}
