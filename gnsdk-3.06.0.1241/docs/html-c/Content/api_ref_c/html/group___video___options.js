var group___video___options =
[
    [ "GNSDK_VIDEO_OPTION_ENABLE_CONTENT_DATA", "group___video___options.html#ga277b1345c3a3c9fd0a504a8f7e398127", null ],
    [ "GNSDK_VIDEO_OPTION_ENABLE_EXTERNAL_IDS", "group___video___options.html#ga64978d07072cd74bb990aea00987902d", null ],
    [ "GNSDK_VIDEO_OPTION_PREFERRED_LANG", "group___video___options.html#ga1ed0c16dd2aa8f2d364df45bc5fad3f9", null ],
    [ "GNSDK_VIDEO_OPTION_QUERY_ENABLE_COMMERCE_TYPE", "group___video___options.html#gabeccc862f3742176c5a78291ca2a04f4", null ],
    [ "GNSDK_VIDEO_OPTION_QUERY_NOCACHE", "group___video___options.html#ga92a5696d59a89e950a9fe17d9f7ff2e7", null ],
    [ "GNSDK_VIDEO_OPTION_RESULT_RANGE_SIZE", "group___video___options.html#ga3924b95b120dded6980abca454fae553", null ],
    [ "GNSDK_VIDEO_OPTION_RESULT_RANGE_START", "group___video___options.html#ga884e935ae12dea7e7ad5a29daff3836a", null ]
];