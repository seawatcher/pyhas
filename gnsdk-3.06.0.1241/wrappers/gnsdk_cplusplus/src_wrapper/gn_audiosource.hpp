/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

/* GnAudioSource.hpp: Gracenote audio helper class */

#ifndef _GNAUDIOSOURCE_HPP_
#define _GNAUDIOSOURCE_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.h"

namespace gracenote
{
	class GnAudioSource
	{
	protected:
		virtual ~GnAudioSource() {};
		
	public:
		virtual gnsdk_uint32_t SamplesPerSecond() = 0;
		virtual gnsdk_uint32_t SampleSizeInBits() = 0;
		virtual gnsdk_uint32_t NumberOfChannels() = 0;

		virtual gnsdk_uint32_t SourceInit()  = 0; /* return 0 for success, any positive none 0 for failure */
		virtual void           SourceClose() = 0;

		virtual gnsdk_size_t   GetData(gnsdk_byte_t* p_buffer, gnsdk_size_t buffer_size) = 0;
	};

}  // namespace gracenote

#endif // _GNAUDIOSOURCE_HPP_
