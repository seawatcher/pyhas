var classgracenote_1_1metadata_1_1_gn_video_product =
[
    [ "GnVideoProduct", "classgracenote_1_1metadata_1_1_gn_video_product.html#aa996562bfe946c3e3244f456136356f4", null ],
    [ "GnVideoProduct", "classgracenote_1_1metadata_1_1_gn_video_product.html#a757a7722a2b5e88a7ff2fbeee4dc53fe", null ],
    [ "GnVideoProduct", "classgracenote_1_1metadata_1_1_gn_video_product.html#a5f56f424d28d4d70d3e5518698e8df92", null ],
    [ "~GnVideoProduct", "classgracenote_1_1metadata_1_1_gn_video_product.html#a8249ac92fa309178bae49fd494e006ab", null ],
    [ "AspectRatio", "classgracenote_1_1metadata_1_1_gn_video_product.html#a0000cd43a36dcd29ba775779457a1a31", null ],
    [ "AspectRatioType", "classgracenote_1_1metadata_1_1_gn_video_product.html#a4866b99397431497d189fcecdc0f9f90", null ],
    [ "CommerceType", "classgracenote_1_1metadata_1_1_gn_video_product.html#a0809ef83c39fb6568f45f09d73af06f2", null ],
    [ "DateOriginalRelease", "classgracenote_1_1metadata_1_1_gn_video_product.html#a799615018a9cf821d63615d2cd03f470", null ],
    [ "DateRelease", "classgracenote_1_1metadata_1_1_gn_video_product.html#ad6592e1b2d30cd1748af7c0a0ee3ae44", null ],
    [ "Discs", "classgracenote_1_1metadata_1_1_gn_video_product.html#a73f7f04049ee4643a4c145810e3d3870", null ],
    [ "Duration", "classgracenote_1_1metadata_1_1_gn_video_product.html#ac3a7988c384cbc370b06972ad32eb72c", null ],
    [ "DurationUnits", "classgracenote_1_1metadata_1_1_gn_video_product.html#a84207d6228626ba871860a348ab1db96", null ],
    [ "ExternalIDs", "classgracenote_1_1metadata_1_1_gn_video_product.html#a927132648270b3fffb6c10bed647c290", null ],
    [ "FullResult", "classgracenote_1_1metadata_1_1_gn_video_product.html#ac36af2b4c911a4c384b59805fb012b9a", null ],
    [ "Genre", "classgracenote_1_1metadata_1_1_gn_video_product.html#a0e9a302078092d64ec243ae73603d4e9", null ],
    [ "GnID", "classgracenote_1_1metadata_1_1_gn_video_product.html#a3b402c0bc423fa7d30cf80fb7d35d331", null ],
    [ "GnUID", "classgracenote_1_1metadata_1_1_gn_video_product.html#a2cb54f5e844ddd0c0a554f29e50bfdf0", null ],
    [ "Notes", "classgracenote_1_1metadata_1_1_gn_video_product.html#a167ba2b2eab225b1ab25049bb5dede6c", null ],
    [ "OfficialTitle", "classgracenote_1_1metadata_1_1_gn_video_product.html#a89a704bcf04add2f8aa3aca3f7e9cd56", null ],
    [ "PackageLanguage", "classgracenote_1_1metadata_1_1_gn_video_product.html#ab228e200f28d585b673cd2db63632c83", null ],
    [ "PackageLanguageDisplay", "classgracenote_1_1metadata_1_1_gn_video_product.html#a219864343cbd85e7368080072a094cae", null ],
    [ "ProductID", "classgracenote_1_1metadata_1_1_gn_video_product.html#aa76dfe3dfcdca55262f46507e6e6b99c", null ],
    [ "Rating", "classgracenote_1_1metadata_1_1_gn_video_product.html#a52cbc5dc1968f191be0b0bdd059d490d", null ],
    [ "TUI", "classgracenote_1_1metadata_1_1_gn_video_product.html#a9d87be91c970d5a967a72ced7614b585", null ],
    [ "TUITag", "classgracenote_1_1metadata_1_1_gn_video_product.html#ab762c088a2ebb9f5cd64a9d9253b703a", null ],
    [ "VideoProductionType", "classgracenote_1_1metadata_1_1_gn_video_product.html#aae20f21d9955bab24bb09767df8cdabb", null ],
    [ "VideoProductionTypeID", "classgracenote_1_1metadata_1_1_gn_video_product.html#a1a834bcef05a030cd40ce727aa37a023", null ],
    [ "VideoRegion", "classgracenote_1_1metadata_1_1_gn_video_product.html#a1865f537503dd5ac326f685affe77711", null ],
    [ "VideoRegionDesc", "classgracenote_1_1metadata_1_1_gn_video_product.html#a7cf094aeca36c4d941a64db0a4fce6c2", null ]
];