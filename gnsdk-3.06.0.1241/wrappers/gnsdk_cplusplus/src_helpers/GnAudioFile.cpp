/*
 *
 *  GRACENOTE, INC. PROPRIETARY INFORMATION
 *  This software is supplied under the terms of a license agreement or
 *  nondisclosure agreement with Gracenote, Inc. and may not be copied
 *  or disclosed except in accordance with the terms of that agreement.
 *  Copyright(c) 2000-2013. Gracenote, Inc. All Rights Reserved.
 *
 */

/*
 * GnAudioFile.cpp
 */

#include "GnAudioFile.h"
#include "../src_wrapper/gnsdk_iterator_base.hpp"

namespace gracenote
{
	GnAudioFile::GnAudioFile(gnsdk_cstr_t filename)
	{
		gracenote::gnstd::gn_strcpy(m_filename, sizeof(m_filename), filename);
		m_chan     = NULL;
	}


	GnAudioFile::~GnAudioFile(void)
	{
		BASS_Free();
	}


	/*-----------------------------------------------------------------------------
	 *  SourceInit
	 */
	gnsdk_uint32_t
	GnAudioFile::SourceInit()
	{
		if (m_chan == NULL)
		{
			/* not playing anything, so don't need an update thread */
			BASS_SetConfig(BASS_CONFIG_UPDATEPERIOD, 0);

			/* setup output - "no sound" device, 44100hz, stereo, 16 bits */
			if (!BASS_Init(0, 44100, 0, NULL, NULL))
			{
				if (BASS_ErrorGetCode() != BASS_ERROR_ALREADY) 
					return GNSDKERR_InitFailed;
			}

			/* try streaming the file/url */
			if (m_chan = BASS_StreamCreateFile(FALSE, m_filename, 0, 0, BASS_STREAM_DECODE))
			{
				if (BASS_ChannelGetInfo(m_chan, &m_info))
					return GNSDKERR_NoError;
			}

			return GNSDKERR_InitFailed;
		}

		return GNSDKERR_NoError;
	}


	/*-----------------------------------------------------------------------------
	 *  SourceClose
	 */
	gnsdk_void_t
	GnAudioFile::SourceClose()
	{
		BASS_StreamFree(m_chan);
		m_chan = NULL;
	}


	/*-----------------------------------------------------------------------------
	 *  SamplesPerSecond
	 */
	gnsdk_uint32_t
	GnAudioFile::SamplesPerSecond()
	{
		return m_info.freq;
	}


	/*-----------------------------------------------------------------------------
	 *  SampleSizeInBits
	 */
	gnsdk_uint32_t
	GnAudioFile::SampleSizeInBits()
	{
		if (m_info.flags & BASS_SAMPLE_8BITS)
			return 8;
		if (m_info.flags & BASS_SAMPLE_FLOAT)
			return 32;
		return 16;
	}


	/*-----------------------------------------------------------------------------
	 *  NumberOfChannels
	 */
	gnsdk_uint32_t
	GnAudioFile::NumberOfChannels()
	{
		return m_info.chans;
	}


	/*-----------------------------------------------------------------------------
	 *  GetData
	 */
	gnsdk_size_t
	GnAudioFile::GetData(gnsdk_byte_t* audio_buffer, gnsdk_size_t buffer_size)
	{
		gnsdk_size_t bytes_read = 0;

		if (BASS_ChannelIsActive(m_chan))
		{
			bytes_read = BASS_ChannelGetData(m_chan, audio_buffer, buffer_size);
		}

		return bytes_read;
	}

} /* namespace gracenote */
