var group___video___filter_keys =
[
    [ "GNSDK_VIDEO_FILTER_KEY_GENRE_EXCLUDE", "group___video___filter_keys.html#ga67bd4bb7857542ecd46fbecd72588ccc", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_GENRE_INCLUDE", "group___video___filter_keys.html#gaf40c9ff7700bba34ea27b7518020f7dd", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_ORIGIN_EXCLUDE", "group___video___filter_keys.html#gaa11fb0193466628280e226226f6a2cee", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_ORIGIN_INCLUDE", "group___video___filter_keys.html#ga421b22f9cd9883c2a266d206196d02fc", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_PRODUCTION_TYPE_EXCLUDE", "group___video___filter_keys.html#ga656da6030d59b93a0263c9a460b781de", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_PRODUCTION_TYPE_INCLUDE", "group___video___filter_keys.html#ga780e4515d66d544b3032352fe4503c16", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_SEASON_EPISODE_NUM", "group___video___filter_keys.html#ga4d4ca66715558c97128325b69239ef79", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_SEASON_NUM", "group___video___filter_keys.html#ga5152ab6d29e73c20873f95c6c69c65d9", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_SERIAL_TYPE_EXCLUDE", "group___video___filter_keys.html#ga03752d551cc92950ed52b22e7c5f1f94", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_SERIAL_TYPE_INCLUDE", "group___video___filter_keys.html#ga97b38f9e7d6a2030d10245df57f936df", null ],
    [ "GNSDK_VIDEO_FILTER_KEY_SERIES_EPISODE_NUM", "group___video___filter_keys.html#ga092c91e525d813213a4390b130e102a3", null ]
];