/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 *  Name: VideoExplore
 *  Description:
 *  This sample shows basic video explore functionality.
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license_path lib_path 
 */
package video_explore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class VideoExplore {
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupVideo(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}


	/* Find the works done by a contributor */
	private static void findWorkForContributor(GnUser user, GnDataObject gnDataObject)
			throws GnException {
		/*************************************************************************
		 * Explore filmography : What other films did this actor play in ?
		 ************************************************************************/
		GnVideo gnVideo = new GnVideo(user);
		// fetch all work done by contributor
		GnResponseVideoWork gnResponseVideoWork = gnVideo
				.findWorks(gnDataObject);
		System.out.println("\nNumber works: "
				+ gnResponseVideoWork.works().count());
		GnVideoWorkIterable gnVideoWorkIterable = gnResponseVideoWork.works();
		GnVideoWorkIterator gnVideoWorkIterator = gnVideoWorkIterable
				.getIterator();
		int count = 0;
		while (gnVideoWorkIterator.hasNext()) {
			GnVideoWork gnVideoWork = gnVideoWorkIterator.next();
			// Work title
			GnTitle gnTitle = gnVideoWork.officialTitle();
			String title = gnTitle.display();
			count++;
			System.out.println("\t" + count + " : " + title);
		}
	}

	/* Perform a video work lookup */
	private static void videoExplore(GnUser user) throws GnException {
		/*
		 * Typically, the GDO passed in to a Link query will come from the
		 * output of a GNSDK query. For an example of how to perform a query and
		 * get a GDO please refer to the documentation or other sample
		 * applications. The below serialized GDO was an 1-track album result
		 * from another GNSDK query.
		 */
		String serialized_gdo = "WEcxA6R75JwbiGUIxLFZHBr4tv+bxvwlIMr0XK62z68zC+/kDDdELzwiHmBPkmOvbB4rYEY/UOOvFwnk6qHiLdb1iFLtVy44LfXNsTH3uNgYfSymsp9uL+hyHfrzUSwoREk1oX/rN44qn/3NFkEYa2FoB73sRxyRkfdnTGZT7MceHHA/28aWZlr3q48NbtCGWPQmTSrK";
		try {
			GnVideo gnVideo = new GnVideo(user);
			GnDataObject dataObject = new GnDataObject(serialized_gdo);
			GnResponseVideoWork videoWorksResponse = gnVideo
					.findWorks(dataObject);
			/* How many works in the response */
			GnVideoWorkIterable workIterator = videoWorksResponse.works();
			GnVideoWorkIterator gnVideoWorkIterator = workIterator
					.getIterator();
			/* Explore contributors : Who are the cast and crew? */
			System.out
					.println("\nVideo Work - Crouching Tiger, Hidden Dragon: \n\nActor Credits:");
			while (gnVideoWorkIterator.hasNext()) {
				GnVideoWork gnVideoWork = gnVideoWorkIterator.next();
				GnVideoCreditIterable gnVideoCreditIterable = gnVideoWork
						.videoCredits();
				GnVideoCreditIterator gnVideoCreditIterator = gnVideoCreditIterable
						.getIterator();
				/* Iterate all actor credits */
				GnContributor tempContribObj = null;
				int iact = 0;
				while (gnVideoCreditIterator.hasNext()) {
					GnVideoCredit gnVideoCredit = gnVideoCreditIterator.next();
					int rollid = gnVideoCredit.roleID();
					/* compare the Roleid if it is equal to Actor's Role ID */
					if (15942 == rollid) {
						++iact;
						GnContributor contrib = gnVideoCredit.contributor();
						if (1 == iact) {
							tempContribObj = contrib;
						}
						/* Display first Actor Credit */
						GnNameIterable gnNameIterable = contrib.namesOfficial();
						GnNameIterator gnNameIterator = gnNameIterable
								.getIterator();
						while (gnNameIterator.hasNext()) {
							GnName gnName = gnNameIterator.next();
							System.out.println("\t" + iact + " : "
									+ gnName.display());
						}
					}
				} /* end of work loop */
				/* Now find the work done by contributor */
				GnNameIterable gnNameIterable = tempContribObj.namesOfficial();
				GnNameIterator gnNameIterator = gnNameIterable.getIterator();
				while (gnNameIterator.hasNext()) {
					GnName gnName = gnNameIterator.next();
					System.out.println("\nActor Credit: " + gnName.display()
							+ " Filmography");
				}
				GnDataObject gnDataObject = new GnDataObject(tempContribObj.serialize().toString());
				
				findWorkForContributor(user, gnDataObject);
			}
		} catch (GnException gnException) {
			System.out.println("\nError Code: " + gnException.getErrorCode()
					+ "\nError Message: " + gnException.getMessage());
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				loadLocale(gnsdk, user);
				/* Sample Album creation and submission */
				videoExplore(user);
			} catch (GnException gnException) {
				System.out.println("\nError Code: " + gnException.getErrorCode()
						+ "\nError Message: " + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
