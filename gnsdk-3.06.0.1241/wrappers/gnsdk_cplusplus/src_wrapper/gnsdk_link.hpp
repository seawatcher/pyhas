/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _LINK_H_
#define _LINK_H_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "gnsdk_list.hpp"
#include "metadata_music.hpp"

namespace gracenote
{
	namespace link
	{
		class GnLink;
		class GnLinkContent;

		/**
		 * GnImagePreference
		 */
		enum GnImagePreference
		{
			/**
			 * Retrieve exact size as specified by GnImageSize
			 */
			exact = 1,

			/**
			 * Retrieve exact or smaller size as specified by GnImageSize
			 */
			largest,

			/**
			 * Retrieve exact or larger size as specified by GnImageSize
			 */
			smallest
		};

		enum GnImageSize
		{
			/**
			 *  Retrieves a 75 x 75 image
			 */
			size_75 = 1,

			/**
			 *  Retrieves a 110 x 110 image
			 */
			size_110,

			/**
			 *  Retrieves a 170 x 170 image
			 */
			size_170,

			/**
			 *  Retrieves a 220 x 220 image
			 */
			size_220,

			/**
			 *  Retrieves a 300 x 300 image
			 */
			size_300,

			/**
			 *  Retrieves a 450 x 450 image
			 */
			size_450,

			/**
			 *  Retrieves a 720 x 720 image
			 */
			size_720,

			/**
			 *  Retrieves a 1080 x 1080 image
			 */
			size_1080
		};

		/**
		 * GnLink
		 */
		class GnLink : public GnObject
		{
		public:
			GnLink(const GnUser& user, const GnDataObject& gnDataObject, GnStatusEvents* pEventHandler = GNSDK_NULL) throw ( GnError );

			GnLink(const GnUser& user, const GnListElement& listElement, GnStatusEvents* pEventHandler = GNSDK_NULL) throw ( GnError );

			virtual ~GnLink();

			/**
			 * Retrieves the Link library version string.
			 * This API can be called at any time, after getting GnSDK instance successfully. The returned
			 * string is a constant. Do not attempt to modify or delete.
			 *
			 * Example: <code>1.2.3.123</code> (Major.Minor.Improvement.Build)<br>
			 * Major: New functionality<br>
			 * Minor: New or changed features<br>
			 * Improvement: Improvements and fixes<br>
			 * Build: Internal build number<br>
			 */
			static gnsdk_cstr_t Version();

			/**
			 *  Retrieves Link library build date string.
			 *  @return Note Build date string of the format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting GnSDK instance successfully. The returned
			 *  string is a constant. Do not attempt to modify or delete.
			 *  Example build date string: 2008-02-12 00:41 UTC
			 */
			static gnsdk_cstr_t BuildDate();

			/**
			 * Retrieves count for the specified content
			 * @param contentType Type of content to count
			 * @return Count
			 * <p><b>Remarks:</b></p>
			 * <code>Count()</code> can be called repeatedly on the same GnLink object with
			 * different content type requests to
			 * retrieve the count for differing values of content type.
			 */
			gnsdk_uint32_t Count(gnsdk_link_content_type_t contentType) throw ( GnError );

			/**
			 * Set this link query lookup mode.
			 * @param lookup_mode		Lookup mode
			 */
			void OptionLookupMode(GnLookupMode lookupMode) throw ( GnError );

			/**
			 *  Explicitly identifies the track of interest by its ordinal number. This option takes precedence
			 *   over any provided by track indicator.
			 * @ingroup Link_OptionKeys
			 */
			void OptionTrackOrd(gnsdk_cstr_t option_key_track_ord_value) throw ( GnError );

			/**
			 *  This option sets the source provider of the content (for example, "Acme").
			 * @ingroup Link_OptionKeys
			 */
			void OptionDataSource(gnsdk_cstr_t option_key_data_source_value) throw ( GnError );

			/**
			 *  This option sets the type of the provider content (for example, "cover").
			 * @ingroup Link_OptionKeys
			 */
			void OptionDataType(gnsdk_cstr_t option_key_data_type_value) throw ( GnError );

			/**
			 *  Clears all options currently set for a given Link query.
			 *  <p><b>Remarks:</b></p>
			 *  As Link query handles can be used to retrieve multiple enhanced data items, it may be appropriate
			 *   to specify different options between data retrievals. You can use this function to clear all options
			 *   before setting new ones.
			 * @ingroup Link_QueryFunctions
			 */
			void ClearOptions() throw ( GnError );

			// image contents

			/**
			 * Retrieves CoverArt data.
			 * @param imageSize size of the image to retrieve
			 * @param imagePreference image retrieval preference
			 * @param item_ord Nth CoverArt
			 * @return GnLinkContent
			 *  <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different size and ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* CoverArt        (GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves GenreArt data.
			 * @param imageSize size of the image to retrieve
			 * @param imagePreference image retrieval preference
			 * @param item_ord Nth GenreArt
			 * @return GnLinkContent
			 *  <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different size and ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* GenreArt        (GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves Image data.
			 * @param imageSize size of the image to retrieve
			 * @param imagePreference image retrieval preference
			 * @param item_ord  Nth Image
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different size and ordinal parameters
			 * @ingroup Link_QueryFunctions
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* Image           (GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves ArtistImage data.
			 * @param  imageSize size of the image to retrieve
			 * @param imagePreference image retrieval preference
			 * @param item_ord Nth ArtistImage
			 * @return  GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different size and ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* ArtistImage     (GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_uint32_t item_ord = 1) throw ( GnError );

			// text contents

			/**
			 * Retrieves Review data.
			 * @param item_ord Nth Review
			 * @return  GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* Review          (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves Biography data.
			 * @param item_ord [in] Nth Biography
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* Biography       (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves AristNews data.
			 * @param item_ord Nth AristNews
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* ArtistNews       (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves LyricXML data.
			 * @param item_ord Nth LyricXML
			 * @return  GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* LyricXML        (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves LyricText data.
			 * @param item_ord Nth LyricText
			 * @return  GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* LyricText       (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves CommentsListener data.
			 * @param item_ord [in] Nth CommentsListener
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* CommentsListener(gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves CommentsRelease data.
			 * @param item_ord [in] Nth CommentsRelease
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* CommentsRelease (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			/**
			 * Retrieves News data.
			 * @param item_ord Nth News
			 * @return GnLinkContent
			 * <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent* News            (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			// binary contents

			/**
			 * Retrieves DspData data.
			 * @param item_ord Nth DspData
			 * @return GnLinkContent
			 *  <p><b>Remarks:</b></p>
			 * This API can be called repeatedly on the same link query handle with
			 * different ordinal parameters
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnLinkContent*            DspData         (gnsdk_uint32_t item_ord = 1) throw ( GnError );

			gnsdk_link_query_handle_t native() const { return mQueryHandle; }

		private:
			gnsdk_link_query_handle_t mQueryHandle;
			GnStatusEvents*           mEventHandler;

			/* disallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnLink);

			static void GNSDK_CALLBACK_API _CallbackStatus(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort);

			gnsdk_cstr_t                   _MapImgSizeToCstr(GnImageSize imgSize);

			GnLinkContent*                 _GnImageArt(GnImageSize imageSize, GnImagePreference imagePreference, gnsdk_link_content_type_t linkContentType, gnsdk_uint32_t item_ord) throw ( GnError );
		};

		/**
		 * GnLinkContent
		 */
		class GnLinkContent : public GnObject
		{
		public:
			virtual ~GnLinkContent();

			/**
			 * Retrieves content data buffer
			 * @param pre_allocaled_byte_buffer
			 */
			void DataBuffer(gnsdk_byte_t* pre_allocaled_byte_buffer);

			/**
			 * Retrieves content data buffer size
			 */
			gnsdk_size_t DataSize();

			/**
			 * Retrieves content data type
			 */
			gnsdk_link_data_type_t    DataType();

		private:
			gnsdk_link_query_handle_t mQueryHandle;
			gnsdk_link_content_type_t mContentType;
			gnsdk_uint32_t            mItemOrdinal;

			gnsdk_byte_t*             mData;
			gnsdk_link_data_type_t    mDataType;
			gnsdk_size_t              mDataSize;

			GnLinkContent(const gnsdk_link_query_handle_t query_handle, gnsdk_link_content_type_t contentType, gnsdk_uint32_t item_ord = 0) throw ( GnError );

			friend class GnLink;

			/* disallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnLinkContent);
		};
	}
}
#endif /* _LINK_H_ */
