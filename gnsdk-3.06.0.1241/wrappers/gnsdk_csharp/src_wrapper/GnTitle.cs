/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnTitle : GnDataObject {
  private HandleRef swigCPtr;

  internal GnTitle(IntPtr cPtr, bool cMemoryOwn) : base(gnsdk_csharp_marshalPINVOKE.GnTitle_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnTitle obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnTitle() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnTitle(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GnTitle() : this(gnsdk_csharp_marshalPINVOKE.new_GnTitle(), true) {
  }

  public string SortableScheme {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTitle_SortableScheme_get(swigCPtr) );
	} 

  }

  public GnLanguage Language {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnTitle_Language_get(swigCPtr);
      GnLanguage ret = (cPtr == IntPtr.Zero) ? null : new GnLanguage(cPtr, true);
      return ret;
    } 
  }

  public GnSortable Sortable {
    get {
      IntPtr cPtr = gnsdk_csharp_marshalPINVOKE.GnTitle_Sortable_get(swigCPtr);
      GnSortable ret = (cPtr == IntPtr.Zero) ? null : new GnSortable(cPtr, true);
      return ret;
    } 
  }

  public string Display {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTitle_Display_get(swigCPtr) );
	} 

  }

  public string Edition {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTitle_Edition_get(swigCPtr) );
	} 

  }

  public string MainTitle {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTitle_MainTitle_get(swigCPtr) );
	} 

  }

  public string Prefix {
	get
	{   
		/* csvarout typemap code */
		return GnMarshalUTF8.StringFromNativeUtf8(gnsdk_csharp_marshalPINVOKE.GnTitle_Prefix_get(swigCPtr) );
	} 

  }

}

}
