var searchData=
[
  ['swigtype_5fp_5ff_5fp_5fvoid_5fenum_5fgnsdk_5fmusicidstream_5fstatus_5ft_5f_5fvoid',['SWIGTYPE_p_f_p_void_enum_gnsdk_musicidstream_status_t__void',['../a00208.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5ff_5fp_5fvoid_5fenum_5fgnsdk_5fstatus_5ft_5funsigned_5fint_5fsize_5ft_5fsize_5ft_5fp_5fchar_5f_5fvoid',['SWIGTYPE_p_f_p_void_enum_gnsdk_status_t_unsigned_int_size_t_size_t_p_char__void',['../a00209.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5ff_5fp_5fvoid_5fp_5fvoid_5fgnsdk_5fgdo_5fhandle_5ft_5f_5fvoid',['SWIGTYPE_p_f_p_void_p_void_gnsdk_gdo_handle_t__void',['../a00210.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5fgdo_5fhandle_5ft',['SWIGTYPE_p_gnsdk_gdo_handle_t',['../a00211.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5flist_5felement_5fhandle_5ft',['SWIGTYPE_p_gnsdk_list_element_handle_t',['../a00212.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5flist_5fhandle_5ft',['SWIGTYPE_p_gnsdk_list_handle_t',['../a00213.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5fmanager_5fhandle_5ft',['SWIGTYPE_p_gnsdk_manager_handle_t',['../a00214.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5fplaylist_5fresults_5fhandle_5ft',['SWIGTYPE_p_gnsdk_playlist_results_handle_t',['../a00215.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgnsdk_5fuser_5fhandle_5ft',['SWIGTYPE_p_gnsdk_user_handle_t',['../a00216.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgracenote_5f_5fgn_5ffacade_5frange_5fiteratort_5fchar_5fconst_5fp_5fgracenote_5f_5fplaylist_5f_5fcollection_5fstorage_5fprovider_5ft',['SWIGTYPE_p_gracenote__gn_facade_range_iteratorT_char_const_p_gracenote__playlist__collection_storage_provider_t',['../a00217.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fgracenote_5f_5fmetadata_5f_5fgn_5fgdo_5fprovidert_5fgracenote_5f_5fmetadata_5f_5fgncontributor_5ft',['SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t',['../a00218.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fp_5fchar',['SWIGTYPE_p_p_char',['../a00219.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fp_5fvoid',['SWIGTYPE_p_p_void',['../a00220.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5funsigned_5fchar',['SWIGTYPE_p_unsigned_char',['../a00221.html',1,'com::gracenote::gnsdk']]],
  ['swigtype_5fp_5fvoid',['SWIGTYPE_p_void',['../a00222.html',1,'com::gracenote::gnsdk']]]
];
