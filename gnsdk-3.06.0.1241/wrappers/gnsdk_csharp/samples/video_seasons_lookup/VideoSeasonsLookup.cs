/*
 * Copyright (c) 2012 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

 /*
  *  Name: VideoSeasonsLookup.cs
  *  Description:
  *  This sample shows basic use of FindSeasons() call
  *
  *  Command-line Syntax:
  *  sample clientId clientIdTag license libPath
  */  


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoSeasonsLookup
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoSeasonsLookupEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Find Product Query ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }
        
        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoSeasonsLookupEvents localeEvents = new VideoSeasonsLookupEvents())
            {

                if (!File.Exists("serialized_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DisplayEpisodes(GnVideoSeason season)
        {
            int count = 0;
            Console.WriteLine("\nNumber of Episodes: " + season.Works.count());

            GnVideoWorkEnumerable gnVideoWorkEnumerable = season.Works;
            foreach (GnVideoWork gnVideoWork in gnVideoWorkEnumerable)
            {
                /* Episode title */
                GnTitle episodeTitle = gnVideoWork.OfficialTitle;

                Console.WriteLine("\t" + ++count + " : " + episodeTitle.Display);
            }
        }

        private static void DoSeasonsLookup(GnUser user)
        {
            /* Get Simpson's First Season GDO */

            Console.WriteLine("\n*****Sample Video Seasons Search: 'Simpsons First Season'*****");

            using (GnStatusEventsDelegate videoEvents = new VideoSeasonsLookupEvents())
            {
                GnVideo video = new GnVideo(user, null/*videoEvents*/);

                /* Create GDO from TUI and TUI Tag */
                GnVideoSeason gnDataObject = new GnVideoSeason("238047322", "5C46058E04036C88490972A653C64A1D");

                Console.WriteLine("\nPerform the search...");

                GnResponseVideoSeasons gnResponseVideoSeasons = video.FindSeasons(gnDataObject);

                if (gnResponseVideoSeasons.Seasons.count() > 0)
                {
                    /****************************************************************
					*  Needs decision (match resolution) check
					*  We have at least one match - see if resolution is necessary.
					****************************************************************/
                    if (gnResponseVideoSeasons.NeedsDecision)
                    {
                        /**********************************************
						* Resolve match here
						**********************************************/
                    }

                    GnVideoSeasonEnumerable gnVideoSeasonIterable = gnResponseVideoSeasons.Seasons;

                    foreach (GnVideoSeason season in gnVideoSeasonIterable)
                    {
                        GnVideoSeason videoSeason = season;
                        if (!season.FullResult)
                        {
                            GnResponseVideoSeasons gnResponseSeasons = video.FindSeasons(season);
                            videoSeason = gnResponseSeasons.Seasons.at(0).next();
                        }
                        GnTitle seasonTitle = videoSeason.OfficialTitle;
                        Console.WriteLine("\nTitle: " + seasonTitle.Display);

                        DisplayEpisodes(videoSeason);
                    }
                }
            }
        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Lookup Seasons and display */
                        DoSeasonsLookup(user);
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }

    }
}
