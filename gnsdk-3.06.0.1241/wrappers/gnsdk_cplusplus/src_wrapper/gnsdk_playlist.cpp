#include "gnsdk_playlist.hpp"
#include "gnsdk_moodgrid.hpp"

#if GNSDK_PLAYLIST

using namespace gracenote;
using namespace gracenote::playlist;

// *****************************************************************************************************************************
// GnPlaylistResult result_provider
// *****************************************************************************************************************************

GnPlaylistIdentifier result_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_cstr_t  media_ident = GNSDK_NULL;
	gnsdk_cstr_t  coll_name   = GNSDK_NULL;
	gnsdk_error_t error       = PLERR_NoError;

	error = gnsdk_playlist_results_enum(results_handle_, pos, &media_ident, &coll_name);
	if(error) { throw GnError(); }
	return GnPlaylistIdentifier(media_ident, coll_name);
}

// optional
gnsdk_uint32_t result_provider::count()
{
	gnsdk_uint32_t count = 0;
	gnsdk_error_t  error = PLERR_NoError;

	error = gnsdk_playlist_results_count(results_handle_, &count);
	if(error) { throw GnError(); }
	return count;
}

// *****************************************************************************************************************************
// GnPlaylistCollection  collection_ident_provider
// *****************************************************************************************************************************

GnPlaylistIdentifier collection_ident_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_cstr_t  media_ident = GNSDK_NULL;
	gnsdk_cstr_t  coll_name   = GNSDK_NULL;
	gnsdk_error_t error       = PLERR_NoError;

	error = gnsdk_playlist_collection_ident_enum(coll_handle_, pos, &media_ident, &coll_name);
	if(error) { throw GnError(); }
	return GnPlaylistIdentifier(media_ident, coll_name);
}

// *****************************************************************************************************************************
// GnPlaylistCollection  collection_join_provider
// *****************************************************************************************************************************

GnPlaylistCollection collection_join_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_playlist_collection_handle_t coll  = GNSDK_NULL;
	gnsdk_error_t                      error = PLERR_NoError;

	error = gnsdk_playlist_collection_join_enum(handle_, pos, &coll);
	if(error) { throw GnError(); }
	return GnPlaylistCollection(coll);
}

// *****************************************************************************************************************************
// GnPlaylistCollection
// *****************************************************************************************************************************

gnsdk_cstr_t GnPlaylistCollection::Name() const throw ( GnError )
{
	gnsdk_cstr_t  name  = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_playlist_collection_get_name(this->get<gnsdk_playlist_collection_handle_t>(), &name);

	if(error) { throw GnError(); }
	return name;
}

// Add a GnAlbum to the collection.
void GnPlaylistCollection::Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnAlbum& album) throw ( GnError )
{
	_add(mediaIdentifier, album.native() );
}

// Add a GnTrack to the collection.
void GnPlaylistCollection::Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnTrack& track) throw ( GnError )
{
	_add(mediaIdentifier, track.native() );
}

// Add a GnContributor to the Collection.
void GnPlaylistCollection::Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnContributor& contributor) throw ( GnError )
{
	_add(mediaIdentifier, contributor.native() );
}

// Add a GnPlaylistMetadata to the Collection.
void GnPlaylistCollection::Add(gnsdk_cstr_t mediaIdentifier, const GnPlaylistMetadata& playlistMetadata) throw ( GnError )
{
	_add(mediaIdentifier, playlistMetadata.native() );
}

// Remove and media identifier from the collection.
void GnPlaylistCollection::Remove(gnsdk_cstr_t mediaIdentifier) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;

	error = gnsdk_playlist_collection_ident_remove(get<gnsdk_playlist_collection_handle_t>(), mediaIdentifier);
	if(error)
	{
		throw GnError();
	}
}

GnPlaylistCollection::ident_iterator GnPlaylistCollection::Find(gnsdk_cstr_t mediaIdentifier, gnsdk_uint32_t start)  throw ( GnError )
{
	gnsdk_error_t  error     = PLERR_NoError;
	gnsdk_cstr_t   coll_name = GNSDK_NULL;
	gnsdk_uint32_t found     = 0;
	gnsdk_uint32_t count     = 0;

	error = gnsdk_playlist_collection_ident_find(get<gnsdk_playlist_collection_handle_t>(), mediaIdentifier, start, &found, &coll_name);

	if(error && ( error != PLERR_NotFound ) )
	{
		throw GnError();
	}
	if(PLERR_NotFound == error)
	{
		gnsdk_playlist_collection_ident_count(get<gnsdk_playlist_collection_handle_t>(), &count);
		found = count;
	}

	collection_ident_provider provider(get<gnsdk_playlist_collection_handle_t>() );
	return ident_iterator(provider, found, count);
}

GnPlaylistMetadata GnPlaylistCollection::Metadata(GnUser& user, const GnPlaylistIdentifier&  mediaIdentifier) throw ( GnError )
{
	gnsdk_error_t                      error  = PLERR_NoError;
	gnsdk_playlist_collection_handle_t h_coll = GNSDK_NULL;
	gnsdk_gdo_handle_t                 h_gdo  = GNSDK_NULL;

	error = gnsdk_playlist_collection_join_get(get<gnsdk_playlist_collection_handle_t>(), mediaIdentifier.CollectionName(), &h_coll);
	if(!error)
	{
		error = gnsdk_playlist_collection_get_gdo(h_coll, user.native(), mediaIdentifier.MediaIdentifier(), &h_gdo);
		gnsdk_playlist_collection_release(h_coll);
	}
	else
	{
		throw GnError();
	}
	GnPlaylistMetadata tmp(h_gdo);
	gnsdk_manager_gdo_release(h_gdo);
	return tmp;
}

GnPlaylistMetadata GnPlaylistCollection::Metadata(GnUser& user, gnsdk_cstr_t mediaIdentifier, gnsdk_cstr_t collectionName) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t h_coll = GNSDK_NULL;
	gnsdk_gdo_handle_t                 h_gdo  = GNSDK_NULL;

	gnsdk_error_t                      error = gnsdk_playlist_collection_join_get(get<gnsdk_playlist_collection_handle_t>(), collectionName, &h_coll);

	if(!error)
	{
		error = gnsdk_playlist_collection_get_gdo(h_coll, user.native(), mediaIdentifier, &h_gdo);
		gnsdk_playlist_collection_release(h_coll);
	}
	else
	{
		throw GnError();
	}
	GnPlaylistMetadata tmp(h_gdo);
	gnsdk_manager_gdo_release(h_gdo);
	return tmp;
}

// Generate Playlist.
bool GnPlaylistCollection::TryValidateStatement(gnsdk_cstr_t pdlStatement) throw ( )
{
	gnsdk_bool_t  b_seed    = GNSDK_FALSE;
	bool          ret_value = false;

	gnsdk_error_t error = gnsdk_playlist_statement_validate(pdlStatement, get<gnsdk_playlist_collection_handle_t>(), &b_seed);

	if(!error)
	{
		ret_value = true;
	}
	return ret_value;
}

bool GnPlaylistCollection::IsSeedRequiredInStatement(gnsdk_cstr_t pdlStatement) throw ( GnError )
{
	gnsdk_bool_t  b_seed = GNSDK_FALSE;

	gnsdk_error_t error = gnsdk_playlist_statement_validate(pdlStatement, get<gnsdk_playlist_collection_handle_t>(), &b_seed);

	if(error) { throw GnError(); }
	return GNSDK_TRUE == b_seed;
}

GnPlaylistResult GnPlaylistCollection::GeneratePlaylist(GnUser& user, gnsdk_cstr_t pdlStatement, const metadata::GnDataObject& musicDataObj) throw ( GnError )
{
	gnsdk_playlist_results_handle_t h_results = GNSDK_NULL;

	gnsdk_error_t                   error = gnsdk_playlist_generate_playlist(user.native(), pdlStatement, get<gnsdk_playlist_collection_handle_t>(), musicDataObj.native(), &h_results);

	if(error) { throw GnError(); }

	GnPlaylistResult                retVal(h_results);

	error = gnsdk_playlist_results_release(h_results);

	if(error) { throw GnError(); }

	return retVal;
}

GnPlaylistResult GnPlaylistCollection::GeneratePlaylist(GnUser& user, gnsdk_cstr_t pdlStatement) throw ( GnError )
{
	gnsdk_playlist_results_handle_t h_results = GNSDK_NULL;

	gnsdk_error_t                   error = gnsdk_playlist_generate_playlist(user.native(), pdlStatement, get<gnsdk_playlist_collection_handle_t>(), GNSDK_NULL, &h_results);

	if(error) { throw GnError(); }

	GnPlaylistResult                retVal(h_results);

	error = gnsdk_playlist_results_release(h_results);

	if(error) { throw GnError(); }

	return retVal;
}

GnPlaylistResult GnPlaylistCollection::GenerateMoreLikeThis(GnUser& user, const metadata::GnDataObject& musicDataObj) throw ( GnError )
{
	gnsdk_playlist_results_handle_t h_results = GNSDK_NULL;

	gnsdk_error_t                   error = gnsdk_playlist_generate_morelikethis(user.native(), get<gnsdk_playlist_collection_handle_t>(), musicDataObj.native(), &h_results);

	if(error) { throw GnError(); }

	GnPlaylistResult                retVal(h_results);

	error = gnsdk_playlist_results_release(h_results);

	if(error) { throw GnError(); }

	return retVal;
}

gn_iterable_container<GnPlaylistCollection::ident_iterator>
GnPlaylistCollection::MediaIdentifiers() throw ( GnError )
{
	gnsdk_uint32_t            count = 0;

	gnsdk_error_t             error = gnsdk_playlist_collection_ident_count(get<gnsdk_playlist_collection_handle_t>(), &count);

	if(error) { throw ( GnError() ); }

	collection_ident_provider provider(get<gnsdk_playlist_collection_handle_t>() );

	return gn_iterable_container<ident_iterator>(ident_iterator(provider, 0, count), ident_iterator(provider, count, count) );
}

gn_iterable_container<GnPlaylistCollection::join_iterator>
GnPlaylistCollection::Joins() throw ( GnError )
{
	gnsdk_uint32_t           count = 0;

	gnsdk_error_t            error = gnsdk_playlist_collection_join_count(get<gnsdk_playlist_collection_handle_t>(), &count);

	if(error) { throw GnError(); }

	collection_join_provider provider(get<gnsdk_playlist_collection_handle_t>() );

	return gn_iterable_container<join_iterator>(join_iterator(provider, 0, count), join_iterator(provider, count, count) );
}

GnPlaylistCollection GnPlaylistCollection::JoinSearchByName(gnsdk_cstr_t collectionName) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t coll = GNSDK_NULL;

	gnsdk_error_t                      error = gnsdk_playlist_collection_join_get_by_name(get<gnsdk_playlist_collection_handle_t>(), collectionName, &coll);

	if(error) { throw GnError(); }

	GnPlaylistCollection               retvalue;
	retvalue.AcceptOwnership(coll);

	return retvalue;
}

void GnPlaylistCollection::Join(GnPlaylistCollection& toJoin) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;

	error = gnsdk_playlist_collection_join(get<gnsdk_playlist_collection_handle_t>(), toJoin.get<gnsdk_playlist_collection_handle_t>() );
	if(error) { throw GnError(); }
}

void GnPlaylistCollection::JoinRemove(GnPlaylistCollection& toRemove) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_collection_join_remove(get<gnsdk_playlist_collection_handle_t>(), toRemove.Name() );

	if(error) { throw GnError(); }
}

void GnPlaylistCollection::MoreLikeThisOptionSet(GenerateMoreLikeThisOption moreLikeThisOption, gnsdk_uint32_t optionValue) throw ( GnError )
{
	gnsdk_error_t error      = PLERR_NoError;
	char          buffer[32] = { 0 };
	gracenote::gnstd::gn_itoa(buffer, 32, optionValue);

	switch(moreLikeThisOption)
	{
	case kMoreLikeThisMaxTracks:
		error = gnsdk_playlist_morelikethis_option_set(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_TRACKS, buffer);
		break;

	case kMoreLikeThisMaxPerArtist:
		error = gnsdk_playlist_morelikethis_option_set(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ARTIST, buffer);
		break;

	case kMoreLikeThisMaxPerAlbum:
		error = gnsdk_playlist_morelikethis_option_set(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ALBUM, buffer);
		break;

	case kMoreLikeThisRandom:
		error = gnsdk_playlist_morelikethis_option_set(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_RANDOM, buffer);
		break;

	default:
		return;
	}

	if(error) { throw GnError(); }
}

gnsdk_uint32_t GnPlaylistCollection::MoreLikeThisOptionGet(GenerateMoreLikeThisOption moreLikeThisOption) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;
	gnsdk_cstr_t  value = GNSDK_NULL;

	switch(moreLikeThisOption)
	{
	case kMoreLikeThisMaxTracks:             error = gnsdk_playlist_morelikethis_option_get(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_TRACKS, &value);
		break;

	case kMoreLikeThisMaxPerArtist:          error = gnsdk_playlist_morelikethis_option_get(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ARTIST, &value);
		break;

	case kMoreLikeThisMaxPerAlbum:           error = gnsdk_playlist_morelikethis_option_get(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_MAX_PER_ALBUM, &value);
		break;

	case kMoreLikeThisRandom:                error = gnsdk_playlist_morelikethis_option_get(get<gnsdk_playlist_collection_handle_t>(), GNSDK_PLAYLIST_MORELIKETHIS_OPTION_RANDOM, &value);
		break;
	}

	if(error) { throw GnError(); }

	return gnstd::gn_atoi(value);
}

void GnPlaylistCollection::PeformSyncProcess(GnPlaylistCollectionSyncEvents& syncEvents) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_collection_sync_process(this->get<gnsdk_playlist_collection_handle_t>(), GnPlaylistCollectionSyncEvents::_collection_sync, &syncEvents);

	if(error) { throw GnError(); }
}

void GnPlaylistCollection::AddForSyncProcess(gnsdk_cstr_t mediaIdentifier) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_collection_sync_ident_add(this->get<gnsdk_playlist_collection_handle_t>(), mediaIdentifier);

	if(error) { throw GnError(); }
}

void GnPlaylistCollection::_add(gnsdk_cstr_t mediaIdentifier, gnsdk_gdo_handle_t gdo) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;

	error = gnsdk_playlist_collection_add_gdo(get<gnsdk_playlist_collection_handle_t>(), mediaIdentifier, gdo);
	if(error)
	{
		throw GnError();
	}
}

// *****************************************************************************************************************************
// GnPlaylist Collection Storage Provider
// *****************************************************************************************************************************

collection_storage_provider::collection_storage_provider() : buffer_(GNSDK_NULL)
{
}

collection_storage_provider::~collection_storage_provider()
{
	delete buffer_;
}

gnsdk_cstr_t collection_storage_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_error_t error = PLERR_NoError;

	if(!buffer_)
	{
		buffer_ = new char[256];
	}
	error = gnsdk_playlist_storage_enum_collections(pos, buffer_, 256);
	if(error)
	{
		throw GnError();
	}
	return buffer_;
}

// *****************************************************************************************************************************
// GnPlaylistCollectionSyncEvents
// *****************************************************************************************************************************

void GnPlaylistCollectionSyncEvents::_collection_sync(gnsdk_void_t* callback_data,
	gnsdk_playlist_collection_handle_t                              handle,
	gnsdk_cstr_t                                                    ident,
	gnsdk_playlist_status_t                                         status,
	gnsdk_bool_t*                                                   p_cancel)
{
	GnPlaylistCollectionSyncEvents* events = reinterpret_cast<GnPlaylistCollectionSyncEvents*>( callback_data );
	GnPlaylistCollection            coll(handle);

	events->OnUpdate(coll, GnPlaylistIdentifier(ident, coll.Name() ), (EventsIdentiferStatus)status);
	*p_cancel = events->OnCancelCheck();
}

// *****************************************************************************************************************************
// GnPlaylist
// *****************************************************************************************************************************

GnPlaylist::GnPlaylist()
{
	GnSDK::init_gnsdk_module(GNSDK_COMPONENT_PLAYLIST);
}

GnPlaylist::~GnPlaylist()
{
}

gnsdk_cstr_t GnPlaylist::Version() const
{
	return gnsdk_playlist_get_version();
}

gnsdk_cstr_t GnPlaylist::BuildDate() const
{
	return gnsdk_playlist_get_build_date();
}

// Creation of collection.
GnPlaylistCollection GnPlaylist::CreateCollection(gnsdk_cstr_t collectionName) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t h_collection;

	gnsdk_error_t                      error = gnsdk_playlist_collection_create(collectionName, &h_collection);

	if(error) { throw GnError(); }

	GnPlaylistCollection               retVal;

	retVal.AcceptOwnership(h_collection);
	return retVal;
}

GnPlaylistCollection GnPlaylist::DeserializeCollection(gnsdk_byte_t* buffer, gnsdk_size_t buffer_sz) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t h_collection;

	gnsdk_error_t                      error = gnsdk_playlist_collection_deserialize(buffer, buffer_sz, &h_collection);

	if(error) { throw GnError(); }

	GnPlaylistCollection               retVal;

	retVal.AcceptOwnership(h_collection);

	return retVal;
}

gnsdk_size_t GnPlaylist::SerializeCollection(GnPlaylistCollection& collection, gnsdk_byte_t* buffer, gnsdk_size_t buffer_sz) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;
	gnsdk_size_t  sz    = buffer_sz;

	error = gnsdk_playlist_collection_serialize(collection.get<gnsdk_playlist_collection_handle_t>(), buffer, &sz);
	if(error) { throw GnError(); }
	return sz;
}

gnsdk_size_t GnPlaylist::SerializeBufferSize(GnPlaylistCollection& collection) throw ( GnError )
{
	gnsdk_error_t error = PLERR_NoError;
	gnsdk_size_t  sz    = 0;

	error = gnsdk_playlist_collection_serialize_size(collection.get<gnsdk_playlist_collection_handle_t>(), &sz);
	if(error)
	{
		throw GnError();
	}
	return sz;
}

void GnPlaylist::StoreCollection(GnPlaylistCollection& collection) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_storage_store_collection(collection.get<gnsdk_playlist_collection_handle_t>() );

	if(error) { throw GnError(); }
}

void GnPlaylist::RemoveFromStorage(GnPlaylistCollection& collection) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_storage_remove_collection(collection.Name() );

	if(error) { throw GnError(); }
}

void GnPlaylist::RemoveFromStorage(gnsdk_cstr_t collectionName) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_storage_remove_collection(collectionName);

	if(error) { throw GnError(); }
}

void GnPlaylist::SetStorageLocation(gnsdk_cstr_t location) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_storage_location_set(location);

	if(error) { throw GnError(); }
}

void GnPlaylist::CompactStorage() throw ( GnError )
{
	gnsdk_error_t error = gnsdk_playlist_storage_compact();

	if(error) { throw GnError(); }
}

bool GnPlaylist::StorageIsValid() const throw ( GnError )
{
	gnsdk_error_info_t b_valid;

	gnsdk_error_t      error = gnsdk_playlist_storage_validate(&b_valid);

	if(error) { throw GnError(); }
	return b_valid.error_code == GNSDK_SUCCESS;
}

gn_iterable_container<GnPlaylist::storage_iterator>
GnPlaylist::StoredCollections() throw ( GnError )
{
	gnsdk_uint32_t              count = 0;

	gnsdk_error_t               error = gnsdk_playlist_storage_count_collections(&count);

	if(error) { throw ( GnError() ); }
	collection_storage_provider provider;
	return gn_iterable_container<storage_iterator>(storage_iterator(provider, 0, count, GNSDK_NULL), storage_iterator(provider, count, count, GNSDK_NULL) );
}

GnPlaylistCollection GnPlaylist::LoadCollection(storage_iterator itr) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t coll_handle = GNSDK_NULL;

	gnsdk_error_t                      error = gnsdk_playlist_storage_load_collection(*itr, &coll_handle);

	if(error) { throw GnError(); }

	GnPlaylistCollection               retVal;

	retVal.AcceptOwnership(coll_handle);

	return retVal;
}

GnPlaylistCollection GnPlaylist::LoadCollection(gnsdk_cstr_t collectionName) throw ( GnError )
{
	gnsdk_playlist_collection_handle_t coll_handle = GNSDK_NULL;

	gnsdk_error_t                      error = gnsdk_playlist_storage_load_collection(collectionName, &coll_handle);

	if(error) { throw GnError(); }

	GnPlaylistCollection               retVal;

	retVal.AcceptOwnership(coll_handle);

	return retVal;
}
#endif
