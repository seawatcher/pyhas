var group___video___query_functions =
[
    [ "gnsdk_video_query_find_videos", "group___video___query_functions.html#gaa25d54cb8f875468202eb5c089522235", null ],
    [ "gnsdk_video_query_create", "group___video___query_functions.html#ga95768083a7b712df46088a2b0ca3f21e", null ],
    [ "gnsdk_video_query_find_contributors", "group___video___query_functions.html#ga482bd25fcd21f2da8cc6a103862dba12", null ],
    [ "gnsdk_video_query_find_objects", "group___video___query_functions.html#gaecfe909f9c5248ce327865b310658020", null ],
    [ "gnsdk_video_query_find_products", "group___video___query_functions.html#ga182306aded9b5183833e2e263a7dde83", null ],
    [ "gnsdk_video_query_find_programs", "group___video___query_functions.html#ga4b9937796c999248761189bf998816ee", null ],
    [ "gnsdk_video_query_find_seasons", "group___video___query_functions.html#gab2e23bb28898721a0b7cb75bee77da91", null ],
    [ "gnsdk_video_query_find_series", "group___video___query_functions.html#ga13842c3504f8b99e358df50904deff33", null ],
    [ "gnsdk_video_query_find_suggestions", "group___video___query_functions.html#ga95cad799af68a3ce14cf81a397e55f6d", null ],
    [ "gnsdk_video_query_find_works", "group___video___query_functions.html#ga533a51b68a5bfd48547fa0ed418a14cc", null ],
    [ "gnsdk_video_query_option_get", "group___video___query_functions.html#gae246b519799170174cd3b2ee8b41472a", null ],
    [ "gnsdk_video_query_option_set", "group___video___query_functions.html#ga00949443a70c950f35af75b538b41d4c", null ],
    [ "gnsdk_video_query_release", "group___video___query_functions.html#ga8fab2a5c9d423e62e1f501b608759456", null ],
    [ "gnsdk_video_query_set_external_id", "group___video___query_functions.html#ga9e2a3fa8da15ca800d869b6aa9321213", null ],
    [ "gnsdk_video_query_set_filter", "group___video___query_functions.html#ga8b0861794f55a3399c3a83ce2ec605d4", null ],
    [ "gnsdk_video_query_set_filter_by_list_element", "group___video___query_functions.html#gad939a489c4a88b8b7da81c593093704c", null ],
    [ "gnsdk_video_query_set_gdo", "group___video___query_functions.html#ga8aa6ed7df83737e32d45a9ca2fd4bd07", null ],
    [ "gnsdk_video_query_set_text", "group___video___query_functions.html#gac0188105149ab4db7c38feda9ee5bdb8", null ],
    [ "gnsdk_video_query_set_toc_string", "group___video___query_functions.html#ga6aa4c01a3f4fc7c6bc93b6f9187a4334", null ]
];