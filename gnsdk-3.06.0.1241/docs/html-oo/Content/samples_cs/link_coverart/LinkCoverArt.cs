﻿/* 
 * Copyright (c) 2000-2013 Gracenote.
 * 
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 * 
 * Some code herein may be covered by US and international patents.
 */
 
/* 
 *  Name: LinkCoverArt
 *  Description:
 *  Retrieves a coverart image using Link starting with a serialized GDO as source.
 * 
 *  Command-line Syntax:
 *  sample clientId clientTag license libPath
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GracenoteSDK;
namespace Sample
{
    public class LinkCoverArt
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string localDB = "../../../sample_db";
        private static string applicationVersion = "1.0.0.0";

        //set this to false to do online queries
        private static bool useLocal = false;
        private static GnUser user = null;
                
        #region Initializegnsdk

        // Callback delegate called when loading locale
        public class LookupStatusEvents : GnStatusEventsDelegate
        {
            /*-----------------------------------------------------------------------------
	         *  status_event
	         */
            public override void status_event(gnsdk_status_t locale_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("Status ");
                switch (locale_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Begin  ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write("Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            /*-----------------------------------------------------------------------------
	     *  cancel_check
	     */
            public override bool cancel_check()
            {
                return false;
            }
        };

        // GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks 
        public class LinkEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t link_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Link Query ...\t");
                switch (link_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        // Display local Gracenote DB information.
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }

            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        //  Load locale 
        //  Deserialize existing locale, or register new one.
        //  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
        static void LoadLocale(GnUser user)
        {
            using (LookupStatusEvents localeEvents = new LookupStatusEvents())
            {
                if (useLocal)
                {
                    GnLocale locale = new GnLocale(
                           GnSDK.kLocaleGroupMusic,            /* Locale group */
                           GnSDK.kLanguageEnglish,             /* Languae */
                           GnSDK.kRegionGlobal,                /* Region */
                           GnSDK.kDescriptorDefault,           /* Descriptor */
                           user,                               /* User */
                           null /*localeEvents*/               /* locale Events object */
                           );
                }
                else
                {
                    if (!File.Exists("serialized_locale.txt"))
                    {
                        GnLocale locale = new GnLocale(
                           GnSDK.kLocaleGroupMusic,            /* Locale group */
                           GnSDK.kLanguageEnglish,             /* Languae */
                           GnSDK.kRegionGlobal,                /* Region */
                           GnSDK.kDescriptorDefault,           /* Descriptor */
                           user,                               /* User */
                           null /*localeEvents*/               /* locale Events object */
                           );

                        //Serialize locale, so we can use reuse it
                        if (!File.Exists("serialized_locale.txt"))
                        {
                            String serializedLocaleString = (locale.Serialize().ToString());
                            using (StreamWriter outfile = new StreamWriter("serialized_locale.txt"))
                            {
                                outfile.Write(serializedLocaleString);
                                outfile.Close();
                            }
                        }
                        return;
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader("serialized_locale.txt"))
                        {
                            String serializedLocaleString = sr.ReadToEnd();

                            GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                            return;
                        }

                    }
                }
            }
        } 

        #endregion

        /// <summary>
        /// Fetching Image
        /// </summary>
        /// <param name="coverArt">size in bytes</param>
        /// <param name="imageTypeStr">name</param>
        private static void fetchImage(string coverArt, String imageTypeStr)
        {
            if (coverArt != null)
            {
                Console.WriteLine("\nRETRIEVED: " + imageTypeStr
                                    + " image: "
                                    + coverArt
                                    + " byte JPEG");
            }
            else
            {
                // Do not return error code for not found. 
                // For image to be fetched, it must exist in the size specified and you must be entitled to fetch images.
                Console.WriteLine("\nNOT FOUND: " + imageTypeStr + " image ");
            }
        }

        private static void FetchCoverArt(GnUser user)
        {

            Console.WriteLine("\n*****Sample Link Album Query*****");

            using (GnStatusEventsDelegate linkStatusEvents = new LinkEvents())
            {

                string serializedGdo = "WEcxAbwX1+DYDXSI3nZZ/L9ntBr8EhRjYAYzNEwlFNYCWkbGGLvyitwgmBccgJtgIM/dkcbDgrOqBMIQJZMmvysjCkx10ppXc68ZcgU0SgLelyjfo1Tt7Ix/cn32BvcbeuPkAk0WwwReVdcSLuO8cYxAGcGQrEE+4s2H75HwxFG28r/yb2QX71pR";

                // Typically, the GDO passed in to a Link query will come from the output of a GNSDK query.
                // For an example of how to perform a query and get a GDO please refer to the documentation
                // or other sample applications.
                // The below serialized GDO was an 1-track album result from another GNSDK query.                

                GnDataObject gnDataobject = new GnDataObject(serializedGdo);

                // Get a query album     

                GnLink link = new GnLink(user, gnDataobject, null);

                if (link != null)
                {
                    /* 
                     * Get coverart
                     */
                    GnLinkContent coverArt = link.CoverArt(GnImageSize.size_170, GnImagePreference.smallest);
                    byte[] coverData = coverArt.Data;

                    /* 
	                 * Do something with the image,
	                 * e.g. display size and save 
	                 */
                    fetchImage(coverData.Length.ToString(), "cover art");
                    if(coverData != null)
                        File.WriteAllBytes("cover.jpeg", coverData);

                    /* 
                     * Get genre art
                     */
                    GnLinkContent imageArtist = link.ArtistImage(GnImageSize.size_170, GnImagePreference.smallest);
                    byte[] artistData = imageArtist.Data;
                    /* 
	                 * Do something with the image,
	                 * e.g. display size and save 
	                 */
                    fetchImage(artistData.Length.ToString(), "artist");
                    if (artistData != null)
                        File.WriteAllBytes("artist.jpeg", artistData);
                }
            }

        }

        /******************************************************************
        *
        *    MAIN
        *
        ******************************************************************/
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                clientID = args[0];
                clientTag = args[1];
                licensePath = args[2];
                libPath = args[3];

                try
                {
                    // Initialize SDK
                    GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                    // Dispaly SDK version
                    Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                    // Enable logging
                    gnsdk.LoggingEnable("sample.log", GnSDK.GN_LOG_PKG_ALL, GnSDK.GN_LOG_LEVEL_ERROR, GnSDK.GN_LOG_OPTION_ALL, 0, false);

                    /*
                     *    Load existing user handle, or register new one.
                     */
                    user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                    if (useLocal)
                    {
                        // Initialize the Storage SQLite Library
                        GnStorageSqlite gnStorage = new GnStorageSqlite();

                        gnStorage.StorageFolder = localDB;

                        GnLookupLocal gnLookupLocal = new GnLookupLocal();

                        //Set the user option to use our local Gracenote DB unless overridden			            
                        user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                        // Display information about our local EDB 
                        DisplayEmbeddedDbInfo(gnLookupLocal);

                    }
                    else
                    {
                        user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                    }


                    //  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
                    LoadLocale(user);

                    // Perform a sample query
                    FetchCoverArt(user);
                }
                // All gracenote sdk objects throws GnException
                catch (GnException e)
                {
                    Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                    Console.WriteLine("Error API            :: " + e.ErrorAPI);
                    Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                    Console.WriteLine("SourceE rror Modulee :: " + e.SourceErrorModule);
                }
            }

        }

    }
}
