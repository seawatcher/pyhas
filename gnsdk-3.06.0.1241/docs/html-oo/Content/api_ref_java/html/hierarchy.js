var hierarchy =
[
    [ "com.gracenote.gnsdk.collection_ident_provider", "a00001.html", null ],
    [ "com.gracenote.gnsdk.collection_join_provider", "a00002.html", null ],
    [ "com.gracenote.gnsdk.collection_storage_provider", "a00003.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistCollectionSyncEventsListener.EventsIdentiferStatus", "a00004.html", null ],
    [ "Exception", null, [
      [ "com.gracenote.gnsdk.GnException", "a00036.html", null ]
    ] ],
    [ "com.gracenote.gnsdk.GnMoodgridPresentation.FilterConditionType", "a00005.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridPresentation.FilterListType", "a00006.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistCollection.GenerateMoreLikeThisOption", "a00007.html", null ],
    [ "com.gracenote.gnsdk.GnAlbumIterable", "a00009.html", null ],
    [ "com.gracenote.gnsdk.GnAlbumIterator", "a00010.html", null ],
    [ "com.gracenote.gnsdk.GnAlbumProvider", "a00011.html", null ],
    [ "com.gracenote.gnsdk.GnAudioSource", "a00014.html", null ],
    [ "com.gracenote.gnsdk.GnContributorIterable", "a00018.html", null ],
    [ "com.gracenote.gnsdk.GnContributorIterator", "a00019.html", null ],
    [ "com.gracenote.gnsdk.GnCreditIterable", "a00021.html", null ],
    [ "com.gracenote.gnsdk.GnCreditIterator", "a00022.html", null ],
    [ "com.gracenote.gnsdk.GnCreditProvider", "a00023.html", null ],
    [ "com.gracenote.gnsdk.GnDataMatchIterable", "a00025.html", null ],
    [ "com.gracenote.gnsdk.GnDataMatchIterator", "a00026.html", null ],
    [ "com.gracenote.gnsdk.GnDataMatchProvider", "a00027.html", null ],
    [ "com.gracenote.gnsdk.GnDspFeatureQuality", "a00031.html", null ],
    [ "com.gracenote.gnsdk.GnDspFeatureType", "a00032.html", null ],
    [ "com.gracenote.gnsdk.GnError", "a00034.html", null ],
    [ "com.gracenote.gnsdk.GnErrorInfo", "a00035.html", null ],
    [ "com.gracenote.gnsdk.GnExternalIDIterable", "a00038.html", null ],
    [ "com.gracenote.gnsdk.GnExternalIDIterator", "a00039.html", null ],
    [ "com.gracenote.gnsdk.GnExternalIDProvider", "a00040.html", null ],
    [ "com.gracenote.gnsdk.GnFingerprintType", "a00041.html", null ],
    [ "com.gracenote.gnsdk.GnIFingerprinterMusicIDFile", "a00043.html", null ],
    [ "com.gracenote.gnsdk.GnImagePreference", "a00044.html", null ],
    [ "com.gracenote.gnsdk.GnImageSize", "a00045.html", null ],
    [ "com.gracenote.gnsdk.GnSDK.GnLicenseInputMode", "a00047.html", null ],
    [ "com.gracenote.gnsdk.GnLinkContentType", "a00050.html", null ],
    [ "com.gracenote.gnsdk.GnLinkDataType", "a00051.html", null ],
    [ "com.gracenote.gnsdk.GnListElementChildIterable", "a00054.html", null ],
    [ "com.gracenote.gnsdk.GnListElementChildIterator", "a00055.html", null ],
    [ "com.gracenote.gnsdk.GnListLevelIterable", "a00056.html", null ],
    [ "com.gracenote.gnsdk.GnListLevelIterator", "a00057.html", null ],
    [ "com.gracenote.gnsdk.GnListRenderFlags", "a00058.html", null ],
    [ "com.gracenote.gnsdk.GnLocaleInfoIterable", "a00061.html", null ],
    [ "com.gracenote.gnsdk.GnLocaleInfoIterator", "a00062.html", null ],
    [ "com.gracenote.gnsdk.GnLocaleInfoProvider", "a00063.html", null ],
    [ "com.gracenote.gnsdk.GnLocalStorageInfoKey", "a00064.html", null ],
    [ "com.gracenote.gnsdk.GnLocalStorageName", "a00065.html", null ],
    [ "com.gracenote.gnsdk.GnLookupData", "a00066.html", null ],
    [ "com.gracenote.gnsdk.GnLookupMode", "a00068.html", null ],
    [ "com.gracenote.gnsdk.GnMatchIterable", "a00070.html", null ],
    [ "com.gracenote.gnsdk.GnMatchIterator", "a00071.html", null ],
    [ "com.gracenote.gnsdk.GnMatchProvider", "a00072.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridDataPoint", "a00075.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridIdentifier", "a00076.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridPresentationDataIterable", "a00078.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridPresentationDataIterator", "a00079.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridPresentationType", "a00080.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridProviderIterable", "a00082.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridProviderIterator", "a00083.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridResultIterable", "a00085.html", null ],
    [ "com.gracenote.gnsdk.GnMoodgridResultIterator", "a00086.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIdFileCallbackStatus", "a00089.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIDFileEventsListener", "a00090.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIdFileFileInfoStatus", "a00091.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIdFileHandleStatus", "a00092.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIDFileInfoIterable", "a00094.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIDFileInfoIterator", "a00095.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIDFileInfoProvider", "a00096.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIDStreamEventsListener", "a00098.html", null ],
    [ "com.gracenote.gnsdk.GnMusicIdStreamStatus", "a00099.html", null ],
    [ "com.gracenote.gnsdk.GnNameIterable", "a00101.html", null ],
    [ "com.gracenote.gnsdk.GnNameIterator", "a00102.html", null ],
    [ "com.gracenote.gnsdk.GnNameProvider", "a00103.html", null ],
    [ "com.gracenote.gnsdk.GnObject", "a00104.html", [
      [ "com.gracenote.gnsdk.GnDataObject", "a00028.html", [
        [ "com.gracenote.gnsdk.GnAlbum", "a00008.html", null ],
        [ "com.gracenote.gnsdk.GnArtist", "a00012.html", null ],
        [ "com.gracenote.gnsdk.GnArtistType", "a00013.html", null ],
        [ "com.gracenote.gnsdk.GnAudioWork", "a00015.html", null ],
        [ "com.gracenote.gnsdk.GnContents", "a00016.html", null ],
        [ "com.gracenote.gnsdk.GnContributor", "a00017.html", null ],
        [ "com.gracenote.gnsdk.GnCredit", "a00020.html", null ],
        [ "com.gracenote.gnsdk.GnDataMatch", "a00024.html", null ],
        [ "com.gracenote.gnsdk.GnEra", "a00033.html", null ],
        [ "com.gracenote.gnsdk.GnExternalID", "a00037.html", null ],
        [ "com.gracenote.gnsdk.GnGenre", "a00042.html", null ],
        [ "com.gracenote.gnsdk.GnLanguage", "a00046.html", null ],
        [ "com.gracenote.gnsdk.GnMatch", "a00069.html", null ],
        [ "com.gracenote.gnsdk.GnMood", "a00073.html", null ],
        [ "com.gracenote.gnsdk.GnName", "a00100.html", null ],
        [ "com.gracenote.gnsdk.GnOrigin", "a00105.html", null ],
        [ "com.gracenote.gnsdk.GnPlaylistMetadata", "a00114.html", null ],
        [ "com.gracenote.gnsdk.GnRating", "a00118.html", null ],
        [ "com.gracenote.gnsdk.GnResponseAlbums", "a00119.html", null ],
        [ "com.gracenote.gnsdk.GnResponseContributor", "a00120.html", null ],
        [ "com.gracenote.gnsdk.GnResponseDataMatches", "a00121.html", null ],
        [ "com.gracenote.gnsdk.GnResponseMatches", "a00122.html", null ],
        [ "com.gracenote.gnsdk.GnResponseTracks", "a00123.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoObjects", "a00124.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoProduct", "a00125.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoProgram", "a00126.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoSeasons", "a00127.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoSeries", "a00128.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoSuggestions", "a00129.html", null ],
        [ "com.gracenote.gnsdk.GnResponseVideoWork", "a00130.html", null ],
        [ "com.gracenote.gnsdk.GnRole", "a00131.html", null ],
        [ "com.gracenote.gnsdk.GnSortable", "a00137.html", null ],
        [ "com.gracenote.gnsdk.GnTempo", "a00142.html", null ],
        [ "com.gracenote.gnsdk.GnTitle", "a00144.html", null ],
        [ "com.gracenote.gnsdk.GnTrack", "a00145.html", null ],
        [ "com.gracenote.gnsdk.GnTVProgram", "a00149.html", null ],
        [ "com.gracenote.gnsdk.GnVideoChapter", "a00156.html", null ],
        [ "com.gracenote.gnsdk.GnVideoCredit", "a00160.html", null ],
        [ "com.gracenote.gnsdk.GnVideoDisc", "a00164.html", null ],
        [ "com.gracenote.gnsdk.GnVideoFeature", "a00169.html", null ],
        [ "com.gracenote.gnsdk.GnVideoLayer", "a00174.html", null ],
        [ "com.gracenote.gnsdk.GnVideoProduct", "a00179.html", null ],
        [ "com.gracenote.gnsdk.GnVideoSeason", "a00185.html", null ],
        [ "com.gracenote.gnsdk.GnVideoSeries", "a00189.html", null ],
        [ "com.gracenote.gnsdk.GnVideoSide", "a00193.html", null ],
        [ "com.gracenote.gnsdk.GnVideoWork", "a00198.html", null ]
      ] ],
      [ "com.gracenote.gnsdk.GnDsp", "a00029.html", null ],
      [ "com.gracenote.gnsdk.GnDspFeature", "a00030.html", null ],
      [ "com.gracenote.gnsdk.GnLink", "a00048.html", null ],
      [ "com.gracenote.gnsdk.GnLinkContent", "a00049.html", null ],
      [ "com.gracenote.gnsdk.GnList", "a00052.html", null ],
      [ "com.gracenote.gnsdk.GnListElement", "a00053.html", null ],
      [ "com.gracenote.gnsdk.GnLocale", "a00059.html", null ],
      [ "com.gracenote.gnsdk.GnLocaleInfo", "a00060.html", null ],
      [ "com.gracenote.gnsdk.GnLookupLocal", "a00067.html", null ],
      [ "com.gracenote.gnsdk.GnMoodgrid", "a00074.html", null ],
      [ "com.gracenote.gnsdk.GnMoodgridPresentation", "a00077.html", null ],
      [ "com.gracenote.gnsdk.GnMoodgridProvider", "a00081.html", null ],
      [ "com.gracenote.gnsdk.GnMoodgridResult", "a00084.html", null ],
      [ "com.gracenote.gnsdk.GnMusicID", "a00087.html", null ],
      [ "com.gracenote.gnsdk.GnMusicIDFile", "a00088.html", null ],
      [ "com.gracenote.gnsdk.GnMusicIDFileInfo", "a00093.html", null ],
      [ "com.gracenote.gnsdk.GnMusicIDStream", "a00097.html", null ],
      [ "com.gracenote.gnsdk.GnPlaylist", "a00106.html", null ],
      [ "com.gracenote.gnsdk.GnPlaylistCollection", "a00107.html", null ],
      [ "com.gracenote.gnsdk.GnPlaylistResult", "a00115.html", null ],
      [ "com.gracenote.gnsdk.GnStorageSqlite", "a00140.html", null ],
      [ "com.gracenote.gnsdk.GnString", "a00141.html", null ],
      [ "com.gracenote.gnsdk.GnUser", "a00153.html", null ],
      [ "com.gracenote.gnsdk.GnVideo", "a00155.html", null ]
    ] ],
    [ "com.gracenote.gnsdk.GnPlaylistCollectionIdentIterable", "a00108.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistCollectionIdentIterator", "a00109.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistCollectionSyncEventsListener", "a00110.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistIdentifier", "a00111.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistJoinIterable", "a00112.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistJoinIterator", "a00113.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistResultIdentIterable", "a00116.html", null ],
    [ "com.gracenote.gnsdk.GnPlaylistResultIdentIterator", "a00117.html", null ],
    [ "com.gracenote.gnsdk.GnSDK", "a00132.html", null ],
    [ "com.gracenote.gnsdk.gnsdk_javaConstants", "a00134.html", [
      [ "com.gracenote.gnsdk.gnsdk_java", "a00133.html", null ]
    ] ],
    [ "com.gracenote.gnsdk.gnsdk_javaJNI", "a00135.html", null ],
    [ "com.gracenote.gnsdk.gnsdk_musicidstream_callbacks_t", "a00136.html", null ],
    [ "com.gracenote.gnsdk.GnStatus", "a00138.html", null ],
    [ "com.gracenote.gnsdk.GnStatusEventsListener", "a00139.html", null ],
    [ "com.gracenote.gnsdk.GnThreadPriority", "a00143.html", null ],
    [ "com.gracenote.gnsdk.GnTrackIterable", "a00146.html", null ],
    [ "com.gracenote.gnsdk.GnTrackIterator", "a00147.html", null ],
    [ "com.gracenote.gnsdk.GnTrackProvider", "a00148.html", null ],
    [ "com.gracenote.gnsdk.GnTVProgramIterable", "a00150.html", null ],
    [ "com.gracenote.gnsdk.GnTVProgramIterator", "a00151.html", null ],
    [ "com.gracenote.gnsdk.GnTVProgramProvider", "a00152.html", null ],
    [ "com.gracenote.gnsdk.GnSDK.GnUserRegisterMode", "a00154.html", null ],
    [ "com.gracenote.gnsdk.GnVideoChapterIterable", "a00157.html", null ],
    [ "com.gracenote.gnsdk.GnVideoChapterIterator", "a00158.html", null ],
    [ "com.gracenote.gnsdk.GnVideoChapterProvider", "a00159.html", null ],
    [ "com.gracenote.gnsdk.GnVideoCreditIterable", "a00161.html", null ],
    [ "com.gracenote.gnsdk.GnVideoCreditIterator", "a00162.html", null ],
    [ "com.gracenote.gnsdk.GnVideoCreditProvider", "a00163.html", null ],
    [ "com.gracenote.gnsdk.GnVideoDiscIterable", "a00165.html", null ],
    [ "com.gracenote.gnsdk.GnVideoDiscIterator", "a00166.html", null ],
    [ "com.gracenote.gnsdk.GnVideoDiscProvider", "a00167.html", null ],
    [ "com.gracenote.gnsdk.GnVideoExternalIdType", "a00168.html", null ],
    [ "com.gracenote.gnsdk.GnVideoFeatureIterable", "a00170.html", null ],
    [ "com.gracenote.gnsdk.GnVideoFeatureIterator", "a00171.html", null ],
    [ "com.gracenote.gnsdk.GnVideoFeatureProvider", "a00172.html", null ],
    [ "com.gracenote.gnsdk.GnVideoFilterType", "a00173.html", null ],
    [ "com.gracenote.gnsdk.GnVideoLayerIterable", "a00175.html", null ],
    [ "com.gracenote.gnsdk.GnVideoLayerIterator", "a00176.html", null ],
    [ "com.gracenote.gnsdk.GnVideoLayerProvider", "a00177.html", null ],
    [ "com.gracenote.gnsdk.GnVideoListElementFilterType", "a00178.html", null ],
    [ "com.gracenote.gnsdk.GnVideoProductIterable", "a00180.html", null ],
    [ "com.gracenote.gnsdk.GnVideoProductIterator", "a00181.html", null ],
    [ "com.gracenote.gnsdk.GnVideoProductProvider", "a00182.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSearchField", "a00183.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSearchType", "a00184.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeasonIterable", "a00186.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeasonIterator", "a00187.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeasonProvider", "a00188.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeriesIterable", "a00190.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeriesIterator", "a00191.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSeriesProvider", "a00192.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSideIterable", "a00194.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSideIterator", "a00195.html", null ],
    [ "com.gracenote.gnsdk.GnVideoSideProvider", "a00196.html", null ],
    [ "com.gracenote.gnsdk.GnVideoTOCFlag", "a00197.html", null ],
    [ "com.gracenote.gnsdk.GnVideoWorkIterable", "a00199.html", null ],
    [ "com.gracenote.gnsdk.GnVideoWorkIterator", "a00200.html", null ],
    [ "com.gracenote.gnsdk.GnVideoWorkProvider", "a00201.html", null ],
    [ "com.gracenote.gnsdk.list_element_child_provider", "a00202.html", null ],
    [ "com.gracenote.gnsdk.list_element_provider", "a00203.html", null ],
    [ "com.gracenote.gnsdk.moodgrid_provider", "a00204.html", null ],
    [ "com.gracenote.gnsdk.moodgrid_result_provider", "a00205.html", null ],
    [ "com.gracenote.gnsdk.presentation_data_provider", "a00206.html", null ],
    [ "com.gracenote.gnsdk.result_provider", "a00207.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_f_p_void_enum_gnsdk_musicidstream_status_t__void", "a00208.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_f_p_void_enum_gnsdk_status_t_unsigned_int_size_t_size_t_p_char__void", "a00209.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_f_p_void_p_void_gnsdk_gdo_handle_t__void", "a00210.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_gdo_handle_t", "a00211.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_list_element_handle_t", "a00212.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_list_handle_t", "a00213.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_manager_handle_t", "a00214.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_playlist_results_handle_t", "a00215.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gnsdk_user_handle_t", "a00216.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gracenote__gn_facade_range_iteratorT_char_const_p_gracenote__playlist__collection_storage_provider_t", "a00217.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_gracenote__metadata__gn_gdo_providerT_gracenote__metadata__GnContributor_t", "a00218.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_p_char", "a00219.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_p_void", "a00220.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_unsigned_char", "a00221.html", null ],
    [ "com.gracenote.gnsdk.SWIGTYPE_p_void", "a00222.html", null ],
    [ "boolean", null, null ],
    [ "final int", null, null ],
    [ "long", null, null ],
    [ "static final int", null, null ],
    [ "static int", null, null ],
    [ "String", null, null ]
];