var classgracenote_1_1correlates_1_1_gn_correlates =
[
    [ "GnCorrelates", "classgracenote_1_1correlates_1_1_gn_correlates.html#aa7b815d5f33a023a695015f100fb3e86", null ],
    [ "~GnCorrelates", "classgracenote_1_1correlates_1_1_gn_correlates.html#aa3378c12c804297ec2bb5ba225f747ce", null ],
    [ "native", "classgracenote_1_1correlates_1_1_gn_correlates.html#a48097f6e1af739c8f28708f088191531", null ],
    [ "RenderSetToXml", "classgracenote_1_1correlates_1_1_gn_correlates.html#a1952e36b415e3cfb4bf038f46fa7fda9", null ],
    [ "Revision", "classgracenote_1_1correlates_1_1_gn_correlates.html#a290b92f7dbbd278f0d77a93c6e6cf3b4", null ],
    [ "Type", "classgracenote_1_1correlates_1_1_gn_correlates.html#a7f9df3eed26e22dc200eead2576aec39", null ],
    [ "kCorrelateArtistTypes", "classgracenote_1_1correlates_1_1_gn_correlates.html#a3c5d5f33ee7b83aa470b35fd64e0292a", null ],
    [ "kCorrelateEras", "classgracenote_1_1correlates_1_1_gn_correlates.html#abb3f3c1bb1fe822077586a6ea9a263c6", null ],
    [ "kCorrelateGenres", "classgracenote_1_1correlates_1_1_gn_correlates.html#a2c7514566058bfb604b6d9f523da60ff", null ],
    [ "kCorrelateMoods", "classgracenote_1_1correlates_1_1_gn_correlates.html#a3571db15c4b1db829612a09a292fe6b1", null ],
    [ "kCorrelateOrigins", "classgracenote_1_1correlates_1_1_gn_correlates.html#a03cf660b17bdb8e683079a6f1811a880", null ]
];