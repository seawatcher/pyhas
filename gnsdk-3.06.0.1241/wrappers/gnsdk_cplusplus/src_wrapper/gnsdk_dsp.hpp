/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_DSP_HPP_
#define _GNSDK_DSP_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"

#ifdef GNSDK_DSP

namespace gracenote
{
	namespace dsp
	{
		enum GnDspFeatureQuality
		{
			kDspFeatureQualityStandard = 1,
			kDspFeatureQualityShort,
			kDspFeatureQualitySilent
		};

		// have to keep GnDspFeatureType enum and the fpTypeString lookup table in sync.
		enum GnDspFeatureType
		{
			kDspFeatureTypeAFX3 = 1,
			kDspFeatureTypeChroma,
			kDspFeatureTypeCantametrixQ,
			kDspFeatureTypeCantametrixR,
			kDspFeatureTypeFEXModule,
			kDspFeatureTypeFraunhofer,
			kDspFeatureTypeFAPIQ3sLQ,
			kDspFeatureTypeFAPIQ3sMQ,
			kDspFeatureTypeFAPIQ6sMQ,
			kDspFeatureTypeFAPIR,
			kDspFeatureTypeNanoFAPIQ,
			kDspFeatureTypeMicroFAPIQ
		};

		class GnDspFeature : public GnObject
		{
		public:
			gnsdk_cstr_t        mData;
			GnDspFeatureQuality mFeatureQuality;
		};

		/**************************************************************************
		** GnDsp
		*/
		class GnDsp : public GnObject
		{
		public:
			/**
			 * Initializes the DSP library.
			 * @param user set user
			 * @param featureType The kind of DSP feature, for example a fingerprint.
			 * @param audioSampleRate The source audio sample rate.
			 * @param audioSampleSize The source audio sample size.
			 * @param audioChannels	The source audio channels
			 */
			GnDsp(const GnUser& user, GnDspFeatureType featureType, gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels)  throw ( GnError );
			virtual ~GnDsp();

			/**
			 *  Retrieves GnDsp SDK version string.
			 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
			 *  string is a constant. Do not attempt to modify or delete.
			 *  Example: 1.2.3.123 (Major.Minor.Improvement.Build)
			 *  Major: New functionality
			 *  Minor: New or changed features
			 *  Improvement: Improvements and fixes
			 *  Build: Internal build number
			 */
			static gnsdk_cstr_t Version();

			/**
			 *  Retrieves the GnDsp SDK's build date string.
			 *  @return gnsdk_cstr_t Build date string of the format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
			 * string is a constant. Do not attempt to modify or delete.
			 *  Example build date string: 2008-02-12 00:41 UTC
			 */
			static gnsdk_cstr_t BuildDate();

			/**
			 * Use this method to feed audio in to GnDsp until it returns true
			 * @param audioData The source audio
			 * @param audioDataBytes The source audio size in bytes
			 * @return false : GnDsp needs more audio, true : GnDSp received enough audio to generate required feature
			 */
			bool FeatureAudioWrite(const gnsdk_byte_t* audioData, gnsdk_size_t audioDataBytes)  throw ( GnError );

			/**
			 * Indicates the the DSP feature has reached the end of the write operation.
			 */
			void FeatureEndOfAudioWrite() throw ( GnError );

			/**
			 * Retrieve GnDspFeature
			 * @return GnDspFeature
			 */
			GnDspFeature*              FeatureRetrieve() throw ( GnError );

		private:
			gnsdk_dsp_feature_handle_t mFeatureHandle;

			gnsdk_cstr_t               _GetFeatureType(GnDspFeatureType type)
			{
				gnsdk_cstr_t fpTypeString[] =
				{
					"AFX3",
					"Chroma",
					"Cantametrix-Q",
					"Cantametrix-R",
					"FEXModule",
					"Fraunhofer",
					"FAPI-Q-3s-LQ",
					"FAPI-Q-3s-MQ",
					"FAPI-Q-6s-MQ",
					"FAPI-R",
					"NanoFAPI-Q",
					"MicroFAPI-Q",
					"Sony12Tones"
				};

				return fpTypeString[type];
			}

			/* disallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnDsp);
		};
	} // namespace Dsp
}     // namespace GracenoteSDK
#endif // GNSDK_DSP
#endif // _GNSDK_DSP_HPP_
