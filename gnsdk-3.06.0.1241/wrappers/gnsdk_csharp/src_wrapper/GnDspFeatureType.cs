/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

public enum GnDspFeatureType {
  kDspFeatureTypeAFX3 = 1,
  kDspFeatureTypeChroma,
  kDspFeatureTypeCantametrixQ,
  kDspFeatureTypeCantametrixR,
  kDspFeatureTypeFEXModule,
  kDspFeatureTypeFraunhofer,
  kDspFeatureTypeFAPIQ3sLQ,
  kDspFeatureTypeFAPIQ3sMQ,
  kDspFeatureTypeFAPIQ6sMQ,
  kDspFeatureTypeFAPIR,
  kDspFeatureTypeNanoFAPIQ,
  kDspFeatureTypeMicroFAPIQ
}

}
