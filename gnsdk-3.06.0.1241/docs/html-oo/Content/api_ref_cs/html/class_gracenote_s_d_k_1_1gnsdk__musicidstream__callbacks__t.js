var class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t =
[
    [ "gnsdk_musicidstream_callbacks_t", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#a71d005f3004ae07dca3ee83e14c25929", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#a943450208aa6b0310800ade1fdb46b11", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#aa2848fcb8c922c9fc32925d4a6107131", null ],
    [ "callback_mids_status", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#accac40dffb8745f23782859b36821b85", null ],
    [ "callback_result_available", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#a24c2095e7e6464c175ef603f8bfe95e0", null ],
    [ "callback_status", "class_gracenote_s_d_k_1_1gnsdk__musicidstream__callbacks__t.html#a46b9916cda2847f2f1d7e1798d83e0c6", null ]
];