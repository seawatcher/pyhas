/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
*/

/*
 *  Name: MusicIdStream
 *  Description:
 *  This example uses MusicID-Stream to fingerprint and identify a music track.
 *
 *  Command-line Syntax:
 *  sample clientID clientTag license gnsdkLibraryPath
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;

namespace Sample
{
    public class MusicIdStream
    {
        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk

        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }
        #endregion

        private static void DisplayGnTitle(GnAlbum album)
        {
            GnTitle gnTitle = album.Title;
            Console.WriteLine("          Title: " + gnTitle.Display);
        }
        
        private static void DisplayFindAlbumResutlsByFingerprint(GnResponseAlbums response)
        {
            GnAlbumEnumerable albumEnumerable = response.Albums;
            if (albumEnumerable.count() > 0)
            {
                GnAlbum finalgnAlbum = null;
                //Console.WriteLine("    Album count: " + albumEnumerable.count());
                foreach (GnAlbum album in albumEnumerable)
                {
                    //DisplayGnTitle(album);
                    if (finalgnAlbum == null)
                        finalgnAlbum = album;
                }
                if (finalgnAlbum != null)
                {
                    GnTitle iGnTitle = finalgnAlbum.Title;
                    Console.WriteLine("    Final album:");
                    Console.WriteLine("          Title: " + iGnTitle.Display);
                }
            }
            else
            {
                Console.WriteLine("\nNo albums found for the input.");
            }
        }
        
        private static void SetFingerprintBeginWriteEnd(GnMusicID gnMusicID)
        {
            bool complete = false;

            FileInfo file = new FileInfo(@"..\..\..\data\05-Hummingbird-sample.wav");

            using (BinaryReader b = new BinaryReader(File.Open(file.FullName, FileMode.Open, FileAccess.Read)))
            {
                b.BaseStream.Position = 0;

                /* skip the wave header (first 44 bytes). we know the format of our sample files*/
                b.BaseStream.Seek(44, SeekOrigin.Begin);

                byte[] audioData = b.ReadBytes(2048);

                gnMusicID.FingerprintBegin(GnFingerprintType.kFingerprintTypeGNFPX, 44100, 16, 2);

                while (audioData.Length > 0)
                {
                    complete = gnMusicID.FingerprintWrite(audioData, (uint)audioData.Length);
                    if (true == complete)
                        break;
                    else
                        audioData = b.ReadBytes(2048);
                }

                gnMusicID.FingerprintEnd();

                if (false == complete)
                {
                    /* Fingerprinter doesn't have enough data to generate a fingerprint. 
                        Note that the sample data does include one track that is too short to fingerprint. */
                    Console.WriteLine("\nWarning: input file does contain enough data to generate a fingerprint :" + file.FullName);
                }
            }
        }
        
        private static void MusicidFingerprintAlbum(GnUser user)
        {

            Console.WriteLine("\n*****Sample MID-Stream Query*****");

            try
            {
                GnMusicID gnMusicID = new GnMusicID(user);

                /* Set the input fingerprint*/
                SetFingerprintBeginWriteEnd(gnMusicID);

                // Perform the search
                GnResponseAlbums response = gnMusicID.FindAlbums(gnMusicID.FingerprintDataGet(), GnFingerprintType.kFingerprintTypeGNFPX);

                DisplayFindAlbumResutlsByFingerprint(response);

            }
            catch (GnException e)
            {
                Console.WriteLine("Error Code           :: " + e.ErrorCode);
                Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                Console.WriteLine("Error API            :: " + e.ErrorAPI);
                Console.WriteLine("Source Error Code    :: " + e.SourceErrorCode);
                Console.WriteLine("SourceE rror Module  :: " + e.SourceErrorModule);
            }
        }

        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }

            /* GNSDK initialization */
            try
            {
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                Console.WriteLine("\nGNSDK Product Version    : v" + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */



                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

				GnStorageSqlite gnStorage = new GnStorageSqlite();
                /* Set locale with desired Group, Language, Region and Descriptor
                * Set the 'locale' to return locale-specific results values. This examples loads an English locale.
                */
                GnLocale locale = new GnLocale( GnSDK.kLocaleGroupMusic,           /* Locale group */
												GnSDK.kLanguageEnglish,             /* Language */
												GnSDK.kRegionGlobal,                /* Region */
												GnSDK.kDescriptorDefault,           /* Descriptor */
												user,                               /* User */
												null /*localeEvents*/               /* locale Events object */
												);
				
				gnsdk.SetDefaultLocale(locale);
				
                MusicidFingerprintAlbum(user);
            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }
    }
}
