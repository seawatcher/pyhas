var group___g_d_o___types___video =
[
    [ "GNSDK_GDO_TYPE_RESPONSE_SUGGESTIONS", "group___g_d_o___types___video.html#gad79df46de26c1835116cdffb743cb8fc", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO", "group___g_d_o___types___video.html#gae51929a2fc9a381f9af083385cc70dc3", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO_OBJECT", "group___g_d_o___types___video.html#ga6f4869b2a21faeff50bc34f9aa39fe2f", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO_PRODUCT", "group___g_d_o___types___video.html#ga64f758849c5e22359498ac4cd31a01ea", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO_SEASON", "group___g_d_o___types___video.html#ga6da211e1e4de92277d3acd41f4ecca85", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES", "group___g_d_o___types___video.html#ga09dc2f887817e3b789780ab097fa5cb1", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK", "group___g_d_o___types___video.html#gadf9a46176b5852bcdf86c03daebc0c18", null ],
    [ "GNSDK_GDO_TYPE_RESPONSE_VIDEOCLIP", "group___g_d_o___types___video.html#ga3ed5a694d76a4f4b46178205953ae8f4", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_ADVERT", "group___g_d_o___types___video.html#ga288622035e3d8fc3538772e4dfcfa1bb", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_CHAPTER", "group___g_d_o___types___video.html#gae5b15487a30295a5159ce30639e1a4da", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_DISC", "group___g_d_o___types___video.html#gac7e416e3544b26750ddc5ac1fc297a29", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_FEATURE", "group___g_d_o___types___video.html#ga518852a21d83eb02776dfe8261a4b93f", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_LAYER", "group___g_d_o___types___video.html#ga8e5382da5dfd802a78759f1bbb16ecfa", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_PRODUCT", "group___g_d_o___types___video.html#ga4461837ffe433c5596abacfbafd1e30d", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_SEASON", "group___g_d_o___types___video.html#gab7114657c245639123536104ec62eec7", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_SERIES", "group___g_d_o___types___video.html#gab13aa12be7eb9c22e8e253826c8184c8", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_SIDE", "group___g_d_o___types___video.html#gaf1805b77da8f7d1d57f37a6d01deea4d", null ],
    [ "GNSDK_GDO_TYPE_VIDEO_WORK", "group___g_d_o___types___video.html#ga1ce0b3d6c43e0b6a1e3bdb3b28a16aca", null ],
    [ "GNSDK_GDO_TYPE_VIDEOCLIP", "group___g_d_o___types___video.html#gaa024d2506da220d26d689afd48c78c33", null ]
];