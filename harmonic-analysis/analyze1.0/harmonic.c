 /***************************************************************************/
 /*                                                                         */
 /*       Copyright (C) 1996  Daniel Sleator and David Temperley            */
 /*  See file "README" for information about commercial use of this system  */
 /*                                                                         */
 /***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "analyze.h"

/* The purpuse of the code in this file is to take a metered chord list
   (as computed by build_metered_chord_representation()), and label each
   chord of it with a root, which is a member of a TPC. 

   The labeling is chosen so as to maximize the sum of four components:

     Compatibility of the proposed root label with the notes in it.
     variance penalty of the sequence of root labels.
     strong beat penalty which occurs when a root begins
     ornamental dissonance penaty.
 */

/* This function computes the compatibility score of a note (in terms of
   its TPC) and with respect to a particular root.  This is for 1 second
   of duration. */


/*

variance: half life of 2 seconds....

variance penalty for each "chord span".
Its distance from its center of gravity times
its duration (in seconds) times 3.

strong beat penalty.  Penalizes chord changes that are on weak beats.
(For example, rapid chord changes are penalized.)  It's

      MAX( (2/S) - 1.5 , 0)

where S is the length of the highest level beat of the beginning of this
chord.  (It's zero if there's no change between this and the previous
chord.)

Ornamental dissonance penalty:
   It is applied when a note has a compatibility of 0 with its root.
   Take the time in seconds between a note's onset and the onset of the
   next note whose pitch differs by -2, -1, 1, or 2 from the current note.
   Let that time be D.  The penalty is 2 + 3D + D^2

   If (in subsequent processing) a note is split into several notes or
   chords, the ornamental dissonance penalty is only applied to the
   FIRST such note.  This note-wise hypothetical penalty should be
   computed for all of the notes once at the beginning.
*/

int next_prime_up(int start) {
/* return the next prime up from start */
  int i;
  start = start | 1; /* make it odd */
  for (;;) {
    for (i=3; (i <= (start/i)); i += 2) {
      if (start % i == 0) break;
    }
    if (start % i == 0) {
      start += 2;
    } else {
      return start;
    }
  }
}

/* int table_size; */
static int hash_prime_1;
static int hash_prime_2;
static int hash_prime_3;

static long int total_hash_cost;
static long int N_hash_lookups;

void initialize_hashing(void) {
  table_size = next_prime_up(4000);
  hash_prime_1 = next_prime_up(100);
  hash_prime_2 = next_prime_up(200);
  hash_prime_3 = next_prime_up(300);

  total_hash_cost = 0;
  N_hash_lookups = 0;
}

int hash(int int_tpc_cog, int int_har_cog, TPC root) {
  /* given these parameters, this figures out which bucket it goes in */
  int x;
  x = hash_prime_1 * root + hash_prime_2 * int_tpc_cog + hash_prime_3 * int_har_cog;
  return (abs(x) % table_size);
}

void free_hash_table(Bucket ** table) {
  int i;
  Bucket * pn, * xpn;
  
  for (i = 0; i<table_size; i++) {
    for (pn = table[i]; pn != NULL; pn = xpn) {
      xpn = pn->next;
      free(pn);
    }
  }
  free(table);
}

Bucket ** create_hash_table(void) {
  int i;
  Bucket ** table;
  table = (Bucket **) malloc(table_size * sizeof (Bucket *));
  if (table == NULL) {
    printf("Could not allocate a table of size %d\n", table_size);
    exit(1);
  }
  for (i = 0; i<table_size; i++) {
    table[i] = NULL;
  }
  return table;
}

Bucket * lookup_in_table(int int_tpc_cog, int int_har_cog, TPC root, Bucket ** table) {
  int h;
  Bucket * pn;

  N_hash_lookups++;
  h = hash(int_tpc_cog, int_har_cog, root);
  for (pn = table[h]; pn != NULL; pn = pn->next) {
    if (pn->int_tpc_cog == int_tpc_cog && pn->int_har_cog == int_har_cog && pn->root == root) return pn;
    total_hash_cost++;
  }
  return NULL;
}

Bucket * insert_into_table(int int_tpc_cog, int int_har_cog, TPC root, Bucket ** table) {
  int h;
  Bucket * pn;
  h = hash(int_tpc_cog, int_har_cog, root);
  pn = (Bucket *) malloc(sizeof(Bucket));
  if (pn == NULL) {
    printf("malloc failed while allocating a bucket\n");
    exit(1);    
  }

  pn->next = table[h];
  table[h] = pn;
  pn->int_har_cog = int_har_cog;
  pn->int_tpc_cog = int_tpc_cog;
  pn->root = root;
  pn->score = 0.0; 
  return pn;
}

Bucket * put_in_table(int int_tpc_cog, int int_har_cog, TPC root, Bucket ** table) {
  /* if there's an appropriate bucket, return it, otherwise insert it */
  /* not super efficient cause it computes the hash function twice */
  Bucket *b;
  b = lookup_in_table(int_tpc_cog, int_har_cog, root, table);
  if (b != NULL) return b;
  return insert_into_table(int_tpc_cog, int_har_cog, root, table);
}


void cleanup_harmonic (void) {
  int i;
  for (i=0; i<N_chords; i++) {
    free_hash_table(column_table[i].table);
  }
  free(column_table);
}

double ornamental_dissonance_penalty(double delta) {
  /* this must be an increasing function of delta (for positive delta)  */
  /* or the code won't find the minimum one */
  return 2 + 3*delta + delta*delta;
}

#define DEFAULT_TIME 10.0
  /* the amount of time to give an ornamental dissonance penalty
     when there is no resolving note */

void label_notes_with_ornamental_dissonance_penalties(Note * nl) {
  Note * note, *f_note;
  double delta_t;
  if (verbosity >= 4) printf("Ornamental dissonance penalties:\n");
  for (note = nl; note != NULL; note = note->next) {
    note->orn_dis_penalty = ornamental_dissonance_penalty(DEFAULT_TIME);
    delta_t = DEFAULT_TIME;
    for (f_note = note->next; f_note != NULL; f_note = f_note->next) {
           /* use this value only if you don't find any real note to define the penalty */
      if (note->start == f_note->start) continue;  /* skip all notes that start when I do */
      if (note->pitch == f_note->pitch + 1  ||  note->pitch == f_note->pitch - 1  ||
	  note->pitch == f_note->pitch + 2  ||  note->pitch == f_note->pitch - 2) {
	delta_t = (f_note->start - note->start)/(1000.0);  /* time in seconds */
	note->orn_dis_penalty = ornamental_dissonance_penalty(delta_t);
	break;  /* make it find the nearest note with dissonance */
      }
    }
    if (verbosity >= 4) {
      printf("pitch = %2d  start = %4d  duration = %4d  delta_t = %6.3f  od = %6.3f\n",
	     note->pitch, note->start, note->duration, delta_t, note->orn_dis_penalty);
    }
  }
}

double compatibility(TPC root, TPC note) {
  /* if the abs of this is > .01 it's considered to be non-zero
     from the perspective of computing ornamental dissonance */
  switch (note - root) {
  case  0: return 5.0;
  case  1: return 3.0;
  case  4: return 2.0;
  case -2: return -2.0;
  case -3: return 1.0;
  default: return 0.0;
  }
}

double note_ornamental_dissonance_penalty(TPC root, TPC tpc, Chord * chord, Note * note, int same_roots) {
  /* same_roots is 1 if the root at this point and the previous point are the same */
  if (same_roots && !chord->is_first_chord) return 0.0;
  if (same_roots && !note->is_first_note) return 0.0;
  if (fabs(compatibility(root, tpc)) > .01) return 0.0;  /* used to be compatibility != 0 */
  else return note->orn_dis_penalty;
}

double tpc_variance(double cog, TPC tpc, double my_mass, double decayed_prior_note_mass) {
  double delta_cog;
  delta_cog = fabs(cog - (double) tpc);
  return (delta_cog * delta_cog * my_mass * decayed_prior_note_mass);
}

void tpc_choice_score(TPC root, int same_roots, Chord *ch, double my_mass, double decayed_prior_note_mass, double tpc_cog) {
  /* tpc_cog is the PRIOR center of gravity. */
    
  int i, oc, nnotes;
  Note *note;
  TPC tpc;
  double score, compat, best_compatibility, orn_diss_penalty, best_orn_diss_penalty, best_score, best_variance, variance;
  double average_tpc;

  for (nnotes=0, note=ch->note; note != NULL; note = note->next) nnotes++;

  side_effect.compatibility = 0.0;
  side_effect.orn_diss_penalty = 0.0;
  side_effect.tpc_variance = 0.0;

  /* strong beat penalty */
  if (same_roots) {
    side_effect.strong_beat_penalty = 0.0;
  } else {
    side_effect.strong_beat_penalty = (2000.0/ch->level_time) - 1.5;
    if (side_effect.strong_beat_penalty < 0.0) side_effect.strong_beat_penalty = 0.0;
  }

  for (i=0, note=ch->note; note != NULL; note = note->next, i++) {
    best_score = best_compatibility = best_orn_diss_penalty = best_variance = 0.0; /* only needed to avoid compiler warnings */
    for (oc = OCT_BELOW; oc <= OCT_ABOVE; oc++) {
      tpc = 12 * oc + note->base_tpc;

      /* compatibility */
      compat = compatibility(root, tpc) * my_mass;
      
      /* orn dis penalty */
      orn_diss_penalty = note_ornamental_dissonance_penalty(root, tpc, ch, note, same_roots);

      /* tpc variance */
      variance = tpc_variance(tpc_cog, tpc, my_mass, decayed_prior_note_mass);

      score = compat - orn_diss_penalty - tpc_variance_scale_factor * variance;

      if (oc == OCT_BELOW || score > best_score) {
	best_score = score;
	side_effect.tpc_choice[i] = tpc;
	best_compatibility = compat;
	best_orn_diss_penalty = orn_diss_penalty;
	best_variance = variance;
      }
    }
    side_effect.compatibility += best_compatibility;
    side_effect.orn_diss_penalty += best_orn_diss_penalty;
    side_effect.tpc_variance += best_variance;
  }
  /* now compute the tpc_cog, and put it in side_effect.tpc_cog */

  average_tpc = 0;
  for (i=0; i<nnotes; i++) {
    average_tpc += (double)(side_effect.tpc_choice[i])/nnotes;
  }
  side_effect.tpc_cog =
    (tpc_cog * decayed_prior_note_mass + average_tpc * nnotes * my_mass)/(nnotes * my_mass + decayed_prior_note_mass);
}

void prune_table(Bucket ** table, int column) {
  int h;
  Bucket * bu, *xbu, *best, *nbu;
  int count=0, badcount=0;

  best = NULL;
  for (h=0; h<table_size; h++) {
    for (bu = table[h]; bu != NULL; bu = bu->next) {
      if(best == NULL || best->score < bu->score) best = bu;
      count++;
    }
  }

  for (h=0; h<table_size; h++) {
    nbu = NULL;
    for (bu = table[h]; bu != NULL; bu = xbu) {
      xbu = bu->next;

      if (bu->score < best->score - pruning_cutoff) {
	badcount++;
	free(bu);
      } else {
	bu->next = nbu;
	nbu = bu;
      }
    }
    table[h] = nbu;
  }
  if (verbosity >= 1) {
    printf("Finished unit %3d.  (keeping %5d options out of %5d)\n", column, count-badcount, count);
  }
}

void initialize_harmonic(Chord *nl) {
  Chord * xnl;
  Note * note;
  double delta_t, decay;
  int i, n;
  for (N_chords = 0, xnl = nl; xnl != NULL; xnl = xnl->next) N_chords++;  /* compute N_chords */

  column_table = (Column*) malloc (N_chords * sizeof(Column));
  if (column_table == NULL) {
    fprintf(stderr, "Malloc failed\n");
    exit(1);
  }
  for (i = 0, xnl = nl; xnl != NULL; xnl = xnl->next, i++) {
    for (n=0, note=xnl->note; note != NULL; note = note->next) n++; /* count number of notes */
    
    column_table[i].chord = xnl;
    column_table[i].table = create_hash_table();

    column_table[i].my_mass = ((double) xnl->duration)/(1000.0);

    /* this part computes the chord masses */
    if (i == 0) {
      column_table[i].chord_mass = column_table[i].my_mass;
      column_table[i].note_mass = n*column_table[i].my_mass;
      column_table[i].decayed_prior_note_mass = 0.0;
      column_table[i].decayed_prior_chord_mass = 0.0;
    } else {
      delta_t = (double) xnl->start - column_table[i-1].chord->start;
      decay = exp(-alpha * delta_t);
      column_table[i].chord_mass = decay * column_table[i-1].chord_mass + column_table[i].my_mass;
      column_table[i].note_mass = decay * column_table[i-1].note_mass + n*column_table[i].my_mass;
      column_table[i].decayed_prior_note_mass = column_table[i-1].note_mass * decay;
      column_table[i].decayed_prior_chord_mass = column_table[i-1].chord_mass * decay;
    }
    if (verbosity >= 4) {
      printf("chord_mass= %6.3f  note_mass= %6.3f  dpnm= %6.3f  dpcm= %6.3f\n", 
	     column_table[i].chord_mass,
	     column_table[i].note_mass,
	     column_table[i].decayed_prior_note_mass,
	     column_table[i].decayed_prior_chord_mass);
    }
  }
}

int discrete_cog(double cog) {
  return (int) (cog * buckets_per_unit_of_cog);
}

void initialize_first_harmonic_column(void) {
  int b;
  TPC root;
  int tpc_prime;
  double cog;
  Bucket *buck;

  for (root = -4; root <= 7; root++) {
    for (tpc_prime = root-5; tpc_prime <= root+6; tpc_prime++) {
      cog = (double) root;
      b = discrete_cog(cog);

      tpc_choice_score(root, 0, column_table[0].chord, column_table[0].my_mass, 1.0, (double)tpc_prime);
       /* we prime the initial column with an artifical decayed_prior_mass of 1.0, and
	  an artificial tpc_cog of tpc_prime. */

      buck = insert_into_table(discrete_cog(side_effect.tpc_cog), b, root, column_table[0].table);
      buck->tpc_prime = tpc_prime;
      buck->har_cog = cog;

      buck->tpc_cog = side_effect.tpc_cog;
      buck->har_variance = 0.0;
      buck->tpc_variance = side_effect.tpc_variance;
      buck->score = side_effect.compatibility - side_effect.orn_diss_penalty;
      buck->prev_bucket = NULL;
    }
  }
}

void compute_harmonic_table(void) {
  int column;
  int my_root;
  double har_variance, score;
  double current_cog;
  double new_cog;
  double delta_cog;
  Bucket * bu, *bu1;
  int int_har_cog, int_tpc_cog, new_guy, h;

  initialize_first_harmonic_column();
  for (column = 1; column<N_chords; column++) {
    /* compute this column of the table */
    for (h=0; h<table_size; h++) {
      for (bu = column_table[column-1].table[h]; bu != NULL; bu = bu->next) {
	for (my_root = floor(bu->har_cog-12.0); my_root <= ceil(bu->har_cog+12.0); my_root++) {
	  current_cog = (double) my_root;

	  /* compute variance */
	  delta_cog = (current_cog - bu->har_cog);
	  /*	  har_variance = delta_cog * column_table[column].my_mass * column_table[column-1].chord_mass; */
	  har_variance = fabs(delta_cog * column_table[column].my_mass * 3.0);  /* replace total mass by 3.0 */
	  
	  /* the following computes the compatibility, strong_beat_penalty, and ornamental dissonance penalty */
	  tpc_choice_score(my_root, bu->root == my_root, column_table[column].chord,
			   column_table[column].my_mass, column_table[column].decayed_prior_note_mass, bu->tpc_cog);
	  
	  score = bu->score
	    + side_effect.compatibility
	    - side_effect.orn_diss_penalty
	    - har_variance
	    - tpc_variance_scale_factor * side_effect.tpc_variance
	    - side_effect.strong_beat_penalty;

	  new_cog = (bu->har_cog * column_table[column].decayed_prior_chord_mass + current_cog * column_table[column].my_mass)
	    /column_table[column].chord_mass;

	  int_tpc_cog = discrete_cog(side_effect.tpc_cog);
	  int_har_cog = discrete_cog(new_cog);
	  bu1 = lookup_in_table(int_tpc_cog, int_har_cog, my_root, column_table[column].table);
	  new_guy = (bu1 == NULL);
	  if (bu1 == NULL) bu1 = insert_into_table(int_tpc_cog, int_har_cog, my_root, column_table[column].table);
	  
	  /* now we look in that bucket and update what's there if this is a better solution */
	  if (new_guy || score > bu1->score) {
	    bu1->score = score;
	    bu1->har_variance = har_variance;
	    bu1->tpc_variance = side_effect.tpc_variance;
	    bu1->root = my_root;
	    bu1->prev_bucket = bu;
	    bu1->har_cog = new_cog;
	    bu1->tpc_cog = side_effect.tpc_cog;
	  }
	}
      }
    }
    prune_table(column_table[column].table, column);
  }
}
  
void print_harmonic(void) {
  Note * note;
  Chord *chord;
  int i, j, h;
  Bucket ** bucket_choice;
  Bucket * best_b, *bb, *bu;

  bucket_choice = (Bucket **) malloc(N_chords * sizeof (Bucket *));

  best_b = NULL;

  for (h=0; h<table_size; h++) {
    for (bu = column_table[N_chords-1].table[h]; bu != NULL; bu = bu->next) {
      if(best_b == NULL || best_b->score < bu->score) best_b = bu;
    }
  }

  if (best_b == NULL) {
    fprintf(stderr, "No bucket used\n");
    exit(1);
  }

  for (i = N_chords-1; i >= 0; i--) {
    bucket_choice[i] = best_b;
    best_b = best_b->prev_bucket;
  }

  printf("Harmonic analysis:\n");

  for (i=0; i<N_chords; i++) {
    chord = column_table[i].chord;
    bb = bucket_choice[i];
    printf(" ");
    for (j=0; j<N_beatlevel; j++) if (chord->level >= j) printf("x "); else printf("  ");

    if (i == 0) {
      tpc_choice_score(bb->root, 0, chord,
		       column_table[i].my_mass, 1.0, (double) bb->tpc_prime);
    } else {
      tpc_choice_score(bb->root, bucket_choice[i-1]->root == bb->root, chord,
		       column_table[i].my_mass, column_table[i].decayed_prior_note_mass, bucket_choice[i-1]->tpc_cog);
    }

    printf("start = %5d  duration = %5d  notes: {", chord->start, chord->duration);
    for (j=0, note = chord->note; note != NULL; note = note->next, j++) {
      printf("(%2d,%2d)", note->pitch, side_effect.tpc_choice[j]);
    }
    printf("}\n");
    printf("    root=%d(%-3s) com=%6.3f  har_cog=%6.3f  har_var=%6.3f  sbp=%6.3f  odp=%6.3f  tpc_cog=%6.3f  tpc_var=%6.3f\n",
	   bb->root, tpc_string(bb->root),
	   side_effect.compatibility, bb->har_cog, bb->har_variance,
	   side_effect.strong_beat_penalty, side_effect.orn_diss_penalty, bb->tpc_cog, bb->tpc_variance);

  }
  if (verbosity >= 3)  {
    printf("Scores chord by chord:\n");
    for (i=0; i<N_chords; i++) {
      chord = column_table[i].chord;
      bb = bucket_choice[i];
      printf("  chord = %3d  Score = %50.30f\n", i, bb->score);
    }
  }

  free(bucket_choice);
}
