/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 * Name: MusicID-File AlbumID sample appilcation
 * Description:
 * AlbumID processing provides an advanced method of media file recognition. The context of the media files
 * (their folder location and similarity with other media files) are used to achieve more accurate media recognition.
 * This method is best used for groups of media files where the grouping of the results matters as much as obtaining
 * accurate, individual results. The GnMusicIDFile::DoAlbumID method provides AlbumID processing.
 *
 * Command-line Syntax:
 * sample clientId clientIdTag license libPath
 */

#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicidfile.hpp"
#include "gnsdk_storage_sqlite.hpp"
#include "gnsdk_lookup_local.hpp"

#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::musicid_file;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Files used by this application */
#define MYAPP_SAMPLE_FILE_1     "../../../sample_data/01_stone_roses.wav"
#define MYAPP_SAMPLE_FILE_2     "../../../sample_data/04_stone_roses.wav"
#define MYAPP_SAMPLE_FILE_3     "../../../sample_data/stone roses live.wav"
#define MYAPP_SAMPLE_FILE_4     "../../../sample_data/Dock Boggs - Sugar Baby - 01.wav"
#define MYAPP_SAMPLE_FILE_5     "../../../sample_data/kardinal_offishall_01_3s.wav"
#define MYAPP_SAMPLE_FILE_6     "../../../sample_data/Kardinal Offishall - Quest For Fire - 15 - Go Ahead Den.wav"

/*
 * Locale function prototypes
 */
void display_result( GnResponseAlbums response );


/*
 * Callback delegate classes
 */

/* Callback delegate called when performing MusicID-File operation */
class MusicIDFileEvents : public GnMusicIDFileEvents
{
	/*-----------------------------------------------------------------------------
	 *  result_available
	 */
	void
	result_available(GnResponseAlbums& album_result, gnsdk_uint32_t current_album, gnsdk_uint32_t total_albums)
	{
		std::cout<<"\n*Album " << current_album << " of " << total_albums << "*\n"<< std::endl;


		display_result(album_result);
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check( )
	{
		return GNSDK_FALSE;
	}

	/*-----------------------------------------------------------------------------
	 *  get_fingerprint
	 */
	void
	get_fingerprint( GnMusicIDFileInfo& fileInfo, gnsdk_uint32_t currentFile, gnsdk_uint32_t totalFiles )
	{
		char         pcmAudio[2048] = {0};
		gnsdk_bool_t complete       = GNSDK_FALSE;
		gnsdk_cstr_t file;


		(void)currentFile;
		(void)totalFiles;

		try
		{
			file = fileInfo.GetFileName();

			std::ifstream audioFile (file, std::ios::in | std::ios::binary);
			if ( audioFile.is_open() )
			{
				/* skip the wave header (first 44 bytes). the format of the sample files is known,
				 * but please be aware that many wav file headers are larger then 44 bytes!
				 */
				audioFile.seekg(44);
				if ( audioFile.good() )
				{
					/* initialize the fingerprinter
					 * Note: The sample files are non-standard 11025 Hz 16-bit mono to save on file size
					 */
					fileInfo.FingerprintBegin(11025, 16, 1);

					do
					{
						audioFile.read(pcmAudio, 2048);
						complete = fileInfo.FingerprintWrite(
						    (gnsdk_byte_t*)pcmAudio,
						    audioFile.gcount()
						    );

						/* does the fingerprinter have enough audio? */
						if (GNSDK_TRUE == complete)
						{
							break;
						}
					}
					while ( audioFile.good() );

					if (GNSDK_TRUE != complete)
					{
						/* Fingerprinter doesn't have enough data to generate a fingerprint.
						    Note that the sample data does include one track that is too short to fingerprint. */
						std::cout << "Warning: input file does contain enough data to generate a fingerprint:\n" << file <<"\n";
						fileInfo.FingerprintEnd();
					}
				}
				else
				{
					std::cout << "\n\nError: Failed to skip wav file header: " << file <<"\n\n";
				}
			}
			else
			{
				std::cout << "\n\nError: Failed to open input file: " << file << "\n\n";
			}
		}
		catch ( GnError e )
		{
			std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
		}
	}

	/*-----------------------------------------------------------------------------
	 *  get_metadata
	 */
	void
	get_metadata( GnMusicIDFileInfo& fileInfo, gnsdk_uint32_t currentFile, gnsdk_uint32_t totalFiles )
	{
		(void)currentFile;
		(void)totalFiles;

		/*
		 * A typical use for this callback is to read file tags (ID3, etc) for the basic
		 * metadata of the track.  To keep the sample code simple, we went with .wav files
		 * and hardcoded in metadata for just one of the sample tracks.  (MYAPP_SAMPLE_FILE_5)
		 */

		/* So, if this isn't the correct sample track, return.*/
		gnsdk_cstr_t identifier = fileInfo.GetIdentifier();

		if (0 != strcmp(MYAPP_SAMPLE_FILE_5, identifier))
		{
			return;
		}

		fileInfo.SetAlbumArtist( "kardinal offishall" );
		fileInfo.SetAlbumTitle ( "quest for fire" );
		fileInfo.SetTrackTitle ( "intro" );
	}

};


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}

};


/*
 * Local function declarations
 */

void
set_query_data( GnMusicIDFile& midf, gnsdk_cstr_t filePath )
{
	GnMusicIDFileInfo fileinfo;

	fileinfo = midf.CreateFileInfo(filePath);


	/* Set data for this file information instance.
	 * This only sets file path but all available data
	 * should be set, such as artist name, track title
	 * and album tile if available, such as via audio
	 * file tags.
	 */
	fileinfo.SetFileName( filePath );
}


void do_musicid_file(GnUser& user, GnMidfQueryFlags queryFlags)
{
	MusicIDFileEvents midFileEvents;
	GnMusicIDFile     midf(user, &midFileEvents);

	set_query_data(midf, MYAPP_SAMPLE_FILE_1);
	set_query_data(midf, MYAPP_SAMPLE_FILE_2);
	set_query_data(midf, MYAPP_SAMPLE_FILE_3);
	set_query_data(midf, MYAPP_SAMPLE_FILE_4);
	set_query_data(midf, MYAPP_SAMPLE_FILE_5);
	set_query_data(midf, MYAPP_SAMPLE_FILE_6);

	midf.DoAlbumID(queryFlags);
}


/*-----------------------------------------------------------------------------
 *  display_result
 */
void
display_result( GnResponseAlbums response )
{
	printf("\tAlbum count: %d\n", response.Albums().count());

	int                      matchCounter = 0;
	metadata::album_iterator it_album     = response.Albums().begin();

	for (; it_album != response.Albums().end(); ++it_album)
	{
		printf("\tMatch %d - Album title:\t\t%s\n", ++matchCounter, it_album->Title().Display());
	}
}


#if USE_LOCAL
/*-----------------------------------------------------------------------------
 *  display_embedded_db_info
 */
static void
display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  versionResult = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);


	std::cout << "GracenoteDB Source DB ID :\t: " << versionResult<< std::endl;
}


#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	LookupStatusEvents localeEvents;

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, &localeEvents );

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main( int argc, char* argv[] )
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		std::cout << "\nUsage:" << argv[0] << " clientId clientIdTag license gnsdkLibraryPath\n"<< std::endl;
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n",  gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",
		    GNSDK_LOG_PKG_ALL,
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,
		    GNSDK_LOG_OPTION_ALL,
		    0,                                                          /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                                 /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		GnLookupLocal gnLookupLocal;

		user->OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		std::cout << "\n-------AlbumID with 'RETURN_SINGLE' option:-------"<< std::endl;
		do_musicid_file(user, GnMidfQueryReturnSingle | GnMidfQueryResponseAlbums);

		std::cout << "\n-------AlbumID with 'RETURN_ALL' option:-------"<< std::endl;
		do_musicid_file(user, GnMidfQueryReturnAll | GnMidfQueryResponseAlbums);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

	return 0;
}

