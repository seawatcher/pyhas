/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace GracenoteSDK {

using System;
using System.Runtime.InteropServices;

public class GnMoodgridPresentationDataEnumerable : System.Collections.Generic.IEnumerable<GnMoodgridDataPoint> {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal GnMoodgridPresentationDataEnumerable(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(GnMoodgridPresentationDataEnumerable obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GnMoodgridPresentationDataEnumerable() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          gnsdk_csharp_marshalPINVOKE.delete_GnMoodgridPresentationDataEnumerable(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

	System.Collections.Generic.IEnumerator<GnMoodgridDataPoint> System.Collections.Generic.IEnumerable<GnMoodgridDataPoint> .GetEnumerator( )
	{
		return begin();
	}
	System.Collections.IEnumerator System.Collections.IEnumerable.
		GetEnumerator( )
	{
		return begin();
	}


  public GnMoodgridPresentationDataEnumerable(GnMoodgridPresentationDataEnumerator start, GnMoodgridPresentationDataEnumerator end) : this(gnsdk_csharp_marshalPINVOKE.new_GnMoodgridPresentationDataEnumerable(GnMoodgridPresentationDataEnumerator.getCPtr(start), GnMoodgridPresentationDataEnumerator.getCPtr(end)), true) {
    if (gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Pending) throw gnsdk_csharp_marshalPINVOKE.SWIGPendingException.Retrieve();
  }

  public GnMoodgridPresentationDataEnumerator begin() {
    GnMoodgridPresentationDataEnumerator ret = new GnMoodgridPresentationDataEnumerator(gnsdk_csharp_marshalPINVOKE.GnMoodgridPresentationDataEnumerable_begin(swigCPtr), true);
    return ret;
  }

  public GnMoodgridPresentationDataEnumerator end() {
    GnMoodgridPresentationDataEnumerator ret = new GnMoodgridPresentationDataEnumerator(gnsdk_csharp_marshalPINVOKE.GnMoodgridPresentationDataEnumerable_end(swigCPtr), true);
    return ret;
  }

  public uint count() {
    uint ret = gnsdk_csharp_marshalPINVOKE.GnMoodgridPresentationDataEnumerable_count(swigCPtr);
    return ret;
  }

  public GnMoodgridPresentationDataEnumerator at(uint index) {
    GnMoodgridPresentationDataEnumerator ret = new GnMoodgridPresentationDataEnumerator(gnsdk_csharp_marshalPINVOKE.GnMoodgridPresentationDataEnumerable_at(swigCPtr, index), true);
    return ret;
  }

  public GnMoodgridPresentationDataEnumerator getByOrdinal(uint index) {
    GnMoodgridPresentationDataEnumerator ret = new GnMoodgridPresentationDataEnumerator(gnsdk_csharp_marshalPINVOKE.GnMoodgridPresentationDataEnumerable_getByOrdinal(swigCPtr, index), true);
    return ret;
  }

}

}
