/*
 *  GRACENOTE, INC. PROPRIETARY INFORMATION
 *  This software is supplied under the terms of a license agreement or
 *  nondisclosure agreement with Gracenote, Inc. and may not be copied
 *  or disclosed except in accordance with the terms of that agreement.
 *  Copyright(c) 2000-2013. Gracenote, Inc. All Rights Reserved.
 *
 */


/*
 *  Name: VideoSearchFilters.cs
 *  Description:
 *  This sample shows basic use of the FindWorks() call
 *
 *  Command-line Syntax:
 *   sample clientId clientIdTag license libPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;
namespace Sample
{
    public class VideoSearchFilters
    {
        private static string licensePath = null;
        private static string clientID = null;
        private static string clientTag = null;
        private static string libPath = null;
        private static string applicationVersion = "1.0.0.0";
		private static GnUser user = null;

        #region Initializegnsdk

        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks  */
        public class VideoSearchEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t video_status, uint percent_complete, uint bytes_total_sent, uint bytes_total_received)
            {
                Console.Write("\nPerforming Video Find Product Query ...\t");
                switch (video_status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write("Status unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write("Status query begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write("Status  connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write("Status sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write("Status receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write("Status complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percent_complete + "),\tTotal Bytes Sent (" + bytes_total_sent + "),\tTotal Bytes Received (" + bytes_total_received + ")");
            }
            public override bool cancel_check()
            {
                return false;
            }
        }

        /* Load existing user, or register new one.
         *
         * GNSDK requires a user instance to perform queries. 
         * User encapsulates your Gracenote provided Client ID which is unique for your
         * application. Users  are registered once with Gracenote then must be saved by
         * your application and reused on future invocations.
         */
        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

        /*
         *  Load locale 
         *  Deserialize existing locale, or register new one.
         *  Load locale to retrive Gracenote Descriptor/List basesd values e.g, genre, era, origin, mood, tempo
         */
        static void LoadLocale(GnUser user)
        {
            using (VideoSearchEvents localeEvents = new VideoSearchEvents())
            {

                if (!File.Exists("serialized_video_locale.txt"))
                {
                    GnLocale locale = new GnLocale(
                                                    GnSDK.kLocaleGroupVideo,        /* Locale group */
                                                    GnSDK.kLanguageEnglish,         /* Languae */
                                                    GnSDK.kRegionDefault,           /* Region */
                                                    GnSDK.kDescriptorSimplified,    /* Descriptor */
                                                    user,                           /* User */
                                                    null /*localeEvents*/           /* locale Events object */
                                                    );

                    /* Serialize locale, so we can use reuse it */
                    if (!File.Exists("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = (locale.Serialize().ToString());
                        using (StreamWriter outfile = new StreamWriter("serialized_video_locale.txt"))
                        {
                            outfile.Write(serializedLocaleString);
                            outfile.Close();
                        }
                    }

                    return;
                }
                else
                {
                    using (StreamReader sr = new StreamReader("serialized_video_locale.txt"))
                    {
                        String serializedLocaleString = sr.ReadToEnd();

                        GnLocale locale = new GnLocale(serializedLocaleString, null /*localeEvents*/);
                        return;
                    }

                }
            }
        }

        #endregion

        private static void DoVideoSearchFilters(GnUser user, string text)
        {
            //string text = "Harrison Ford";

            using (GnStatusEventsDelegate videoEvents = new VideoSearchEvents())
            {
                /* Create video query */
                GnVideo video = new GnVideo(user, null /*videoEvents*/);
                Console.WriteLine("\n*****Sample Video Work Search: '" + text + "'*****");

                /* load video genre list from service ( online ) */
                GnList list = new GnList(GnSDK.kListTypeGenreVideos, GnSDK.kLanguageEnglish, GnSDK.kRegionDefault, GnSDK.kDescriptorSimplified, user);

                /* Set filter - no comedies dammit! */
                GnListElement listElement = list.ElementByString("Comedy");
                video.FilterByListElement('0', GnVideoListElementFilterType.kListElementFilterGenre, listElement);

                /* Set the input text and perform the search */
                GnResponseVideoWork responseVideoWork = video.FindWorks(text, GnVideoSearchField.kSearchFieldContributorName, GnVideoSearchType.kSearchTypeDefault);
                Console.WriteLine("\n\nNumber matches: " + responseVideoWork.Works.count());

                if (responseVideoWork.Works.count() > 0)
                {
                    /***********************************************************************************
	                 *
	                 *  NEEDS DECISION (MATCH RESOLUTION) CHECK
	                 *
	                 ************************************************************************************/
                    if (responseVideoWork.NeedsDecision)
                    {
                        /**********************************************
				         * Resolve match here
				         **********************************************/
                    }
                    /* Handle the results */
                    GnVideoWorkEnumerable videoWorkEnumerable = responseVideoWork.Works;
                    foreach (GnVideoWork work in videoWorkEnumerable)
                    {
                        GnVideoWork gnVideoWork = work;
                        if (!work.FullResult)
                        {
                            GnResponseVideoWork responseWork = video.FindWorks(work);
                            gnVideoWork = responseWork.Works.at(0).next();
                        }
                        /* Display title  */
                        GnTitle workTitle = gnVideoWork.OfficialTitle;
                        Console.WriteLine("\nTitle: " + workTitle.Display);

                        /* Primary genre */
                        GnGenre gnGenrae = gnVideoWork.Genre;
                        Console.WriteLine("Genre: " + gnGenrae.Level1);
                    }
                }
            }

        }

        private static void enableLogging(GnSDK gnsdk)
        {
            gnsdk.LoggingEnable(
                                 "sample.log",                                               /* Log file path */
                                 GnSDK.GN_LOG_PKG_ALL,                                       /* Include entries for all packages and subsystems */
                                 GnSDK.GN_LOG_LEVEL_ERROR | GnSDK.GN_LOG_LEVEL_WARNING,      /* Include only error and warning entries */
                                 GnSDK.GN_LOG_OPTION_ALL,                                    /* All logging options: timestamps, thread IDs, etc */
                                 0,                                                          /* Max size of log: 0 means a new log file will be created each run */
                                 false                                                       /* true = old logs will be renamed and saved */
                                 );
        }

        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientID = args[0].Trim();
                    clientTag = args[1].Trim();
                    licensePath = args[2].Trim();
                    libPath = args[3].Trim();

                    try
                    {
                        /* Initialize the GNSDK Manager */
                        GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);

                        // Dispaly SDK version
                        Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                        /* Enable logging */
                        enableLogging(gnsdk);

                        /* Create user */
                        user = GetUser(gnsdk, clientID, clientTag, applicationVersion);

                        /* Load locale to retrieve Gracenote Descriptor/List based values e.g, genre, era, origin, mood, tempo */
                        LoadLocale(user);

                        /* Do Video search Filter */
                        DoVideoSearchFilters(user, "Harrison Ford");
                    }
                    catch (GnException e)
                    {
                        Console.WriteLine("Error API            :: " + e.ErrorAPI);
                        Console.WriteLine("Error Description    :: " + e.ErrorDescription);
                        Console.WriteLine("Error Code           :: " + e.ErrorCode);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
            }
        }
    
    }
}
