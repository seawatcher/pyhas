var classgracenote_1_1metadata_1_1_gn_video_credit =
[
    [ "GnVideoCredit", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a27f942b6ca5017f7e568602f05be5d26", null ],
    [ "GnVideoCredit", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a2f1822cf2eca6c88f8f3dc523979a1bb", null ],
    [ "~GnVideoCredit", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a0d95796664c20bc0615a794257ab14c3", null ],
    [ "ArtistType", "classgracenote_1_1metadata_1_1_gn_video_credit.html#acb8fc9544b89a8e8f945b0da34edafa1", null ],
    [ "CharacterName", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a8d477fa6c0113aac13fabb2462da593a", null ],
    [ "Contributor", "classgracenote_1_1metadata_1_1_gn_video_credit.html#adf9e5199b1ec3f7f8996afa946f2882d", null ],
    [ "Era", "classgracenote_1_1metadata_1_1_gn_video_credit.html#aca8b5bb209208e8066732059ed1c705b", null ],
    [ "Genre", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a33da2035c030d49dc01e8b563dfee880", null ],
    [ "OfficialName", "classgracenote_1_1metadata_1_1_gn_video_credit.html#acf52ac5a54e1ea2bdd94c3404a86f752", null ],
    [ "Origin", "classgracenote_1_1metadata_1_1_gn_video_credit.html#ae66cfdf7e53c552922e1b2e4b600db5d", null ],
    [ "Rank", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a96cfc5848f3f8757e839f680f622a3ea", null ],
    [ "Role", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a1d5fd72898bd7c9f87bc7f5fa8d07fef", null ],
    [ "RoleBilling", "classgracenote_1_1metadata_1_1_gn_video_credit.html#af0393b42d000721bcf861e70fcfec6fe", null ],
    [ "RoleID", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a9735588e192802110d41328bdeb1686b", null ],
    [ "Seasons", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a71e17965c13a4f3bf5ef655d15d7d86f", null ],
    [ "Series", "classgracenote_1_1metadata_1_1_gn_video_credit.html#adc669917ca5840e626bd06f2f59be93b", null ],
    [ "Works", "classgracenote_1_1metadata_1_1_gn_video_credit.html#a2548795996261f3c292339416f3c251a", null ]
];