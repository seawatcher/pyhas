/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _METADATA_MUSIC_HPP_
#define _METADATA_MUSIC_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk_iterator_base.hpp"
#include "metadata.hpp"

namespace gracenote
{
	namespace metadata
	{
		/*
		 *   Forward class declaration
		 */
		class GnTrack;
		class GnCredit;
		class GnAlbum;
		class GnMatch;
		class GnDataMatch;
		typedef gn_facade_range_iterator<GnTrack, gn_gdo_provider<GnTrack> >           track_iterator;
		typedef gn_facade_range_iterator<GnCredit, gn_gdo_provider<GnCredit> >         credit_iterator;
		typedef gn_facade_range_iterator<GnMatch, gn_gdo_provider<GnMatch> >           matches_iterator;
		typedef gn_facade_range_iterator<GnAlbum, gn_gdo_provider<GnAlbum> >           album_iterator;
		typedef gn_facade_range_iterator<GnDataMatch, gn_gdo_provider<GnDataMatch> >   datamatches_iterator;

		/**
		 * Represents the genre of the parent object, such as Rock, Pop and Jazz.
		 */
		class GnGenre : public GnDataObject
		{
		public:
			explicit GnGenre() : GnDataObject(GNSDK_NULL_GDO) { }
			GnGenre(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnGenre() { }

			/**
			 * Retrieves the display string from level 1 of the associated Gracenote genre hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_GENRE_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated Gracenote genre hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_GENRE_LEVEL2);
			}

			/**
			 * Retrieves the display string from level 3 of the associated Gracenote genre hierarchy
			 * @return Display string
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level3() const
			{
				return StringValue(GNSDK_GDO_VALUE_GENRE_LEVEL3);
			}
		};

		/**
		 * Represents the origin of the parent object
		 */
		class GnOrigin : public GnDataObject
		{
		public:
			GnOrigin() : GnDataObject(GNSDK_NULL_GDO) { }
			GnOrigin(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnOrigin() { }

			/**
			 * Retrieves the display string from level 1 of the associated Gracenote origin hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_ORIGIN_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated Gracenote origin hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_ORIGIN_LEVEL2);
			}

			/**
			 * Retrieves the display string from level 3 of the associated Gracenote origin hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level3() const
			{
				return StringValue(GNSDK_GDO_VALUE_ORIGIN_LEVEL3);
			}

			/**
			 * Retrieves the display string from level 4 of the associated Gracenote origin hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level4() const
			{
				return StringValue(GNSDK_GDO_VALUE_ORIGIN_LEVEL4);
			}
		};

		/**
		 * Represents the era of the parent object, such as 2000's and Late 1990s.
		 */
		class GnEra : public GnDataObject
		{
		public:
			GnEra() : GnDataObject(GNSDK_NULL_GDO) { }
			GnEra(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnEra() { }

			/**
			 * Retrieves the display string from level 1 of the associated Gracenote era hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_ERA_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated Gracenote era hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_ERA_LEVEL2);
			}

			/**
			 * Retrieves the display string from level 3 of the associated Gracenote era hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level3() const
			{
				return StringValue(GNSDK_GDO_VALUE_ERA_LEVEL3);
			}
		};

		/**
		 * Represents the artist type of the parent object, such as Female, Male Duo and Female Group.
		 */
		class GnArtistType : public GnDataObject
		{
		public:
			GnArtistType() : GnDataObject(GNSDK_NULL_GDO) { }
			GnArtistType(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnArtistType() { }

			/**
			 * Retrieves the display string from level 1 of the associated Gracenote artist type hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_ARTISTTYPE_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated Gracenote artist type hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_ARTISTTYPE_LEVEL2);
			}
		};

		/**
		 * A person that participated in the creation of the Album or Track.
		 */
		class GnContributor : public GnDataObject
		{
		public:
			GnContributor() : GnDataObject(GNSDK_NULL_GDO) { }
			GnContributor(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnContributor(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_CONTRIBUTOR) { }

			virtual ~GnContributor() { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves a Gracenote identifier for this contributor.
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 * Retrieves a Gracenote identifier for this contributor.
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 * Retrieves the contributor's product identifier.
			 * <p><b>Remarks:</b></p>
			 * Available for most types, this retrieves a value which can be stored or transmitted. This
			 * value can be used as a static identifier for the current content as it will not change over time.
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 * Retrieves the Gracenote TUI for this contributor.
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 * Retrieves the Gracenote TUI Tag for this contributor.
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 * Retrieves the contributor's biography.
			 */
			gnsdk_cstr_t Biography() const
			{
				return StringValue(GNSDK_GDO_VALUE_BIOGRAPHY);
			}

			/**
			 * Retrieves the contributor's birth date.
			 */
			gnsdk_cstr_t BirthDate() const
			{
				return StringValue(GNSDK_GDO_VALUE_BIRTH_DATE);
			}

			/**
			 * Retrieves the contributor's birth place.
			 */
			gnsdk_cstr_t BirthPlace() const
			{
				return StringValue(GNSDK_GDO_VALUE_BIRTH_PLACE);
			}

			/**
			 * Retrieves the contributor's death date.
			 */
			gnsdk_cstr_t DeathDate() const
			{
				return StringValue(GNSDK_GDO_VALUE_DEATH_DATE);
			}

			/**
			 * Retrieves the contributor's death place.
			 */
			gnsdk_cstr_t DeathPlace() const
			{
				return StringValue(GNSDK_GDO_VALUE_DEATH_PLACE);
			}

			/**
			 * Retrieves the contributor's media space.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t MediaSpace() const
			{
				return StringValue(GNSDK_GDO_VALUE_MEDIA_SPACE);
			}

			/**
			 * Retrieves an iterator for accessing the contributor's official name(s).
			 */
			gn_iterable_container<name_iterator> NamesOfficial() const
			{
				gn_gdo_provider<GnName> provider(*this, GNSDK_GDO_CHILD_NAME_OFFICIAL);
				gnsdk_uint32_t          count = provider.count() + 1;
				return gn_iterable_container<name_iterator>(name_iterator(provider, 1, count), name_iterator(provider, count, count) );
			}

			/**
			 * Retrieves an iterator for accessing the contributor's regional name(s).
			 */
			gn_iterable_container<name_iterator> NamesRegional() const
			{
				gn_gdo_provider<GnName> provider(*this, GNSDK_GDO_CHILD_NAME_REGIONAL);
				gnsdk_uint32_t          count = provider.count() + 1;
				return gn_iterable_container<name_iterator>(name_iterator(provider, 1, count), name_iterator(provider, count, count) );
			}

			/**
			 * Retrieves an iterator for the external IDs associated with this contributor.
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			/**
			 * Retrieves a contributor that collaborated with this contributor in the context of the returned result.
			 */
			GnContributor Collaborator() const
			{
				return ChildGet<GnContributor>(GNSDK_GDO_CHILD_CONTRIBUTOR);
			}

			/**
			 * Retrieves the contributor's genre.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 * Retrieves the contributor's origin.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 * Retrieves the contributor's era.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnEra Era() const
			{
				return Reflect<GnEra>();
			}

			/**
			 * Retrieves contributor's artist type.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnArtistType ArtistType() const
			{
				return Reflect<GnArtistType>();
			}

			bool CollaboratorResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_COLLABORATOR_RESULT);

				return gnstd::gn_strtobool(return_value);
			}
		};

		/**
		 * Represents the role that a contributor played in a music or video production;
		 * for example, singing, playing an instrument, acting, directing, and so on.
		 * <p><b>Note:</b></p>
		 * For music credits, the absence of a role for a person indicates that person is the primary
		 * artist, who may have performed multiple roles.
		 */
		class GnRole : public GnDataObject
		{
		public:
			GnRole() : GnDataObject(GNSDK_NULL_GDO) { }
			GnRole(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle)   { }

			virtual ~GnRole() { }

			/**
			 * Retrieves a role category, such as string instruments or brass instruments.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Category() const
			{
				return StringValue(GNSDK_GDO_VALUE_ROLE_CATEGORY);
			}

			/**
			 * Retrieves the role's display string.
			 */
			gnsdk_cstr_t Role() const
			{
				return StringValue(GNSDK_GDO_VALUE_ROLE);
			}
		};

		/**
		 * A credit lists the contribution of a person (or occasionally a company, such as a record label)
		 * to a recording.
		 */
		class GnCredit : public GnDataObject
		{
		public:
			GnCredit() : GnDataObject(GNSDK_NULL_GDO) { }
			GnCredit(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnCredit() { }

			/**
			 * Retrieves the credit's name, such as the name of the person or company.
			 */
			GnName Name() const
			{
				return ChildGet<GnName>(GNSDK_GDO_CHILD_NAME_OFFICIAL);
			}

			/**
			 * Retrieves the role played by the person and company represented by this credit.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnRole Role() const
			{
				return Reflect<GnRole>();
			}

			/**
			 * Retrieves the credit's contributor.
			 */
			GnContributor Contributor() const
			{
				return ChildGet<GnContributor>(GNSDK_GDO_CHILD_CONTRIBUTOR);
			}
		};

		/**
		 * A collection of classical music recordings.
		 */
		class GnAudioWork : public GnDataObject
		{
		public:
			GnAudioWork(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnAudioWork() { }

			/**
			 * Retrieves the audio work's official title.
			 */
			GnTitle Title() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 * Retrieves a credit for this audio work.
			 */
			GnCredit Credit() const
			{
				return ChildGet<GnCredit>(GNSDK_GDO_CHILD_CREDIT);
			}

			/**
			 * Retrieves the Gracenote TUI for this audio work.
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 * Retrieves the Gracenote TUI for this audio work.
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 * Retrieves a Gracenote Tag identifier for this audio work (Tag ID is a synonym for Product ID)
			 * <p><b>Remarks:</b></p>
			 * This method exists primarily to support legacy implementations. We recommend using
			 * the Product ID method to retrieve product related identifiers
			 */
			gnsdk_cstr_t TagID() const
			{
				return StringValue(GNSDK_GDO_VALUE_TAGID);
			}

			/**
			 * Retrieves a Gracenote Identifier for this audio work.
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 * Retrieves a Gracenote identifier for this audio work.
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 * Retrieves the audio work's origin.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 * Retrieves the audio work's era.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnEra Era() const
			{
				return Reflect<GnEra>();
			}

			/**
			 * Retrieves the audio work's genre.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 * Retrieves a classical music composition form value (for example, Symphony) for this audio work.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t CompositionForm() const
			{
				return StringValue(GNSDK_GDO_VALUE_COMPOSITION_FORM);
			}
		};

		/**
		 * Represents the mood of a search result
		 */
		class GnMood : public GnDataObject
		{
		public:
			GnMood() : GnDataObject(GNSDK_NULL_GDO)  { }
			GnMood(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle)   { }

			virtual ~GnMood() { }

			/**
			 * Retrieves the display string from level 1 of the associated mood hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_MOOD_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated mood hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_MOOD_LEVEL2);
			}
		};

		/**
		 * Represents the tempo of a search result
		 */
		class GnTempo : public GnDataObject
		{
		public:
			GnTempo() : GnDataObject(GNSDK_NULL_GDO) { }
			GnTempo(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnTempo() { }

			/**
			 * Retrieves the display string from level 1 of the associated tempo hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level1() const
			{
				return StringValue(GNSDK_GDO_VALUE_TEMPO_LEVEL1);
			}

			/**
			 * Retrieves the display string from level 2 of the associated tempo hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level2() const
			{
				return StringValue(GNSDK_GDO_VALUE_TEMPO_LEVEL2);
			}

			/**
			 * Retrieves the display string from level 3 of the associated tempo hierarchy
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Level3() const
			{
				return StringValue(GNSDK_GDO_VALUE_TEMPO_LEVEL3);
			}
		};

		/**
		 * The person or group primarily responsible for creating the Album or Track.
		 */
		class GnArtist : public GnDataObject
		{
		public:
			GnArtist() : GnDataObject(GNSDK_NULL_GDO) { }
			GnArtist(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnArtist() { }

			/**
			 * Retrieves the Gracenote global identifier for this artist.
			 */
			gnsdk_cstr_t GlobalID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GLOBALID);
			}

			/**
			 * Retrieves the artist's official name.
			 */
			GnName Name() const
			{
				return ChildGet<GnName>(GNSDK_GDO_CHILD_NAME_OFFICIAL);
			}

			/**
			 * Retrieves a contributor associated with this artist in the context of the returned result.
			 */
			GnContributor Contributor() const
			{
				return ChildGet<GnContributor>(GNSDK_GDO_CHILD_CONTRIBUTOR);
			}
		};

		/**
		 * A song or instrumental recording.
		 */
		class GnTrack : public GnDataObject
		{
		public:
			GnTrack() : GnDataObject(GNSDK_NULL_GDO) { }
			GnTrack(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnTrack(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_TRACK) { }

			virtual ~GnTrack() { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves the track's official title.
			 */
			GnTitle Title() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 * Retrieves track's artist.
			 */
			GnArtist Artist() const
			{
				return ChildGet<GnArtist>(GNSDK_GDO_CHILD_ARTIST);
			}

			/**
			 * Retrieves a credit associated with the track.
			 */
			GnCredit Credit() const
			{
				return ChildGet<GnCredit>(GNSDK_GDO_CHILD_CREDIT);
			}

			/**
			 * Retrieves the track's audio work.
			 */
			GnAudioWork Work() const
			{
				return ChildGet<GnAudioWork>(GNSDK_GDO_CHILD_AUDIO_WORK);
			}

			/**
			 * Retrieves the track's mood.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnMood Mood() const
			{
				return Reflect<GnMood>();
			}

			/**
			 * Retrieves the track's tempo.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnTempo Tempo() const
			{
				return Reflect<GnTempo>();
			}

			/**
			 * Retrieves the track's genre.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 * Retrieves an iterator for all of the external identifiers associated with this track.
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			/**
			 * Retrieves the Gracenote TUI for this track.
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 * Retrieves the Gracenote TUI Tag for this track.
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 * Retrieves a Gracenote Tag identifier for this track (Tag ID is a synonym for Product ID)
			 * <p><b>Remarks:</b></p>
			 * This method exists primarily to support legacy implementations. We recommend using
			 * the Product ID method to retrieve product related identifiers
			 */
			gnsdk_cstr_t TagID() const
			{
				return StringValue(GNSDK_GDO_VALUE_TAGID);
			}

			/**
			 * Retrieves a Gracenote identifier for this track.
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 * Retrieves a Gracenote identifier for this track.
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 * Retrieves the track number assigned by its order on the album.
			 */
			gnsdk_cstr_t TrackNumber() const
			{
				return StringValue(GNSDK_GDO_VALUE_TRACK_NUMBER);
			}

			/**
			 * Retrieves the track's year.
			 */
			gnsdk_cstr_t Year() const
			{
				return StringValue(GNSDK_GDO_VALUE_YEAR);
			}

			/**
			 * Retrieves the track's identifier associated with a file information object presented to Music-ID.
			 */
			gnsdk_cstr_t MatchedIdent() const
			{
				return StringValue(GNSDK_GDO_VALUE_IDENT);
			}

#if GNSDK_MUSICID_FILE

			/**
			 * Retrieves the track's file name associated with a file information object presented to Music-ID.
			 */
			gnsdk_cstr_t MatchedFilename() const
			{
				return StringValue(GNSDK_MUSICIDFILE_GDO_VALUE_FILENAME);
			}

			/**
			 * Retrieve the track's match type.
			 *  <p><b>Remarks:</b></p>
			 * The match type indicates which query type was the most authoritative matching type for a given file
			 * information object.
			 * MusicID-File does a query for each type of input data, and each type of input data has an authoritative rank.
			 * The match type indicates the highest authoritative matched type for this file information object.
			 * The match type is only useful in comparison to other match types. By itself it does not indicate
			 * a strong or poor match. The higher the match type, the more authoritative the identification process
			 * used.
			 * The following table lists the possible match type values:
			 * <table>
			 * <tr><th>Match Type</th><th>Value</th></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TUI</td><td>11</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_MUI</td><td>10</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TOC</td><td>9</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_ASSOCIATED_ID</td><td>8</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_WF</td><td>7</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TEXT_ON_WF</td><td>6</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_ASSOCIATED_TEXT</td><td>5</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TEXT_TRACK</td><td>4</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TEXT_ALBUM</td><td>3</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_TEXT_CONTRIBUTOR</td><td>2</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_NONE</td><td>1</td></tr>
			 * <tr><td>MIDF_MATCH_TYPE_INVALID</td><td>0</td></tr>
			 * </table>
			 */
			gnsdk_cstr_t MatchLookupType() const
			{
				return StringValue(GNSDK_MUSICIDFILE_GDO_VALUE_MATCH_TYPE);
			}

			gnsdk_cstr_t MatchConfidence() const
			{
				return StringValue(GNSDK_MUSICIDFILE_GDO_VALUE_MATCH_CONFIDENCE);
			}
#endif

			/**
			 * Retrieve the track's match score.
			 * <p><b>Remarks:</b></p>
			 * The match score gives a correlation between the input text and the matched track,
			 * indicating how well the input text matched the track. However, any result that is returned
			 * is considered to be a good match. The match score is only useful in comparison to other match
			 * scores. By itself it does not indicate a strong or poor match.
			 */
			gnsdk_int32_t MatchScore() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_TEXT_MATCH_SCORE);

				return gnstd::gn_atoi(return_value);
			}

			GnTitle TitleTLS() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_TLS);
			}

			GnTitle TitleRegional() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_REGIONAL);
			}

			/*
			 *   GnAlbum
			 *   Album() const
			 *   {
			 *    return ChildGet<GnAlbum>(GNSDK_GDO_CHILD_ALBUM);
			 *   }
			 */

			/*
			 *   gnsdk_cstr_t
			 *   Date() const
			 *   {
			 *    return StringValue(GNSDK_GDO_VALUE_DATE);
			 *   }
			 */
		};

		/**
		 * GnResponseTracks
		 * Track response
		 */
		class GnResponseTracks : public GnDataObject
		{
		public:
			GnResponseTracks() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseTracks(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseTracks() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves an iterator for accessing the tracks in the response.
			 */
			gn_iterable_container<track_iterator> Tracks() const
			{
				gn_gdo_provider<GnTrack> provider(*this, GNSDK_GDO_CHILD_TRACK);
				gnsdk_uint32_t           count = provider.count() + 1;
				return gn_iterable_container<track_iterator>(track_iterator(provider, 1, count), track_iterator(provider, count, count) );
			}
		};

		class GnContents : public GnDataObject
		{
		public:
			GnContents() : GnDataObject(GNSDK_NULL_GDO) { }
			GnContents(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnContents()  { }
		};

		/**
		 * A collection of audio recordings, typically of songs or instrumentals.
		 */
		class GnAlbum : public GnDataObject
		{
		public:
			GnAlbum() : GnDataObject(GNSDK_NULL_GDO) { }
			GnAlbum(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnAlbum(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_ALBUM) { }

			virtual ~GnAlbum() { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves this album's official title.
			 */
			GnTitle Title() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 * Retrieves this album's artist.
			 */
			GnArtist Artist() const
			{
				return ChildGet<GnArtist>(GNSDK_GDO_CHILD_ARTIST);
			}

			/**
			 * Retrieves this album's genre.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 * Retrieves this album's label, being the name of the record company that released the album.
			 * Album label values are not always available.
			 * <p><b>Remarks:</b></p>
			 * This is a list-based value requiring that a corresponding locale or list be loaded.
			 */
			gnsdk_cstr_t Label() const
			{
				return StringValue(GNSDK_GDO_VALUE_ALBUM_LABEL);
			}

			/**
			 * Retrieves this album's language
			 */
			GnLanguage Language() const
			{
				return Reflect<GnLanguage>();
			}

			/**
			 * Retrieves the Gracenote TUI for this album.
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 * Retrieves the Gracenote TUI Tag for this album.
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 * Retrieves a Gracenote Tag identifier for this album (Tag ID is a synonym for Product ID)
			 * <p><b>Remarks:</b></p>
			 * This method exists primarily to support legacy implementations. We recommend using
			 * the Product ID method to retrieve product related identifiers
			 */
			gnsdk_cstr_t TagID() const
			{
				return StringValue(GNSDK_GDO_VALUE_TAGID);
			}

			/**
			 * Retrieves a Gracenote identifier for this album.
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 * Retrieves a Gracenote identifier for this album.
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 * Retrieves the Gracenote global identifier for this album.
			 */
			gnsdk_cstr_t GlobalID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GLOBALID);
			}

			/**
			 * Retrieves the volume number of this album in a multi-volume set.
			 * This value is not always available.
			 */
			gnsdk_int32_t DiscInSet() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ALBUM_DISC_IN_SET);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves total number of volumes in a multi-volume set.
			 * This value is not always available.
			 */
			gnsdk_int32_t TotalInSet() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ALBUM_TOTAL_IN_SET);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves the year the album was released.
			 */
			gnsdk_cstr_t Year() const
			{
				return StringValue(GNSDK_GDO_VALUE_YEAR);
			}

			// gnsdk_cstr_t AlbumType() const
			// {
			// };

			/**
			 * Retrieves a boolean value (Y/N) that indicates whether enhanced classical music data exists
			 * for this album.
			 */
			bool IsClassical() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_CLASSICAL_DATA);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves the total number of tracks on this album.
			 */
			gnsdk_int32_t TrackCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ALBUM_TRACK_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * @internal Compilation @endinternal
			 * Retrieves an album compilation value for a album.
			 */
			gnsdk_cstr_t Compilation() const
			{
				return StringValue(GNSDK_GDO_VALUE_ALBUM_COMPILATION);
			}

			/**
			 * @internal Compilation @endinternal
			 * Based on text input, GNSDK Manager generates a match confidence score for results of a top-level
			 *   object, for Album and Track text results.
			 */
			gnsdk_int32_t MatchScore() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_TEXT_MATCH_SCORE);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a the match position in milliseconds of the matched track
			 */
			gnsdk_int32_t MatchPosition () const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_MATCH_POSITION_MS);
				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a track from this album using the track number.
			 * @param trackNumber	Ordinal of the desired track
			 */
			GnTrack Track(gnsdk_uint32_t trackNumber) const
			{
				return ChildGet<GnTrack>(GNSDK_GDO_CHILD_TRACK_BY_NUMBER, trackNumber);
			}

			/**
			 * Retrieves an iterator for accessing the tracks on this album.
			 */
			gn_iterable_container<track_iterator> Tracks() const
			{
				gn_gdo_provider<GnTrack> provider(*this, GNSDK_GDO_CHILD_TRACK);
				gnsdk_uint32_t           count = provider.count() + 1;
				return gn_iterable_container<track_iterator>(track_iterator(provider, 1, count), track_iterator(provider, count, count) );
			}

			/**
			 * Retrieves an iterator for accessing the matched track(s) on this album.
			 */
			gn_iterable_container<track_iterator>
			TracksMatched() const
			{
				gn_gdo_provider<GnTrack> provider(*this, GNSDK_GDO_CHILD_TRACK_MATCHED);
				gnsdk_uint32_t           count = provider.count() + 1;
				return gn_iterable_container<track_iterator>(track_iterator(provider, 1, count), track_iterator(provider, count, count) );
			}

			/**
			 * Retrieves an iterator for accessing credits for this album, except artist credit.
			 * The Artist Credit is accessed via a dedicated method.
			 */
			gn_iterable_container<credit_iterator> Credits() const
			{
				gn_gdo_provider<GnCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t            count = provider.count() + 1;
				return gn_iterable_container<credit_iterator>(credit_iterator(provider, 1, count), credit_iterator(provider, count, count) );
			}

			/**
			 * Retrieves an iterator for all of the external identifiers associated with this album.
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			GnTitle TitleTLS() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_TLS);
			}

			GnTitle TitleRegional() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_REGIONAL);
			}

			/*
			 *   gnsdk_cstr_t
			 *   GetDisplayLanguage() const
			 *   {
			 *     return StringValue(GNSDK_GDO_VALUE_TITLE_DISPLAY_LANGUAGE);
			 *
			 *   }
			 *
			 */
			GnTitle TitleRegionalLocale() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_REGIONAL_LOCALE);
			}

			/*
			 *   gn_iterable_container<GnContents>
			 *   Contents() const
			 *   {
			 *    gn_gdo_provider<GnContributor> provider(*this, GNSDK_GDO_CHILD_CONTENT);
			 *    gnsdk_uint32_t count = provider.count() + 1;
			 *    return gn_iterable_container<contents_iterator>(contents_iterator(provider,1, count),contents_iterator(provider, count,count));
			 *   }
			 *
			 */
			gnsdk_int32_t TrackMatchNumber() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_TRACK_MATCHED_NUM);

				return gnstd::gn_atoi(return_value);
			}
		};

		/**
		 * Album response
		 */
		class GnResponseAlbums : public GnDataObject
		{
		public:
			GnResponseAlbums() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseAlbums(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseAlbums() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote limits the number of
			 *  responses returned in any one request, so the range values are available to indicate the total
			 *  number of results, and where the current results fit in within that total.
			 *
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *  requested. To accurately iterate through results, always check the range start, end, and total
			 *  values and the responses returned for the query (or queries). Ensure that you
			 *  are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *  using the initial requested value.
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves the ordinal of the last result in the returned range.
			 * <p><b>Remarks:</b></p>
			 * Range values are available to aid in paging results. Gracenote limits the number of
			 * responses returned in any one request, so the range values are available to indicate the total
			 * number of results, and where the current results fit in within that total.
			 *
			 * <p><b>Important:</b></p>
			 * The number of results actually returned for a query may not equal the number of results initially
			 * requested. To accurately iterate through results, always check the range start, end, and total
			 * values and the responses returned for the query (or queries). Ensure that you
			 * are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 * using the initial requested value.
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves the estimated total number of results possible.
			 * <p><b>Remarks:</b></p>
			 * Range values are available to aid in paging results. Gracenote limits the number of
			 * responses returned in any one request, so the range values are available to indicate the total
			 * number of results, and where the current results fit in within that total.
			 * The total value may be estimated.
			 *
			 * <p><b>Important:</b></p>
			 * The number of results actually returned for a query may not equal the number of results initially
			 * requested. To accurately iterate through results, always check the range start, end, and total
			 * values and the responses returned for the query (or queries). Ensure that you
			 * are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 * using the initial requested value.
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves an iterator to access the matched albums.
			 */
			gn_iterable_container<album_iterator> Albums() const
			{
				gn_gdo_provider<GnAlbum> provider(*this, GNSDK_GDO_CHILD_ALBUM);
				gnsdk_uint32_t           count = provider.count() + 1;
				return gn_iterable_container<album_iterator>(album_iterator(provider, 1, count), album_iterator(provider, count, count) );
			}
		};

		/**
		 * This represent a MusicIdMatch object
		 */
		class GnMatch : public GnDataObject
		{
		public:
			GnMatch() : GnDataObject(GNSDK_NULL_GDO) { }
			GnMatch(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnMatch() { }

			/**
			 * Retrieves the Gracenote TUI for the match.
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 * Retrieves the Gracenote TUI Tag for the match.
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			gnsdk_cstr_t MatchInfo() const
			{
				return StringValue(GNSDK_GDO_VALUE_MATCH_INFO_TYPE);
			}

			/**
			 * Retrieves an iterator for all of the external identifiers associated with this match.
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}
		};

		class GnResponseMatches : public GnDataObject
		{
		public:
			explicit GnResponseMatches(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseMatches() { }

			/**
			 * Retrieves an iterator for accessing the match(es) in this response.
			 */
			gn_iterable_container<matches_iterator> Matches() const
			{
				gn_gdo_provider<GnMatch> provider(*this, GNSDK_GDO_CHILD_MATCH_INFO);
				gnsdk_uint32_t           count = provider.count() + 1;
				return gn_iterable_container<matches_iterator>(matches_iterator(provider, 1, count), matches_iterator(provider, count, count) );
			}
		};

		class GnDataMatch : public GnDataObject
		{
		public:
			GnDataMatch() : GnDataObject(GNSDK_NULL_GDO) { }
			explicit GnDataMatch(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			virtual ~GnDataMatch() { }

			bool IsAlbum()
			{
				return 0 == gnstd::gn_strcmp(GNSDK_GDO_TYPE_ALBUM, this->GetType() );
			}

			bool IsContributor()
			{
				return 0 == gnstd::gn_strcmp(GNSDK_GDO_TYPE_CONTRIBUTOR, this->GetType() );
			}

			GnAlbum GetAsAlbum()
			{
				if(IsAlbum() )
				{
					return Reflect<GnAlbum>();
				}
				else
				{
					return GnAlbum();
				}
			}

			GnContributor GetAsContributor()
			{
				if(IsContributor() )
				{
					return Reflect<GnContributor>();
				}
				else
				{
					return GnContributor();
				}
			}
		};

		class GnResponseDataMatches : public GnDataObject
		{
		public:
			GnResponseDataMatches(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnResponseDataMatches() : GnDataObject(GNSDK_NULL_GDO) { }

			virtual ~GnResponseDataMatches() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 * Retrieves an iterator for accessing the match(es) in this response.
			 */
			gn_iterable_container<datamatches_iterator> DataMatches() const
			{
				gn_gdo_provider<GnDataMatch> provider(*this, GNSDK_GDO_CHILD_MATCH);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<datamatches_iterator>(datamatches_iterator(provider, 1, count), datamatches_iterator(provider, count, count) );
			}
		};
	} // namespace metadata
}     // namespace gracenote
#endif // _METADATA_MUSIC_HPP_
