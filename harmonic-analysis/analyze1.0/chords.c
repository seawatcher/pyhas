 /***************************************************************************/
 /*                                                                         */
 /*       Copyright (C) 1996  Daniel Sleator and David Temperley            */
 /*  See file "README" for information about commercial use of this system  */
 /*                                                                         */
 /***************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "analyze.h"

/* This file contains code that can builds the chord list from a note
   list and the beatlevel structure.  See the comment near the
   definition of the Chord structure for more info on what a chord is.
   For a description of the properties of the note list see
   documentation above build_note_list_from_event_list().  */


Chord * new_chord(Note *note) {
  Chord * c_chord;
  /* build a new chord with the parameters taken from the given note */
  c_chord = (Chord *) malloc(sizeof(Chord));
  c_chord->start = note->start;
  c_chord->duration = note->duration;
  c_chord->note = NULL;
  c_chord->next = NULL;
  c_chord->level = c_chord->level_time = 0;  /* not filled in */
  return c_chord;
}

Chord * reverse_ch(Chord * a) {
  Chord *slx;
  Chord *b = NULL;
    
    for (; a != NULL; a = slx) {
	slx = a->next;
	
	a->next = b;
	b = a;
    }
    return b;
}

/* this function builds a chord representation of a piece from the note
   list.  The function does not effect the note list.  The level and
   level_time fields of the chord list are not filled in by this
   function.  */
   
Chord * build_chord_representation (Note * nl) {
  Note * xnl, *n_copy;
  Chord * c_chord, * head_chord;
  int end_time;
  head_chord = c_chord = NULL;

  for (xnl = nl; xnl != NULL; xnl = xnl->next) {
    if (c_chord == NULL) head_chord = c_chord = new_chord(xnl);
    n_copy = (Note *) malloc(sizeof(Note));
    *n_copy = *xnl;
    if (c_chord->start == xnl->start && c_chord->duration == xnl->duration) {
      /* add to current chord */
      n_copy->next = c_chord->note;
      c_chord->note = n_copy;
      continue;
    }

    end_time = c_chord->start + c_chord->duration;

    if (end_time > xnl->start) {
      fprintf(stderr, "A note begins during another note.\n");
      exit(1);
    }

    if (end_time < xnl->start) {
      /* add a blank chord */
      c_chord->next = new_chord(xnl);  /* will throw out these parameters */
      c_chord = c_chord->next;
      c_chord->start = end_time;  /* this starts when the previous ends */
      c_chord->duration = xnl->start - end_time;
    }

    /* start a new chord right after the current one */
    c_chord->next = new_chord(xnl);
    c_chord = c_chord->next;
    n_copy->next = NULL;
    c_chord->note = n_copy;
  }
  return head_chord;
}

void print_chord_list(Chord *clist) {
  Note * note;
  int i;
  printf("Chord List:\n");

  for (;clist != NULL; clist = clist->next) {
    printf(" ");
    for (i=0; i<N_beatlevel; i++) if (clist->level >= i) printf("x "); else printf("  ");
    
    printf("start = %5d  duration = %5d  od = %1d  notes: {", clist->start, clist->duration, clist->is_first_chord);
    for (note = clist->note; note != NULL; note = note->next) {
      printf("(%2d,%2d)", note->pitch, note->tpc);
    }
    printf("}\n");
  }
}

/* This takes a cord representation as computed by
   build_chord_representation() and returns a new chord representation
   in which each original chord is broken into some integral number of
   shorter chords.  The durations of these short chords should be
   roughly equal to the baseunit.  Furthermore, these chords are
   labelled with the rythmic level and the rythmic time of the note.

   The note lists of the new chord list point to the same note lists as
   the original chord list.  Several chords in the new list may point to
   the same note lists.  */

Chord * build_metered_chord_representation(Chord *chord) {
  Chord *ch, *nch, *ret_ch;
  int parts, i, level, count;
  double actual_time;
  double ratio;

  if (baseunit < 0) {
    fprintf(stderr, "BaseUnit has not been set.\n");
    exit(1);
  }

  ret_ch = NULL;
  
  for (ch = chord; ch!=NULL; ch = ch->next) {
    parts = (2*ch->duration + baseunit) / (2 * baseunit);  /* rounded version of duration/baseunit */
    actual_time = ((double) ch->duration)/ ((double) parts);
    ratio = actual_time/((double)baseunit);
    if (ratio < .9 || ratio > 1.1) {
      fprintf(stderr, "A chord of duration %d is incomensurate with the baseunit (%d).\n", ch->duration, baseunit);
      exit(1);
    }
    /* now we break the given chord into parts parts */
    for (i=0; i<parts; i++) {
      nch = (Chord *) malloc (sizeof(Chord));
      nch->start = (i * ch->duration)/parts + ch->start;
      nch->duration = (((i+1) * ch->duration)/parts) - ((i * ch->duration)/parts);
      nch->note = ch->note;
      nch->next = ret_ch;
      nch->level = nch->level_time = 0; /* will be filled in later */
      nch->is_first_chord = (i == 0);
      ret_ch = nch;
    }
  }
  ret_ch = reverse_ch(ret_ch);

  /* now we add the rythmic stuff */

  for (level = 1; level < N_beatlevel; level++) {
    count = beatlevel[level].count - beatlevel[level].start;
    for (ch = ret_ch; ch!=NULL; ch = ch->next) {
      if  (ch->level == level-1) {
	if ((count % beatlevel[level].count) == 0) {
	  ch->level = level;
	}
	count++;
      }
    }
  }

  for (ch = ret_ch; ch!=NULL; ch = ch->next, count++) {
    ch->level_time = beatlevel[ch->level].units * baseunit;
  }
  
  return ret_ch;
}


/* This takes a metered chord representation, computed by the function
   above, and constructs a new one.  It works by coalescing successive
   chords that have the same notes into a single chord.  It only does
   this if the beat level of the new one is lower (less important) than
   the beat level of the chord you started this group with.
   */

#if 1

Chord * compact_metered_chord_representation(Chord *chord) {
  Chord *ch, *nch, *ret_ch, *current_chord;
  ret_ch = NULL;
  if (chord == NULL) return NULL;

  current_chord = (Chord *) malloc(sizeof(Chord));
  *current_chord = *chord;
  current_chord->next = NULL;
  for (ch = chord->next; ch!=NULL; ch = ch->next) {
    /* current_chord is the chord we're building onto at the moment */
    if (ch->note == current_chord->note &&    /* they have the same notes */
	ch->level < current_chord->level) {   /* the new chord is at a less significant beat level */
      /* glue them together */
      current_chord->duration += ch->duration;
    } else {
      /* don't glue them together */
      /* emit the current chord, and start a new one */

      nch = (Chord *) malloc (sizeof(Chord));
      *nch = *ch;
      nch->next = current_chord;
      current_chord = nch;
    }
  }
  return reverse_ch(current_chord);
}

#else

Chord * compact_metered_chord_representation(Chord *chord) {
  Chord *ch, *nch, *ret_ch, *current_chord;
  ret_ch = NULL;

  current_chord = NULL;
  for (ch = chord; ch!=NULL; ch = ch->next) {
    nch = (Chord *) malloc (sizeof(Chord));
    *nch = *ch;
    nch->next = current_chord;
    current_chord = nch;
  }
  return reverse_ch(current_chord);
}

#endif

void free_chords(Chord * ch) {
  Chord *xch;
  for (; ch != NULL; ch = xch) {
    xch = ch->next;
    free(ch);
  }
}
