/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 *  Name: VideoLocale
 *  Description:
 *  This sample shows basic access of locale-dependent fields
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license_path lib_path 
 */
package video_locale;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class VideoLocale {
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	/*
	 * GnStatusEventsListener : overrider methods of this class to get delegate
	 * callbacks
	 */
	private static class LookupStatusEvents extends GnStatusEventsListener {
		@Override
		public void statusEvent(GnStatus video_status, long percent_complete,
				long bytes_total_sent, long bytes_total_received) {
			System.out.print("status (");
			switch (video_status) {
				case gnsdk_status_unknown :
					System.out.print("Unknown ");
					break;
				case gnsdk_status_begin :
					System.out.print("Begin ");
					break;
				case gnsdk_status_connecting :
					System.out.print("Connecting ");
					break;
				case gnsdk_status_sending :
					System.out.print("Sending ");
					break;
				case gnsdk_status_receiving :
					System.out.print("Receiving ");
					break;
				case gnsdk_status_disconnected :
					System.out.print("Disconnected ");
					break;
				case gnsdk_status_complete :
					System.out.print("Complete ");
					break;
				default :
					break;
			}
			System.out.println("), % complete (" + percent_complete
					+ "), sent (" + bytes_total_sent + "), received ("
					+ bytes_total_received + ")");
		}

		@Override
		public boolean cancelCheck() {
			return false;
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {

		LookupStatusEvents localeEvents = new LookupStatusEvents();
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupVideo(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(final GnSDK gnsdk, final String clientId,
			final String clientIdTag, final String applicationVersion)
			throws GnException, IOException {

		String serialized = null;

		if (!new File("user.txt").canRead()) {
			System.out
					.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			
			serialized = gnsdk.registerUser(userRegistrationMode, clientId,
					clientIdTag, applicationVersion).toString();

			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));

			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
			try {
				/* read stored user data from file */
				serialized = br.readLine();

			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				br.close();
			}
		}

		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}


	private static void performSampleWorkSearch(GnUser user) throws GnException {
		String value = null;
		/*
		 * Typically, the GDO passed in to a Link query will come from the
		 * output of a GNSDK query. For an example of how to perform a query and
		 * get a GDO please refer to the documentation or other sample
		 * applications. The below serialized GDO was an 1-track album result
		 * from another GNSDK query.
		 */
		String serializedGDO = "WEcxA6R75JwbiGUIxLFZHBr4tv+bxvwlIMr0XK62z68zC+/kDDdELzwiHmBPkmOvbB4rYEY/UOOvFwnk6qHiLdb1iFLtVy44LfXNsTH3uNgYfSymsp9uL+hyHfrzUSwoREk1oX/rN44qn/3NFkEYa2FoB73sRxyRkfdnTGZT7MceHHA/28aWZlr3q48NbtCGWPQmTSrK";
		System.out.println("\n*****Sample Video Work Search*****");
		LookupStatusEvents midEvents = new LookupStatusEvents();
		GnVideo video = new GnVideo(user, null); /*
												 * midEvents can be used to view
												 * the progress
												 */
		GnDataObject gnDataObject = new GnDataObject(serializedGDO);
		GnResponseVideoWork gnResponseVideoWork = video.findWorks(gnDataObject);
		long count = 0;
		count = gnResponseVideoWork.works().count();
		System.out.println("\n\nNumber matches: " + count);
		GnVideoWorkIterable gnVideoWorkIterable = gnResponseVideoWork.works();
		GnVideoWorkIterator gnVideoWorkIterator = gnVideoWorkIterable
				.getIterator();
		while (gnVideoWorkIterator.hasNext()) {
			GnVideoWork gnVideoWork = (GnVideoWork) gnVideoWorkIterator.next();
			// Work title
			GnTitle workTitle = gnVideoWork.officialTitle();
			value = workTitle.display();
			if (null != value && "" != value)
				System.out.println("\nTitle: " + workTitle.display());
			// Origin info
			GnOrigin gnOrigin = gnVideoWork.origin();
			value = gnOrigin.level1();
			if (null != value && "" != value)
				System.out.println("\nOrigin1: " + value);
			value = gnOrigin.level2();
			if (null != value && "" != value)
				System.out.println("\nOrigin2: " + value);
			value = gnOrigin.level3();
			if (null != value && "" != value)
				System.out.println("\nOrigin3: " + value);
			value = gnOrigin.level4();
			if (null != value && "" != value)
				System.out.println("\nOrigin4: " + value);
			// Genre info
			GnGenre gnGenre = gnVideoWork.genre();
			value = gnGenre.level1();
			if (null != value && "" != value)
				System.out.println("\nGenre1: " + value);
			value = gnGenre.level2();
			if (null != value && "" != value)
				System.out.println("\nGenre2: " + value);
			value = gnGenre.level3();
			if (null != value && "" != value)
				System.out.println("\nGenre3: " + value);
			// Primary rating
			GnRating gnRating = gnVideoWork.rating();
			value = gnRating.rating();
			if (null != value && "" != value) {
				System.out.print("\nRating: " + value);
				value = gnRating.ratingType();
				if (null != value && "" != value)
					System.out.print(" [" + value + "]");
				value = gnRating.ratingDesc();
				if (null != value && "" != value)
					System.out.print(" - " + value);
				value = gnRating.ratingReason();
				if (null != value && "" != value)
					System.out.print(" Reason: " + value);
				System.out.println();
			}
			value = gnVideoWork.plotSynopsis();
			if (null != value && "" != value)
				System.out.println("\nPlot synopsis: " + value);
			GnVideoCreditIterable gnVideoCreditIterable = gnVideoWork
					.videoCredits();
			GnVideoCreditIterator gnVideoCreditIterator = gnVideoCreditIterable
					.getIterator();
			while (gnVideoCreditIterator.hasNext()) {
				GnVideoCredit gnVideoCredit = (GnVideoCredit) gnVideoCreditIterator
						.next();
				// Display name
				GnName gnName = gnVideoCredit.officialName();
				value = gnName.display();
				if (value != null && value != "")
					System.out.println("\nName: " + value);
				// Genres
				GnGenre creditGenre = gnVideoCredit.genre();
				value = creditGenre.level1();
				if (value != null && value != "")
					System.out.println("\n\tGenre1: " + value);
				value = creditGenre.level2();
				if (value != null && value != "")
					System.out.println("\n\tGenre2: " + value);
				value = creditGenre.level3();
				if (value != null && value != "")
					System.out.println("\n\tGenre3: " + value);
			}
		}
	}


	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				loadLocale(gnsdk, user);
				performSampleWorkSearch(user);
			} catch (GnException gnException) {
				System.out.println("\nError Code: " + gnException.getErrorCode()
						+ "\nError Message: " + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
