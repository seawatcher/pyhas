/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_ITERATOR_BASE_HPP_
#define _GNSDK_ITERATOR_BASE_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

namespace gracenote
{
	template<typename T, typename _Provider>
	class gn_facade_range_iterator
	{
	public:
		// typedefs for iterator_traits
		typedef T                value_type;
		typedef gnsdk_uint32_t   difference_type;
		typedef T*               pointer;
		typedef T&               reference;

		// destructor and interface.
		~gn_facade_range_iterator() { }

		bool operator                            == (const gn_facade_range_iterator& rhs) const { return ( pos_ == rhs.pos_ ) && ( count_ == rhs.count_ );  }

		bool operator                            != (const gn_facade_range_iterator& rhs) const { return !( ( ( pos_ == rhs.pos_ ) && ( count_ == rhs.count_ ) ) ); }

		reference operator                       * ()                       { return current_ = provider_.get_data(pos_);  }
		pointer operator                         -> ()                      { return &( current_ = provider_.get_data(pos_) ); }

		const gn_facade_range_iterator& operator ++ ()                      { ++pos_; return *this; }

		const gn_facade_range_iterator& operator += (gnsdk_uint32_t offset) { pos_ += offset; return *this; }

		// SWIG Specific interface.
		value_type                               next() { return current_ = provider_.get_data(pos_++);  }

		// SWIG Specific interface.
		bool            hasNext()                       { return pos_ < count_;  }

		difference_type distance(const gn_facade_range_iterator& itr) const { return ( itr.pos_ > pos_ ) ? itr.pos_ - pos_ : pos_ - itr.pos_; }

		gn_facade_range_iterator(_Provider provider, gnsdk_uint32_t pos, gnsdk_uint32_t count)
			: provider_(provider), pos_(pos), count_(count)
		{ }
		gn_facade_range_iterator(_Provider provider, gnsdk_uint32_t pos, gnsdk_uint32_t count, T start)
			: provider_(provider), pos_(pos), count_(count), current_(start)
		{ }

	private:
		_Provider      provider_;
		gnsdk_uint32_t pos_;
		gnsdk_uint32_t count_;
		T              current_;
	};

	template<typename _Itr>
	class gn_iterable_container
	{
	public:
		gn_iterable_container(const _Itr& start, const _Itr& end) : start_(start), end_(end) { }
		~gn_iterable_container() { }
		typedef _Itr             iterator_type;
		typedef gnsdk_uint32_t   difference_type;

		// Interface for gn_iterable_container.
		_Itr            begin() const              { return start_;  }
		_Itr            end() const                { return end_; }
		difference_type count() const              { return start_.distance(end_); }
		_Itr            at(difference_type index)  { return start_ += index;  }
		_Itr operator   [] (difference_type index) { return start_ += index;  }

	private:
		_Itr            start_;
		_Itr            end_;
	};

	class gnstd
	{
	public:
		static bool gn_strtobool(gnsdk_cstr_t value)
		{
			if(value)
			{
				if(value[0] != '0')
				{
					return true;
				}
			}
			return false;
		}

		static gnsdk_int32_t gn_atoi(gnsdk_cstr_t value)
		{
			gnsdk_int32_t val   = 0;
			bool          b_neg = false;

			if(value)
			{
				while( ( *value == ' ' ) || ( *value == '\t' ) )
				{
					value++;
				}

				if(*value == '-')
				{
					b_neg = true;
					++value;
				}
				else if(*value == '+')
				{
					++value;
				}

				while( ( *value >= '0' ) && ( *value <= '9' ) )
				{
					val = val * 10 + ( *value - '0' );
					++value;
				}

				if(b_neg)
				{
					val = -val;
				}
			}
			return val;
		}

		static bool gn_itoa(char* buffer, int buffer_sz, int value)
		{
			int  used    = 1;
			int  current = 0;

			// minimum is a signed char
			bool retval = false;
			char base   = '0';

			// special case
			if( ( value == 0 ) && ( ( used ) < buffer_sz ) )
			{
				buffer[current++] = base;
			}
			if( ( value < 0 ) && ( ( ++used ) < buffer_sz ) )
			{
				buffer[current++] = '-';
				value            *= -1;
			}
			int start_pos = current;

			for(char next_val = 0; value > 0 && used < buffer_sz; ++used, value /= 10)
			{
				next_val          = (char)( value % 10 );
				buffer[current++] = base + next_val;
			}

			// swap string around

			for(int end_pos = current - 1; start_pos < end_pos; start_pos++, end_pos--)
			{
				char temp = buffer[end_pos];
				buffer[end_pos]   = buffer[start_pos];
				buffer[start_pos] = temp;
			}

			retval          = ( 0 == value );
			buffer[current] = '\0';
			return retval;
		}

		static gnsdk_int32_t gn_strcmp(const char* lhs, const char* rhs)
		{
			while(*lhs == *rhs)
			{
				if(*lhs == '\0')
				{
					return 0;
				}
				++lhs;
				++rhs;
			}

			return *lhs - *rhs;
		}

		static gnsdk_size_t gn_strcpy(char* dest, gnsdk_size_t dest_size, const char* src)
		{
			gnsdk_size_t count = 0;

			if(dest && src)
			{
				while( ( count < dest_size ) && ( *dest++ = *src++ ) )
				{
					count++;
				}
			}

			return count;
		}
	};

	// proxy
	namespace utility
	{
		template<typename T>
		struct gnsdk_proxy
		{
			T handle_;
		};

		// Not For Public consumption.
	}
}
#endif // _GNSDK_ITERATOR_BASE_HPP_
