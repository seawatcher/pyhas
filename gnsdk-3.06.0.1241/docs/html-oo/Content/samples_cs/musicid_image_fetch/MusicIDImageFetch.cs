﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using GracenoteSDK;

namespace Sample
{

    /**********************************************
     *    Test Data
     **********************************************/

    /* Simulate MP3 tag data. */
    public class MP3
    {
        public string albumTitleTag;
        public string artistNameTag;
        public string genreTag;
        public MP3(string albumTitle, string artistname, string genre)
        {
            albumTitleTag = albumTitle;
            artistNameTag = artistname;
            genreTag = genre;
        }
    }


    public class MusicIDImageFetch
    {

        private static string localDB = "../../../sample_db";
        /*  Online vs Local queries
         *	Set to false to have the sample perform online queries.
         *  Set to true to have the sample perform local queries.
         *  For local queries, a Gracenote local database must be present.
         */
        private const bool useLocal = false;
        private static GnLookupLocal lookupLocal;
        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";
		
		private static GnUser user = null;

        private static List<MP3> mp3Folder = new List<MP3>();

         #region Initializegnsdk        

       
        /* GnStatusEventsDelegate : overrider methods of this class to get delegate callbacks */
        public class MusicIdEvents : GnStatusEventsDelegate
        {
            public override void status_event(gnsdk_status_t status, uint percentComplete, uint bytesTotalSent, uint bytesTotalReceived)
            {
                Console.Write("\nPerforming MusicID Query ...\t");
                Console.Write("status (");
                switch (status)
                {
                    case gnsdk_status_t.gnsdk_status_unknown:
                        Console.Write(" Unknown ");
                        break;
                    case gnsdk_status_t.gnsdk_status_begin:
                        Console.Write(" Begin ");
                        break;
                    case gnsdk_status_t.gnsdk_status_connecting:
                        Console.Write(" Connecting ");
                        break;
                    case gnsdk_status_t.gnsdk_status_sending:
                        Console.Write(" Sending ");
                        break;
                    case gnsdk_status_t.gnsdk_status_receiving:
                        Console.Write(" Receiving ");
                        break;
                    case gnsdk_status_t.gnsdk_status_disconnected:
                        Console.Write(" Disconnected ");
                        break;
                    case gnsdk_status_t.gnsdk_status_complete:
                        Console.Write(" Complete ");
                        break;
                    default:
                        break;
                }
                Console.WriteLine("\n\t% Complete (" + percentComplete + "),\tTotal Bytes Sent (" + bytesTotalSent + "),\tTotal Bytes Received (" + bytesTotalReceived + ")");
            }
        }

        /*
         *  Display local Gracenote DB information.
         */
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        //Load existing user, or register new one.

        //GNSDK requires a user instance to perform queries. 
        //User encapsulates your Gracenote provided Client ID which is unique for your
        //application. Users  are registered once with Gracenote then must be saved by
        //your application and reused on future invocations.

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string serializedUserString = null;
            string user_filename = clientId + "_user.txt";
            // check file for existence 
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                if (useLocal)
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly, clientId, clientIdTag, applicationVersion).ToString();
                else
                    serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).ToString();

                //Serialize user, so we can use reuse it
                if (!File.Exists(user_filename))
                {
                    using (StreamWriter outfile = new StreamWriter(user_filename))
                    {
                        outfile.Write(serializedUserString);
                        outfile.Close();
                    }
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }
        #endregion

        /***************************************************************************
         *
         *    DoListSearch
         *
         ***************************************************************************/
        static GnListElement
        DoListSearch(string input, GnList list)
        {
            GnListElement listElement = list.ElementByString(input);

            uint level = listElement.Level;
            string value = listElement.DisplayString;

            Console.WriteLine("List element result: " + value + " (level " + level + ")");
            return listElement;
        }

        /***************************************************************************
         *
         *    PerformImageFetch
         *
         * This function performs a text lookup and image fetch
         *
         ***************************************************************************/
        private static void PerformImageFetch(GnAlbum album, string gdoType, GnUser user)
        {
            GnImageSize imageSize = GnImageSize.size_170;
            string availableImageSize = null;
            bool notFound = false;

            if ((album == null) || (gdoType == null))
            {
                Console.WriteLine("Must pass a GDO and type");
                return;
            }

            /* Create the query handle without a callback or callback data (GNSDK_NULL) */
            GnLink link = new GnLink(user, album);

            /* Set preferred image size */
            GnImageSize preferredImageSize = GnImageSize.size_170;

            /* Obtain image size available */
            if (useLocal)
            {
                uint count = lookupLocal.StorageInfoCount(GnLocalStorageName.kContent, GnLocalStorageInfoKey.kImageSize);

                for (uint ordinal = 0; ordinal < count; ordinal++)
                {
                    availableImageSize = lookupLocal.StorageInfo(GnLocalStorageName.kContent, GnLocalStorageInfoKey.kImageSize, ordinal);
                    //TODO
                }
            }
            else
                imageSize = preferredImageSize;


            /* If album type get cover art */
            notFound = FetchImage(link, imageSize, "cover art");

            /* if no cover art, try to get the album's artist image */
            if (notFound == true)
            {
                notFound = FetchImage(link, imageSize, "artist image");

                /* if no artist image, try to get the album's genre image so we have something to display */
                if (notFound == true)
                    notFound = FetchImage(link, imageSize, "genre image");
            }

        }

        /***************************************************************************
         *
         *    FetchImage
         *
         ***************************************************************************/
        private static bool FetchImage(GnLink link, GnImageSize imageSize, string imageType)
        {
            GnLinkContent linkContent = null;
            bool notFound = false;

            try
            {
                /* Perform the image fetch */
                switch (imageType)
                {
                    case "cover art":
                        linkContent = link.CoverArt(imageSize, GnImagePreference.exact);
                        break;
                    case "artist image":
                        linkContent = link.ArtistImage(imageSize, GnImagePreference.exact);
                        break;
                    case "genre image":
                        linkContent = link.GenreArt(imageSize, GnImagePreference.exact);
                        break;
                }


                Console.WriteLine("\nRETRIEVED: " + imageType
                                    + ": "
                                    + linkContent.Data.Length
                                    + " byte JPEG");

                notFound = false;
            }
            catch (GnException gn)
            {
                // Do not return error code for not found. 
                // For image to be fetched, it must exist in the size specified and you must be entitled to fetch images.
                Console.WriteLine("NOT FOUND: " + imageType);
                notFound = true;
            }

            return notFound;
        }

        /***************************************************************************
         *
         *    FindGenreImage
         *
         * This function performs a text lookup of a genre string.
         * If there is a match, the genre image is fetched.
         *
         ***************************************************************************/
        private static int FindGenreImage(GnUser user, string genre, GnList list)
        {
            int gotMatch = 0;
            string availableImageSize;
            GnImageSize imageSize = GnImageSize.size_170;
            GnLink link = null;

            Console.WriteLine("Genre String Search");

            if (genre == null)
            {
                Console.WriteLine("Must pass a genre");
                return -1;
            }

            /* Find the list element for our input string */
            GnListElement listElement = DoListSearch(genre, list);

            if (listElement != null)
            {
                /* we were able to idenfity the input genre string */
                gotMatch = 1;

                /* Create the query handle without a callback or callback data (GNSDK_NULL) */
                link = new GnLink(user, listElement);
            }

            /* Set preferred image size */
            GnImageSize preferredImageSize = GnImageSize.size_170;

            /* Obtain image size available */
            if (useLocal)
            {
                uint count = lookupLocal.StorageInfoCount(GnLocalStorageName.kContent, GnLocalStorageInfoKey.kImageSize);

                for (uint ordinal = 0; ordinal < count; ordinal++)
                {
                    availableImageSize = lookupLocal.StorageInfo(GnLocalStorageName.kContent, GnLocalStorageInfoKey.kImageSize, ordinal);
                    //TODO
                }
            }
            else
                imageSize = preferredImageSize;



            bool notFound = FetchImage(link, imageSize, "genre image");

            return gotMatch;
        }

        /***************************************************************************
         *
         *    DoSampleTextQuery
         *
         * This function performs a text lookup using musicid. If there is a match,
         * the associated image is fetched.
         *
         ***************************************************************************/
        private static int DoSampleTextQuery(string albumTitleTag, string artistNameTag, GnUser user)
        {
            int gotMatch = 0;
            string gdoType = null;

            Console.WriteLine("MusicID Text Match Query");

            if ((albumTitleTag == null) && (artistNameTag == null))
            {
                Console.WriteLine("Must pass album title or artist name");
                return -1;
            }

            if (albumTitleTag == null)
                albumTitleTag = String.Empty;
            else
                Console.WriteLine("album title    : " + albumTitleTag);        /*    show the fields that we have set    */

            if (artistNameTag == null)
                artistNameTag = String.Empty;
            else
                Console.WriteLine("artist name    : " + artistNameTag);

            GnMusicID musicID = new GnMusicID(user);
            GnResponseDataMatches responseDataMatches = musicID.FindMatches(albumTitleTag, String.Empty, artistNameTag);

            uint count = responseDataMatches.DataMatches().count();

            Console.WriteLine("Number matches = " + count);

            /* Get the first match type */
            if (count > 0)
            {
                /* OK, we got at least one match. */
                gotMatch = 1;

                /* Just use the first match for demonstration. */
                GnDataMatch dataMatch = responseDataMatches.DataMatches().at(0).next();


                gdoType = dataMatch.GetType();

                Console.WriteLine("\nFirst Match GDO type: " + gdoType);

                /* Get the best image for the match */
                PerformImageFetch(dataMatch.GetAsAlbum(), gdoType, user);
            }

            return gotMatch;
        }

        /***************************************************************************
        *
        *    ProcessSampleMp3
        *
        ***************************************************************************/
        private static void ProcessSampleMp3(MP3 mp3, GnUser user, GnList list)
        {
            int got_match = 0;

            /* Do a music text query and fetch image from result. */
            got_match = DoSampleTextQuery(mp3.albumTitleTag, mp3.artistNameTag, user);

            /* If there were no results from the musicid query for this file, try looking up the genre tag to get the genre image. */
            if (0 == got_match)
            {
                if (null != mp3.genreTag)
                {
                    got_match = FindGenreImage(user, mp3.genreTag, list);
                }
            }

            /* did we succesfully find a relevant image? */
            if (0 == got_match)
            {
                Console.Write("Because there was no match result for any of the input tags, you may want to associated the generic music image with this track, music_75x75.jpg, music_170x170.jpg or music_300x300.jpg\n");
            }
        }

        private static void PopulateData()
        {

            /* Simulate a folder of MP3s.
             *
             * file 1: Online - At the time of writing, yields an album match but no cover art or artist images are available.
             *                  Fetches the genre image for the returned album's genre.
             *         Local - Yields no match for the query of "Low Commotion Blues Band" against the sample database.
             *                 Uses the genre tag from the file, "Rock", to perform a genre search and fetches the genre image from that.
             * file 2: Online - Yields an album match for album tag "Supernatural". Fetches the cover art for that album.
             *         Local - Same result.
             * file 3: Online - Yields an album matches for Phillip Glass. Fetches the cover art for the first album returned.
             *         Local - Yields an artist match for the artist tag "Phillip Glass". Fetches the artist image for Phillip Glass.
             */
            mp3Folder.Add(new MP3("Ask Me No Questions", "Low Commotion Blues Band", "Rock"));
            mp3Folder.Add(new MP3("Supernatural", "Santana", null));
            mp3Folder.Add(new MP3(null, "Phillip Glass", null));

        }

        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }

            /* GNSDK initialization */
            try
            {
                // Initialize SDK
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                // Dispaly SDK version
                Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                // Enable logging
                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */

                /*         
                 *    Load existing user handle, or register new one.                    
                 */
                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

				GnStorageSqlite gnStorage = new GnStorageSqlite();
                
				if (useLocal)
                {
                    gnStorage.StorageFolder = localDB;

                    lookupLocal = new GnLookupLocal();

                    user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                    DisplayEmbeddedDbInfo(lookupLocal);

                }
                else
                {
                    user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                }

                /* Set locale with desired Group, Language, Region and Descriptor
                * Set the 'locale' to return locale-specific results values. This examples loads an English locale.
                */
				 GnLocale locale = new GnLocale( GnSDK.kLocaleGroupMusic,           /* Locale group */
												GnSDK.kLanguageEnglish,             /* Language */
												GnSDK.kRegionGlobal,                /* Region */
												GnSDK.kDescriptorDefault,           /* Descriptor */
												user,                               /* User */
												null /*localeEvents*/               /* locale Events object */
												);
				
				gnsdk.SetDefaultLocale(locale);


                PopulateData();
                /* Get the genre list handle. This will come from our pre-loaded locale.
                 Be sure these params match the ones used in get_locale to avoid loading a different list into memory.

                 NB: If you plan to do many genre searches, it is most efficient to get this handle once during
                 initialization (after setting your locale) and then only release it before shutdown.
                 */

                GnList list = new GnList(GnSDK.kListTypeGenres, GnSDK.kLanguageEnglish, GnSDK.kRegionDefault, GnSDK.kDescriptorSimplified, user);

                /* Simulate iterating a sample of mp3s. */
                for (int fileIndex = 0; fileIndex < mp3Folder.Count; fileIndex++)
                {
                    Console.Write("\n\n***** Processing File " + (fileIndex) + " *****\n");
                    ProcessSampleMp3(mp3Folder[fileIndex], user, list);
                }
            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }
    }
}
