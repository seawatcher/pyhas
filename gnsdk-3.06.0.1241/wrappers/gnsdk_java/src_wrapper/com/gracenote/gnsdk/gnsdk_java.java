/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class gnsdk_java implements gnsdk_javaConstants {
  public static long gnsdkMusicidstreamInitialize(SWIGTYPE_p_gnsdk_manager_handle_t sdkmgr_handle) {
    return gnsdk_javaJNI.gnsdkMusicidstreamInitialize(SWIGTYPE_p_gnsdk_manager_handle_t.getCPtr(sdkmgr_handle));
  }

  public static long gnsdkMusicidstreamShutdown() {
    return gnsdk_javaJNI.gnsdkMusicidstreamShutdown();
  }

  public static String gnsdkMusicidstreamGetVersion() {
    return gnsdk_javaJNI.gnsdkMusicidstreamGetVersion();
  }

  public static String gnsdkMusicidstreamGetBuildDate() {
    return gnsdk_javaJNI.gnsdkMusicidstreamGetBuildDate();
  }

  public static long gnsdkMusicidstreamChannelCreate(SWIGTYPE_p_gnsdk_user_handle_t user_handle, gnsdk_musicidstream_callbacks_t p_callbacks, SWIGTYPE_p_void callback_data, SWIGTYPE_p_p_void p_musicidstream_channel_handle) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelCreate(SWIGTYPE_p_gnsdk_user_handle_t.getCPtr(user_handle), gnsdk_musicidstream_callbacks_t.getCPtr(p_callbacks), p_callbacks, SWIGTYPE_p_void.getCPtr(callback_data), SWIGTYPE_p_p_void.getCPtr(p_musicidstream_channel_handle));
  }

  public static long gnsdkMusicidstreamChannelRelease(SWIGTYPE_p_void musicidstream_channel_handle) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelRelease(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle));
  }

  public static long gnsdkMusicidstreamChannelOptionSet(SWIGTYPE_p_void musicidstream_channel_handle, String option_key, String option_value) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelOptionSet(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle), option_key, option_value);
  }

  public static long gnsdkMusicidstreamChannelOptionGet(SWIGTYPE_p_void musicidstream_channel_handle, String option_key, SWIGTYPE_p_p_char p_option_value) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelOptionGet(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle), option_key, SWIGTYPE_p_p_char.getCPtr(p_option_value));
  }

  public static long gnsdkMusicidstreamChannelAudioWriteBegin(SWIGTYPE_p_void musicidstream_channel_handle, long audio_sample_rate, long audio_sample_size, long audio_channels) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelAudioWriteBegin(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle), audio_sample_rate, audio_sample_size, audio_channels);
  }

  public static long gnsdkMusicidstreamChannelAudioWrite(SWIGTYPE_p_void musicidstream_channel_handle, SWIGTYPE_p_unsigned_char p_audio_data, long audio_data_length) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelAudioWrite(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle), SWIGTYPE_p_unsigned_char.getCPtr(p_audio_data), audio_data_length);
  }

  public static long gnsdkMusicidstreamChannelAudioWriteEnd(SWIGTYPE_p_void musicidstream_channel_handle) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelAudioWriteEnd(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle));
  }

  public static long gnsdkMusicidstreamChannelAudioIdentify(SWIGTYPE_p_void musicidstream_channel_handle) {
    return gnsdk_javaJNI.gnsdkMusicidstreamChannelAudioIdentify(SWIGTYPE_p_void.getCPtr(musicidstream_channel_handle));
  }

  public static long gnsdkHandleAddref(SWIGTYPE_p_void handle) {
    return gnsdk_javaJNI.gnsdkHandleAddref(SWIGTYPE_p_void.getCPtr(handle));
  }

  public static long gnsdkHandleRelease(SWIGTYPE_p_void handle) {
    return gnsdk_javaJNI.gnsdkHandleRelease(SWIGTYPE_p_void.getCPtr(handle));
  }

  public static int getGnMidfQueryReturnSingle() {
    return gnsdk_javaJNI.GnMidfQueryReturnSingle_get();
  }

  public static int getGnMidfQueryReturnAll() {
    return gnsdk_javaJNI.GnMidfQueryReturnAll_get();
  }

  public static int getGnMidfQueryResponseAlbums() {
    return gnsdk_javaJNI.GnMidfQueryResponseAlbums_get();
  }

  public static int getGnMidfQueryResponseMatches() {
    return gnsdk_javaJNI.GnMidfQueryResponseMatches_get();
  }

  public static int getGnMidfQueryRunAsynchronously() {
    return gnsdk_javaJNI.GnMidfQueryRunAsynchronously_get();
  }

  public static int getGnMidfQueryAggressiveSearch() {
    return gnsdk_javaJNI.GnMidfQueryAggressiveSearch_get();
  }

  public static int getGnMidfQueryNoThreads() {
    return gnsdk_javaJNI.GnMidfQueryNoThreads_get();
  }

  public static int getGnMidfQueryDefault() {
    return gnsdk_javaJNI.GnMidfQueryDefault_get();
  }

}
