var class_gracenote_s_d_k_1_1_gn_playlist_metadata =
[
    [ "GnPlaylistMetadata", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#a7f8ce3efbc39eac1c0f8098f7d0be26a", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#af2123d0777ad1874a9166811e0e2df46", null ],
    [ "AlbumName", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#aa0ec33c946e3cbf628a0d788e033b788", null ],
    [ "ArtistName", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#a209322e8dc68d6ba5d0b7ce3f33102b7", null ],
    [ "ArtistType", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#aa36cb5158aa99a7e3fbcb87b74608247", null ],
    [ "Era", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#ab9c46381be0f42dbfa3d5b6114c3fe35", null ],
    [ "Genre", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#aeab90b34665dd98e98b9be65ea217dcf", null ],
    [ "Mood", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#a1df3ae72b8d48fcc6aefdaffa205ecb1", null ],
    [ "Origin", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#a4fa957674c1071c1515e09c8c7728078", null ],
    [ "Tempo", "class_gracenote_s_d_k_1_1_gn_playlist_metadata.html#af15d297ada88e18d4584861c70718a80", null ]
];