group_name = sample
appname = sample

build_dir = ../../../../../builds
install_dir = ../../../../../install/common

ifeq ($(extra_libs),../../xtralibs/ipp)
ifeq "$(wildcard ../../builds )" ""
extra_libs = ../../../../xtralibs/ipp
endif
endif

# Code samples
ifneq "$(wildcard ../../builds )" ""
build_dir = ../../builds
install_dir = ../../..
endif

# Code snippets
ifneq "$(wildcard ../../../builds )" ""
build_dir = ../../../builds
install_dir = ../../../..
else ifeq ($(snippet),1)
build_dir = ../../../../../../builds
install_dir = ../../../../../../install/common
endif

ifeq ($(IMPLDIR),linux)
platformlibs = -lstdc++
endif

ifeq ($(IMPLDIR),qnx)
platformlibs = -lstdc++
endif

