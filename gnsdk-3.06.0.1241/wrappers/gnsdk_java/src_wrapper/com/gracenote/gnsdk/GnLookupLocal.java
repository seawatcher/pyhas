/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.6
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gracenote.gnsdk;

public class GnLookupLocal extends GnObject {
  private long swigCPtr;

  protected GnLookupLocal(long cPtr, boolean cMemoryOwn) {
    super(gnsdk_javaJNI.GnLookupLocal_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GnLookupLocal obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        gnsdk_javaJNI.delete_GnLookupLocal(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GnLookupLocal() throws com.gracenote.gnsdk.GnException {
    this(gnsdk_javaJNI.new_GnLookupLocal(), true);
  }

  public String version() {
    return gnsdk_javaJNI.GnLookupLocal_version(swigCPtr, this);
  }

  public String buildDate() {
    return gnsdk_javaJNI.GnLookupLocal_buildDate(swigCPtr, this);
  }

  public void storageCompact(GnLocalStorageName storageName) throws com.gracenote.gnsdk.GnException {
    gnsdk_javaJNI.GnLookupLocal_storageCompact(swigCPtr, this, storageName.swigValue());
  }

  public void storageLocation(GnLocalStorageName storageName, String storageLocation) throws com.gracenote.gnsdk.GnException {
    gnsdk_javaJNI.GnLookupLocal_storageLocation(swigCPtr, this, storageName.swigValue(), storageLocation);
  }

  public void storageValidate(GnLocalStorageName storageName) throws com.gracenote.gnsdk.GnException {
    gnsdk_javaJNI.GnLookupLocal_storageValidate(swigCPtr, this, storageName.swigValue());
  }

  public String storageInfo(GnLocalStorageName storageName, GnLocalStorageInfoKey storageInfoKey, long ordinal) throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLookupLocal_storageInfo(swigCPtr, this, storageName.swigValue(), storageInfoKey.swigValue(), ordinal);
  }

  public long storageInfoCount(GnLocalStorageName storageName, GnLocalStorageInfoKey storageInfoKey) throws com.gracenote.gnsdk.GnException {
    return gnsdk_javaJNI.GnLookupLocal_storageInfoCount(swigCPtr, this, storageName.swigValue(), storageInfoKey.swigValue());
  }

}
