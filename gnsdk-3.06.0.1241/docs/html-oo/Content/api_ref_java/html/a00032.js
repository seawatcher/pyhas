var a00032 =
[
    [ "swigValue", "a00032.html#a83315986c652af377142ab8a3227b290", null ],
    [ "kDspFeatureTypeAFX3", "a00032.html#a01c695a5fa167f2d937b44bbe20a10be", null ],
    [ "kDspFeatureTypeCantametrixQ", "a00032.html#a149ac5b2641a4f12da38470c31adb46e", null ],
    [ "kDspFeatureTypeCantametrixR", "a00032.html#ab1b011bc812adbe553ee2ff14282e3c6", null ],
    [ "kDspFeatureTypeChroma", "a00032.html#aac13b125ea8aaa5dbd90d9db38c87b08", null ],
    [ "kDspFeatureTypeFAPIQ3sLQ", "a00032.html#ada2a4c6e203b9409a6dca0dbb10f0ce4", null ],
    [ "kDspFeatureTypeFAPIQ3sMQ", "a00032.html#a62ce74a538f03f213668876d60f9ef70", null ],
    [ "kDspFeatureTypeFAPIQ6sMQ", "a00032.html#a960a7403458053f9ff59bc4bbfd93fcd", null ],
    [ "kDspFeatureTypeFAPIR", "a00032.html#acc9df49368714b05e28d75510463fd95", null ],
    [ "kDspFeatureTypeFEXModule", "a00032.html#a201c3e11209904759cc073b9e51b25de", null ],
    [ "kDspFeatureTypeFraunhofer", "a00032.html#a96f7435f3fb15dca39f8eff6a3b54524", null ],
    [ "kDspFeatureTypeMicroFAPIQ", "a00032.html#a4b16fc5a99f741662dfa84c498f2e498", null ],
    [ "kDspFeatureTypeNanoFAPIQ", "a00032.html#a40d6a71df5e1f87106dd3247a45917e0", null ]
];