# Copyright (c) 2000-present Gracenote.
#
# This software may not be used in any way or distributed without
# permission. All rights reserved.
#
# Some code herein may be covered by US and international patents.

"""
Name: playlist
Description:
  Playlist is the GNSDK library that generates playlists when integrated with the
  MusicID or MusicID-File library (or both). Playlist APIs enable an application to:
    01. Create, administer, populate, and synchronize a collection summary.
    02. Store a collection summary within a local storage solution.
    03. Validate PDL statements.
    04. Generate Playlists using either the More Like This function or the general
        playlist generation function.
    05. Manage results.
  Streamline your Playlist implementation by using the provided More Like This
  function, gnsdk_playlist_generate_morelikethis, which generates optimal playlists
  and eliminates the need to create and validate Playlist Definition Language
  statements.

Command line syntax:
> python main.py <client_id> <client_id_tag> <license_filename>

"""

import os
import sys
sys.path.append("..")

# gnsdk_utilities finds gnsdk and sets up sys.path. Import first.
import gnsdk_utilities
import gnsdk

application_version = "1.0.0.0"

def get_build_info(name, gnsdk_lib):
    return name.ljust(20, '.') + \
        " version: " + gnsdk_lib.version() + \
        " builddate: " + gnsdk_lib.build_date()

class GNSDK_Manager:

    def __init__(self, client_id, client_id_tag, license_path):
        try:
            self.__manager = gnsdk.GnSDK(
                gnsdk_utilities.lib_path,
                license_path,
                gnsdk.GnSDK.kFilename
            )
        except gnsdk.GnError as e:
            print "Error creating GnSDK instance: %s" % e.error_description()
            exit(1)

        if configuration.enable_logging:
            self.__manager.logging_enable(
                "sample.log",
                self.__manager.gn_log_pkg_all,
                self.__manager.gn_log_level_error|self.__manager.gn_log_level_warning,
                self.__manager.gn_log_option_all,
                0,                                                         # Max size
                False                                                      # save logs
            )

        try:
            self.__storage = gnsdk.GnStorageSqlite()
        except gnsdk.GnError as e:
            print "Error creating GnStorageSqlite instance: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            self.__storage.storage_folder_set(configuration.storage_path)
            try:
                self.lookup_local = gnsdk.GnLookupLocal()
            except gnsdk.GnError as e:
                print "Error creating GnLookupLocal instance: %s" % e.error_description()
                exit(1)

        # Get the user, registering if necessary.
        self.user = self.get_user(client_id, client_id_tag, application_version)

        # Set the locale.
        try:
            locale = gnsdk.GnLocale(
                self.__manager.k_locale_group_playlist,
                self.__manager.k_language_english,
                self.__manager.k_region_default,
                self.__manager.k_descriptor_simplified,
                self.user
            )
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

        try:
            self.__manager.set_default_locale(locale)
        except gnsdk.GnError as e:
            print "Error setting locale: %s" % e.error_description()
            exit(1)

    def get_user(self, client_id, client_id_tag, application_version):
        user_filename = "user.txt"
        try:
            user_file = open(user_filename, "r")
        except:
            print "\n%s not found: Welcome new user!\n" % user_filename
            if configuration.use_local:
                reg_mode = self.__manager.kUserRegModeLocalOnly
            else:
                reg_mode = self.__manager.kUserRegModeOnline
            try:
                serialized_user = self.__manager.register_user(
                    reg_mode,
                    client_id,
                    client_id_tag,
                    application_version
                ).c_str()
            except gnsdk.GnError as e:
                print "Error creating user: %s" % e.error_description()
                exit(1)
            open(user_filename, "w").write(serialized_user)
        else:
            print "\nLoading user from %s: Welcome back!\n" % user_filename
            serialized_user = user_file.read()
        try:
            user = gnsdk.GnUser(
                serialized_user,
                client_id,
                client_id_tag,
                application_version
            )
        except gnsdk.GnError as e:
            print "Error creating user: %s" % e.error_description()
            exit(1)

        if configuration.use_local:
            user.option_lookup_mode(gnsdk.kLookupModeLocal)

        return user

    def display_build_info(self):
        # Print all of the version information.
        print get_build_info("manager", self.__manager)
        print get_build_info("storage SQLite", self.__storage)
        if configuration.use_local:
            print get_build_info("lookup_local", self.lookup_local)
            print "gdb version: %s" % self.get_gdb_version()

    def get_gdb_version(self):
        record_count = self.lookup_local.storage_info_count(
            gnsdk.kMetadata,
            gnsdk.kGDBVersion
        )
        for i in xrange(record_count):
            version = self.lookup_local.storage_info(
                gnsdk.kMetadata,
                gnsdk.kGDBVersion,
                i+1
            )
        return version

if __name__ == "__main__":
    def usage(program):
        print "USAGE!!:\n\t%s <client_id> <client_id_tag> <license_path>\n" % program
        exit(1)

    if len(sys.argv) != 4:
        usage(sys.argv[0])

    # configuration instance
    configuration = gnsdk_utilities.GNSDK_Configuration()
    print "\nConfiguration:\n" + str(configuration)

    # Create the manager
    manager = GNSDK_Manager(sys.argv[1], sys.argv[2], sys.argv[3])
    manager.display_build_info()

    print "\n*****Playlist Collection creation and Playlist Generation*****\n"

    collection_summary_name = "sample_collection"

    def create_playlist_collection():
        input_query_tocs = [
            "150 13224 54343 71791 91348 103567 116709 132142 141174 157219 175674 197098 238987 257905",
            "182 23637 47507 63692 79615 98742 117937 133712 151660 170112 189281",
            "182 14035 25710 40955 55975 71650 85445 99680 115902 129747 144332 156122 170507",
            "150 10705 19417 30005 40877 50745 62252 72627 84955 99245 109657 119062 131692 141827 152207 164085 173597 187090 204152 219687 229957 261790 276195 289657 303247 322635 339947 356272",
            "150 14112 25007 41402 54705 69572 87335 98945 112902 131902 144055 157985 176900 189260 203342",
            "150 1307 15551 31744 45022 57486 72947 85253 100214 115073 128384 141948 152951 167014",
            "183 69633 96258 149208 174783 213408 317508",
            "150 19831 36808 56383 70533 87138 105157 121415 135112 151619 169903 189073",
            "182 10970 29265 38470 59517 74487 83422 100987 113777 137640 150052 162445 173390 196295 221582",
            "150 52977 87922 128260 167245 187902 215777 248265",
            "183 40758 66708 69893 75408 78598 82983 87633 91608 98690 103233 108950 111640 117633 124343 126883 132298 138783 144708 152358 175233 189408 201408 214758 239808",
            "150 92100 135622 183410 251160 293700 334140",
            "150 17710 33797 65680 86977 116362 150932 166355 183640 193035",
            "150 26235 51960 73111 93906 115911 142086 161361 185586 205986 227820 249300 277275 333000",
            "150 1032 27551 53742 75281 96399 118691 145295 165029 189661 210477 232501 254342 282525",
            "150 26650 52737 74200 95325 117675 144287 163975 188650 209350 231300 253137 281525 337875",
            "150 19335 35855 59943 78183 96553 111115 125647 145635 163062 188810 214233 223010 241800 271197",
            "150 17942 32115 47037 63500 79055 96837 117772 131940 148382 163417 181167 201745",
            "150 17820 29895 41775 52915 69407 93767 105292 137857 161617 171547 182482 204637 239630 250692 282942 299695 311092 319080",
            "182 21995 45882 53607 71945 80495 94445 119270 141845 166445 174432 187295 210395 230270 240057 255770 277745 305382 318020 335795 356120",
            "187 34360 64007 81050 122800 157925 195707 230030 255537 279212 291562 301852 310601",
            "150 72403 124298 165585 226668 260273 291185"
        ]

        # Create a musicID instance to lookup the TOCs
        music_id = gnsdk.GnMusicID(manager.user)
        print get_build_info("music id", music_id)
        music_id.option_result_single(True)
        music_id.option_lookup_data(gnsdk.kLookupDataSonicData, True)
        music_id.option_lookup_data(gnsdk.kLookupDataPlaylist, True)

        print "Creating a new collection"
        playlist_collection = playlist.create_collection(collection_summary_name)
        for toc_idx, toc in enumerate(input_query_tocs):
            response = music_id.find_albums(toc)
            albums = gnsdk_utilities.GDO_Iterable(response.albums())
            for album in albums:
                tracks = gnsdk_utilities.GDO_Iterable(album.tracks())
                for trk_idx, track in enumerate(tracks):
                    playlist_collection.add("%d_%d"%(toc_idx, trk_idx), album)
                    playlist_collection.add("%d_%d"%(toc_idx, trk_idx), track)

        playlist.store_collection(playlist_collection)
        print "Finished populating collection summary"

    def get_playlist_collection():
        collection_count = playlist.stored_collections_count()
        print "Currently stored collections: %d" % collection_count
        collection_name = playlist.stored_collection_name_at(0)
        collection_name = collection_summary_name
        playlist_collection = playlist.load_collection(collection_name)
        print "Loaded Collection '%s' from storage" % collection_name
        return playlist_collection

    def get_seed(playlist_collection):
        ident_iter = gnsdk_utilities.GnSDK_Iterable(
            playlist_collection.media_identifiers()
        )
        ident = ident_iter[4]
        return playlist_collection.metadata(manager.user, ident)

    def enumerate_playlist_result(result):
        result_iterator = gnsdk_utilities.GnSDK_Iterable(result.identifiers())
        print "    Generated playlist: %d tracks" % len(result_iterator)
        for identifier in result_iterator:
            data = playlist_collection.metadata(manager.user, identifier)
            print "      %s : %s: %s, %s, %s, %s, %s, %s, %s, %s" % (
                identifier.collection_name(),
                identifier.media_identifier(),
                data.album_name(),
                data.artist_name(),
                data.artist_type(),
                data.era(),
                data.genre(),
                data.origin(),
                data.mood(),
                data.tempo()
            )

    def do_playlist_pdl(playlist_collection):
        print "\nGenerating PDL playlists"
        pdl_statements = [
            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 0",
            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2929) > 300",
            "GENERATE PLAYLIST WHERE GN_Genre = 2929",
            "GENERATE PLAYLIST WHERE GN_Genre = 2821",
            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 0",
            "GENERATE PLAYLIST WHERE (GN_Genre LIKE 2821) > 300",
            "GENERATE PLAYLIST WHERE (GN_Genre LIKE SEED) > 300 LIMIT 20 RESULTS",
            "GENERATE PLAYLIST WHERE (GN_ArtistName LIKE 'Green Day') > 300 LIMIT 20 RESULTS, 2 PER GN_ArtistName;"
        ]
        for idx, pdl_statement in enumerate(pdl_statements):
            print "\n  PDL statement %d: %s" % (idx, pdl_statement)
            seed = get_seed(playlist_collection)
            playlist_result = playlist_collection.generate_playlist(
                manager.user,
                pdl_statement,
                seed
            )
            enumerate_playlist_result(playlist_result)

    def display_mlt_options(playlist_collection, mlt_options):
        for mlt_option in mlt_options:
            value = playlist_collection.more_like_this_option_get(mlt_option[1])
            print "  mlt option %s = %s" % (mlt_option[0], value)

    def gen_mlt(playlist_collection, seed):
        playlist_result = playlist_collection.generate_more_like_this(manager.user, seed)
        enumerate_playlist_result(playlist_result)
        
    def do_playlist_mlt(playlist_collection):

        mlt_options = [("max tracks",     playlist_collection.kMoreLikeThisMaxTracks,    30),
                       ("max per artist", playlist_collection.kMoreLikeThisMaxPerArtist, 10),
                       ("max per album",  playlist_collection.kMoreLikeThisMaxPerAlbum,   5),
                       ("random",         playlist_collection.kMoreLikeThisRandom,        1)
                      ]

        display_mlt_options(playlist_collection, mlt_options)

        seed = get_seed(playlist_collection)
        print "\nGenerating MoreLikeThis playlist"

        print "\n  MoreLikeThis with Default Options"
        gen_mlt(playlist_collection, seed)

        print "\n  MoreLikeThis with Custom Options"
        for mlt_option in mlt_options:
            playlist_collection.more_like_this_option_set(mlt_option[1], mlt_option[2])
        gen_mlt(playlist_collection, seed)

    # Create a playlist instance
    playlist = gnsdk.GnPlaylist()
    print get_build_info("playlist", playlist)
    playlist.set_storage_location(os.getcwd())

    # Get the collection summary, create if necessary
    if not playlist.stored_collections_count():
        create_playlist_collection()
    playlist_collection = get_playlist_collection()

    # Do a PDL based playlist
    do_playlist_pdl(playlist_collection)

    # Do a MLT playlist
    do_playlist_mlt(playlist_collection)
