var a00050 =
[
    [ "swigValue", "a00050.html#ae67e8afb6694a439269af314dfe1287f", null ],
    [ "gnsdk_link_content_artist_news", "a00050.html#a18b35f818757d6e5343a6e66514798a9", null ],
    [ "gnsdk_link_content_biography", "a00050.html#aaed6125e7219d8e09ab04a3ae397a895", null ],
    [ "gnsdk_link_content_comments_listener", "a00050.html#abd0c30c522d9ee160d2926c0bf293146", null ],
    [ "gnsdk_link_content_comments_release", "a00050.html#a597316dddf19765d6922a3f1db694cfa", null ],
    [ "gnsdk_link_content_cover_art", "a00050.html#a3abe6a115f1dbed45ec9ac5cef7b1c98", null ],
    [ "gnsdk_link_content_dsp_data", "a00050.html#a3a3c77eb926a562f660fab22d836c488", null ],
    [ "gnsdk_link_content_genre_art", "a00050.html#a222d5868532227ba9e0fa519830f2e0f", null ],
    [ "gnsdk_link_content_image", "a00050.html#ae25bfc00a0226bdb8d8558eeb8d877af", null ],
    [ "gnsdk_link_content_image_artist", "a00050.html#acc0b8863687ca5058849d997736e5e8b", null ],
    [ "gnsdk_link_content_lyric_text", "a00050.html#a22af2897017f43d97ed501250297aa65", null ],
    [ "gnsdk_link_content_lyric_xml", "a00050.html#abfbb62d1ef957f2ef2a84a89e9f7078f", null ],
    [ "gnsdk_link_content_news", "a00050.html#a3d442959ffa3d02bc15aec5bef861909", null ],
    [ "gnsdk_link_content_review", "a00050.html#a88a498e529655e49582804fb147ff965", null ],
    [ "gnsdk_link_content_unknown", "a00050.html#ade92d01c511e6181a8ddc60e2650cc15", null ]
];