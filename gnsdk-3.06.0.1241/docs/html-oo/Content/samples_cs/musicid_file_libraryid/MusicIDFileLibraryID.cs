/*
 * Copyright (c) 2000-2013 Gracenote.
 * 
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 * 
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: MusicIDFileLibraryID.cs (MusicID-File LibraryID sample appilcation)
 *  Description:
 *  LibraryID processing adds another level of processing above AlbumID for very large collections of media files.   
 *  LibraryID extends AlbumID functionality by  performing additional scanning and processing of all the files in 
 *  an entire collection. This enables LibraryID to find groupings that are not captured by AlbumID processing.
 *  This method is highly recommended for use when there are a large number (hundreds to thousands) of files to 
 *  identify, though it is also equally effective when processing only a few files. This method takes most of  
 *  the guesswork out of MusicID-File and lets the library do all the work for the application. 
 *  The GnMusicIDFile::DoAlbumID method provides LibraryID processing.
 *
 *  Command-line Syntax:
 *  sample clientId clientIdTag license gnsdkLibraryPath
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GracenoteSDK;
using System.Diagnostics;

namespace Sample
{
    public class MusicIDFileLibraryID
    {
        private static string folderPath = @"..\..\..\";
        private static string localDB = "../../../sample_db";

        /*  Online vs Local queries
         *	Set to false to have the sample perform online queries.
         *  Set to true to have the sample perform local queries.
         *  For local queries, a Gracenote local database must be present.
         */
        private const bool useLocal = false;

        private static string licenseFile = null;
        private static string clientId = null;
        private static string clientIdTag = null;
        private static string gnsdkLibraryPath = null;
        private static string applicationVersion = "1.0.0.0";
        private static GnUser user = null;
        
        #region Initializegnsdk

        /*
        * Callback delegate classes
        */

        /* Callback delegate called when performing MusicID-File operation */
        public class MusicIDFileEvents : GnMusicIDFileEventsDelegate
        {
            public override void status(GnMusicIDFileInfo fileinfo, gnsdk_musicidfile_callback_status_t status, uint currentFile, uint totalFiles)
            {
                switch (status)
                {
                    case gnsdk_musicidfile_callback_status_t.gnsdk_musicidfile_status_fileinfo_processing_begin:
                        Console.WriteLine("\nMID-File Status: " + currentFile + " of " + totalFiles + " - fileinfo_processing_begin - " + fileinfo.Identifier);
                        break;
                    case gnsdk_musicidfile_callback_status_t.gnsdk_musicidfile_status_fileinfo_query:
                        Console.WriteLine("\nMID-File Status: " + currentFile + " of " + totalFiles + " - fileinfo_query - " + fileinfo.Identifier);
                        break;
                    case gnsdk_musicidfile_callback_status_t.gnsdk_musicidfile_status_fileinfo_processing_complete:
                        Console.WriteLine("\nMID-File Status: " + currentFile + " of " + totalFiles + " - fileinfo_processing_complete - " + fileinfo.Identifier);
                        break;
                }
            }

            public override bool cancel_check()
            {
                return false;
            }
            public override void result_available(GnResponseAlbums album_result, uint current_album, uint total_albums)
            {
                Console.WriteLine("\nMID-File Result: ");
                Console.WriteLine("\tAlbum count: " + album_result.Albums.count());
                DisplayAlbums(album_result);
            }
        }

        /*
         *  Display local Gracenote DB information.
         */
        private static void DisplayEmbeddedDbInfo(GnLookupLocal gnLookupLocal)
        {
            uint ordinal = gnLookupLocal.StorageInfoCount(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion);
            string versionResult = gnLookupLocal.StorageInfo(GnLocalStorageName.kMetadata, GnLocalStorageInfoKey.kGDBVersion, ordinal);
            Console.WriteLine("GracenoteDB Source DB ID : " + versionResult);
        }

        static GnUser GetUser(GnSDK gnsdk, string clientId, string clientIdTag, string applicationVersion)
        {
            string user_filename = clientId + "_user.txt";
            string serializedUserString;

            /* check file for existence  */
            if (!File.Exists(user_filename))
            {
                Console.WriteLine("\nInfo: No stored user - this must be the app's first run.");

                serializedUserString = gnsdk.RegisterUser(GnSDK.GnUserRegisterMode.kUserRegModeOnline, clientId, clientIdTag, applicationVersion).c_str();
                using (StreamWriter outfile = new StreamWriter(user_filename))
                {
                    outfile.Write(serializedUserString);
                    outfile.Close();
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(user_filename))
                {
                    serializedUserString = sr.ReadToEnd();
                }
            }
            return new GnUser(serializedUserString, clientId, clientIdTag, applicationVersion);
        }

       

        #endregion
        
        public static void DisplayAlbums(GnResponseAlbums response)
        {
            int match = 0;
            GnAlbumEnumerable albumEnumerable = response.Albums;
            GnAlbumEnumerator albumEnumerator = albumEnumerable.GetEnumerator();
            while (albumEnumerator.hasNext())
            {
                GnAlbum album = albumEnumerator.Current;
                GnTitle albumTitle = album.Title;
                Console.WriteLine("\tMatch " + ++match + " - Album title:\t\t" + albumTitle.Display);

            }

        }

        private static void setMetadata(GnMusicIDFileInfo fileinfo)
        {
            try
            {
                String identifier = fileinfo.Identifier;

                /*
	             * A typical use for this callback is to read file tags (ID3, etc) for the basic
	             * metadata of the track.  To keep the sample code simple, we went with .wav files
	             * and hardcoded in metadata for just one of the sample tracks.  (MYAPP_SAMPLE_FILE_5)
	             */

                /* So, if this isn't the correct sample track, return.*/
                if (!identifier.Contains("kardinal_offishall_01_3s.wav"))
                {
                    return;
                }
                fileinfo.AlbumArtist = "kardinal offishall";
                fileinfo.AlbumTitle = "quest for fire";
                fileinfo.TrackTitle = "intro";
            }
            catch (GnException ex)
            {
                Console.WriteLine("Error while setting metadata setMetadata()" + ex.Message);
            }
        }

        private static void setFingerprint(GnMusicIDFileInfo fileInfo)
        {
            FileStream fileStream = null;
            try
            {
                bool complete = false;
                byte[] audioData = new byte[2048];
                int numRead = 0;

                string filename = fileInfo.FileName.ToString();

                fileStream = new FileStream(folderPath + filename, FileMode.Open, FileAccess.Read);

                // check file for existence
                if (fileStream == null || !fileStream.CanRead)
                {
                    Console.WriteLine("\n\nError: Failed to open input file: " + filename);
                }
                else
                {
                    /* skip the wave header (first 44 bytes). we know the format of our sample files, but please
		                be aware that many wav file headers are larger then 44 bytes! */
                    if (44 != fileStream.Seek(44, SeekOrigin.Begin))
                    {
                        Console.WriteLine("\n\nError: Failed to seek past header: %s\n", filename);
                    }
                    else
                    {
                        /* initialize the fingerprinter
                            Note: Our sample files are non-standard 11025 Hz 16-bit mono to save on file size */
                        fileInfo.FingerprintBegin(11025, 16, 1);

                        numRead = fileStream.Read(audioData, 0, 2048);
                        while ((numRead) > 0)
                        {
                            /* write audio to the fingerprinter */
                            complete = fileInfo.FingerprintWrite(audioData, Convert.ToUInt32(numRead));

                            /* does the fingerprinter have enough audio? */
                            if (complete)
                            {
                                break;
                            }

                            numRead = fileStream.Read(audioData, 0, 2048);
                        }
                        fileStream.Close();

                        /* signal that we are done */
                        fileInfo.FingerprintEnd();
                        Debug.WriteLine("Fingerprint: " + fileInfo.Fingerprint + " File: " + fileInfo.FileName);
                    }
                }
                if (!complete)
                {
                    /* Fingerprinter doesn't have enough data to generate a fingerprint.
			               Note that the sample data does include one track that is too short to fingerprint. */
                    Console.WriteLine("Warning: input file does not contain enough data to generate a fingerprint:\n" + filename);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("FileNotFoundException " + e.Message);
            }
            catch (IOException e)
            {
                Console.WriteLine("IOException " + e.Message);
            }
            finally
            {
                try
                {
                    fileStream.Close();
                }
                catch (IOException e)
                {
                    Console.WriteLine("IOException " + e.Message);
                }
            }
        }

        static void AddFile(GnMusicIDFile midf, string filePath)
        {
            GnMusicIDFileInfo fileinfo = midf.CreateFileInfo(filePath);

            /* Set the file path in the fileinfo */
            fileinfo.FileName = filePath;

            /*Set fingerprint and metadata*/
            setFingerprint(fileinfo);
            setMetadata(fileinfo);
        }

        static void SetQueryData(GnMusicIDFile midf)
        {
            List<string> filenames = new List<string>();

            filenames.Add(@"data\01_stone_roses.wav");
            filenames.Add(@"data\04_stone_roses.wav");
            filenames.Add(@"data\stone roses live.wav");
            filenames.Add(@"data\Dock Boggs - Sugar Baby - 01.wav");
            filenames.Add(@"data\kardinal_offishall_01_3s.wav");
            filenames.Add(@"data\Kardinal Offishall - Quest For Fire - 15 - Go Ahead Den.wav");

            /* add our 6 sample files to the query */
            foreach (string file in filenames)
            {
                AddFile(midf, file);
            }
        }

        static void DoMusicIDFile(GnUser user, int queryFlag)
        {
           
            /* Perform the Query */
            using (GnMusicIDFileEventsDelegate myMidEvents = new MusicIDFileEvents())
            {
                GnMusicIDFile midf = new GnMusicIDFile(user, myMidEvents);

                /* Add our sample files to the query. Metadata and fingerprints will be set in the callbacks. */
                SetQueryData(midf);

                midf.DoLibraryID(queryFlag);
            }

        }

        /*
        * Sample app start (main)
        */
        static void Main(string[] args)
        {
            /*    Client ID, Client ID Tag, License file and GNSDK lib path must be passed in */
            if (args.Length != 4)
            {
                Console.WriteLine("\n\tUsage : \n\n\tsample.exe <clientid> <clientid_tag> license.txt gnsdk_native_lib_path");
                Console.WriteLine("\n\tContact Gracenote professional services to get \n\t'clientid', 'clientid_tag', and 'license'");
            }
            else
            {
                try
                {
                    clientId = args[0].Trim();
                    clientIdTag = args[1].Trim();
                    licenseFile = args[2].Trim();
                    gnsdkLibraryPath = args[3].Trim();
                }
                catch (IOException)
                {
                    throw;
                }
            }

            /* GNSDK initialization */
            try
            {
                // Initialize SDK
                GnSDK gnsdk = new GnSDK(gnsdkLibraryPath, licenseFile, GnSDK.GnLicenseInputMode.kFilename);

                // Dispaly SDK version
                Console.WriteLine("\nGNSDK Product Version    : " + gnsdk.ProductVersion + " \t(built " + gnsdk.BuildDate + ")");

                // Enable logging
                gnsdk.LoggingEnable(
                    "sample.log",
                    GnSDK.GN_LOG_PKG_ALL,
                    GnSDK.GN_LOG_LEVEL_ERROR,
                    GnSDK.GN_LOG_OPTION_ALL,
                    0,                                /* Max size of log: 0 means a new log file will be created each run */
                    false);                           /* true = old logs will be renamed and saved */

                /*         
                    *    Load existing user handle, or register new one.                    
                    */
                user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);
				
				GnStorageSqlite gnStorage = new GnStorageSqlite();
                if (useLocal)
                {
                    gnStorage.StorageFolder = localDB;

                    GnLookupLocal gnLookupLocal = new GnLookupLocal();

                    user.OptionLookupMode(GnLookupMode.kLookupModeLocal);

                    DisplayEmbeddedDbInfo(gnLookupLocal);

                }
                else
                {
                    user.OptionLookupMode(GnLookupMode.kLookupModeOnline);
                }

				/* Set locale with desired Group, Language, Region and Descriptor
		        * Set the 'locale' to return locale-specific results values. This examples loads an English locale.
		        */
                 GnLocale locale = new GnLocale( GnSDK.kLocaleGroupMusic,           /* Locale group */
												GnSDK.kLanguageEnglish,             /* Language */
												GnSDK.kRegionGlobal,                /* Region */
												GnSDK.kDescriptorDefault,           /* Descriptor */
												user,                               /* User */
												null /*localeEvents*/               /* locale Events object */
												);
				
				gnsdk.SetDefaultLocale(locale);
                /* Perform MusicID-File LibraryID lookups */

                DoMusicIDFile(user, gnsdk_csharp_marshal.GnMidfQueryReturnSingle);

            }
            catch (GnException e)
            {
                Console.WriteLine("GnException : (" + e.Message.ToString() + ")" + e.Message);
            }
        }

    }
}
