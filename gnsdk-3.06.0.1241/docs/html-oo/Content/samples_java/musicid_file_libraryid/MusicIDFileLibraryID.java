/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 Name: MusicIDFileLibraryID sample application
 * Description:
 * LibraryID processing adds another level of processing above AlbumID for very large collections of media files.
 * LibraryID extends AlbumID functionality by performing additional scanning and processing of all the files in
 * an entire collection. This enables LibraryID to find groupings that are not captured by AlbumID processing.
 * This method is highly recommended for use when there are a large number (hundreds to thousands) of files to
 * identify, though it is also equally effective when processing only a few files. This method takes most of
 * the guesswork out of MusicID-File and lets the library do all the work for the application.
 * The GnMusicIDFile::doLibraryID method provides LibraryID processing.
 *
 * Command-line Syntax:
 * sample clientId clientIdTag license libPath
 */
package musicid_file_libraryid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class MusicIDFileLibraryID
{
    /* Set USELOCAL to false to have the sample perform online queries.
     * Set USELOCAL to true to have the sample perform local queries.
     * Note: For local queries, Gracenote database is required.
     */
    private static final boolean USELOCAL = false;

    /* Files used by this sample application */
    private static final String ARRAYLIST[] = { "01_stone_roses.wav", 
    											"04_stone_roses.wav", 
    											"stone roses live.wav",
    											"Dock Boggs - Sugar Baby - 01.wav", 
    											"kardinal_offishall_01_3s.wav", 
    											"Kardinal Offishall - Quest For Fire - 15 - Go Ahead Den.wav" };

    private static final String KEY = "../../../sample_data/";
    
    static{
    	try{
    		System.loadLibrary("gnsdk_java_marshal");
    	} catch (UnsatisfiedLinkError unsatisfiedLinkError){
	    System.err.println("Native code library failed to load\n" + unsatisfiedLinkError.getMessage());
	    System.exit(1);
    	}
    }
    
    private static boolean checkFilePath(String license_path){
    	File file = new File(license_path);
    	
    	if (file.isFile()){
    		return true;
    	}
    	
    	return false;
    }

    private static void displayEmbeddedDbInfo(GnLookupLocal gnLookupLocal) throws GnException {
		long ordinal = gnLookupLocal.storageInfoCount(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion
				);
		
		final String versionResult = gnLookupLocal.storageInfo(
				GnLocalStorageName.kMetadata,
				GnLocalStorageInfoKey.kGDBVersion, 
				ordinal
				);
		
		System.out.println("GracenoteDB Source DB ID : " + versionResult);
	}

	private static class LocaleEvent extends GnStatusEventsListener {
		public void statusEvent(
				GnStatus locale_status,
				long percent_complete, 
				long bytes_total_sent,
				long bytes_total_received) {
			System.out.print("\nLoading locale ...\n\tStatus - "+ locale_status);

			System.out.println("\t% Complete (" + percent_complete+ 
					"),\n\tTotal Bytes Sent (" + bytes_total_sent+ 
					"),\n\tTotal Bytes Received (" + bytes_total_received+ ")");
		}

		public boolean cancelCheck() {
			return false;
		}
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {
		LocaleEvent localeEvents = new LocaleEvent();
		
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupMusic(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				localeEvents
				);

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}
	
	private static GnUser getUser(
			final GnSDK gnsdk,
			final String clientId,
			final String clientIdTag,
			final String applicationVersion
			) throws GnException, IOException {
		
		String serialized = null;
		
		if (!new File("user.txt").canRead()) {
			System.out.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			if (USELOCAL) {
				userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeLocalOnly;
			}
			
			serialized = gnsdk.registerUser(
					userRegistrationMode, 
					clientId, 
					clientIdTag, 
					applicationVersion
					).toString();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));
			
			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		}else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
		    try {
		    	/* read stored user data from file */
		    	serialized = br.readLine();
		        
		    	} catch (IOException e) {
		    		System.out.println("\nError: Failed to open the user file. (user.txt)");
		    	}finally {
		    		br.close();
		    	}
			}
		
		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}

    private static void displayLibrary(GnResponseAlbums response){
    	int albumNo = 1;
	
    	GnAlbumIterator albIterable = response.albums().getIterator();
	
    	while (albIterable.hasNext()){
    		GnAlbum alb = albIterable.next();
    		GnTitle albTitle = alb.title();
	    
    		System.out.println("\tMatch " + albumNo + " - Album title:\t\t" + albTitle.display());
	    
    		albumNo++;
    	}
    }

    /* Callback delegate called when performing MusicID-File operation */
    private static class MusicIdFileEvents extends GnMusicIDFileEventsListener
    {
    	@Override
    	public void resultAvailable(GnResponseAlbums album_result, long current_album, long total_albums){
    	    System.out.println("\nMID-File Result: ");
    	    System.out.println("\tAlbum count: " + album_result.albums().count());
    	    
    	    displayLibrary(album_result);
    	}
    	
    	@Override
    	public boolean cancelCheck(){
    	    return false;
    	}
    	
    	@Override
    	public void getFingerprint(GnMusicIDFileInfo fileinfo, 
    							   long currentFile, 
    							   long totalFiles){
    	    boolean complete = false;
    	    String filepath = null;
    	    String keyPtah = null;
    	    
    	    try{
    	    	filepath = fileinfo.getFileName();
    	    	keyPtah = KEY + filepath;
    	    } catch (GnException gnException){
    	    	System.out.println("\nError Code: " + gnException.getErrorCode() + "\nError Message: " + gnException.getMessage());
    	    	return;
    	    }
    	   
    	    File f = new File(keyPtah);
    	   
    	    if (!f.exists()){
    	    	System.out.println("\n\nError: Failed to open input file: " + filepath);
    	    } else{
    	    	try{
    	    		/*
    	    		 * initialize the fingerprinter Note: The sample files are
    	    		 * non-standard 11025 Hz 16-bit mono to save on file size
    	    		 */
    	    		fileinfo.fingerprintBegin(11025, 16, 1);
    	    		FileInputStream fin = null;
    	    		DataInputStream din = null;
    		   
    	    		File file = new File(keyPtah);
    	    		fin = new FileInputStream(file);
    	    		din = new DataInputStream(fin);
    		    
    	    		int readSize = 0;
    		    
    	    		/*
    	    		 * skip the wave header (first 44 bytes). the format of the
    	    		 * sample files is known, but please be aware that many wav
    	    		 * file headers are larger then 44 bytes!
    	    		 */
    		   
    	    		fin.skip(44);
    	    		while ((readSize = din.available()) > 0){
    	    			if (readSize > 1024){
    	    				readSize = 1024;
    	    			}
    			
    	    			byte[] result = readBytes(din, readSize);
    			
    	    			complete = fileinfo.fingerprintWrite(result, (long) result.length);
    			
    	    			if (complete){
    	    				break;
    	    			}
    	    		}
    		   
    	    		fileinfo.fingerprintEnd();
    		    
    	    		if (!complete){
    	    			/*
    	    			 * Finger print module doesn't have enough data to generate a
    	    			 * finger print. Note that the sample data does include
    	    			 * one track that is too short to finger print.
    	    			 */
    	    			System.out.println("\nWarning: input file does not contain enough data to generate a fingerprint:\n"
    	    			 + file.getName() + "\n");
    	    		}
    	    	} catch (IOException ioException){
    	    		System.out.println("Execption reading audio file" + ioException.getMessage());
    	    	} catch (GnException gnException){
    	    		System.out.println("\nError Code: " + gnException.getErrorCode() 
    	    				+ "\nError Message: " + gnException.getMessage());
    	    	}
    	    }
    	}
    	
    	@Override
    	public void getMetadata(GnMusicIDFileInfo fileinfo, long currentFile, long totalFiles){
    		try{
    			/*
    			 * A typical use for this callback is to read file tags (ID3,
    			 * etc) for the basic meta data of the track. To keep the sample
    			 * code simple, we went with .wav files and hard coded in meta
    			 * data for just one of the sample tracks.
    			 */
		
    			/* So, if this isn't the correct sample track, return. */
    			String identifier = fileinfo.getIdentifier();
		
    			if (!identifier.contains("kardinal_offishall_01_3s.wav")){
    				return;
    			}
		
    			fileinfo.setAlbumArtist("kardinal offishall");
    			fileinfo.setAlbumTitle("quest for fire");
    			fileinfo.setTrackTitle("intro");
    		} catch (GnException gnException){
    			System.out.println("\nError Code: " + gnException.getErrorCode() 
    					+ "\nError Message: " + gnException.getMessage());
    		}
		}
    }

    private static byte[] readBytes(DataInputStream din, int readSize){
    	byte[] byteArray = new byte[readSize];
	
    	for (int i = 0; i < readSize; i++){
    		try{
    			byteArray[i] = din.readByte();
    		} catch (IOException e){
    			System.out.println("Execption reading audio file" + e.getMessage());
    		}
    	}
    	return byteArray;
    }

    private static void addFile(GnMusicIDFile midf, String filePath) throws GnException{
    	GnMusicIDFileInfo fileinfo = new GnMusicIDFileInfo();
	
    	fileinfo = midf.createFileInfo(filePath);
    	/*
    	 * Set data for this file information instance. This only sets file path
    	 * but all available data should be set, such as artist name, track
    	 * title and album tile if available, such as via audio file tags.
    	 */
	
    	fileinfo.setFileName(filePath);
    }

    private static void setQueryData(GnMusicIDFile midf) throws GnException{
    	FilenameFilter filenameFilter = new FilenameFilter(){
    		@Override
    		public boolean accept(File dir, String name){
    			if (name.endsWith(".mp3") || 
    					name.endsWith(".wav") || 
    					name.endsWith(".au")){
    				return true;
    			}
    			return false;
    		}
    	};
	
    	String currentDir = System.getProperty("user.dir");
	
    	File folder = new File(currentDir, KEY);
    	File[] listOfFiles = folder.listFiles(filenameFilter);
	
    	if (listOfFiles == null){
    		System.out.println("Data directory not found (" + KEY + ")");
    	} else{
    		String ARRAYFILES1[] = new String[listOfFiles.length];
	    
    		for (int i = 0; i < listOfFiles.length; i++){
    			ARRAYFILES1[i] = listOfFiles[i].getName();
    		}
	   
    		final List<String> FILELIST = Arrays.asList(ARRAYFILES1);
	    
    		for (int i = 0; i < ARRAYLIST.length; i++){
    			if (FILELIST.contains(ARRAYLIST[i])){
    				addFile(midf, ARRAYLIST[i]);
    			}
    		}
    	}
    }

    private static void doMusicidFileLibraryId(GnUser user) throws GnException, IOException{
    	GnMusicIDFileEventsListener myMidEvents = new MusicIdFileEvents();
    	
    	GnMusicIDFile gnMusicIDFile = new GnMusicIDFile(user, myMidEvents);
    	
    	setQueryData(gnMusicIDFile);
    	
    	gnMusicIDFile.doLibraryID(
    			gnsdk_java.getGnMidfQueryReturnSingle() | gnsdk_java.getGnMidfQueryResponseAlbums()
    			);
    }

    /*
	 * Sample app start (main)
	 */
	public static void main(String[] args) throws IOException, GnException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.println("\nGNSDK Product Version : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				if (USELOCAL) {
					GnStorageSqlite sqliteStorage = new GnStorageSqlite();
					sqliteStorage.storageFolderSet("../../../sample_db");

					GnLookupLocal gnLookupLocal = new GnLookupLocal();
					user.optionLookupMode(GnLookupMode.kLookupModeLocal);

					displayEmbeddedDbInfo(gnLookupLocal);
				} else {
					user.optionLookupMode(GnLookupMode.kLookupModeOnline);
				}

				loadLocale(gnsdk, user);
			
				doMusicidFileLibraryId(user);
			} catch (GnException gnException) {
				System.out.println("GnException \t" + gnException.getMessage());
			} finally {
				System.runFinalization();
				System.gc();
			}
		} else {
			System.out.println("Licence file not found");
			System.exit(0);
		}
	}
}
