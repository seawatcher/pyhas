/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#include "gnsdk_moodgrid.hpp"

#if GNSDK_MOODGRID

using namespace gracenote;
using namespace gracenote::moodgrid;

// **********************************************************
/// moodgrid_result_provider
// **********************************************************

GnMoodgridIdentifier moodgrid_result_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_cstr_t  media_ident = GNSDK_NULL;
	gnsdk_cstr_t  group       = GNSDK_NULL;
	gnsdk_error_t error       = PLERR_NoError;

	error = gnsdk_moodgrid_results_enum(this->resultHandle_, pos, &media_ident, &group);
	if(error) { throw GnError(); }
	return GnMoodgridIdentifier(media_ident, group);
}

gnsdk_uint32_t moodgrid_result_provider::count()
{
	gnsdk_uint32_t count = 0;
	gnsdk_error_t  error = PLERR_NoError;

	error = gnsdk_moodgrid_results_count(resultHandle_, &count);
	if(error) { throw GnError(); }
	return count;
}

// **********************************************************
// GnMoodgridResult
// **********************************************************
gnsdk_uint32_t GnMoodgridResult::Count() const throw ( GnError )
{
	gnsdk_error_t  error = PLERR_NoError;
	gnsdk_uint32_t count = 0;

	error = gnsdk_moodgrid_results_count(get<gnsdk_moodgrid_result_handle_t>(), &count);
	if(error) { throw GnError(); }
	return count;
}

gn_iterable_container<GnMoodgridResult::iterator>
GnMoodgridResult::Identifiers()
{
	moodgrid_result_provider provider(get<gnsdk_moodgrid_result_handle_t>() );
	gnsdk_uint32_t           count = provider.count();

	return gn_iterable_container<iterator>(iterator(provider, 0, count), iterator(provider, count, count) );
}

// **********************************************************
// GnMoodgridProvider
// **********************************************************
gnsdk_cstr_t GnMoodgridProvider::Name() throw ( GnError )
{
	gnsdk_cstr_t  value = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_moodgrid_provider_get_data(this->get<gnsdk_moodgrid_provider_handle_t>(), GNSDK_MOODGRID_PROVIDER_NAME, &value);

	if(error) { throw GnError(); }
	return value;
}

gnsdk_cstr_t GnMoodgridProvider::Type() throw ( GnError )
{
	gnsdk_cstr_t  value = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_moodgrid_provider_get_data(this->get<gnsdk_moodgrid_provider_handle_t>(), GNSDK_MOODGRID_PROVIDER_TYPE, &value);

	if(error) { throw GnError(); }
	return value;
}

bool GnMoodgridProvider::RequiresNetwork() throw ( GnError )
{
	gnsdk_cstr_t  value = GNSDK_NULL;
	gnsdk_error_t error = gnsdk_moodgrid_provider_get_data(this->get<gnsdk_moodgrid_provider_handle_t>(), GNSDK_MOODGRID_PROVIDER_TYPE, &value);

	if(error) { throw GnError(); }
	return value[0] == '1';
}

// **********************************************************
// presentation_data_provider
// **********************************************************
GnMoodgridDataPoint presentation_data_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_error_t  error = MGERR_NoError;
	gnsdk_uint32_t x, y, max_x, max_y;

	error = gnsdk_moodgrid_presentation_type_dimension( (gnsdk_moodgrid_presentation_type_t)type_, &max_x, &max_y);
	if(error) { throw GnError(); }

	x = ( pos + max_y ) / max_y;
	y = ( pos % max_y ) + 1;

	GnMoodgridDataPoint value = { x, y };
	return value;
}

// optional
gnsdk_uint32_t presentation_data_provider::count() const
{
	gnsdk_uint32_t max_x, max_y;
	gnsdk_error_t  error = MGERR_NoError;

	error = gnsdk_moodgrid_presentation_type_dimension( (gnsdk_moodgrid_presentation_type_t)type_, &max_x, &max_y);
	if(error)
	{
		throw GnError();
	}
	return max_x * max_y;
}

// **********************************************************
// GnMoodgridPresentation
// **********************************************************
gn_iterable_container<GnMoodgridPresentation::data_iterator>
GnMoodgridPresentation::Moods()
{
	gnsdk_error_t                      error = MGERR_NoError;
	gnsdk_moodgrid_presentation_type_t type;

	error = gnsdk_moodgrid_presentation_get_type(this->get<gnsdk_moodgrid_presentation_handle_t>(), &type);
	if(error) { throw GnError(); }

	presentation_data_provider provider( (GnMoodgridPresentationType)type);
	gnsdk_uint32_t             count = provider.count();

	return gn_iterable_container<data_iterator>(data_iterator(provider, 0, count), data_iterator(provider, count, count) );
}

void GnMoodgridPresentation::AddFilter(gnsdk_cstr_t uniqueIndentfier, FilterListType elistType, gnsdk_cstr_t strValueId, FilterConditionType eConditionType) throw ( GnError )
{
	gnsdk_cstr_t cstrList      = GNSDK_NULL;
	gnsdk_cstr_t cstrCondition = GNSDK_MOODGRID_FILTER_CONDITION_INCLUDE;

	switch(elistType)
	{
	case kListTypeGenre:
		cstrList = GNSDK_MOODGRID_FILTER_LIST_TYPE_GENRE;
		break;

	case kListTypeEras:
		cstrList = GNSDK_MOODGRID_FILTER_LIST_TYPE_ORIGINS;
		break;

	case kListTypeOrigins:
		cstrList = GNSDK_MOODGRID_FILTER_LIST_TYPE_ERAS;
		break;
	}

	// Only need to handle the exclude case.
	if(kConditionTypeExclude == eConditionType)
	{
		cstrCondition = GNSDK_MOODGRID_FILTER_CONDITION_EXCLUDE;
	}

	gnsdk_error_t error = gnsdk_moodgrid_presentation_filter_set(get<gnsdk_moodgrid_presentation_handle_t>(),
		uniqueIndentfier,
		cstrList,
		strValueId,
		cstrCondition);
	if(error) { throw GnError(); }
}

void GnMoodgridPresentation::RemoveFilter(gnsdk_cstr_t uniqueIndentfier) throw ( GnError )
{
	gnsdk_error_t error = gnsdk_moodgrid_presentation_filter_remove(get<gnsdk_moodgrid_presentation_handle_t>(), uniqueIndentfier);

	if(error) { throw GnError(); }
}

void GnMoodgridPresentation::RemoveAllFilters() throw ( GnError )
{
	gnsdk_error_t error = gnsdk_moodgrid_presentation_filter_remove_all(get<gnsdk_moodgrid_presentation_handle_t>() );

	if(error) { throw GnError(); }
}

GnMoodgridPresentationType GnMoodgridPresentation::LayoutType() throw ( GnError )
{
	gnsdk_error_t                      error = MGERR_NoError;
	gnsdk_moodgrid_presentation_type_t eReturnType;

	error = gnsdk_moodgrid_presentation_get_type(this->get<gnsdk_moodgrid_presentation_handle_t>(), &eReturnType);
	if(error) { throw GnError(); }

	return static_cast<GnMoodgridPresentationType>( eReturnType );
}

gnsdk_cstr_t GnMoodgridPresentation::MoodName(GnMoodgridDataPoint& position) throw ( GnError )
{
	gnsdk_error_t error = MGERR_NoError;
	gnsdk_cstr_t  name  = GNSDK_NULL;

	error = gnsdk_moodgrid_presentation_get_mood_name(get<gnsdk_moodgrid_presentation_handle_t>(), position.X, position.Y, &name);
	if(error) { throw GnError(); }
	return name;
}

gnsdk_cstr_t GnMoodgridPresentation::MoodId(GnMoodgridDataPoint& position) throw ( GnError )
{
	gnsdk_error_t error = MGERR_NoError;
	gnsdk_cstr_t  id    = GNSDK_NULL;

	error = gnsdk_moodgrid_presentation_get_mood_id(get<gnsdk_moodgrid_presentation_handle_t>(), position.X, position.Y, &id);
	if(error) { throw GnError(); }
	return id;
}

GnMoodgridResult GnMoodgridPresentation::FindRecommendations(GnMoodgridProvider& provider, GnMoodgridDataPoint& position) throw ( GnError )
{
	gnsdk_error_t                  error  = MGERR_NoError;
	gnsdk_moodgrid_result_handle_t handle = GNSDK_NULL;

	error = gnsdk_moodgrid_presentation_find_recommendations(get<gnsdk_moodgrid_presentation_handle_t>(), provider.get<gnsdk_moodgrid_provider_handle_t>(), position.X, position.Y, &handle);
	if(error) { throw GnError(); }

	GnMoodgridResult retVal;
	retVal.AcceptOwnership(handle);
	return retVal;
}

gnsdk_uint32_t GnMoodgridPresentation::FindRecommendationsEstimate(GnMoodgridProvider& provider, GnMoodgridDataPoint& position) throw ( GnError )
{
	gnsdk_error_t  error    = MGERR_NoError;
	gnsdk_uint32_t estimate = GNSDK_NULL;

	error = gnsdk_moodgrid_presentation_find_recommendations_estimate(get<gnsdk_moodgrid_presentation_handle_t>(), provider.get<gnsdk_moodgrid_provider_handle_t>(), position.X, position.Y, &estimate);
	if(error) { throw GnError(); }

	return estimate;
}

// **********************************************************
// moodgrid_provider
// **********************************************************
GnMoodgridProvider moodgrid_provider::get_data(gnsdk_uint32_t pos)
{
	gnsdk_error_t                    error    = MGERR_NoError;
	gnsdk_moodgrid_provider_handle_t provider = GNSDK_NULL;

	error = gnsdk_moodgrid_provider_enum(pos, &provider);
	if(error) { throw GnError(); }

	GnMoodgridProvider result(provider);

	error = gnsdk_handle_release(provider);

	if(error) { throw GnError(); }

	return result;
}

// optional
gnsdk_uint32_t moodgrid_provider::count() const
{
	gnsdk_uint32_t count = 0;
	gnsdk_error_t  error = MGERR_NoError;

	error = gnsdk_moodgrid_provider_count(&count);
	if(error)
	{
		throw GnError();
	}
	return count;
}

// **********************************************************
// GnMoodgrid
// **********************************************************
gn_iterable_container<GnMoodgrid::provider_iterator>
GnMoodgrid::Providers()
{
	moodgrid_provider provider;
	gnsdk_uint32_t    count = provider.count();

	return gn_iterable_container<provider_iterator>(provider_iterator(provider, 0, count), provider_iterator(provider, count, count) );
}

GnMoodgridPresentation GnMoodgrid::CreatePresentation(GnUser& user, GnMoodgridPresentationType type) throw ( GnError )
{
	gnsdk_error_t                        error  = MGERR_NoError;
	gnsdk_moodgrid_presentation_handle_t handle = GNSDK_NULL;

	error = gnsdk_moodgrid_presentation_create(user.native(), (gnsdk_moodgrid_presentation_type_t)type, GNSDK_NULL, GNSDK_NULL, &handle);

	if(error) { throw GnError(); }

	GnMoodgridPresentation result;

	result.AcceptOwnership(handle);

	return result;
}
#endif /*GNSDK_MOODGRID*/
