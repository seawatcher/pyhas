var class_gracenote_s_d_k_1_1_gn_s_d_k =
[
    [ "GnLicenseInputMode", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb3bfe93eb89398f535e4e9f3c50197b", [
      [ "kInvalid", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb3bfe93eb89398f535e4e9f3c50197bab10913c938482a8aa4ba85b7a1116cb4", null ],
      [ "kString", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb3bfe93eb89398f535e4e9f3c50197ba302e56d566a2561033b27c13680ea4b2", null ],
      [ "kFilename", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb3bfe93eb89398f535e4e9f3c50197ba3abcb87422cace1f5ae75a9480bc12e6", null ],
      [ "kStandardIn", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb3bfe93eb89398f535e4e9f3c50197ba5640a6e4eb10ab4e3b1bcf543142c29a", null ]
    ] ],
    [ "GnUserRegisterMode", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a2cd3d51c253422595078a4db8a22b510", [
      [ "kUserRegModeOnline", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a2cd3d51c253422595078a4db8a22b510a179013ff8aa2927e4105f715bb728427", null ],
      [ "kUserRegModeLocalOnly", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a2cd3d51c253422595078a4db8a22b510ab3227af4c29f29b2dc48890a27182e83", null ]
    ] ],
    [ "GnSDK", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a28129d1894cc0a92ec9eba408525f9b8", null ],
    [ "AvailableLocales", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a7a61155ad4283b4b451d18ebc24fe8b7", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a9fb16657237225551709ecad6b7adfd6", null ],
    [ "LoggingDisable", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a1f955c71ddaa73cbe2aeaa2263ff0495", null ],
    [ "LoggingEnable", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#ad4e3d523e0e043f8f8d5bdcfdfb3839c", null ],
    [ "LoggingWrite", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a0d3d9f73b951a2d8932c144d823fcad1", null ],
    [ "RegisterUser", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#adb69d1d79215420fcdacf71f1cc54316", null ],
    [ "SetDefaultLocale", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a8e8505d9d958b7174bd8e881dbcba4e2", null ],
    [ "StorageCleanup", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a8a902f5d23965943ba19dc9e017f1009", null ],
    [ "StorageCompact", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a6b3b34199298bff7cc4679925660c14a", null ],
    [ "StorageFlush", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a68b9ab90b0c6de26afda62e1d545b3c3", null ],
    [ "StorageLocation", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a76caf191592ef0bb5662b87b2c386748", null ],
    [ "swigCMemOwn", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a75c8f3130adafcf1e9a23c98d1e47812", null ],
    [ "BuildDate", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#a100fc34a7623d88ee1fe1cffc2cc5bf5", null ],
    [ "ProductVersion", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#acc05548b0e7d73b5d7b2477b0aeada4e", null ],
    [ "Version", "class_gracenote_s_d_k_1_1_gn_s_d_k.html#ae3210975070daf1f48087a8bacc5f342", null ]
];