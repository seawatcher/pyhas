var a00099 =
[
    [ "swigValue", "a00099.html#ac7d11eb21f6656fe58c737ca7c8b83da", null ],
    [ "gnsdk_mids_identifying_ended", "a00099.html#ab991978b231f6461a13f666007862544", null ],
    [ "gnsdk_mids_identifying_fp_generated", "a00099.html#a3a61d6fed2374fa332b24a3e417ad665", null ],
    [ "gnsdk_mids_identifying_local_query_ended", "a00099.html#a74c408fbbd8b9784c6faa2ecd925058f", null ],
    [ "gnsdk_mids_identifying_local_query_started", "a00099.html#ac37a80226d38db72f9ea37bfdb888737", null ],
    [ "gnsdk_mids_identifying_online_query_ended", "a00099.html#aab841eb6d517a96eed26b0f88379f1fd", null ],
    [ "gnsdk_mids_identifying_online_query_started", "a00099.html#aa15a4cfea281a0ad07661bc4c328b228", null ],
    [ "gnsdk_mids_identifying_started", "a00099.html#ad77848fcaf6b203d59a85cd1fbcae6cd", null ],
    [ "gnsdk_mids_status_audio_music", "a00099.html#a0e2c69b8622bb4f83b5bda4c4dc2dbf4", null ],
    [ "gnsdk_mids_status_audio_noise", "a00099.html#a5ca8731c66601d9fa6771a07ec8debba", null ],
    [ "gnsdk_mids_status_audio_none", "a00099.html#a21e2c03bcb95c7b2a48e57c3f5408abe", null ],
    [ "gnsdk_mids_status_audio_silence", "a00099.html#adcb8b19c3bc6b7184548ebedafcbac40", null ],
    [ "gnsdk_mids_status_audio_speech", "a00099.html#a641c1b569ad9eeccd5398badf0520e3b", null ],
    [ "gnsdk_mids_status_error_noclassifier", "a00099.html#a91b66f82df38fad1350af09b81734e44", null ],
    [ "gnsdk_mids_status_invalid", "a00099.html#a1754b44ef7403bc2364133405e356fa6", null ],
    [ "gnsdk_mids_status_transition_channel_change", "a00099.html#a0a6745248d8bda86c70be256c653c70d", null ],
    [ "gnsdk_mids_status_transition_content_to_content", "a00099.html#a20bd8927d5b223584f2228eef0d52da5", null ],
    [ "gnsdk_mids_status_transition_none", "a00099.html#ad8894124a7258d8aa6b28e79b66597a9", null ]
];