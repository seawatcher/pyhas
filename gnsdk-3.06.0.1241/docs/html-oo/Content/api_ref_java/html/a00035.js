var a00035 =
[
    [ "GnErrorInfo", "a00035.html#a5aedb20f6535fe6f8890bd780772044c", null ],
    [ "GnErrorInfo", "a00035.html#adc4c47f6cb4bd30fdb7f286a34a56604", null ],
    [ "delete", "a00035.html#a3db51bcf8bba7371c5adbb4b8a1d324f", null ],
    [ "finalize", "a00035.html#ad6e2c5531e7dfaec11a55c191665f0dc", null ],
    [ "getError_api", "a00035.html#aed04002ae2bedac07cebb97a95e4d820", null ],
    [ "getError_code", "a00035.html#a8dc4de68d400e79e91e3aad365500d0a", null ],
    [ "getError_description", "a00035.html#aa62bc8723ee5132f1c118367168c1e63", null ],
    [ "getError_module", "a00035.html#a80f7f7e4c19e7966079ba26a28f19d80", null ],
    [ "getSource_error_code", "a00035.html#a8a026eeec667e356d29afb9f069a4fbb", null ],
    [ "getSource_error_module", "a00035.html#adf605fd8847d10a62feb853826c8fb46", null ],
    [ "setError_api", "a00035.html#a8b3ef2b1e0c63e8d0149b71dae0a0e73", null ],
    [ "setError_code", "a00035.html#a55529e71ca87142395f3385c8dfc8dbf", null ],
    [ "setError_description", "a00035.html#aa5e9104db1933328f0c97f828a22f941", null ],
    [ "setError_module", "a00035.html#a147d5e750704d7a350ce0ea74c122a4b", null ],
    [ "setSource_error_code", "a00035.html#a32d9781b7f514474fe9cbd7155d33c6a", null ],
    [ "setSource_error_module", "a00035.html#a69dbc4466f55f72224e21eeba78237d6", null ],
    [ "swigCMemOwn", "a00035.html#ac65cb51c5128c134acd068f52a3c4a6f", null ]
];