var group___video___filter_values___production_type =
[
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_ANIMATION", "group___video___filter_values___production_type.html#ga40e2d4b2fa8b8844589737f8d4182088", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_DOCUMENTARY", "group___video___filter_values___production_type.html#gab7931458138b56ba032ad9a8837f0452", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_EDUCATIONAL", "group___video___filter_values___production_type.html#ga00332466e4a3ecd89594d3c77269b2d1", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_GAME_SHOW", "group___video___filter_values___production_type.html#ga3aa7c4d7a5b1a23e43d5d27f37bfe6d5", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_INSTRUCTIONAL", "group___video___filter_values___production_type.html#ga39601d271a92094672de69f0fde88fe7", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_KARAOKE", "group___video___filter_values___production_type.html#gaf23c4b51472fdad3ca0888c20fc15303", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_LIVE_PERFORMANCE", "group___video___filter_values___production_type.html#gadcc58a3753fb3ed36686b15c1d8de083", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_MINI_SERIES", "group___video___filter_values___production_type.html#gafcd82e3a2c7e7c5f896b28bedce7893a", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_MOTION_PICTURE", "group___video___filter_values___production_type.html#gaca24a64980a50646e2852341f47f3486", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_MUSIC_VIDEO", "group___video___filter_values___production_type.html#ga6881368c62a3cb41727e922173599dd1", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_SERIAL", "group___video___filter_values___production_type.html#ga9a24da38f36af6c4a75e7761bd4560ea", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_SHORT_FEATURE", "group___video___filter_values___production_type.html#ga9e7512d08fe62a2834124c9706ed2e87", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_SPORTING_EVENT", "group___video___filter_values___production_type.html#ga6d0ffbe9774046b0763e8ecb841e48c9", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_STAGE_PRODUCTION", "group___video___filter_values___production_type.html#gaf1f0541f1b4275e8a86039ab3bd1852b", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_TV_SERIES", "group___video___filter_values___production_type.html#ga8c3a91a77cf3262a2fabeaa97f964df9", null ],
    [ "GNSDK_VIDEO_FILTER_VALUE_PRODUCTION_TYPE_VARIETY_SHOW", "group___video___filter_values___production_type.html#ga4c228460c311777c97b36270725fdcfe", null ]
];