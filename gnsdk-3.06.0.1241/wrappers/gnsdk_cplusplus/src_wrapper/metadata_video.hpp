/**
 * Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _METADATA_VIDEO_HPP_
#define _METADATA_VIDEO_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk_iterator_base.hpp"
#include "gnsdk.hpp"
#include "metadata_music.hpp"
#include "metadata.hpp"

namespace gracenote
{
	namespace metadata
	{
		// Forward Class Declaration
		class GnVideoProduct;

		class GnVideoWork;
		class GnVideoSeason;
		class GnVideoSeries;
		class GnTVProgram;
#if GNSDK_ACR
		class GnAcrMatch;
#endif
#if ( defined( GNSDK_ACR ) || defined( GNSDK_EPG ) )
		class GnTVAiring;
		class GnTVChannel;
#endif

		class GnVideoDisc;
		class GnVideoSide;
		class GnVideoLayer;
		class GnVideoFeature;
		class GnVideoChapter;
		class GnVideoCredit;

		typedef gn_facade_range_iterator<GnVideoProduct, gn_gdo_provider<GnVideoProduct> >   product_iterator;
		typedef gn_facade_range_iterator<GnVideoWork, gn_gdo_provider<GnVideoWork> >         works_iterator;
		typedef gn_facade_range_iterator<GnVideoSeason, gn_gdo_provider<GnVideoSeason> >     season_iterator;
		typedef gn_facade_range_iterator<GnVideoSeries, gn_gdo_provider<GnVideoSeries> >     series_iterator;
		typedef gn_facade_range_iterator<GnTVProgram, gn_gdo_provider<GnTVProgram> >         tvprogram_iterator;
#if GNSDK_ACR
		typedef gn_facade_range_iterator<GnAcrMatch, gn_gdo_provider<GnAcrMatch> >           acr_iterator;
#endif
#if ( defined( GNSDK_ACR ) || defined( GNSDK_EPG ) )
		typedef gn_facade_range_iterator<GnTVAiring, gn_gdo_provider<GnTVAiring> >           tvairing_iterator;
		typedef gn_facade_range_iterator<GnTVChannel, gn_gdo_provider<GnTVChannel> >         tvchannel_iterator;
#endif

		typedef gn_facade_range_iterator<GnVideoDisc, gn_gdo_provider<GnVideoDisc> >         disc_iterator;
		typedef gn_facade_range_iterator<GnVideoSide, gn_gdo_provider<GnVideoSide> >         side_iterator;
		typedef gn_facade_range_iterator<GnVideoLayer, gn_gdo_provider<GnVideoLayer> >       layer_iterator;
		typedef gn_facade_range_iterator<GnVideoFeature, gn_gdo_provider<GnVideoFeature> >   feature_iterator;
		typedef gn_facade_range_iterator<GnVideoChapter, gn_gdo_provider<GnVideoChapter> >   chapter_iterator;
		typedef gn_facade_range_iterator<GnVideoCredit, gn_gdo_provider<GnVideoCredit> >     video_credit_iterator;
		typedef gn_facade_range_iterator<GnContributor, gn_gdo_provider<GnContributor> >     contributor_iterator;

		/******************************************************************************
		** GnRating
		*/
		class GnRating : public GnDataObject
		{
		public:
			GnRating() : GnDataObject(GNSDK_NULL_GDO) { }
			GnRating(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnRating() { }

			/**
			 *  Retrieves a rating value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Rating() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING);
			}

			/**
			 *  Retrieves a rating type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingType() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_TYPE);
			}

			/**
			 *  Retrieves a rating description value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingDesc() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_DESC);
			}

			/**
			 *  Retrieves a rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t RatingTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RATING_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a rating reason value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingReason() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_REASON);
			}

			/**
			 *  Retrieves a MPAA rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingMPAA() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_MPAA);
			}

			/**
			 *  Retrieves a MPAA TV rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingMPAATV() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_MPAA_TV);
			}

			/**
			 *  Retrieves a FAB rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingFAB() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_FAB);
			}

			/**
			 *  Retrieves a CHVRS rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingCHVRS() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_CHVRS);
			}

			/**
			 *  Retrieves a Canadian TV rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingCanadianTV() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_CANADIAN_TV);
			}

			/**
			 *  Retrieves a BBFC rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingBBFC() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_BBFC);
			}

			/**
			 *  Retrieves a CBFC rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingCBFC() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_CBFC);
			}

			/**
			 *  Retrieves a OFLC TV rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingOFLC() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_OFLC);
			}

			/**
			 *  Retrieves a Hong Kong rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingHongKong() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_HONG_KONG);
			}

			/**
			 *  Retrieves a Finnish rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingFinnish() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_FINNISH);
			}

			/**
			 *  Retrieves a KMRB rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingKMRB() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_KMRB);
			}

			/**
			 *  Retrieves a DVD Parental rating type value from a GDO.
			 *  GNSDK_GDO_VALUE_RATING_DVD_PARENTAL
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingDVDParental() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_DVD_PARENTAL);
			}

			/**
			 *  Retrieves a EIRIN rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingEIRIN() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_EIRIN);
			}

			/**
			 *  Retrieves a INCAA rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingINCAA() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_INCAA);
			}

			/**
			 *  Retrieves a DJTCQ rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingDJTCQ() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_DJTCQ);
			}

			/**
			 *  Retrieves a Quebecois rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingQuebec() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_QUEBEC);
			}

			/**
			 *  Retrieves a French rating type value from a GDO.
			 *  GNSDK_GDO_VALUE_RATING_FRANCE
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingFrance() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_FRANCE);
			}

			/**
			 *  Retrieves a FSK rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingFSK() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_FSK);
			}

			/**
			 *  Retrieves a Italian rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingItaly() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_ITALY);
			}

			/**
			 *  Retrieves a Spanish rating type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RatingSpain() const
			{
				return StringValue(GNSDK_GDO_VALUE_RATING_SPAIN);
			}
		};

		/******************************************************************************
		** GnVideoCredit
		*/
		class GnVideoCredit : public GnDataObject
		{
		public:
			GnVideoCredit() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoCredit(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnVideoCredit()  { }

			/**
			 *  Retrieves a role value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This value shows the role that a person played in a music or video production; for example,
			 *   singing, playing an instrument, acting, directing, and so on.
			 * <p><b>Note:</b></p>
			 *  For music credits, the absence of a role for a person indicates that person is the primary
			 *   artist, who may have performed multiple roles.
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Role
			 */
			gnsdk_cstr_t Role() const
			{
				return StringValue(GNSDK_GDO_VALUE_ROLE);
			}

			/**
			 *  Returns a Role ID for a credit.
			 * @ingroup GDO_ValueKeys_Role
			 */
			gnsdk_int32_t RoleID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ROLE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a role billing value from a GDO.
			 * @ingroup GDO_ValueKeys_Role
			 */
			gnsdk_cstr_t RoleBilling() const
			{
				return StringValue(GNSDK_GDO_VALUE_ROLE_BILLING);
			}

			/**
			 *  Retrieves a character name value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t CharacterName() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHARACTER_NAME);
			}

			/**
			 *  Retrieves a rank (ordinal) for the current credit type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Rank() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RANK);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 *  return artist type object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnArtistType ArtistType() const
			{
				return Reflect<GnArtistType>();
			}

			/**
			 *  return origin type object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 *  return era type object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnEra Era() const
			{
				return Reflect<GnEra>();
			}

			/**
			 *   Retrieves the official child name GDO.
			 * @ingroup GDO_ChildKeys_Name
			 */
			GnName OfficialName() const
			{
				return ChildGet<GnName>(GNSDK_GDO_CHILD_NAME_OFFICIAL);
			}

			/**
			 *  Retrieves a child contributor type.
			 * @ingroup GDO_ChildKeys_Contributor
			 */
			GnContributor Contributor() const
			{
				return ChildGet<GnContributor>(GNSDK_GDO_CHILD_CONTRIBUTOR);
			}

			// TODO
			// NEED TO CHECK IF THIS IMPLEMENTATION IS CORRECT
			gn_iterable_container<works_iterator>  Works();

			// TODO
			gn_iterable_container<series_iterator> Series();

			// TODO
			gn_iterable_container<season_iterator> Seasons();
		};

		/******************************************************************************
		** GnVideoChapter
		*/

		class GnVideoChapter : public GnDataObject
		{
		public:
			GnVideoChapter() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoChapter(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnVideoChapter()  { }

			/**
			 *  Retrieves an ordinal of the current type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Ordinal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ORDINAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *   program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *   Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *   Retrieves the official child title GDO.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *   Retrieves a child credit type. For music types, this includes
			 *   all credits except the artist credit.
			 * @ingroup GDO_ChildKeys_Credit
			 */
			gn_iterable_container<video_credit_iterator> VideoCredits() const
			{
				gn_gdo_provider<GnVideoCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<video_credit_iterator>(video_credit_iterator(provider, 1, count), video_credit_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoSeason
		*/
		class GnVideoSeason : public GnDataObject
		{
		public:
			GnVideoSeason() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoSeason(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnVideoSeason(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_VIDEO_SEASON) { }

			virtual ~GnVideoSeason() { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *   Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *   Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *  Retrieves a product ID for the current product from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  Available for most types, this retrieves a value which can be stored or transmitted. This
			 *  value can be used as a static identifier for the current content as it will not change over time.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *  to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *  Retrieves a TUI value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *  Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoProductionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t VideoProductionTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a release date from the current type. Available for video Products, Features, and
			 *  Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateOriginalRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_ORIGINAL_RELEASE);
			}

			/**
			 *  Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *  program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *  Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *  Retrieves a Franchise number value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseNum() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_NUM);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Franchise count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves Plot sypnosis value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsis() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS);
			}

			/**
			 *  Retrieves Plot sypnosis language value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsisLanguage() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS_LANGUAGE);
			}

			/**
			 *  Retrieves Plot tagline value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotTagline() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_TAGLINE);
			}

			/**
			 *  Retrieves Serial type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SerialType() const
			{
				return StringValue(GNSDK_GDO_VALUE_SERIAL_TYPE);
			}

			/**
			 *  Retrieves Work type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t WorkType() const
			{
				return StringValue(GNSDK_GDO_VALUE_WORK_TYPE);
			}

			/**
			 *  Retrieves Audience value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Audience() const
			{
				return StringValue(GNSDK_GDO_VALUE_AUDIENCE);
			}

			/**
			 *  Retrieves Video mood value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoMood() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_MOOD);
			}

			/**
			 *  Retrieves Story type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t StoryType() const
			{
				return StringValue(GNSDK_GDO_VALUE_STORY_TYPE);
			}

			/**
			 *  Retrieves Reputation value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Repuataion() const
			{
				return StringValue(GNSDK_GDO_VALUE_REPUTATION);
			}

			/**
			 *  Retrieves Scenario value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Scenario() const
			{
				return StringValue(GNSDK_GDO_VALUE_SCENARIO);
			}

			/**
			 *  Retrieves Setting environment value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingEnvironment() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_ENVIRONMENT);
			}

			/**
			 *  Retrieves Setting time period value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingTimePeriod() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_TIME_PERIOD);
			}

			/**
			 *  Retrieves Topic value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Topic() const
			{
				return StringValue(GNSDK_GDO_VALUE_TOPIC);
			}

			/**
			 *  Retrieves a Season number value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonNumber() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_NUMBER);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Season count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves Source value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Source() const
			{
				return StringValue(GNSDK_GDO_VALUE_SOURCE);
			}

			/**
			 *  Retrieves Style value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Style() const
			{
				return StringValue(GNSDK_GDO_VALUE_STYLE);
			}

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 *  return origin object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 *  return rating object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *   Retrieves the official child title GDO.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *  Retrieves a Franchise title child from a GDO.
			 * @ingroup GDO_ChildKeys_Video
			 */
			GnTitle FranchiseTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_FRANCHISE);
			}

			/**
			 *  Retrieves an extended ID type (retrievable from most GDOs). This child GDO supports Link data.
			 * @ingroup GDO_ChildKeys_ExternalId
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			// TODO

			/**
			 *   Retrieves a child work type.
			 * @ingroup GDO_ChildKeys_Works
			 */
			gn_iterable_container<works_iterator> Works();

			// TODO

			/**
			 *   Retrieves a child product type.
			 * @ingroup GDO_ChildKeys_Products
			 */
			gn_iterable_container<product_iterator> Products();

			/**
			 *   Retrieves a child credit type. For music types, this includes
			 *   all credits except the artist credit.
			 * @ingroup GDO_ChildKeys_Credit
			 */
			gn_iterable_container<video_credit_iterator> VideoCredits() const
			{
				gn_gdo_provider<GnVideoCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<video_credit_iterator>(video_credit_iterator(provider, 1, count), video_credit_iterator(provider, count, count) );
			}

			/**
			 *   Retrieves a child series type.
			 * @ingroup GDO_ChildKeys_Series
			 */
			gn_iterable_container<series_iterator> Series();
		};

		/******************************************************************************
		** GnVideoSeries
		*/
		class GnVideoSeries : public GnDataObject
		{
		public:
			GnVideoSeries() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoSeries(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnVideoSeries(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_VIDEO_SERIES) { }

			virtual ~GnVideoSeries()  { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *   Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *   Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *  Retrieves a product ID for the current product from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  Available for most types, this retrieves a value which can be stored or transmitted. This
			 *  value can be used as a static identifier for the current content as it will not change over time.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *  to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *  Retrieves a TUI value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *  Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoProductionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t VideoProductionTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a release date from the current type. Available for video Products, Features, and
			 *  Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateOriginalRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_ORIGINAL_RELEASE);
			}

			/**
			 *  Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *  program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *  Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *  Retrieves a Franchise number value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseNum() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_NUM);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Franchise count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a plot synopsis for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsis() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS);
			}

			/**
			 *  Retrieves a plot tagline value from a GDO.
			 *  GNSDK_GDO_VALUE_PLOT_TAGLINE
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotTagline() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_TAGLINE);
			}

			/**
			 *  Retrieves the available language value for a returned GNSDK_GDO_VALUE_PLOT_SYNOPSIS object.
			 *  <p><b>Remarks:</b></p>
			 *  Use this value to retrieve the actual language value of a returned object.
			 *  The language value of a returned object depends on availability. Information in the language set
			 *  for the locale may not be available, and the object's information may be available only in its
			 *  default official language. For example, if a locale's set language is Spanish, but the object's
			 *  information is available only in English, this value returns as English.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsisLanguage() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS_LANGUAGE);
			}

			/**
			 *  Retrieves a serial type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SerialType() const
			{
				return StringValue(GNSDK_GDO_VALUE_SERIAL_TYPE);
			}

			/**
			 *  Retrieves a work type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t WorkType() const
			{
				return StringValue(GNSDK_GDO_VALUE_WORK_TYPE);
			}

			/**
			 *  Retrieves an audience value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Audience() const
			{
				return StringValue(GNSDK_GDO_VALUE_AUDIENCE);
			}

			/**
			 *  Retrieves a mood group value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This value retrieves mood information for music and video, depending on the respective calling
			 *  type.
			 * @ingroup GDO_ValueKeys_Music
			 */
			gnsdk_cstr_t VideoMood() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_MOOD);
			}

			/**
			 *  Retrieves a story type value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t StoryType() const
			{
				return StringValue(GNSDK_GDO_VALUE_STORY_TYPE);
			}

			/**
			 *  Retrieves a reputation value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Reputation() const
			{
				return StringValue(GNSDK_GDO_VALUE_REPUTATION);
			}

			/**
			 *  Retrieves a scenario value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Scenario() const
			{
				return StringValue(GNSDK_GDO_VALUE_SCENARIO);
			}

			/**
			 *  Retrieves a setting environment value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingEnvironment() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_ENVIRONMENT);
			}

			/**
			 *  Retrieves a historical time period value, such as Elizabethan Era, 1558-1603.
			 *  GNSDK_GDO_TYPE_VIDEO_WORK GNSDK_GDO_TYPE_VIDEO_SEASON GNSDK_GDO_TYPE_VIDEO_SERIES
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingTimePeriod() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_TIME_PERIOD);
			}

			/**
			 *  Retrieves a source value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Source() const
			{
				return StringValue(GNSDK_GDO_VALUE_SOURCE);
			}

			/**
			 *  Retrieves a style value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Style() const
			{
				return StringValue(GNSDK_GDO_VALUE_STYLE);
			}

			/**
			 *  Retrieves a topic value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Topic() const
			{
				return StringValue(GNSDK_GDO_VALUE_TOPIC);
			}

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 *  return origin object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 *  return rating object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *   Retrieves the official child title.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *   Retrieves the franchise child title.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle FranchiseTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_FRANCHISE);
			}

			/**
			 *  Retrieves an extended ID type (retrievable from most GDOs). This child GDO supports Link data.
			 *  GNSDK_GDO_CHILD_EXTERNAL_ID
			 *  GNSDK_GDO_TYPE_ALBUM GNSDK_GDO_TYPE_CONTRIBUTOR GNSDK_GDO_TYPE_TRACK
			 *  GNSDK_GDO_TYPE_VIDEOCLIP GNSDK_GDO_TYPE_VIDEO_PRODUCT GNSDK_GDO_TYPE_VIDEO_SEASON
			 *  GNSDK_GDO_TYPE_VIDEO_SERIES GNSDK_GDO_TYPE_VIDEO_WORK
			 * @ingroup GDO_ChildKeys_ExternalId
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			// TODO

			/**
			 *   Retrieves a child work type.
			 * @ingroup GDO_ChildKeys_Works
			 */
			gn_iterable_container<works_iterator> Works();

			// TODO

			/**
			 *   Retrieves a child product type.
			 * @ingroup GDO_ChildKeys_Products
			 */
			gn_iterable_container<product_iterator> Products();

			/**
			 *  Retrieve a child Season type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_SEASON GDOs.
			 *
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *  video Season to retrieve.
			 *  Video product types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SEASON GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<season_iterator> Seasons() const
			{
				gn_gdo_provider<GnVideoSeason> provider(*this, GNSDK_GDO_CHILD_VIDEO_SEASON);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<season_iterator>(season_iterator(provider, 1, count), season_iterator(provider, count, count) );
			}

			/**
			 *   Retrieves a child credit type. For music types, this includes
			 *   all credits except the artist credit.
			 * @ingroup GDO_ChildKeys_Credit
			 */
			gn_iterable_container<video_credit_iterator> VideoCredits() const
			{
				gn_gdo_provider<GnVideoCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<video_credit_iterator>(video_credit_iterator(provider, 1, count), video_credit_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoWork
		*/
		class GnVideoWork : public GnDataObject
		{
		public:
			GnVideoWork() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoWork(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnVideoWork(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_VIDEO_WORK) { }

			virtual ~GnVideoWork() { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *   Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *   Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *  Retrieves a product ID for the current product from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  Available for most types, this retrieves a value which can be stored or transmitted. This
			 *   value can be used as a static identifier for the current content as it will not change over time.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *  Retrieves a TUI value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *  Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *  Retrieves a second TUI from a Works GDO, if that GDO can be accessed by two IDs. This TUI is used
			 *   for matching partial Products objects to full Works objects.
			 *  Use this value to ensure correct TUI value matching for cases when a video Product GDO contains
			 *   multiple partial Work GDOs. Each partial Work GDO corresponds
			 *  to a full Works object, and each full Works object contains the GNSDK_GDO_VALUE_TUI value,
			 *   incremented by one digit to maintain data integrity with Gracenote Service.
			 *  The GNSDK_GDO_VALUE_TUI_MATCH_PRODUCT maps the partial Works object TUI value to the full Works
			 *   object.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI_Match_Product() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_MATCH_PRODUCT);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t VideoProductionTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoProductionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE);
			}

			/**
			 *  Retrieves a release date from the current type. Available for video Products, Features, and
			 *  Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateOriginalRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_ORIGINAL_RELEASE);
			}

			/**
			 *  Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *   program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *   Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *  Retrieves a Franchise number value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseNum() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_NUM);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Franchise count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t FranchiseCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FRANCHISE_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Season episode value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeriesEpisode() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_EPISODE);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves an episode count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeriesEpisodeCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_EPISODE_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Season episode value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonEpisode() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_EPISODE);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves an episode count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonEpisodeCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_EPISODE_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Season count value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a Season number value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t SeasonNumber() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_SEASON_NUMBER);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a plot synopsis for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsis() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS);
			}

			/**
			 *  Retrieves a plot tagline value from a GDO.
			 *  GNSDK_GDO_VALUE_PLOT_TAGLINE
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotTagline() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_TAGLINE);
			}

			/**
			 *  Retrieves the available language value for a returned GNSDK_GDO_VALUE_PLOT_SYNOPSIS object.
			 *  <p><b>Remarks:</b></p>
			 *  Use this value to retrieve the actual language value of a returned object.
			 *  The language value of a returned object depends on availability. Information in the language set
			 *   for the locale may not be available, and the object's information may be available only in its
			 *   default official language. For example, if a locale's set language is Spanish, but the object's
			 *   information is available only in English, this value returns as English.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsisLanguage() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS_LANGUAGE);
			}

			/**
			 *  Retrieves a serial type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SerialType() const
			{
				return StringValue(GNSDK_GDO_VALUE_SERIAL_TYPE);
			}

			/**
			 *  Retrieves a work type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t WorkType() const
			{
				return StringValue(GNSDK_GDO_VALUE_WORK_TYPE);
			}

			/**
			 *  Retrieves an audience value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Audience() const
			{
				return StringValue(GNSDK_GDO_VALUE_AUDIENCE);
			}

			/**
			 *  Retrieves a mood group value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This value retrieves mood information for music and video, depending on the respective calling
			 *   type.
			 * @ingroup GDO_ValueKeys_Music
			 */
			gnsdk_cstr_t VideoMood() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_MOOD);
			}

			/**
			 *  Retrieves a story type value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t StoryType() const
			{
				return StringValue(GNSDK_GDO_VALUE_STORY_TYPE);
			}

			/**
			 *  Retrieves a scenario value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Scenario() const
			{
				return StringValue(GNSDK_GDO_VALUE_SCENARIO);
			}

			/**
			 *  Retrieves a setting environment value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingEnvironment() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_ENVIRONMENT);
			}

			/**
			 *  Retrieves a historical time period value, such as Elizabethan Era, 1558-1603.
			 *  GNSDK_GDO_TYPE_VIDEO_WORK GNSDK_GDO_TYPE_VIDEO_SEASON GNSDK_GDO_TYPE_VIDEO_SERIES
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SettingTimePeriod() const
			{
				return StringValue(GNSDK_GDO_VALUE_SETTING_TIME_PERIOD);
			}

			/**
			 *  Retrieves a source value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Source() const
			{
				return StringValue(GNSDK_GDO_VALUE_SOURCE);
			}

			/**
			 *  Retrieves a style value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Style() const
			{
				return StringValue(GNSDK_GDO_VALUE_STYLE);
			}

			/**
			 *  Retrieves a topic value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Topic() const
			{
				return StringValue(GNSDK_GDO_VALUE_TOPIC);
			}

			/**
			 *  Retrieves a reputation value from a video Work GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *  call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *  <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *  a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *  information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Reputation() const
			{
				return StringValue(GNSDK_GDO_VALUE_REPUTATION);
			}

			/**
			 *  return origin object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnOrigin Origin() const
			{
				return Reflect<GnOrigin>();
			}

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 *  return rating object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *   Retrieves the official child title.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *   Retrieves the franchise title.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle FranchiseTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_FRANCHISE);
			}

			/**
			 *   Retrieves the series title.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle SeriesTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_SERIES);
			}

			// TODO

			/**
			 *   Retrieves a child product type.
			 * @ingroup GDO_ChildKeys_Products
			 */
			gn_iterable_container<product_iterator> Products();

			/**
			 *   Retrieves a child credit type. For music types, this includes
			 *   all credits except the artist credit.
			 * @ingroup GDO_ChildKeys_Credit
			 */
			gn_iterable_container<video_credit_iterator> VideoCredits() const
			{
				gn_gdo_provider<GnVideoCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<video_credit_iterator>(video_credit_iterator(provider, 1, count), video_credit_iterator(provider, count, count) );
			}

			/**
			 *  Retrieve a child Season type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_SEASON GDOs.
			 *
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *  video Season to retrieve.
			 *  Video product types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SEASON GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<season_iterator> Seasons() const
			{
				gn_gdo_provider<GnVideoSeason> provider(*this, GNSDK_GDO_CHILD_VIDEO_SEASON);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<season_iterator>(season_iterator(provider, 1, count), season_iterator(provider, count, count) );
			}

			/**
			 * provide iterator over series
			 *  Retrieve a child Series type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *   GNSDK_GDO_TYPE_VIDEO_SERIES GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *   video Series to retrieve.
			 *  Video Series types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<series_iterator> Series() const
			{
				gn_gdo_provider<GnVideoSeries> provider(*this, GNSDK_GDO_CHILD_VIDEO_SERIES);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<series_iterator>(series_iterator(provider, 1, count), series_iterator(provider, count, count) );
			}

			/**
			 *  provide iterator over external IDs
			 *  Retrieves an extended ID type (retrievable from most GDOs). This child GDO supports Link data.
			 *  GNSDK_GDO_CHILD_EXTERNAL_ID
			 *  GNSDK_GDO_TYPE_ALBUM GNSDK_GDO_TYPE_CONTRIBUTOR GNSDK_GDO_TYPE_TRACK
			 *   GNSDK_GDO_TYPE_VIDEOCLIP GNSDK_GDO_TYPE_VIDEO_PRODUCT GNSDK_GDO_TYPE_VIDEO_SEASON
			 *   GNSDK_GDO_TYPE_VIDEO_SERIES GNSDK_GDO_TYPE_VIDEO_WORK
			 * @ingroup GDO_ChildKeys_ExternalId
			 */

			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoFeature
		*/

		class GnVideoFeature : public GnDataObject
		{
		public:
			GnVideoFeature() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoFeature(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnVideoFeature()  { }

			/**
			 *  Retrieves an ordinal of the current type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Ordinal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ORDINAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the matched boolean value for the current type; this indicates whether this type
			 *   is the one that matched the input criteria. Available from many video types.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool Matched() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_MATCHED);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Retrieves a video feature type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoFeatureType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_FEATURE_TYPE);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoProductionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE);
			}

			/**
			 *  Retrieves a video production type value from a GDO.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t VideoProductionTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a release date value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  For video Features or video Products, this retrieves the respective video item release date.
			 *   Release date values are not always available.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_RELEASE);
			}

			/**
			 *  Retrieves a release date from the current type. Available for video Products, Features, and
			 *  Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateOriginalRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_ORIGINAL_RELEASE);
			}

			/**
			 *  Retrieves the notes for the current type. Available from most video types.
			 *  GNSDK_GDO_VALUE_NOTES
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Notes() const
			{
				return StringValue(GNSDK_GDO_VALUE_NOTES);
			}

			/**
			 *  Retrieves the aspect ratio for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatio() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO);
			}

			/**
			 *  Retrieves the aspect ratio for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatioType() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO_TYPE);
			}

			/**
			 *  Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *   program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *   Chapters, Features, Products, Seasons, Series, and Works.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *  Retrieves a plot summary value from a GDO.
			 *  GNSDK_GDO_VALUE_PLOT_SUMMARY
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSummary() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SUMMARY);
			}

			/**
			 *  Retrieves a plot synopsis for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsis() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS);
			}

			/**
			 *  Retrieves a plot tagline value from a GDO.
			 *  GNSDK_GDO_VALUE_PLOT_TAGLINE
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotTagline() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_TAGLINE);
			}

			/**
			 *  Retrieves the available language value for a returned GNSDK_GDO_VALUE_PLOT_SYNOPSIS object.
			 *  <p><b>Remarks:</b></p>
			 *  Use this value to retrieve the actual language value of a returned object.
			 *  The language value of a returned object depends on availability. Information in the language set
			 *   for the locale may not be available, and the object's information may be available only in its
			 *   default official language. For example, if a locale's set language is Spanish, but the object's
			 *   information is available only in English, this value returns as English.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t PlotSynopsisLanguage() const
			{
				return StringValue(GNSDK_GDO_VALUE_PLOT_SYNOPSIS_LANGUAGE);
			}

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			/**
			 *   Retrieves the official child title GDO.
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *  return rating object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *  Retrieves a child Video Work type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_WORK GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   video Work to retrieve.
			 *  Video Work types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			GnVideoWork Works() const
			{
				return ChildGet<GnVideoWork>(GNSDK_GDO_CHILD_VIDEO_WORK);
			}

			/**
			 *  Retrieves a collection over child video chapter type collection.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_CHAPTER GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *   video chapter to retrieve.
			 *  Video chapter types are generally available from
			 *  GNSDK_GDO_TYPE_VIDEO_FEATURE GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<chapter_iterator> Chapters() const
			{
				gn_gdo_provider<GnVideoChapter> provider(*this, GNSDK_GDO_CHILD_VIDEO_CHAPTER);
				gnsdk_uint32_t                  count = provider.count() + 1;
				return gn_iterable_container<chapter_iterator>(chapter_iterator(provider, 1, count), chapter_iterator(provider, count, count) );
			}

			/**
			 *   Retrieves a child credit type. For music types, this includes
			 *   all credits except the artist credit.
			 * @ingroup GDO_ChildKeys_Credit
			 */
			gn_iterable_container<video_credit_iterator> VideoCredits() const
			{
				gn_gdo_provider<GnVideoCredit> provider(*this, GNSDK_GDO_CHILD_CREDIT);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<video_credit_iterator>(video_credit_iterator(provider, 1, count), video_credit_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoLayer
		*/
		class GnVideoLayer : public GnDataObject
		{
		public:
			GnVideoLayer() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoLayer(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnVideoLayer() { }

			/**
			 *  Retrieves an ordinal of the current type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Ordinal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ORDINAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the matched boolean value for the current type; this indicates whether this type
			 *   is the one that matched the input criteria. Available from many video types.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool Matched() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_MATCHED);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Retrieves the aspect ratio for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatio() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO);
			}

			/**
			 *  Retrieves the aspect ratio for the current type. Available from many video types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatioType() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO_TYPE);
			}

			/**
			 *  Retrieves a TV system value (like NTSC) from the current type. Available from many video
			 *   types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t TvSystem() const
			{
				return StringValue(GNSDK_GDO_VALUE_TV_SYSTEM);
			}

			/**
			 *  Retrieves a region code (for example, FE) for the current type. Available from many video
			 *   types.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t RegionCode() const
			{
				return StringValue(GNSDK_GDO_VALUE_REGION_CODE);
			}

			/**
			 *  Retrieves a video product region value from the current type. Available from many video
			 *   types.
			 *  Example: 1
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoRegion() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_REGION);
			}

			/**
			 *  Retrieves a video product region description for the current type. Available from many video
			 *   types.
			 *  Example
			 *  USA, Canada, US Territories, Bermuda, and Cayman Islands
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoRegionDesc() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_REGION_DESC);
			}

			/**
			 *  Retrieves a media type value from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  This is a list-based value requiring that the list be loaded into memory through a successful
			 *   call to gnsdk_manager_locale_load.
			 *
			 *  To render locale-dependent information for list-based values, the application must call
			 *   <code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *   a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *   information.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t MediaType() const
			{
				return StringValue(GNSDK_GDO_VALUE_MEDIA_TYPE);
			}

			/**
			 *  Retrieves an iterator over child video feature type of collection.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_FEATURE GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *   video feature to retrieve.
			 *  Video feature types are generally available from GNSDK_GDO_TYPE_VIDEO_LAYER GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<feature_iterator> Features() const
			{
				gn_gdo_provider<GnVideoFeature> provider(*this, GNSDK_GDO_CHILD_VIDEO_FEATURE);
				gnsdk_uint32_t                  count = provider.count() + 1;
				return gn_iterable_container<feature_iterator>(feature_iterator(provider, 1, count), feature_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoSide
		*/
		class GnVideoSide : public GnDataObject
		{
		public:
			GnVideoSide() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoSide(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnVideoSide() { }

			/**
			 *  Retrieves an ordinal of the current type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Ordinal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ORDINAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the matched boolean value for the current type; this indicates whether this type
			 *   is the one that matched the input criteria. Available from many video types.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool Matched() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_MATCHED);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Retrieves the notes for the current type. Available from most video types.
			 *  GNSDK_GDO_VALUE_NOTES
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Notes() const
			{
				return StringValue(GNSDK_GDO_VALUE_NOTES);
			}

			/**
			 *  return object of official title
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *  Retrieves an iterator over child video layer type collection.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_LAYER GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *   video layer to retrieve.
			 *  Video layer types are generally available from GNSDK_GDO_TYPE_VIDEO_DISC GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<layer_iterator> Layers() const
			{
				gn_gdo_provider<GnVideoLayer> provider(*this, GNSDK_GDO_CHILD_VIDEO_LAYER);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<layer_iterator>(layer_iterator(provider, 1, count), layer_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoDisc
		*/
		class GnVideoDisc : public GnDataObject
		{
		public:
			GnVideoDisc() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoDisc(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnVideoDisc(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_VIDEO_DISC) { }

			virtual ~GnVideoDisc()  { }

			/**
			 *   Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *   Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *  Retrieves a product ID for the current product from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  Available for most types, this retrieves a value which can be stored or transmitted. This
			 *   value can be used as a static identifier for the current content as it will not change over time.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *  Retrieves a TUI value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *  Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *  Retrieves an ordinal of the current type.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t Ordinal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_ORDINAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the matched boolean value for the current type; this indicates whether this type
			 *   is the one that matched the input criteria. Available from many video types.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool Matched() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_MATCHED);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Retrieves the notes for the current type. Available from most video types.
			 *  GNSDK_GDO_VALUE_NOTES
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Notes() const
			{
				return StringValue(GNSDK_GDO_VALUE_NOTES);
			}

			/**
			 *  return object of official title
			 * @ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *  Retrieves an iterator over child video Side type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_SIDE GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *   video Side to retrieve.
			 *  Video Side types are generally available from GNSDK_GDO_TYPE_VIDEO_DISC GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<side_iterator> Sides() const
			{
				gn_gdo_provider<GnVideoSide> provider(*this, GNSDK_GDO_CHILD_VIDEO_SIDE);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<side_iterator>(side_iterator(provider, 1, count), side_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnVideoProduct
		*/
		class GnVideoProduct : public GnDataObject
		{
		public:
			GnVideoProduct() : GnDataObject(GNSDK_NULL_GDO) { }
			GnVideoProduct(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }
			GnVideoProduct(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_VIDEO_PRODUCT) { }

			virtual ~GnVideoProduct()  { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 *	<p><b>Remarks:</b></p>
			 *	Available for the following music and video types:
			 *  <ul>
			 *  <li>GNSDK_GDO_TYPE_ALBUM
			 *  <li>GNSDK_GDO_TYPE_CONTRIBUTOR
			 *  <li>GNSDK_GDO_TYPE_TRACK
			 *  <li>GNSDK_GDO_TYPE_VIDEO_PRODUCT
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SEASON
			 *  <li>GNSDK_GDO_TYPE_VIDEO_SERIES
			 *  <li>GNSDK_GDO_TYPE_VIDEO_WORK
			 *  </ul>
			 *	<p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *	depends on data availability.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *	Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *	Retrieves a product ID for the current product from a GDO.
			 *	<p><b>Remarks:</b></p>
			 *	Available for most types, this retrieves a value which can be stored or transmitted. This
			 *	value can be used as a static identifier for the current content as it will not change over time.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *	Retrieves a TUI value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *	Retrieves a TUI Tag value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *	Retrieves the package language as text (for example, "English").
			 *  <p><b>Remarks:</b></p>
			 *	Use this value to retrieve the actual language value of a returned object.
			 *	The language value of a returned object depends on availability. Information in the language set
			 *	for the locale may not be available, and the object's information may be available only in its
			 *	default official language. For example, if a locale's set language is Spanish, but the object's
			 *	information is available only in English, this value returns as English.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_cstr_t PackageLanguageDisplay() const
			{
				return StringValue(GNSDK_GDO_VALUE_PACKAGE_LANGUAGE_DISPLAY);
			}

			/**
			 *	Retrieves a language value from a GDO as the ISO code (for example, "eng").
			 *	<p><b>Remarks:</b></p>
			 *	GNSDK supports a subset of the ISO 639-2 Language Code List. For more information, refer to ISO
			 *	639-2 Language Code List: Codes for the representation of names of languages.
			 *	Specify a locale language's lower-case three-letter code, which is shown in the macro's C/C++
			 *	syntax section.
			 *	<p><b>Note:</b></p>
			 *   The following languages use Gracenote-specific three-letter codes:
			 *  <ul>
			 *  <li>qtb (Simplified Chinese)*
			 *  <li>qtd (Traditional Chinese)*
			 *  </ul>
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_cstr_t PackageLanguage() const
			{
				return StringValue(GNSDK_GDO_VALUE_PACKAGE_LANGUAGE);
			}

			/**
			 *	Retrieves a video production type value from a GDO.
			 *	<p><b>Remarks:</b></p>
			 *	This is a list-based value requiring that the list be loaded into memory through a successful
			 *	call to gnsdk_manager_locale_load.
			 *
			 *	To render locale-dependent information for list-based values, the application must call
			 *	<code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *	a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *	information.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoProductionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE);
			}

			/**
			 *	Retrieves a video production type value from a GDO.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t VideoProductionTypeID() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_VIDEO_PRODUCTION_TYPE_ID);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves a release date from the current type. Available for video Products, Features, and
			 *	Works.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateOriginalRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_ORIGINAL_RELEASE);
			}

			/**
			 *	Retrieves a release date value from a GDO.
			 *	<p><b>Remarks:</b></p>
			 *	For video Features or video Products, this retrieves the respective video item release date.
			 *	Release date values are not always available.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateRelease() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_RELEASE);
			}

			/**
			 *	Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *	program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *	Chapters, Features, Products, Seasons, Series, and Works.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *	Retrieves the aspect ratio for the current type. Available from many video types.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatio() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO);
			}

			/**
			 *	Retrieves the aspect ratio for the current type. Available from many video types.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t AspectRatioType() const
			{
				return StringValue(GNSDK_GDO_VALUE_ASPECT_RATIO_TYPE);
			}

			/**
			 *	Retrieves a video product region value from the current type. Available from many video
			 *	types.
			 *	Example: 1
			 *	<p><b>Remarks:</b></p>
			 *	This is a list-based value requiring that the list be loaded into memory through a successful
			 *	call to <code>gnsdk_manager_locale_load</code>.
			 *	To render locale-dependent information for list-based values, the application must call
			 *	<code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *	a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *	information.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoRegion() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_REGION);
			}

			/**
			 *	Retrieves a video product region description for the current type. Available from many video
			 *	types.
			 *	Example
			 *	USA, Canada, US Territories, Bermuda, and Cayman Islands
			 *	<p><b>Remarks:</b></p>
			 *	This is a list-based value requiring that the list be loaded into memory through a successful
			 *	call to <code>gnsdk_manager_locale_load</code>.
			 *
			 *	To render locale-dependent information for list-based values, the application must call
			 *	<code>gnsdk_manager_locale_load</code> and possibly also <code>gnsdk_sdkmanager_gdo_set_locale</code>. The application returns
			 *	a <code>LocaleNotLoaded</code> message when locale information is not set prior to a request for list-based value
			 *	information.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t VideoRegionDesc() const
			{
				return StringValue(GNSDK_GDO_VALUE_VIDEO_REGION_DESC);
			}

			/**
			 *	Retrieves the notes for the current type. Available from most video types.
			 *	GNSDK_GDO_VALUE_NOTES
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t Notes() const
			{
				return StringValue(GNSDK_GDO_VALUE_NOTES);
			}

			/**
			 *	Retrieves a numerical value indicating if commerce type data exists for a video disc.
			 *	<p><b>Remarks:</b></p>
			 *	For information on the specific values this key retrieves, contact your Gracenote Professional
			 *	Services representative.
			 *
			 *  <p><b>Related Topics</b></p>
			 *  <ul>
			 *	<li><code>gnsdk_videoid_query_set_toc_string</code></li>
			 *   <li>GNSDK_VIDEOID_OPTION_QUERY_ENABLE_COMMERCE_TYPE</li>
			 *   <li>GNSDK_GDO_TYPE_VIDEO_PRODUCT</li>
			 *   </ul>
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t CommerceType() const
			{
				return StringValue(GNSDK_GDO_VALUE_COMMERCE_TYPE);
			}

			// TODO:

			/**
			 *  return genre object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnGenre Genre() const
			{
				return Reflect<GnGenre>();
			}

			// TODO:

			/**
			 *  return rating object.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *	Retrieves the official child title GDO.
			 *	@ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *	Provides an iterator over extended ID types (retrievable from most GDOs). This child GDO supports Link data.
			 *	GNSDK_GDO_CHILD_EXTERNAL_ID
			 *	GNSDK_GDO_TYPE_ALBUM GNSDK_GDO_TYPE_CONTRIBUTOR GNSDK_GDO_TYPE_TRACK
			 *	GNSDK_GDO_TYPE_VIDEOCLIP GNSDK_GDO_TYPE_VIDEO_PRODUCT GNSDK_GDO_TYPE_VIDEO_SEASON
			 *	GNSDK_GDO_TYPE_VIDEO_SERIES GNSDK_GDO_TYPE_VIDEO_WORK
			 * @ingroup GDO_ChildKeys_ExternalId
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}

			/**
			 *	Provides an iterator over child video disc types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_DISC GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *	video disc to retrieve.
			 *
			 *  Video disc types are generally available from
			 *  GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<disc_iterator> Discs() const
			{
				gn_gdo_provider<GnVideoDisc> provider(*this, GNSDK_GDO_CHILD_VIDEO_DISC);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<disc_iterator>(disc_iterator(provider, 1, count), disc_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnTVProgram
		*/
		class GnTVProgram : public GnDataObject
		{
		public:
			GnTVProgram(gnsdk_gdo_handle_t gdoHandle = GNSDK_NULL_GDO) : GnDataObject(gdoHandle) { }
			GnTVProgram(gnsdk_cstr_t id, gnsdk_cstr_t id_tag) : GnDataObject(id, id_tag, GNSDK_ID_SOURCE_TVPROGRAM) { }

			virtual ~GnTVProgram()  { }

			/**
			 *  Retrieves a value indicating if a GDO type response result is full (not partial).
			 *  Returns true if full, false if partial.
			 * <p><b>Note:</b></p>
			 *   What constitutes a full result varies among the individual response types and results, and also
			 *  depends on data availability.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			bool FullResult() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_FULL_RESULT);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Retrieves a GNID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *	Retrieves a GNUID value for a product from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *	Retrieves a product ID for the current product from a GDO.
			 *  <p><b>Remarks:</b></p>
			 *  Available for most types, this retrieves a value which can be stored or transmitted. This
			 *	value can be used as a static identifier for the current content as it will not change over time.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *	Retrieves a TUI value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *	Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			GnCredit Credit() const
			{
				return ChildGet<GnCredit>(GNSDK_GDO_CHILD_CREDIT);
			}

			GnVideoWork Work() const
			{
				return ChildGet<GnVideoWork>(GNSDK_GDO_CHILD_VIDEO_WORK);
			}
		};

#if ( defined( GNSDK_ACR ) || defined( GNSDK_EPG ) )

		/******************************************************************************
		** GnTVChannel
		*/
		class GnTVChannel : public GnDataObject
		{
		public:
			GnTVChannel() : GnDataObject(GNSDK_NULL_GDO) { }
			GnTVChannel(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnTVChannel() { }

			/**
			 *	Retrieves a GNID value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNID);
			}

			/**
			 *	Retrieves a GNUID value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t GnUID() const
			{
				return StringValue(GNSDK_GDO_VALUE_GNUID);
			}

			/**
			 *	Retrieves a Product ID value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t ProductID() const
			{
				return StringValue(GNSDK_GDO_VALUE_PRODUCTID);
			}

			/**
			 *	Retrieves a TUI value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *	Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *	Retrieves a Rank value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t Rank() const
			{
				return StringValue(GNSDK_GDO_VALUE_RANK);
			}

			/**
			 *	Retrieves the channel number
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelNumber() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_NUM);
			}

			/**
			 *	Retrieves the channel name
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelName() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_NAME);
			}

			/**
			 *	Retrieves the channel callsign
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelCallsign() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_CALLSIGN);
			}

			/**
			 *	Retrieves the identifier
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t Identifier() const
			{
				return StringValue(GNSDK_GDO_VALUE_IDENT);
			}

			/**
			 *	Retrieves the channel callsign
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t OnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_ONID);
			}

			/**
			 *	Retrieves the channel callsign
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t TSID() const
			{
				return StringValue(GNSDK_GDO_VALUE_TSID);
			}

			/**
			 *	Retrieves the channel callsign
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t SID() const
			{
				return StringValue(GNSDK_GDO_VALUE_SID);
			}

			/**
			 *  Retrieves an extended ID type (retrievable from most GDOs). This child GDO supports Link data.
			 * @ingroup GDO_ChildKeys_ExternalId
			 */
			gn_iterable_container<externalid_iterator> ExternalIDs() const
			{
				gn_gdo_provider<GnExternalID> provider(*this, GNSDK_GDO_CHILD_EXTERNAL_ID);
				gnsdk_uint32_t                count = provider.count() + 1;
				return gn_iterable_container<externalid_iterator>(externalid_iterator(provider, 1, count), externalid_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnTVAiring
		*/
		class GnTVAiring : public GnDataObject
		{
		public:
			GnTVAiring(gnsdk_gdo_handle_t gdoHandle = GNSDK_NULL_GDO) : GnDataObject(gdoHandle) { }

			virtual ~GnTVAiring() { }

			/**
			 *	Retrieves a TUI value from a GDO.
			 *	@ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUI() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI);
			}

			/**
			 *	Retrieves a TUI Tag value from a GDO.
			 * @ingroup GDO_ValueKeys_GracenoteIDs
			 */
			gnsdk_cstr_t TUITag() const
			{
				return StringValue(GNSDK_GDO_VALUE_TUI_TAG);
			}

			/**
			 *	Retrieves a date start value
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateStart() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_START);
			}

			/**
			 *	Retrieves a date end value
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DateEnd() const
			{
				return StringValue(GNSDK_GDO_VALUE_DATE_END);
			}

			/**
			 *	Retrieves a duration value in seconds from the current type, such as "3600" for a 60-minute
			 *   program. Available for video Chapters, Features, Products, Seasons, Series, and Works.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_int32_t Duration() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_DURATION);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves a duration units value (seconds, "SEC") from the current type. Available for video
			 *   Chapters, Features, Products, Seasons, Series, Works, and ACR.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t DurationUnits() const
			{
				return StringValue(GNSDK_GDO_VALUE_DURATION_UNITS);
			}

			/**
			 *	Retrieves a EPG viewing type
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t EpgViewingType() const
			{
				return StringValue(GNSDK_GDO_VALUE_EPGVIEWINGTYPE);
			}

			/**
			 *	Retrieves a EPG audio type
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t EpgAudioType() const
			{
				return StringValue(GNSDK_GDO_VALUE_EPGAUDIOTYPE);
			}

			/**
			 *	Retrieves a EPG video type
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t EpgVideoType() const
			{
				return StringValue(GNSDK_GDO_VALUE_EPGVIDEOTYPE);
			}

			/**
			 *	Retrieves a EPG caption type
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t EpgCaptionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_EPGCAPTIONTYPE);
			}

			/**
			 *	return rating object.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			GnRating Rating() const
			{
				return Reflect<GnRating>();
			}

			/**
			 *  Gives an iterator over child TV Program types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_CHILD_TVPROGRAM GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   TV Program to retrieve.
			 *  TV Programs types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK or GNSDK_GDO_TYPE_RESPONSE_MATCH_ACR GDOs.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gn_iterable_container<tvprogram_iterator> TVProgram() const
			{
				gn_gdo_provider<GnTVProgram> provider(*this, GNSDK_GDO_CHILD_TVPROGRAM);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<tvprogram_iterator>(tvprogram_iterator(provider, 1, count), tvprogram_iterator(provider, count, count) );
			}

			/**
			 *  Gives an iterator over child TV channels types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_CHILD_TVAIRING GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   TV Channel to retrieve.
			 *  TV Channel types are generally available from GNSDK_GDO_TYPE_RESPONSE_MATCH_ACR GDOs.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gn_iterable_container<tvchannel_iterator> TVChannel() const
			{
				gn_gdo_provider<GnTVChannel> provider(*this, GNSDK_GDO_CHILD_TVCHANNEL);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<tvchannel_iterator>(tvchannel_iterator(provider, 1, count), tvchannel_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnAcrMatch
		*/
		class GnAcrMatch : public GnDataObject
		{
		public:
			GnAcrMatch(gnsdk_gdo_handle_t gdoHandle = GNSDK_NULL_GDO) : GnDataObject(gdoHandle) { }

			virtual ~GnAcrMatch() { }

			/**
			 *	Retrieves the channel number
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelNumber() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_NUM);
			}

			/**
			 *	Retrieves the channel name
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelName() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_NAME);
			}

			/**
			 *	Retrieves the channel GNID
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t ChannelGnID() const
			{
				return StringValue(GNSDK_GDO_VALUE_CHANNEL_GNID);
			}

			/**
			 *	Retrieves the match position
			 *	@ingroup GDO_ValueKeys_ACR
			 */
			gnsdk_cstr_t MatchPosition() const
			{
				return StringValue(GNSDK_GDO_VALUE_ACR_MATCH_POSITION);
			}

			/**
			 *	Retrieves the official child title GDO.
			 *	@ingroup GDO_ChildKeys_Title
			 */
			GnTitle OfficialTitle() const
			{
				return ChildGet<GnTitle>(GNSDK_GDO_CHILD_TITLE_OFFICIAL);
			}

			/**
			 *  Gives an iterator over child Video Work types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_WORK GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   video Work to retrieve.
			 *  Video Work types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK GDOs.
			 * @ingroup GDO_ChildKeys_ACR
			 */
			gn_iterable_container<works_iterator> Works() const
			{
				gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
			}

			/**
			 *  Gives an iterator over child TV Program types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_CHILD_TVPROGRAM GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   TV Program to retrieve.
			 *  TV Programs types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK or GNSDK_GDO_TYPE_RESPONSE_MATCH_ACR GDOs.
			 * @ingroup GDO_ChildKeys_ACR
			 */

// SCH: Currently functionality in the data model, but not implemented in ACR.
// gn_iterable_container<tvprogram_iterator> TVProgram() const
// {
// gn_gdo_provider<GnTVProgram> provider(*this, GNSDK_GDO_CHILD_TVPROGRAM);
// gnsdk_uint32_t count = provider.count() + 1;
// return gn_iterable_container<tvprogram_iterator>(tvprogram_iterator(provider, 1, count), tvprogram_iterator(provider, count, count));
// }

			/**
			 *  Gives an iterator over child TV Airing types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_CHILD_TVAIRING GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   TV Program to retrieve.
			 *  TV Airing types are generally available from GNSDK_GDO_TYPE_RESPONSE_MATCH_ACR GDOs.
			 * @ingroup GDO_ChildKeys_ACR
			 */
			gn_iterable_container<tvairing_iterator> TVAiring() const
			{
				gn_gdo_provider<GnTVAiring> provider(*this, GNSDK_GDO_CHILD_TVAIRING);
				gnsdk_uint32_t              count = provider.count() + 1;
				return gn_iterable_container<tvairing_iterator>(tvairing_iterator(provider, 1, count), tvairing_iterator(provider, count, count) );
			}
		};
#endif

		/******************************************************************************
		** GnResponseVideoSuggestions
		*/
		class GnResponseVideoSuggestions : public GnDataObject
		{
		public:
			GnResponseVideoSuggestions() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoSuggestions(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoSuggestions() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  The total value may be estimated.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Retrieves the text for the current search suggestion type.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SuggestionText() const
			{
				return StringValue(GNSDK_GDO_VALUE_SUGGESTION_TEXT);
			}

			/**
			 *	Retrieves the title for the search suggestion type for the current ordinal value.
			 * @ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SuggestionTitle(gnsdk_uint32_t ordinal)
			{
				return StringValue(GNSDK_GDO_VALUE_SUGGESTION_TITLE, ordinal);
			}

			/**
			 *	Retrieves the type for the current search suggestion type; this value is only available for a
			 *	video product title search.
			 *	@ingroup GDO_ValueKeys_Video
			 */
			gnsdk_cstr_t SuggestionType() const
			{
				return StringValue(GNSDK_GDO_VALUE_SUGGESTION_TYPE);
			}
		};

		/******************************************************************************
		** GnResponseVideoObjects
		*/
		class GnResponseVideoObjects : public GnDataObject
		{
		public:
			GnResponseVideoObjects(gnsdk_gdo_handle_t gdoHandle = GNSDK_NULL_GDO) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoObjects() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  The total value may be estimated.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			gn_iterable_container<product_iterator> Products() const
			{
				gn_gdo_provider<GnVideoProduct> provider(*this, GNSDK_GDO_CHILD_VIDEO_PRODUCT);
				gnsdk_uint32_t                  count = provider.count() + 1;
				return gn_iterable_container<product_iterator>(product_iterator(provider, 1, count), product_iterator(provider, count, count) );
			}

			gn_iterable_container<works_iterator> Works() const
			{
				gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
			}

			gn_iterable_container<season_iterator> Seasons() const
			{
				gn_gdo_provider<GnVideoSeason> provider(*this, GNSDK_GDO_CHILD_VIDEO_SEASON);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<season_iterator>(season_iterator(provider, 1, count), season_iterator(provider, count, count) );
			}

			gn_iterable_container<series_iterator> Series() const
			{
				gn_gdo_provider<GnVideoSeries> provider(*this, GNSDK_GDO_CHILD_VIDEO_SERIES);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<series_iterator>(series_iterator(provider, 1, count), series_iterator(provider, count, count) );
			}

			gn_iterable_container<contributor_iterator> Contributors() const
			{
				gn_gdo_provider<GnContributor> provider(*this, GNSDK_GDO_CHILD_CONTRIBUTOR);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<contributor_iterator>(contributor_iterator(provider, 1, count), contributor_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseVideoProgram
		*/
		class GnResponseVideoProgram : public GnDataObject
		{
		public:
			GnResponseVideoProgram() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoProgram(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoProgram() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	The total value may be estimated.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Provides an iterator over child TVPROGRAM types.
			 *	@ingroup GDO_ChildKeys_Epg
			 */
			gn_iterable_container<tvprogram_iterator> TVProgram() const
			{
				gn_gdo_provider<GnTVProgram> provider(*this, GNSDK_GDO_CHILD_TVPROGRAM);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<tvprogram_iterator>(tvprogram_iterator(provider, 1, count), tvprogram_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseContributor
		*/
		class GnResponseContributor : public GnDataObject
		{
		public:
			GnResponseContributor() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseContributor(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseContributor() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *	<p><b>Remarks:</b></p>
			 *	If you do not set a starting value, the default behavior is to return the first set of results.
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	The total value may be estimated.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Retrieves a child contributor type.
			 *	@ingroup GDO_ChildKeys_Contributor
			 */
			gn_iterable_container<contributor_iterator> Contributors() const
			{
				gn_gdo_provider<GnContributor> provider(*this, GNSDK_GDO_CHILD_CONTRIBUTOR);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<contributor_iterator>(contributor_iterator(provider, 1, count), contributor_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseVideoSeries
		*/
		class GnResponseVideoSeries : public GnDataObject
		{
		public:
			GnResponseVideoSeries() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoSeries(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoSeries() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *	<p><b>Remarks:</b></p>
			 *	If you do not set a starting value, the default behavior is to return the first set of results.
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	The total value may be estimated.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Provide an iterator over child Series type.
			 *	<p><b>Remarks:</b></p>
			 *	Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *	GNSDK_GDO_TYPE_VIDEO_SERIES GDOs.
			 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *	video Series to retrieve.
			 *	Video Series types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES GDOs.
			 *	@ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<series_iterator> Series() const
			{
				gn_gdo_provider<GnVideoSeries> provider(*this, GNSDK_GDO_CHILD_VIDEO_SERIES);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<series_iterator>(series_iterator(provider, 1, count), series_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseVideoSeasons
		*/
		class GnResponseVideoSeasons : public GnDataObject
		{
		public:
			GnResponseVideoSeasons() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoSeasons(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoSeasons()  { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *	<p><b>Remarks:</b></p>
			 *	If you do not set a starting value, the default behavior is to return the first set of results.
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *	<p><b>Remarks:</b></p>
			 *	Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *	The total value may be estimated.
			 *	Range values are available from all response types.
			 *	As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *	<p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *	Provide an iterator over child Season type.
			 *	<p><b>Remarks:</b></p>
			 *	Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *	GNSDK_GDO_TYPE_VIDEO_SEASON GDOs.
			 *
			 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *	video Season to retrieve.
			 *	Video product types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SEASON GDOs.
			 *	@ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<season_iterator> Seasons() const
			{
				gn_gdo_provider<GnVideoSeason> provider(*this, GNSDK_GDO_CHILD_VIDEO_SEASON);
				gnsdk_uint32_t                 count = provider.count() + 1;
				return gn_iterable_container<season_iterator>(season_iterator(provider, 1, count), season_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseVideoWork
		*/
		class GnResponseVideoWork : public GnDataObject
		{
		public:
			GnResponseVideoWork() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoWork(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoWork() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *   responses returned in any one request, so the range values are available to indicate the total
			 *   number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *	The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 *	@ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the estimated total number of results possible.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *   responses returned in any one request, so the range values are available to indicate the total
			 *   number of results, and where the current results fit in within that total.
			 *  The total value may be estimated.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Gives an iterator over child Video Work types.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_WORK GDOs.
			 *  The ordinal parameter for gnsdk_manager_gdo_child_get with this key indicates the n'th child
			 *   video Work to retrieve.
			 *  Video Work types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_WORK GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<works_iterator> Works() const
			{
				gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
				gnsdk_uint32_t               count = provider.count() + 1;
				return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
			}
		};

		/******************************************************************************
		** GnResponseVideoProduct
		*/
		class GnResponseVideoProduct : public GnDataObject
		{
		public:
			GnResponseVideoProduct() : GnDataObject(GNSDK_NULL_GDO) { }
			GnResponseVideoProduct(gnsdk_gdo_handle_t gdoHandle) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseVideoProduct()  { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *   responses returned in any one request, so the range values are available to indicate the total
			 *   number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *   responses returned in any one request, so the range values are available to indicate the total
			 *   number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves the estimated total number of results possible.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *   responses returned in any one request, so the range values are available to indicate the total
			 *   number of results, and where the current results fit in within that total.
			 *  The total value may be estimated.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *   to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *   requested. To accurately iterate through results, always check the range start, end, and total
			 *   values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *   are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *   using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 * Retrieves a boolean value indicating whether the album match(es) need disambiguation.
			 */
			bool NeedsDecision() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_NEEDS_DECISION);

				return gnstd::gn_strtobool(return_value);
			}

			/**
			 *  Retrieves an iterator over child video product type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *	video product to retrieve.
			 *  Video product types are generally available from
			 *  GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
			 * @ingroup GDO_ChildKeys_Video
			 */
			gn_iterable_container<product_iterator> Products() const
			{
				gn_gdo_provider<GnVideoProduct> provider(*this, GNSDK_GDO_CHILD_VIDEO_PRODUCT);
				gnsdk_uint32_t                  count = provider.count() + 1;
				return gn_iterable_container<product_iterator>(product_iterator(provider, 1, count), product_iterator(provider, count, count) );
			}
		};

#if GNSDK_ACR

		/******************************************************************************
		** GnResponseAcrMatch
		*/
		class GnResponseAcrMatch : public GnDataObject
		{
		public:
			GnResponseAcrMatch(gnsdk_gdo_handle_t gdoHandle = GNSDK_NULL_GDO) : GnDataObject(gdoHandle) { }

			virtual ~GnResponseAcrMatch() { }

			gnsdk_int32_t ResultCount() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RESULT_COUNT);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the first result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  If you do not set a starting value, the default behavior is to return the first set of results.
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeStart() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_START);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the ordinal of the last result in the returned range.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeEnd() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_END);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *	Retrieves the estimated total number of results possible.
			 *  <p><b>Remarks:</b></p>
			 *  Range values are available to aid in paging results. Gracenote Service limits the number of
			 *	responses returned in any one request, so the range values are available to indicate the total
			 *	number of results, and where the current results fit in within that total.
			 *  The total value may be estimated.
			 *  Range values are available from all response types.
			 *  As this is a unique value, the ordinal parameter for gnsdk_manager_gdo_value_get must be set to 1
			 *	to retrieve the first (or only) value instance.
			 *  <p><b>Important:</b></p>
			 *  The number of results actually returned for a query may not equal the number of results initially
			 *	requested. To accurately iterate through results, always check the range start, end, and total
			 *	values and the responses returned by Gracenote Service for the query (or queries). Ensure that you
			 *	are incrementing by using the actual last range end value plus one (range_end+1), and not by simply
			 *	using the initial requested value.
			 * @ingroup GDO_ValueKeys_Misc
			 */
			gnsdk_int32_t RangeTotal() const
			{
				gnsdk_cstr_t return_value = StringValue(GNSDK_GDO_VALUE_RESPONSE_RANGE_TOTAL);

				return gnstd::gn_atoi(return_value);
			}

			/**
			 *  Retrieves an iterator over child acr product type.
			 *  <p><b>Remarks:</b></p>
			 *  Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
			 *  GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
			 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
			 *	acr product to retrieve.
			 *  Video product types are generally available from
			 *  GNSDK_GDO_TYPE_RESPONSE_MATCH_ACR GDOs.
			 * @ingroup GDO_ChildKeys_ACR
			 */
			gn_iterable_container<acr_iterator> AcrMatch() const
			{
				gn_gdo_provider<GnAcrMatch> provider(*this, GNSDK_GDO_CHILD_MATCH_ACR);
				gnsdk_uint32_t              count = provider.count() + 1;
				return gn_iterable_container<acr_iterator>(acr_iterator(provider, 1, count), acr_iterator(provider, count, count) );
			}
		};
#endif

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<product_iterator> GnVideoWork::Products()
		{
			gn_gdo_provider<GnVideoProduct> provider(*this, GNSDK_GDO_CHILD_VIDEO_PRODUCT);
			gnsdk_uint32_t                  count = provider.count() + 1;
			return gn_iterable_container<product_iterator>(product_iterator(provider, 1, count), product_iterator(provider, count, count) );
		}

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<product_iterator> GnVideoSeries::Products()
		{
			gn_gdo_provider<GnVideoProduct> provider(*this, GNSDK_GDO_CHILD_VIDEO_PRODUCT);
			gnsdk_uint32_t                  count = provider.count() + 1;
			return gn_iterable_container<product_iterator>(product_iterator(provider, 1, count), product_iterator(provider, count, count) );
		}

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<product_iterator> GnVideoSeason::Products()
		{
			gn_gdo_provider<GnVideoProduct> provider(*this, GNSDK_GDO_CHILD_VIDEO_PRODUCT);
			gnsdk_uint32_t                  count = provider.count() + 1;
			return gn_iterable_container<product_iterator>(product_iterator(provider, 1, count), product_iterator(provider, count, count) );
		}

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<works_iterator> GnVideoCredit::Works()
		{
			gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
			gnsdk_uint32_t               count = provider.count() + 1;
			return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
		}

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<works_iterator> GnVideoSeason::Works()
		{
			gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
			gnsdk_uint32_t               count = provider.count() + 1;
			return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
		}

		/**
		 *	Retrieves an iterator over child video product type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for <code>gnsdk_manager_gdo_child_get</code> to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_PRODUCT GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video product to retrieve.
		 *	Video product types are generally available from
		 *	GNSDK_GDO_TYPE_RESPONSE_VIDEO GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<works_iterator> GnVideoSeries::Works()
		{
			gn_gdo_provider<GnVideoWork> provider(*this, GNSDK_GDO_CHILD_VIDEO_WORK);
			gnsdk_uint32_t               count = provider.count() + 1;
			return gn_iterable_container<works_iterator>(works_iterator(provider, 1, count), works_iterator(provider, count, count) );
		}

		/**
		 *	Retrieve a child Series type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_SERIES GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video Series to retrieve.
		 *	Video Series types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<series_iterator> GnVideoCredit::Series()
		{
			gn_gdo_provider<GnVideoSeries> provider(*this, GNSDK_GDO_CHILD_VIDEO_SERIES);
			gnsdk_uint32_t                 count = provider.count() + 1;
			return gn_iterable_container<series_iterator>(series_iterator(provider, 1, count), series_iterator(provider, count, count) );
		}

		/**
		 *	Retrieve a child Season type.
		 *  <p><b>Remarks:</b></p>
		 *  Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
		 *  GNSDK_GDO_TYPE_VIDEO_SEASON GDOs.
		 *
		 *  The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *  video Season to retrieve.
		 *  Video product types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SEASON GDOs.
		 *  @ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<season_iterator> GnVideoCredit::Seasons()
		{
			gn_gdo_provider<GnVideoSeason> provider(*this, GNSDK_GDO_CHILD_VIDEO_SEASON);
			gnsdk_uint32_t                 count = provider.count() + 1;
			return gn_iterable_container<season_iterator>(season_iterator(provider, 1, count), season_iterator(provider, count, count) );
		}

		/**
		 *	Retrieve a child Series type.
		 *	<p><b>Remarks:</b></p>
		 *	Key is used as parameter for gnsdk_manager_gdo_child_get to retrieve child
		 *	GNSDK_GDO_TYPE_VIDEO_SERIES GDOs.
		 *	The ordinal parameter for <code>gnsdk_manager_gdo_child_get</code> with this key indicates the n'th child
		 *	video Series to retrieve.
		 *	Video Series types are generally available from GNSDK_GDO_TYPE_RESPONSE_VIDEO_SERIES GDOs.
		 *	@ingroup GDO_ChildKeys_Video
		 */
		inline gn_iterable_container<series_iterator> GnVideoSeason::Series()
		{
			gn_gdo_provider<GnVideoSeries> provider(*this, GNSDK_GDO_CHILD_VIDEO_SERIES);
			gnsdk_uint32_t                 count = provider.count() + 1;
			return gn_iterable_container<series_iterator>(series_iterator(provider, 1, count), series_iterator(provider, count, count) );
		}
	} // namespace metadata_video
}     // namespace gracenote
#endif // _METADATA_VIDEO_HPP_
