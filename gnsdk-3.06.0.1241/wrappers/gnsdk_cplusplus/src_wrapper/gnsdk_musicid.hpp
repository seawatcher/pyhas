/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_MUSICID_HPP_
#define _GNSDK_MUSICID_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "metadata_music.hpp"

namespace gracenote
{
	using namespace metadata;

	namespace musicid
	{
		/**
		 *  The Gracenote MusicID library provides for audio recognition using CD TOC-based search,
		 *  text-based search, fingerprint, and identifier lookup functionality.
		 *
		 *  Note: Customers must be licensed to implement use of a MusicID product in an application. 
		 *  Contact your Gracenote representative with questions about product licensing and entitlement.
		 *
		 *  Important: You must initialize the GNSDK DSP (digital signal processing) library before 
		 *  initializing the MusicID library, as the DSP library's functionality is necessary for MusicID's 
		 *  audio recognition functionality.
		 */
		class GnMusicID : public GnObject
		{
		public:


			/**
			 *  Initializes the MusicID library.
			 *  @param user set GnUser object representing the user making the GnMusicID request
			 *  @param pEventHandler set Optional status event handler to get bytes sent, received, or completed.
			 */
			GnMusicID(const GnUser& user, GnStatusEvents* pEventHandler = GNSDK_NULL) throw ( GnError );
			virtual ~GnMusicID( );


			/**
			 *  Retrieves the MusicID library version string.
			 *  @return gnsdk_cstr_t Version string, if successful
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. 
			 *  The returned string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example version string: 1.2.3.123 (Major.Minor.Improvement.Build)
			 *
			 *  Major: New functionality
			 *
			 *  Minor: New or changed features
			 *
			 *  Improvement: Improvements and fixes
			 *
			 *  Build: Internal build number
			 */
			gnsdk_cstr_t Version( ); 


			/**
			 *  Retrieves the MusicID SDK's build date string.
			 *  @return gnsdk_cstr_t Build date string of the format: YYYY-MM-DD hh:mm UTC
			 *  <p><b>Remarks:</b></p>
			 *  This API can be called at any time, after getting instance of GnSDK successfully. 
			 *  The returned string is a constant. Do not attempt to modify or delete.
			 *
			 *  Example build date string: 2008-02-12 00:41 UTC
			 */
			gnsdk_cstr_t BuildDate( );


			/**
			 *  Indicates whether the MusicID query should be performed against local embedded databases or online.
			 *  @param lookupMode  One of the #GnLookupMode values
			 */
			void OptionLookupMode(GnLookupMode lookupMode) throw ( GnError );


			/**
			 *  Indicates the lookup data value for the MusicID query.
			 *  @param lookupData One of the #GnLookupData values
			 *  @param bEnable Optional boolean
			 */
			void  OptionLookupData(GnLookupData lookupData, bool bEnable) throw ( GnError );
			
			
			/**
			 *  Indicates the lookup option for the MusicID query.
			 *  @param lookupOption One of the GnSDK preferred data option
			 *  @param bEnable Optional boolean
			 */
			void OptionSet(gnsdk_cstr_t strLookupOption, bool bEnable) throw ( GnError );


			/**
			 *  Indicates the preferred language of the returned results.
			 *  @param preferrredLanguage One of the GnSDK language values
			 */
			void OptionPreferredLanguage(gnsdk_cstr_t strLanguage) throw ( GnError );


			/**
			 *  Indicates the preferred external ID of the returned results.
			 *  @param preferredLanguage Gracenote external ID source name
			 */
			void OptionPreferredExternalID(gnsdk_cstr_t strExternalId) throw ( GnError );

			
			/**
			 *  Indicates using cover art to prefer the returned results.
			 *  @param strPreferCoverart Prefer cover art
			 */
			void OptionPreferredCoverart(gnsdk_cstr_t strCoverart) throw ( GnError );
			/**
			 *  Indicates whether a response must return only the single best result.
			 *  @param bEnable set True to enable, false to disable
			 *  <p><b>Remarks:</b></p>
			 *  If enabled, the MusicID library selects the single best result based on the query type and input.
			 */
			void OptionResultSingle(bool bEnable) throw ( GnError );


				/**
			 *  Enables or disables revision check option.
			 *  @param bEnable set Boolean value
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			void OptionRevisionCheck(bool bEnable) throw ( GnError );


			/**
			 *  Sets the starting range.
			 *  @param rangeStart set Starting range value
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			void OptionRangeStart(gnsdk_uint32_t rangeStart) throw ( GnError );


			/**
			 *  Sets the range size.
			 *  @param rangeSize set range size value
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			void OptionRangeSize(gnsdk_uint32_t rangeSize) throw ( GnError );


			/**
			 *  Retrieves externally- and internally-generated Gracenote
			 *  fingerprint Extraction (GNFPX) or Cantametrix (CMX) fingerprint data.
			 *  @return gnsdk_cstr_t String fingerprint data
			 */
			gnsdk_cstr_t FingerprintDataGet() throw ( GnError );


			/**
			 *  Initializes native fingerprint generation for a MusicID query.
			 *  @param fpDataType One of the #GnFingerprintType fingerprint data types,
			 *  either Gracenote Fingerprint Extraction (GNFPX) or Cantametrix (CMX)
			 *  @param audioSampleRate Sample rate of audio to be provided in Hz (for example,44100)
			 *  @param audioSampleSize Size of a single sample of audio to be provided: 8 for 8-bit audio
			 *  (0-255 integers), 16 for 16-bit audio , and 32 for 32-bit audio (floating point)
			 *  @param audioChannels Number of channels for audio to be provided (1 or 2)
			 */
			void FingerprintBegin(GnFingerprintType fpType, gnsdk_uint32_t audioSampleRate, gnsdk_uint32_t audioSampleSize, gnsdk_uint32_t audioChannels) throw ( GnError );


			/**
			 *  Provides uncompressed audio data to a query for native fingerprint generation and returns
			 *  a boolean value indicating when enough data has been received.
			 *  @param audioData Pointer to audio data buffer that matches audio format described in
			 *  FingerprintBegin().
			 *  @param audioDataSize Size of audio data buffer in bytes
			 *  @return True/False Boolean value indicating whether the fingerprint
			 *  generation process gathered enough audio data
			 *  <p><b>Remarks:</b></p>
			 *  Call this API after FingerprintDataGet() to:
			 *  <ul>
			 *  <li>Generate a native Gracenote Fingerprint Extraction (GNFPX) or Cantametrix (CMX) fingerprint.
			 *  <li>Receive a boolean value indicating whether MusicID has
			 *  received sufficient audio data to generate the fingerprint.
			 *  </ul>
			 *  Additionally, if fingerprints have been generated (as shown by the returned
			 *  value):
			 *  <ul>
			 *  <li>Optimally, stop calling FingerprintWrite after it returns true
			 *  to conserve application resources.
			 *  <li>Call FingerprintDataGet() for cases where the application needs to retrieve the
			 *  raw fingerprint value from storage.
			 *  </ul>
			 */
			bool FingerprintWrite(const gnsdk_byte_t* audioData, gnsdk_size_t audioDataSize) throw ( GnError );


			/**
			 *  Finalizes native fingerprint generation for a MusicID query handle.
			 *  <p><b>Remarks:</b></p>
			 *  Call this API when the audio stream ends; this alerts the system that it has received all the
			   available audio for the particular stream.
			 *  If FingerprintWrite() returns True before the stream ends, we recommend that you:
			 *  <ul>
			 *  <li>Stop providing audio at that time, since enough has been received.
			 *  <li>Do not FingerprintEnd(), as this is unnecessary.
			 *  </ul>
			 *  Fingerprints may be generated based on the call to this API; however, this is not guaranteed.
			 */
			void FingerprintEnd( ) throw ( GnError );

			void FingerprintFromSource(GnAudioSource& audioSource, GnFingerprintType fpType) throw ( GnError );

			/**
			 *  Performs a MusicID query for album results based on text input.
			 *  @param albumTitle set  Album title
			 *  @param trackTitle set  Track title
			 *  @param artistName set  Artist name
			 *  @return GnResponseAlbums
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName) throw ( GnError );
            
            
            /**
			 *  Performs a MusicID query for album results based on text input .
			 *  @param albumTitle           set  Album title
			 *  @param trackTitle           set  Track title
			 *  @param artistName           set  Artist name
			 *  @param albumArtistName      set  Album Artist name
             *  @param trackArtistName      set  Track Artist name
             *  @param composerName         set  Album Composer ( e.g. Classical, Instrumental, Movie Score)
         	 *  @return GnResponseDataMatches
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
            GnResponseAlbums  FindAlbums(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName) throw ( GnError );
			/**
			 *  Performs a MusicID query for album results using CD TOC.
			 *  @param CDTOC CD TOC
			 *  @return GnResponseAlbums
			 *
			 *  Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(gnsdk_cstr_t strCDTOC) throw ( GnError );


			/**
			 *  Performs a MusicID query for album results using CD TOC, fingerprint data and finger print type.
			 *  @param CDTOC  CD TOC
			 *  @param fPData Fingerprint data
			 *  @param fPType One of the #GnFingerprintType fingerprint types
			 *  @return GnResponseAlbums
			 *
			 *  Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(gnsdk_cstr_t strCDTOC, gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType) throw ( GnError );


			/**
			 *  Performs a MusicID query for album results using fingerprint data and finger print type.
			 *  @param fPData set  Fingerprint data
			 *  @param fPType set  One of the #GnFingerprintType fingerprint types
			 *  @return GnResponseAlbums
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType) throw ( GnError );


			/**
			 *  Performs a MusicID query for album results.
			 *  @param gnDataObject Gracenote data object
			 *  @return GnResponseAlbums
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(GnDataObject& gnDataObject) throw ( GnError );

            /**
			 *  Performs a MusicID query for album results.
			 *  @param gnDataObject Gracenote data object
			 *  @return GnResponseAlbums
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseAlbums  FindAlbums(GnAudioSource& audioSource, GnFingerprintType fpType) throw ( GnError );

			/**
			 *  Performs a MusicID query for best Matches results i.e. GnAlbums and/or GnContributors ordered in priority .
			 *  @param albumTitle set  Album title
			 *  @param trackTitle set  Track title
			 *  @param artistName set  Artist name
			 *  @return GnResponseDataMatches
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseDataMatches  FindMatches(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName) throw ( GnError );
            
            /**
			 *  Performs a MusicID query for best Matches results i.e. GnAlbums and/or GnContributors ordered in priority .
			 *  @param albumTitle           set  Album title
			 *  @param trackTitle           set  Track title
			 *  @param artistName           set  Artist name
			 *  @param albumArtistName      set  Album Artist name 
             *  @param trackArtistName      set  Track Artist name
             *  @param composerName         set  Album Composer ( e.g. Classical, Instrumental, Movie Score)
         	 *  @return GnResponseDataMatches
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
            GnResponseDataMatches  FindMatches(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName) throw ( GnError );
            
            
			/**
			 *  Performs a MusicID query for track results.
			 *  @param gnDataObject Gracenote data object
			 *  @return GnResponseTracks
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to retrieve tracks based on an input parameters.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseTracks  FindTracks(GnDataObject& gnDataObject) throw ( GnError );


			/**
			 *  Performs a MusicID query for track results.
			 *  @param fPData set  Fingerprint data
			 *  @param fPType set  One of the #GnFingerprintType fingerprint types
			 *  @return GnResponseTracks
			 *  <p><b>Remarks:</b></p>
			 *  Use this function to retrieve tracks based on an input parameters.
			 *
			 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
			 */
			GnResponseTracks  FindTracks(gnsdk_cstr_t strFingerprintData, GnFingerprintType fpType) throw ( GnError );


		private:
			GnStatusEvents*              mEventHandler;
			gnsdk_musicid_query_handle_t mQueryHandle;


			/**
			 *  Internal method used by FindAlbums()
			 *  @return GnResponseAlbums
			 */
			GnResponseAlbums               _FindAlbums( ) throw ( GnError );

            /**
			 *  Internal method used by SetText()
			 *  @return void
			 */
			void      _SetText(gnsdk_cstr_t albumTitle, gnsdk_cstr_t trackTitle, gnsdk_cstr_t artistName , gnsdk_cstr_t albumArtistName, gnsdk_cstr_t trackArtistName, gnsdk_cstr_t composerName ) throw ( GnError );
            

            
			/**
			 *  Internal method used by FindTracks()
			 *  @return GnResponseTracks
			 */
			GnResponseTracks               _FindTracks( ) throw ( GnError );


			// GnResponseAlbums			   _FindMatches( ) throw	( GnError );


			/**
			 *  Internal method used to convert const to string for Finger Print data type
			 *  @return gnsdk_cstr_t string
			 */
			gnsdk_cstr_t                   _MapfPTypeCStr(GnFingerprintType fpType);


			/**
			 *
			 */
			static void GNSDK_CALLBACK_API _callback_status(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort);


			/* disallow assignment operator */
			DISALLOW_COPY_AND_ASSIGN(GnMusicID);
		};

	} // namespace MusicID
}     // namespace GracenoteSDK
#endif // _GNSDK_MUSICID_HPP_
