package com.gracenote.gnsdk;

import java.io.File;
import java.nio.ByteBuffer;

public class GnAudioFile extends GnAudioSource {

	/***** UNIMPLEMENTED *****/

	
	private File 			file;
	
	public GnAudioFile( File file ){
		this.file = file;
	}
	
	
	@Override
	public long sourceInit() {
		return 1;
	}

	
	@Override
	public void sourceClose() {
	}
	
	
	@Override
	public long samplesPerSecond(){
		return 0;
	}
	
	
	@Override
	public long sampleSizeInBits(){
		return 0;
	}
	
	
	@Override
	public long numberOfChannels(){
		return 0;
	}
	
	
	@Override
	public long getData(ByteBuffer byteBuffer, long numBytesRequested){
		return 0;
	}
	
	
	public String filename () {
		return file.getAbsolutePath();
	}
	
	
	@Override
	public void finalize(){
		try {
			super.finalize();
			sourceClose();
		} catch ( Throwable t ) {
			// ignore
		}
	}
	
	
	/**
	 * Uses the audio file's extension to determine if it is supported by
	 * available decoders
	 * @param audioFilename	Audio file's name
	 * @return True if the file has an extension of a supported format
	 */
	public static boolean isFileFormatSupported( String audioFilename ) {
		return false;
	}
	
}
