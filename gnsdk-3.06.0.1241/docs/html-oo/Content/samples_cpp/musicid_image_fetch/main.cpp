/*
* Copyright (c) 2000-2013 Gracenote.
*
* This software may not be used in any way or distributed without
* permission. All rights reserved.
*
* Some code herein may be covered by US and international patents.
*/

/*
*  Name: musicid_image_fetch/main.cpp
*  Description:
*  This example does a text search and finds images based on album or contributor.
*  It also finds an image based on genre.
*
*  Command-line Syntax:
*  sample clientId clientIdTag licenseFile  gnsdkLibraryPath
*/

#include <iostream>
#include <list>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_musicid.hpp"
#include "gnsdk_link.hpp"
#include "gnsdk_storage_sqlite.hpp"

#include "gnsdk_loader.h"

/* Online vs Local queries
 *	Set to 0 to have the sample perform online queries.
 *   Set to 1 to have the sample perform local queries.
 *    For local queries, a Gracenote local database must be present.
 */
#ifndef USE_LOCAL
	#define USE_LOCAL 0
#endif

using namespace gracenote;
using namespace gracenote::link;
using namespace gracenote::musicid;
using namespace gracenote::storage_sqlite;
using namespace gracenote::lookup_local;


/* Simulate MP3 tag data. */
typedef struct mp3_s
{
	const char*    album_title_tag;
	const char*    artist_name_tag;
	const char*    genre_tag;
} mp3_file_t;


/* Simulate a folder of MP3s.
*
* file 1: Online - At the time of writing, yields an album match but no cover art or artist images are available.
*                  Fetches the genre image for the returned album's genre.
*         Local - Yields no match for the query of "Low Commotion Blues Band" against the sample database.
*                 Uses the genre tag from the file, "Rock", to perform a genre search and fetches the genre image from that.
* file 2: Online - Yields an album match for album tag "Supernatural". Fetches the cover art for that album.
*         Local - Same result.
* file 3: Online - Yields an album matches for Phillip Glass. Fetches the cover art for the first album returned.
*         Local - Yields an artist match for the artist tag "Phillip Glass". Fetches the artist image for Phillip Glass.
*/
static mp3_file_t
_mp3_folder[] =
{
	{"Ask Me No Questions",  "Low Commotion Blues Band",    "Rock"},
	{"Supernatural",         "Santana",                     GNSDK_NULL},
	{GNSDK_NULL,             "Phillip Glass",               GNSDK_NULL}
};

/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	*  status_event
	*/
	void
		status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	*  cancel_check
	*/
	bool
		cancel_check()
	{
		return GNSDK_FALSE;
	}
};

/*-----------------------------------------------------------------------------
 *  fetchImage
 *  Display file size
 */
static bool
fetch_image(GnLink& link,gnsdk_link_content_type_t contentType, const char*  imageTypeStr)
{
	gnsdk_uint32_t  fileSize			  = 0;
	GnLinkContent *linkContent			  = GNSDK_NULL;
	GnImagePreference imgPreference		  = exact;
	bool notFound						  = false;
	GnImageSize imageSize				  = size_170;
	/* Perform the image fetch */
	try
	{
		switch (contentType)
		{
		case gnsdk_link_content_cover_art:
			linkContent = link.CoverArt(imageSize, imgPreference);
			break;
		case gnsdk_link_content_image_artist:
			linkContent = link.ArtistImage(imageSize, imgPreference);
			break;
		case gnsdk_link_content_genre_art:
			linkContent = link.GenreArt(imageSize, imgPreference);
			break;
		}

		fileSize = linkContent->DataSize();	 

		/* Do something with the image, e.g. display, save, etc. Here we just print the size. */
		printf("\nRETRIEVED: %s: %d byte JPEG\n", imageTypeStr, fileSize);	

		notFound = false;

		if(GNSDK_NULL != linkContent){delete linkContent;}
	}
	catch(GnError e){

		/* For image to be fetched, it must exist in the size specified and you must be entitled to fetch images. */
		printf("NOT FOUND: %s\n", imageTypeStr);
		notFound = true;
	}

	return notFound;
}


/*-----------------------------------------------------------------------------
 *  perform_image_fetch
 */
static void perform_image_fetch(GnDataMatch& dataMatch, GnUser& user)
{        
	gnsdk_cstr_t				preferred_image_size	= GNSDK_NULL;
	bool						imgNotFound				= false;


	GnLink link(user,(GnDataObject)dataMatch);

	/* Perform the image fetch */
	if(dataMatch.IsAlbum())
	{
		/* If album type get cover art */
		if(fetch_image(link,gnsdk_link_content_cover_art, "cover art"))
		{
			/* if no cover art, try to get the album's artist image */
			if(fetch_image(link,gnsdk_link_content_image_artist, "artist image"))
			{
				/* if no artist image, try to get the album's genre image so we have something to display */
				fetch_image(link,gnsdk_link_content_genre_art, "genre image");
			}
		}
	}	
	else if(dataMatch.IsContributor())
	{
		/* If contributor type get artist image */
		if(fetch_image(link,gnsdk_link_content_image_artist, "artist image"))
		{
			/* if no artist image, try to get the album's genre image so we have something to display */
			fetch_image(link,gnsdk_link_content_genre_art, "genre image");
		}
	}
	else { printf("Unknown gdo Type, must be ALBUM or CONTRIBUTOR\n");	}

}

/*-----------------------------------------------------------------------------
 *  perform_image_fetch
 *  Query sample text
 */
static int do_sample_text_query(
								gnsdk_cstr_t            album_title,
								gnsdk_cstr_t            artist_name,
								GnUser&     user
								)
{	
	gnsdk_cstr_t					 gdo_type        = GNSDK_NULL;
	gnsdk_uint32_t					 file_size		   = 0;
	int	                             got_match       = 0;

	printf("MusicID Text Match Query\n");

	if ((album_title == NULL) && (artist_name == NULL))	{    printf("Must pass album title or artist name\n");	return -1;	}
	if (album_title != GNSDK_NULL)	{	printf( "%-15s: %s\n", "album title", album_title );	}
	if (artist_name != GNSDK_NULL)  {	printf( "%-15s: %s\n", "artist name", artist_name ); 	}

	GnMusicID musicid(user);
	/* Perform the query */
	GnResponseDataMatches response=musicid.FindMatches(album_title,GNSDK_NULL,artist_name);	
	int count =response.DataMatches().count();
	printf( "Number matches = %d\n\n", count);

	/* Get the first match type */
	if(count > 0 )	{ 	got_match=1; }	/* OK, we got at least one match. */	

	/* Just use the first match for demonstration. */
	GnDataMatch dataMatch = response.DataMatches().at(0).next();
	gdo_type = dataMatch.GetType();
	printf( "First Match GDO type: %s\n", gdo_type);

	perform_image_fetch(dataMatch,user);

	return got_match;
} 

/*-----------------------------------------------------------------------------
 *  find_genre_image
 */
static int find_genre_image(GnUser& user, gnsdk_cstr_t genre, GnList& list)
{

	int gotMatch	=	0;
	printf("Genre String Search\n");

	if (genre == NULL)	{		printf("Must pass a genre\n");		return -1;	}

	printf( "%-15s: %s\n", "genre", genre );
	/* Find the list element for our input string */
	GnListElement listElement(list.ElementByString(genre));	

	printf("List element result: %s (level %d)\n", listElement.DisplayString(), listElement.Level());	

	GnLink link(user, listElement);	
	gotMatch = 1;
	fetch_image(link,gnsdk_link_content_genre_art, "genre image");

	return gotMatch;
}


static void process_sample_mp3(mp3_file_t*  mp3_file,	GnUser&    user, GnList& list)	
{
	int           got_match  = 0;
	/* Do a music text query and fetch image from result. */
	got_match=do_sample_text_query(mp3_file->album_title_tag, mp3_file->artist_name_tag, user);

	/* If there were no results from the musicid query for this file, try looking up the genre tag to get the genre image. */
	if (0 == got_match)
	{
		if (GNSDK_NULL != mp3_file->genre_tag)
		{
			got_match = find_genre_image(user, mp3_file->genre_tag, list);
		}
	}
	/* did we succesfully find a relevant image? */
	if (0 == got_match)
	{
		printf("Because there was no match result for any of the input tags, you may want to associated the generic music image with this track, music_75x75.jpg, music_170x170.jpg or music_300x300.jpg\n");
	}

} 

#if USE_LOCAL
static void display_embedded_db_info()
{
	GnLookupLocal gnLookupLocal;
	int           ordinal       = gnLookupLocal.StorageInfoCount(kMetadata, kGDBVersion);
	gnsdk_cstr_t  gdb_version = gnLookupLocal.StorageInfo(kMetadata, kGDBVersion, ordinal);	
	printf("Gracenote DB Version : %s\n", gdb_version);
}
#endif


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}

/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	/*LookupStatusEvents localeEvents;*/

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, GNSDK_NULL );/*use &statusEvents instead of GNSDK_NULL to view progress*/

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int 
main(int argc, char* argv[])
{
	gnsdk_cstr_t licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t gnsdkLibraryPath	= GNSDK_NULL; 
	gnsdk_cstr_t clientId           = GNSDK_NULL;
	gnsdk_cstr_t clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t applicationVersion = "1.0.0.0";/* Increment with each version of your app */


	/*    Client ID, Client ID Tag, License file and lib path must be passed in */
	if (argc == 5)
	{
		clientId 			= argv[1];
		clientIdTag 		= argv[2];
		licenseFile 		= argv[3];
		gnsdkLibraryPath 	= argv[4];
	}else
	{
		printf("\nUsage:\n%s clientId clientIdTag license gnsdkLibraryPath\n", argv[0]);
		return 0;
	}


	/* set dynamic loader path for GNSDK */
	gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);		

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
			"sample.log",
			GNSDK_LOG_PKG_ALL,
			GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,
			GNSDK_LOG_OPTION_ALL,
			0,                                                          /* Max size of log: 0 means a new log file will be created each run */
			GNSDK_FALSE                                                 /* GNSDK_TRUE = old logs will be renamed and saved */
			);


		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);			


#if USE_LOCAL
		GnStorageSqlite storageSqlite;
		storageSqlite.StorageFolderSet("../../../sample_db");

		user.OptionLookupMode(kLookupModeLocal);

		display_embedded_db_info();
#endif

		LoadLocale(gnsdk, user);

		/* Get the genre list handle. This will come from our pre-loaded locale.
		 * Be sure these params match the ones used in get_locale to avoid loading a different list into memory.
		 *
		 * NB: If you plan to do many genre searches, it is most efficient to get this handle once during
		 * initialization (after setting your locale) and then only release it before shutdown.
		 */

		GnList list(GnSDK::kListTypeGenres, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user);


		/* Simulate iterating a sample of mp3s. */
		for (int file_index = 0; file_index < sizeof(_mp3_folder)/sizeof(mp3_file_t); file_index++)
		{
			printf("\n\n***** Processing File %d *****\n", file_index);
			process_sample_mp3(&_mp3_folder[file_index], user, list);
		}	

	}
	catch(GnError e)
	{
		std::cout << e.ErrorDescription() << std::endl;
		std::cout << std::hex << e.ErrorCode() << std::endl;
	}

	return 0;
}

