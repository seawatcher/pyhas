/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _LIST_H_
#define _LIST_H_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "metadata.hpp"

namespace gracenote
{
	class GnListElement;

	/**
	 * Flags indicating rendering level for a list object, used when rendering a list to XML.
	 */
	enum GnListRenderFlags
	{
		kRenderDefault,

		/**
		 * Minimal information included.
		 */
		kRenderMinimal,

		/**
		 * All information included.
		 */
		kRenderFull
	};

	class list_element_child_provider
	{
	public:
		list_element_child_provider() : elementHandle_(GNSDK_NULL) { }
		list_element_child_provider(gnsdk_list_element_handle_t handle) : elementHandle_(handle) { }
		/* Provider Interface */
		GnListElement                        get_data(gnsdk_uint32_t pos);

	private:
		gnsdk_list_element_handle_t           elementHandle_;
	};

	/**
	 * Element of a Gracenote list.
	 */
	class GnListElement : public GnObject
	{
	public:
		/**
		 *    Constructor
		 */
		GnListElement() {}
		explicit GnListElement(gnsdk_list_element_handle_t handle) : GnObject(handle) { }

		virtual ~GnListElement() {}

		
		/**
		 * Retrieves a display string for a given list element.
		 * <p><b>Remarks:</b></p>
		 * Use this function to directly retrieve the display string from a list element.
		 */
		gnsdk_cstr_t DisplayString() throw ( GnError );

		/**
		 * Retrieves a specified list element ID for a given list element.
		 * <p><b>Remarks:</b></p>
		 * Use this function to retrieve the ID of a list element.
		 */
		gnsdk_uint32_t ID() throw ( GnError );

		/**
		 * Retrieves a list element ID for use in submitting parcels.
		 */
		gnsdk_uint32_t IDForSubmit() throw ( GnError );

		/**
		 * Retrieves the list element's description.
		 */
		gnsdk_cstr_t Description() throw ( GnError );

		/**
		 * The list element's Rating Type ID (available in content ratings list).
		 */
		gnsdk_cstr_t RatingTypeID() throw ( GnError );

		/**
		 * Retrieves the parent element of the given list element.
		 * <p><b>Remarks:</b></p>
		 * When GNSDK Manager throws an error exception with error code SDKMGRERR_NotFound,
		 * then the given element is the top-most parent element.
		 */
		GnListElement Parent()  throw ( GnError );

		/**
		 * Retrieves the hierarchy level for a given list element.
		 */
		gnsdk_uint32_t Level()  throw ( GnError );

		typedef gn_facade_range_iterator<GnListElement, list_element_child_provider>   list_element_child_iterator;

		gn_iterable_container<list_element_child_iterator> Children() throw ( GnError );

		/**
		 * Retrieve this list element's native handle.
		 */
		gnsdk_list_element_handle_t native() const { return get<gnsdk_list_element_handle_t>(); }

	private:
		friend class GnList;
		
	};

	class list_element_provider
	{
	public:
		list_element_provider() : listHandle_(GNSDK_NULL), level_(1) { }
		list_element_provider(gnsdk_list_handle_t handle, gnsdk_uint32_t level) : listHandle_(handle), level_(level)  { }

		/* Provider Interface */
		GnListElement                  get_data(gnsdk_uint32_t pos);

	private:
		gnsdk_list_handle_t             listHandle_;
		gnsdk_uint32_t                  level_;

	};

	/**
	 * Gracenote list.
	 */
	class GnList : public GnObject
	{
	public:
		

		GnList(gnsdk_cstr_t strListType, gnsdk_cstr_t strLanguage, gnsdk_cstr_t strRegion, gnsdk_cstr_t strDescriptor, const GnUser& user, GnStatusEvents* status_events = GNSDK_NULL)  throw ( GnError );
		GnList(gnsdk_cstr_t strListType, GnLocale& locale, const GnUser& user, GnStatusEvents* status_events = GNSDK_NULL)  throw ( GnError );
		GnList(gnsdk_cstr_t strSerialized) throw ( GnError );
		/**
		 * Tests an existing list for updates and downloads a new list, if available. The application must
		 * ensure the List module can contact the Gracenote Service to test for a new list version, by
		 * appropriately configuring the user lookup option.
		 * Note: This function blocks the current thread until the download is complete; set a status callback function to receive progress messages.
		 * @param user				User making the list update request
		 * @return True if an update is available, false otherwise.
		 * <p><b>Remarks:</b></p>
		 * Use this function to periodically update a list. The list will be updated if an update is available.
		 * Optionally an application can check if an update is available before calling this method.
		 *
		 * Long Running Potential: Network I/O, File system I/O (for online query cache or local lookup)
		 */

		bool Update(const GnUser& user) throw ( GnError );

		/**
		 * Tests an existing list to determine if a newer revision is available. If available, the new revision is not downloaded. To download
		 * the new revision the application must call GnList.Update().
		 * The application must ensure the List module can contact the Gracenote Service to test for a new list version, by
		 * appropriately configuring the user lookup option.
		 * Note: This function blocks the current thread until the check is complete; set a status callback function to receive progress messages.
		 * @param user				User making the list update check request
		 * @return True if an update is available, false otherwise.
		 * <p><b>Remarks:</b></p>
		 * Use this function to periodically check Gracenote Service for updates to an existing list handle.
		 * <p><b>Note:</b></p>
		 * You should configure application(s) to automatically check for list updates to ensure use of the
		 * most current data.
		 * You should immediately check for updates after constructing a list object from a saved serialized list string as it may
		 * be out of date.
		 * This function blocks the current thread until the download is complete;
		 * set a status callback function to receive progress messages.
		 *
		 * Long Running Potential: Network I/O
		 */
		bool IsUpdateAvailable(const GnUser& user) throw ( GnError );

		/**
		 * Serializes a list into encrypted text, so the application can store it for later use.
		 * <p><b>Note:</b></p>
		 * If you application is using a GNSDK local storage solution, lists are automatically stored
		 * and retrieved from a local store according to the configuration of user option.
		 * Applications implementing their own local storage functionality can use this method
		 * to render a list into a format that can be stored persistently and restored at a later time using
		 * the appropriate constructor.
		 */
		GnString Serialize() throw ( GnError );

		/**
		 *  Renders list data to XML.
		 *  @param levels			List level values to render
		 *  @param renderFlags		Flags configuring rendering output
		 */
		GnString RenderToXml(gnsdk_uint32_t levels, GnListRenderFlags renderFlags) throw ( GnError );

		/**
		 * Retrieves this list's type.
		 */
		gnsdk_cstr_t Type() throw ( GnError );

		/**
		 * Retrieves this list's descriptor.
		 */
		gnsdk_cstr_t Descriptor() throw ( GnError );

		/**
		 * Retrieves this list's language.
		 */
		gnsdk_cstr_t Language() throw ( GnError );

		/**
		 * Retrieves this list's region.
		 */
		gnsdk_cstr_t Region() throw ( GnError );

		/**
		 * Retrieves this list's revision string.
		 */
		gnsdk_cstr_t Revision() throw ( GnError );

		/**
		 * Retrieves a maximum number of levels in a hierarchy for a given list.
		 * <p><b>Remarks:</b></p>
		 * When this function succeeds, the returned parameter contains the number of levels in the given
		 * list's hierarchy. This level count value is needed when determining which level to access when
		 * retrieving elements or data from a list.
		 * Lists can be flat or hierarchical. A flat list has only one level. A hierarchical list has a
		 * parent-child relationship, where the parent's value is broad enough to encompass its child values
		 * (for example, a level 1 Rock genre is a parent to level 2 Country Rock and Punk Rock genres). You
		 * can configure an application to use a single level or the entire hierarchy.
		 * Level 1 indicates the top level of the list, which usually contains the more general data. The
		 * highest level value for a list contains the most fine-grained data.
		 */
		gnsdk_uint32_t                               LevelCount() throw ( GnError );

		typedef gn_facade_range_iterator<GnListElement, list_element_provider>   list_element_iterator;

		gn_iterable_container<list_element_iterator> ListElements(gnsdk_uint32_t level) throw ( GnError );

		/**
		 * Retrieves a list element from a list using a specific list element ID.
		 * @param itemId		List element item ID
		 */
		GnListElement ElementById(gnsdk_uint32_t itemId) throw ( GnError );

		/**
		 * Retrieves list element whose range includes the specified value
		 * @param range		Value for range comparison
		 */
		GnListElement ElementByRange(gnsdk_uint32_t range) throw ( GnError );

		/**
		 * Retrieves list element whose string matches the specified value.
		 * @param strEquality		Value of string to look up
		 */
		GnListElement ElementByString(gnsdk_cstr_t strEquality) throw ( GnError );

		/**
		 * Retrieves list element corresponding to the data object.
		 * @param dataObject		GnDataObject  that corresponds.
		 * @param ordinal			Ordinal
		 * @param level				List level value
		 */
		GnListElement ElementByGnDataObject(const metadata::GnDataObject& dataObject, gnsdk_uint32_t ordinal, gnsdk_uint32_t level) throw ( GnError );

		/**
		 * Retrieve this list element's native handle.
		 */
		gnsdk_list_handle_t            native() const { return get<gnsdk_list_handle_t>(); }

	private:
		static void GNSDK_CALLBACK_API _list_status_callback(void* callback_data, gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received, gnsdk_bool_t* p_abort);

		GnStatusEvents*                event_handler_;
		friend class GnLocale;
		friend class GnLocaleInfo;

	
	};
}
#endif /* _LIST_H_ */
