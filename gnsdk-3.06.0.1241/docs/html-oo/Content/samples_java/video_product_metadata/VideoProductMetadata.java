/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */
/*
 *  Name: VideoProductsMetadata
 *  Description:
 *  This sample shows accessing product metadata: Disc > Side >  Layer >  Feature > Chapters.
 *
 *  Command-line Syntax:
 *  clientid  clientid_tag license path lib path 
 */
package video_product_metadata;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import com.gracenote.gnsdk.*;
import com.gracenote.gnsdk.GnSDK.*;

public class VideoProductMetadata {
	static {
		try {
			System.loadLibrary("gnsdk_java_marshal");
		} catch (UnsatisfiedLinkError unsatisfiedLinkError) {
			System.err.println("Native code library failed to load\n"
					+ unsatisfiedLinkError.getMessage());
			System.exit(1);
		}
	}

	/*
	 * GnStatusEventsListener : overrider methods of this class to get delegate
	 * callbacks
	 */
	private static class LookupStatusEvents extends GnStatusEventsListener {
		@Override
		public void statusEvent(GnStatus video_status, long percent_complete,
				long bytes_total_sent, long bytes_total_received) {
			System.out.print("status (");
			switch (video_status) {
				case gnsdk_status_unknown :
					System.out.print("Unknown ");
					break;
				case gnsdk_status_begin :
					System.out.print("Begin ");
					break;
				case gnsdk_status_connecting :
					System.out.print("Connecting ");
					break;
				case gnsdk_status_sending :
					System.out.print("Sending ");
					break;
				case gnsdk_status_receiving :
					System.out.print("Receiving ");
					break;
				case gnsdk_status_disconnected :
					System.out.print("Disconnected ");
					break;
				case gnsdk_status_complete :
					System.out.print("Complete ");
					break;
				default :
					break;
			}
			System.out.println("), % complete (" + percent_complete
					+ "), sent (" + bytes_total_sent + "), received ("
					+ bytes_total_received + ")");
		}

		@Override
		public boolean cancelCheck() {
			return false;
		}
	}

	private static boolean checkFilePath(final String licensePath) {
		File file = new File(licensePath);
		if (file.isFile()) {
			return true;
		}
		return false;
	}

	private static void loadLocale(GnSDK gnsdk, GnUser user) throws GnException, IOException {

		LookupStatusEvents localeEvents = new LookupStatusEvents();
		/* Set locale with desired Group, Language, Region and Descriptor */	
		GnLocale locale = new GnLocale(
				GnSDK.getKLocaleGroupVideo(),
				GnSDK.getKLanguageEnglish(), 
				GnSDK.getKRegionDefault(),
				GnSDK.getKDescriptorSimplified(), 
				user, 
				null);/* localeEvents can be used to view the progress*/

		/* set this locale as default for the duration of gnsdk */
		gnsdk.setDefaultLocale(locale);
	}

	private static GnUser getUser(final GnSDK gnsdk, final String clientId,
			final String clientIdTag, final String applicationVersion)
			throws GnException, IOException {

		String serialized = null;

		if (!new File("user.txt").canRead()) {
			System.out
					.println("\nInfo: No stored user - this must be the app's first run.");

			GnUserRegisterMode userRegistrationMode = GnSDK.GnUserRegisterMode.kUserRegModeOnline;;
			
			serialized = gnsdk.registerUser(userRegistrationMode, clientId,
					clientIdTag, applicationVersion).toString();

			BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt"));

			try {
				/* store user data to file */
				bw.write(serialized.toString());
			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				bw.close();
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader("user.txt"));
			try {
				/* read stored user data from file */
				serialized = br.readLine();

			} catch (IOException e) {
				System.out
						.println("\nError: Failed to open the user file. (user.txt)");
			} finally {
				br.close();
			}
		}

		return new GnUser(serialized, clientId, clientIdTag, applicationVersion);
	}

	private static void dispalyBasicData(GnVideoProduct gnVideoProduct)
			throws GnException {
		String value = null;
		/* Get the video title GDO */
		GnTitle productTitle = gnVideoProduct.officialTitle();
		/* Display video title */
		System.out.println("Title: " + productTitle.display());
		/* Video edition */
		value = productTitle.edition();
		if (null != value && "" != value)
			System.out.println("Edition: " + value);
		/* Video type */
		value = gnVideoProduct.videoProductionType();
		if (null != value && "" != value)
			System.out.println("Production type: " + value);
		/* Video original release */
		value = gnVideoProduct.dateOriginalRelease();
		if (null != value && "" != value)
			System.out.println("Orig release: " + value);
		/* Rating */
		GnRating rating = gnVideoProduct.rating();
		value = rating.rating();
		if (null != value && "" != value) {
			System.out.print("Rating: " + value);
			value = rating.ratingType();
			if (null != value && "" != value)
				System.out.print(" [" + value + "]");
			value = rating.ratingDesc();
			if (null != value && "" != value)
				System.out.print(" - " + value);
			System.out.println();
		}
		/* Video release year */
		value = gnVideoProduct.dateRelease();
		if (null != value && "" != value)
			System.out.println("Release: " + value);
		/* Discs in set */
		System.out.println("Discs: " + gnVideoProduct.discs().count());
	}

	private static void displayChapters(GnVideoFeature gnVideoFeature)
			throws GnException {
		/* Get chapter count */
		long chapterCount = gnVideoFeature.chapters().count();
		System.out.println("\t\t\tchapters: " + chapterCount);
		GnVideoChapterIterator chapterIterator = gnVideoFeature.chapters()
				.getIterator();
		while (chapterIterator.hasNext()) {
			/* Get the chapter GDO */
			GnVideoChapter gnVideoChapter = chapterIterator.next();
			/* Get chapter number */
			int chapternumStr = gnVideoChapter.ordinal();
			/* Get the chapter title name GDO */
			GnTitle gnChpaterTitle = gnVideoChapter.officialTitle();
			String valueDisplay = gnChpaterTitle.display();
			if (null != valueDisplay && "" != valueDisplay)
				System.out.print("\t\t\t\t" + chapternumStr + ": "
						+ valueDisplay);
			int duration = gnVideoChapter.duration();
			if (duration > 0) {
				int seconds = duration;
				int minutes = seconds / 60;
				int hours = minutes / 60;
				seconds = seconds - (60 * minutes);
				minutes = minutes - (60 * hours);
				System.out.printf(" [%d:%02d:%02d]\n", hours, minutes, seconds);
			}
		} /* For each chapter */
	}

	private static void displayLayers(GnVideoSide gnVideoSide)
			throws GnException {
		long layerCount = gnVideoSide.layers().count();
		if (layerCount > 0)
			System.out.println("\tNumber layers: " + layerCount);
		else {
			System.out.println("\tNo layer data\n");
			return;
		}
		GnVideoLayerIterator layerIterator = gnVideoSide.layers().getIterator();
		while (layerIterator.hasNext()) {
			/* Get the layer GDO */
			GnVideoLayer gnVideoLayer = layerIterator.next();
			/* Get the layer number */
			int layerNumber = gnVideoLayer.ordinal();
			/* matched - was this the layer that matched the initial query input */
			String matched = "";
			if (gnVideoLayer.matched())
				matched = "MATCHED";
			else
				matched = "";
			System.out.println("\t\tLayer " + layerNumber + " -------- "
					+ matched);
			/* Get media type */
			String mediaType = gnVideoLayer.mediaType();
			if (null != mediaType && "" != mediaType)
				System.out.println("\t\tMedia type: " + mediaType);
			/* Get TV system */
			String tvSystem = gnVideoLayer.tvSystem();
			if (null != tvSystem && "" != tvSystem) {
				System.out.println("\t\tTV system: " + tvSystem);
			}
			/* Get region */
			String regionCode = gnVideoLayer.regionCode();
			if (null != regionCode && "" != regionCode) {
				System.out.println("\t\tRegion Code: " + regionCode);
			}
			String videoRegion = gnVideoLayer.videoRegion();
			if (null != videoRegion && "" != videoRegion)
				System.out.println("\t\tVideo Region: " + videoRegion);
			/* Get aspect ratio */
			String aspectRatio = gnVideoLayer.aspectRatio();
			if (null != aspectRatio && "" != aspectRatio) {
				System.out.println("\t\tAspect ratio: " + aspectRatio);
				/* Get aspect ration type */
				String aspectRatioType = gnVideoLayer.aspectRatioType();
				if (null != aspectRatioType && "" != aspectRatioType)
					System.out.println(" [" + aspectRatioType + "]");
			}
			/* Get number of layer features */
			long featureCount = gnVideoLayer.features().count();
			if (featureCount > 0)
				System.out.println("\t\tFeatures: " + featureCount);
			/* Loop thru features */
			GnVideoFeatureIterator featureIterator = gnVideoLayer.features()
					.getIterator();
			while (featureIterator.hasNext()) {
				/* Get the feature GDO */
				GnVideoFeature gnVideoFeature = featureIterator.next();
				/* Get feature number */
				int featureNumber = gnVideoFeature.ordinal();
				/* Get matched */ 
				matched = "";
				if (gnVideoFeature.matched())
					matched = "MATCHED";
				else
					matched = "";
				System.out.println("\n\t\t\tFeature " + featureNumber
						+ " -------- " + matched);
				/* Get the feature title name GDO */
				GnTitle gnTitle = gnVideoFeature.officialTitle();
				/* Get title */
				String displayTitle = gnTitle.display();
				if (null != displayTitle && "" != displayTitle) {
					System.out.println("\t\t\tFeature title: " + displayTitle);
				}
				int duration = gnVideoFeature.duration();
				if (duration > 0) {
					int seconds = duration;
					int minutes = seconds / 60;
					int hours = minutes / 60;
					seconds = seconds - (60 * minutes);
					minutes = minutes - (60 * hours);
					System.out.printf("\t\t\tLength: %d:%02d:%02d\n", hours,
							minutes, seconds);
				}
				/* Aspect ratio */
				aspectRatio = gnVideoFeature.aspectRatio();
				if (null != aspectRatio && "" != aspectRatio) {
					System.out.print("\t\t\tAspect ratio: " + aspectRatio);
					/* Aspect ratio type */
					String aspectRatioType = gnVideoFeature.aspectRatioType();
					if (null != aspectRatioType && "" != aspectRatioType)
						System.out.println(" [" + aspectRatioType + "]");
				}
				/*
				 * Note: Genre display strings come from a genre hierarchy, so
				 * you can choose what level of granularity to display. Ganere
				 * level1 are the top level genres and the most general,
				 * GNSDK_GDO_VALUE_GENRE_LEVEL2 is the mid-level genre and the
				 * most granular genre is GNSDK_GDO_VALUE_GENRE_LEVEL#.
				 */
				/* Feature genre(s) */
				String valuelevel1 = gnVideoFeature.genre().level1();
				if (null != valuelevel1 && "" != valuelevel1) {
					System.out.println("\t\t\tPrimary genre: " + valuelevel1);
				}
				/* Feature rating */
				GnRating gnFeatureRating = gnVideoFeature.rating();
				String valueRating = gnFeatureRating.rating();
				if (null != valueRating && "" != valueRating) {
					System.out.print("\t\t\tRating: " + valueRating);
					String valueRatingType = gnFeatureRating.ratingType();
					if (null != valueRatingType && "" != valueRatingType)
						System.out.print(" [" + valueRatingType + "]");
					String valueRatingDesc = gnFeatureRating.ratingDesc();
					if (null != valueRatingDesc && "" != valueRatingDesc)
						System.out.print(" - " + valueRatingDesc);
					System.out.println();
				}
				/* Feature type */
				String valueVideoFeatureType = gnVideoFeature
						.videoFeatureType();
				if (null != valueVideoFeatureType
						&& "" != valueVideoFeatureType) {
					System.out.println("\t\t\tFeature type: "
							+ valueVideoFeatureType);
				}
				/* Video type */
				String valueVideoProductionType = gnVideoFeature
						.videoProductionType();
				if (null != valueVideoProductionType
						&& "" != valueVideoProductionType) {
					System.out.println("\t\t\tProduction type: "
							+ valueVideoProductionType);
				}
				/* feature plot */
				String valueplotSummary = gnVideoFeature.plotSummary();
				if (null != valueplotSummary && "" != valueplotSummary) {
					System.out.println("\t\t\tPlot summary: "
							+ valueplotSummary);
				}
				String valueplotSynopsis = gnVideoFeature.plotSynopsis();
				if (null != valueplotSynopsis && "" != valueplotSynopsis) {
					System.out.println("\t\t\tPlot synopsis: "
							+ valueplotSynopsis);
				}
				String valueplotTagline = gnVideoFeature.plotTagline();
				if (null != valueplotTagline && "" != valueplotTagline) {
					System.out.println("\t\t\tTagline: " + valueplotTagline);
				}
				displayChapters(gnVideoFeature);
			} /* For each feature */
		} /* For each layer */
	}

	private static void displayDiscInformation(GnVideoProduct gnVideoProduct)
			throws GnException {
		/* Get disc count */
		long discCount = gnVideoProduct.discs().count();
		System.out.println("\nDiscs:" + discCount);
		/* Discs in set */
		GnVideoDiscIterable gnVideoDiscIterable = gnVideoProduct.discs();
		GnVideoDiscIterator discItr = gnVideoDiscIterable.getIterator();
		while (discItr.hasNext())/* Discs in set loop */
		{
			GnVideoDisc disc = (GnVideoDisc) discItr.next();
			/* Disc number */
			int discNumber = disc.ordinal();
			/* Matched -- */
			String discMatch = "";
			if (disc.matched())
				discMatch = "MATCHED";
			else
				discMatch = "";
			System.out
					.println("disc " + discNumber + " -------- " + discMatch);
			GnTitle discTitle = disc.officialTitle();
			System.out.println("\tTitle: " + discTitle.display());
			/* Count the number of sides */
			long sideCount = 0;
			sideCount = disc.sides().count();
			System.out.println("\tNumber sides: " + sideCount);
			/* Sides in set - sides details */
			GnVideoSideIterable gnVideoSideIterable = disc.sides();
			GnVideoSideIterator sideItr = gnVideoSideIterable.getIterator();
			while (sideItr.hasNext())/* side loop */
			{
				GnVideoSide side = sideItr.next();
				/* Side number */
				int sideNumber = side.ordinal();
				/* Matched -- */
				String sideMatch = "";
				if (side.matched())
					sideMatch = "MATCHED";
				else
					sideMatch = "";
				System.out.println("\tSide " + sideNumber + " -------- "
						+ sideMatch);
				displayLayers(side);
			}/* side loop */
		} /* Discs in set loop */
	}

	private static void displayMultipleProduct(
			GnResponseVideoProduct gnResponseVideoProduct) throws GnException {
		int count = 0;
		/* How many videos in the result? */
		GnVideoProductIterator responseContext = gnResponseVideoProduct
				.products().getIterator();
		while (responseContext.hasNext()) {
			System.out.println("Match : "+ ++count);
			GnVideoProduct product = responseContext.next();
			dispalyBasicData(product);
		}
	}

	private static void displaySingleProduct(
			GnResponseVideoProduct gnResponseVideoProduct, GnUser user)
			throws GnException {
		/*
		 * Note that the GDO accessors below are *ordinal* based, not index
		 * based. so the 'first' of anything has a one-based ordinal of '1' -
		 * *not* an index of '0'
		 */
		/* Get the current product from the response */
		GnVideoProductIterable gnVideoProductIterable = gnResponseVideoProduct
				.products();
		GnVideoProductIterator gnVideoProductIterator = gnVideoProductIterable
				.getIterator();
		GnVideoProduct gnVideoProduct = gnVideoProductIterator.next();
		dispalyBasicData(gnVideoProduct);
		/* Get a GDO with full metadata */
		GnVideo gnVideo = new GnVideo(user);
		GnResponseVideoProduct response = gnVideo.findProducts(gnVideoProduct);
		GnVideoProduct fullVideoProduct = response.products().at(0).next();
		displayDiscInformation(fullVideoProduct);
	}

	private static void videoProductsMetadata(GnUser user, String title)
			throws GnException {
		long resultCount = 0;
		LookupStatusEvents videoEvents = new LookupStatusEvents();
		System.out.println("\n*****Sample Title Search: '" + title + "'*****");
		GnVideo gnVideo = new GnVideo(user, null); /* videoEvents can be used to view the progress*/
		/* Setting range values */
		gnVideo.optionRangeStart("1");
		gnVideo.optionRangeSize("20");
		GnResponseVideoProduct gnResponseVideoProduct = gnVideo.findProducts(
				title, GnVideoSearchField.kSearchFieldProductTitle,
				GnVideoSearchType.kSearchTypeDefault);
		resultCount = gnResponseVideoProduct.products().count();
		if (1 == resultCount) {
			displaySingleProduct(gnResponseVideoProduct, user);
		} else {
			/* We now have 1-n matches needing resolution 
			displayMultipleProduct(gnResponseVideoProduct);*/
			/*
			 * Typically the user would choose one (or none) of the presented
			 * choices. For this simplified sample, just pick the first choice
			 */
			displaySingleProduct(gnResponseVideoProduct, user);
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println("Usage : clientId clientIdTag license gnsdkLibraryPath");
			System.exit(0);
		}

		final String clientId = args[0].trim();
		final String clientIdTag = args[1].trim();
		final String licensePath = args[2].trim();
		final String libPath = args[3].trim();

		final String applicationVersion = "1.0.0.0";
		
		boolean fileExists = checkFilePath(licensePath);

		if (fileExists) {
			try {
				/* GNSDK initialization */
				GnSDK gnsdk = new GnSDK(libPath, licensePath, GnSDK.GnLicenseInputMode.kFilename);
			
				System.out.print("\nGNSDK Product Version    : "+ gnsdk.productVersion());
				System.out.println(" \t(built " + gnsdk.buildDate()+ ")" );
			
				gnsdk.loggingEnable(
						"sample.log", 													/* Log file path */
						GnSDK.getGN_LOG_PKG_ALL(), 										/* Include entries for all packages and subsystems */
						GnSDK.getGN_LOG_LEVEL_ERROR()|GnSDK.getGN_LOG_FILTER_WARNING(), /* Include only error and warning entries */
						GnSDK.getGN_LOG_OPTION_ALL(), 									/* All logging options: time stamps, thread IDs, etc */
						new BigInteger("0"), 											/* Max size of log: 0 means a new log file will be created each run */
						false															/* true = old logs will be renamed and saved */
						); 
			
				GnUser user = getUser(gnsdk, clientId, clientIdTag, applicationVersion);
			
				loadLocale(gnsdk, user);
				/* Sample Album creation and submission */
				videoProductsMetadata(user, "Star");
				} catch (GnException gnException) {
					System.out.println("\nError Code: " + gnException.getErrorCode()
							+ "\nError Message: " + gnException.getMessage());
				} finally {
					System.runFinalization();
					System.gc();
				}
			} else {
				System.out.println("Licence file not found");
				System.exit(0);
			}
		}
	}
