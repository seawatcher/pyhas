/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Description:
 *  This sample shows basic use of the FindSeries() call
 *
 *  Command-line Syntax:
 *  sample client_id client_id_tag license lib_path
 */


#include <iostream>
#include <fstream>
#include <string>

#include "gnsdk.hpp"
#include "gnsdk_video.hpp"

#include "gnsdk_loader.h"

using namespace gracenote;
using namespace gracenote::video;


/* Callback delegate called when performing queries */
class LookupStatusEvents : public GnStatusEvents
{
	/*-----------------------------------------------------------------------------
	 *  status_event
	 */
	void
	status_event(gnsdk_status_t status, gnsdk_uint32_t percent_complete, gnsdk_size_t bytes_total_sent, gnsdk_size_t bytes_total_received)
	{
		std::cout << "status (";

		switch (status)
		{
		case gnsdk_status_unknown:
			std::cout <<"Unknown ";
			break;

		case gnsdk_status_begin:
			std::cout <<"Begin ";
			break;

		case gnsdk_status_connecting:
			std::cout <<"Connecting ";
			break;

		case gnsdk_status_sending:
			std::cout <<"Sending ";
			break;

		case gnsdk_status_receiving:
			std::cout <<"Receiving ";
			break;

		case gnsdk_status_disconnected:
			std::cout <<"Disconnected ";
			break;

		case gnsdk_status_complete:
			std::cout <<"Complete ";
			break;

		default:
			break;
		}
		std::cout << "), % complete (" << percent_complete << "), sent (" << bytes_total_sent << "), received (" << bytes_total_received << ")" << std::endl;
	}

	/*-----------------------------------------------------------------------------
	 *  cancel_check
	 */
	bool
	cancel_check()
	{
		return GNSDK_FALSE;
	}
};


/*-----------------------------------------------------------------------------
 *  display_series_metadata
 */
static void
display_series_metadata(GnVideoSeries& pSeries)
{
	GnGenre genre = pSeries.Genre();


	printf("Genre: %s\n", genre.Level1());

	printf("Production Type: %s\n", pSeries.VideoProductionType());

	printf("Plot: %s\n", pSeries.PlotSynopsis());

	printf("Reputation: %s\n", pSeries.Reputation());

}


/*-----------------------------------------------------------------------------
 *  do_series_lookup
 */
static void
do_series_lookup(GnUser& user)
{
	gnsdk_cstr_t tuiID  = "238073738";
	gnsdk_cstr_t tuiTag = "AE1B6F5143346B16AAD196EDC3093C6F";


	try
	{
		printf("\n*****Sample Video Series Search: '%s'*****\n", "Simpsons Series");

		LookupStatusEvents lookupEvents;

		GnVideo myVideoID(user, &lookupEvents);

		GnVideoSeries dataObject(tuiID, tuiTag);

		printf("\nPerform the search...\n");

		GnResponseVideoSeries videoSeriesResponse = myVideoID.FindSeries(dataObject);

		metadata::series_iterator seriesIterator = videoSeriesResponse.Series().begin();

		for (; seriesIterator != videoSeriesResponse.Series().end(); ++seriesIterator )
		{
			GnVideoSeries series = *seriesIterator;

			GnTitle seriesTitle = series.OfficialTitle();

			printf("\nTitle: %s\n", seriesTitle.Display());

			display_series_metadata(series);
		}
	}
	catch (GnError error)
	{
		std::cout << error.ErrorAPI() << std::endl;
		std::cout << error.ErrorDescription() << std::endl;
		std::cout << error.ErrorCode() << std::endl;
	}
}


/*-----------------------------------------------------------------------------
 *  GetUser
 *    Return a stored user if exists, or create new user and store it for
 *    for use next time.
 */
GnUser
GetUser(GnSDK& gnsdk, gnsdk_cstr_t clientID, gnsdk_cstr_t clientIdTag, gnsdk_cstr_t applicationVersion)
{
	std::fstream userRegFile;
	std::string  serialized;

	userRegFile.open("user.txt", std::fstream::in);
	if (userRegFile.fail())
	{
		GnSDK::GnUserRegisterMode usermode = GnSDK::kUserRegModeOnline;
#if USE_LOCAL
		usermode = GnSDK::kUserRegModeLocalOnly;
#endif

		serialized = gnsdk.RegisterUser(usermode, clientID, clientIdTag, applicationVersion).c_str();

		/* store user data to file */
		userRegFile.open("user.txt", std::fstream::out);
		userRegFile << serialized;
	}
	else
	{
		/* read stored user data from file */
		userRegFile >> serialized;
	}

	return GnUser(serialized.c_str(), clientID, clientIdTag, applicationVersion);
}


/*-----------------------------------------------------------------------------
 *  LoadLocale
 *    Load a 'locale' to return locale-specific values in the Metadata.
 *    This examples loads an English locale.
 */
void
LoadLocale(GnSDK& gnsdk, GnUser& user)
{
	LookupStatusEvents localeEvents;

	/* Set locale with desired Group, Language, Region and Descriptor */
	GnLocale locale( GnSDK::kLocaleGroupMusic, GnSDK::kLanguageEnglish, GnSDK::kRegionDefault, GnSDK::kDescriptorSimplified, user, &localeEvents );

	/* set this locale as default for the duration of gnsdk */
	gnsdk.SetDefaultLocale(locale);
}


/******************************************************************
*
*    MAIN
*
******************************************************************/
int
main(int argc, char* argv[])
{
	gnsdk_cstr_t  licenseFile        = GNSDK_NULL;
	gnsdk_cstr_t  gnsdkLibraryPath   = GNSDK_NULL;
	gnsdk_cstr_t  clientId           = GNSDK_NULL;
	gnsdk_cstr_t  clientIdTag        = GNSDK_NULL;
	gnsdk_cstr_t  applicationVersion = "1.0.0.0";
	gnsdk_error_t error;


	if (argc == 5)
	{
		clientId         = argv[1];
		clientIdTag      = argv[2];
		licenseFile      = argv[3];
		gnsdkLibraryPath = argv[4];
	}
	else
	{
		std::cout << "\nUsage:" << argv[0] << " clientId clientIdTag license gnsdkLibraryPath\n" << std::endl;
		return 0;
	}

	/* set dynamic loader path for GNSDK */
	error = gnsdk_loader_set_gnsdk_path(gnsdkLibraryPath);

	/* GNSDK initialization */
	try
	{
		GnSDK gnsdk(licenseFile, GnSDK::kFilename);

		/* Display GNSDK Version infomation */
		printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk.ProductVersion(), gnsdk.BuildDate());

		gnsdk.LoggingEnable(
		    "sample.log",                                   /* Log file path */
		    GNSDK_LOG_PKG_ALL,                              /* Include entries for all packages and subsystems */
		    GNSDK_LOG_LEVEL_ERROR|GNSDK_LOG_LEVEL_WARNING,  /* Include only error and warning entries */
		    GNSDK_LOG_OPTION_ALL,                           /* All logging options: timestamps, thread IDs, etc */
		    0,                                              /* Max size of log: 0 means a new log file will be created each run */
		    GNSDK_FALSE                                     /* GNSDK_TRUE = old logs will be renamed and saved */
		    );

		GnUser user = GetUser(gnsdk, clientId, clientIdTag, applicationVersion);

		LoadLocale(gnsdk, user);

		/* Lookup Series and display */
		do_series_lookup(user);
	}
	catch (GnError e)
	{
		std::cout << e.ErrorAPI() << "\t" << std::hex << e.ErrorCode() << "\t" <<  e.ErrorDescription() << std::endl;
	}

	return 0;
}

