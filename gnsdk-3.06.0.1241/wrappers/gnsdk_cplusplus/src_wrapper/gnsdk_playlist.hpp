/** Public header file for Gracenote SDK C++ Wrapper
 * Author:
 *   Copyright (c) 2013 Gracenote, Inc.
 *
 *   This software may not be used in any way or distributed without
 *   permission. All rights reserved.
 *
 *   Some code herein may be covered by US and international patents.
 */

#ifndef _GNSDK_PLAYLIST_HPP_
#define _GNSDK_PLAYLIST_HPP_

#ifndef __cplusplus
#error "C++ compiler required"
#endif

#include "gnsdk.hpp"
#include "metadata_music.hpp"

#if GNSDK_PLAYLIST

namespace gracenote
{
	namespace playlist
	{
		class GnPlaylistCollectionSyncEvents;
		class GnPlaylistCollection;
		class GnPlaylistResult;

/**************************************************************************
** GnPlaylistIdentifier
*/
		class GnPlaylistIdentifier
		{
		public:
			GnPlaylistIdentifier() : ident_(GNSDK_NULL), collection_(GNSDK_NULL) { }

// Constructors

			virtual ~GnPlaylistIdentifier() { }
			gnsdk_cstr_t MediaIdentifier() const
			{
				return ident_;
			}

			gnsdk_cstr_t CollectionName() const
			{
				return collection_;
			}

			bool operator == (const GnPlaylistIdentifier& rhs) const
			{
				return ( ident_ == rhs.ident_ ) && ( collection_ == rhs.collection_ );
			}

		protected:
			GnPlaylistIdentifier(gnsdk_cstr_t media_identifier, gnsdk_cstr_t collection_name) : ident_(media_identifier), collection_(collection_name) { }
			friend class result_provider;
			friend class collection_ident_provider;
			friend class GnPlaylistCollectionSyncEvents;

		private:
			gnsdk_cstr_t ident_;
			gnsdk_cstr_t collection_;
		};

/**************************************************************************
** GnPlaylistMetadata
*/
		class GnPlaylistMetadata : public metadata::GnDataObject
		{
		public:
			GnPlaylistMetadata(gnsdk_gdo_handle_t media_gdo) : GnDataObject(media_gdo) { }
			GnPlaylistMetadata() : GnDataObject(GNSDK_NULL_GDO) { }

			virtual ~GnPlaylistMetadata() { }

			gnsdk_cstr_t AlbumName() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_ALBUM);
			}

			gnsdk_cstr_t ArtistName() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_ARTIST);
			}

			gnsdk_cstr_t ArtistType() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_ARTTYPE);
			}

			gnsdk_cstr_t Era() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_ERA);
			}

			gnsdk_cstr_t Genre() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_GENRE);
			}

			gnsdk_cstr_t Origin() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_ORIGIN);
			}

			gnsdk_cstr_t Mood() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_MOOD);
			}

			gnsdk_cstr_t Tempo() const
			{
				return StringValue(GNSDK_PLAYLIST_ATTRIBUTE_NAME_TEMPO);
			}
		};

/**************************************************************************
** internal result_provider
*/
		class result_provider
		{
		public:
			result_provider() : results_handle_(GNSDK_NULL) { }
			result_provider(gnsdk_playlist_results_handle_t results) : results_handle_(results) { }
			~result_provider() { }

// Required.
			GnPlaylistIdentifier            get_data(gnsdk_uint32_t pos);

// optional
			gnsdk_uint32_t                  count();

		private:
			gnsdk_playlist_results_handle_t results_handle_;
		};

/**************************************************************************
** GnPlaylistResult
*/
		class GnPlaylistResult : public GnObject
		{
		public:
			typedef utility::gnsdk_proxy<gnsdk_playlist_results_handle_t>             result_proxy_handle;
			typedef gn_facade_range_iterator<GnPlaylistIdentifier, result_provider>   iterator;

			explicit GnPlaylistResult(gnsdk_playlist_results_handle_t handle) : GnObject(handle)  { }

			gn_iterable_container<GnPlaylistResult::iterator> Identifiers()
			{
				result_provider provider(this->get<gnsdk_playlist_results_handle_t>() );
				gnsdk_uint32_t  count = provider.count();

				return gn_iterable_container<iterator>(iterator(provider, 0, count), iterator(provider, count, count) );
			}
		};

/**************************************************************************
** internal collection_ident_provider class
*/
		class collection_ident_provider
		{
		public:
			collection_ident_provider() : coll_handle_(GNSDK_NULL) { }
			collection_ident_provider(gnsdk_playlist_collection_handle_t coll_handle) : coll_handle_(coll_handle) { }

			GnPlaylistIdentifier               get_data(gnsdk_uint32_t pos);

		private:
			gnsdk_playlist_collection_handle_t coll_handle_;
		};

/**************************************************************************
** internal collection_join_provider
*/
		class collection_join_provider
		{
		public:
			collection_join_provider() : handle_(GNSDK_NULL) { }
			collection_join_provider(gnsdk_playlist_collection_handle_t coll_handle) : handle_(coll_handle) { }

			GnPlaylistCollection               get_data(gnsdk_uint32_t pos);

		private:
			gnsdk_playlist_collection_handle_t handle_;
		};

/**************************************************************************
** GnPlaylistCollection
*/
		class GnPlaylistCollection : public GnObject
		{
		public:
			enum GenerateMoreLikeThisOption
			{
				/**
				 * Option for querying/specifying the maximum number of results returned in a "More Like This" playlist.
				 * The value for this key must evaluate to a number greater than 0.
				 *
				 */
				kMoreLikeThisMaxTracks = 1,

				/**
				 * Option for querying/specifying the maximum number of results per artist returned in a "More Like This" playlist.
				 * The value for this key must evaluate to a number greater than 0.
				 *
				 */
				kMoreLikeThisMaxPerArtist,

				/**
				 * Option for querying/specifying the maximum number of results per album returned in a "More Like This" playlist.
				 * The value for this key must evaluate to a number greater than 0.
				 *
				 */
				kMoreLikeThisMaxPerAlbum,

				/**
				 * Option for querying/specifying the seed value for the random number generator used in calculating a
				 * "More Like This" playlist. Using the same number for a seed will generate the same 'random' sequence, thus allowing
				 * the same playlist ordering to be recreated. Choosing different numbers will create different playlists. Setting
				 * this value to "0" will disable using a random seed.
				 *
				 */
				kMoreLikeThisRandom
			};

			typedef gn_facade_range_iterator<GnPlaylistIdentifier, collection_ident_provider>   ident_iterator;

			typedef utility::gnsdk_proxy<gnsdk_playlist_collection_handle_t>                    proxy_handle;

// *****************************************************
// PlaylistCollection Interface
// *****************************************************
// Constructor
			GnPlaylistCollection() { }
			explicit GnPlaylistCollection(gnsdk_playlist_collection_handle_t handle) : GnObject(handle) { }

// Collection descriptors
			gnsdk_cstr_t Name() const throw ( GnError );

// Add a GnAlbum to the collection.
			void         Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnAlbum& album) throw ( GnError );

// Add a GnTrack to the collection.
			void         Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnTrack& track) throw ( GnError );

// Add a GnContributor to the Collection.
			void         Add(gnsdk_cstr_t mediaIdentifier, const metadata::GnContributor& contributor) throw ( GnError );

// Add a GnPlaylistMetadata to the Collection.
			void         Add(gnsdk_cstr_t mediaIdentifier, const GnPlaylistMetadata& playlistMetadata) throw ( GnError );

// Remove and media identifier from the collection.
			void         Remove(gnsdk_cstr_t mediaIdentifier) throw ( GnError );

			// TBD
			// void Remove(iterator begin, iterator end) {}

			ident_iterator                        Find(gnsdk_cstr_t mediaIdentifier, gnsdk_uint32_t start) throw ( GnError );

			GnPlaylistMetadata                    Metadata(GnUser& user, const GnPlaylistIdentifier&  mediaIdentifier) throw ( GnError );

			GnPlaylistMetadata                    Metadata(GnUser& user, gnsdk_cstr_t mediaIdentifier, gnsdk_cstr_t collectionName) throw ( GnError );

// Generate Playlist.
			bool                                  TryValidateStatement(gnsdk_cstr_t pdlStatement) throw ( );

			bool                                  IsSeedRequiredInStatement(gnsdk_cstr_t pdlStatement) throw ( GnError );

			GnPlaylistResult                      GeneratePlaylist(GnUser& user, gnsdk_cstr_t pdlStatement, const metadata::GnDataObject& musicDataObj) throw ( GnError );

			GnPlaylistResult                      GeneratePlaylist(GnUser& user, gnsdk_cstr_t pdlStatement) throw ( GnError );

			GnPlaylistResult                      GenerateMoreLikeThis(GnUser& user, const metadata::GnDataObject& musicDataObj) throw ( GnError );

			gn_iterable_container<ident_iterator> MediaIdentifiers() throw ( GnError );

			typedef gn_facade_range_iterator<GnPlaylistCollection, collection_join_provider>   join_iterator;

			gn_iterable_container<join_iterator>  Joins() throw ( GnError );

			GnPlaylistCollection                  JoinSearchByName(gnsdk_cstr_t collectionName) throw ( GnError );

			void                                  Join(GnPlaylistCollection& toJoin) throw ( GnError );

			void                                  JoinRemove(GnPlaylistCollection& toRemove) throw ( GnError );

/**
 *   Sets a "More Like This" option on the collection.
 *   Please note that these options are not serialized or stored.
 *
 *  @param  moreLikeThisOption	 An option from the available "More Like This" list
 *  @param  optionValue		 Value to set for given option
 *
 **/
			void MoreLikeThisOptionSet(GenerateMoreLikeThisOption moreLikeThisOption, gnsdk_uint32_t optionValue) throw ( GnError );

/**
 *   Retrieves an option for "More Like This" for a given collection.
 *   Please note that these options are not serialized or stored.
 *
 *  @param  moreLikeThisOption	 An option from the available "More Like This" list
 *  @return int morelikethisoption
 *
 **/
			gnsdk_uint32_t MoreLikeThisOptionGet(GenerateMoreLikeThisOption moreLikeThisOption) throw ( GnError );

			void           AddForSyncProcess(gnsdk_cstr_t mediaIdentifier) throw ( GnError );

			void           PeformSyncProcess(GnPlaylistCollectionSyncEvents& syncEvents) throw ( GnError );

		protected:
			void           _add(gnsdk_cstr_t mediaIdentifier, gnsdk_gdo_handle_t gdo) throw ( GnError );

// friend class definition
			friend class GnPlaylist;
		};

/**************************************************************************
** GnPlaylistCollectionSyncEvents
*/
		class GnPlaylistCollectionSyncEvents
		{
		public:
			enum  EventsIdentiferStatus
			{
				kIdentifierStatusUnknown = 0,
				kIdentifierStatusNew     = 10,
				kIdentifierStatusOld     = 20
			};

			// Methods to Override.
			virtual void OnUpdate(GnPlaylistCollection collection, GnPlaylistIdentifier identifier, EventsIdentiferStatus status)
			{
				GNSDK_UNUSED(collection);
				GNSDK_UNUSED(identifier);
				GNSDK_UNUSED(status);
			}

			virtual bool OnCancelCheck() { return false; }

		protected:
			friend class GnPlaylistCollection;
			static void  _collection_sync(gnsdk_void_t*, gnsdk_playlist_collection_handle_t, gnsdk_cstr_t, gnsdk_playlist_status_t, gnsdk_bool_t*);
		};

/**************************************************************************
** Internal : collection_storage_provider
*/

// Internal provider class
		class collection_storage_provider
		{
		public:
			collection_storage_provider();
			~collection_storage_provider();

// Default method required .
			gnsdk_cstr_t  get_data(gnsdk_uint32_t pos);

		private:
			gnsdk_char_t* buffer_;
		};

/**************************************************************************
** GnPlaylist
*/
		class GnPlaylist : public GnObject
		{
		public:
/** @internal GnMusicID @endinternal
 *  Initializes the Playlist library.
 *  <p><b>Remarks:</b></p>
 * @ingroup Music_Playlist_InitializationFunctions
 */
			GnPlaylist();

			virtual ~GnPlaylist();

/** @internal Version @endinternal
 *  Retrieves the Playlist SDK version string.
 *  @return version string if successful
 *  @return GNSDK_NULL if not successful
 *  <p><b>Remarks:</b></p>
 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
 *   string is a constant. Do not attempt to modify or delete.
 *  Example version string: 1.2.3.123 (Major.Minor.Improvement.Build)
 *  Major: New functionality
 *  Minor: New or changed features
 *  Improvement: Improvements and fixes
 *  Build: Internal build number
 * @ingroup Music_Playlist_InitializationFunctions
 */
			gnsdk_cstr_t Version() const;

/** @internal BuildDate @endinternal
 *  Retrieves the Playlist SDK's build date string.
 *  @return Note Build date string of the format: YYYY-MM-DD hh:mm UTC
 *  <p><b>Remarks:</b></p>
 *  This API can be called at any time, after getting instance of GnSDK successfully. The returned
 *   string is a constant. Do not attempt to modify or delete.
 *  Example build date string: 2008-02-12 00:41 UTC
 * @ingroup Music_Playlist_InitializationFunctions
 */
			gnsdk_cstr_t                            BuildDate() const;

// Creation of collection.
			GnPlaylistCollection                    CreateCollection(gnsdk_cstr_t collectionName) throw ( GnError );

			GnPlaylistCollection                    DeserializeCollection(gnsdk_byte_t* buffer, gnsdk_size_t buffer_sz) throw ( GnError );

			gnsdk_size_t                            SerializeCollection(GnPlaylistCollection& collection, gnsdk_byte_t* buffer, gnsdk_size_t buffer_sz) throw ( GnError );

			gnsdk_size_t                            SerializeBufferSize(GnPlaylistCollection& collection) throw ( GnError );

			void                                    StoreCollection(GnPlaylistCollection& collection) throw ( GnError );

			void                                    RemoveFromStorage(GnPlaylistCollection& collection) throw ( GnError );

			void                                    RemoveFromStorage(gnsdk_cstr_t collectionName) throw ( GnError );

			void                                    SetStorageLocation(gnsdk_cstr_t location) throw ( GnError );

			void                                    CompactStorage() throw ( GnError );

			bool                                    StorageIsValid() const throw ( GnError );

			typedef gn_facade_range_iterator<gnsdk_cstr_t, collection_storage_provider>   storage_iterator;

			gn_iterable_container<storage_iterator> StoredCollections() throw ( GnError );

			GnPlaylistCollection                    LoadCollection(storage_iterator itr)  throw ( GnError );

			GnPlaylistCollection                    LoadCollection(gnsdk_cstr_t collectionName) throw ( GnError );

		private:
			DISALLOW_COPY_AND_ASSIGN(GnPlaylist);
		};
	} // namespace Playlist
}     // namespace GracenoteSDK
#endif /* GNSDK_PLAYLIST */
#endif // _GNSDK_PLAYLIST_HPP_
