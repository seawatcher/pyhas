/*
 * Copyright (c) 2000-2013 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */

/*
 *  Name: code_snippets/musicid_gdo_metadata/main.c
 *  Description: Demonstrates extracting metadata from album gdo
 */

/*
 * Warning
 *
 * The GNSDK package provides three kinds of code examples: sample applications,
 * reference applications, and code snippets.We strongly recommend you use the sample
 * and reference applications as a basis for any applications you develop.
 *
 * The code snippets are located in the /samples/code_snippets folder. They are provided
 * "as is" for documentation purposes only. Their intention is to illustrate a specific
 * function or feature. Although they have been compiled in Windows x64, they lack certain
 * basic functionality and have not been validated for all platforms.
 */

#define GNSDK_MUSICID    1
#include "gnsdk.h"

/* Standard C headers - used by the sample app, but not required for GNSDK */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**********************************************
 *    Local Function Declarations
 **********************************************/
static int
_init_gnsdk(
    gnsdk_cstr_t            client_id,
    gnsdk_cstr_t            client_id_tag,
    gnsdk_cstr_t            client_id_app_version,
    gnsdk_cstr_t            license_path,
    gnsdk_user_handle_t*    p_user_handle
    );

static int
 _gdo_get_metadata(gnsdk_user_handle_t user_handle);

static
void _shutdown_gnsdk(
    gnsdk_user_handle_t     user_handle,
    const char*             client_id
    );

/******************************************************************
 *
 *    MAIN
 *
 ******************************************************************/
int main(int argc, char* argv[])
{
    gnsdk_user_handle_t     user_handle          = GNSDK_NULL;
    const char*             client_id            = NULL;
    const char*             client_id_tag        = NULL;
    const char*             client_app_version   = "1"; /* Increment with each version of your app */
    const char*             license_path         = NULL;
    int                     rc                   = 0;

    /*    Client ID, Client ID Tag and License file must be passed in */
    if (argc == 4)
    {
        client_id        = argv[1];
        client_id_tag    = argv[2];
        license_path     = argv[3];

        /* Initialize GNSDK     */
        rc = _init_gnsdk(
                client_id,
                client_id_tag,
                client_app_version,
                license_path,
                &user_handle
                );

        if (0 == rc)
        {
            /* Lookup album and display metadata */
            rc = _gdo_get_metadata(user_handle);

            /* Clean up and shutdown */
            _shutdown_gnsdk(user_handle, client_id);
        }
    }
    else
    {
        printf("\nUsage:\n%s clientid clientidtag license\n", argv[0]);
        rc = -1;
    }

    return rc;

}    /* main() */


/******************************************************************
 *
 *    _DISPLAY_LAST_ERROR
 *
 *    Echo the error and information.
 *
 *****************************************************************/
static void
_display_last_error(
	int				line_num
	)
{
	/* Get the last error information from the SDK */
	const gnsdk_error_info_t*       error_info = gnsdk_manager_error_info();

	/* Error_info will never be GNSDK_NULL.
	 * The SDK will always return a pointer to a populated error info structure.
	 */
	printf(
		"\nerror from: %s()  [on line %d]\n\t0x%08x %s",
		error_info->error_api,
		line_num,
		error_info->error_code,
		error_info->error_description
		);

} /* display_last_error() */


/******************************************************************
 *
 *    _DISPLAY_GNSDK_PRODUCT_INFO
 *
 *    Display product version information
 *
 ******************************************************************/
static void _display_gnsdk_product_info(void)
{
    /* Display GNSDK Version infomation */
    printf("\nGNSDK Product Version    : %s \t(built %s)\n", gnsdk_manager_get_product_version(), gnsdk_manager_get_build_date());

}    /* _display_gnsdk_product_info() */


/******************************************************************
 *
 *   _GDO_GET_METADATA
 *
 ******************************************************************/
static int _gdo_get_metadata(gnsdk_user_handle_t user_handle)
{
    gnsdk_error_t                   error               = GNSDK_SUCCESS;
    gnsdk_musicid_query_handle_t    query_handle        = GNSDK_NULL;
    gnsdk_gdo_handle_t              input_gdo           = GNSDK_NULL;
    gnsdk_gdo_handle_t              album_gdo           = GNSDK_NULL;
    gnsdk_gdo_handle_t              album_title_gdo     = GNSDK_NULL;
    gnsdk_gdo_handle_t              response_gdo        = GNSDK_NULL;
    gnsdk_cstr_t                    response_type       = GNSDK_NULL;
    gnsdk_uint32_t                  track_count         = 0;


    /* Create musicid query handle */
    printf("\nCreating musicid query\n");
    error = gnsdk_musicid_query_create(    user_handle,GNSDK_NULL,    GNSDK_NULL, &query_handle);
    if (error != GNSDK_SUCCESS)
    {
       return -1;
    }

    /* Get GDO  for query
     * You can either deserialze gdo from serialized GDO string or use live GDO */
    error =     gnsdk_manager_gdo_deserialize("WEcxAwpU57x6ijap4RVINrN7Uui/UEzqC7RJqnLkGzfPBYPZAIQR3ZNCbN78yxFd+uKAIOeqqVYNQSg90E9jWpzS3dCNtKaLSmSQQGJFVEtwdIe6tCPYme+bCSsN9qzj1xSnrNTwwf5HUof9PPlX4d71JWGUczplR50Bh4I2b4tvmW2xpdZ1O91gMfxVZYU=",&input_gdo);

    printf("\nSetting query GDO\n");
    error = gnsdk_musicid_query_set_gdo(query_handle, input_gdo);

    /* Perform the query */
    printf("\nDoing MusicID...\n");
    error = gnsdk_musicid_query_find_albums(query_handle,&response_gdo);

    /* Display GDO type */
    error = gnsdk_manager_gdo_get_type(response_gdo,&response_type);
    printf("\nGDO type : %s\n",response_type);

    /* Get 1st Album GDO from response_gdo. Use gnsdk_manager_gdo_child_count(...)
     * to get how may album children the response has. Typically, a text search will
     * result in a multi match. */
    error = gnsdk_manager_gdo_child_get(response_gdo, GNSDK_GDO_CHILD_ALBUM, 1,  &album_gdo);

    if(!error)
    {
        gnsdk_str_t            out                     = 0;
        gnsdk_uint32_t        i                     = 0;

        /* Value Keys for metadata we are going to retrieve */
        gnsdk_char_t*        album_keys[] = {    GNSDK_GDO_VALUE_ALBUM_COMPILATION,
                                                GNSDK_GDO_VALUE_ALBUM_DISC_IN_SET,
                                                GNSDK_GDO_VALUE_ALBUM_LABEL,
                                                GNSDK_GDO_VALUE_ALBUM_TOTAL_IN_SET,
                                                GNSDK_GDO_VALUE_ALBUM_TRACK_COUNT,
                                                GNSDK_GDO_VALUE_CLASSICAL_DATA,
                                                GNSDK_GDO_VALUE_FULL_RESULT,
                                                GNSDK_GDO_VALUE_GLOBALID,
                                                GNSDK_GDO_VALUE_GNID,
                                                GNSDK_GDO_VALUE_GNUID,
                                                GNSDK_GDO_VALUE_YEAR,
                                                GNSDK_GDO_VALUE_PACKAGE_LANGUAGE,
                                                GNSDK_GDO_VALUE_PACKAGE_LANGUAGE_DISPLAY,
                                                GNSDK_GDO_VALUE_PRODUCTID,
                                                GNSDK_GDO_VALUE_TUI,
                                                GNSDK_GDO_VALUE_TUI_TAG
                                            };

        /* Labels for metadata we are going to get */
        gnsdk_char_t*            album_key_names[] = {
                                                "ALBUM_COMPILATION",
                                                "ALBUM_DISC_IN_SET",
                                                "ALBUM_LABEL",
                                                "ALBUM_TOTAL_IN_SET",
                                                "ALBUM_TRACK_COUNT",
                                                "CLASSICAL_DATA",
                                                "FULL_RESULT",
                                                "GLOBALID",
                                                "GNID",
                                                "GNUID",
                                                "YEAR",
                                                "PACKAGE_LANGUAGE",
                                                "PACKAGE_LANGUAGE_DISPLAY",
                                                "PRODUCTID",
                                                "TUI",
                                                "TUI_TAG"
                                                };

        /* Get album title GDO */
        error = gnsdk_manager_gdo_child_get(album_gdo, GNSDK_GDO_CHILD_TITLE_OFFICIAL, 1,  &album_title_gdo);
        if (!error)
        {
            /* Get Album Title */
            error = gnsdk_manager_gdo_value_get(album_title_gdo, GNSDK_GDO_VALUE_DISPLAY, 1, &out);
            if (!error)
               printf("%s:\t%s \n", "Album Title:",  out);
        }

        /* Loop thru all value keys in our array */
        for (i=0;i <  (sizeof(album_keys)/sizeof(album_keys[0])) ;i++)
        {
            gnsdk_uint32_t        vcount     =0;
            gnsdk_uint32_t        j         = 1;
            gnsdk_error_t        alb_error     = GNSDK_SUCCESS;

            error=    gnsdk_manager_gdo_value_count(album_gdo, album_keys[i], &vcount);
            if(GNSDK_SUCCESS != error)
            {
                continue;
            }

            while(alb_error == GNSDK_SUCCESS)
            {
                alb_error =    gnsdk_manager_gdo_value_get(album_gdo, album_keys[i], j, &out);
                if (alb_error == GNSDK_SUCCESS)
                {
                    printf("\t%s:\t%s \n", album_key_names[i],  out);
                    j++;
                }
            }
        } /* for all value keys */
    } /* if (!error) */


    /* Get Album track information */

    /* How may tracks on this album? */
    error = gnsdk_manager_gdo_child_count(album_gdo, GNSDK_GDO_CHILD_TRACK, &track_count);

    printf("\n%s\n", "Track Titles:");
    if(!error)
    {
        gnsdk_uint32_t        track_index     = 0;
        gnsdk_gdo_handle_t    track_gdo       = GNSDK_NULL;
        gnsdk_gdo_handle_t    track_title_gdo = GNSDK_NULL;

        /* Loop thru all Child Track GDOs */
        for (track_index = 1; track_index <= track_count; track_index++)
        {
            /* Get Child Track GDO */
            error = gnsdk_manager_gdo_child_get(album_gdo, GNSDK_GDO_CHILD_TRACK, track_index, &track_gdo);

            if (!error)
            {
               /* Get Child Track Title GDO */
               error = gnsdk_manager_gdo_child_get(track_gdo, GNSDK_GDO_CHILD_TITLE_OFFICIAL, 1, &track_title_gdo);
            }

            if(!error)
            {
                gnsdk_str_t       out               = 0;
                gnsdk_uint32_t    i                 = 0;
                gnsdk_char_t*     track_keys[]      = {GNSDK_GDO_VALUE_DISPLAY};
                gnsdk_char_t*     track_key_names[] = {"Title"};

                for (i=0;i<  sizeof(track_keys)/sizeof(track_keys[0]);i++)
                {
                    gnsdk_uint32_t   vcount       = 0;
                    gnsdk_uint32_t   j            = 1;
                    gnsdk_error_t    track_error  = GNSDK_SUCCESS;

                    error=    gnsdk_manager_gdo_value_count(track_title_gdo, track_keys[i], &vcount);
                    if(GNSDK_SUCCESS != error)
                    {
                        continue;
                    }

                    while(!track_error)
                    {
                        track_error    =gnsdk_manager_gdo_value_get(track_title_gdo, track_keys[i], j, &out);
                        if (!track_error){
                            printf("\tTrack - %d : %s:\t%s \n", track_index, track_key_names[i],  out);
                            j++;
                        }

                    } /* while */
                } /* for */
            } /* if (!error) */

            gnsdk_manager_gdo_release(track_gdo);
            gnsdk_manager_gdo_release(track_title_gdo);
        } /* for */
    } /* if (!error) */
    else
       printf("\nError Getting Track Count: 0x%x\n",error);


    printf("\nShutting down GNSDK\n");

    /* Release resources  */
    gnsdk_manager_gdo_release(album_gdo);
    gnsdk_manager_gdo_release(album_title_gdo);
    gnsdk_manager_gdo_release(input_gdo);
    gnsdk_manager_gdo_release(response_gdo);
    gnsdk_musicid_query_release(query_handle);

    return 0;

} /* _gdo_get_metadata() */


/*********************************************************************************
 *
 *    _SET_LOCALE
 *
 *  Set application locale. Note that this is only necessary if you are accessing
 *  locale-dependant fields such as genre, mood, origin, era, etc. Your app
 *  may or may not be accessing ocale_dependent fields, but it does not hurt
 *  to do this initialization as a matter of course .
 *
 **********************************************************************************/
static int
_set_locale(
    gnsdk_user_handle_t        user_handle
    )
{
    gnsdk_locale_handle_t  locale_handle    = GNSDK_NULL;
    gnsdk_error_t          error            = GNSDK_SUCCESS;
    int                    rc               = 0;

    error = gnsdk_manager_locale_load(
                GNSDK_LOCALE_GROUP_MUSIC,   /* Locale group */
                GNSDK_LANG_ENGLISH,         /* Languae */
                GNSDK_REGION_DEFAULT,       /* Region */
                GNSDK_DESCRIPTOR_DETAILED,  /* Descriptor */
                user_handle,                /* User handle */
                GNSDK_NULL,                 /* User callback function */
                0,                          /* Optional data for user callback function */
                &locale_handle              /* Return handle */
                );

    if (GNSDK_SUCCESS == error)
    {
        /* Setting the 'locale' as default
         * If default not set, no locale-specific results would be available
         */
        error = gnsdk_manager_locale_set_group_default(locale_handle);
        if (GNSDK_SUCCESS != error)
        {
            _display_last_error(__LINE__);
            rc = -1;
        }

        /* The manager will hold onto the locale when set as default
         * so it's ok to release our reference to it here
         */
        gnsdk_manager_locale_release(locale_handle);
    }
    else
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    return rc;

} /* _set_locale() */


/*****************************************************************************************
 *
 *    _GET_USER_HANDLE
 *
 *    Load existing user handle, or register new one.
 *
 *    GNSDK requires a user handle instance to perform queries.
 *    User handles encapsulate your Gracenote provided Client ID which is unique for your
 *    application. User handles are registered once with Gracenote then must be saved by
 *    your application and reused on future invocations.
 *
 *******************************************************************************************/
static int
_get_user_handle(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_error_t        error               = GNSDK_SUCCESS;
    gnsdk_user_handle_t  user_handle         = GNSDK_NULL;
    char*                user_filename       = NULL;
    size_t               user_filename_len   = 0;
    int                  rc                  = 0;
    FILE*                file                = NULL;


    user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
    user_filename = malloc(user_filename_len);

    if (NULL != user_filename)
    {
        strcpy(user_filename,client_id);
        strcat(user_filename,"_user.txt");

        /* Do we have a user saved locally? */
        file = fopen(user_filename, "r");
        if (NULL != file)
        {
            gnsdk_char_t serialized_user_string[1024] = {0};

            if (NULL != (fgets(serialized_user_string, 1024, file)))
            {
                /* Create the user handle from the saved user */
                error = gnsdk_manager_user_create(serialized_user_string, &user_handle);
                if (GNSDK_SUCCESS != error)
                {
                    _display_last_error(__LINE__);
                    rc = -1;
                }
            }
            else
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
            fclose(file);
        }
        else
        {
            printf("\nInfo: No stored user - this must be the app's first run.\n");
        }

        /* If not, create new one */
        if (GNSDK_NULL == user_handle)
        {
            error = gnsdk_manager_user_create_new(
                        client_id,
                        client_id_tag,
                        client_app_version,
                        &user_handle
                        );
            if (GNSDK_SUCCESS != error)
            {
                _display_last_error(__LINE__);
                rc = -1;
            }
        }

        free(user_filename);
    }
    else
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    if (rc == 0)
    {
        *p_user_handle = user_handle;
    }

    return rc;

} /* _get_user_handle() */


/****************************************************************************************
 *
 *    _INIT_GNSDK
 *
 *     Initializing the GNSDK is required before any other APIs can be called.
 *     First step is to always initialize the Manager module, then use the returned
 *     handle to initialize any modules to be used by the application.
 *
 *     For this sample, we also load a locale which is used by GNSDK to provide
 *     appropriate locale-sensitive metadata for certain metadata values. Loading of the
 *     locale is done here for sample convenience but can be done at anytime in your
 *     application.
 *
 ****************************************************************************************/
static int _init_gnsdk(
    const char*             client_id,
    const char*             client_id_tag,
    const char*             client_app_version,
    const char*             license_path,
    gnsdk_user_handle_t*    p_user_handle
    )
{
    gnsdk_manager_handle_t   sdkmgr_handle    = GNSDK_NULL;
    gnsdk_error_t            error            = GNSDK_SUCCESS;
    gnsdk_user_handle_t      user_handle      = GNSDK_NULL;
    int                      rc               = 0;

    /* Display GNSDK Product Version Info */
    _display_gnsdk_product_info();

    /* Initialize the GNSDK Manager */
    error = gnsdk_manager_initialize(
                &sdkmgr_handle,
                license_path,
                GNSDK_MANAGER_LICENSEDATA_FILENAME
                );

    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }


    /* Initialize the MusicID SDK */
    error = gnsdk_musicid_initialize(sdkmgr_handle);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
        rc = -1;
    }

    /* Get a user handle for our client ID.  This will be passed in for all queries */
    if (0 == rc)
    {
        rc = _get_user_handle(client_id, client_id_tag, client_app_version, &user_handle );
    }

    if (0 == rc)
    {
        rc = _set_locale(user_handle );
    }

    if (0 != rc)
    {
        /* Clean up on failure. */
        _shutdown_gnsdk(user_handle, client_id);
    }
    else
    {
        /* Return the User handle for use at query time */
        *p_user_handle = user_handle;
    }

    return rc;

}    /* _init_gnsdk() */


/***************************************************************************
 *
 *    _SHUTDOWN_GNSDK
 *
 *        Call shutdown on all initialized GNSDK modules.
 *        Release all existing handles before shutting down any of the modules.
 *        Shutting down the Manager module should occur last, but the shutdown
 *        ordering of all other modules does not matter.
 *
 ***************************************************************************/
static void _shutdown_gnsdk(
    gnsdk_user_handle_t        user_handle,
    const char*                client_id
    )
{
    gnsdk_error_t    error                          = GNSDK_SUCCESS;
    gnsdk_str_t      updated_serialized_user_string = GNSDK_NULL;
    char*            user_filename                  = GNSDK_NULL;
    size_t           user_filename_len              = 0;
    int              rc                             = 0;
    int              return_value                   = 0;
    FILE*            file                           = NULL;

    /* Release our user handle and see if we need to update our stored version */
    error = gnsdk_manager_user_release(user_handle, &updated_serialized_user_string);
    if (GNSDK_SUCCESS != error)
    {
        _display_last_error(__LINE__);
    }
    else if (GNSDK_NULL != updated_serialized_user_string)
    {
        user_filename_len = strlen(client_id)+strlen("_user.txt")+1;
        user_filename = malloc(user_filename_len);

        if (NULL != user_filename)
        {
            strcpy(user_filename,client_id);
            strcat(user_filename,"_user.txt");

            file = fopen(user_filename, "w");
            if (NULL != file)
            {
                return_value = fputs(updated_serialized_user_string, file);
                if (0 > return_value)
                {
                    printf("\nError writing user registration file from buffer.\n");
                    rc = -1;
                }
                fclose(file);
            }
            else
            {
                printf("\nError: Failed to open the user filename for use in saving the updated serialized user. (%s)\n", user_filename);
            }
            free(user_filename);
        }
        else
        {
            printf("\nError: Failed to allocated user filename for us in saving the updated serialized user.\n");
        }
        gnsdk_manager_string_free(updated_serialized_user_string);

    } /* else if (GNSDK_NULL != updated_serialized_user_string) */

    /* Shutdown the libraries */
    gnsdk_manager_shutdown();

}    /* _shutdown_gnsdk() */

