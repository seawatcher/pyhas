var a00077 =
[
    [ "FilterConditionType", "a00005.html", "a00005" ],
    [ "FilterListType", "a00006.html", "a00006" ],
    [ "GnMoodgridPresentation", "a00077.html#a913cafa77967e0a8f1e11bb27df4350d", null ],
    [ "GnMoodgridPresentation", "a00077.html#aa979fc59fb5e2dc7d6afde0c9db13d8b", null ],
    [ "addFilter", "a00077.html#a2336a3821750380c578e43e958b16c0d", null ],
    [ "delete", "a00077.html#ad2e28eccc73439cc4fe26f9c45eb2a4b", null ],
    [ "finalize", "a00077.html#a0ed926d69240f2b866bf237d3b66575e", null ],
    [ "findRecommendations", "a00077.html#a87c6916bf404ca6aa8fa9071dc4709a9", null ],
    [ "findRecommendationsEstimate", "a00077.html#aacd291a7b6104cda9dabdc048c1aae62", null ],
    [ "layoutType", "a00077.html#a39d810779b9429f806b1882ef01d1abe", null ],
    [ "moodId", "a00077.html#a12d826aa34c3387427e79494c5007aec", null ],
    [ "moodName", "a00077.html#a7214608fec0cee453f2afa67f7874ae2", null ],
    [ "moods", "a00077.html#a5c1e799b5f96c4acfb0bcb92e4699597", null ],
    [ "removeAllFilters", "a00077.html#a924dc13af00e6bdbf6fe34387fd6b7a4", null ],
    [ "removeFilter", "a00077.html#a81dae3c209a811461ecfe5383065808b", null ]
];