#!/usr/bin/python

import sys, pygnCustom, json


clientID = '1829888-E8CE842433F197651E893514809873C9' # Enter your Client ID from developer.gracenote.com here
userID = '264115232554776975-8A33607C14A4C0B33AC7827AF6D4E263' # Get a User ID from pygn.register() - Only register once per end-user

print '\nSearch for artist "Goblin "\n'
result = pygn.search(clientID=clientID, userID=userID, artist='Goblin', album='Profondo rosso', track='Death Dies')
print json.dumps(result, sort_keys=True, indent=4)

