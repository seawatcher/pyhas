var class_gracenote_s_d_k_1_1_gn_response_data_matches =
[
    [ "GnResponseDataMatches", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#adebbdef17a925111cf1e7bc037d051b0", null ],
    [ "GnResponseDataMatches", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#a250d1c816945f19b5c14234dc42e2805", null ],
    [ "DataMatches", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#a614d85adf6e5409a3f2620cd1deb1500", null ],
    [ "Dispose", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#ac58e1e2f19a67c9a961d4d83f143ed95", null ],
    [ "NeedsDecision", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#af188b2aa5c9eae46b35c303c7ef53192", null ],
    [ "RangeEnd", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#a0cedd5ce3014272808881f348d7f0a23", null ],
    [ "RangeStart", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#af2ef7730e835811a74f80db98c72f99a", null ],
    [ "RangeTotal", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#a4ff6579d229b7cd5cf39969dbd102af5", null ],
    [ "ResultCount", "class_gracenote_s_d_k_1_1_gn_response_data_matches.html#aac933c589fee3b6671a0e77e33f362a2", null ]
];