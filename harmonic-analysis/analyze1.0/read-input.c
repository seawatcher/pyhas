 /***************************************************************************/
 /*                                                                         */
 /*       Copyright (C) 1996  Daniel Sleator and David Temperley            */
 /*  See file "README" for information about commercial use of this system  */
 /*                                                                         */
 /***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "analyze.h"

/* This file contains the code that reads in the input.  It
   builds the beatlevel structures and forms a Note list
   from the input. */

static int line_no;  /* the line number */
static char line[1000];

typedef struct event_struct {
  /* this structure is just used for reading in the notes */
  int note_on;  /* 1 if this is note on, 0 otherwise */
  Pitch pitch;
  int time;     /* the time of this event */
  struct event_struct * next;
} Event;

void free_event_list(Event *e) {
    Event *xe;
    for (; e != NULL; e = xe) {
	xe = e->next;

	free ((char *) e);
    }
}

void test_pitch(Pitch pitch) {
  if (pitch < 0 || pitch > MAX_PITCH-1) {
    fprintf (stderr, "Error (line %d) Found a pitch that is out of range: %d\n", line_no, pitch);
    exit(1);
  }
}

void warn(void) {
  char * x;
  static int first_warning = 1;
  x = strchr(line, '\n');
  if (x != NULL) *x = '\0';
  if (first_warning) {
    printf("Ignoring the following lines:\n");
    first_warning = 0;
  }
  printf("---> %s\n", line);
}

Event * build_event_list_from_input(void) {
  char first_part[100];
  int second_part;
  int third_part;
  int fourth_part;
  int event_time;
  int N_parts;
  int on_event, both_event, off_event;
  int on_time, off_time;
  int i;
  Pitch pitch;
  Event * event_list, * new_event;

  beatlevel[0].count = 1;
  beatlevel[0].start = 0;
  beatlevel[0].units = 1;
  N_beatlevel = 1;
  
  event_time = 0;
  event_list = NULL;

  for (line_no = 1; fgets(line, sizeof(line), instream) != NULL; line_no++) {
    for (i=0; isspace(line[i]); i++);
    if (line[i] == '%' || line[i] == '\0') continue;  /* ignore comment lines */
    N_parts = sscanf(line, "%99s %d %d %d", first_part, &second_part, &third_part, &fourth_part);
    if (N_parts < 2) {
      warn();
      continue;
    }

    if (strcasecmp(first_part, "BaseUnit") == 0) {
      if (N_parts != 2) {
	fprintf(stderr, "Error (line %d) BaseUnit expects one integer parameter.\n", line_no);
	exit(1);
      }
      baseunit = second_part;
      continue;
    }

    if (strcasecmp(first_part, "BeatLevel") == 0) {
      if (N_parts != 4) {
	fprintf(stderr, "Error (line %d) BeatLevel expects three integer parameters.\n", line_no);
	exit(1);
      }

      if (N_beatlevel != second_part) {
	fprintf(stderr,"Error (line %d) A BeatLevel numbers must be in increasing order starting from 1.\n", line_no);
	exit(1);	
      }
      
      if (N_beatlevel+1 > MAX_BEAT_LEVEL) {
	fprintf(stderr,"Error (line %d) A BeatLevel cannot be greater than %d\n", line_no, MAX_BEAT_LEVEL);
	exit(1);	
      }
      beatlevel[N_beatlevel].count = third_part;
      beatlevel[N_beatlevel].start = fourth_part;
      beatlevel[N_beatlevel].units = beatlevel[N_beatlevel].count * beatlevel[N_beatlevel-1].units;

      if (verbosity >= 2) {
	printf(" beatlevel[%d].count(start)(units) =%d (%d)(%d)\n", N_beatlevel, third_part, fourth_part, beatlevel[N_beatlevel].units);
      }
      
      N_beatlevel++;
      continue;
    }

    on_event = (strcasecmp(first_part, "Note-on") == 0);
    both_event = (strcasecmp(first_part, "Note") == 0);
    off_event = (strcasecmp(first_part, "Note-off") == 0);

    if (!(on_event || off_event || both_event)) {
      warn();
      continue;
    }
    if (on_event || off_event) {
      if (N_parts != 3) {
	fprintf(stderr, "Error (line %d) a note needs a time and a pitch\n", line_no);
	exit(1);		
      }
      
      event_time = second_part;
      pitch = third_part;

      test_pitch(pitch);
      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = on_event;
      new_event->time = event_time;
      new_event->next = event_list;
      event_list = new_event;
    } else {
      /* it's a both event */
      if (N_parts != 4) {
	fprintf(stderr, "Error (line %d) a note needs an on-time an off-time and a pitch\n", line_no);
	exit(1);		
      }
      on_time = second_part;
      off_time = third_part;
      pitch = fourth_part;
      
      test_pitch(pitch);      
      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = 1;
      new_event->time = on_time;
      new_event->next = event_list;
      event_list = new_event;

      new_event = (Event *) malloc(sizeof(Event));
      new_event->pitch = pitch;
      new_event->note_on = 0;
      new_event->time = off_time;
      new_event->next = event_list;
      event_list = new_event;
    }
  }
  
  return event_list;
}


Note * reverse(Note * a) {
  Note *slx;
  Note *b = NULL;
    
    for (; a != NULL; a = slx) {
	slx = a->next;
	
	a->next = b;
	b = a;
    }
    return b;
}

NPC Pitch_to_NPC(Pitch p) {
  return (p+12000000) % 12;
  /* the 12000000 guarantees that it works correctly for negative numbers
     (as long as they're at least -12000000).*/
}

TPC base_TPC(NPC n) {
  /* this returns the base tpc for a NPC */
  /* These are numbered 0...11 */
  return ((2 + 7*n + 12000000) % 12);
}

int comp_event(Event ** a, Event ** b) {
  if ((*a)->time != (*b)->time) return  ((*a)->time - (*b)->time);
  return ((*a)->note_on - (*b)->note_on);
}

/* This function builds a note list from an event list.  This note list
   is sorted by increasing starting time.  And notes are broken up so
   that each pair of notes either cover the identical time interval or
   they cover disjoint intervals. 

   The following fields of each note are filled in:
       start, duration, pitch, npc, base_tpc, next
*/
Note * build_note_list_from_event_list(Event * e) {
  int event_time, previous_event_time;
  int on_event, i, xet, event_n;
  Pitch pitch;
  Note * note_list, * new_note;
  char pitch_in_use[MAX_PITCH];
  char already_emitted[MAX_PITCH];  /* to compute ornamental dissonance marks */
  Event *el;
  Event ** etable;
  int N_events;

  for (N_events=0, el = e; el != NULL; el = el->next) N_events++;
  etable = (Event **) malloc (N_events * sizeof (Event *));
  for (i=0, el = e; el != NULL; el = el->next, i++) etable[i] = el;

  qsort(etable, N_events, sizeof (Event *), (int (*)(const void *, const void *))comp_event);

  for(i=0; i<MAX_PITCH; i++) pitch_in_use[i] = already_emitted[i] = 0;

  event_time = 0;
  note_list = NULL;

  /*
  for (i=0; i<N_events; i++) {
    el = etable[i];
    printf("event: pitch = %d  note_on = %d  time = %d \n", el->pitch, el->note_on, el->time);
  }
  */

  for (event_n=0; event_n<N_events; event_n++) {
    el = etable[event_n];

    previous_event_time = event_time;

    xet = el->time;
    pitch = el->pitch;
    on_event = el->note_on;

    event_time = xet;

    if (on_event && pitch_in_use[pitch]) {
      fprintf (stderr, "Pitch %d is already on, but it just got turned on.\n", pitch);
      exit(1);
    }

    if (!on_event && !pitch_in_use[pitch]) {
      fprintf (stderr, "Pitch %d is not on, but it just got turned off.\n", pitch);
      exit(1);
    }

    /* now, for each pitch that's turned on, we generate a note */
    /* the onset time is previous_event_time, the duration is */
    /* the diff between that and event_time */

    if (previous_event_time > event_time) {
      fprintf(stderr, "Events are not sorted by time.\n");
      exit(1);
    }

    if (previous_event_time != event_time) {
      /* Don't create any notes unless some time has elapsed */
      for(i=0; i<MAX_PITCH; i++) {
	if (pitch_in_use[i]) {
	  new_note = (Note*) malloc(sizeof(Note));
	  new_note->pitch = i;
	  new_note->start = previous_event_time;
	  new_note->duration = event_time - previous_event_time;
	  new_note->npc = new_note->base_tpc = new_note->tpc = 0;
	  new_note->orn_dis_penalty = 0.0;
	  new_note->next = note_list;
	  new_note->is_first_note = (!already_emitted[i]);
	  already_emitted[i] = 1;
	  note_list = new_note;
	}
      }
    }
    pitch_in_use[pitch] = on_event;
    already_emitted[pitch] = 0;
  }

  for(i=0; i<MAX_PITCH; i++) {
    if (pitch_in_use[i]) {
      fprintf(stderr, "Pitch %d was still in use at the end.\n", i);
      exit(1);
    }
  }

  for (new_note =  note_list; new_note != NULL; new_note = new_note->next) {
    new_note->npc = Pitch_to_NPC(new_note->pitch);
    new_note->base_tpc = base_TPC(new_note->npc);
  }

  free(etable);

  return reverse(note_list);
}

void build_note_array(Note * nl) {
  Note * xnl;
  int i;
  for (N_notes = 0, xnl = nl; xnl != NULL; xnl = xnl->next) N_notes++;  /* compute N_notes */
  note_array = (Note **) malloc(N_notes * sizeof(Note *));
  if (note_array == NULL) {
    fprintf(stderr, "Malloc of note_array failed\n");
    exit(1);
  }
  for (i=0, xnl = nl; xnl != NULL; xnl = xnl->next, i++) {
    note_array[i] = xnl;
  }
}

Note * build_note_list_from_input(void) {
  /* build the note list, also create the note array, also set N_notes */
  Event * e;
  Note * nl;
  e = build_event_list_from_input();
  nl = build_note_list_from_event_list(e);
  build_note_array(nl);
  free_event_list(e);
  return nl;
}
